package discovery.test.mockups;

import discovery.base.search.Stoppable;
import discovery.test.common.mockups.discovery.StoppableMockup;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DhtStartOptions;
import haxe.Exception;
import discovery.dht.dhttypes.DoneCallback;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.Dht;
import discovery.dht.dhttypes.DhtValue;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;
import org.hamcrest.Matchers.*;
import discovery.utils.EqualsComparator.*;

using discovery.test.matchers.CommonMatchers;


typedef DhtPutArgs = {
    key: DhtKey,
    value: DhtValue,
    callback:DoneCallback,
    ?options:DhtPutOptions
};

@:structInit
class DhtGetArgs{
    public var key:DhtKey;
    public var listener:DhtGetListener;

    @:optional
    public var stoppableMockup(default, null):StoppableMockup;

    public function shouldHaveBeenStopped() {
        this.stoppableMockup.shouldHaveBeenStopped();
    }
}

class DhtMockup {
    var dhtMock:Dht;

    var getCalls:Array<DhtGetArgs>;
    var putCalls:Array<DhtPutArgs>;
    var usedStartOptions:DhtStartOptions;

    var dhtStarted:Bool=false;
    var boundAddress:PeerAddress=null;

    public function new() {
        getCalls = [];
        putCalls = [];
        dhtMock = Mockatoo.mock(Dht);

        when(dhtMock.put(any, any, any)).thenCall(onDhtPut);
        when(dhtMock.start(any)).thenCall(onDhtStart);
        when(dhtMock.get(any, any)).thenCall(onDhtGet);
        when(dhtMock.boundAddress()).thenCall(()->{return boundAddress;});
    }

    function onDhtGet(args: Array<Any>): Stoppable{
        var stoppableMockup = new StoppableMockup();

        getCalls.push({
            key: args[0],
            listener: args[1],
            stoppableMockup: stoppableMockup
        });

        return stoppableMockup.stoppable();
    }

    function onDhtPut(args :Array<Any>){
        putCalls.push({
            key: args[0],
            value: args[1],
            callback: args[2],
            options: args[3]
        });
    }

    function onDhtStart(args: Array<Any>) {
        usedStartOptions = args[0];

        if(dhtStarted
            && usedStartOptions != null
            && usedStartOptions.onStart != null)
        {
            usedStartOptions.onStart();
        }
    }

    public function dht() {
        return dhtMock;
    }


    public function setBoundAddress(bound:PeerAddress) {
        this.boundAddress = bound;
    }


    public function theLastGetQuery() {
        getCalls.shouldNotBeEmpty('No query was made');

        return getCalls[getCalls.length - 1];
    }


    public function verifyNoQuerySent() {
        getCalls.shouldBeEmpty('No query should have been sent');
    }

    public function verifyQueryFor(key:DhtKey) {
        var matchingCalls = getCalls.filter((call:DhtGetArgs)->{
            return call != null
                && equals(call.key, key);
        });

        assertThat(matchingCalls, hasSize(greaterThan(0)),
            'Number of calls to get(${key})');
    }

    public function verifyPutValue(
        key:DhtKey, dhtValue:DhtValue, ?verificationMode: VerificationMode)
    {

        var matchingCalls = putCalls.filter((call:DhtPutArgs)->{
            return call != null
                && equals(call.key, key) //review: key not matching
                && equals(call.value, dhtValue)
                ;
        });

        var matcher = verificationModeToMatcher(verificationMode);

        assertThat(matchingCalls, matcher,
            'Number of calls to put(key: ${key}, value: ${dhtValue})');
    }


    function verificationModeToMatcher(verificationMode:Null<VerificationMode>){
        var matcher:org.hamcrest.Matcher<Any>;

        switch (verificationMode){
            case times(n): matcher = hasSize(n);
            case atLeast(n): matcher = hasSize(greaterThan(n));
            case atMost(n): matcher = hasSize(lessThan(n));
            case never: matcher = isEmpty();
            default: matcher = hasSize(greaterThan(0));
        }

        return matcher;
    }

    public function notifyGetResultFor(key:DhtKey, values:Array<DhtValue>) {
        var evt:DhtDataEvent = {
            values: values
        };

        notifyQueryFor(key, (call)->{
            if(call.listener.onData != null){
                call.listener.onData(evt);
            }
        });
    }

    public function notifyQueryFinished(key:DhtKey, ?err:Exception) {
        notifyQueryFor(key, (call)->{
            if(call.listener.onEnd != null){
                call.listener.onEnd(err);
            }
        });
    }

    function notifyQueryFor(key:DhtKey, callback:(DhtGetArgs)->Void) {
        for(call in getCalls){
            if(call.listener != null
                && equals(call.key, key))
            {
                callback(call);
            }
        }
    }

    public function notifyPutResultFor(key:DhtKey, ?err:Exception) {
        for(call in putCalls){
            if(call.callback != null && equals(call.key, key))
            {
                call.callback(err);
            }
        }
    }

    public function notifyLastPutResult(?err:Exception) {
        var putArgs = lastPutArgs();

        assertThat(putArgs.callback, is(notNullValue()),
            "Last put has no callback");

        putArgs.callback(err);
    }

    public function lastPutArgs(): DhtPutArgs{
        assertThat(putCalls, not(isEmpty()), "No put call was made yet");

        return putCalls[putCalls.length - 1];
    }

    public function verifyDhtStarted(?mode: VerificationMode) {
        Mockatoo.verify(dhtMock.start(any), mode);
    }

    public function verifyDhtStartedWith(options:DhtOptions) {
        if(options == null){
            usedStartOptions.shouldBe(null);
            return;
        }

        usedStartOptions.shouldNotBeNull('no start options specified');
        usedStartOptions.bind.shouldBeEqualsTo(options.bind, 'bind address');
        usedStartOptions.bootstrap.shouldBeEqualsTo(options.bootstrap, 'bootstrap address');
    }

    public function lastStartOptions():DhtStartOptions {
        return usedStartOptions;
    }

    public function notifyDhtStarted() {
        notifyDhtStartedWith();
    }


    public function notifyDhtStartedWith(?err: Exception) {
        var startOptions = lastStartOptions();

        if(startOptions != null && startOptions.onStart != null){
            startOptions.onStart(err);
        }
    }

    public function startDht() {
        notifyDhtStarted();

        dhtStarted = true;
    }
}