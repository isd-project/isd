package discovery.test.mockups;

import discovery.dht.dhttypes.DhtOptions;
import mockatoo.Mockatoo;
import discovery.dht.dhttypes.building.DhtComponents;

class DhtComponentsMockup {
    var componentsMock:DhtComponents;
    var dhtGetterMockup:DhtGetterMockup;
    var hashMakerMockup:HashMakerMockup;

    public function new() {
        dhtGetterMockup = new DhtGetterMockup();
        hashMakerMockup = new HashMakerMockup();

        componentsMock = Mockatoo.mock(DhtComponents);

        Mockatoo
            .when(componentsMock.dhtGetter())
            .thenReturn(dhtGetter().getter());

        Mockatoo
            .when(componentsMock.hashMaker())
            .thenReturn(hashMakerMockup.hashMaker());
    }

    public function components() {
        return componentsMock;
    }

    public function dhtGetter() {
        return dhtGetterMockup;
    }

    public function dht(){
        return dhtGetter().dht();
    }

    public
    function verifyConfiguredWith(options:DhtOptions, ?times:VerificationMode) {
        dhtGetter().verifyConfiguredWith(options, times);
    }
}