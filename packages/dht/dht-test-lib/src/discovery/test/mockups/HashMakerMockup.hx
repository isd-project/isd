package discovery.test.mockups;

import discovery.format.Binary;
import mockatoo.Mockatoo;
import discovery.dht.crypto.HashMaker;

class HashMakerMockup {
    var hashMakerMock:HashMaker;

    var hashedStrings:Array<String>;

    public function new() {
        hashedStrings = [];

        hashMakerMock = Mockatoo.mock(HashMaker);

        Mockatoo.when(hashMakerMock.hashString(any)).thenCall(onHashString);
        Mockatoo.when(hashMakerMock.hashBinary(any)).thenCall(onHashBinary);
    }

    function onHashString(args: Array<Any>) {
        var str = args[0];
        hashedStrings.push(str);

        return Binary.fromText(str);
    }

    function onHashBinary(args: Array<Any>) {
        var bin = args[0];

        return binaryHashOf(bin);
    }

    public function binaryHashOf(binary:Binary): Binary{
        return binary.clone();
    }

    public function hashMaker():HashMaker {
        return hashMakerMock;
    }

    public function checkHashMakeFromString(stringValue: String) {
        Mockatoo.verify(hashMakerMock.hashString(stringValue));
    }

    public function checkHashMakeFromBinary(binary:Binary) {
        Mockatoo.verify(hashMakerMock.hashBinary(binary));
    }

    public function lastHashedString() {
        if(hashedStrings.length == 0) return null;

        return hashedStrings[hashedStrings.length - 1];
    }
}