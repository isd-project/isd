package discovery.test.mockups;

import discovery.dht.dhttypes.PeerAddress;
import js.lib.Error;
import discovery.test.mockups.js.StreamFake;
import discovery.dht.impl.js.dhtrpc.externs.QueryData;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.format.Binary;
import discovery.dht.impl.js.dhtrpc.externs.DhtCommand;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpcBuildOptions;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpcBuilder;
import mockatoo.Mockatoo;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpc;

import org.hamcrest.Matchers.*;


typedef DhtCommandArgs = {
    command: String,
    target: Binary,
    ?value: DrpcValue,
    ?callback: QueryDoneCallback,
    ?stream: StreamFake
};

class DhtRpcMockup implements DhtRpcBuilder{
    var dhtMock:DhtRpc;

    var commands:Map<String, DhtCommand>;
    var updateCalls:Map<String,DhtCommandArgs>;
    var queryCalls:Map<String,DhtCommandArgs>;

    public function new() {
        commands = new Map();
        updateCalls = new Map();
        queryCalls = new Map();

        dhtMock = Mockatoo.mock(DhtRpc);

        Mockatoo.when(dhtMock.command(any, any)).thenCall(onDhtCommand);
        Mockatoo
            .when(dhtMock.update(any, any, any, any))
            .thenCall(onDhtUpdate);

        Mockatoo
            .when(dhtMock.query(any, any, any, any))
            .thenCall(onDhtQuery);
    }

    public function dht(): DhtRpc {
        return dhtMock;
    }

    public function build(?options:DhtRpcBuildOptions):DhtRpc {
        return dht();
    }

    public function checkCommandCreated(command:String) {
        Mockatoo.verify(dhtMock.command(command, isNotNull));
    }

    public function checkListenCalledWith(addr:PeerAddress) {
        Mockatoo.verify(dhtMock.listen(addr.port, addr.address, any));
    }

    public function getCommand(cmdName:String): DhtCommand {
        return commands.get(cmdName);
    }

    public function updateCalledFor(command:String): DhtCommandArgs{
        Mockatoo.verify(dhtMock.update(command, any, any, any));

        return updateCalls.get(command);
    }

    public function queryCalledFor(cmdName:String): DhtCommandArgs{
        Mockatoo.verify(dhtMock.query(cmdName, any, any, any));

        return queryCalls.get(cmdName);
    }

    public function failsLastUpdateWith(cmdName:String, err:js.lib.Error) {
        var callback = requireCommandCallback(cmdName, updateCalls);

        callback(err, null);
    }

    public function queryReceiveData(cmdName:String, data:QueryData) {
        var args = requireCommandArgs(cmdName, queryCalls);

        args.stream.emit('data', data);
    }


    public function lastQueryEnded(cmdName:String, ?err: Error) {
        var callback = requireCommandCallback(cmdName, queryCalls);

        callback(err, []);
    }

    //  --------------------------------------------------

    function requireCommandCallback(cmdName:String,
                                    calls:Map<String,DhtCommandArgs>)
    {
        var args = requireCommandArgs(cmdName, calls);

        assertThat(args.callback, is(notNullValue()), "Command call not received callback");

        return args.callback;
    }

    function requireCommandArgs(cmdName:String,
                                calls:Map<String,DhtCommandArgs>)
    {
        var args = calls.get(cmdName);

        assertThat(args, is(notNullValue()), "Command was not called");

        return args;
    }

    //  --------------------------------------------------

    function onDhtCommand(args: Array<Any>) {
        var cmdName:String = args[0];
        var cmd:DhtCommand = args[1];

        commands[cmdName] = cmd;
    }

    function onDhtUpdate(args: Array<Any>) {
        return putCommandArgsAt(args, updateCalls);
    }

    function onDhtQuery(args: Array<Any>) {
        return putCommandArgsAt(args, queryCalls);
    }

    function putCommandArgsAt(args: Array<Any>, map:Map<String, DhtCommandArgs>)
    {
        var stream = new StreamFake();
        var commandArgs = extractCommandArgs(args);
        commandArgs.stream = stream;

        map[commandArgs.command] = commandArgs;

        return stream;
    }

    function extractCommandArgs(args:Array<Any>): DhtCommandArgs{
        var target = Binary.fromBuffer(args[1]);

        var commandArgs:DhtCommandArgs = {
            command: args[0],
            target: target,
            value: args[2],
            callback: args[3]
        };

        return commandArgs;
    }
}