package discovery.test.mockups.js;

import discovery.test.fakes.JsEventEmitterFake;

typedef StreamFake = JsEventEmitterFake;