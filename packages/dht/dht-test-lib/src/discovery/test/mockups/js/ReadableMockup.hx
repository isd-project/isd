package discovery.test.mockups.js;

import haxe.Constraints.Function;
import discovery.test.fakes.JsEventEmitterFake;
import mockatoo.Mockatoo;
import js.node.stream.Readable.IReadable;


class ReadableMockup {
    var readableMock:IReadable;

    var fakeEventEmitter:JsEventEmitterFake;

    public function new() {
        fakeEventEmitter = new JsEventEmitterFake();

        readableMock = Mockatoo.mock(IReadable);
        Mockatoo.when(readableMock.on(any, any)).thenCall(registerEvent);
    }

    function registerEvent(args: Array<Dynamic>) {
        var eventName:EventName<Function> = args[0];
        var listener:Function = args[1];

        fakeEventEmitter.on(eventName, listener);
    }

    public function readable():IReadable {
        return readableMock;
    }

    public function verifyDestroyCalled() {
        Mockatoo.verify(readableMock.destroy(any));
    }

    public function emitEvent(event:String) {
        fakeEventEmitter.emit(event);
    }
}