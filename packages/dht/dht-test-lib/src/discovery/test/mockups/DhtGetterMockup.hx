package discovery.test.mockups;

import discovery.dht.Dht;
import haxe.Exception;
import discovery.dht.dhttypes.DhtOptions;
import mockatoo.Mockatoo;
import discovery.dht.DhtGetter;

class DhtGetterMockup {
    var getterMock:DhtGetter;
    var dhtMockup:DhtMockup;

    var getDhtCallbacks:Array<GetDhtCallback> = [];

    public function new() {
        dhtMockup = new DhtMockup();

        getterMock = Mockatoo.mock(DhtGetter);

        Mockatoo.when(getterMock.getDht(any)).thenCall(onGetDht);
    }

    function onGetDht(args: Array<Dynamic>) {
        this.getDhtCallbacks.push(args[0]);
    }

    public function getter():DhtGetter {
        return getterMock;
    }

    public function dht():DhtMockup {
        return dhtMockup;
    }

    public function notifyDhtReceived() {
        notifyGetDhtWith(dht().dht());
    }

    public function notifyDhtStartupFailed(error:Exception) {
        notifyGetDhtWith(null, error);
    }

    function notifyGetDhtWith(dht: Null<Dht>, ?error:Exception) {
        for (callback in getDhtCallbacks){
            if(callback != null){
                callback(dht, error);
            }
        }
    }

    public function verifyDhtWasRetrieved() {
        Mockatoo.verify(getterMock.getDht(any));
    }

    public function
        verifyConfiguredWith(dhtOptions:DhtOptions, ?times:VerificationMode)
    {
        Mockatoo.verify(getterMock.configure(dhtOptions), times);
    }
}