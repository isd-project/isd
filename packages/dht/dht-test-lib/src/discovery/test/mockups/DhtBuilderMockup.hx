package discovery.test.mockups;

import discovery.dht.DhtGetter;
import discovery.dht.dhttypes.startup.DefaultDhtStarter;
import discovery.dht.discoverymechanism.DhtStarter;
import discovery.dht.crypto.HashMaker;
import mockatoo.Mockatoo;
import discovery.dht.DhtBuilder;

class DhtBuilderMockup {
    var builderMock:DhtBuilder;

    var dhtMockup:DhtMockup;
    var hashMakerMockup:HashMakerMockup;

    public function new() {
        dhtMockup = new DhtMockup();
        hashMakerMockup = new HashMakerMockup();

        builderMock = Mockatoo.mock(DhtBuilder);

        Mockatoo.when(builderMock.build())
            .thenReturn(dhtMockup.dht());

        Mockatoo.when(builderMock.buildHashMaker())
            .thenReturn(hashMakerMockup.hashMaker());
    }

    public function builder():DhtBuilder {
        return builderMock;
    }

    public function dhtStarter():DhtStarter {
        return new DefaultDhtStarter({
            dhtBuilder: builder()
        });
    }

    public function dhtGetter(): DhtGetter{
        return new DefaultDhtStarter({
            dhtBuilder: builder()
        });
    }

    public function verifyDhtBuilt(?mode: VerificationMode) {
        Mockatoo.verify(builderMock.build(), mode);
    }

    public function dht() {
        return dhtMockup;
    }

    public function hashMaker():HashMaker {
        return hashMakerMockup.hashMaker();
    }
}