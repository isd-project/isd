package discovery.test.matchers.js;

import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import org.hamcrest.Matcher;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;

using discovery.utils.EqualsComparator;


class StorageValueMatchers {

    public static function withSameValue(storageValue:StorageValue) {
        return thatMatches(StorageValue, hasValueFilter(storageValue.value),
                    'with same value of: ${storageValue.value}');
    }

    public static function whichExpired():Matcher<StorageValue> {
        return thatMatches(StorageValue, (v:StorageValue)->{
            return v.expired();
        }, "an expired StorageValue", (v:StorageValue)->{
            if(v.expired())
                 return '${v} expired';
            else return '${v} not have expired';
        });
    }

    public static function hasValueFilter(expected:DrpcValue){
        return function (value: StorageValue) {
            return value != null
                && expected.equals(value.value);
        }
    }

    static function thatMatches<T>(type: Dynamic, matcher:(value:T) -> Bool, ?description:String, ?mismatchDescriptor: (T)->String):Matcher<T>
    {
        return new FunctionMatcher(type, matcher, description, mismatchDescriptor);
    }
}