package discovery.test.generators;

import discovery.dht.dhttypes.PeerAddress;
import discovery.domain.Validity;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import discovery.dht.impl.js.dhtrpc.externs.DhtQuery;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.test.helpers.RandomGen;
import discovery.test.helpers.generators.PrimitivesGenerator;
import discovery.test.helpers.generators.BinaryGen;
import discovery.dht.impl.js.dhtrpc.externs.PeerNode;
import js.node.Buffer;
import discovery.dht.impl.js.dhtrpc.externs.DhtQuery;


typedef StorageValueOptions = {
    ?validity: Validity
};

class DrpcRandomGenerator {
    var primitives:PrimitivesGenerator;
    var binary:BinaryGen;

    public function new(?binary: BinaryGen, ?primitives:PrimitivesGenerator) {
        if(primitives == null){
            primitives = new PrimitivesGenerator();
        }
        if(binary == null){
            binary = new BinaryGen(primitives);
        }

        this.primitives = primitives;
        this.binary = binary;
    }

    public function query(command: String, ?value:Dynamic): DhtQuery {
        return {
            command: command,
            value: if(value != null) value else dhtValue(),
            target: target(),
            node: peerNode()
        };
    }

    public function dhtValue(minSize:Int=1, maxSize:Int=20):DrpcValue {
        var size  = primitives.int(minSize, maxSize);
        var bytes = binary.bytes(size);

        return Buffer.hxFromBytes(bytes);
    }

    public function target():Buffer {
        var idBytes = binary.bytes(32);

        return Buffer.hxFromBytes(idBytes);
    }

    public function peerNode():PeerNode {
        return {
            id: target(),
            port: RandomGen.port(),
            host: RandomGen.address().toString()
        };
    }

    public function peerAddress():PeerAddress {
        return {
            address: RandomGen.address().toString(),
            port: RandomGen.port()
        };
    }

    public function storageValue(?options: StorageValueOptions): StorageValue{
        if(options == null) options = {};

        return {
            value: dhtValue(),
            validity: options.validity
        };
    }
}