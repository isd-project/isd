package discovery.test.generators;

import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtKey;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.DhtValue;

class DhtRandomGenerator {
    public function new() {

    }

    public function dhtValue():DhtValue {
        return RandomGen.binary.binary();
    }

    public function dhtKey():DhtKey {
        return RandomGen.binary.binary(32, 64);
    }

    public function dhtOptions():DhtOptions {
        return {
            bind: peerAddress(),
            bootstrap: [peerAddress()]
        };
    }

    public function bootstrapAddress():PeerAddress {
        return peerAddress();
    }

    public function peerAddress():Null<PeerAddress> {
        return {
            address: RandomGen.address().toString(),
            port: RandomGen.port()
        };
    }
}