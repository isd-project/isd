package discovery.test.integration;

import discovery.exceptions.IllegalStateException;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.Dht;
import discovery.async.DoneCallback;
import discovery.dht.DhtBuilder;

class DhtInfrastructureBuilder {
    var dhtBuilder:DhtBuilder;
    var bootstrapNode:Dht;

    public function new(builder: DhtBuilder) {
        this.dhtBuilder = builder;
    }

    public function stop(?onStop:StopCallback) {
        if(bootstrapNode != null){
            bootstrapNode.stop(onStop);
        }
    }

    public function startBootstrapNode(done: DoneCallback) {
        var bNode = dhtBuilder.build();
        bNode.start({
            bind: {
                address: '127.0.0.1'
            },
            onStart: (?err)->{
                bootstrapNode = bNode;

                done(err);
            }
        });
    }

    public function bootstrapInfo():Null<DhtOptions> {
        if(!hasBootstrapper()){
            throw new IllegalStateException(
                'Does not have (completed) bootstrap yet. You should start the bootstrapper node before.');
        }

        return {
            bootstrap: [bootstraperAddress()]
        }
    }

    public function hasBootstrapper() {
        return bootstrapNode != null;
    }

    function bootstraperAddress():PeerAddress {
        return bootstrapNode.boundAddress();
    }
}