import massive.munit.TestSuite;

import discovery.test.tests.publication.DhtPublicationTest;
import discovery.test.tests.dhttypes.DhtValueTest;
import discovery.test.tests.dhttypes.DhtKeyTest;
import discovery.test.tests.dhttypes.PeerAddressTest;
import discovery.test.tests.discoverymechanism.search.DhtDiscoverySearchTest;
import discovery.test.tests.discoverymechanism.publish.DhtDiscoveryPublisherTest;
import discovery.test.tests.discoverymechanism.publish.DefaultDhtKeyMakerTest;
import discovery.test.tests.discoverymechanism.DhtStarterGetDhtTest;
import discovery.test.tests.discoverymechanism.PublishServiceTest;
import discovery.test.tests.discoverymechanism.SearchForServiceTest;
import discovery.test.tests.discoverymechanism.DhtDiscoveryBuilderTest;
import discovery.test.tests.discoverymechanism.DhtStarterTest;
import discovery.test.tests.impl.js.DrpcDefaultStorageTest;
import discovery.test.tests.impl.js.DrpcValueTest;
import discovery.test.tests.impl.js.conversions.DhtParseToolsTest;
import discovery.test.tests.impl.js.conversions.DhtValueConversionTest;
import discovery.test.tests.impl.js.StorageValuesListTest;
import discovery.test.tests.impl.js.DrpcStorageCommandTest;
import discovery.test.tests.impl.js.DrpcNodeUnitTest;
import discovery.test.tests.impl.js.ReadableStopTest;
import discovery.test.tests.keytransport.DhtKeySearchTest;
import discovery.test.tests.keytransport.dhtkeytransport.DhtKeyTransportPublishTest;
import discovery.test.tests.keytransport.dhtkeytransport.DhtKeyTransportSearchTest;
import discovery.test.tests.keytransport.DhtKeyTransportBuilderTest;
import discovery.test.tests.keytransport.DhtKeyMessagesTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.tests.publication.DhtPublicationTest);
		add(discovery.test.tests.dhttypes.DhtValueTest);
		add(discovery.test.tests.dhttypes.DhtKeyTest);
		add(discovery.test.tests.dhttypes.PeerAddressTest);
		add(discovery.test.tests.discoverymechanism.search.DhtDiscoverySearchTest);
		add(discovery.test.tests.discoverymechanism.publish.DhtDiscoveryPublisherTest);
		add(discovery.test.tests.discoverymechanism.publish.DefaultDhtKeyMakerTest);
		add(discovery.test.tests.discoverymechanism.DhtStarterGetDhtTest);
		add(discovery.test.tests.discoverymechanism.PublishServiceTest);
		add(discovery.test.tests.discoverymechanism.SearchForServiceTest);
		add(discovery.test.tests.discoverymechanism.DhtDiscoveryBuilderTest);
		add(discovery.test.tests.discoverymechanism.DhtStarterTest);
		add(discovery.test.tests.impl.js.DrpcDefaultStorageTest);
		add(discovery.test.tests.impl.js.DrpcValueTest);
		add(discovery.test.tests.impl.js.conversions.DhtParseToolsTest);
		add(discovery.test.tests.impl.js.conversions.DhtValueConversionTest);
		add(discovery.test.tests.impl.js.StorageValuesListTest);
		add(discovery.test.tests.impl.js.DrpcStorageCommandTest);
		add(discovery.test.tests.impl.js.DrpcNodeUnitTest);
		add(discovery.test.tests.impl.js.ReadableStopTest);
		add(discovery.test.tests.keytransport.DhtKeySearchTest);
		add(discovery.test.tests.keytransport.dhtkeytransport.DhtKeyTransportPublishTest);
		add(discovery.test.tests.keytransport.dhtkeytransport.DhtKeyTransportSearchTest);
		add(discovery.test.tests.keytransport.DhtKeyTransportBuilderTest);
		add(discovery.test.tests.keytransport.DhtKeyMessagesTest);
	}
}
