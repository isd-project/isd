package discovery.test.tests.discoverymechanism;

import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtOptions;
import discovery.test.generators.DhtRandomGenerator;
import discovery.test.mockups.DhtMockup;
import haxe.Exception;
import discovery.dht.Dht;
import discovery.dht.dhttypes.startup.DefaultDhtStarter;
import discovery.test.mockups.DhtBuilderMockup;
import discovery.dht.DhtGetter;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtStarterGetDhtTest implements Scenario{
    @steps
    var steps: DhtStarterGetDhtSteps;

    @Test
    @scenario
    public function create_and_start_dht_on_get(){
        given().a_dht_starter();
        when().getting_the_dht();
        then().a_dht_should_be_created();
        and().the_dht_should_be_started();
    }

    @Test
    @scenario
    public function notify_dht_when_started(){
        given().a_get_dht_callback();
        given().started_to_get_the_dht();
        when().the_dht_starts();
        then().it_should_be_passed_to_the_listener();
        and().no_error_should_be_produced();
    }

    @Test
    @scenario
    public function notify_start_error(){
        given().a_get_dht_callback();
        given().started_to_get_the_dht();
        when().the_dht_start_fails();
        then().the_error_should_be_passed_to_the_callback();
    }

    @Test
    @scenario
    public function configure_start_parameters(){
        given().a_dht_starter();
        when().setting_some_dht_start_parameters();
        and().getting_the_dht();
        then().the_dht_should_be_started_using_the_given_parameters();
    }

    @Test
    @scenario
    public function reset_configuration_to_default(){
        given().a_dht_starter();
        when().configuring_it_with(null);
        and().getting_the_dht();
        then().the_dht_should_be_started_with({});
    }

    @Test
    @scenario
    public function default_configuration_can_be_set_on_constructor(){
        final defaultOptions:DhtOptions = {
            bootstrap: [PeerAddress.fromString('1.2.3.4:5678')],
            bind: PeerAddress.fromString('10.11.12.13')
        };

        given().a_dht_starter_created_with(defaultOptions);
        when().configuring_it_with(null);
        and().getting_the_dht();
        then().the_dht_should_be_started_with(defaultOptions);
    }
}

class DhtStarterGetDhtSteps extends Steps<DhtStarterGetDhtSteps>{
    var dhtStarter:DefaultDhtStarter;

    var dhtBuilderMockup:DhtBuilderMockup;
    var dhtGenerator:DhtRandomGenerator;

    var capturedDht:Dht;
    var getDhtError:Null<Exception>;
    var getDhtCallback:GetDhtCallback;
    var expectedError:Exception;
    var expectedStartOptions:DhtOptions;

    public function new(){
        super();

        dhtBuilderMockup = new DhtBuilderMockup();
        dhtGenerator = new DhtRandomGenerator();
    }

    function dhtMockup():DhtMockup {
        return dhtBuilderMockup.dht();
    }


    // given summary -------------------------------------------

    @step
    public function started_to_get_the_dht() {
        given().a_dht_starter();
        when().getting_the_dht();
    }

    // given ---------------------------------------------------

    @step
    public function a_dht_starter() {
        given().a_dht_starter_created_with(null);
    }

    @step
    public function a_dht_starter_created_with(defaultOptions:DhtOptions) {
        dhtStarter = new DefaultDhtStarter({
            dhtBuilder: dhtBuilderMockup.builder(),
            defaultOptions: defaultOptions
        });
    }

    @step
    public function a_get_dht_callback() {
        getDhtCallback = (dht:Dht, ?err:Exception)->{
            capturedDht = dht;
            getDhtError = err;
        };
    }

    // when  ---------------------------------------------------

    @step
    public function setting_some_dht_start_parameters() {
        when().configuring_it_with(dhtGenerator.dhtOptions());
    }

    @step
    public function configuring_it_with(params: DhtOptions) {
        expectedStartOptions = params;
        dhtStarter.setStartOptions(expectedStartOptions);
    }

    @step
    public function getting_the_dht() {
        dhtStarter.getDht(getDhtCallback);
    }

    @step
    public function the_dht_starts() {
        dhtMockup().notifyDhtStarted();
    }

    @step
    public function the_dht_start_fails() {
        expectedError = new Exception('Example error!');

        dhtMockup().notifyDhtStartedWith(expectedError);
    }

    // then  ---------------------------------------------------

    @step
    public function a_dht_should_be_created() {
        dhtBuilderMockup.verifyDhtBuilt();
    }

    @step
    public function the_dht_should_be_started() {
        dhtMockup().verifyDhtStarted();
    }

    @step
    public function it_should_be_passed_to_the_listener() {
        capturedDht.shouldNotBeNull('no dht value received');
        capturedDht.shouldBe(dhtMockup().dht());
    }

    @step
    public function no_error_should_be_produced() {
        getDhtError.shouldBe(null);
    }

    @step
    public function the_error_should_be_passed_to_the_callback() {
        getDhtError.shouldBe(expectedError);
    }

    @step
    public function the_dht_should_be_started_using_the_given_parameters() {
        then().the_dht_should_be_started_with(expectedStartOptions);
    }

    @step
    public function the_dht_should_be_started_with(expected:DhtOptions)
    {
        dhtMockup().verifyDhtStartedWith(expected);
    }
}