package discovery.test.tests.impl.js;

import haxe.io.Bytes;
import discovery.test.helpers.RandomGen;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import js.node.Buffer;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class DrpcValueTest implements Scenario{
    @steps
    var steps: DrpcValueSteps;

    @Test
    @scenario
    public function compare_bytes(){
        var hex = RandomGen.binary.hex(10);

        compare_should_be(buffer(hex), buffer(hex), 0);
        compare_should_be(buffer("01"), buffer("02"), -1);
        compare_should_be(buffer("deadbeef"), buffer("feed"), -1);
    }

    @Test
    @scenario
    public function compare_strings(){
        var name = RandomGen.primitives.name();

        compare_should_be(name, name, 0);
        compare_should_be("hello", "hello", 0);
        compare_should_be("a", "b", -1);
        compare_should_be("A", "a", -1);
        compare_should_be("lore", "lorem", -1);
    }
    @Test
    @scenario
    public function compare_objects(){
        compare_should_be({a:"a", b:1}, {a:"a", b:1}, 0);
        compare_should_be({a:"a", b:0}, {a:"a", b:1}, -1);
        compare_should_be({a:"a", b:1}, {a:"b", b:0}, -1);
    }

    @Test
    @scenario
    public function compare_arrays(){
        compare_arrays_should_be([1, 2, 3], [1,2,3], 0);
        compare_arrays_should_be([1, 2], [1,2,3], -1);
        compare_arrays_should_be([1, 2], [1,3], -1);

        compare_arrays_should_be(["a", "b"], ["a","b"], 0);
        compare_arrays_should_be(["a", "a"], ["a","b"], -1);
        compare_arrays_should_be([], ["a"], -1);

        compare_arrays_should_be([buffer("AA"), buffer("BB")],
                                 [buffer("AA"), buffer("BB")], 0);
        compare_arrays_should_be([buffer("AA")], [buffer("BB")], -1);
    }

    @Test
    @scenario
    public function compare_different_types(){
        //compare string binary
        compare_should_be("lorem", buffer("6c6f72656d"), 0);
        compare_should_be("ipsum", buffer("6c6f72656d"), -1);
        compare_should_be(buffer("0102"), "0102", -1);

        //string with object
        compare_should_be('{"a":"a","b":1}', {a:"a", b:1}, 1);
        compare_should_be('{"a":"a","b":1}', {a:"a"}, 1);

        //...
    }

    @Test
    @scenario
    public function is_buffer(){
        should_be_buffer(buffer("abcdef"), true);
        should_be_buffer("string", false);
        should_be_buffer(null, false);
    }


    @Test
    @scenario
    public function convert_to_buffer(){
        var buf = buffer("abcdef");

        DrpcValue.convertToBuffer(buf).shouldBeEqualsTo(buf);
        DrpcValue.convertToBuffer(null).shouldBeEqualsTo(null);
    }

    // ---------------------------------------------------------------------

    function buffer(hex:String): Buffer {
        var bytes = Bytes.ofHex(hex);
        return Buffer.hxFromBytes(bytes);
    }

    function compare_should_be(v1:DrpcValue, v2:DrpcValue, expected:Int) {
        var comp1 = v1.compare(v2);
        var comp2 = v2.compare(v1);

        var v1Str = v1.toString();
        var v2Str = v2.toString();

        assertThat(comp1, is(equalTo(expected)),
            'Compare between ${v1Str} and ${v2Str} should be ${expected}'
        );
        assertThat(comp2, is(equalTo(-expected)),
            'Compare between ${v2Str} and ${v1Str} should be ${-expected}'
        );
    }

    function compare_arrays_should_be<T1, T2>(
        a:Array<T1>, b:Array<T2>, expected:Int)
    {
        return compare_should_be(cast a, cast b, expected);
    }

    function should_be_buffer(value:DrpcValue, expected:Bool) {
        value.isBuffer().shouldBe(expected,
            'value: ${buffer} should be a Buffer instance');
    }
}

class DrpcValueSteps extends Steps<DrpcValueSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}