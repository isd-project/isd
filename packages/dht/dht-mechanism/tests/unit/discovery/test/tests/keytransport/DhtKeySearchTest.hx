package discovery.test.tests.keytransport;

import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.test.helpers.RandomGen;
import discovery.dht.keytransport.DhtKeyComponents;
import discovery.test.mockups.DhtComponentsMockup;
import discovery.dht.keytransport.search.DhtKeySearch;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DhtKeySearchTest implements Scenario{
    @steps
    var steps: DhtKeySearchSteps;

    @Test
    @scenario
    public function stop_before_start(){
        given().a_key_search_instance();
        when().it_is_stopped();
        and().it_is_started();
        then().no_query_should_be_sent_to_the_dht();
    }

    @Test
    @scenario
    public function call_search_end_when_stopping(){
        given().an_end_search_listener();
        given().a_key_search_instance();
        when().it_is_stopped();
        then().the_end_listener_should_be_called();
    }
}

class DhtKeySearchSteps extends Steps<DhtKeySearchSteps>{
    var keySearch:DhtKeySearch;

    var dhtComponents:DhtComponentsMockup;
    var endListenerMockup:ListenerMock<Null<Exception>>;
    var endListener:DoneCallback;

    public function new(){
        super();

        dhtComponents = new DhtComponentsMockup();
        endListenerMockup = new ListenerMock();
    }

    // given ---------------------------------------------------


    @step
    public function an_end_search_listener() {
        endListener = cast endListenerMockup.onEvent;
    }

    @step
    public function a_key_search_instance() {
        var pkiComponents = new DhtKeyComponents({
            dhtComponents: dhtComponents.components()
        });

        keySearch = new DhtKeySearch(pkiComponents, {
            onKey: (result)->{},
            onEnd: endListener
        });
    }

    // when  ---------------------------------------------------

    @step
    public function it_is_stopped() {
        keySearch.stop();
    }

    @step
    public function it_is_started() {
        keySearch.start(RandomGen.crypto.fingerprint());

        dhtComponents.dhtGetter().notifyDhtReceived();
        dhtComponents.dht().notifyDhtStarted();
    }

    // then  ---------------------------------------------------

    @step
    public function no_query_should_be_sent_to_the_dht() {
        dhtComponents.dht().verifyNoQuerySent();
    }

    @step
    public function the_end_listener_should_be_called() {
        endListenerMockup.assertWasCalled();
    }
}