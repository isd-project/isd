package discovery.test.tests.impl.js.conversions;

import discovery.domain.Struct;
import js.node.Buffer;
import haxe.io.Bytes;
import discovery.format.Binary;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.DhtValue;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;
using discovery.dht.impl.js.dhtrpc.DhtParseTools;


class DhtValueConversionTest implements Scenario{
    @steps
    var steps: DhtValueConversionSteps;

    @Test
    @scenario
    public function from_string(){
        var dhtValue:DhtValue = RandomGen.primitives.name();
        var drpcValue:DrpcValue = dhtValue.toDrpcValue();

        drpcValue.asString().shouldBeEqualsTo(dhtValue.asString());
    }

    @Test
    @scenario
    public function from_binary(){
        var dhtValue:DhtValue = RandomGen.binary.binary();
        var drpcValue:DrpcValue = dhtValue.toDrpcValue();

        drpcValue.asBuffer().shouldNotBeNull('DrpcValue from binary DhtValue');

        var hexDrpc = drpcValue.asBuffer().toString('hex');
        var hexDht  = dhtValue.toBinary().toHex();

        hexDrpc.shouldBeEqualsTo(hexDht);
    }


    @Test
    @scenario
    public function from_object_to_drpc_value(){
        var dhtValue:DhtValue = {
            int: 123,
            float: 1.2,
            text: "hello!",
            sub: {
                array: [1, 2, 3]
            }
        };

        var drpcValue:DrpcValue = dhtValue.toDrpcValue();

        drpcValue.asObject().shouldBeEqualsTo(dhtValue.asObject());
    }


    @Test
    @scenario
    public function from_object_with_bytes_to_drpc_value(){
        var obj = {
            binary: Binary.fromHex("abcd1234"),
            bytes: Bytes.ofHex("deadbeeffeed")
        };
        var dhtValue:DhtValue = obj;

        var drpcValue:DrpcValue = dhtValue.toDrpcValue();
        var parsedObj:Struct = drpcValue.asObject();

        parsedObj.shouldNotBeNull('DhtValue object converted to DrpcValue');
        parsedObj.shouldNotBeTheSame(obj);

        (parsedObj.bytes:Any).shouldBeEqualsTo(Buffer.hxFromBytes(obj.bytes));
        (parsedObj.binary:Any).shouldBeEqualsTo(Buffer.hxFromBytes(obj.binary));
    }


    @Test
    @scenario
    public function from_drpc_object_to_dht_value(){
        var obj = {
            int: 9876,
            float: -5.3,
            sub: {
                array: ["lorem", "ipsum", "dolor"]
            },
            text: "hello world!",

        };
        var dprcValue:DrpcValue = obj;

        var dhtValue:DhtValue = dprcValue.toDhtValue();

        dhtValue.asObject().shouldBeEqualsTo(obj);
    }


    @Test
    @scenario
    public function from_drpc_object_with_binary_to_dht_value(){
        var hex = RandomGen.binary.hex(8);
        var obj = {
            buffer: Buffer.from(hex, 'hex')
        };
        var dprcValue:DrpcValue = obj;


        var dhtValue:DhtValue = dprcValue.toDhtValue();


        var parsedObj:Struct = dhtValue.asObject();
        var parsedBuffer:Any = parsedObj.buffer;

        parsedBuffer.shouldNotBeNull();
        parsedBuffer.shouldBeAn(BinaryData);
        (parsedBuffer:Binary).toHex().shouldBeEqualsTo(hex);


        dhtValue.asObject().shouldNotBeTheSame(obj);
    }
}

class DhtValueConversionSteps extends Steps<DhtValueConversionSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}