package discovery.test.tests.discoverymechanism;

import discovery.domain.Identifier;
import discovery.test.tests.discoverymechanism.steps.SearchServiceBaseSteps;
import discovery.domain.Search;
import discovery.domain.Query;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class SearchForServiceTest implements Scenario{
    @steps
    var steps: SearchForServiceSteps;

    @Test
    @scenario
    public function send_search_query_on_dht(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().searching_for_some_service_type();
        then().a_query_should_be_sent_to_the_service_type_key();
    }


    @Test
    @scenario
    public function locate_service_from_identifier(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().locating_a_service_from_its_identity();
        then().a_query_should_be_sent_to_the_identifier_key();
    }

    @Test
    @scenario
    public function notify_response(){
        given().a_search_for_a_service_was_started();
        given().a_search_on_found_listener_was_registered();
        when().a_response_with_a_matching_service_is_received();
        then().the_received_service_should_be_notified_to_the_listener();
    }
}

class SearchForServiceSteps
    extends SearchServiceBaseSteps<SearchForServiceSteps>
{

    public function new(){
        super();
    }

    function doSearch(query:Query):Search {
        return mechanism.search(query);
    }

    function doLocateService(identifier:Identifier):Search {
        return mechanism.locate(identifier);
    }

    // given summary ---------------------------------------------------


    // given ---------------------------------------------------

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------

}