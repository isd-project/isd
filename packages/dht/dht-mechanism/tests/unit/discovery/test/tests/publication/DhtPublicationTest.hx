package discovery.test.tests.publication;

import discovery.utils.time.Duration;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.utils.TimerLoopMockup;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.dht.Dht;
import discovery.dht.dhttypes.publication.DhtPublication;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DhtValue;
import discovery.test.generators.DhtRandomGenerator;
import discovery.test.mockups.DhtMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.time.TimeUnitTools;


class DhtPublicationTest implements Scenario{
    @steps
    var steps: DhtPublicationSteps;

    @Test
    @scenario
    public function start_publication(){
        given().a_dht_instance();
        given().a_publication_for_some_key_and_value();
        when().starting_a_publication();
        then().that_value_should_be_put_on_dht();
    }

    @Test
    @scenario
    public function notify_on_publication_start(){
        given().a_publication_started();
        given().a_listener_for_on_published_event_was_registered();
        when().the_dht_put_succeeds();
        then().the_on_publish_event_should_be_notified();
    }


    @Test
    @scenario
    public function notify_on_publication_failure(){
        given().a_publication_started();
        given().a_listener_for_on_published_event_was_registered();
        when().the_dht_put_fails();
        then().the_on_publish_event_should_be_notified_with_the_failure();
    }

    @Ignore('PENDING')
    @Test
    @scenario
    public function retry_on_publication_failure(){

    }

    @Test
    @scenario
    public function listen_for_publication_default_expiration(){
        given().a_timer_loop_instance();
        given().a_publication_was_created_using_that_timer();
        when().the_publication_starts();
        then().the_publication_should_register_for_a_default_expire_time();
    }

    @Test
    @scenario
    public function listen_for_given_timeout(){
        given().a_timer_loop_instance();
        and().a_publication_expiration_time();
        given().a_publication_was_created_using_that_timer();
        when().the_publication_starts();
        then().the_publication_should_register_for_half_the_expire_time();
    }

    @Test
    @scenario
    public function set_put_expire_time(){
        given().a_publication_expiration_time();
        given().a_publication_was_created();
        when().the_publication_starts();
        then().the_expire_time_should_be_set_on_publication();
    }


    @Test
    @scenario
    public function republication_before_timeout(){
        given().a_timer_loop_instance();
        and().a_publication_expiration_time();
        given().the_publication_has_started();
        when().the_expire_time_timeout();
        then().the_publication_value_should_be_republished();
    }


    @Test
    @scenario
    public function republication_event_notification(){
        given().a_publication_succeeded();
        given().a_listener_for_on_published_event_was_registered();
        and().a_listener_to_republished_event_was_registered();
        when().the_publication_is_republished();
        then().the_on_publish_event_should_not_be_notified();
        but().the_republished_event_should_be_notified();
    }

    @Test
    @scenario
    public function stop_publication(){
        given().a_publication_started();
        when().stopping_the_publication();
        then().the_timer_repeat_should_be_cancelled();
    }
}

class DhtPublicationSteps extends Steps<DhtPublicationSteps>{
    var dhtPublication:DhtPublication;

    var dhtMockup:DhtMockup;
    var timerLoop:TimerLoopMockup;
    var generator:DhtRandomGenerator;

    var dht:Dht;
    var dhtKey:DhtKey;
    var dhtValue:DhtValue;

    var expireTime:Duration;

    var expectedException:Exception;

    var onPublishListener:ListenerMock<DhtPublishedEvent>;
    var onRepublishListener:ListenerMock<DhtPublishedEvent>;

    public function new(){
        super();

        dhtMockup = new DhtMockup();
        timerLoop = new TimerLoopMockup();
        generator = new DhtRandomGenerator();

        onPublishListener = new ListenerMock();
        onRepublishListener = new ListenerMock();
    }

    // summary -------------------------------------------------

    @step
    public function the_publication_has_started() {
        given().a_publication_started();
    }

    @step
    public function a_publication_started() {
        given().a_publication_was_created();
        when().starting_a_publication();
    }

    @step
    public function a_publication_was_created_using_that_timer() {
        given().a_publication_was_created();
    }


    @step
    public function a_publication_was_created() {
        given().a_dht_instance();
        given().a_publication_for_some_key_and_value();
    }

    @step
    public function a_publication_succeeded() {
        given().a_publication_started();
        when().the_dht_put_succeeds();
    }

    // given ---------------------------------------------------

    @step
    public function a_timer_loop_instance()
    {}

    @step
    public function a_publication_expiration_time() {
        expireTime = RandomGen.time.durationBetween(10.seconds(), 2.days());
    }

    @step
    public function a_dht_instance() {
        dht = dhtMockup.dht();
    }


    @step
    public function a_publication_for_some_key_and_value() {
        given().some_dht_value_and_key();
        given().a_publication_from_them();
    }

    @step
    function some_dht_value_and_key() {
        dhtKey = generator.dhtKey();
        dhtValue = generator.dhtValue();
    }

    @step
    function a_publication_from_them() {
        dhtPublication = new DhtPublication(dht, dhtKey, dhtValue, {
            timer: timerLoop.timer(),
            expireTime: expireTime
        });
    }

    @step
    public function a_listener_for_on_published_event_was_registered() {
        dhtPublication.onPublish.listen(onPublishListener.onEvent);
    }

    @step
    public function a_listener_to_republished_event_was_registered() {
        dhtPublication.onRepublish.listen(onRepublishListener.onEvent);
    }

    // when  ---------------------------------------------------

    @step
    public function the_publication_starts() {
        when().starting_a_publication();
    }

    @step
    public function starting_a_publication() {
        dhtPublication.start();
    }

    @step
    public function the_dht_put_succeeds() {
        dhtMockup.notifyLastPutResult();
    }

    @step
    public function the_dht_put_fails() {
        expectedException = new Exception("dht put failed");
        dhtMockup.notifyLastPutResult(expectedException);
    }

    @step
    public function the_expire_time_timeout() {
        timerLoop.callLastRepeatCallback();
    }

    @step
    public function the_publication_is_republished() {
        when().the_dht_put_succeeds();
    }

    @step
    public function stopping_the_publication() {
        dhtPublication.stop();
    }

    // then  ---------------------------------------------------

    @step
    public function that_value_should_be_put_on_dht() {
        dhtMockup.verifyPutValue(dhtKey, dhtValue);
    }

    @step
    public function the_publication_value_should_be_republished() {
        dhtMockup.verifyPutValue(dhtKey, dhtValue, times(2));
    }


    @step
    public function the_expire_time_should_be_set_on_publication() {
        var expectedExpireTime = Math.round(expireTime);
        var args = dhtMockup.lastPutArgs();

        assertThat(args.options, is(notNullValue()), "No options specified on last DHT put");
        assertThat(args.options.expireTimeMs, is(equalTo(expectedExpireTime)));
    }

    @step
    public function the_on_publish_event_should_be_notified() {
        onPublishListener.assertWasCalled();
    }

    @step
    public function the_on_publish_event_should_be_notified_with_the_failure() {
        onPublishListener.assertWasCalled();

        var capturedEvent = onPublishListener.captured;

        assertThat(capturedEvent.error, is(equalTo(expectedException)));
    }

    @step
    public function the_on_publish_event_should_not_be_notified() {
        onPublishListener.assertWasCalledTheseTimes(0);
    }

    @step
    public function the_republished_event_should_be_notified() {
        onRepublishListener.assertWasCalled();
    }

    @step
    public function the_publication_should_register_for_a_default_expire_time() {
        timerLoop.verifyRepeatCalled();
    }

    @step
    public function the_publication_should_register_for_half_the_expire_time() {
        var expectedRefreshTime = expireTime.multipliedBy(0.5).timeInt();

        timerLoop.verifyRepeatCalled();
        var call = timerLoop.lastCallOfRepeat();

        assertThat(call.intervalMs,
            both(lessThanOrEqualTo(expectedRefreshTime+1))
            .and(greaterThanOrEqualTo(expectedRefreshTime -1)));
    }


    @step
    public function the_timer_repeat_should_be_cancelled() {
        var call = timerLoop.lastCallOfRepeat();

        call.cancellableMock.verifyCancelled();
    }
}