package discovery.test.tests.keytransport.dhtkeytransport;

import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners.KeyFoundListener;
import discovery.keys.keydiscovery.SearchKeyListeners.SearchKeyResult;
import discovery.keys.storage.PublicKeyData;
import discovery.dht.dhttypes.DhtKey;
import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtKeyTransportSearchTest implements Scenario{
    @steps
    var steps: DhtKeyTransportSearchSteps;

    @Test
    @scenario
    public function start_key_search(){
        given().a_key_search_was_started();
        when().the_dht_starts();
        then().the_key_search_should_be_send_to_the_dht();
    }

    @Test
    @scenario
    public function notify_search_result(){
        given().a_key_search_listener();
        given().a_key_search_was_started();
        given().the_dht_has_started();
        when().a_key_result_is_received();
        then().the_search_listener_should_be_notified_with_the_result();
    }

    @Test
    @scenario
    public function notify_search_end(){
        given().a_key_search_end_listener();
        given().a_key_search_was_started();
        given().the_dht_has_started();
        when().the_search_ends_with_no_error();
        then().the_search_end_listener_should_be_called_with_no_error();
    }


    @Test
    @scenario
    public function notify_search_failure(){
        given().a_key_search_end_listener();
        given().a_key_search_was_started();
        given().the_dht_has_started();
        when().the_search_ends_with_and_error();
        then().the_search_end_listener_should_be_called_with_that_error();
    }

    @Test
    @scenario
    public function notify_failure_on_get_dht(){
        given().a_key_search_end_listener();
        given().a_key_search_was_started();
        when().the_dht_fails_to_start();
        then().the_search_end_listener_should_be_called_with_that_error();
    }

    @Test
    @scenario
    public function return_valid_stoppable_on_search(){
        given().a_dht_key_transport_was_built();
        when().searching_for_a_key_by_its_fingerprint();
        then().a_valid_stoppable_should_be_returned();
    }

    @Test
    @scenario
    public function stop_dht_query(){
        given().a_key_was_found_on_a_key_search();
        when().the_search_is_stopped();
        then().the_dht_query_should_be_stopped();
    }

    @Test
    @scenario
    public function receive_stop_command(){
        given().a_key_was_found_on_a_key_search();
        when().the_search_is_stopped();
        but().a_key_is_found_by_the_dht();
        then().the_search_listener_should_receive_only_the_first_result();
    }
}

class DhtKeyTransportSearchSteps
    extends DhtKeyTransportSteps<DhtKeyTransportSearchSteps>
{
    var fingerprint:Fingerprint;

    var searchListenerMockup:ListenerMock<SearchKeyResult>;
    var searchListener:KeyFoundListener;
    var endListenerMockup:ListenerMock<Exception>;
    var endListener:DoneCallback;
    var stoppable:Stoppable;

    var searchKey:DhtKey;
    var expectedResult:SearchKeyResult;
    var expectedPubKey:PublicKeyData;

    var receivedResults:Array<SearchKeyResult> = [];

    public function new(){
        super();

        searchListenerMockup = new ListenerMock();
        endListenerMockup = new ListenerMock();
    }


    function lastSearchResult() {
        return searchListenerMockup.lastCall();
    }

    // given summary -------------------------------------------

    @step
    public function a_key_was_found_on_a_key_search() {
        given().a_key_search_listener();
        given().a_key_search_was_started();
        given().the_dht_has_started();
        when().a_key_result_is_received();
    }

    @step
    public function a_key_search_was_started() {
        given().a_dht_key_transport_was_built();
        when().searching_for_a_key_by_its_fingerprint();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_search_listener() {
        searchListener = searchListenerMockup.onEvent;
    }

    @step
    public function a_key_search_end_listener() {
        endListener = cast endListenerMockup.onEvent;
    }

    // when  ---------------------------------------------------

    @step
    public function searching_for_a_key_by_its_fingerprint() {
        fingerprint = RandomGen.crypto.fingerprint();

        stoppable = keyTransport.searchKey(fingerprint, {
            onKey: (result)->{
                receivedResults.push(result);

                if(searchListener != null){
                    searchListener(result);
                }
            },
            onEnd: endListener
        });


        searchKey = keyMessages.fingerprintKey(fingerprint);
    }


    @step
    public function a_key_is_found_by_the_dht() {
        when().a_key_result_is_received();
    }

    @step
    public function a_key_result_is_received() {
        expectedPubKey = RandomGen.crypto.publicKeyData();
        var serializedKey = publicKeySerializer.serialize(expectedPubKey);

        dhtMockup().notifyGetResultFor(searchKey, [ serializedKey ]);
    }

    @step
    public function the_search_ends_with_and_error() {
        when().the_search_ends_with(new Exception('Test search failure'));
    }

    @step
    public function the_search_ends_with_no_error() {
        when().the_search_ends_with();
    }

    @step
    function the_search_ends_with(?err:Exception) {
        this.expectedException = err;

        dhtMockup().notifyQueryFinished(searchKey, err);
    }

    @step
    public function the_search_is_stopped() {
        stoppable.stop();
    }


    // then  ---------------------------------------------------

    @step
    public function the_key_search_should_be_send_to_the_dht() {
        dhtMockup().verifyQueryFor(searchKey);
    }

    @step
    public function the_search_listener_should_be_notified_with_the_result() {
        var result = lastSearchResult();

        result.found.shouldBeEqualsTo(expectedPubKey);
    }

    @step
    public function the_search_listener_should_receive_only_the_first_result()
    {
        searchListenerMockup.assertWasCalledTheseTimes(1);
    }

    @step
    public function the_search_end_listener_should_be_called_with_no_error() {
        then().the_search_end_listener_should_be_called_with();
    }

    @step
    public function the_search_end_listener_should_be_called_with_that_error() {
        then().the_search_end_listener_should_be_called_with(expectedException);
    }

    @step
    function the_search_end_listener_should_be_called_with(?err:Exception) {
        endListenerMockup.assertWasCalledWith(err);
    }

    @step
    public function the_dht_query_should_be_stopped() {
        dhtMockup().theLastGetQuery().shouldHaveBeenStopped();
    }

    @step
    public function a_valid_stoppable_should_be_returned() {
        stoppable.shouldNotBeNull();
    }
}