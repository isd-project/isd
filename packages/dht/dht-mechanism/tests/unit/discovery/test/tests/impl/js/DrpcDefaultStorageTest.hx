package discovery.test.tests.impl.js;

import discovery.utils.time.Duration;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import discovery.domain.Validity;
import org.hamcrest.CustomTypeSafeMatcher;
import org.hamcrest.Matcher;
import discovery.format.Binary;
import discovery.test.generators.DrpcRandomGenerator;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage;
import discovery.dht.impl.js.dhtrpc.storage.DefaultDrpcStorage;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
import discovery.test.matchers.js.StorageValueMatchers.*;

using discovery.utils.time.TimeUnitTools;



class DrpcDefaultStorageTest implements Scenario{
    @steps
    var steps: DrpcDefaultStorageSteps;

    @Test
    @scenario
    public function store_and_retrieve(){
        given().a_storage_instance();
        when().storing_some_value_and_key();
        and().listing_the_key_values();
        then().the_stored_value_should_be_found();
    }


    @Test
    @scenario
    public function store_values_and_retrieve(){
        given().a_storage_instance();
        when().storing_some_values_at_some_key();
        and().listing_the_key_values();
        then().the_stored_values_should_be_found();
    }

    @Test
    @scenario
    public function filter_expired_values() {
        given().a_storage_instance();
        given().some_values_are_stored_at_some_key();
        and().there_are_some_expired_values();
        when().listing_the_key_values();
        then().no_expired_value_should_be_returned();
    }


    @Test
    @scenario
    public function configure_expiration_time() {
        given().a_storage_instance();
        when().configuring_the_storage_using({
            expirationTime: {
                defaultValue: 50.millis(),
                maxValue: 100.millis()
            }
        });
        then().default_expiration_time_should_be(50.millis());
        and().max_expiration_time_should_be(100.millis());
    }

    @Test
    @scenario
    public function configuration_should_change_expiration_time() {
        given().a_storage_instance();
        when().configuring_the_storage_using({
            expirationTime: {
                defaultValue: 50.millis(),
                maxValue: 100.millis()
            }
        });
        and().storing_some_value_and_key();
        then().the_stored_value_should_have_expiration_of(50.millis());
    }


    @Test
    @scenario
    public function configuration_should_apply_to_created_values_lists() {
        given().a_storage_instance();
        given().some_values_are_stored_at_some_key();
        when().configuring_the_storage_using({
            expirationTime: {
                defaultValue: 50.minutes(),
                maxValue: 100.minutes()
            }
        });
        and().storing_some_value_at_the_given();
        then().the_last_stored_value_should_have_expiration_of(50.minutes());
    }
}

class DrpcDefaultStorageSteps extends Steps<DrpcDefaultStorageSteps>{
    var storage:DrpcStorage;

    var generator:DrpcRandomGenerator;

    var key:Binary;
    var expectedValues:Array<StorageValue>;
    var addedValues:Array<StorageValue>;

    var foundValues:Array<StorageValue>;

    public function new(){
        super();

        generator = new DrpcRandomGenerator();

        expectedValues = [];
        addedValues = [];
    }

    // given ---------------------------------------------------

    @step
    public function a_storage_instance() {
        storage = new DefaultDrpcStorage();
    }

    @step
    public function some_values_are_stored_at_some_key() {
        when().storing_some_values_at_some_key(3);
    }


    @step
    public function there_are_some_expired_values() {
        var nowTime = Date.now().getTime();
        var expiration = Date.fromTime(nowTime - 1000);
        var start      = Date.fromTime(expiration.getTime() - 1000);
        var expiredValidity = new Validity(expiration, start);

        when().storing_some_values_at_this_key(key, 2, {
            validity: expiredValidity
        });
    }

    // when  ---------------------------------------------------

    @step
    public function storing_some_value_and_key() {
        when().storing_some_values_at_some_key(1);
    }

    @step
    public function storing_some_value_at_the_given() {
        when().storing_some_values_at_this_key(key, 1);
    }

    @step
    public function storing_some_values_at_some_key(count:Int = 5, ?options:StorageValueOptions)
    {
        key = generator.target();

        when().storing_some_values_at_this_key(key, count, options);
    }

    @step
    public function storing_some_values_at_this_key(key:Binary, count:Int = 5, ?options:StorageValueOptions)
    {
        for(i in 0...count){
            var value = generator.storageValue(options);
            expectedValues.push(value);

            var stored = storage.store(key, value);
            addedValues.push(stored);
        }
    }

    @step
    public function listing_the_key_values() {
        this.foundValues = storage.listValuesFor(key);
    }

    @step
    public function configuring_the_storage_using(options:StorageConfig) {
        storage.configure(options);
    }

    // then  ---------------------------------------------------

    @step
    public
    function the_stored_value_should_have_expiration_of(expiration:Duration)
    {
        when().listing_the_key_values();
        then().the_stored_values_should_have_expiration_of(expiration);
    }

    @step
    public function
        the_last_stored_value_should_have_expiration_of(expiration:Duration)
    {
        then().this_value_should_have_expiration_of(lastAddedValue(), expiration);
    }

    @step
    function the_stored_values_should_have_expiration_of(expiration:Duration)
    {
        for(value in foundValues){
            then().this_value_should_have_expiration_of(value, expiration);
        }
    }

    @step
    function this_value_should_have_expiration_of(
        value:StorageValue, expiration:Duration)
    {
        assertThat(value.expirationTime(), is(equalTo(expiration)),
            'Expiration of storage value: ${value} should be ${expiration}, but was ${value.expirationTime()}'
        );
    }

    @step
    public function the_stored_value_should_be_found() {
        then().the_stored_values_should_be_found();
    }

    @step
    public function the_stored_values_should_be_found() {
        assertThat(foundValues, not(isEmpty()));

        var matchers = expectedValues.map((v)->{
            return hasSameValueOf(v);
        });
        assertThat(foundValues, containsInAnyOrder(matchers),
            "Stored values not found");
    }


    @step
    public function no_expired_value_should_be_returned() {
        assertThat(foundValues, not(hasItem(isExpired())));
    }

    function isExpired():Matcher<StorageValue> {
        return new ExpiredStorageValueMatcher();
    }

    @step
    public function default_expiration_time_should_be(expected:Duration) {
        var expirationTime = requireExpirationTimeConfig();
        assertThat(expirationTime.defaultValue, is(equalTo(expected)));
    }

    @step
    public function max_expiration_time_should_be(expected:Duration) {
        var expirationTime = requireExpirationTimeConfig();
        assertThat(expirationTime.maxValue, is(equalTo(expected)));
    }

    //---------------------------------------------------------------------

    function requireExpirationTimeConfig() {
        var config = storage.getConfig();

        assertThat(config.expirationTime, is(notNullValue()),
            "No expiration time info");

        return config.expirationTime;
    }

    function hasSameValueOf(v:StorageValue): Matcher<StorageValue> {
        return withSameValue(v);
    }

    function lastAddedValue():StorageValue {
        return addedValues[addedValues.length - 1];
    }
}

class ExpiredStorageValueMatcher extends CustomTypeSafeMatcher<StorageValue> {
    public function new() {
        super("an expired storage value");
    }
    override function matchesSafely(item:StorageValue):Bool{
        return item.expired();
    }

    override function describeMismatchSafely(item:StorageValue, mismatchDescription:org.hamcrest.Description):Void
    {
        mismatchDescription.appendText("an " + item);
    }

    override function isExpectedType(type:Dynamic):Bool
    {
        return Std.isOfType(type, Dynamic);
    }
}