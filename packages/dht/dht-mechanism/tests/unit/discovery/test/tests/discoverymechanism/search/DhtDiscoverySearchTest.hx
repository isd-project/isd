package discovery.test.tests.discoverymechanism.search;

import discovery.dht.DhtGetter;
import discovery.test.mockups.DhtGetterMockup;
import discovery.dht.dhttypes.search.IdentifierKeyBuilder;
import discovery.dht.dhttypes.search.SearchKeyBuilder;
import discovery.domain.Identifier;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.search.QueryKeyBuilder;
import haxe.Exception;
import discovery.dht.dhttypes.search.DhtDiscoverySearch;
import discovery.test.common.CallableMockup;
import discovery.test.common.ListenerMock;
import discovery.dht.discoverymechanism.DhtStarter;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.test.tests.discoverymechanism.steps.SearchServiceBaseSteps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtDiscoverySearchTest implements Scenario{
    @steps
    var steps: DhtDiscoverySearchSteps;

    @Test
    @scenario
    public function send_query_for_service_type_on_dht(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().searching_for_some_service_type();
        then().a_query_should_be_sent_to_the_service_type_key();
    }

    @Test
    @scenario
    public function locate_service_from_identifier(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().locating_a_service_from_its_identity();
        then().a_query_should_be_sent_to_the_identifier_key();
    }

    @Test
    @scenario
    public function stop_search(){
        given().a_search_for_a_service_was_started();
        given().a_finish_listener_was_registered();
        when().the_search_is_stopped();
        then().the_stop_callback_should_be_called();
        and().the_finish_listener_should_be_called();
    }

    @Test
    @scenario
    public function finish_search_on_failure(){
        given().a_search_for_a_service_was_started();
        given().a_finish_listener_was_registered();
        when().the_search_fails();
        then().the_finish_listener_should_be_called_with_the_failure_cause();
    }
}

class DhtDiscoverySearchSteps
    extends SearchServiceBaseSteps<DhtDiscoverySearchSteps>
{
    var dhtDiscoverySearch:DhtDiscoverySearch;

    var dhtStarter:DhtStarter;

    var onStopCallableMockup:CallableMockup;
    var onFinishListenerMock:ListenerMock<FinishSearchEvent>;

    var expectedException:Exception;


    public function new(){
        super();

        dhtStarter = dhtBuilderMockup.dhtStarter();

        onStopCallableMockup = new CallableMockup();
        onFinishListenerMock = new ListenerMock();
    }

    function doSearch(query:Query):Search {
        dhtDiscoverySearch = makeSearch(new QueryKeyBuilder(query, keyMaker));

        return dhtDiscoverySearch.start();
    }

    function doLocateService(identifier:Identifier):Search {
        dhtDiscoverySearch = makeSearch(new IdentifierKeyBuilder(identifier, keyMaker));

        return dhtDiscoverySearch.start();
    }

    // given ---------------------------------------------------

    @step
    public function a_finish_listener_was_registered() {
        search.onceFinished.listen(onFinishListenerMock.onEvent);
    }

    // when  ---------------------------------------------------


    @step
    public function the_search_is_stopped() {
        search.stop(onStopCallableMockup.callable());
    }

    @step
    public function the_search_fails() {
        expectedException = new Exception('query failed');
        dhtMockup().notifyQueryFinished(expectedSearchKey, expectedException);
    }

    // then  ---------------------------------------------------

    @step
    public function the_stop_callback_should_be_called() {
        onStopCallableMockup.verifyCallbackCalled();
    }

    @step
    public function the_finish_listener_should_be_called() {
        onFinishListenerMock.assertWasCalled();
    }

    @step
    public function the_finish_listener_should_be_called_with_the_failure_cause() {
        onFinishListenerMock.assertWasCalled();

        var result = onFinishListenerMock.captured;

        result.shouldNotBeNull('captured finishEvent should not be null');
        result.error.shouldBe(expectedException);
    }


    // -------------------------------------------------

    function makeSearch(keyBuilder: SearchKeyBuilder):DhtDiscoverySearch {
        return new DhtDiscoverySearch({
            dhtGetter: dhtGetter(),
            keyBuilder: keyBuilder
        });
    }

    function dhtGetter():DhtGetter {
        return dhtBuilderMockup.dhtGetter();
    }
}