package discovery.test.tests.discoverymechanism.steps;


import discovery.dht.discoverymechanism.DhtSerializerBuilder;
import discovery.domain.serialization.AnnouncementSerializer;
import discovery.domain.serialization.SignedSerializer;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.serialization.Serializer;
import mockatoo.Mockatoo.VerificationMode;
import haxe.exceptions.NotImplementedException;
import discovery.test.common.CallableMockup;
import discovery.base.publication.PublicationControl;
import haxe.Exception;
import discovery.test.common.mockups.discovery.PublishListenersMockup;
import discovery.dht.discoverymechanism.DefaultDhtKeyMaker;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.domain.PublishInfo;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.DhtKey;

class PublishServiceBaseSteps<Self:PublishServiceBaseSteps<Self>>
    extends DhtMechanismSteps<Self>
{
    var publishListenersMockup:PublishListenersMockup;
    var stopCallback:CallableMockup;

    var publishInfo:PublishInfo;
    var expectedException:Exception;
    var publicationControl:PublicationControl;

    public function new(){
        super();

        publishListenersMockup = new PublishListenersMockup();
        stopCallback = new CallableMockup();
    }

    function announcement() {
        return publishInfo.announcement();
    }

    function identifierKey():DhtKey {
        var identifier = announcement().description.identifier();
        return keyMaker.keyFromIdentifier(identifier);
    }

    function serviceTypeKey() {
        var type = announcement().description.type;
        return keyMaker.serviceTypeKey(type);
    }

    // given summary ---------------------------------------------------


    @step
    public function a_service_was_published_with_success() {
        given().a_service_publication_started();
        when().the_service_announcement_is_put_on_dht();
    }

    @step
    public function a_service_publication_started() {
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().publishing_a_service_announcement();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function publishing_a_service_announcement(){
        throw new NotImplementedException("This method needs to be overrided in subclasses");
    }

    @step
    public function the_service_announcement_is_put_on_dht() {
        var keys = Random.shuffle([identifierKey(), serviceTypeKey()]);

        for(k in keys){
            dhtMockup().notifyPutResultFor(k, null);
        }
    }

    @step
    public function the_service_announcement_fails_to_be_put_on_dht() {
        var keys = Random.shuffle([identifierKey(), serviceTypeKey()]);

        expectedException = new Exception("any failure");

        // currently just one failed 'put' causes the publication to fail (maybe should retry?)
        dhtMockup().notifyPutResultFor(keys[0], null);
        dhtMockup().notifyPutResultFor(keys[1], expectedException);
    }

    @step
    public function stopping_the_publication() {
        publicationControl.stop(stopCallback.callable());
    }

    // then  ---------------------------------------------------

    @step
    public function the_serialized_announce_should_be_put_at_the_identifier_key() {
        then().announcement_should_be_put_at(identifierKey());
    }

    @step
    public function the_serialized_announce_should_be_put_at_the_service_type_key() {
        then().announcement_should_be_put_at(serviceTypeKey());
    }

    function announcement_should_be_put_at(expectedKey: DhtKey) {
        var announcement = publishInfo.signed();
        var serializedAnnounce = announceSerializer.serialize(announcement);

        dhtMockup().verifyPutValue(expectedKey, serializedAnnounce);
    }

    @step
    public function the_on_publish_event_should_be_called() {
        publishListenersMockup.verifyOnPublishCalled(times(1));
    }

    @step
    public function on_publish_event_should_not_be_called() {
        publishListenersMockup.verifyOnPublishCalled(never);
    }

    @step
    public function the_on_finish_event_should_be_called_with_the_exception() {
        publishListenersMockup.verifyOnFinishCalledWith(expectedException);
    }

    @step
    public function
        the_on_finish_event_should_be_called_once_with_no_exception()
    {
        then().the_on_finish_event_should_be_called_with_no_exception(times(1));
    }

    @step
    public function the_on_finish_event_should_be_called_with_no_exception(?mode:VerificationMode)
    {
        publishListenersMockup.verifyOnFinishCalledWith(null, mode);
    }


    @step
    public function on_finish_event_should_not_be_called_again() {
        publishListenersMockup.verifyOnFinishCalled(times(1));
    }

    @step
    public function the_stop_callback_should_be_called() {
        stopCallback.verifyCallbackCalled();
    }
}