package discovery.test.tests.discoverymechanism.publish;

import discovery.domain.PublishInfo;
import haxe.exceptions.NotImplementedException;
import discovery.test.common.CallableMockup;
import discovery.test.helpers.RandomGen;
import discovery.dht.discoverymechanism.DhtDiscoveryPublisher;
import discovery.test.tests.discoverymechanism.steps.PublishServiceBaseSteps;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DhtDiscoveryPublisherTest implements Scenario{
    @steps
    var steps: DhtDiscoveryPublisherSteps;

    @Test
    @scenario
    public function publish_service_announcement_on_dht(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().publishing_a_service_announcement();
        then().the_serialized_announce_should_be_put_at_the_identifier_key();
        then().the_serialized_announce_should_be_put_at_the_service_type_key();
    }


    @Test
    @scenario
    public function publish_service_with_incomplete_identifier(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().publishing_a_service_announcement_with_an_incomplete_identifier();
        then().the_serialized_announce_should_be_put_at_the_service_type_key();
    }

    @Test
    @scenario
    public function stop_publication(){
        given().a_service_was_published_with_success();
        when().stopping_the_publication();
        then().the_on_finish_event_should_be_called_with_no_exception();
        and().the_stop_callback_should_be_called();
    }

    @Test
    @scenario
    public function stop_publication_before_putting_into_dh(){
        given().a_service_publication_started();
        when().the_publication_is_stopped();
        and().the_service_announcement_is_put_on_dht();
        then().the_on_finish_event_should_be_called_with_no_exception();
        but().on_publish_event_should_not_be_called();
    }


    @Test
    @scenario
    public function stopping_publication_when_already_stopped(){
        given().a_service_was_published_with_success();
        and().the_publication_was_stopped();
        when().stopping_the_publication_again();
        then().the_stop_callback_should_be_called();
        but().on_finish_event_should_not_be_called_again();
    }


    @Test
    @scenario
    public function stopping_publication_after_failure(){
        given().a_service_publication_failed();
        when().stopping_the_publication();
        then().the_stop_callback_should_be_called();
        but().on_finish_event_should_not_be_called_again();
    }

    @Test
    @scenario
    public function failure_after_publication_stopped(){
        given().a_service_publication_started();
        given().the_publication_is_stopped();
        when().the_service_announcement_fails_to_be_put_on_dht();
        then().the_on_finish_event_should_be_called_once_with_no_exception();
        and().on_publish_event_should_not_be_called();
    }
}

class DhtDiscoveryPublisherSteps
    extends PublishServiceBaseSteps<DhtDiscoveryPublisherSteps>
{
    var publication:DhtDiscoveryPublisher;

    public function new(){
        super();
    }


    // given summary----------------------------------------------

    @step
    public function a_service_publication_failed() {
        given().a_service_publication_started();
        when().the_service_announcement_fails_to_be_put_on_dht();
    }


    // given ---------------------------------------------------

    @step
    public function the_publication_was_stopped() {
        when().the_publication_is_stopped();
    }

    // when  ---------------------------------------------------

    @step
    override public function publishing_a_service_announcement() {
        publishInfo = RandomGen.publishInfo();

        publish(publishInfo);
    }


    @step
    public function publishing_a_service_announcement_with_an_incomplete_identifier() {
        publishInfo = RandomGen.publishInfo();
        publishInfo.description().providerId = null;
        publishInfo.description().instanceId = null;

        publish(publishInfo);
    }


    @step
    public function the_publication_is_stopped() {
        when().stopping_the_publication();
    }

    @step
    public function stopping_the_publication_again() {
        //reset stop callback
        stopCallback = new CallableMockup();

        when().stopping_the_publication();
    }

    // then  ---------------------------------------------------


    // ---------------------------------------------------

    function publish(publishInfo:PublishInfo) {
        publication = makePublication(publishInfo);
        publication.start(dhtMockup().dht());
        publicationControl = publication;
    }

    function makePublication(publishInfo: PublishInfo):DhtDiscoveryPublisher {
        return new DhtDiscoveryPublisher({
            publishInfo: publishInfo,
            listener: publishListenersMockup.listeners(),
            deps: {
                keyMaker: keyMaker,
                promises: promisesFactory
            }
        });
    }
}