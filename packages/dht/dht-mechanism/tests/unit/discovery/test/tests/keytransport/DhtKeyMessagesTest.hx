package discovery.test.tests.keytransport;

import discovery.dht.dhttypes.DhtKey;
import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.test.mockups.HashMakerMockup;
import discovery.dht.keytransport.DhtKeyMessages;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtKeyMessagesTest implements Scenario{
    @steps
    var steps: DhtKeyMessagesSteps;

    @Test
    @scenario
    public function make_fingerprint_key(){
        given().a_key_messages_from_a_hash_maker();
        given().a_fingerprint();
        when().making_a_key_from_this_fingerprint();
        then().it_should_be_the_hash_of_the_fingerprint_hash();
    }
}

class DhtKeyMessagesSteps extends Steps<DhtKeyMessagesSteps>{

    var keyMessages:DhtKeyMessages;

    var hashMakerMockup:HashMakerMockup;

    var fingerprint:Fingerprint;
    var fingerprintKey:DhtKey;

    public function new(){
        super();

        hashMakerMockup = new HashMakerMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_messages_from_a_hash_maker() {
        keyMessages = new DhtKeyMessages(hashMakerMockup.hashMaker());
    }

    @step
    public function a_fingerprint() {
        fingerprint = RandomGen.crypto.fingerprint();
    }

    // when  ---------------------------------------------------

    @step
    public function making_a_key_from_this_fingerprint() {
        fingerprintKey = keyMessages.fingerprintKey(fingerprint);
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_be_the_hash_of_the_fingerprint_hash() {
        hashMakerMockup.checkHashMakeFromBinary(fingerprint.hash);

        var expected:DhtKey = hashMakerMockup.binaryHashOf(fingerprint.hash);

        fingerprintKey.shouldBeEqualsTo(expected);
    }
}