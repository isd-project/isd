package discovery.test.tests.impl.js.conversions;

import discovery.format.Binary;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.test.helpers.RandomGen;
import js.node.Buffer;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.dht.impl.js.dhtrpc.DhtParseTools;


class DhtParseToolsTest implements Scenario{
    @steps
    var steps: DhtParseToolsSteps;

    @Test
    @scenario
    public function from_buffer(){
        given().a_binary_drpc_value();
        when().converting_do_dht_values();
        then().it_should_return_the_expected_binary_values();
    }
}

class DhtParseToolsSteps extends Steps<DhtParseToolsSteps>{
    var drpcValue:DrpcValue;
    var dhtValues:Array<DhtValue>;
    var expectedBinaries:Array<Binary>;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_binary_drpc_value() {
        var binary = RandomGen.binary.binary(5, 8);

        expectedBinaries = [binary];
        drpcValue = Buffer.hxFromBytes(binary);
    }

    // when  ---------------------------------------------------

    @step
    public function converting_do_dht_values() {
        dhtValues = drpcValue.toDhtValues();

    }

    // then  ---------------------------------------------------


    @step
    public function it_should_return_the_expected_binary_values() {
        var binaryValues = dhtValues.map((v)->v.toBinary());

        assertThat(binaryValues, is(equalTo(expectedBinaries)));
    }
}