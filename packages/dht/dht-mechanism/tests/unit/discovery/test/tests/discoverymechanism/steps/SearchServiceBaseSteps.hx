package discovery.test.tests.discoverymechanism.steps;

import discovery.domain.Identifier;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DhtValue;
import discovery.domain.Discovered;
import discovery.domain.Search;
import discovery.domain.Query;
import discovery.test.helpers.RandomGen;
import discovery.test.tests.discoverymechanism.steps.DhtMechanismSteps;

using discovery.test.matchers.CommonMatchers;


abstract class SearchServiceBaseSteps<Self:SearchServiceBaseSteps<Self>>
    extends DhtMechanismSteps<Self>
{
    var query:Query;
    var expectedSearchKey:DhtKey;
    var search:Search;

    var foundService:Discovered;
    var expectedDiscovered:Discovered;


    public function new(){
        super();
    }

    abstract function doSearch(query: Query): Search;
    abstract function doLocateService(identifier:Identifier):Search;

    // given summary ---------------------------------------------------

    @step
    public function a_search_for_a_service_was_started() {
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().searching_for_some_service_type();
    }

    // given ---------------------------------------------------

    @step
    public function a_search_on_found_listener_was_registered() {
        search.shouldNotBeNull('search was null');

        search.onFound.listen((discovered)->{
            foundService = discovered;
        });
    }

    // when  ---------------------------------------------------

    @step
    public function searching_for_some_service_type() {
        query = RandomGen.typeQuery();
        expectedSearchKey = keyMaker.serviceTypeKey(query.type);

        search = doSearch(query);
    }

    @step
    public function locating_a_service_from_its_identity() {
        var identifier = RandomGen.identifier();
        expectedSearchKey = keyMaker.keyFromIdentifier(identifier);

        search = doLocateService(identifier);
    }


    @step
    public function a_response_with_a_matching_service_is_received() {
        expectedDiscovered = RandomGen.discovered();

        var received = serializedAnnounce(expectedDiscovered);

        dhtMockup().notifyGetResultFor(expectedSearchKey, [received]);
    }

    // then  ---------------------------------------------------

    @step
    public function a_query_should_be_sent_to_the_service_type_key() {
        then().a_query_should_be_sent_to_the_expected_key();
    }

    @step
    public function a_query_should_be_sent_to_the_identifier_key() {
        then().a_query_should_be_sent_to_the_expected_key();
    }

    @step
    function a_query_should_be_sent_to_the_expected_key() {
        dhtMockup().verifyQueryFor(expectedSearchKey);
    }

    @step
    public function the_received_service_should_be_notified_to_the_listener() {
        foundService.shouldNotBeNull('no service found');
        foundService.shouldBeEqualsTo(expectedDiscovered);
    }

    // ------------------------------------------------------------

    function serializedAnnounce(discovered:Discovered):DhtValue {
        var announce = discovered.signedAnnouncement();
        var obj = announceSerializer.serialize(announce);

        return DhtValue.fromObject(obj);
    }
}