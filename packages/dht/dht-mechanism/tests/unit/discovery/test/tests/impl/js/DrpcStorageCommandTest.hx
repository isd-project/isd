package discovery.test.tests.impl.js;

import discovery.utils.time.Duration;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import js.node.Buffer;
import discovery.format.Binary;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.dht.impl.js.dhtrpc.DrpcConstants;
import js.lib.Error;
import discovery.dht.impl.js.dhtrpc.externs.DhtQuery;
import discovery.test.generators.DrpcRandomGenerator;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorageCommand;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage;
import discovery.dht.impl.js.dhtrpc.storage.DefaultDrpcStorage;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.EqualsComparator;


class DrpcStorageCommandTest implements Scenario{
    @steps
    var steps: DrpcStorageCommandSteps;

    @Test
    @scenario
    public function store_value(){
        given().an_storage_instance();
        and().a_storage_command();
        when().a_valid_update_request_is_received();
        then().the_received_value_should_be_stored();
        and().the_callback_should_be_called_with_no_value();
    }


    @Test
    @scenario
    public function store_empty_value(){
        given().an_storage_instance();
        and().a_storage_command();
        when().a_update_request_is_received_with_no_value();
        then().no_value_should_be_stored();
        and().the_callback_should_be_called_with_no_value();
    }


    @Test
    @scenario
    public function query_values(){
        given().an_storage_instance();
        and().a_storage_command();
        given().there_are_some_values_in_storage_for_some_key();
        when().a_query_request_is_received_for_that_key();
        then().the_callback_should_be_called_with_the_stored_values();
    }

    @Test
    @scenario
    public function configure_storage(){
        given().an_storage_instance();
        and().a_storage_command();
        when().configuring_the_storage_command_with_some_configuration();
        then().the_configuration_should_be_passed_to_the_storage_instance();
    }
}

class DrpcStorageCommandSteps extends Steps<DrpcStorageCommandSteps>{
    var storage:DrpcStorage;
    var storageCommand:DrpcStorageCommand;

    var dhtGenerator:DrpcRandomGenerator;

    var dhtResponseCalled:Bool=false;
    var dhtResponseError:Error;
    var dhtResponseValue:DrpcValue;

    var key:Buffer;
    var dhtQuery:DhtQuery;

    var expectedConfiguration: StorageConfig;

    public function new(){
        super();

        dhtGenerator = new DrpcRandomGenerator();
    }

    // summary steps --------------------------------------------

    @step
    public function a_value_was_stored() {
        given().an_storage_instance();
        given().a_storage_command();
        when().a_valid_update_request_is_received();
    }

    // given ---------------------------------------------------

    @step
    public function an_storage_instance() {
        storage = new DefaultDrpcStorage();
    }

    @step
    public function a_storage_command() {
        storageCommand = new DrpcStorageCommand(storage);
    }


    @step
    public function there_are_some_values_in_storage_for_some_key() {
        key = dhtGenerator.target();

        for (i in 0...5){
            var value = dhtGenerator.storageValue();
            storage.store(key, value);
        }
    }

    // when  ---------------------------------------------------

    @step
    public function a_valid_update_request_is_received() {
        when().receiving_this_update_request(updateQuery());
    }

    @step
    public function a_update_request_is_received_with_no_value() {
        when().receiving_this_update_request(queryWithNoValue());
    }


    @step
    public function a_query_request_is_received() {
        when().receiving_this_query_request(queryRequest());
    }

    @step
    public function a_query_request_is_received_for_that_key() {
        var query = queryRequest();
        query.target = key;

        when().receiving_this_query_request(query);
    }

    @step
    function receiving_this_update_request(query:DhtQuery) {
        this.dhtQuery = query;
        storageCommand.update(dhtQuery, queryResponseCallback);
    }


    @step
    function receiving_this_query_request(query:DhtQuery) {
        this.dhtQuery = query;
        storageCommand.query(dhtQuery, queryResponseCallback);
    }

    function queryResponseCallback(?err: Error, ?value:DrpcValue) {
        this.dhtResponseCalled = true;
        this.dhtResponseError = err;
        this.dhtResponseValue = value;
    }


    @step
    public function configuring_the_storage_command_with_some_configuration() {
        expectedConfiguration = {
            expirationTime: {
                defaultValue: Duration.of(2, Minutes),
                maxValue: Duration.of(30, Minutes)
            }
        };

        this.storageCommand.configure(expectedConfiguration);
    }


    // then  ---------------------------------------------------


    @step
    public function the_received_value_should_be_stored() {
        var values = listValuesAtQueryKey();
        var expected = dhtValueToBinary(dhtQuery.value);

        assertThat(values, hasItem(equalTo(expected)));
    }

    @step
    public function no_value_should_be_stored() {
        var values = listValuesAtQueryKey();

        assertThat(values, isEmpty(), "dht stored values");
    }

    function listValuesAtQueryKey() {
        var storageValues = storage.listValuesFor(dhtQuery.target);

        return storageValues.map(storageValueToBinary);
    }


    @step
    public function the_callback_should_be_called_with_no_value() {
        then().the_callback_was_called_with_no_error();

        assertThat(dhtResponseValue, is(nullValue()), "dht response value");
    }

    @step
    public function the_callback_should_be_called_with_the_stored_values() {
        then().the_callback_was_called_with_no_error();

        var expectedValues = listValuesAtQueryKey();
        var values = dhtResponseValue.asArray().map((value:DrpcValue)->{
                return Binary.fromBuffer(value.asBuffer());
            });

        assertThat(values, is(array(expectedValues)),
            "dht response value");
    }

    @step
    function the_callback_was_called_with_no_error() {
        assertThat(dhtResponseCalled, is(true),
            "dht response callback should have been called");
        assertThat(dhtResponseError, is(nullValue()), "dht response error");
    }

    @step
    public function the_configuration_should_be_passed_to_the_storage_instance(){
        storage.getConfig().shouldBeEqualsTo(expectedConfiguration);
    }

    // ------------------------------------------------------------

    function updateQuery():DhtQuery {
        return queryWithRandomValue();
    }

    function queryRequest():DhtQuery {
        return queryWithNoValue();
    }

    function queryWithNoValue():DhtQuery {
        var query = queryWithRandomValue();
        query.value = null;

        return query;
    }

    function queryWithRandomValue(): DhtQuery {
        return dhtGenerator.query(DrpcConstants.storageCommand);
    }

    function storageValueToBinary(storageValue: StorageValue): Binary{
        if(storageValue == null) return null;

        return dhtValueToBinary(storageValue.value);
    }

    function dhtValueToBinary(value:DrpcValue): Binary{
        return value.asBuffer();
    }

    function bufferToBinary(buffer: Buffer): Binary{
        if(buffer == null){
            return null;
        }

        return Binary.fromBytes(buffer.hxToBytes());
    }
}