package discovery.test.tests.keytransport.dhtkeytransport;

import discovery.keys.storage.PublicKeyData;
import haxe.Exception;
import discovery.dht.keytransport.DhtKeyComponents.DhtKeyDependencies;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.dhttypes.building.BaseDhtComponents;
import discovery.test.mockups.HashMakerMockup;
import discovery.test.common.fakes.domain.DummySerializer;
import discovery.dht.keytransport.DhtKeyMessages;
import discovery.test.mockups.DhtGetterMockup;
import discovery.domain.serialization.Serializer;
import discovery.dht.DhtKeyTransport;
import hxgiven.Steps;


class DhtKeyTransportSteps<Self:DhtKeyTransportSteps<Self>> extends Steps<Self> {

    var keyTransport:DhtKeyTransport;

    var dependencies:DhtKeyDependencies;
    var publicKeySerializer:Serializer<PublicKeyData>;

    var dhtGetterMockup:DhtGetterMockup;
    var hashMakerMockup:HashMakerMockup;

    var keyMessages:DhtKeyMessages;

    var expectedException:Exception;


    public function new(){
        super();

        dhtGetterMockup = new DhtGetterMockup();
        hashMakerMockup = new HashMakerMockup();
        publicKeySerializer = new DummySerializer();

        keyMessages = new DhtKeyMessages(hashMakerMockup.hashMaker());
    }

    function dhtMockup() {
        return dhtGetterMockup.dht();
    }

    function dhtComponents():DhtComponents {
        return new BaseDhtComponents(
            dhtGetterMockup.getter(),
            hashMakerMockup.hashMaker()
        );
    }

    // given summary -------------------------------------------

    @step
    public function a_dht_key_transport_was_built() {
        given().the_key_dependencies();
        given().a_key_transport_from_those_dependencies();
    }

    // given ---------------------------------------------------


    @step
    public function a_dht_key_transport() {
        given().the_key_dependencies();
        given().a_key_transport_from_those_dependencies();
    }

    @step
    public function the_key_dependencies() {
        dependencies = {
            dhtComponents: dhtComponents(),
            pubKeySerializer: publicKeySerializer
        };
    }

    @step
    public function a_key_transport_from_those_dependencies() {
        keyTransport = new DhtKeyTransport(dependencies);
    }

    @step
    public function the_dht_has_started() {
        when().the_dht_starts();
    }

    // when  ---------------------------------------------------

    @step
    public function the_dht_starts() {
        dhtGetterMockup.notifyDhtReceived();
    }

    @step
    public function the_dht_fails_to_start() {
        expectedException = new Exception('dht start failure');

        dhtGetterMockup.notifyDhtStartupFailed(expectedException);
    }

    // then  ---------------------------------------------------

}
