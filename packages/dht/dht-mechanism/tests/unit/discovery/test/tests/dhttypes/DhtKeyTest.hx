package discovery.test.tests.dhttypes;

import discovery.test.mockups.HashMakerMockup;
import discovery.dht.crypto.HashMaker;
import haxe.Exception;
import discovery.format.exceptions.InvalidValueException;
import discovery.format.Binary;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.DhtKey;
import discovery.test.generators.DrpcRandomGenerator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DhtKeyTest implements Scenario{
    @steps
    var steps: DhtKeySteps;

    @Test
    @scenario
    public function get_hash_from_binary(){
        given().a_binary_key();
        when().getting_the_hash_value();
        then().the_binary_value_should_be_returned();
    }

    @Test
    @scenario
    public function try_get_hash_from_string_key(){
        given().a_string_key();
        when().getting_the_hash_value();
        then().an_exception_should_be_thrown();
    }

    @Test
    @scenario
    public function get_hash_using_hash_maker_from_binary_key(){
        given().a_binary_key();
        given().a_hash_maker();
        when().getting_the_hash_value_passing_the_hash_maker();
        then().the_binary_value_should_be_returned();
    }

    @Test
    @scenario
    public function get_hash_using_hash_maker_from_string_key(){
        given().a_string_key();
        given().a_hash_maker();
        when().getting_the_hash_value_passing_the_hash_maker();
        then().key_hash_should_be_calculated_using_the_hash_maker();
    }


    @Test
    @scenario
    public function get_hash_from_null_key(){
        given().a_null_key();
        when().getting_the_hash_value();
        then().a_null_hash_should_be_returned();
        and().no_exception_should_be_thrown();
    }
}

class DhtKeySteps extends Steps<DhtKeySteps>{
    var key:DhtKey;

    var generator:DrpcRandomGenerator;
    var hashMakerMockup:HashMakerMockup;

    var stringKey:String;
    var expectedHash:Binary;
    var hash:Binary;
    var hashMaker:HashMaker;

    var capturedException:Exception;

    public function new(){
        super();

        hashMakerMockup = new HashMakerMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_binary_key() {
        expectedHash = RandomGen.binary.bytes(32);
        key = expectedHash;
    }


    @step
    public function a_string_key() {
        stringKey = RandomGen.primitives.name();

        key = stringKey;
    }

    @step
    public function a_null_key() {
        key = DhtKey.fromText(null);
    }

    @step
    public function a_hash_maker() {
        hashMaker = hashMakerMockup.hashMaker();
    }

    // when  ---------------------------------------------------

    @step
    public function getting_the_hash_value() {
        when().getting_the_hash_with();
    }

    @step
    public function getting_the_hash_value_passing_the_hash_maker() {
        when().getting_the_hash_with(hashMaker);
    }

    @step
    public function getting_the_hash_with(?hashMaker: HashMaker) {
        try{
            hash = key.getHash(hashMaker);
        }
        catch(e){
            capturedException = e;
        }
    }
    // then  ---------------------------------------------------

    @step
    public function the_binary_value_should_be_returned() {
        assertThat(hash, is(equalTo(expectedHash)), "Hash does not match");
    }

    @step
    public function an_exception_should_be_thrown() {
        assertThat(capturedException, isA(InvalidValueException),
            "Expected exception was not thrown");
    }

    @step
    public function key_hash_should_be_calculated_using_the_hash_maker() {
        hashMakerMockup.checkHashMakeFromString(stringKey);
    }

    @step
    public function a_null_hash_should_be_returned() {
        assertThat(hash, is(nullValue()));
    }


    @step
    public function no_exception_should_be_thrown() {
        assertThat(capturedException, is(nullValue()), "no exception expected");
    }
}