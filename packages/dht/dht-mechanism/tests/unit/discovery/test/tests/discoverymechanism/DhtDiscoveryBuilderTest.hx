package discovery.test.tests.discoverymechanism;

import discovery.test.mockups.DhtComponentsMockup;
import discovery.test.generators.DhtRandomGenerator;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.DhtDiscoveryMechanism;
import discovery.test.fakes.FakePromisableFactory;
import discovery.dht.DhtDiscoveryMechanism.DhtDiscoveryDeps;
import discovery.dht.DhtDiscoveryMechanismBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtDiscoveryBuilderTest implements Scenario{
    @steps
    var steps: DhtDiscoveryBuilderSteps;

    @Before
    public function setup(){
        resetSteps();
    }

    @Test
    @scenario
    public function build_mechanism_from_dependencies(){
        given().the_dht_discovery_dependencies();
        given().a_mechanism_builder_from_given_dependencies();
        when().building_a_dht_mechanism();
        then().a_valid_mechanism_should_be_returned();
    }

    @Test
    @scenario
    public function configure_dht_start_options(){
        given().a_mechanism_builder_was_created();
        when().configuring_the_builder_with_some_options();
        and().building_a_dht_mechanism();
        then().the_dht_options_should_be_passed_to_the_mechanism();
    }

    @Test
    @scenario
    public function pass_dht_start_options_on_builder_constructor(){
        given().the_dht_discovery_dependencies();
        given().the_dht_options_are_specified();
        when().a_mechanism_builder_is_created_with_given_options();
        and().a_dht_mechanism_is_built();
        then().the_dht_options_should_be_passed_to_the_mechanism();
    }


    @Test
    @scenario
    public function restore_configurations_to_default(){
        given().a_mechanism_builder_was_created_with_some_default_options();
        when().configuring_the_builder_with_some_options();
        and().configuring_the_builder_with(null);
        when().a_dht_mechanism_is_built();
        then().the_default_configurations_should_be_used();
    }
}

class DhtDiscoveryBuilderSteps extends Steps<DhtDiscoveryBuilderSteps>
{
    var builder:DhtDiscoveryMechanismBuilder;

    var dhtComponentsMockup:DhtComponentsMockup;
    var generator:DhtRandomGenerator;

    var dependencies:DhtMechanismBuilderDependencies;
    var dhtOptions:DhtOptions;
    var defaultOptions:DhtOptions;

    var builtMechanism:DhtDiscoveryMechanism;


    public function new(){
        super();

        dhtComponentsMockup = new DhtComponentsMockup();
        generator = new DhtRandomGenerator();
    }

    // summary -------------------------------------------------

    @step
    public function a_mechanism_builder_was_created_with_some_default_options()
    {
        given().the_dht_options_are_specified();
        given().a_mechanism_builder_was_created();

        defaultOptions = this.dhtOptions;
    }

    @step
    public function a_mechanism_builder_was_created() {
        given().the_dht_discovery_dependencies();
        given().a_mechanism_builder_from_given_dependencies();
    }

    // given ---------------------------------------------------

    @step
    public function the_dht_discovery_dependencies() {
        dependencies = {
            promises: new FakePromisableFactory(),
            dhtComponents: dhtComponentsMockup.components()
        };
    }


    @step
    public function the_dht_options_are_specified() {
        dhtOptions = generator.dhtOptions();
    }


    @step
    public function a_mechanism_builder_from_given_dependencies() {
        when().a_mechanism_builder_is_created_with_given_options();
    }

    // when  ---------------------------------------------------


    @step
    public function a_mechanism_builder_is_created_with_given_options() {
        builder = new DhtDiscoveryMechanismBuilder(dependencies, dhtOptions);
    }

    @step
    public function configuring_the_builder_with_some_options() {
        given().the_dht_options_are_specified();
        when().configuring_the_builder_with(dhtOptions);
    }

    @step
    public function configuring_the_builder_with(?options:DhtOptions) {
        builder.configure(options);
    }


    @step
    public function a_dht_mechanism_is_built(){
        when().building_a_dht_mechanism();
    }

    @step
    public function building_a_dht_mechanism() {
        builtMechanism = builder.build();
    }

    // then  ---------------------------------------------------

    @step
    public function a_valid_mechanism_should_be_returned() {
        builtMechanism.shouldNotBeNull();
    }

    @step
    public function the_default_configurations_should_be_used() {
        then().these_configurations_should_be_used(defaultOptions);
    }

    @step
    public
    function the_dht_options_should_be_passed_to_the_mechanism() {
        then().these_configurations_should_be_used(dhtOptions);
    }

    @step
    function these_configurations_should_be_used(options: DhtOptions) {
        dhtComponentsMockup.verifyConfiguredWith(options);
    }
}