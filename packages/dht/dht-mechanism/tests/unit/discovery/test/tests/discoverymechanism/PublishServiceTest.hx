package discovery.test.tests.discoverymechanism;

import discovery.test.tests.discoverymechanism.steps.PublishServiceBaseSteps;
import discovery.test.helpers.RandomGen;
import hxgiven.Scenario;


class PublishServiceTest implements Scenario{
    @steps
    var steps: PublishServiceSteps;

    @Test
    @scenario
    public function publish_service_announcement_on_dht(){
        given().a_dht_discovery_mechanism_was_created();
        and().the_dht_has_started();
        when().publishing_a_service_announcement();
        then().the_serialized_announce_should_be_put_at_the_identifier_key();
        then().the_serialized_announce_should_be_put_at_the_service_type_key();
    }


    @Test
    @scenario
    public function notify_publish_event(){
        given().a_service_publication_started();
        when().the_service_announcement_is_put_on_dht();
        then().the_on_publish_event_should_be_called();
    }

    @Test
    @scenario
    public function notify_publish_failure(){
        given().a_service_publication_started();
        when().the_service_announcement_fails_to_be_put_on_dht();
        then().the_on_finish_event_should_be_called_with_the_exception();
    }

    @Test
    @scenario
    public function stop_publication(){
        given().a_service_was_published_with_success();
        when().stopping_the_publication();
        then().the_on_finish_event_should_be_called_with_no_exception();
        and().the_stop_callback_should_be_called();
    }
}

class PublishServiceSteps extends PublishServiceBaseSteps<PublishServiceSteps>{

    // given summary ---------------------------------------------------


    // given ---------------------------------------------------


    // when  ---------------------------------------------------

    @step
    override public function publishing_a_service_announcement() {
        publishInfo = RandomGen.publishInfo();

        publicationControl = mechanism.publish(
            publishInfo, publishListenersMockup.listeners());
    }

    // then  ---------------------------------------------------

}