package discovery.test.tests.impl.js;

import discovery.test.common.CallableMockup;
import discovery.dht.impl.js.dhtrpc.ReadableStop;
import discovery.test.mockups.js.ReadableMockup;
import discovery.base.search.Stoppable;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ReadableStopTest implements Scenario{
    @steps
    var steps: ReadableStopSteps;

    @Test
    @scenario
    public function destroy_readable_on_stop(){
        given().a_readable_stop_from_a_readable_stream();
        when().calling_stop();
        then().the_readable_should_be_destroyed();
    }

    @Test
    @scenario
    public function notify_readable_stop(){
        given().a_readable_stop_from_a_readable_stream();
        when().calling_stop();
        and().the_close_event_is_emitted();
        then().the_stop_callback_should_be_called();
    }
}

class ReadableStopSteps extends Steps<ReadableStopSteps>{
    var readableStop:Stoppable;

    var readableMockup:ReadableMockup;
    var stopCallbackMockup:CallableMockup;

    public function new(){
        super();

        readableMockup = new ReadableMockup();
        stopCallbackMockup = new CallableMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_readable_stop_from_a_readable_stream() {
        readableStop = new ReadableStop(readableMockup.readable());
    }

    // when  ---------------------------------------------------

    @step
    public function calling_stop() {
        readableStop.stop(stopCallbackMockup.callable());
    }

    @step
    public function the_close_event_is_emitted() {
        readableMockup.emitEvent('close');
    }

    // then  ---------------------------------------------------

    @step
    public function the_readable_should_be_destroyed() {
        readableMockup.verifyDestroyCalled();
    }

    @step
    public function the_stop_callback_should_be_called() {
        stopCallbackMockup.verifyCallbackCalled();
    }
}