package discovery.test.tests.discoverymechanism.steps;


import discovery.dht.dhttypes.building.DhtComponentsAdapter;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.discoverymechanism.DhtSerializerBuilder;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.serialization.Serializer;
import discovery.dht.discoverymechanism.DefaultDhtKeyMaker;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.generators.DhtRandomGenerator;
import discovery.test.helpers.RandomGen;
import discovery.dht.DhtDiscoveryMechanism;
import discovery.test.mockups.DhtBuilderMockup;
import hxgiven.Steps;

using discovery.test.matchers.CommonMatchers;


class DhtMechanismSteps<Self:DhtMechanismSteps<Self>> extends Steps<Self>{
    var mechanism:DiscoveryMechanism;

    var dhtRandom:DhtRandomGenerator;

    var dhtBuilderMockup:DhtBuilderMockup;
    var dhtComponents:DhtComponents;

    var promisesFactory: FakePromisableFactory;

    var keyMaker:DhtKeyMaker;
    var announceSerializer:Serializer<Signed<Announcement>>;

    public function new(){
        super();

        dhtBuilderMockup = new DhtBuilderMockup();
        dhtComponents = new DhtComponentsAdapter(dhtBuilderMockup.builder());

        dhtRandom = new DhtRandomGenerator();

        promisesFactory = new FakePromisableFactory();

        keyMaker = new DefaultDhtKeyMaker(dhtBuilderMockup.hashMaker());

        var builder = new DhtSerializerBuilder();
        announceSerializer = builder.buildSerializer();
    }


    // summary ---------------------------------------------------

    @step
    public function a_dht_discovery_mechanism_was_created() {
        given().a_dht_discovery_mechanism();
    }

    @step
    public function a_service_was_published() {
        when().publishing_some_service();
    }

    // given ---------------------------------------------------

    @step
    public function a_dht_discovery_mechanism() {
        mechanism = new DhtDiscoveryMechanism(dhtDeps());
    }

    @step
    public function the_dht_has_started() {
        dhtMockup().startDht();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_some_service() {
        var publishInfo = RandomGen.publishInfo();

        mechanism.publish(publishInfo);
    }

    // then  ---------------------------------------------------

    // -------------------------

    function dhtMockup() {
        return dhtBuilderMockup.dht();
    }

    function dhtDeps():DhtDiscoveryDeps {
        return {
            dhtComponents: dhtComponents,
            promises: promisesFactory
        };
    }
}