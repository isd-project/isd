package discovery.test.tests.impl.js;

import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage.StorageConfig;
import discovery.test.matchers.FunctionMatcher;
import haxe.Timer;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import discovery.domain.Validity;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import org.hamcrest.Matcher;
import org.hamcrest.CustomTypeSafeMatcher;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import discovery.test.generators.DrpcRandomGenerator;
import discovery.test.helpers.RandomGen;
import discovery.utils.time.Duration;
import discovery.dht.impl.js.dhtrpc.storage.StorageValuesList;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
import discovery.test.matchers.js.StorageValueMatchers.*;

using discovery.utils.EqualsComparator;
using discovery.utils.IteratorTools;
using discovery.utils.time.TimeUnitTools;
using discovery.test.matchers.CommonMatchers;


class StorageValuesListTest extends AsyncTests implements Scenario{
    @steps
    var steps: StorageValuesListSteps;

    @Test
    @scenario
    public function empty_list(){
        given().the_storage_is_empty();
        then().isEmpty_should_be(true);
    }


    @Test
    @scenario
    public function adding_value(){
        given().the_storage_is_empty();
        when().adding_some_value();
        then().isEmpty_should_be(false);
    }

    @Test
    @scenario
    public function adding_value_and_listing(){
        when().adding_some_value();
        and().listing_the_values();
        then().the_added_value_should_be_found();
    }

    @Test
    public function configure_test(){
        final DEFAULT_EXPIRATION = StorageValuesList.DEFAULT_EXPIRATION;

        var arguments = [
            [null, null, DEFAULT_EXPIRATION, DEFAULT_EXPIRATION],
            [50.seconds(), null, 50.seconds(), 50.seconds()],
            [50.seconds(), 100.seconds(), 50.seconds(), 100.seconds()]
        ];

        for(args in arguments){
            this.apply_configuration({
                expirationTime: {
                    defaultValue: args[0],
                    maxValue: args[1]
                }
            }, args[2], args[3]);
        }
    }

    @scenario
    public function apply_configuration(config:StorageConfig, defaultExpiration: Duration, maxExpiration:Duration)
    {
        steps
        .given('this configuration: ${config}', ()->{});
        when().configuring_the_storage_with(config);
        then().the_default_expiration_should_be(defaultExpiration);
        and().the_max_expiration_should_be(maxExpiration);
    }


    @Test
    @scenario
    public function when_adding_value_set_default_expiration(){
        given().a_default_validity_duration();
        when().adding_an_storage_value_without_expiration_date();
        then().the_value_expiration_should_be_set_to_default();
    }

    @Test
    @scenario
    public function should_not_add_duplicated_values(){
        given().the_storage_is_empty();
        when().adding_some_value();
        and().adding_the_same_value_again();
        then().the_list_should_have_length_of(1);
    }


    @Test
    @scenario
    public function limit_maximum_expiration_time(){
        given().some_maximum_expiration_time();
        when().storing_a_value_with_most_of_the_expiration_time();
        then().the_value_expiration_should_be_limited_to_the_maximum();
    }


    @Test
    @scenario
    public function limit_maximum_expiration_time_to_default(){
        given().a_storage_with_no_maximum_expiration_time();
        when().setting_the_default_time();
        then().the_max_expiration_time_should_be_the_default();
    }


    @Test
    @scenario
    public function should_not_add_expired_values(){
        given().the_storage_is_empty();
        given().an_expired_value();
        when().adding_that_value();
        then().isEmpty_should_be(true);
    }


    @Test
    @scenario
    public function remove_value(){
        given().the_storage_is_empty();
        given().a_value_was_added();
        when().removing_that_value();
        then().isEmpty_should_be(true);
    }

    @AsyncTest
    @scenario
    public function remove_expired_values(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 50);

        given().the_storage_has_default_expiration_of(10.millis());
        given().a_value_was_added();
        when().passing_a_time_of(12.millis(), next(()->{
            when().listing_the_values();
            then().no_expired_value_should_be_returned();
            and().isEmpty_should_be(true);

            done();
        }));
    }


    @AsyncTest
    @scenario
    public function on_adding_duplicate_update_expiration(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 50);

        given().the_storage_has_default_expiration_of(10.millis());
        given().a_value_was_added();
        when().passing_a_time_of(5.millis(), next(()->{
            and().adding_the_same_value_again();
            then().that_value_expiration_should_have_been_updated();

            done();
        }));
    }


    @Test
    @scenario
    public function on_add_duplicated_keep_the_greater_expiration_date(){
        given().the_storage_has_default_expiration_of(10.minutes());
        given().a_value_was_added_with_expiration_of(8.minutes());
        when().the_same_value_is_added_with_expiration_of(1.minutes());
        then().the_value_validity_should_not_decrease();
    }

}

class StorageValuesListSteps extends Steps<StorageValuesListSteps>{
    var storageValues:StorageValuesList;

    var generator:DrpcRandomGenerator;

    var defaultDuration:Duration;
    var storageValue:StorageValue;
    var addedValue:StorageValue;
    var previousValue:StorageValue;

    var foundValues:Array<StorageValue>;

    public function new(){
        super();

        storageValues = new StorageValuesList();
        generator = new DrpcRandomGenerator();
    }

    // given ---------------------------------------------------

    @step
    public function a_default_validity_duration() {
        final oneMin = 1.minutes();
        final oneDay = 1.days();

        defaultDuration = RandomGen.time.durationBetween(oneMin, oneDay);

        storageValues.defaultExpirationTime = defaultDuration;
    }


    @step
    public function the_storage_is_empty() {
        storageValues.clear();
    }


    @step
    public function some_maximum_expiration_time() {
        var minDuration = storageValues.defaultExpirationTime;
        var maxDuration = minDuration + 30.minutes();
        var duration = RandomGen.time.durationBetween(minDuration, maxDuration);
        storageValues.maxExpirationTime = duration;
    }

    @step
    public function a_storage_with_no_maximum_expiration_time() {
        storageValues.maxExpirationTime = null;
    }

    @step
    public function an_expired_value() {
        var expired = Date.fromTime(Date.now().getTime() - 100);
        var started = Date.fromTime(expired.getTime() - 100);

        storageValue = generator.storageValue({
            validity: new Validity(expired, started)
        });
    }

    @step
    public function the_storage_has_default_expiration_of(time:Duration) {
        storageValues.defaultExpirationTime = time;
    }

    @step
    public function a_value_was_added() {
        when().adding_some_value();
    }


    // when  ---------------------------------------------------


    @step
    public function configuring_the_storage_with(config:StorageConfig) {
        storageValues.configure(config);
    }

    @step
    public function adding_an_storage_value_without_expiration_date() {
        when().adding_a_storage_value();
    }

    @step
    public function adding_some_value() {
        when().adding_a_storage_value();
    }


    @step
    public function adding_the_same_value_again() {
        when().adding_that_value();
    }

    @step
    public function adding_that_value() {
        when().adding_this_value(storageValue);
    }

    @step
    public function storing_a_value_with_most_of_the_expiration_time() {
        var expirationTime = storageValues.maxExpirationTime + 1.millis();

        when().adding_value_with_expiration(expirationTime);
    }

    @step
    public function a_value_was_added_with_expiration_of(?expiration:Duration) {
        when().adding_value_with_expiration(expiration);
    }

    @step
    public
    function the_same_value_is_added_with_expiration_of(?expiration:Duration)
    {
        when().adding_value_with_expiration(expiration, addedValue);
    }

    @step
    function adding_value_with_expiration(
        expiration:Duration, ?value:StorageValue)
    {
        storageValue = value_with_expiration(expiration, value);
        when().adding_this_value(storageValue);
    }
    function value_with_expiration(expiration:Duration, ?value:StorageValue) {
        var newValue = if(value != null) addedValue.clone();
                        else generator.storageValue();

        if(expiration != null){
            newValue.validity = Validity.validFor(expiration);
        }

        return newValue;
    }

    @step
    function adding_a_storage_value(?options:StorageValueOptions) {
        storageValue = generator.storageValue(options);

        when().adding_this_value(storageValue);
    }

    @step
    function adding_this_value(value: StorageValue) {
        previousValue = addedValue;
        addedValue = storageValues.add(value);
    }

    @step
    public function removing_that_value() {
        storageValues.remove(storageValue);
    }


    @step
    public function listing_the_values() {
        foundValues = storageValues.list().toArray();
    }

    @step
    public function setting_the_default_time() {
        given().a_default_validity_duration();
    }

    @step
    public
    function passing_a_time_of(time:Duration, next: NextCallback)
    {
        Timer.delay(()->{
            next();
        }, Math.round(time.time()));
    }

    // then  ---------------------------------------------------


    @step
    public function isEmpty_should_be(expected: Bool) {
        var empty=if(expected) "empty" else "not empty";

        assertThat(storageValues.isEmpty(), is(expected),
            'storage values should be ${empty}');
    }

    @step
    public function the_value_expiration_should_be_set_to_default() {
        function defaultExpiration(value: StorageValue){
            if(value == null || value.validity == null) return false;

            var valueDuration = value.validity.duration();

            return value != null
                && value.validity != null
                && defaultDuration.equals(valueDuration, 1);
        };

        addedValue.shouldMatchWithType(StorageValue, defaultExpiration,
            'has default expiration duration: ${defaultDuration}'
        );
    }


    @step
    public function the_added_value_should_be_found() {
        assertThat(foundValues, hasItem(withSameValue(storageValue)));
    }

    @step
    public function the_list_should_have_length_of(expectedLength:Int) {
        when().listing_the_values();

        assertThat(foundValues, hasSize(expectedLength));
    }

    @step
    public function the_value_expiration_should_be_limited_to_the_maximum(){
        when().listing_the_values();
        then().the_value_expiration_should_be_limited_to(storageValues.maxExpirationTime);
    }

    @step
    function the_value_expiration_should_be_limited_to(maxValue: Duration)
    {
        var value:StorageValue = findValue(storageValue);

        assertThat(value, is(notNullValue()), "Added value not found");
        assertThat(value.validity.duration().time(),
            is(lessThanOrEqualTo(maxValue.time())), "Expiration time");
    }

    @step
    public function the_value_validity_should_not_decrease() {
        assertThat(addedValue.validTo(),
            is(greaterThanOrEqualTo(previousValue.validTo())));
    }

    @step
    public function the_max_expiration_time_should_be_the_default() {
        assertThat(storageValues.maxExpirationTime,
            is(equalTo(storageValues.defaultExpirationTime)));
    }

    @step
    public function no_expired_value_should_be_returned() {
        assertThat(foundValues, not(hasItem(whichExpired())));
    }

    @step
    public function that_value_expiration_should_have_been_updated() {
        assertThat(addedValue.validTo(),
            is(greaterThan(previousValue.validTo())));
    }

    @step
    public function the_default_expiration_should_be(expected:Duration) {
        assertThat(storageValues.defaultExpirationTime, is(equalTo(expected)));
    }

    @step
    public function the_max_expiration_should_be(expected:Duration) {
        assertThat(storageValues.maxExpirationTime, is(equalTo(expected)));
    }

    // ---------------------------------------------------------------------


    function thatMatches<T>(type: Dynamic, matcher:(value:T) -> Bool, ?description:String, ?mismatchDescriptor: (T)->String):Matcher<T>
    {
        return new FunctionMatcher(type, matcher, description, mismatchDescriptor);
    }


    function findValue(expected:StorageValue):StorageValue {
        assertThat(foundValues, is(notNullValue()), "No values retrieved");

        var filtered = this.foundValues.filter(hasValueFilter(expected.value));

        return if(filtered.length > 0) filtered[0] else null;
    }
}

