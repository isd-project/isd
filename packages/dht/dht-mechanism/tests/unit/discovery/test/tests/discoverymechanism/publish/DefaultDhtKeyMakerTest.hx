package discovery.test.tests.discoverymechanism.publish;

import discovery.dht.dhttypes.DhtKey;
import haxe.exceptions.NotImplementedException;
import discovery.impl.parser.TinkIdentifierParser;
import discovery.base.IdentifierParser;
import discovery.domain.Identifier;
import discovery.domain.Description;
import discovery.test.helpers.RandomGen;
import discovery.dht.discoverymechanism.DefaultDhtKeyMaker;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.dht.crypto.HashMaker;
import discovery.test.mockups.HashMakerMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class DefaultDhtKeyMakerTest implements Scenario{
    @steps
    var steps: DefaultDhtKeyMakerSteps;

    @Test
    @scenario
    public function generate_service_type_key(){
        given().a_hash_maker_instance();
        given().a_key_maker_from_that_hash_maker();
        given().a_service_type_of('http');
        when().generating_a_service_type_key();
        then().a_hash_should_be_generated_from('servicetype=http');
        and().a_valid_dht_key_should_be_returned();
    }


    @Test
    public function generate_identifier_key_tests(){
        generate_identifier_key(
            "srv://example.http.a665f0afd84ea51eecde868589;fmt=b16",
            "srv://example.http.a665f0afd84ea51eecde868589;fmt=b16");
        generate_identifier_key(
            "srv://FCOYBLI.example.http.UZS7BL6YJ2SR53G6Q2CYS===;fmt=b32",
            "srv://289d80ad.example.http.a665f0afd84ea51eecde868589;fmt=b16");
        generate_identifier_key(
            "srv://KJ2ArQ.example.http.pmXwr9hOpR7s3oaFiQ;fmt=b64u",
            "srv://289d80ad.example.http.a665f0afd84ea51eecde868589;fmt=b16");
    }

    @scenario
    public function generate_identifier_key(
        identifierUrl: String, expectedHashInput: String)
    {
        steps.given('params: identifierUrl=${identifierUrl}, expectedHashInput: ${expectedHashInput}',
            ()->{});

        given().a_key_maker_from_a_hash_maker_instance();
        given().using_this_identifier(identifierUrl);
        when().generating_an_identifier_key();
        then().a_hash_should_be_generated_from(
            expectedHashInput
        );
        and().a_valid_dht_key_should_be_returned();
    }

    @Test
    @scenario
    public function try_to_generate_key_from_null_identifier(){
        given().a_key_maker_from_a_hash_maker_instance();
        given().this_identifier(null);
        when().generating_an_identifier_key();
        then().a_null_key_should_be_returned();
    }


    @Test
    @scenario
    public function try_to_generate_key_from_incomplete_identifier(){
        given().a_key_maker_from_a_hash_maker_instance();
        given().this_identifier({
            name: 'hello',
            type: null,
            providerId: null
        });
        when().generating_an_identifier_key();
        then().a_null_key_should_be_returned();
    }
}

class DefaultDhtKeyMakerSteps extends Steps<DefaultDhtKeyMakerSteps>{

    var keyMaker:DhtKeyMaker;

    var hashMakerMockup:HashMakerMockup;
    var hashMaker:HashMaker;
    var identifierParser:IdentifierParser;

    var serviceType:String;
    var srvIdentifier:Identifier;
    var generatedKey:DhtKey;

    public function new(){
        super();

        hashMakerMockup = new HashMakerMockup();
        identifierParser = new TinkIdentifierParser();
    }

    function parseIdentifier(identifierStr:String): Identifier{
        return identifierParser.parseUrl(identifierStr);
    }

    // given summary -------------------------------------------

    @step
    public function a_key_maker_from_a_hash_maker_instance() {
        given().a_hash_maker_instance();
        given().a_key_maker_from_that_hash_maker();
    }

    // given ---------------------------------------------------

    @step
    public function a_hash_maker_instance() {
        hashMaker = hashMakerMockup.hashMaker();
    }

    @step
    public function a_key_maker_from_that_hash_maker() {
        keyMaker = new DefaultDhtKeyMaker(hashMaker);
    }

    @step
    public function a_service_type_of(srvType:String) {
        serviceType = srvType;
    }

    @step
    public function using_this_identifier(identifierUrl:String) {
        srvIdentifier = parseIdentifier(identifierUrl);
    }

    @step
    public function this_identifier(identifier: Identifier) {
        srvIdentifier = identifier;
    }

    // when  ---------------------------------------------------

    @step
    public function generating_a_service_type_key() {
        generatedKey = keyMaker.serviceTypeKey(serviceType);
    }

    @step
    public function generating_an_identifier_key() {
        generatedKey = keyMaker.keyFromIdentifier(srvIdentifier);
    }

    // then  ---------------------------------------------------

    @step
    public function a_hash_should_be_generated_from(expectedHashInput:String) {
        hashMakerMockup.lastHashedString().shouldBe(expectedHashInput);
    }

    @step
    public function a_hash_should_be_generated_from_the_base16_representation() {
        then().a_hash_should_be_generated_from(srvIdentifier.toUrl(Base16));
    }

    @step
    public function a_valid_dht_key_should_be_returned() {
        generatedKey.shouldNotBeNull();
    }

    @step
    public function a_null_key_should_be_returned() {
        generatedKey.shouldBe(null);
    }
}