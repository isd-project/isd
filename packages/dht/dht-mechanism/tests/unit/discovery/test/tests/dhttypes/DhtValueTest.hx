package discovery.test.tests.dhttypes;

import discovery.test.helpers.RandomGen;
import discovery.format.Binary;
import discovery.dht.dhttypes.DhtValue;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class DhtValueTest implements Scenario{
    @steps
    var steps: DhtValueSteps;

    @Test
    @scenario
    public function binary_conversion(){
        var binary = Binary.fromHex("abcd1234");
        var value:DhtValue = binary;
        var binResult = value;

        binResult.shouldBeEqualsTo(binary);
    }


    @Test
    @scenario
    public function text_conversion(){
        var text = RandomGen.primitives.name();
        var value:DhtValue = text;
        var textResult     = value;

        textResult.shouldBeEqualsTo(text);
    }


    @Test
    @scenario
    public function convert_text_to_binary(){
        var text = RandomGen.primitives.name();
        var value:DhtValue = text;
        var result         = value.toBinary();

        result.shouldBeEqualsTo(Binary.fromText(text));
    }


    @Test
    @scenario
    public function convert_from_anonymous_object(){
        var obj = {
            num: 1,
            text: "hello",
            sub: {
                array: [1, 2, 3]
            }
        };
        var value:DhtValue = obj;
        var result         = value.asObject();

        result.shouldBeEqualsTo(obj);
    }

    @Test
    @scenario
    public function convert_from_object(){
        var desc = RandomGen.fullDescription();
        var value:DhtValue = desc;
        var result         = value.asObject();

        result.shouldBeEqualsTo(desc);
    }

}

class DhtValueSteps extends Steps<DhtValueSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}