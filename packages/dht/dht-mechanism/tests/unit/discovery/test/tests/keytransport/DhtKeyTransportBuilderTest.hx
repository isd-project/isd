package discovery.test.tests.keytransport;

import discovery.test.mockups.DhtComponentsMockup;
import discovery.test.generators.DhtRandomGenerator;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.DhtKeyTransport;
import discovery.dht.DhtKeyTransportBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtKeyTransportBuilderTest implements Scenario{
    @steps
    var steps: DhtKeyTansportBuilderSteps;

    @Test
    @scenario
    public function building_key_transport(){
        given().a_dht_components_instance();
        given().a_dht_key_transport_builder_from_it();
        when().building_a_key_transport();
        then().a_valid_dht_pki_should_be_built();
    }

    @Test
    @scenario
    public function configure_dht_key_transport(){
        given().a_dht_components_instance();
        given().a_dht_key_transport_builder_from_it();
        given().some_dht_configurations();
        when().configuring_the_pki_builder();
        then().it_should_delegate_the_configuration_to_the_dht_components();
    }
}

class DhtKeyTansportBuilderSteps extends Steps<DhtKeyTansportBuilderSteps>{
    var builder:DhtKeyTransportBuilder;

    var dhtComponents:DhtComponents;
    var dhtGenerator:DhtRandomGenerator;
    var componentsMockup:DhtComponentsMockup;

    var dhtKeyTransport:DhtKeyTransport;
    var dhtOptions:DhtOptions;

    public function new(){
        super();

        dhtGenerator = new DhtRandomGenerator();
        componentsMockup = new DhtComponentsMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_dht_components_instance() {
        dhtComponents = componentsMockup.components();
    }

    @step
    public function a_dht_key_transport_builder_from_it() {
        builder = new DhtKeyTransportBuilder(dhtComponents);
    }

    @step
    public function some_dht_configurations() {
        dhtOptions = dhtGenerator.dhtOptions();
    }

    // when  ---------------------------------------------------

    @step
    public function building_a_key_transport() {
        dhtKeyTransport = builder.buildKeyTransport();
    }

    @step
    public function configuring_the_pki_builder() {
        builder.configure(dhtOptions);
    }

    // then  ---------------------------------------------------

    @step
    public function a_valid_dht_pki_should_be_built() {
        dhtKeyTransport.shouldNotBeNull();
    }

    @step
    public
        function it_should_delegate_the_configuration_to_the_dht_components()
    {
        componentsMockup.dhtGetter().verifyConfiguredWith(dhtOptions);
    }
}