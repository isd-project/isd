package discovery.test.tests.dhttypes;

import discovery.dht.dhttypes.PeerAddress;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class PeerAddressTest implements Scenario{
    @steps
    var steps: PeerAddressSteps;

    @Test
    @scenario
    public function to_string(){
        test_to_string({address: '1.2.3.4', port: 1234}, '1.2.3.4:1234');
        test_to_string({address: '1.2.3.4'}, '1.2.3.4');
        test_to_string({port: 1234}, ':1234');
        test_to_string({}, null);
        test_to_string({address: 'cfd9:7a20:cbbe::89c9:febf:1239'},
                    '[cfd9:7a20:cbbe::89c9:febf:1239]');
        test_to_string({address: 'cfd9:7a20:cbbe::89c9:febf:1239', port:9090},
                    '[cfd9:7a20:cbbe::89c9:febf:1239]:9090');
    }


    @Test
    @scenario
    public function from_string(){
        test_from_string('1.2.3.4', {address: '1.2.3.4'});
        test_from_string('1.2.3.4:1234', {address: '1.2.3.4', port: 1234});
        test_from_string(':1234', {port: 1234});
        test_from_string('localhost', {address: 'localhost'});
        test_from_string('localhost:80', {address: 'localhost', port: 80});
        test_from_string(
            '[fe80::dead:beef:feed:1234]', {
            address: 'fe80::dead:beef:feed:1234'});
    }

    function test_to_string(addr:PeerAddress, expected:String) {
        addr.toString().shouldBe(expected);
    }

    function test_from_string(addrStr:String, expected:PeerAddress) {
        PeerAddress.fromString(addrStr).shouldBeEqualsTo(expected);
    }
}

class PeerAddressSteps extends Steps<PeerAddressSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}