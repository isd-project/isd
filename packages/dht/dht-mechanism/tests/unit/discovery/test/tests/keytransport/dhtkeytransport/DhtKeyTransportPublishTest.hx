package discovery.test.tests.keytransport.dhtkeytransport;

import discovery.keys.storage.PublicKeyData;
import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DhtValue;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.KeyMockup;
import hxgiven.Scenario;

using discovery.keys.KeyTransformations;


class DhtKeyTransportPublishTest implements Scenario{
    @steps
    var steps: DhtKeyTransportPublishSteps;

    @Test
    @scenario
    public function publish_key_to_dht(){
        given().a_key_publishing_was_started();
        when().the_dht_starts();
        then().the_serialized_public_key_data_should_be_put_at_dht();
    }

    @Test
    @scenario
    public function notify_published_key(){
        given().a_key_publishing_listener();
        given().a_key_publishing_was_started();
        and().the_dht_has_started();
        when().the_publication_completes();
        then().the_publishing_listener_should_be_notified_with_no_errors();
    }


    @Test
    @scenario
    public function notify_publish_error(){
        given().a_key_publishing_listener();
        given().a_key_publishing_was_started();
        and().the_dht_has_started();
        when().the_publication_fails();
        then().the_publishing_listener_should_be_notified_with_the_error();
    }

    @Test
    @scenario
    public function notify_get_dht_failure(){
        given().a_key_publishing_listener();
        given().a_key_publishing_was_started();
        when().the_dht_fails_to_start();
        then().the_publishing_listener_should_be_notified_with_the_error();
    }
}

class DhtKeyTransportPublishSteps
    extends DhtKeyTransportSteps<DhtKeyTransportPublishSteps>
{

    var pubKeyData:PublicKeyData;

    var publishListenerMockup:ListenerMock<Null<Exception>>;
    var publishListener:DoneCallback;

    public function new(){
        super();

        publishListenerMockup = new ListenerMock();
    }

    // given summary -------------------------------------------

    @step
    public function a_key_publishing_was_started() {
        given().a_dht_key_transport_was_built();
        and().some_public_key_data();
        when().publishing_that_public_key_data();
    }

    // given ---------------------------------------------------

    @step
    public function some_public_key_data() {
        pubKeyData = RandomGen.crypto.publicKeyData();
    }

    @step
    public function a_key_publishing_listener() {
        publishListener = cast publishListenerMockup.onEvent;
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_that_public_key_data() {
        keyTransport.publishKey(pubKeyData, publishListener);
    }

    @step
    public function the_publication_completes() {
        dhtMockup().notifyLastPutResult();
    }

    @step
    public function the_publication_fails() {
        expectedException = new Exception('test failure');

        dhtMockup().notifyLastPutResult(expectedException);
    }

    // then  ---------------------------------------------------

    @step
    public function the_serialized_public_key_data_should_be_put_at_dht() {
        var fingerprint = pubKeyData.fingerprint;
        var expectedKey:DhtKey = keyMessages.fingerprintKey(fingerprint);
        var expectedValue:DhtValue = publicKeySerializer.serialize(pubKeyData);

        dhtMockup().verifyPutValue(expectedKey, expectedValue);
    }

    @step
    public function the_publishing_listener_should_be_notified_with_no_errors() {
        publishListenerMockup.assertWasCalledWith(null);
    }

    @step
    public function the_publishing_listener_should_be_notified_with_the_error() {
        publishListenerMockup.assertWasCalledWith(expectedException);
    }
}