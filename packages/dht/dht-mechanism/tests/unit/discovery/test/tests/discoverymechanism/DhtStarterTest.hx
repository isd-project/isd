package discovery.test.tests.discoverymechanism;

import discovery.dht.dhttypes.startup.DefaultDhtStarter;
import discovery.dht.discoverymechanism.DhtStarter;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.test.mockups.DhtBuilderMockup;
import hxgiven.Steps;
import hxgiven.Scenario;


class DhtStarterTest implements Scenario{
    @steps
    var steps: DhtStarterSteps;

    @Test
    @scenario
    public function start_dht(){
        given().a_dht_starter();
        when().starting_it();
        then().it_should_build_and_start_a_dht();
    }

    @Test
    @scenario
    public function once_dht_started_call_listener(){
        given().an_start_listener();
        given().the_dht_was_started_using_that_listener();
        when().the_dht_startup_completes();
        then().the_listener_should_be_called();
    }

    @Test
    @scenario
    public function should_start_dht_only_once(){
        given().the_dht_already_started();
        when().starting_the_dht();
        but().the_dht_should_have_been_started_only_once();
    }

    @Test
    @scenario
    public function once_already_started_should_call_listener_anyway(){
        given().the_dht_already_started();
        given().another_start_listener();
        when().starting_the_dht();
        then().the_listener_should_be_called();
    }


    @Test
    @scenario
    public function should_call_all_waiting_listeners(){
        given().a_dht_starter();
        and().some_start_listeners();
        when().each_listener_is_registered_to_the_dht_startup();
        when().the_dht_startup_completes();
        then().all_listeners_should_be_called();
    }
}

class DhtStarterSteps extends Steps<DhtStarterSteps>{

    var starter:DhtStarter;
    var builderMockup:DhtBuilderMockup;

    var listener:ListenerMock<Null<Exception>>;
    var startListeners:Array<ListenerMock<Exception>>;

    public function new(){
        super();

        builderMockup = new DhtBuilderMockup();

        startListeners = [];
    }

    // summary ---------------------------------------------------

    @step
    public function the_dht_already_started() {
        given().the_dht_was_started_using_that_listener();
        when().the_dht_startup_completes();
    }

    @step
    public function the_dht_was_started_using_that_listener() {
        given().a_dht_starter();
        when().starting_it();
    }

    // given ---------------------------------------------------

    @step
    public function a_dht_starter(){
        starter = new DefaultDhtStarter({
            dhtBuilder: builderMockup.builder()
        });
    }

    @step
    public function another_start_listener() {
        given().an_start_listener();
    }

    @step
    public function an_start_listener() {
        listener = new ListenerMock();
    }

    @step
    public function some_start_listeners() {
        for(i in 0...5){
            startListeners.push(new ListenerMock());
        }
    }

    // when  ---------------------------------------------------{}

    @step
    public function starting_it() {
        when().starting_the_dht();
    }

    @step
    public function starting_the_dht() {
        var startListener = if(listener == null) null
                            else (?err) -> listener.onEvent(err);

        starter.starts(startListener);
    }

    @step
    public function each_listener_is_registered_to_the_dht_startup() {
        for(l in startListeners){
            starter.starts((?err)->l.onEvent(err));
        }
    }


    @step
    public function the_dht_startup_completes() {
        dhtMockup().notifyDhtStarted();
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_build_and_start_a_dht() {
        builderMockup.verifyDhtBuilt();
        dhtMockup().verifyDhtStarted();
    }

    @step
    public function the_listener_should_be_called() {
        listener.assertWasCalled();
    }

    @step
    public function the_dht_should_have_been_started_only_once() {
        dhtMockup().verifyDhtStarted(times(1));
    }

    @step
    public function all_listeners_should_be_called() {
        for(i in 0...startListeners.length){
            var l = startListeners[i];
            l.assertWasCalled('Listener ${i} was not called');
        }
    }

    // -------------------------

    function dhtMockup() {
        return builderMockup.dht();
    }
}