package discovery.test.tests.impl.js;

import discovery.base.search.Stoppable;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.exceptions.DhtGetException;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.dht.impl.js.dhtrpc.externs.QueryData;
import discovery.dht.exceptions.DhtPutException;
import js.lib.Error;
import haxe.Exception;
import discovery.test.helpers.RandomGen;
import discovery.test.generators.DrpcRandomGenerator;
import discovery.dht.impl.js.dhtrpc.DrpcConstants;
import discovery.dht.impl.js.dhtrpc.DrpcNode;
import discovery.dht.Dht;
import discovery.test.mockups.DhtRpcMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.dht.impl.js.dhtrpc.DhtParseTools;
using discovery.test.matchers.CommonMatchers;


class DrpcNodeUnitTest implements Scenario{
    @steps
    var steps: DrpcNodeSteps;

    @Test
    @scenario
    public function create_command(){
        given().a_dht_node();
        when().starting_the_node();
        then().the_storage_command_should_be_created();
    }


    @Test
    @scenario
    public function listen_on_address(){
        given().a_dht_node();
        given().some_bind_address();
        when().starting_the_node();
        then().should_start_listening_at_that_address();
    }


    @Test
    @scenario
    public function listen_on_default_address(){
        given().a_dht_node();
        given().no_bind_address();
        given().no_bootstrap_info();
        when().starting_the_node();
        then().should_start_listening_at_default_address();
    }


    // put ------------------------------------------------

    @Test
    @scenario
    public function put_value(){
        given().a_started_node();
        given().some_hash_key();
        and().a_binary_value();
        when().putting_that_value();
        then().the_storage_command_update_should_be_called_with_given_key_and_value();
    }


    @Test
    @scenario
    public function put_error(){
        given().a_value_was_put_into_dht();
        when().the_operation_fails();
        then().callback_should_receive_an_put_exception();
    }

    // query ------------------------------------------------

    @Test
    @scenario
    public function start_query(){
        given().a_started_node();
        given().some_hash_key();
        when().querying_for_the_given_key();
        then().a_query_should_be_sent_to_the_given_key();
    }


    @Test
    @scenario
    public function query_receive_one_data_value(){
        given().a_query_was_started();
        when().a_data_value_is_received();
        then().the_listener_should_receive_that_value();
    }


    @Test
    @scenario
    public function query_receive_some_data_values(){
        given().a_query_was_started();
        when().a_data_event_with_some_values_is_received();
        then().the_listener_should_receive_those_values();
    }

    @Test
    @scenario
    public function query_end(){
        given().a_query_was_started();
        when().the_query_ends();
        then().the_listener_end_function_should_be_called();
    }

    @Test
    @scenario
    public function query_fails(){
        given().a_query_was_started();
        when().the_query_ends_with_error();
        then().the_listener_end_function_should_be_called_with_an_exception();
    }


    @Test
    @scenario
    public function query_should_return_a_valid_stoppable(){
        given().a_started_node();
        when().starting_some_query();
        then().a_valid_stoppable_should_be_returned();
    }
}

class DrpcNodeSteps extends Steps<DrpcNodeSteps>{
    var dhtRpc:DhtRpcMockup;

    var dhtNode:Dht;
    var dhtKey:DhtKey;
    var dhtValue:DhtValue;
    var bindAddress:PeerAddress;

    var receivedQueryData:QueryData;
    var expectedValues:Array<DhtValue>;
    var receivedValues: Array<DhtValue>;
    var queryEndCalled:Bool=false;
    var capturedException:Exception;

    var queryStoppable:Stoppable;

    var dhtGenerator:DrpcRandomGenerator;

    public function new(){
        super();

        dhtRpc = new DhtRpcMockup();
        dhtGenerator = new DrpcRandomGenerator();

        expectedValues = [];
        receivedValues = [];
    }

    // summary steps ------------------------------------------

    @step
    public function a_value_was_put_into_dht() {
        given().a_started_node();
        given().some_hash_key();
        and().a_binary_value();
        when().putting_that_value();
    }

    @step
    public function a_query_was_started() {
        given().a_started_node();
        given().some_hash_key();
        when().querying_for_the_given_key();
    }

    @step
    public function a_started_node() {
        given().a_dht_node();
        when().starting_the_node();
    }

    @step
    public function starting_some_query() {
        given().some_hash_key();
        when().querying_for_the_given_key();
    }

    // given ---------------------------------------------------
    @step
    public function a_dht_node() {
        dhtNode = new DrpcNode({
            dhtBuilder: dhtRpc
        });
    }

    @step
    public function some_bind_address(){
        bindAddress = dhtGenerator.peerAddress();
    }

    @step
    public function no_bind_address() {
        bindAddress = null;
    }

    @step
    public function no_bootstrap_info() {}

    @step
    public function some_hash_key() {
        dhtKey = dhtGenerator.target().hxToBytes();
    }

    @step
    public function a_binary_value() {
        dhtValue = RandomGen.binary.bytes(RandomGen.primitives.int(3, 10));
    }

    // when  ---------------------------------------------------

    @step
    public function starting_the_node() {
        dhtNode.start({
            bind: bindAddress
        });
    }

    @step
    public function putting_that_value() {
        dhtNode.put(dhtKey, dhtValue, (?err:Exception)->{
            this.capturedException = err;
        });
    }

    @step
    public function querying_for_the_given_key() {
        queryStoppable = dhtNode.get(dhtKey, {
            onData: (data)->{
                if(data.values != null){
                    for(v in data.values){
                        receivedValues.push(v);
                    }
                }
            },
            onEnd: (?err)->{
                queryEndCalled = true;
                capturedException = err;
            }
        });
    }

    @step
    public function the_operation_fails() {
        var cmd = DrpcConstants.storageCommand;

        dhtRpc.failsLastUpdateWith(cmd, new Error("dht failed"));
    }

    @step
    public function a_data_value_is_received() {
        when().a_data_event_is_received_with_a_number_of_values(1);
    }


    @step
    public function a_data_event_with_some_values_is_received() {
        when().a_data_event_is_received_with_a_number_of_values(5);
    }

    @step
    function a_data_event_is_received_with_a_number_of_values(count: Int) {

        var cmd = DrpcConstants.storageCommand;

        var values:DrpcValue = [for (i in 0...count) dhtGenerator.dhtValue()];

        receivedQueryData = {
            node: dhtGenerator.peerNode(),
            value: values
        };

        expectedValues = values.toDhtValues();

        dhtRpc.queryReceiveData(cmd, receivedQueryData);
    }

    @step
    public function the_query_ends() {
        when().the_query_ends_with();
    }

    @step
    public function the_query_ends_with_error() {
        when().the_query_ends_with(new Error("query failed"));
    }

    @step
    public function the_query_ends_with(?err: Error) {
        dhtRpc.lastQueryEnded(DrpcConstants.storageCommand, err);
    }

    // then  ---------------------------------------------------

    @step
    public function the_storage_command_should_be_created() {
        final cmd = DrpcConstants.storageCommand;
        final cmdEncoding = DrpcConstants.storageCommandEncoding;

        dhtRpc.checkCommandCreated(cmd);
        var cmd = dhtRpc.getCommand(cmd);

        assertThat(cmd.query, is(notNullValue()),
            "Storage command: missing query" );

        assertThat(cmd.update, is(notNullValue()),
            "Storage command: missing update" );

        assertThat(cmd.valueEncoding, is(equalTo(cmdEncoding)),
            "Storage command value encoding does not match expected"
        );
    }

    @step
    public function the_storage_command_update_should_be_called_with_given_key_and_value() {
        var cmdArgs = dhtRpc.updateCalledFor(DrpcConstants.storageCommand);

        verifyCommandArgs(cmdArgs);
    }

    @step
    public function callback_should_receive_an_put_exception() {
        assertThat(capturedException, isA(DhtPutException));
    }

    @step
    public function a_query_should_be_sent_to_the_given_key() {
        var cmdArgs = dhtRpc.queryCalledFor(DrpcConstants.storageCommand);

        verifyCommandArgs(cmdArgs);
    }

    @step
    public function the_listener_should_receive_that_value() {
        then().the_listener_should_receive_those_values();
    }

    @step
    public function the_listener_should_receive_those_values() {
        var matchers = expectedValues.map((v)->equalTo(v));

        assertThat(receivedValues, containsInAnyOrder(matchers),
            'Expected values ${expectedValues} was not received in ${receivedValues}'
        );
    }

    @step
    public function the_listener_end_function_should_be_called() {
        assertThat(queryEndCalled, is(true), "Listener end was not called");
    }

    @step
    public
    function the_listener_end_function_should_be_called_with_an_exception() {
        then().the_listener_end_function_should_be_called();

        assertThat(capturedException, isA(DhtGetException),
            "Expected exception was not received");
    }

    @step
    public function should_start_listening_at_that_address(){
        then().should_start_listening_at(bindAddress);
    }

    @step
    public function should_start_listening_at_default_address() {
        then().should_start_listening_at({});
    }

    @step
    function should_start_listening_at(addr:PeerAddress){
        dhtRpc.checkListenCalledWith(addr);
    }

    @step
    public function a_valid_stoppable_should_be_returned() {
        queryStoppable.shouldNotBeNull();
    }

    // -----------------------------------------

    function verifyCommandArgs(cmdArgs:DhtCommandArgs){
        var cmdValues = cmdArgs.value.toDhtValues();

        assertThat(cmdArgs.command, is(equalTo(DrpcConstants.storageCommand)),
            'Command name does not match'
        );
        assertThat(cmdArgs.target, is(equalTo(dhtKey.getHash())),
            'Command key does not match'
        );

        if(dhtValue != null){
            assertThat(cmdValues, not(isEmpty()),
                "Missing expected command value");
            assertThat(cmdValues[0].toBinary(), is(equalTo(dhtValue.toBinary())),
                'Command value does not match'
            );
        }
        else{
            assertThat(cmdValues, is(nullValue()), 'No value expected in command');
        }

        assertThat(cmdArgs.callback, is(notNullValue()),
            'Missing command callback'
        );
    }
}