package discovery.test.scenarios;

import discovery.keys.storage.PublicKeyData;
import hx.concurrent.atomic.AtomicBool;
import haxe.Exception;
import discovery.dht.dhttypes.building.DhtComponentsAdapter;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.test.helpers.RandomGen;
import discovery.dht.DhtKeyTransportBuilder;
import discovery.dht.DhtKeyTransport;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.dht.DhtBuilder;
import discovery.test.integration.DhtInfrastructureBuilder;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtKeyTransportTest extends AsyncTests implements Scenario{
    @steps
    var steps: DhtKeyTransportSteps;

    @AsyncTest
    @scenario
    public function publish_and_retrieve_key(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 500);

        given().some_dht_infrastructure(next(()->{
            given().a_publisher_pki_distributor();
            and().a_client_pki_distributor();
            given().some_public_key_data();
            when().the_publisher_publishes_that_key_data(next(()->{
                and().the_client_searches_for_the_key(next(()->{
                    then().the_key_should_be_retrieved_to_the_client();
                    done();
                }));
            }));
        }));
    }
}

class DhtKeyTransportSteps extends Steps<DhtKeyTransportSteps>{

    var dhtBuilder:DhtBuilder;
    var dhtComponents:DhtComponents;
    var dhtInfrastructureBuilder:DhtInfrastructureBuilder;
    var pkiBuilder:DhtKeyTransportBuilder;

    var keyPublisher:DhtKeyTransport;
    var keyClient:DhtKeyTransport;

    var publishedKey:PublicKeyData;
    var capturedPubKeyData:PublicKeyData;

    public function new(){
        super();

        dhtBuilder = new DrpcNodeBuilder();
        dhtComponents = new DhtComponentsAdapter(dhtBuilder);
        dhtInfrastructureBuilder = new DhtInfrastructureBuilder(dhtBuilder);
        pkiBuilder = new DhtKeyTransportBuilder(dhtComponents);
    }

    // given ---------------------------------------------------

    @step
    public function some_dht_infrastructure(next: NextCallback) {
        dhtInfrastructureBuilder.startBootstrapNode((?err)->{
            if(err == null){
                var bootstrapInfo = dhtInfrastructureBuilder.bootstrapInfo();

                pkiBuilder.configure(bootstrapInfo);
            }

            next(err);
        });
    }

    @step
    public function a_publisher_pki_distributor() {
        keyPublisher = pkiBuilder.buildKeyTransport();
    }

    @step
    public function a_client_pki_distributor() {
        keyClient = pkiBuilder.buildKeyTransport();
    }

    @step
    public function some_public_key_data() {
        publishedKey = RandomGen.crypto.publicKeyData();
    }

    // when  ---------------------------------------------------

    @step
    public function the_publisher_publishes_that_key_data(next: NextCallback) {
        keyPublisher.publishKey(publishedKey, next);
    }

    @step
    public function the_client_searches_for_the_key(next:NextCallback) {
        var nextCalled = new AtomicBool(false);

        function callNext(?err:Exception) {
            if(nextCalled.getAndSet(true) == true) {
                //already called
                return;
            }

            next(err);
        }

        keyClient.searchKey(publishedKey.fingerprint, {
            onKey: (result)->{
                capturedPubKeyData = result.found;

                callNext();
            },
            onEnd: callNext
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_key_should_be_retrieved_to_the_client() {
        capturedPubKeyData.shouldBeEqualsTo(publishedKey);
    }
}