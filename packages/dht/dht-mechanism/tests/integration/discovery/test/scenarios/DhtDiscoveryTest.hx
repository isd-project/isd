package discovery.test.scenarios;

import discovery.dht.DhtDiscoveryMechanismBuilder;
import discovery.dht.dhttypes.building.DhtComponentsAdapter;
import discovery.test.integration.DhtInfrastructureBuilder;
import discovery.domain.Identifier;
import discovery.domain.Search;
import discovery.domain.Discovered;
import haxe.Exception;
import hx.concurrent.atomic.AtomicBool;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.dht.dhttypes.DhtOptions;
import discovery.base.publication.PublicationControl;
import discovery.domain.PublishInfo;
import discovery.test.helpers.RandomGen;
import discovery.dht.DhtDiscoveryMechanism;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.dht.DhtBuilder;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DhtDiscoveryTest extends AsyncTests implements Scenario{
    @steps
    var steps: DhtDiscoverySteps;

    @AsyncTest
    @scenario
    public function publish_and_discovery_by_type(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 500);

        given().a_bootstrap_dht_node(next(()->{
        given().a_publisher_and_client_nodes();
        when().a_service_is_published(next(()->{
            and().a_client_search_for_that_service_type(next(()->{
                then().the_published_service_should_be_found();
                done();
            }));
        }));
        }));
    }

    @AsyncTest
    @scenario
    public function publish_and_locate_service(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 500);

        given().a_bootstrap_dht_node(next(()->{
        given().a_publisher_and_client_nodes();
        when().a_service_is_published(next(()->{
            and().a_client_search_for_that_service_identity(next(()->{
                then().the_published_service_should_be_found();
                done();
            }));
        }));
        }));
    }
}

class DhtDiscoverySteps extends Steps<DhtDiscoverySteps>{

    var dhtNodeBuilder:DhtBuilder;
    var promises:PromisableFactory;

    var mechanismBuilder:DhtDiscoveryMechanismBuilder;
    var client:DiscoveryMechanism;
    var publisher:DiscoveryMechanism;

    var publishedInfo:PublishInfo;
    var foundService:Discovered;
    var publication:PublicationControl;

    var infrastructureBuilder:DhtInfrastructureBuilder;

    public function new(){
        super();

        dhtNodeBuilder = new DrpcNodeBuilder();
        promises = new JsPromisableFactory();

        infrastructureBuilder = new DhtInfrastructureBuilder(dhtNodeBuilder);
        mechanismBuilder = new DhtDiscoveryMechanismBuilder({
            dhtComponents: ()->new DhtComponentsAdapter(dhtNodeBuilder),
            promises: promises
        });
    }

    function bootstrapOptions():Null<DhtOptions> {
        return infrastructureBuilder.bootstrapInfo();
    }

    // given ---------------------------------------------------

    @step
    public function a_bootstrap_dht_node(next: NextCallback) {
        infrastructureBuilder.startBootstrapNode((?err)->{
            if(err == null){
                mechanismBuilder.configure(bootstrapOptions());
            }

            next(err);
        });
    }

    @step
    public function a_publisher_and_client_nodes() {
        client = mechanismBuilder.build();
        publisher = mechanismBuilder.build();
    }

    // when  ---------------------------------------------------

    @step
    public function a_service_is_published(next:NextCallback) {
        publishedInfo = PublishInfo.make(RandomGen.fullAnnouncement());

        var calledNext = new AtomicBool(false);

        publication = publisher.publish(publishedInfo, {
            onPublish: () -> {
                var wasCalled = calledNext.getAndSet(true);
                if(!wasCalled){
                    next();
                }
            },
            onFinish: (?err) -> {
                var wasCalled = calledNext.getAndSet(true);
                if(!wasCalled){
                    if(err == null){
                        err = new Exception("Publication finished before succeed");
                    }
                    next(err);
                }
                else if(err != null){
                    trace('publication failed after calling next step.\n'
                        + '\t' + 'error: ${err.details()}');
                }
            }
        });
    }

    @step
    public function a_client_search_for_that_service_type(next:NextCallback) {
        var search = client.search({
            type: publishedServiceType()
        });

        doSearch(search, next);
    }


    @step
    public function
        a_client_search_for_that_service_identity(next:NextCallback)
    {
        var search = client.locate(publishedServiceIdentifier());

        doSearch(search, next);
    }

    function doSearch(search: Search, next: NextCallback) {
        search.onFound.listen((discovered)->{
            foundService = discovered;

            search.stop();
        });

        //TO-DO: detect failure on finish
        search.onceFinished.listen((evt)->{
            next();
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_published_service_should_be_found() {
        foundService.shouldNotBeNull('no service found');

        var expected = publishedInfo.announcement();
        foundService.announcement().shouldBeEqualsTo(expected,
            'Found service announcement should be equals to the expected');
    }

    // --------------------------------------------------------

    function publishedServiceType():String {
        publishedInfo.shouldNotBeNull('No published info!');

        return publishedInfo.description().type;
    }

    function publishedServiceIdentifier():Identifier {
        return publishedInfo.getIdentifier();
    }
}