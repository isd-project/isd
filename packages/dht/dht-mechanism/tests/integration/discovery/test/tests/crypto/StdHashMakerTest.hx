package discovery.test.tests.crypto;

import discovery.dht.crypto.StdSha256HashMaker;
import discovery.dht.crypto.HashMaker;
import discovery.format.Binary;
import discovery.keys.crypto.HashTypes;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class StdHashMakerTest implements Scenario{
    @steps
    var steps: StdHashMakerSteps;


    @Test
    public function sha256_string_hash(){
        var values:Array<Array<String>> = [
            ["", "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"],
            ["1", "6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b"],
            [" ", "36a9e7f1c95b82ffb99743e0c5c4ce95d83c9a430aac59f84ef3cbfab6145068"],
            ["4fb80a4d9aceca", "af93f896f3d72093fba557a70714c3329d7c542a3fc09ff96c7e6a63e632faf1"],
            ["@Xc7eV.uiWetDjz.[! 8~Id+l2|W.", "ba772d81e2edf6f51359997ec0c619a857ad1a81938bdf9a8986bb7035ed9990"],
        ];

        for(pair in values){
            test_sha256_hash(pair[0], pair[1]);
        }
    }

    @scenario
    public function test_sha256_hash(input:String, expectedHex:String) {
        given().an_std_sha256_hash_maker();
        when().generating_a_hash_from_string(input);
        then().this_hash_should_be_generated(Binary.fromHex(expectedHex));
    }
}

class StdHashMakerSteps extends Steps<StdHashMakerSteps>{
    var hashMaker:HashMaker;
    var hash:Binary;
    var input:String;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function an_std_sha256_hash_maker() {
        hashMaker = new StdSha256HashMaker();
    }

    // when  ---------------------------------------------------

    @step
    public function generating_a_hash_from_string(input:String) {
        this.input = input;
        hash = hashMaker.hashString(input);
    }

    // then  ---------------------------------------------------

    @step
    public function this_hash_should_be_generated(expected:Binary) {
        assertThat(hash, is(equalTo(expected)),
            'Generated hash from "${input}" does not match the expected value');
    }
}