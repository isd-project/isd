package discovery.test.tests.impl.js;

import discovery.async.Future;
import discovery.base.search.Stoppable;
import haxe.exceptions.NotImplementedException;
import haxe.Exception;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.dhttypes.publication.DhtPublication;
import haxe.Timer;
import discovery.utils.time.Duration;
import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.format.Binary;
import discovery.async.promise.Promisable;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.dht.dhttypes.DhtKey;
import discovery.test.helpers.RandomGen;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.dht.Dht;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.time.TimeUnitTools;
using discovery.utils.EqualsComparator;
using discovery.test.matchers.CommonMatchers;


class DrpcNodeTest extends AsyncTests implements Scenario{
    @steps
    var steps: DrpcNodeSteps;

#if (js && nodejs)
    @Before
    public function setup(){
        resetSteps();
    }

    @After
    public function teardown(){
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function bootstrap(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_bootstrapper_node();
        given().another_dht_node();
        when().bootstrapping_the_node(next(()->{
            then().the_dht_node_should_have_some_peer();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function put_and_query(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 500);

        given().a_client_and_a_publisher_nodes();
        given().some_infrastructure_nodes(); //to improve robustness
        //maybe: wait for bootstrap completes
        given().a_value_was_published_at_some_key(next(()->{
        when().querying_that_key(next(()->{
            then().the_published_value_should_be_found();
            done();
        }));
        }));
    }

    @AsyncTest
    @scenario
    public function put_and_query_object(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 500);

        given().a_client_and_a_publisher_nodes();
        given().some_infrastructure_nodes(); //to improve robustness
        given().an_object_was_published_at_some_key(next(()->{
        when().querying_that_key(next(()->{
            then().the_published_value_should_be_found();
            done();
        }));
        }));
    }


    @AsyncTest
    @scenario
    public function put_and_query_multiple_values(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 1500);

        given().a_dht_bootstrapper_node();
        given().some_publisher_nodes();
        and().a_client_node();
        given().each_publisher_node_publishs_at_a_given_key(next(()->{
            when().querying_that_key(next(()->{
                then().all_published_values_should_be_found();
                done();
            }));
        }));
    }


    @AsyncTest
    @scenario
    public function filter_expired_values(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 1500);

        given().an_expiration_time_of(50.millis());
        given().a_dht_bootstrapper_node();
        given().a_client_and_a_publisher_nodes();
        given().a_value_was_published_at_some_key(next(()->{
        and().after_a_time_of(51.millis(), next(()->{
            when().another_value_is_published_at_the_same_key(next(()->{
                when().querying_that_key(next(()->{
                    then().only_the_last_value_should_be_found();
                    done();
                }));
            }));
        }));
        }));
    }


    @AsyncTest
    @scenario
    public function
        publication_should_keep_resource_alive(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 1500);

        given().an_expiration_time_of(25.millis());
        given().a_dht_bootstrapper_node();
        given().a_client_and_a_publisher_nodes();
        given().a_dht_publication_was_created_for_some_key_and_value(next(()->{
        when().the_expiration_time_passes(next(()->{
            but().the_client_queries_the_key(next(()->{
                then().the_published_value_should_be_found();
                done();
            }));
        }));
        }));
    }

    @AsyncTest
    @scenario
    public function stop_query(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 500);

        given().a_client_and_a_publisher_nodes();
        given().some_infrastructure_nodes(); //to improve robustness
        when().start_querying_some_key(next(()->{
        and().stopping_the_query(next(()->{
            then().the_query_end_listener_should_be_called(done);
        }));
        }));
    }
#end
}

class DrpcNodeSteps extends Steps<DrpcNodeSteps>{
    var promisesFactory:PromisableFactory;

    var bootstrapNode:Dht;
    var clientNode:Dht;
    var publisherNodes:Array<Dht>;
    var dhtNodes:Array<Dht>;
    var dhtBuilder:DrpcNodeBuilder;
    var port:Int;

    var key:DhtKey;
    var valueToPublish:DhtValue;
    var expectedValues:Array<DhtValue>;
    var foundValues:Array<DhtValue>;
    var foundValuesSet:IMap<DhtValue, Bool>;

    var publication:DhtPublication;
    var expirationTime:Duration;

    var getKeyStoppable:Stoppable;
    var futureQueryEnd:Future<Null<Exception>>;

    public function new(){
        super();

        dhtBuilder = new DrpcNodeBuilder();
        promisesFactory = new JsPromisableFactory();

        dhtNodes = [];
        publisherNodes = [];

        expectedValues = [];
        foundValues = [];

        foundValuesSet = new ComparableMap(Reflect.compare);

        futureQueryEnd = new Future();
    }


    public function teardown() {
        stopNodes(dhtNodes);

        if(publication != null){
            publication.stop();
        }
    }

    // given ---------------------------------------------------
    @step
    public function a_dht_bootstrapper_node() {
        bootstrapNode = dhtBuilder.build();
        dhtNodes.push(bootstrapNode);

        port = RandomGen.int(10000, 50000);
        bootstrapNode.startListening(port);
    }

    @step
    public function another_dht_node() {
        clientNode = dhtBuilder.build();
        dhtNodes.push(clientNode);
    }

    @step
    public function a_client_and_a_publisher_nodes() {
        given().a_dht_bootstrapper_node();
        given().a_client_node();
        given().a_number_of_publisher_nodes(1);
    }

    @step
    public function a_client_node() {
        clientNode = bootstrapedNode();
    }

    @step
    public function some_publisher_nodes() {
        given().a_number_of_publisher_nodes(5);
    }

    @step
    function a_number_of_publisher_nodes(count: Int) {
        for(i in 0...count){
            publisherNodes.push(bootstrapedNode());
        }
    }


    @step
    public function some_infrastructure_nodes() {
        given().a_number_of_instrastructure_nodes(5);
    }

    @step
    function a_number_of_instrastructure_nodes(number:Int) {
        for(i in 0...number){
            bootstrapedNode();
        }
    }

    @step
    public function an_expiration_time_of(time:Duration) {
        this.expirationTime = time;
        this.dhtBuilder.configure({
            storageConfig: {
                expirationTime: {
                    defaultValue: time
                }
            }
        });
    }

    @step
    public function after_a_time_of(time:Duration, next:NextCallback) {
        when().a_time_of_x_has_passed(time, next);
    }


    @step
    public function a_value_was_published_at_some_key(next: NextCallback) {
        when().each_publisher_node_publishs_at_a_given_key(next);
    }

    @step
    public function
        each_publisher_node_publishs_at_a_given_key(next:NextCallback)
    {
        given().some_key();
        when().each_publishing_node_publishes_at_the_given_key(next);
    }

    @step
    public
    function another_value_is_published_at_the_same_key(next:NextCallback) {
        when().each_publisher_node_publishs_at_a_given_key(next);
    }

    @step
    public function an_object_was_published_at_some_key(next:NextCallback) {
        given().some_key();
        given().an_object_value();
        when().each_publishing_node_publishes_at_the_given_key(next);
    }

    @step
    function some_key() {
        key = "example";
    }

    @step
    function an_object_value(){
        valueToPublish = {
            text: "A",
            binary: Binary.fromHex("abcd"),
            integer: 123,
            array: [
                1, 2, 3
            ]
        };
    }

    @step
    function each_publishing_node_publishes_at_the_given_key(next:NextCallback) {
        var promises = [for(node in publisherNodes) publishAtNode(node, key, valueToPublish)];

        promisesFactory.mergeAll(promises).then((publishedValues)->{
            this.expectedValues = publishedValues.map((v:Any)->{
                return (v:DhtValue);
            });

            next();
        }, (err)->{
            next(err);
        });
    }


    // when  ---------------------------------------------------
    @step
    public function bootstrapping_the_node(next: NextCallback) {
        clientNode.bootstrap(
            [{ address: 'localhost', port: port}], {onStart:next});
    }



    @step
    public function
        a_dht_publication_was_created_for_some_key_and_value(next: NextCallback)
    {
        given().some_key();
        var value:DhtValue = RandomGen.primitives.name();

        expectedValues.push(value);

        publication = new DhtPublication(this.publisherNodes[0], key, value, {
            expireTime: expirationTime
        });
        publication.onPublish.listen((evt)->{
            next(evt.error);
        });
        publication.start();
    }

    @step
    public function the_expiration_time_passes(next: NextCallback) {
        when().a_time_of_x_has_passed(expirationTime + 1.millis(), next);
    }

    @step
    public function a_time_of_x_has_passed(time:Duration, next:NextCallback) {
        Timer.delay(()->{
            next();
        }, Math.round(time.time()));
    }

    @step
    public function the_client_queries_the_key(next: NextCallback) {
        when().querying_that_key(next);
    }

    @step
    public function start_querying_some_key(next: NextCallback) {
        key = RandomGen.primitives.name();

        when().querying_that_key(next);
    }

    @step
    public function querying_that_key(next: NextCallback) {
        var found = false;

        getKeyStoppable = clientNode.get(key, {
            onData: (data)->{
                onReceivedData(data);

                if(!found && foundValues.length >= expectedValues.length){
                    found = true;

                    next();
                }
            },
            onEnd: (?err)->{
                //FIXME: sometimes the test fails, because the query is finished before finding the result

                futureQueryEnd.notify(err);

                if(!found){
                    next(err);
                }
            }
        });
    }

    function onReceivedData(data: DhtDataEvent) {
        for(value in data.values){
            if(!foundValuesSet.exists(value)){
                foundValuesSet.set(value, true);

                foundValues.push(value);
            }
        }
    }

    @step
    public function stopping_the_query(next: NextCallback) {
        getKeyStoppable.shouldNotBeNull(
            'getKeyStoppable should not be null');

        getKeyStoppable.stop(()->next());
    }

    // then  ---------------------------------------------------

    @step
    public function the_dht_node_should_have_some_peer() {
        assertThat(clientNode.getPeers(), not(isEmpty()),
            'Dht node peers');
    }

    @step
    public function the_published_value_should_be_found() {
        then().all_published_values_should_be_found();
    }

    @step
    public function all_published_values_should_be_found() {
        expectedValues.sort(Reflect.compare);
        foundValues.sort(Reflect.compare);

        foundValues.shouldBeEqualsTo(expectedValues);
    }

    @step
    public function only_the_last_value_should_be_found() {
        assertThat(expectedValues, not(isEmpty()),
            "Should have at least one expected value");

        var lastValue = foundValues[expectedValues.length - 1];
        assertThat(foundValues, is(array(equalTo(lastValue))),
            "found values does not match");
    }

    @step
    public function the_query_end_listener_should_be_called(next: NextCallback)
    {
        futureQueryEnd.onResult((_, ?err)->{
            next();
        });
    }

    // ----------------------------------------------------------------

    function stopNodes(nodes:Array<Dht>) {
        for(node in nodes){
            node.stop();
        }
    }

    function bootstrapedNode():Dht {
        var dht = dhtBuilder.build();
        dhtNodes.push(dht);

        dht.bootstrap([{ address: 'localhost', port: port}]);

        return dht;
    }

    function publishAtNode(node: Dht, key: DhtKey, ?value: DhtValue)
        : Promisable<DhtValue>
    {
        if(value == null){
            value = RandomGen.primitives.name();
        }

        return promisesFactory.promise((resolve, reject)->{

            node.put(key, value, (?err)->{
                if(err == null){
                    resolve(value);
                }
                else{
                    reject(err);
                }
            });
        });
    }
}