package discovery.test.tests.impl.js;

import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.helpers.RandomGen;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpc;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DhtRpcTest extends AsyncTests implements Scenario{
    @steps
    var steps: DhtRpcSteps;

    #if (js && nodejs)
    @AsyncTest
    @scenario
    public function bootstrap(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_bootstrapper_node();
        when().bootstrapping_a_node(next(()->{
            then().the_dht_node_should_have_some_peer();
            done();
        }));
    }
#end
}

class DhtRpcSteps extends Steps<DhtRpcSteps>{
    var bootstrapNode:DhtRpc;
    var clientNode:DhtRpc;

    var port:Int;

    public function new(){
        super();
    }

    // given ---------------------------------------------------
    @step
    public function a_dht_bootstrapper_node() {
        bootstrapNode = DhtRpcModule.build();

        port = RandomGen.int(10000, 50000);
        bootstrapNode.listen(port);
    }

    @step
    public function another_dht_node() {
        clientNode = DhtRpcModule.build();
    }

    // when  ---------------------------------------------------
    @step
    public function bootstrapping_a_node(next: NextCallback) {
        clientNode = DhtRpcModule.build({
            bootstrap: ['localhost:${port}']
        });

        clientNode.ready(()->{
            next();
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_dht_node_should_have_some_peer() {
        assertThat(clientNode.getNodes(), not(isEmpty()),
            'Dht node peers');
    }

}