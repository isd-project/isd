package discovery.test.tests.impl.js;

import discovery.exceptions.IllegalArgumentException;
import haxe.Exception;
import discovery.dht.impl.js.dhtrpc.DrpcNode;
import discovery.dht.Dht;
import discovery.test.helpers.RandomGen;
import discovery.dht.dhttypes.PeerAddress;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import discovery.test.integration.tools.NextCallback;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DrpcNodeStartTest extends AsyncTests implements Scenario{
    @steps
    var steps: DrpcNodeStartSteps;

    @AsyncTest
    @scenario
    public function bind_to_specific_address(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_node();
        and().this_bind_address({
            address: '127.0.0.1',
            port: RandomGen.primitives.int(16000, 65000)
        });
        when().starting_the_dht_with_the_given_bind_address(next(()->{
            then().the_node_should_have_the_expected_bound_address();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function bind_to_empty_address(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_node();
        and().this_bind_address({});
        when().starting_the_dht_with_the_given_bind_address(next(()->{
            then().the_dht_should_have_bound_to_a_non_empty_address();
            done();
        }));
    }


    @AsyncTest
    @scenario
    public function bind_to_default(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_node();
        given().no_bind_address();
        given().no_bootstrap_info();
        when().starting_the_dht_with_the_given_bind_address(next(()->{
            then().the_dht_should_have_bound_to_a_non_empty_address();
            done();
        }));
    }


    @AsyncTest
    @scenario
    public function bind_to_localhost(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_node();
        given().this_bind_address({address: 'localhost'});
        when().starting_the_dht_with_the_given_bind_address(next(()->{
            then().the_dht_should_have_bound_to_a_non_empty_address();
            done();
        }));
    }


    @AsyncTest
    @scenario
    public function bind_to_invalid_address(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_dht_node();
        given().an_invalid_bind_address();
        when().starting_the_dht_with_the_given_bind_address(next(()->{
            then().the_start_listener_should_receive_an_illegal_argument_error();
            done();
        }));
    }

}

class DrpcNodeStartSteps extends Steps<DrpcNodeStartSteps>{

    var bindAddress:PeerAddress;

    var dht:Dht;

    var onStartError:Exception;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_dht_node() {
        dht = new DrpcNode();
    }

    @step
    public function no_bind_address() {
        given().this_bind_address(null);
    }

    @step
    public function an_invalid_bind_address() {
        given().this_bind_address({address: 'invalid'});
    }

    @step
    public function this_bind_address(addr: PeerAddress) {
        this.bindAddress = addr;
    }

    @step
    public function no_bootstrap_info() {}

    // when  ---------------------------------------------------

    @step
    public function starting_the_dht_with_the_given_bind_address(next:NextCallback)
    {
        dht.start({
            onStart: (?err)->{
                this.onStartError = err;
                next();
            },
            bind: bindAddress
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_node_should_have_the_expected_bound_address() {
        var addr = dht.boundAddress();

        addr.shouldNotBeNull('no bound address');

        if(bindAddress.port != 0){
            addr.port.shouldBeEqualsTo(bindAddress.port, 'port does not match');
        }
        if(bindAddress.address != null){
            addr.address.shouldBeEqualsTo(bindAddress.address,
                'address not match');
        }
    }

    @step
    public function the_dht_should_have_bound_to_a_non_empty_address() {
        var addr = dht.boundAddress();

        addr.shouldNotBeNull('no bound address');
        addr.address.shouldNotBeNull('no bound port');
        addr.port.shouldNotBeNull('no bound port');
    }

    @step
    public function the_start_listener_should_receive_an_illegal_argument_error() {
        onStartError.shouldBeAn(IllegalArgumentException);
    }
}