import massive.munit.TestSuite;

import discovery.test.tests.crypto.StdHashMakerTest;
import discovery.test.tests.impl.js.DhtRpcTest;
import discovery.test.tests.impl.js.DrpcNodeStartTest;
import discovery.test.tests.impl.js.DrpcNodeTest;
import discovery.test.scenarios.DhtKeyTransportTest;
import discovery.test.scenarios.DhtDiscoveryTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.tests.crypto.StdHashMakerTest);
		add(discovery.test.tests.impl.js.DhtRpcTest);
		add(discovery.test.tests.impl.js.DrpcNodeStartTest);
		add(discovery.test.tests.impl.js.DrpcNodeTest);
		add(discovery.test.scenarios.DhtKeyTransportTest);
		add(discovery.test.scenarios.DhtDiscoveryTest);
	}
}
