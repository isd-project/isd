package discovery.dht;

import discovery.async.promise.PromisableFactory;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.base.configuration.AbstractGetter;
import discovery.base.configuration.DiscoveryMechanismBuilder;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.DhtDiscoveryMechanism.DhtDiscoveryDeps;

typedef DhtMechanismBuilderDependencies = {
    dhtComponents: AbstractGetter<DhtComponents>,
    promises: PromisableFactory
}

private typedef Deps = DhtMechanismBuilderDependencies;

class DhtDiscoveryMechanismBuilder implements DiscoveryMechanismBuilder{
    var deps:DhtMechanismBuilderDependencies;
    var options:DhtOptions;
    var defaultOptions:DhtOptions;

    public function new(deps: Deps, ?defaultOptions:DhtOptions) {
        this.deps = deps;
        this.defaultOptions = defaultOptions;

        configure(defaultOptions);
    }

    public function configure(?dhtOptions:DhtOptions) {
        if(dhtOptions == null){
            dhtOptions = defaultOptions;
        }

        this.options = dhtOptions;
    }

    public function buildMechanism():DiscoveryMechanism {
        return build();
    }

    public function build():DhtDiscoveryMechanism {
        return new DhtDiscoveryMechanism(mechanismDeps());
    }

    function mechanismDeps():DhtDiscoveryDeps {
        var component = deps.dhtComponents();
        component.configure(options);

        return {
            dhtComponents: component,
            promises: deps.promises
        };
    }
}