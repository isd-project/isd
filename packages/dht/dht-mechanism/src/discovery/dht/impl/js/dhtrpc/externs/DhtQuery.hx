package discovery.dht.impl.js.dhtrpc.externs;

import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import js.node.Buffer;

typedef DhtQuery = {
    /** command name **/
    command: String,

    /** the node who sent the query/update **/
    node: PeerNode,

    /** the query/update target (32 byte target) **/
    target: Buffer,

    /** the query/update payload decoded with the inputEncoding **/
    value: DrpcValue
}