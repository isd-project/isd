package discovery.dht.impl.js.dhtrpc.externs;

interface DhtRpcBuilder {
    function build(?options: DhtRpcBuildOptions):DhtRpc;
}