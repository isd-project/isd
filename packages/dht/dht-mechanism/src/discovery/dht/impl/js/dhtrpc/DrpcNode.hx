package discovery.dht.impl.js.dhtrpc;

import js.node.stream.Readable.IReadable;
import discovery.base.search.Stoppable;
import haxe.ValueException;
import discovery.exceptions.IllegalArgumentException;
import discovery.async.Future;
import discovery.dht.Dht.DhtPutOptions;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage.StorageConfig;
import discovery.dht.crypto.StdSha256HashMaker;
import discovery.dht.crypto.HashMaker;
import discovery.dht.exceptions.DhtGetException;
import discovery.dht.impl.js.dhtrpc.externs.QueryData;
import discovery.dht.exceptions.DhtPutException;
import haxe.Exception;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import js.node.Buffer;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorageCommand;
import discovery.dht.impl.js.dhtrpc.externs.PeerNode;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpcBuildOptions;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpcDefaultBuilder;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpcBuilder;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.Dht.DhtGetListener;
import discovery.dht.dhttypes.DoneCallback;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtStartOptions;
import discovery.dht.dhttypes.DhtPeer;
import discovery.dht.impl.js.dhtrpc.externs.DhtRpc;

using discovery.dht.impl.js.dhtrpc.DhtParseTools;


private typedef ReqFunction = (command: String,
                            target: Buffer,
                            ?value: DrpcValue,
                            ?callback: QueryDoneCallback) -> IReadable;

typedef DrpcNodeOptions = {
    ?dhtBuilder: DhtRpcBuilder,
    ?buildOptions:DhtRpcBuildOptions,
    ?hashMaker: HashMaker,
    ?storageConfig: StorageConfig
};

class DrpcNode implements Dht{
    var dhtBuilder:DhtRpcBuilder;
    var dhtRpc:DhtRpc;

    var storageCommand:DrpcStorageCommand;
    var hashMaker:HashMaker;

    public function new(?options:DrpcNodeOptions) {
        options = if(options == null) {} else options;

        if(options.dhtBuilder == null){
            //TO-DO: handle buildOptions
            options.dhtBuilder = new DhtRpcDefaultBuilder();
        }

        if(options.hashMaker == null){
            options.hashMaker = new StdSha256HashMaker();
        }

        this.dhtBuilder = options.dhtBuilder;
        this.hashMaker = options.hashMaker;
        this.storageCommand = new DrpcStorageCommand();

        if(options.storageConfig != null){
            this.storageCommand.configure(options.storageConfig);
        }
    }

    public function getPeers():Array<DhtPeer> {
        if(dhtRpc == null) return []; //maybe should throw

        return [ for(node in dhtRpc.getNodes()) dhtNodeToPeer(node)];
    }


    public function boundAddress():PeerAddress {
        var addr = sockAddress();

        if(addr == null) return null;

        return {
            address: addr.address,
            port: addr.port
        };
    }

    function sockAddress() {
        if(dhtRpc == null) return null;

        try{
            return dhtRpc.address();
        }
        catch(e){
            return null;
        }
    }

    public function start(options:DhtStartOptions) {
        if(options == null) options = {};

        dhtRpc = dhtBuilder.build(parseStartOptions(options));

        registerCommands();

        listen(options.bind, (?err)->{
            onReady(options, err);
        });
    }

    function listen(bind:PeerAddress, done:DoneCallback) {
        if(bind == null){
            bind = {};
        }

        var futureResult:Future<Any> = new Future();
        futureResult.onResult((_, ?err)->{
            done(err);
        });

        if(dhtRpc.socket != null){
            dhtRpc.socket.on('error', (err:js.lib.Error)->{
                var errMsg = 'Could not bind to address: ${bind}';
                var cause   = new ValueException(err);
                var failure = new IllegalArgumentException(errMsg, cause);

                futureResult.notify(null,failure);
            });
        }

        //TO-DO: check if already is bound
        dhtRpc.listen(bind.port, bind.address, ()->{
            futureResult.notify(null);
        });
    }

    function onReady(options:DhtStartOptions, ?err:Exception) {
        if(err != null){
            if(options.onStart != null){
                options.onStart(err);
            }
            return;
        }

        dhtRpc.ready(()->{
            if(options.onStart != null){
                options.onStart();
            }
        });
    }

    function registerCommands() {
        dhtRpc.command(DrpcConstants.storageCommand, storageCommand.toCommand());
    }


    public function stop(?onStop:StopCallback) {
        dhtRpc.destroy();

        if(onStop != null){
            onStop();
        }
    }

    // -------------------------------------------------------------

    public function put(key:DhtKey, value:DhtValue, done:DoneCallback, ?opts:DhtPutOptions)
    {
        IllegalArgumentException.verify(key != null, 'key',
            'cannot put into dht using null key'
        );

        //TO-DO: pass expiration time from DhtPutOptions

        dhtRequest(dhtRpc.update, key, value, (?err)->{
            if(err != null){
                err = new DhtPutException("Put failed", err);
            }
            done(err);
        });
    }

    public function get(key:DhtKey, listener:DhtGetListener): Stoppable{
        var queryListener = new QueryListener(listener);

        var stream = dhtRequest(dhtRpc.query, key, null, queryListener.onEnd);

        stream.on('data', queryListener.onData);

        return new ReadableStop(stream);
    }

    function dhtRequest(
        reqFunction: ReqFunction, key: DhtKey, value:DhtValue,
        ?done:DoneCallback): IReadable
    {
        //TO-DO: check dht was started

        var cmd = DrpcConstants.storageCommand;
        var keyHash   = key.getHash(hashMaker);
        var keyBuffer = Buffer.hxFromBytes(keyHash);

        var value:DrpcValue = if(value == null) null
                              else value.toDrpcValue();

        return reqFunction(cmd, keyBuffer, value, (err, results)->{
            var exception:Exception = null;
            if(errorIsValid(err)){
                exception = new JsErrorException(err);
            }

            done(exception);
        });
    }

    function errorIsValid(error: js.lib.Error) {
        return error != null
            //Below error is set when stream is closed
            && error.message != 'Premature close';
    }

    // -------------------------------------------------------------

    function parseStartOptions(options:DhtStartOptions):DhtRpcBuildOptions {
        return {
            bootstrap: parseBootstrapAddresses(options.bootstrap),
        };
    }

    function parseBootstrapAddresses(peers:Array<PeerAddress>): Array<String> {
        if(peers == null) return null;

        return [for(p in peers) '${p.address}:${p.port}'];
    }

    function dhtNodeToPeer(node:PeerNode):DhtPeer {
        return {
            address: node.host,
            port: node.port
        };
    }
}

class QueryListener{
    var listener:DhtGetListener;

    public function new(listener: DhtGetListener) {
        this.listener = listener;
    }

    public function onData(data:QueryData){
        if(listener != null && listener.onData != null){
            listener.onData(data.toDhtData());
        }
    }

    public function onEnd(?err:Exception) {
        if(listener != null && listener.onEnd != null){
            listener.onEnd(embedException(err));
        }
    }

    function embedException(err:Null<Exception>):Exception {
        if(err == null) return null;

        return new DhtGetException(err.message, err);
    }
}