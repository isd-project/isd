package discovery.dht.impl.js.dhtrpc;

import haxe.DynamicAccess;
import js.node.Buffer;
import haxe.io.Bytes;
import discovery.utils.traversal.ObjectTransformation;
import discovery.format.exceptions.InvalidValueException;
import discovery.dht.Dht.DhtDataEvent;
import discovery.dht.impl.js.dhtrpc.externs.QueryData;
import discovery.format.Binary;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;

using discovery.utils.Cast;


class DhtParseTools {

    public static function toDhtData(data:QueryData):DhtDataEvent {
        return {
            values: toDhtValues(data.value)
        };
    }

    public static function toDhtValues(drpcValue:DrpcValue):Array<DhtValue> {
        if(drpcValue == null) return null;

        var values = if(drpcValue.isArray()) drpcValue.asArray()
                     else [drpcValue];

        return values.map((v)->toDhtValue(v));
    }

    public static function toDhtValue(drpcValue:DrpcValue):DhtValue {
        if(drpcValue == null) return null;

        if(drpcValue.isString()){
            return drpcValue.asString();
        }
        else if(drpcValue.isBuffer()){
            return Binary.fromBuffer(drpcValue.asBuffer());
        }
        else if(drpcValue.isObject()){
            var objFrom:Dynamic = drpcValue.asObject();
            var objTo = ObjectTransformation
                    .doTransformation(Dynamic, bufferToBinary)
                    .apply(copyObject(objFrom));

            return DhtValue.fromObject((objTo:Dynamic));
        }

        throw new InvalidValueException('Unsupported value: ${drpcValue}');
    }

    static function bufferToBinary(value: Dynamic) {
        var buf = DrpcValue.convertToBuffer(value);
        if(buf != null){
            value = buf;
        }

        if(value.isOfType(Buffer)){
            return Binary.fromBuffer(value.castIfIs(Buffer));
        }

        return value;
    }

    public static function toDrpcValue(dhtValue:DhtValue): DrpcValue {
        if(dhtValue == null) return null;

        if(dhtValue.isOfType(String)){
            return dhtValue.asString();
        }
        else if(dhtValue.isBinary()){
            return DrpcValue.fromBinary(dhtValue.toBinary());
        }
        else if(dhtValue.isObject()){
            var obj = ObjectTransformation
                    .doTransformation(Bytes, Buffer.hxFromBytes)
                    .transform(BinaryData, binaryToBuffer)
                    .apply(copyObject(dhtValue.asObject()))
                    ;

            return DrpcValue.fromObject((obj : Dynamic));
        }

        return null;
    }

    static function copyObject(obj: DynamicAccess<Dynamic>){
        return obj.copy();
    }

    static function binaryToBuffer(bin: Binary) {
        return Buffer.hxFromBytes(bin.toBytes());
    }
}