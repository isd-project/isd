package discovery.dht.impl.js.dhtrpc.externs;

typedef PeerNode = {
    id: Dynamic, //fix type?
    host: String,
    port: Int
}