package discovery.dht.impl.js.dhtrpc.externs.commands;

import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import js.lib.Error;


/**
    You should call the query/update callback with (err, value) where value will be encoded using the outputEncoding and returned to the node.
**/
typedef DhtResponseCallback = (?err:Error, ?value: DrpcValue)->Void;