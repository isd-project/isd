package discovery.dht.impl.js.dhtrpc.storage;

import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage.StorageConfig;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.format.Binary;

using discovery.utils.IteratorTools;


private typedef StorageValues = Array<StorageValue>;

class DefaultDrpcStorage implements DrpcStorage{
    var keyToValues:Map<String, StorageValuesList>;
    var config:StorageConfig;

    public function new() {
        keyToValues = new Map();
    }

    public function configure(options:StorageConfig) {
        if(options == null) return;

        this.config = options;

        for (key=>valuesList in keyToValues.keyValueIterator()){
            valuesList.configure(config);
        }
    }

    public function getConfig():StorageConfig {
        return config;
    }

    public function listKeyValues(key: Binary):Array<DrpcValue> {
        function toValue(v:StorageValue){
            return if(v != null) v.value else null;
        }

        return [for(v in keyValuesIter(key)) toValue(v)];
    }

    public function listValuesFor(key:Binary):Array<StorageValue> {
        return keyValuesIter(key).toArray();
    }

    function keyValuesIter(key:Binary) {
        return getValues(toKey(key)).list();
    }

    public function store(key: Binary, storageValue:StorageValue) {
        var keyHex = toKey(key);

        var svList = getValues(keyHex);
        var value = svList.add(storageValue);

        keyToValues.set(keyHex, svList);

        return value;
    }

    function getValues(key:String) {
        var values = keyToValues.get(key);

        if(values == null) return makeStorageValuesList();

        return values;
    }

    function toKey(key:Binary): String{
        return key.toHex();
    }

    function makeStorageValuesList() {
        var valuesList = new StorageValuesList();
        valuesList.configure(config);
        return valuesList;
    }
}