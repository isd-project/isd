package discovery.dht.impl.js.dhtrpc.externs;

enum abstract DhtValueEncoding(String) {
    var json = 'json';
    var utf8 = 'utf-8';
    var binary = 'binary';
}