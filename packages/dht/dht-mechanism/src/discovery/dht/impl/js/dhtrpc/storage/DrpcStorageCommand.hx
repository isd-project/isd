package discovery.dht.impl.js.dhtrpc.storage;

import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage.StorageConfig;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.dht.impl.js.dhtrpc.externs.DhtCommand;
import discovery.dht.impl.js.dhtrpc.externs.DhtQuery;
import discovery.dht.impl.js.dhtrpc.externs.commands.DhtResponseCallback;

class DrpcStorageCommand {
    var storage:DrpcStorage;

    public function new(?storage: DrpcStorage) {
        if(storage == null){
            storage = new DefaultDrpcStorage();
        }

        this.storage = storage;
    }

    public function configure(config:StorageConfig) {
        this.storage.configure(config);
    }

    public function query(query: DhtQuery, callback:DhtResponseCallback) {
        var values = storage.listKeyValues(query.target);
        var drpcValue:DrpcValue = values;

        callback(null, drpcValue);
    }

    public function update(query: DhtQuery, callback:DhtResponseCallback) {
        if(query.target != null && query.value != null){
            this.storage.store(query.target, {
                value: query.value
            });
        }

        callback();
    }

    public function toCommand():DhtCommand {
        return {
            query: this.query,
            update: this.update,
            valueEncoding: DrpcConstants.storageCommandEncoding
        };
    }
}