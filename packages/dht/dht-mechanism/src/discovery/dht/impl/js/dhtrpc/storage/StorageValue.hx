package discovery.dht.impl.js.dhtrpc.storage;

import discovery.utils.time.Duration;
import discovery.domain.Validity;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;


@:structInit
class StorageValue{
    public var value: DrpcValue;

    @:optional
    public var validity: Validity;

    public function expired(): Bool {
        return validity != null && !validity.isValid();
    }

    public function expirationTime(): Duration {
        if(validity == null) return null;

        return validity.duration();
    }

    public function clone(): StorageValue {
        return {
            value: value,
            validity: validity
        };
    }

    public function toString() {
        return '{
    value: ${Std.string(this.value)},
    expirationTime: ${expirationTime()},
    validity: ${validity}
}';
    }

    public function validTo():Date {
        if(this.validity == null) return null;

        return this.validity.validTo;
    }
}