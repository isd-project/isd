package discovery.dht.impl.js.dhtrpc.externs.commands;

import discovery.dht.impl.js.dhtrpc.externs.commands.DhtResponseCallback;

typedef OnDhtQuery = (query: DhtQuery, callback:DhtResponseCallback)->Void;