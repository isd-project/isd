package discovery.dht.impl.js.dhtrpc;

import discovery.dht.crypto.StdSha256HashMaker;
import discovery.dht.crypto.HashMaker;
import discovery.dht.impl.js.dhtrpc.DrpcNode.DrpcNodeOptions;

class DrpcNodeBuilder implements DhtBuilder{
    var options:DrpcNodeOptions;

    public function new(?options:DrpcNodeOptions) {
        this.options = options;
    }

    public function build():Dht {
        return new DrpcNode(this.options);
    }

    public function configure(options: DrpcNodeOptions) {
        this.options = options;
    }

    public function buildHashMaker():HashMaker {
        return new StdSha256HashMaker();
    }
}