package discovery.dht.impl.js.dhtrpc;

import js.lib.Error;
import haxe.Exception;

class JsErrorException extends Exception{
    public var error(default, null): Error;

    public function new(err: Error, ?previous:Exception) {
        super(err.message, previous);

        this.error = err;
    }
}