package discovery.dht.impl.js.dhtrpc;

import discovery.async.Future;
import js.node.stream.Readable.IReadable;
import discovery.base.search.Stoppable;

class ReadableStop implements Stoppable{
    var readable:IReadable;

    var closedFuture:Future<Any>;

    public function new(readable: IReadable) {
        this.readable = readable;

        closedFuture = new Future();

        readable.on('close', onClose);
    }

    function onClose() {
        closedFuture.notify(true);
    }

    public function stop(?callback:StopCallback) {
        this.readable.destroy();

        if(callback != null){
            closedFuture.onResult((_, ?err)->callback());
        }
    }
}