package discovery.dht.impl.js.dhtrpc.externs;

import discovery.dht.impl.js.dhtrpc.externs.commands.OnDhtQuery;
import discovery.dht.impl.js.dhtrpc.externs.DhtValueEncoding;
import js.lib.Error;
import haxe.Constraints.Function;


typedef DhtCommand = {
    ?query: OnDhtQuery,
    ?update: OnDhtQuery,

    /**
        Optional value encoding for the query/update incoming value. Defaults to binary.
    **/
    ?inputEncoding: DhtValueEncoding,

    /**
        Optional value encoding for the query/update outgoing value. Defaults to binary.
    **/
    ?outputEncoding: DhtValueEncoding,

    /**
        sets both input/output encoding to this value
    **/
    ?valueEncoding: DhtValueEncoding,
}