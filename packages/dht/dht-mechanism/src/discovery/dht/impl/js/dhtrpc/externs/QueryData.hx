package discovery.dht.impl.js.dhtrpc.externs;

import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.dht.impl.js.dhtrpc.externs.PeerNode;

typedef QueryData = {
    ?type: Dynamic,
    node: PeerNode,
    value: DrpcValue
};