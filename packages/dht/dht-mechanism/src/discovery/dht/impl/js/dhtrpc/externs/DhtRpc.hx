package discovery.dht.impl.js.dhtrpc.externs;

import js.node.stream.Readable.IReadable;
import js.node.net.Socket.SocketAdress;
import js.lib.Error;
import discovery.dht.impl.js.dhtrpc.externs.QueryData;
import js.node.Buffer;
import js.node.Stream;
import haxe.Constraints.Function;
import js.node.events.EventEmitter;




@:jsRequire('dht-rpc')
extern class DhtRpcModule{
    @:selfCall
    public static function build(?options: DhtRpcBuildOptions):DhtRpc;
}

typedef QueryDoneCallback = (Error, Array<QueryData>)->Void;

interface DhtRpc extends IEventEmitter{
    public function listen(?port:Int, ?address:String, ?callback:()->Void):Void;
    public function ready(listener:()->Void): Void;

    public var socket(default, null):js.node.dgram.Socket;
    public function address(): SocketAdress;

    public function destroy():Void;

    public function getNodes(): Array<PeerNode>;

    public function command(name: String, options: DhtCommand): Void;

    public function query(command: String,
                            target: Buffer,
                            ?value: DrpcValue,
                            ?callback: QueryDoneCallback): IReadable;

    public function update(command: String,
                            target: Buffer,
                            ?value: DrpcValue,
                            ?callback: QueryDoneCallback): IReadable;
}