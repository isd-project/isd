package discovery.dht.impl.js.dhtrpc.externs;

typedef UdpSocket = js.node.dgram.Socket;

typedef DhtRpcBuildOptions = {
    ?ephemeral: Bool,
    ?bootstrap: Array<String>,
    ?socket: UdpSocket
};