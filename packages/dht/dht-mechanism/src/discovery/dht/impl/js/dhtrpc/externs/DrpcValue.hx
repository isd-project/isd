package discovery.dht.impl.js.dhtrpc.externs;

import haxe.io.Bytes;
import discovery.format.Binary;
import haxe.Json;
import js.lib.Object;
import js.node.Buffer;
using discovery.utils.Cast;


abstract DrpcValue(Dynamic)
    from Buffer
    from String
    from Array<DrpcValue>
{
    private function new (value: Dynamic) {
        this = value;
    }

    @:from
    public static function fromObjectArray<T:{}>(array: Array<T>): DrpcValue{
        var valueArray:Array<DrpcValue> = cast array;
        return valueArray;
    }

    @:from
    inline public static function fromObject<T:{}>(obj: T): DrpcValue{
        return new DrpcValue(obj);
    }

    @:from
    inline public static function fromBytes(bytes: Bytes): DrpcValue{
        return new DrpcValue(Buffer.hxFromBytes(bytes));
    }

    @:from
    inline public static function fromBinary(binary: Binary): DrpcValue{
        return fromBytes(binary.toBytes());
    }

    public function isString():Bool { return asString() != null;}

    inline
    public function isBuffer():Bool { return asBuffer() != null;}
    public function isObject():Bool { return asObject() != null;}
    inline
    public function isArray ():Bool {
        return asArray() != null && !isBuffer();
    }

    public inline function asBuffer(): Buffer{
        if(this == null) return null;

        var buf = convertToBuffer(this);

        //assign this to buffer to optimize next calls
        // (maybe should not change itself, to avoid unexpected effects?)
        if(buf != null && buf != this){
            this = buf;
        }

        return buf;
    }

    public static function convertToBuffer(obj:Dynamic): Buffer{
        if(obj == null)          return null;
        if(obj.isOfType(Buffer)) return obj;

        if(obj.type == 'Buffer'){//serialized buffer
            return Buffer.from(obj);
        }

        return null;
    }

    public function asString(): String{
        return this.castIfIs(String);
    }

    public function asObject(): Object{
        return this.castIfIs(Object);
    }

    public function asArray(): Array<DrpcValue> {
        return this.castIfIs(Array);
    }

    public function asArrayOf<T:{}>(objClass:Class<T>) {
        return this.castIfIs(Array);
    }

    public inline function compare(other: DrpcValue): Int {
        return doCompare(this, other);
    }

    static function doCompare(v1:DrpcValue, v2:DrpcValue) {

        if(v1.isBuffer() && v2.isBuffer()){
            return v1.asBuffer().compare(v2.asBuffer());
        }
        if(v1.isString() && v2.isString()){
            return Reflect.compare(v1.asString(), v2.asString());
        }
        if(v1.isArray() && v2.isArray()){
            return compareArrays(v1.asArray(), v2.asArray());
        }
        if(v1.isObject() && v2.isObject()){
            return compareObjects(v1, v2);
        }

        return Reflect.compare(v1, v2);
    }

    public inline function toString(): String {
        if(isString()) return asString();
        if(isBuffer()) return asBuffer().toString('hex');
        if(isObject()) return Json.stringify(asObject());
        if(isArray())  return asArray().toString();

        return Std.string(this);
    }

    static function compareObjects(obj1:Dynamic, obj2:Dynamic)
    {
        //hack just to simplify
       var json1 = Json.stringify(obj1);
       var json2 = Json.stringify(obj2);

       return Reflect.compare(json1, json2);
    }

    static function compareArrays(a1:Array<DrpcValue>, a2:Array<DrpcValue>)
    {
        //optmization
        if(a1.length != a2.length){
            final cmp = Reflect.compare(a1.length, a2.length);

            return cmp;
        }


        for(i in 0...a1.length){
            var v1:DrpcValue = a1[i];
            var v2:DrpcValue = a2[i];
            var cmp = v1.compare(v2);

            if(cmp != 0){
                return cmp;
            }
        }

        return 0;
    }
}