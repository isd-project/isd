package discovery.dht.impl.js.dhtrpc.storage;

import discovery.utils.time.Duration;
import discovery.dht.impl.js.dhtrpc.externs.DrpcValue;
import discovery.format.Binary;

typedef StorageConfig = {
    ?expirationTime:{
        ?defaultValue:Duration,
        ?maxValue:Duration
    }
};

interface DrpcStorage {
    function configure(options:StorageConfig):Void;
    function getConfig(): StorageConfig;

    function listKeyValues(key:Binary): Array<DrpcValue>;
    function listValuesFor(key:Binary): Array<StorageValue>;
    function store(key:Binary, storageValue: StorageValue): StorageValue;
}