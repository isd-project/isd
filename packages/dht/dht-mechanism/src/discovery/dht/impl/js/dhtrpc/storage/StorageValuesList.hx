package discovery.dht.impl.js.dhtrpc.storage;

import discovery.utils.collections.ComparableSet;
import discovery.dht.impl.js.dhtrpc.storage.DrpcStorage.StorageConfig;
import discovery.utils.collections.RemoveIfIterator;
import discovery.domain.Validity;
import discovery.dht.impl.js.dhtrpc.storage.StorageValue;
import discovery.utils.time.Duration;

using discovery.utils.time.TimeUnitTools;


@:using(discovery.utils.IteratorTools)
class StorageValuesList {
    public static final DEFAULT_EXPIRATION = 10.minutes();

    @:isVar
    public var defaultExpirationTime(default, set):Duration;

    @:isVar
    public var maxExpirationTime(get, default):Duration;

    var valueSet:ComparableSet<StorageValue>;

    public function new(
        ?defaultExpirationTime: Duration,
        ?maxExpirationTime:Duration)
    {
        if(defaultExpirationTime == null){
            defaultExpirationTime = DEFAULT_EXPIRATION;
        }

        this.valueSet = new ComparableSet(compareValues);
        this.defaultExpirationTime = defaultExpirationTime;
        this.maxExpirationTime = maxExpirationTime;
    }

    function set_defaultExpirationTime(value: Duration): Duration {
        if(value == null){
            value = DEFAULT_EXPIRATION;
        }

        return this.defaultExpirationTime = value;
    }

    function get_maxExpirationTime():Duration {
        if(this.maxExpirationTime == null) return this.defaultExpirationTime;
        return this.maxExpirationTime;
    }

    function compareValues(v1:StorageValue, v2: StorageValue): Int{
        if(v1 == v2 || v1.value == v2.value) return 0;
        if(v1 == null || v1.value == null) return 1;
        if(v2 == null || v2.value == null) return -1;

        return v1.value.compare(v2.value);
    }

    // -----------------------------------------------------------

    public function configure(config:StorageConfig) {
        if(config == null) return;
        if(config.expirationTime == null) return;

        this.set_defaultExpirationTime(config.expirationTime.defaultValue);
        this.maxExpirationTime     = config.expirationTime.maxValue;
    }

    public function add(value:StorageValue): StorageValue{
        if(value == null)   return null;
        if(value.expired()) return null;

        var addedValue = updateValue(value.clone());

        valueSet.set(addedValue);

        return addedValue;
    }

    public function list(): Iterator<StorageValue>{
        var iter = valueSet.values();

        function valueExpired(value:StorageValue) {
            return value == null || value.expired();
        }

        return new RemoveIfIterator(iter, this, valueExpired);
    }

    public function remove(value: StorageValue): Bool {
        return valueSet.remove(value);
    }

    public function isEmpty():Bool {
        return valueSet.isEmpty();
    }

    public function clear() {
        valueSet.clear();
    }

    function updateValue(value:StorageValue):StorageValue {
        var previous = valueSet.get(value);

        value.validity = updateValidity(value, previous);

        return value;
    }

    function updateValidity(value:StorageValue, previous:StorageValue):Validity {
        var validity = value.validity;

        if(validity == null){
            validity = Validity.validFor(defaultExpirationTime);
        }
        if(validity.duration() > maxExpirationTime){
            validity = Validity.validFor(maxExpirationTime);
        }

        if(previous != null
            && previous.validity != null
            && previous.validity.validTo.getTime() > validity.validTo.getTime())
        {
            return previous.validity;
        }

        return validity;
    }
}