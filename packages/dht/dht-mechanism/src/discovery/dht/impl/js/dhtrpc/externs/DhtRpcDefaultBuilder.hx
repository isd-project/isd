package discovery.dht.impl.js.dhtrpc.externs;

import discovery.dht.impl.js.dhtrpc.externs.DhtRpc.DhtRpcModule;

class DhtRpcDefaultBuilder implements DhtRpcBuilder{
    public function new() {}

    public function build(?options:DhtRpcBuildOptions):DhtRpc {
        return DhtRpcModule.build(options);
    }
}