package discovery.dht.impl.js.dhtrpc;

import discovery.dht.impl.js.dhtrpc.externs.DhtValueEncoding;

class DrpcConstants {
    public static final storageCommand = "drpc.storage";
    public static final storageCommandEncoding = DhtValueEncoding.json;
}