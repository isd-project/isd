package discovery.dht;

import discovery.base.search.CompositeStoppable;
import discovery.dht.crypto.HashMaker;

class DisposableDhtBuilder implements DhtBuilder{
    var delegate:DhtBuilder;

    var builtDhts:Array<Dht> = [];

    public function new(builder: DhtBuilder) {
        this.delegate = builder;
    }

    public function closeAll(?callback:()->Void){
        var compositeStop = new CompositeStoppable(resetDhts());

        compositeStop.stop(callback);
    }

    public function build():Dht {
        var dht = delegate.build();

        builtDhts.push(dht);

        return dht;
    }

    function resetDhts() {
        var old = builtDhts;
        builtDhts = [];

        return old;
    }

    public function buildHashMaker():HashMaker {
        return delegate.buildHashMaker();
    }
}