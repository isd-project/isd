package discovery.dht;

import discovery.dht.crypto.HashMaker;

interface DhtBuilder{
    function build(): Dht;
    function buildHashMaker():HashMaker;
}