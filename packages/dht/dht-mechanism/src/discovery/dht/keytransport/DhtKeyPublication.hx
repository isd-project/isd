package discovery.dht.keytransport;

import discovery.dht.keytransport.DhtKeyComponents;
import haxe.Exception;
import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;


class DhtKeyPublication {
    var components:DhtKeyComponents;
    var done:DoneCallback;

    var pubKeyData:PublicKeyData;

    public function new(components: DhtKeyComponents, onDone:DoneCallback)
    {
        this.components = components;
        this.done = if(onDone != null) onDone else (?err)->{};
    }

    public function start(pubKeyData:PublicKeyData) {
        this.pubKeyData = pubKeyData;

        components.withDht(doPublishKey);
    }

    function doPublishKey(dht:Dht, ?err:Exception) {
        if(err != null){
            notifyPublishEnd(err);
            return;
        }

        var key   = components.fingerprintDhtKey(pubKeyData.fingerprint);
        var value = components.pubKeySerializer().serialize(pubKeyData);

        dht.put(key, value, done);
    }

    function notifyPublishEnd(?err:Exception) {
        if(done != null){
            done(err);
        }
    }
}