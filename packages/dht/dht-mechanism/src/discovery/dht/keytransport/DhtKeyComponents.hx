package discovery.dht.keytransport;

import discovery.keys.serialization.PublicKeyDataSerializer;
import discovery.keys.storage.PublicKeyData;
import discovery.utils.dependencies.MissingDependencyException;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.dhttypes.DhtKey;
import discovery.domain.serialization.HaxeSerializer;
import discovery.keys.crypto.Fingerprint;
import discovery.dht.DhtGetter.GetDhtCallback;
import discovery.domain.serialization.Serializer;
import discovery.dht.keytransport.DhtKeyMessages;
import discovery.keys.storage.KeyData;


typedef DhtKeyDependencies = {
    dhtComponents: DhtComponents,
    ?pubKeySerializer:Serializer<PublicKeyData>
};

class DhtKeyComponents {
    var dhtGetter:DhtGetter;
    var keyMessages:DhtKeyMessages;
    var publicKeySerializer:Serializer<PublicKeyData>;

    public function new(pkiDeps: DhtKeyDependencies) {
        final components = pkiDeps.dhtComponents;
        final getter = components.dhtGetter();
        final hashMaker = components.hashMaker();

        MissingDependencyException.requireValue(getter, 'DhtGetter');
        MissingDependencyException.requireValue(hashMaker, 'HashMaker');

        this.dhtGetter = getter;
        this.keyMessages = new DhtKeyMessages(hashMaker);
        publicKeySerializer = makePubKeySerializer(pkiDeps.pubKeySerializer);
    }

    function makePubKeySerializer(serializer:Null<Serializer<PublicKeyData>>) {
        if(serializer != null){
            return serializer;
        }

        var keySerializer = new PublicKeyDataSerializer();
        return new HaxeSerializer(keySerializer);
    }

    public function withDht(callback:GetDhtCallback) {
        this.dhtGetter.withDht(callback);
    }

    public function messages() {
        return keyMessages;
    }

    public function fingerprintDhtKey(keyFingerprint:Fingerprint): DhtKey {
        return keyMessages.fingerprintKey(keyFingerprint);
    }

    public function pubKeySerializer() {
        return publicKeySerializer;
    }
}