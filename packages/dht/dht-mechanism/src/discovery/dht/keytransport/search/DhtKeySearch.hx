package discovery.dht.keytransport.search;

import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.base.search.Stoppable;
import discovery.base.search.StopOnce;
import discovery.dht.keytransport.DhtKeyComponents;
import discovery.dht.dhttypes.DhtValue;
import discovery.base.search.Stoppable.StopCallback;
import discovery.dht.Dht.DhtDataEvent;
import haxe.Exception;
import discovery.keys.crypto.Fingerprint;


class DhtKeySearch implements Stoppable{
    var components:DhtKeyComponents;
    var listeners:SearchKeyListeners;

    var keyFingerprint:Fingerprint;

    var stopControl:StopOnce;
    var queryStop:Stoppable;

    public function new(pkiComponents: DhtKeyComponents, cb:SearchKeyListeners)
    {
        this.components = pkiComponents;
        this.listeners = fillListenersWithDefaults(cb);

        this.stopControl = new StopOnce(doStop);
    }

    public function start(fing:Fingerprint) {
        this.keyFingerprint = fing;

        components.withDht(doSearchKey);
    }

    function doSearchKey(dht:Dht, ?err:Exception)
    {
        if(err != null){
            notifySearchEnd(err);
            return;
        }

        var key = components.fingerprintDhtKey(keyFingerprint);

        if(stopWasCalled()) return;

        queryStop = dht.get(key, {
            onData: onGetKeyResponse,
            onEnd: listeners.onEnd
        });
    }

    function onGetKeyResponse(dataEvt:DhtDataEvent) {
        if(listeners == null || listeners.onKey == null) return;

        for(value in dataEvt.values){
            onReceivedKey(value);
        }
    }

    function onReceivedKey(value:DhtValue) {
        if(stopWasCalled()) return;

        var pubKeyData = components.pubKeySerializer().deserialize(value);

        if(pubKeyData != null){
            listeners.onKey({
                found: pubKeyData
            });
        }
    }

    public function stop(?callback: StopCallback) {
        this.stopControl.stop(callback);
    }

    function doStop(?callback: StopCallback) {
        if(queryStop != null){
            queryStop.stop(callback);
        }
        else if(callback != null){
            //search was not started yet, so we confirm the stop directly
            callback();

            notifySearchEnd();
        }
    }

    function stopWasCalled() {
        return this.stopControl.stopWasCalled();
    }

    function notifySearchEnd(?err:Exception) {
        if(listeners == null || listeners.onEnd == null) return;

        listeners.onEnd(err);
    }

    function fillListenersWithDefaults(cb:SearchKeyListeners):SearchKeyListeners
    {
        var onKey = if(cb != null && cb.onKey != null) cb.onKey
                    else (key)->{};
        var onEnd = if(cb != null && cb.onEnd != null) cb.onEnd
                    else (?err)->{};

        return {
            onKey: onKey,
            onEnd: onEnd
        };
    }
}