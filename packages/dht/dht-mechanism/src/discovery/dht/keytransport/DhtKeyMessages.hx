package discovery.dht.keytransport;

import discovery.dht.crypto.HashMaker;
import discovery.dht.dhttypes.DhtKey;
import discovery.keys.crypto.Fingerprint;

class DhtKeyMessages {
    var hashMaker:HashMaker;

    public function new(hashMaker:HashMaker) {
        this.hashMaker = hashMaker;
    }

    public function fingerprintKey(fing:Fingerprint):DhtKey{
        return hashMaker.hashBinary(fing.hash);
    }
}