package discovery.dht;

import discovery.keys.Keychain;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.keys.storage.PublicKeyData;
import discovery.dht.keytransport.search.DhtKeySearch;
import discovery.dht.keytransport.DhtKeyPublication;
import discovery.dht.keytransport.DhtKeyComponents;
import discovery.keys.crypto.Fingerprint;
import discovery.async.DoneCallback;


class DhtKeyTransport implements KeyTransport{
    var keyComponents:DhtKeyComponents;

    public function new(deps: DhtKeyDependencies) {
        this.keyComponents = new DhtKeyComponents(deps);
    }

    public function publishKey(pubKeyData: PublicKeyData, ?done:DoneCallback) {
        //TO-DO: keep refreshing publication
        var publication = new DhtKeyPublication(keyComponents, done);

        publication.start(pubKeyData);
    }

    public
    function searchKey(fing:Fingerprint, cb:SearchKeyListeners):Stoppable {
        var search = new DhtKeySearch(keyComponents, cb);

        search.start(fing);

        return search;
    }

    public function publishKeychain(keychain:Keychain, ?done:DoneCallback)
    {
        //for now, the dht does not support registering keychains
    }
}


