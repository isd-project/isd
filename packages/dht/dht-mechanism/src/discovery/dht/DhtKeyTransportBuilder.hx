package discovery.dht;

import discovery.base.configuration.Configurable;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.dht.dhttypes.DhtOptions;
import discovery.dht.dhttypes.building.DhtComponents;

class DhtKeyTransportBuilder
    implements KeyTransportBuilder
    implements Configurable<DhtOptions>
{
    var dhtComponents:DhtComponents;

    public function new(dhtComponents: DhtComponents) {
        this.dhtComponents = dhtComponents;
    }

    public function configure(dhtOptions:DhtOptions) {
        dhtComponents.dhtGetter().configure(dhtOptions);
    }

    public function buildKeyTransport():DhtKeyTransport {
        var keyTransport = new DhtKeyTransport({
            dhtComponents: dhtComponents
        });

        return keyTransport;
    }
}