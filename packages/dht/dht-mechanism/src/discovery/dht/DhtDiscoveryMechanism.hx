package discovery.dht;

import discovery.utils.dependencies.DependenciesVerifications;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.dhttypes.search.IdentifierKeyBuilder;
import discovery.dht.dhttypes.search.SearchKeyBuilder;
import discovery.dht.dhttypes.search.QueryKeyBuilder;
import discovery.dht.dhttypes.search.DhtDiscoverySearch;
import discovery.dht.discoverymechanism.DhtDiscoveryPublisher;
import discovery.async.promise.PromisableFactory;
import discovery.dht.discoverymechanism.DefaultDhtKeyMaker;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.domain.Identifier;
import discovery.domain.PublishInfo;
import discovery.domain.PublishListeners;
import discovery.base.publication.PublicationControl;

typedef DhtDiscoveryDeps = {
    dhtComponents: DhtComponents,
    promises: PromisableFactory
};

class DhtDiscoveryMechanism implements DiscoveryMechanism {
    var dhtGetter:DhtGetter;
    var keyMaker:DhtKeyMaker;
    var promises:PromisableFactory;

    public function new(deps: DhtDiscoveryDeps) {
        DependenciesVerifications.requireAll(deps);

        var hashMaker = deps.dhtComponents.hashMaker();

        this.keyMaker  = new DefaultDhtKeyMaker(hashMaker);
        this.dhtGetter = deps.dhtComponents.dhtGetter();
        this.promises  = deps.promises;
    }

    public function finish(callback:() -> Void) {}

    public function search(query:Query):Search {
        var keyBuilder = new QueryKeyBuilder(query, keyMaker);

        return doSearch(keyBuilder);
    }

    public function locate(id:Identifier):Search {
        return doSearch(new IdentifierKeyBuilder(id, keyMaker));
    }

    function doSearch(keyBuilder:SearchKeyBuilder):Search {
        var search = new DhtDiscoverySearch({
            dhtGetter: dhtGetter,
            keyBuilder: keyBuilder
        });

        return search.start();
    }


    public function publish(info:PublishInfo, ?listener:PublishListeners):PublicationControl {

        var publisher = makePublisher(info, listener);

        dhtGetter.withDht((dht, ?err)->{
            publisher.start(dht);
        });

        return publisher;
    }

    function makePublisher(info:PublishInfo, ?listener:PublishListeners) {
        return new DhtDiscoveryPublisher({
            publishInfo: info,
            listener: listener,
            deps: {
                keyMaker: keyMaker,
                promises: promises
            }
        });
    }
}
