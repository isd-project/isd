package discovery.dht;

import discovery.base.search.Stoppable;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtKey;
import discovery.dht.dhttypes.DoneCallback;
import discovery.dht.dhttypes.DhtPeer;
import discovery.dht.dhttypes.DhtStartOptions;


typedef DhtDataEvent = {
    ?values: Array<DhtValue>
};

typedef DhtGetListener = {
    ?onData: (DhtDataEvent)->Void,
    ?onEnd: DoneCallback
};

typedef StopCallback = discovery.base.search.Stoppable.StopCallback;

typedef DhtPutOptions = {
    ?expireTimeMs:Int
};

@:using(discovery.dht.dhttypes.DhtTools)
interface Dht extends Stoppable{
    function start(options: DhtStartOptions): Void;
    function stop(?onStop:StopCallback):Void;

    function getPeers():Array<DhtPeer>;

    function put(key:DhtKey, value:DhtValue, done:DoneCallback, ?opt: DhtPutOptions): Void;
    function get(key:DhtKey, listener:DhtGetListener): Stoppable;

    function boundAddress(): PeerAddress;
}