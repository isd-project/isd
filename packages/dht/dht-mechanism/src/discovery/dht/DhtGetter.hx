package discovery.dht;

import discovery.dht.dhttypes.DhtOptions;
import haxe.Exception;

typedef GetDhtCallback = (Dht, ?Exception)->Void;

@:using(discovery.dht.DhtGetter.DhtGetterTools)
interface DhtGetter {
    function configure(dhtOptions:DhtOptions):Void;

    function getDht(callback:GetDhtCallback):Void;
}

class DhtGetterTools {
    public static function withDht(getter:DhtGetter, callback:GetDhtCallback) {
        getter.getDht(callback);
    }
}