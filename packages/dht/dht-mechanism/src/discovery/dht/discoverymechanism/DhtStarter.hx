package discovery.dht.discoverymechanism;

import discovery.dht.dhttypes.DoneCallback;


interface DhtStarter {

    /**
        Starts the DHT if not started yet.

        @param onStart is called when the DHT starts, or right way if already started.
    **/
    public function starts(?onStart:DoneCallback):Void;

    /** Register a listener for onStart and starts the Dht, if needed. **/
    public function onceStarted(onStart:DoneCallback):Void;

    /**
        Returns the dht if already created/started or null, otherwise.
    **/
    public function dht():Null<Dht>;
}