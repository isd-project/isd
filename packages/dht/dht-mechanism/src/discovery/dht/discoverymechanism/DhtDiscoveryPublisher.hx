package discovery.dht.discoverymechanism;

import discovery.domain.serialization.Serializer;
import discovery.domain.serialization.AnnouncementSerializer;
import discovery.domain.serialization.SignedSerializer;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.Announcement;
import haxe.Exception;
import hx.concurrent.atomic.AtomicBool;
import discovery.base.publication.PublicationControl;
import discovery.async.promise.Promisable;
import discovery.dht.dhttypes.DhtValue;
import discovery.dht.dhttypes.DhtKey;
import discovery.async.promise.PromisableFactory;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.domain.PublishInfo;
import discovery.domain.PublishListeners;


typedef DhtDiscoveryPublisherArgs = {
    publishInfo: PublishInfo,
    ?listener: PublishListeners,

    ?dht: Dht,

    deps: {
        keyMaker: DhtKeyMaker,
        promises: PromisableFactory
    }
};

class DhtDiscoveryPublisher implements PublicationControl{
    var data: DhtDiscoveryPublisherArgs;

    var finished:AtomicBool;

    var announceSerializer:Serializer<Signed<Announcement>>;

    public function new(args: DhtDiscoveryPublisherArgs) {
        this.data = args;
        this.finished = new AtomicBool(false);

        if(this.data.listener == null){
            this.data.listener = {};
        }

        var builder = new DhtSerializerBuilder();
        announceSerializer = builder.buildSerializer();
    }

    public function start(dht:Dht) {
        this.data.dht = dht;

        doPublication(data.publishInfo, data.listener);
    }

    function doPublication(info:PublishInfo, ?listeners:PublishListeners) {
        var desc        = info.description();
        var identifier  = info.getIdentifier();
        var identityKey = keyMaker().keyFromIdentifier(identifier);
        var srvTypeKey  = keyMaker().serviceTypeKey(desc.type);

        var announce = serializeAnnounce(info.signed());

        var putPromises = [dhtPut(srvTypeKey, announce)];

        if(identityKey != null){
            putPromises.push(dhtPut(identityKey, announce));
        }

        promises().mergeAll(putPromises).then((_)->{
            published();
        }, (err)->{
            finish(err);
        });
    }

    function dhtPut(key:DhtKey, data:DhtValue): Promisable<Any>
    {
        return promises().promise((resolve, reject)->{
            dht().put(key, data, (?err)->{
                if(err == null){
                    resolve(null);
                }
                else{
                    reject(err);
                }
            });
        });
    }

    public function stop(?callback:() -> Void) {
        finish();

        if(callback != null){
            callback();
        }
    }

    //attributes ------------------------------------------------------

    inline function dht():Dht {
        return data.dht;
    }

    inline function promises():PromisableFactory{
        return data.deps.promises;
    }

    inline function keyMaker() {
        return data.deps.keyMaker;
    }

    function listener(): PublishListeners{
        return data.listener;
    }


    function published() {
        if(listener() != null
            && listener().onPublish != null
            && !finished.value)
        {
            listener().onPublish();
        }
    }

    function finish(?err: Exception) {
        if(finished.getAndSet(true)){//returns the previous value
            //already finished
            return;
        }

        if(listener() != null && listener().onFinish != null){

            listener().onFinish(err);
        }
    }

    function serializeAnnounce(announce:Signed<Announcement>) {
        return announceSerializer.serialize(announce);
    }
}