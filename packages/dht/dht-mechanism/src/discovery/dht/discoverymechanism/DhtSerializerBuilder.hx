package discovery.dht.discoverymechanism;

import discovery.domain.serialization.AnnouncementSerializer;
import discovery.domain.serialization.SignedSerializer;
import discovery.domain.serialization.HaxeSerializer;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.serialization.Serializer;

class DhtSerializerBuilder {
    public function new() {

    }

    public function buildSerializer(): Serializer<Signed<Announcement>> {
        return new HaxeSerializer(
                new SignedSerializer(
                    new AnnouncementSerializer()));
    }
}