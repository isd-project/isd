package discovery.dht.discoverymechanism;

import discovery.domain.Identifier;
import discovery.dht.crypto.HashMaker;
import discovery.dht.dhttypes.DhtKey;

class DefaultDhtKeyMaker implements DhtKeyMaker {
    var hashMaker:HashMaker;

    public function new(hashMaker: HashMaker) {
        this.hashMaker = hashMaker;
    }

    public function keyFromIdentifier(identifier:Identifier):DhtKey {
        if(identifier == null) return null;

        var url = identifier.toUrl(Base16);

        if(url == null)
            return null;
        else
            return hashMaker.hashString(url);
    }

    public function serviceTypeKey(srvType:String):DhtKey {
        return hashMaker.hashString('servicetype=${srvType}');
    }
}