package discovery.dht.discoverymechanism;

import discovery.domain.Identifier;
import discovery.dht.dhttypes.DhtKey;

interface DhtKeyMaker {
    function keyFromIdentifier(identifier:Identifier): DhtKey;
    function serviceTypeKey(type:String): DhtKey;
}