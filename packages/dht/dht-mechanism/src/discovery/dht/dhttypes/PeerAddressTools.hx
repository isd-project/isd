package discovery.dht.dhttypes;

class PeerAddressTools {
    public static function toPeerAddress(str:String) {
        if(str == null) return null;

        return PeerAddress.fromString(str);
    }

    public static function toPeerAddressArray(addresses: Array<String>) {
        if(addresses == null) return null;

        return addresses.map((a)->toPeerAddress(a));
    }
}