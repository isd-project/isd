package discovery.dht.dhttypes.startup;


import hx.concurrent.atomic.AtomicBool;
import discovery.dht.DhtGetter.GetDhtCallback;
import discovery.dht.discoverymechanism.DhtStarter;
import discovery.async.Future;
import discovery.dht.dhttypes.DoneCallback;
import discovery.dht.dhttypes.DhtOptions;


class DefaultDhtStarter  implements DhtStarter
                         implements DhtGetter
{
    var _dht:Dht;

    var dhtBuilder:DhtBuilder;
    var dhtOptions:DhtOptions;
    var defaultOptions:DhtOptions;
    var startFuture:Future<Dht>;
    var started:AtomicBool;

    public function new(dhtOptions: DhtStarterOptions)
    {
        if(dhtOptions.defaultOptions == null){
            dhtOptions.defaultOptions = {};
        }

        this.dhtBuilder     = dhtOptions.dhtBuilder;
        this.defaultOptions = dhtOptions.defaultOptions;
        this.dhtOptions     = defaultOptions;

        startFuture = new Future();
        started = new AtomicBool(false);
    }

    public function dht():Null<Dht> {
        return _dht;
    }

    public function getDht(callback:GetDhtCallback) {
        onceStarted((?err)->{
            if(callback != null){
                callback(dht(), err);
            }
        });
    }

    public function configure(dhtOptions:DhtOptions) {
        setStartOptions(dhtOptions);
    }

    public function setStartOptions(options: DhtOptions) {
        if(options == null) options = defaultOptions;

        this.dhtOptions = options;
    }

    public function onceStarted(onStart:DoneCallback) {
        starts(onStart);
    }

    public function starts(?onStart:DoneCallback) {
        if(onStart != null){
            startFuture.onResult((_, ?err)->{
                onStart(err);
            });
        }

        if(!started.getAndSet(true)){
            _dht = dhtBuilder.build();

            _dht.start({
                bind: dhtOptions.bind,
                bootstrap: dhtOptions.bootstrap,
                onStart: (?err)->{
                    startFuture.notify(_dht, err);
                }
            });
        }
    }
}