package discovery.dht.dhttypes.startup;

import discovery.dht.DhtBuilder;

typedef DhtStarterOptions = {
    dhtBuilder: DhtBuilder,
    ?defaultOptions: DhtOptions
};