package discovery.dht.dhttypes.publication;

import discovery.utils.time.DefaultTimerLoop;
import discovery.utils.Cancellable;
import discovery.utils.time.Duration;
import discovery.utils.time.TimerLoop;
import discovery.utils.events.EventEmitter;
import haxe.Exception;
import discovery.utils.events.EventEmitterFactory;
import discovery.domain.Event;
import discovery.dht.dhttypes.DhtValue;

typedef DhtPublishedEvent = {
    ?error: Exception
};


typedef DhtPublicationOptions = {
    ?eventFactory:EventEmitterFactory,
    ?timer: TimerLoop,
    ?expireTime:Duration
};

class DhtPublication {
    final DEFAULT_EXPIRE_TIME = Duration.of(10, Minutes).time();


    public var onPublish(default, null):Event<DhtPublishedEvent>;
    public var onRepublish(default, null):Event<DhtPublishedEvent>;

    var dht:Dht;
    var key:DhtKey;
    var value:DhtValue;

    var eventsFactory:EventEmitterFactory;
    var onPublishEmitter:EventEmitter<DhtPublishedEvent>;
    var onRepublishEmitter:EventEmitter<DhtPublishedEvent>;

    var timer: TimerLoop;
    var expireTime:Duration;

    var published:Bool=false;
    var repeatCancellable:Cancellable;

    public function new(dht: Dht, key: DhtKey, value: DhtValue,
        ?options:DhtPublicationOptions)
    {
        if(options == null) options = {};

        this.dht = dht;
        this.key = key;
        this.value = value;

        if(options.eventFactory == null){
            options.eventFactory = new discovery.impl.Defaults.EventEmitterFactory();
        }
        this.eventsFactory = options.eventFactory;

        this.timer      = makeTimer(options.timer);
        this.expireTime = options.expireTime;

        this.onPublish = onPublishEmitter = eventsFactory.eventEmitter();
        this.onRepublish = onRepublishEmitter = eventsFactory.eventEmitter();
    }

    public function start() {
        publish();

        this.repeatCancellable = timer.repeat(onTimeout, refreshTime());
    }


    public function stop() {
        if(repeatCancellable != null){
            repeatCancellable.cancel();
        }
    }

    // -------------------------------------------

    function makeTimer(?timer:TimerLoop):TimerLoop {
        if(timer != null) return timer;

        return new DefaultTimerLoop();
    }

    function onTimeout(){
        publish();
    }

    function publish() {
        dht.put(key, value, onPublished, {
            expireTimeMs: expireTimeMs()
        });
    }

    function onPublished(?err: Exception){
        var evt:DhtPublishedEvent = {
            error: err
        };

        if(!published){
            published = true;

            this.onPublishEmitter.notify(evt);
        }
        else{
            this.onRepublishEmitter.notify(evt);
        }

    }

    function expireTimeMs(): Int{
        if(expireTime != null){
            return expireTime.timeInt();
        }

        return Math.round(DEFAULT_EXPIRE_TIME);
    }

    function refreshTime(): Int {
        return Math.round(expireTimeMs()/2);
    }
}