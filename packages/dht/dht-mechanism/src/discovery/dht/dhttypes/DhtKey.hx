package discovery.dht.dhttypes;

import discovery.dht.crypto.HashMaker;
import discovery.format.exceptions.InvalidValueException;
import haxe.extern.EitherType;
import haxe.io.Bytes;
import discovery.format.Binary;

using discovery.utils.Cast;


private typedef StringOrBinary = EitherType<String, Binary>;

@:structInit
class HashText {
    var data(default, default):StringOrBinary;

    public function new(data: EitherType<String, BinaryData>) {
        this.data = data;
    }

    public function getHash(?hashMaker: HashMaker): Binary {
        if(data == null) return null;
        if(isBinary()) return binaryData();

        if(hashMaker != null){
           return hashMaker.hashString(this.data);
        }

        throw new InvalidValueException("Data is not a valid hash");
    }

    function isBinary() {
        return binaryData() != null;
    }

    function binaryData():Binary {
        return data.castIfIs(BinaryData);
    }
}

@:forward.new
@:transitive
@:forward
abstract DhtKey(HashText) from HashText to HashText{

    @:from
    public static function fromText(text: String): DhtKey{
        return new HashText(text);
    }

    @:from
    public static function fromBinary(binary: Binary): DhtKey{
        return new HashText(binary);
    }

    @:from
    public static function fromBytes(bytes: Bytes): DhtKey{
        return fromBinary(bytes);
    }
}