package discovery.dht.dhttypes.search;

import discovery.domain.Identifier;
import discovery.dht.discoverymechanism.DhtKeyMaker;

class IdentifierKeyBuilder implements SearchKeyBuilder{
    var identifier:Identifier;
    var keyMaker:DhtKeyMaker;

    public function new(identifier:Identifier, keyMaker: DhtKeyMaker) {
        this.identifier = identifier;
        this.keyMaker = keyMaker;
    }

    public function buildKey():DhtKey {
        return keyMaker.keyFromIdentifier(identifier);
    }
}