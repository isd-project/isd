package discovery.dht.dhttypes.search;

interface SearchKeyBuilder {
    public function buildKey(): DhtKey;
}