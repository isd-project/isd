package discovery.dht.dhttypes.search;

import haxe.Exception;
import discovery.dht.discoverymechanism.DhtSerializerBuilder;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.serialization.Serializer;
import discovery.dht.Dht.DhtDataEvent;
import discovery.domain.Announcement;
import discovery.domain.Discovered;
import discovery.base.search.SearchBase;
import discovery.base.search.SearchControl;
import discovery.domain.Search;


typedef DhtDiscoverySearchArgs = {
    dhtGetter:DhtGetter,
    keyBuilder: SearchKeyBuilder
};

class DhtDiscoverySearch implements SearchControl{

    var dhtGetter:DhtGetter;

    var keyBuilder:SearchKeyBuilder;

    var search:SearchBase;

    var deserializer:Serializer<Signed<Announcement>>;

    public function new(args:DhtDiscoverySearchArgs) {
        this.dhtGetter  = args.dhtGetter;
        this.keyBuilder = args.keyBuilder;

        this.search = new SearchBase(this);

        var builder = new DhtSerializerBuilder();
        deserializer = builder.buildSerializer();
    }

    public function stop(?callback:() -> Void) {
        if(callback != null){
            callback();
        }
    }

    public function start():Search {
        dhtGetter.withDht((dht, ?err)->{
            this.getKey(dht);
        });

        return search;
    }


    function getKey(dht: Dht) {
        var key = keyBuilder.buildKey();

        dht.get(key, {
            onData: (dataEvt)->{
                onReceivedData(dataEvt);
            },
            onEnd: (?err)->{
                onQueryFinished(err);
            }
        });
    }

    function onReceivedData(dataEvt: DhtDataEvent){
        for(value in dataEvt.values){
            var obj = value.asObject();

            var announce = deserializer.deserialize(obj);
            if(announce != null){
                this.discovered(announce);
            }
        }
    }

    function onQueryFinished(?err:Exception) {
        search.finished({
            error: err
        });
    }

    function discovered(disc:Discovered) {
        search.discovered(disc);
    }
}


