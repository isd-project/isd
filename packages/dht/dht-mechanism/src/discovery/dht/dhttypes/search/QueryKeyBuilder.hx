package discovery.dht.dhttypes.search;

import discovery.dht.dhttypes.DhtKey;
import discovery.dht.discoverymechanism.DhtKeyMaker;
import discovery.domain.Query;


class QueryKeyBuilder implements SearchKeyBuilder {
    var query: Query;
    var keyMaker:DhtKeyMaker;

    public function new(query: Query, keyMaker: DhtKeyMaker) {
        this.query = query;
        this.keyMaker = keyMaker;
    }

    public function buildKey(): DhtKey {
        var type = query.type; //TO-DO: throw if type is null (?)

        return this.keyMaker.serviceTypeKey(type);
    }
}