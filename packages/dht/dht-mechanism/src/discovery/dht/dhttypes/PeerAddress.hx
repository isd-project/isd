package discovery.dht.dhttypes;

using StringTools;

@:structInit
class PeerAddress{
    @:optional
    public var address: String;

    @:optional
    public var port: Int;

    public static function fromString(addrString: String): PeerAddress {
        var ipv6 = '\\[([\\w\\d\\:]+)\\]';
        var ipv4OrName = '[\\w\\d\\.]+';
        var port = '\\d+';

        var addrPattern = '((${ipv4OrName})|(${ipv6}))?(\\:(${port}))?';
        var addrRegex = new EReg(addrPattern, "g");

        if(addrRegex.match(addrString)){
            var portStr = addrRegex.matched(6);
            var port = if(portStr != null) Std.parseInt(portStr) else null;

            var addr = addrRegex.matched(2);
            if(addr == null){
                addr = addrRegex.matched(4);
            }

            return {
                address: addr,
                port: port
            };
        }

        return null;
    }

    public function toString():String {
        var addr = addressToString();

        if(port == null) return addr;
        if(addr == null) addr = '';

        return '${addr}:$port';
    }


    function addressToString():String {
        if(address == null) return null;

        if(address.contains(':')){
            return '[${address}]';
        }

        return address;
    }
}
