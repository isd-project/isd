package discovery.dht.dhttypes;

import discovery.dht.Dht;


class DhtTools {

    static public function
        startListening(dht: Dht, port:Int, ?address:String, ?options: DhtStartOptions): Void
    {
        options = if(options == null) {} else options;
        options.bind = {address: address, port: port};

        dht.start(options);
    }

    static public function
        bootstrap(dht: Dht, peers: Array<PeerAddress>, ?options:DhtStartOptions): Void
    {
        options = if(options == null) {} else options;
        options.bootstrap = peers;

        dht.start(options);
    }
}