package discovery.dht.dhttypes;

import discovery.dht.dhttypes.DhtOptions;

@:structInit
class DhtStartOptions extends DhtOptions{
    @:optional
    public var onStart: DoneCallback;
}