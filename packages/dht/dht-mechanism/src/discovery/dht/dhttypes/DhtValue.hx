package discovery.dht.dhttypes;

import discovery.format.parsers.JsonParser;
import discovery.domain.operators.basic.Value;
import haxe.io.Bytes;
import discovery.format.EncodedData;
import discovery.format.Binary;

using discovery.utils.Cast;


@:using(discovery.utils.Cast)
@:transitive
@:forward
@:forwardStatics
abstract DhtValue(Value)
    from String
    from Binary
    from Value to Value
{
    @:from
    public static function fromBytes(bytes: Bytes): DhtValue{
        return Value.fromBytes(bytes);
    }

    @:from
    inline public static function fromObject<T:{}>(obj: T): DhtValue {
        return Value.fromObject(obj);
    }


    public function isBinary() {
        return this.isOfType(BinaryData)
            || this.isOfType(TextWithFormat)
            || this.isOfType(Bytes);
    }

    @:to
    public function toBinary():Binary {
        if(this.isOfType(Bytes)){
            return Binary.fromBytes(this.castIfIs(Bytes));
        }
        if(this.isOfType(BinaryData)){
            return this;
        }
        if(this.isOfType(TextWithFormat)){
            return this.castIfIs(TextWithFormat).toBinary();
        }
        if(this.isOfType(String)){
            return Binary.fromText(this);
        }

        return null;
    }

    public function toString(): String {
        return new JsonParser().stringify(this, null, '  ');
    }
}