package discovery.dht.dhttypes;


@:structInit
class DhtOptions{
    @:optional
    public var bind: PeerAddress;

    @:optional
    public var bootstrap: Array<PeerAddress>;

    public static function copyFrom(other:DhtOptions) {
        if(other == null) return null;

        return {
            bind: other.bind,
            bootstrap: other.bootstrap
        };
    }
}