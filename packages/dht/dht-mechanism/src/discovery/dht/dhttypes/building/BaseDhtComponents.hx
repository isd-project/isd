package discovery.dht.dhttypes.building;

import discovery.dht.crypto.HashMaker;

class BaseDhtComponents implements DhtComponents{
    var getter:DhtGetter;
    var dhtHashMaker:HashMaker;

    public function new(getter: DhtGetter, hashMaker:HashMaker) {
        this.getter = getter;
        this.dhtHashMaker = hashMaker;
    }

    public function dhtGetter():DhtGetter {
        return getter;
    }

    public function hashMaker():HashMaker {
        return dhtHashMaker;
    }
}