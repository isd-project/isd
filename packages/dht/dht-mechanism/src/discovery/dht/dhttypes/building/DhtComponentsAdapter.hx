package discovery.dht.dhttypes.building;

import discovery.dht.dhttypes.startup.DefaultDhtStarter;

class DhtComponentsAdapter extends BaseDhtComponents{

    public function new(dhtBuilder: DhtBuilder) {
        super(
            new DefaultDhtStarter({dhtBuilder: dhtBuilder}),
            dhtBuilder.buildHashMaker()
        );
    }
}