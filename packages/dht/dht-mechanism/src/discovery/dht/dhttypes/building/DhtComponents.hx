package discovery.dht.dhttypes.building;

import discovery.dht.crypto.HashMaker;

@:using(discovery.dht.dhttypes.building.DhtComponents.DhtComponentsTools)
interface DhtComponents {
    function dhtGetter():DhtGetter;
    function hashMaker():HashMaker;
}

class DhtComponentsTools {
    public static
    function configure(components:DhtComponents, options:DhtOptions)
    {
        components.dhtGetter().configure(options);
    }
}