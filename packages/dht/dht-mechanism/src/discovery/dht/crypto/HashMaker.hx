package discovery.dht.crypto;

import discovery.format.Binary;

interface HashMaker {
    function hashString(str:String): Binary;
    function hashBinary(binary:Binary): Binary;
}