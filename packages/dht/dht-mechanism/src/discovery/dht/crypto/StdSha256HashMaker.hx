package discovery.dht.crypto;

import haxe.crypto.Sha256;
import discovery.format.Binary;

class StdSha256HashMaker implements HashMaker{
    public function new() {

    }

    public function hashString(str:String):Binary {
        return hashBinary(Binary.fromText(str));
    }

    public function hashBinary(binary:Binary):Binary {
        return Sha256.make(binary);
    }
}