package discovery.dht.bootstrapper;

import discovery.cli.console.SysConsole;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;

using discovery.dht.dhttypes.PeerAddressTools;


class Main {
    static function main() {
        var bootstrapper = new Bootstrapper(new DrpcNodeBuilder());
        var console = new SysConsole();

        var cli = new BootstrapperCli(bootstrapper, console);

        var args = Sys.args();
        cli.start(args);
    }

    static function old_main() {

        var args = Sys.args();
        var bindAddress = if(args.length > 1) args[1] else null;

        var bootstrapper= new Bootstrapper(new DrpcNodeBuilder());
        bootstrapper.start({
            bind: bindAddress,

            onStart: (?err)->{
                var listeningAddress = bootstrapper.dht.boundAddress();

                Sys.println('bootstrap complete at ${listeningAddress}');
            }
        });
    }
}