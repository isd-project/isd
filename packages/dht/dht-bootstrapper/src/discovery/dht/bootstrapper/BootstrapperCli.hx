package discovery.dht.bootstrapper;

import haxe.Exception;
import discovery.cli.console.Console;
import tink.CoreApi;
import discovery.cli.tink.NonePrompt;

class BootstrapperCli {
    var bootstrapper:Bootstrapper;
    var console:Console;

    public function new(bootstrapper:Bootstrapper, console:Console) {
        this.bootstrapper = bootstrapper;
        this.console = console;
    }

    public function start(args:Array<String>) {
        var cli = new BootstrapperCliInterface(bootstrapper, console);

        var result = tink.Cli.process(args, cli, new NonePrompt());
        result.handle(function(outcome: Outcome<Noise, Error>){
            switch (outcome){
                case Failure(error): processError(error);
                default:
            }
        });
    }

    function processError(error:Error) {
        Sys.println(error);
    }
}

class BootstrapperCliInterface {
    var boostrapper:Bootstrapper;
    var console:Console;

    public function new(boostrapper:Bootstrapper, console:Console) {
        this.boostrapper = boostrapper;
        this.console = console;
    }

    @:flag('bind')
    public var bindAddress:String=null;


    /**
        Define one or more addresses to be used for bootstrap.
    **/
    @:flag('bootstrap')
    @:alias(false)
    public var bootstrapAddresses(default, default):Array<String>=null;

    @:defaultCommand
    public function start() {
        boostrapper.start({
            bind: bindAddress,
            bootstrap: bootstrapAddresses,
            onStart: onStart
        });
    }

    function onStart(?err: Exception) {
        if(err != null){
            console.printError('Failed to start due to error: ${err}');
            console.printError(err.details());
            return;
        }

        console.println('dht started');
        console.println('bound to address: ${boostrapper.dht.boundAddress()}');
        console.println('peers: ${boostrapper.dht.getPeers()}');
    }
}