package discovery.dht.bootstrapper;

import discovery.dht.dhttypes.DhtStartOptions;
import discovery.async.DoneCallback;

using discovery.dht.dhttypes.PeerAddressTools;


typedef BootstrapperStartOptions = {
    ?bind: String,
    ?bootstrap: Array<String>,
    ?onStart: DoneCallback
};

class Bootstrapper {
    var dhtBuilder:DhtBuilder;

    public var dht(default, null):Dht;

    public function new(dhtBuilder: DhtBuilder) {
        this.dhtBuilder = dhtBuilder;
    }

    public function start(?opt: BootstrapperStartOptions) {
        var dhtOptions = toDhtOptions(opt);

        dht = dhtBuilder.build();
        dht.start(dhtOptions);
    }

    function toDhtOptions(opt:Null<BootstrapperStartOptions>): DhtStartOptions {
        if(opt == null) return null;

        var dhtOptions:DhtStartOptions = {};

        dhtOptions.bind = opt.bind.toPeerAddress();
        dhtOptions.bootstrap = opt.bootstrap.toPeerAddressArray();
        dhtOptions.onStart = opt.onStart;

        return dhtOptions;
    }

    public function stop() {
        trace('stopping bootstrapper');

        if(dht == null) return;

        var _dht = dht;
        dht = null;

        _dht.stop(()->{
            trace('bootstrapper dht finished');
        });
    }
}