import massive.munit.TestSuite;

import discovery.dht.bootstrapper.tests.BootstrapperTest;
import discovery.dht.bootstrapper.tests.BootstrapperCliTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.dht.bootstrapper.tests.BootstrapperTest);
		add(discovery.dht.bootstrapper.tests.BootstrapperCliTest);
	}
}
