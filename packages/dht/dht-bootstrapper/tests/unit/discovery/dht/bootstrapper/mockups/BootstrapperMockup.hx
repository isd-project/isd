package discovery.dht.bootstrapper.mockups;

import mockatoo.Mock;
import discovery.test.mockups.DhtMockup;
import haxe.Exception;
import org.hamcrest.AssertionException;
import discovery.dht.bootstrapper.Bootstrapper.BootstrapperStartOptions;
import mockatoo.Mockatoo;

using discovery.utils.EqualsComparator;

class BootstrapperMockup {
    var bootstrapperMock:Bootstrapper;

    var dhtMockup:DhtMockup;
    var onStartCalls: Array<BootstrapperStartOptions> = [];

    public function new() {
        bootstrapperMock = Mockatoo.mock(Bootstrapper);
        dhtMockup = new DhtMockup();

        Mockatoo.when(bootstrapperMock.start(any)).thenCall(onStart);
        Mockatoo.when(bootstrapperMock.dht).thenReturn(dhtMockup.dht());
    }

    function onStart(args: Array<Dynamic>){
        onStartCalls.push(args[0]);
    }

    public function dht() {
        return dhtMockup;
    }

    public function bootstrapper() {
        return bootstrapperMock;
    }

    public function verifyStartedWith(expected:BootstrapperStartOptions) {
        Mockatoo.verify(bootstrapperMock.start(any));

        for(call in onStartCalls){
            if(matchOnStart(call, expected)){
                return true;
            }
        }

        throw new AssertionException(
            'No calls to "start" with expected arguments: ${expected}\n'
            + '\tLast calls were: ${onStartCalls}');
    }

    public function notifyStarted(?err:Exception) {
        if(onStartCalls.length == 0)
            throw new Exception('Bootstrapper was not started');

        for(call in onStartCalls){
            if(call.onStart != null){
                call.onStart();
            }
        }
    }


    function matchOnStart(
        call:BootstrapperStartOptions, expected:BootstrapperStartOptions): Bool
    {
        if(expected == call) return true;
        if(call == null || expected == null) return false;

        return expected.bind.equals(call.bind)
            && expected.bootstrap.equals(call.bootstrap);
    }
}