package discovery.dht.bootstrapper.tests;

import discovery.dht.bootstrapper.Bootstrapper.BootstrapperStartOptions;
import discovery.dht.dhttypes.DhtStartOptions;
import discovery.dht.dhttypes.PeerAddress;
import discovery.test.generators.DhtRandomGenerator;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.async.DoneCallback;
import discovery.test.mockups.DhtBuilderMockup;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class BootstrapperTest implements Scenario{
    @steps
    var steps: BootstrapperSteps;

    @Test
    @scenario
    public function start_bootstrapper(){
        given().a_bind_address();
        given().a_bootstrapper_instance_with_a_dht_builder();
        when().starting_the_bootstrapper();
        then().a_dht_should_be_built();
        and().the_dht_should_be_started_with_the_expected_parameters();
    }

    @Test
    @scenario
    public function start_with_bootstrap_address(){
        given().a_bootstrapper_instance_with_a_dht_builder();
        given().a_bootstrap_address();
        when().starting_the_bootstrapper();
        then().a_dht_should_be_built();
        and().the_dht_should_be_started_with_the_expected_parameters();
    }


    @Test
    @scenario
    public function notify_start(){
        given().an_start_listener();
        given().a_bootstrapper_is_started();
        when().the_dht_starts();
        then().the_start_listener_should_be_notified();
    }
}

class BootstrapperSteps extends Steps<BootstrapperSteps>{
    var dhtBuilderMockup:DhtBuilderMockup;
    var startListenerMockup:ListenerMock<Null<Exception>>;
    var dhtGenerator:DhtRandomGenerator;

    var expectedOptions:DhtStartOptions;
    var bootstrapperOptions:BootstrapperStartOptions;

    var bootstrapper:Bootstrapper;

    public function new(){
        super();

        dhtBuilderMockup = new DhtBuilderMockup();
        startListenerMockup = new ListenerMock();
        dhtGenerator = new DhtRandomGenerator();

        expectedOptions = {};
        bootstrapperOptions = {};
    }

    // summary ---------------------------------------------------

    @step
    public function a_bootstrapper_is_started() {
        given().a_bind_address();
        given().a_bootstrapper_instance_with_a_dht_builder();
        when().starting_the_bootstrapper();
    }

    // given ---------------------------------------------------

    @step
    public function a_bind_address() {
        var bindAddress = dhtGenerator.peerAddress();

        expectedOptions.bind = bindAddress;
        bootstrapperOptions.bind = bindAddress.toString();
    }

    @step
    public function a_bootstrap_address() {
        var bootstrapAddress = dhtGenerator.peerAddress();

        expectedOptions.bootstrap = [bootstrapAddress];
        bootstrapperOptions.bootstrap = [bootstrapAddress.toString()];
    }

    @step
    public function a_bootstrapper_instance_with_a_dht_builder() {
        bootstrapper = new Bootstrapper(dhtBuilderMockup.builder());
    }

    @step
    public function an_start_listener() {
        bootstrapperOptions.onStart = cast startListenerMockup.onEvent;
    }

    // when  ---------------------------------------------------

    @step
    public function starting_the_bootstrapper() {
        bootstrapper.start(bootstrapperOptions);
    }

    @step
    public function the_dht_starts() {
        dht().notifyDhtStarted();
    }

    // then  ---------------------------------------------------

    @step
    public function a_dht_should_be_built() {
        dhtBuilderMockup.verifyDhtBuilt();
    }

    @step
    public function the_dht_should_be_started_with_the_expected_parameters() {
        var options = verifyDhtStarted();

        options.shouldBeEqualsTo(expectedOptions);
    }

    @step
    public function the_start_listener_should_be_notified() {
        startListenerMockup.assertWasCalled();
    }

    // ----------------------------------------------------

    function verifyDhtStarted() {
        dht().verifyDhtStarted();
        return dht().lastStartOptions();
    }

    function dht(){
        return dhtBuilderMockup.dht();
    }
}