package discovery.dht.bootstrapper.tests;

import discovery.test.fakes.FakeConsole;
import discovery.cli.console.Console;
import discovery.test.mockups.ConsoleMockup;
import haxe.exceptions.NotImplementedException;
import discovery.dht.dhttypes.PeerAddress;
import discovery.test.generators.DhtRandomGenerator;
import discovery.dht.bootstrapper.mockups.BootstrapperMockup;
import discovery.dht.bootstrapper.Bootstrapper.BootstrapperStartOptions;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class BootstrapperCliTest implements Scenario{
    @steps
    var steps: BootstrapperCliSteps;

    @Test
    @scenario
    public function receive_bind_parameter(){
        given().a_bootstrapper_cli();
        when().starting_it_with_arguments(['--bind', '123.456.789.10:1234']);
        then().the_bootstrapper_should_be_started_with({
            bind: '123.456.789.10:1234'
        });
    }


    @Test
    @scenario
    public function receive_bootstrap_parameter(){
        given().a_bootstrapper_cli();
        when().starting_it_with_arguments([
            '--bootstrap', '1.2.3.4:5678',
            '--bootstrap', '[dead:beef:f00d::1]:9876',
        ]);
        then().the_bootstrapper_should_be_started_with({
            bootstrap: ['1.2.3.4:5678', '[dead:beef:f00d::1]:9876']
        });
    }

    @Test
    @scenario
    public function print_connect_result_on_complete(){
        given().a_bootstrapper_cli();
        given().the_cli_was_started_with_valid_arguments();
        when().the_dht_starts_bound_to_some_address('1.2.3.4:12345');
        then().the_cli_should_print_lines([
            'dht started',
            'bound to address: 1.2.3.4:12345'
        ]);
    }
}

class BootstrapperCliSteps extends Steps<BootstrapperCliSteps>{
    var bootstrapperCli:BootstrapperCli;
    var bootstrapperMockup:BootstrapperMockup;
    var consoleMockup:ConsoleMockup;
    var dhtGenerator:DhtRandomGenerator;
    var fakeConsole:FakeConsole;

    public function new(){
        super();

        bootstrapperMockup = new BootstrapperMockup();
        consoleMockup = new ConsoleMockup();
        fakeConsole = new FakeConsole();

        dhtGenerator = new DhtRandomGenerator();
    }

    // given ---------------------------------------------------

    @step
    public function a_bootstrapper_cli() {
        var bootstrapper = bootstrapperMockup.bootstrapper();
        // var console:Console = consoleMockup.console();

        bootstrapperCli = new BootstrapperCli(bootstrapper, fakeConsole);
    }

    @step
    public function the_cli_was_started_with_valid_arguments() {
        when().starting_it_with_arguments(['--bind', 'localhost']);
    }

    // when  ---------------------------------------------------

    @step
    public function starting_it_with_arguments(args:Array<String>) {
        bootstrapperCli.start(args);
    }

    @step
    public function the_dht_starts_bound_to_some_address(address:String) {
        var bound = PeerAddress.fromString(address);

        bootstrapperMockup.dht().setBoundAddress(bound);
        bootstrapperMockup.notifyStarted();
    }

    // then  ---------------------------------------------------

    @step
    public function the_bootstrapper_should_be_started_with
                                (expected:BootstrapperStartOptions)
    {
        bootstrapperMockup.verifyStartedWith(expected);
    }

    @step
    public function the_cli_should_print_lines(expected:Array<String>) {
        var lines = fakeConsole.printedLines();

        lines.shouldContainItems(expected);
    }
}