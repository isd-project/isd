package discovery.test.tests.discovery;

import discovery.impl.ipdiscovery.node.NodeIpDiscover;
import discovery.domain.IPAddress;
import discovery.test.integration.tools.NextCallback;
import discovery.domain.IpDiscover;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using StringTools;


class NodeIpDiscoveryTest extends AsyncTests implements Scenario{
    @steps
    var steps: NodeIpDiscoverySteps;

    @AsyncTest
    @scenario
    public function discover_public_ips(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().an_ip_discovery_instance();
        when().listing_the_public_ips(next(()->{
            then().zero_or_more_ips_should_be_listed();
            and().localhost_ips_should_not_be_included();
            done();
        }));
    }
}

class NodeIpDiscoverySteps extends Steps<NodeIpDiscoverySteps>{
    var ipDiscover:IpDiscover;
    var foundIps: Array<IPAddress>;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function an_ip_discovery_instance() {
        ipDiscover = new NodeIpDiscover();
    }

    // when  ---------------------------------------------------

    @step
    public function listing_the_public_ips(next:NextCallback) {
        ipDiscover.listPublicIps().then((ips)->{
            foundIps = ips;
            next();
        }, next);

    }

    // then  ---------------------------------------------------

    @step
    public function zero_or_more_ips_should_be_listed() {
        assertThat(foundIps, is(notNullValue()), "Found ips should not be null");
    }

    @step
    public function localhost_ips_should_not_be_included() {
        for(ip in foundIps){
            assertThat(ip, is(notNullValue()),
                'null ip at found ips: ${foundIps}');
            assertThat(isLocalhost(ip), is(false),
                'Founds ips should not include localhost: ${ip}');
            assertThat(isLinkLocal(ip), is(false),
                'Founds ips should not include link local address: ${ip}');
        }
    }

    function isLocalhost(ip:IPAddress):Bool {
        var ipString = ip.toString();

        return ipString.startsWith("127.")
            || ipString == "::1";
    }

    function isLinkLocal(ip:IPAddress):Bool {
        var ipString = ip.toString();

        return ipString.startsWith("fe80:");
    }
}