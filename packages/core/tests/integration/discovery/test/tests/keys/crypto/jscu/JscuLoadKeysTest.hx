package discovery.test.tests.keys.crypto.jscu;

import discovery.keys.crypto.KeyFormat;
import discovery.format.Formats;
import discovery.format.EncodedData.TextWithFormat;
import discovery.keys.crypto.HashTypes;
import discovery.keys.storage.ExportedFingerprint;
import discovery.keys.crypto.KeyPair;
import discovery.keys.storage.KeyData;
import haxe.Exception;
import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.CryptoKey;
import discovery.test.integration.data.KeysTestData;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.crypto.KeyFormats;
import discovery.keys.crypto.ExportedKey;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class JscuLoadKeysTest extends AsyncTests implements Scenario{
    @steps
    var steps:JscuLoadKeysSteps;

#if(js)
    @AsyncTest
    @scenario
    public function load_exported_key(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_valid_exported_key();
        when().loading_the_exported_key(next(()->{
            then().a_CryptoKey_should_be_produced();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function load_key_data(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_valid_key_data_instance();
        when().loading_the_key_data(next(()->{
            then().a_key_pair_should_be_loaded();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function
        load_key_data_without_private_key(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_key_data_with_no_private_key();
        when().loading_the_key_data(next(()->{
            then().a_key_pair_with_only_the_public_key_should_be_loaded();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function load_null_key_data(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_null_key_data_instance();
        when().loading_the_key_data(next(()->{
            then().a_null_key_pair_should_be_returned();
            done();
        }));
    }

    @AsyncTest
    public function export_and_load_key_test(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        var formats = [Jwk, Pem, Der];
        var iterator = formats.iterator();

        function nextTest() {
            if(iterator.hasNext()){
                export_and_load_key(iterator.next(), (?err)->{nextTest();});
            }
            else{
                done();
            }
        }

        nextTest();
    }

    @scenario
    function export_and_load_key(format: KeyFormat,  doneTest: NextCallback)
    {
        given().a_valid_crypto_key(next(()->{
            steps.given('an exporting format: ${format}', ()->{});
            when().exporting_the_key_to(format, next(()->{
                and().loading_the_exported_key(next(()->{
                    then().a_CryptoKey_should_be_produced();
                    doneTest();
                }));
            }));
        }));
    }

#end
}

class JscuLoadKeysSteps extends Steps<JscuLoadKeysSteps>{

    var keyGenerator:KeyGenerator;

    var exportedKey:ExportedKey;
    var keyData:KeyData;
    var cryptoKey:CryptoKey;

    var loadedKey:CryptoKey;
    var loadedKeyPair:KeyPair;

    var expectedKeyType:KeyType;

    public function new() {
        super();
        keyGenerator = new JscuKeyGenerator();
    }

    // given -----------------------------------------------------------------

    @step
    public function a_valid_exported_key() {
        exportedKey = {
            format: Jwk,
            data: '{
    "kty" : "EC"
    ,"crv" : "P-256"
    ,"x" : "UsPLKLC4-1BNhhsjWdDn-CNUjiI3fbGZVnxQoHcqMCE"
    ,"y" : "CJbRJG1T79684NA83fa8TEIA1Vzm8-ouH1UwiYDkfnQ"
}'
        };
        expectedKeyType = EllipticCurve();
    }

    @step
    public function a_valid_crypto_key(next: NextCallback) {
        given().a_valid_key_data_instance();
        when().loading_the_key_data((?err)->{
            if(err == null){
                this.cryptoKey = loadedKeyPair.publicKey;
                expectedKeyType = cryptoKey.keyType;
            }
            next(err);
        });
    }

    @step
    public function a_valid_key_data_instance() {
        keyData = KeysTestData.elliptCurveKey.clone();
    }

    @step
    public function a_key_data_with_no_private_key() {
        given().a_valid_key_data_instance();

        keyData.privateKey = null;
    }

    @step
    public function a_null_key_data_instance() {
        keyData = null;
    }

    // when -----------------------------------------------------------------

    @step
    public function loading_the_exported_key(next:NextCallback) {
        keyGenerator.loadExportedKey(exportedKey).then((key)->{
            loadedKey = key;
            next();
        }).catchError((err)->{
            next(err);
        });
    }

    @step
    public function loading_the_key_data(next: NextCallback) {
        keyGenerator.loadKey(keyData).then((keyPair)->{
            loadedKeyPair = keyPair;
            next();
        }, (err)->{
            next(err);
        });
    }

    @step
    public function exporting_the_key_to(format:KeyFormat, next: NextCallback) {
        cryptoKey.export(format)
            .then((exported)->{
                this.exportedKey = exported;
                next();
            }, (err)->{
                next(err);
            });
    }

    // then -----------------------------------------------------------------

    @step
    public function a_CryptoKey_should_be_produced() {
        assertThat(loadedKey, is(notNullValue()),
            "Loaded key should not be null");
        assertThat(loadedKey.keyType, is(equalTo(expectedKeyType)),
            "KeyType does not match"
        );
    }


    @step
    public function a_valid_key_pair_should_be_loaded() {
        then().a_key_pair_should_be_loaded();
    }

    @step
    public function a_key_pair_should_be_loaded() {
        assertThat(loadedKeyPair, is(notNullValue()),
            'Loaded key pair is null!'
        );
        if(keyData.privateKey != null){
            assertThat(loadedKeyPair.privateKey, is(notNullValue()),
                'Loaded key pair private key is null!'
            );
        }
        if(keyData.publicKey != null){
            assertThat(loadedKeyPair.publicKey, is(notNullValue()),
                'Loaded key pair public key is null!'
            );
        }
    }

    @step
    public function a_key_pair_with_only_the_public_key_should_be_loaded() {
        then().a_key_pair_should_be_loaded();
    }

    @step
    public function a_null_key_pair_should_be_returned() {
        assertThat(loadedKeyPair, is(nullValue()),
            "KeyPair should be null");
    }
}