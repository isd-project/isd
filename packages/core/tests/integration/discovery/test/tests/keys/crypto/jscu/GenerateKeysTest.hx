package discovery.test.tests.keys.crypto.jscu;

import discovery.keys.storage.KeyData;
import discovery.keys.Key;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.crypto.KeyPair;
import haxe.ValueException;
import massive.haxe.Exception;
import discovery.async.promise.Promisable;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


using discovery.keys.KeyTransformations;


class GenerateKeysTest extends AsyncTests implements Scenario{
    @steps
    var steps:GenerateKeysSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    #if js
    @AsyncTest
    #end
    @scenario
    public function generating_an_elliptic_curve_key(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        next(()->{
            given().a_keygenerator_instance();
            when().a_key_generating_is_started_with({
                keyType: EllipticCurve({
                    namedCurve: "P-256"
                }),
            });
            and().the_key_generation_finishes(next(()->{
                then().a_keypair_should_have_been_generated_with_the_given_type();
                done();
            }));
        })();
    }

    #if js
    @AsyncTest
    #end
    @scenario
    public function generating_rsa_key(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        next(()->{
            given().a_keygenerator_instance();
            when().a_key_generating_is_started_with({
                keyType: RSA({modulusLength: 1024}),
            });
            and().the_key_generation_finishes(next(()->{
                then().a_keypair_should_have_been_generated_with_the_given_type();
                done();
            }));
        })();
    }

    #if js
    @AsyncTest
    #end
    @scenario
    public function transforming_generated_keypair_into_key(
        asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        next(()->{
            given().a_keygenerator_instance();
            given().a_key_was_generated_with({
                keyType: EllipticCurve({
                    namedCurve: "P-256"
                }),
            }, next(()->{
            and().the_generated_key_pair_is_transformed_into_key(next(()->{
                then().the_key_should_have_valid_values();
                done();
            }));
            }));
        })();
    }


    #if js
    @AsyncTest
    #end
    @scenario
    public function exporting_key_into_key_data(
        asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        next(()->{
            given().a_keygenerator_instance();
            given().a_key_was_generated_with({
                keyType: EllipticCurve({
                    namedCurve: "P-256"
                }),
            }, next(()->{
            and().the_generated_key_pair_was_transformed_into_a_key(next(()->{
            when().exporting_the_key_into_keyData(next(()->{
                then().a_valid_keyData_should_have_been_generated();
                done();
            }));
            }));
            }));
        })();
    }
}

class GenerateKeysSteps extends Steps<GenerateKeysSteps> {
    var keygenerator:KeyGenerator;
    var genOptions: GenerateKeyOptions;
    var keyPromise:Promisable<KeyPair>;
    var keyPair:KeyPair;
    var transformedKey:Key;
    var keyData:KeyData;

    // given ------------------------------------------------------------

    @step
    public function a_keygenerator_instance() {

#if js
        keygenerator = new discovery.impl.keys.jscu.JscuKeyGenerator();
#end
    }

    @step
    public function a_key_was_generated_with(
        options: GenerateKeyOptions, next: NextCallback)
    {
        when().a_key_generating_is_started_with(options);
        and().the_key_generation_finishes(next);
    }

    @step
    public function the_generated_key_pair_was_transformed_into_a_key(
        next: NextCallback)
    {
        when().the_generated_key_pair_is_transformed_into_key(next);
    }

    // when ------------------------------------------------------------

    @step
    public function a_key_generating_is_started_with(options: GenerateKeyOptions) {
        genOptions = options;
        keyPromise = keygenerator.generateKey(options);
    }

    @step
    public function the_key_generation_finishes(next:NextCallback) {
        assertThat(keyPromise, is(notNullValue()));

        keyPromise.then((result)->{
            keyPair = result;
            next();
        })
        .catchError((err: Dynamic)->{
            if(Std.isOfType(err, Exception)){
                next(err);
            }
            else{
                next(new ValueException(err));
            }
        });
    }

    @step
    public function the_generated_key_pair_is_transformed_into_key(
        next: NextCallback)
    {
        keyPair.toKey().then((key)->{
            transformedKey = key;
            next();
        },(err)->{
            next(err);
        });
    }


    @step
    public function exporting_the_key_into_keyData(next: NextCallback) {
        transformedKey.toKeyData().then((k)->{
            keyData = k;
            next();
        },(err)->{
            next(err);
        });
    }


    // then ------------------------------------------------------------

    @step
    public function a_keypair_should_have_been_generated_with_the_given_type() {
        assertThat(keyPair, is(notNullValue()), "KeyPair was not generated");
        assertThat(keyPair.privateKey,
            is(notNullValue()), "KeyPair should have privateKey value");
        assertThat(keyPair.publicKey,
            is(notNullValue()), "KeyPair should have publicKey value");

        assertThat(keyPair.privateKey.keyType, is(equalTo(genOptions.keyType)));
        assertThat(keyPair.publicKey.keyType, is(equalTo(genOptions.keyType)));

        assertThat(keyPair.privateKey.isPrivate(), is(true));
        assertThat(keyPair.publicKey.isPrivate(), is(false));
    }

    @step
    public function the_key_should_have_valid_values() {
        assertThat(transformedKey, is(notNullValue()),
            "key should not be null");
        assertThat(transformedKey.fingerprint, is(notNullValue()),
            "key fingerprint"
        );
        assertThat(transformedKey.fingerprint.alg, is(notNullValue()),
            "key fingerprint alg"
        );
        assertThat(transformedKey.fingerprint.hash, is(notNullValue()),
            "key fingerprint hash"
        );
        assertThat(transformedKey.keyType, is(notNullValue()),
            "key type");
        assertThat(transformedKey.privateKey, is(notNullValue()),
            "key privateKey"
        );
        assertThat(transformedKey.privateKey, is(notNullValue()),
            "key privateKey"
        );
        assertThat(transformedKey.publicKey, is(notNullValue()),
            "key publicKey"
        );
    }

    @step
    public function a_valid_keyData_should_have_been_generated() {
        assertThat(keyData, is(notNullValue()), "keyData");
        assertThat(
            keyData.fingerprint.equals(transformedKey.fingerprint), is(true),
            "keyData.fingeprint");
        assertThat(keyData.keyType.equals(transformedKey.keyType), is(true),
            "keyData.keytype");
        assertThat(keyData.privateKey, is(notNullValue()),
            "keyData.privateKey");
        assertThat(keyData.publicKey, is(notNullValue()),
            "keyData.publicKey");
    }
}