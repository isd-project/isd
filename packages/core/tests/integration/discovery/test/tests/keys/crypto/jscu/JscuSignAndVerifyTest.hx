package discovery.test.tests.keys.crypto.jscu;

import discovery.async.promise.Promisable;
import discovery.domain.Description;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.crypto.KeyPair;
import discovery.keys.crypto.signature.ObjectSigner;
import discovery.keys.crypto.signature.ObjectVerifier;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.signature.Signer;
import discovery.keys.crypto.signature.Verifier;
import discovery.keys.storage.KeyData;
import discovery.test.helpers.RandomGen;
import discovery.test.integration.data.KeysTestData;
import discovery.test.integration.tools.AsyncTests;
import discovery.test.integration.tools.NextCallback;
import haxe.Exception;
import haxe.io.Bytes;
import hxgiven.Scenario;
import hxgiven.Steps;
import massive.munit.async.AsyncFactory;

import org.hamcrest.Matchers.*;


class JscuSignAndVerifyTest extends AsyncTests implements Scenario{
    @steps
    var steps:JscuSignAndVerifySteps;


    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function start_signature_with_default_algorithm(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
            given().no_hash_algorithm();
            when().starting_a_signature_using_these_parameters();
            then().a_valid_signer_should_be_created();
            done();
        }));
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function generateSignature(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
        when().signing_some_data();
        and().resolving_the_signature_promise(next(()->{
            then().a_valid_signature_should_be_obtained();
            done();
        }));
        }));
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function start_verification(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
        given().some_data_was_signed_using_some_algorithm(next(()->{
            when().starting_a_verification_using_the_same_algorithm();
            then().a_valid_verifier_should_be_returned();
            done();
        }));
        }));
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function sign_and_verify(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
        given().some_data_was_signed_with_the_private_key(next(()->{
            when().verifying_the_signature_with_the_public_key(next(()->{
                then().the_signature_should_be_validated();
                done();
            }));
        }));
        }));
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function sign_and_verify_object(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
        given().a_signed_object_was_generated_with_the_private_key(next(()->{
            when().verifying_the_signed_object_with_the_public_key(next(()->{
                then().the_signature_should_be_validated();
                done();
            }));
        }));
        }));
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function verify_changed_data(asyncFactory: AsyncFactory)
    {
        makeHandler(asyncFactory, 300);

        given().a_JscuKey_pair(next(()->{
        given().some_data_was_signed_with_the_private_key(next(()->{
            but().the_data_has_slightly_changed();
            when().verifying_the_signature_with_the_public_key(next(()->{
                then().the_signature_should_not_be_validated();
                done();
            }));
        }));
        }));
    }
}


class JscuSignAndVerifySteps extends Steps<JscuSignAndVerifySteps>{

    var keyGenerator:KeyGenerator;
    var keyData:KeyData;
    var keyPair:KeyPair;
    var signedData:Bytes;
    var dataToVerify:Bytes;

    var objToSign:Description;
    var signedObj:Signed<Description>;

    var signerAlgorithm:Null<HashTypes>;
    var signer:Signer;
    var verifier:Verifier;

    var generatedSignature:Signature;
    var signaturePromise:Promisable<Signature>;
    var verifyPromise:Promisable<Verify>;
    var verifyResult:Verify;

    public function new() {
        super();

        keyGenerator = new JscuKeyGenerator();
    }

    @step
    public function some_valid_key_data() {
        this.keyData = KeysTestData.elliptCurveKey.clone();
    }

    @step
    public function some_data_to_sign() {
        signedData = RandomGen.binary.bytes(Random.int(8,32));
    }

    @step
    public function some_hash_algorithm() {
        signerAlgorithm = HashTypes.Sha256;
    }

    @step
    public function no_hash_algorithm() {
        signerAlgorithm = null;
    }

    @step
    public function a_JscuKey_pair(next:NextCallback) {
        given().some_valid_key_data();
        when().loading_the_key(next);
    }

    @step
    public function
        some_data_was_signed_using_some_algorithm(next: NextCallback)
    {
        given().some_data_was_signed_with_the_private_key(next);
    }

    @step
    public function
        some_data_was_signed_with_the_private_key(next: NextCallback)
    {
        when().signing_some_data();
        and().resolving_the_signature_promise(next);

        dataToVerify = signedData.sub(0, signedData.length);
    }

    @step
    public function
        a_signed_object_was_generated_with_the_private_key(next: NextCallback)
    {
        given().some_object_to_sign();
        given().some_hash_algorithm();
        when().starting_a_signature_using_these_parameters();

        ObjectSigner.makeSigned(signer, objToSign).then((signed)->{
            signedObj = signed;
            next();
        }, (err)->{
            next(err);
        });
    }

    @step
    function some_object_to_sign() {
        objToSign = RandomGen.fullDescription();
    }


    @step
    public function the_data_has_slightly_changed() {
        dataToVerify = changedOneByte(dataToVerify);

        assertThat(dataToVerify.compare(signedData), not(is(0)),
            'Data should have changed'
        );
    }

    function changedOneByte(data: Bytes): Bytes {
        var startIdx = Random.int(0, data.length);

        /* Iterate over all indexes to ensure that at least
            one byte can be changed;
         */
        for(i in 0...data.length){
            var idx = (i + startIdx) % data.length;

            var byte = data.get(idx);
            var changed = ~byte;
            data.set(idx, changed);

            /**
                Looks like there is some occasions that, negating the byte does not change it.
                So, if it was not changed, we try again with the next index.
            **/
            if(changed != byte){
                break;
            }
        }

        return data;
    }

    // when -----------------------------------------------------------
    @step
    public function loading_the_key(next: NextCallback) {
        keyGenerator.loadKey(keyData).then((keyPair)->{
            this.keyPair = keyPair;
            next();
        }, (err)->{
            next(err);
        });
    }

    @step
    public function starting_a_signature_using_these_parameters() {
        signer = keyPair.privateKey.startSignature(signerAlgorithm);
    }

    @step
    public function appending_these_data_to_the_signer() {
        signer.appendData(signedData);
    }

    @step
    public function finishing_the_signature() {
        signaturePromise = signer.end();
    }

    @step
    public function signing_some_data() {
        given().some_hash_algorithm();
        when().starting_a_signature_using_these_parameters();
        then().a_valid_signer_should_be_created();

        given().some_data_to_sign();
        when().appending_these_data_to_the_signer();
        and().finishing_the_signature();
        then().a_valid_signature_promise_should_be_generated();
    }

    @step
    public function resolving_the_signature_promise(next:NextCallback) {
        signaturePromise.then((signature)->{
            this.generatedSignature = signature;
            next();
        },(err)->{
            next(err);
        });
    }

    @step
    public function
        starting_a_verification_using_the_same_algorithm()
    {
        verifier = keyPair.publicKey.startVerification(signerAlgorithm);
    }

    @step
    public function
        verifying_the_signature_with_the_public_key(next: NextCallback)
    {
        when().starting_a_verification_using_the_same_algorithm();

        verifier.appendData(dataToVerify);
        verifier.verify(generatedSignature)
            .then((verify)->{
                this.verifyResult = verify;
                next();
            }, (err)->{
                next(err);
            });
    }


    @step
    public function
        verifying_the_signed_object_with_the_public_key(next: NextCallback)
    {
        when().starting_a_verification_using_the_same_algorithm();

        ObjectVerifier.verifySigned(verifier, signedObj).then((result)->{
            verifyResult = result;
            next();
        }, (err)->{
            next(err);
        });
    }


    // then -----------------------------------------------------------

    @step
    public function a_valid_signer_should_be_created() {
        assertThat(signer, is(notNullValue()),
            'Signer is null! Failed to create signer with algorithm: $signerAlgorithm');
    }

    @step
    public function a_valid_signature_promise_should_be_generated() {
        assertThat(signaturePromise, is(notNullValue()), "Failed to create signature");
    }

    @step
    public function a_valid_signature_should_be_obtained() {
        assertThat(generatedSignature, is(notNullValue()),
            'Generated signature should not be null');
        assertThat(generatedSignature.length, is(greaterThan(0)),
            "Generated signature should not be empty"
        );
    }

    @step
    public function a_valid_verifier_should_be_returned() {
        assertThat(verifier, is(notNullValue()), "Verifier should not be null");
    }

    @step
    public function the_signature_should_be_validated() {
        then().the_signature_verification_should_be(true);
    }

    @step
    public function the_signature_should_not_be_validated() {
        then().the_signature_verification_should_be(false);
    }

    @step
    public function the_signature_verification_should_be(expected:Bool) {
        assertThat(verifyResult, is(expected),
            'Verification result does not match the expected value'
            );
    }
}