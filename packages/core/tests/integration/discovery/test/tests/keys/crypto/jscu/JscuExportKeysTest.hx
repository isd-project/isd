package discovery.test.tests.keys.crypto.jscu;

import discovery.keys.crypto.KeyFormats;
import discovery.keys.crypto.KeyFormat;
import haxe.Exception;
import discovery.test.integration.data.KeysTestData;
import discovery.test.integration.tools.NextCallback;
import discovery.impl.keys.jscu.crypto.JscuCryptoKey;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.ExportedKey;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class JscuExportKeysTest extends AsyncTests implements Scenario{
    @steps
    var steps:JscuExportKeysSteps;

    #if(js)
    @AsyncTest
    #end
    @scenario
    public function making_key_from_data(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 300);

        given().some_key_data();
        when().creating_a_key_from_it(next(()->{
            then().a_valid_key_should_be_created();
            done();
        }));
    }

    #if(js)
    @AsyncTest
    #end
    @scenario
    public function exporting_key_to_pem(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 300);

        given().some_key_data(KeysTestData.ellipticCurveJwk);
        when().creating_a_key_from_it(next(()->{
        and().exporting_it_to(KeyFormat.Pem, next(()->{
            then().the_returned_key_should_be_equals_to(KeysTestData.ellipticCurvePem);
            done();
        }));
        }));
    }

    #if(js)
    @AsyncTest
    #end
    @scenario
    public function exporting_key(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 300);

        given().some_crypto_key(next(()->{
        when().exporting_it_to_some_format(next(()->{
            then().a_valid_exported_key_should_be_returned();
            done();
        }));
        }));
    }
}

class JscuExportKeysSteps extends Steps<JscuExportKeysSteps>{

    var keyData:ExportedKey;
    var exportedKey:ExportedKey;
    var cryptoKey:CryptoKey;
    var exportFormat:KeyFormat;

    // given --------------------------------------------------------------

    @step
    public function some_key_data(?exportedKey) {
        if(exportedKey == null){
            exportedKey = KeysTestData.elliptCurveKey.privateKey;
        }
        keyData = exportedKey;
    }


    @step
    public function some_export_format() {
        exportFormat = Random.fromArray([Pem, Der, Jwk]);
    }

    @step
    public function some_crypto_key(next:NextCallback) {
        given().some_key_data();
        when().creating_a_key_from_it(next);
    }

    // when --------------------------------------------------------------
    @step
    public function creating_a_key_from_it(next: NextCallback) {
        JscuCryptoKey.makeKey(keyData)
        .then((key)->{
            cryptoKey = key;
            next();
        },(err)->{
            next(err);
        });
    }

    @step
    public function exporting_it_to_some_format(next:NextCallback) {
        given().some_export_format();
        when().exporting_it_to(exportFormat, next);
    }

    @step
    public function exporting_it_to(format:KeyFormat, next:NextCallback) {
        exportFormat = format;

        cryptoKey.export(exportFormat).then((exportedKey)->{
            this.exportedKey = exportedKey;
            next();
        }, (err)->{
            next(err);
        });
    }

    // then --------------------------------------------------------------

    @step
    public function a_valid_key_should_be_created() {
        assertThat(cryptoKey, is(notNullValue()));
        assertThat(cryptoKey.keyType, is(notNullValue()));
    }

    @step
    public function a_valid_exported_key_should_be_returned() {
        assertThat(exportedKey, is(notNullValue()), "Exported key should not be null");
        assertThat(exportedKey.format, is(equalTo(exportFormat)),
            "Exported key format should be equal to the expected value");
        assertThat(exportedKey.data, is(notNullValue()),
            "Exported key data should not be null");
    }

    @step
    public function the_returned_key_should_be_equals_to(expected:ExportedKey) {
        assertThat(exportedKey, is(notNullValue()), "Exported key should not be null");
        assertThat(exportedKey.format, is(equalTo(expected.format)),
            "Exported key format is not the expected");
        assertThat(exportedKey.data.toString(),
            is(equalTo(expected.data.toString())),
            "Exported key data is not the expected");
    }
}