package discovery.test.tests.keys.storage.lowdb;

import discovery.format.EncodedData.TextWithFormat;
import discovery.impl.keys.storage.lowdb.helpers.LowDbCreateOptions;
import discovery.impl.keys.storage.lowdb.KeyStorageLowDb;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.KeyFormats;
import discovery.keys.crypto.KeyTypes;
import discovery.keys.storage.KeyData;
import discovery.keys.storage.KeyStorage;
import discovery.test.helpers.RandomGen;
import haxe.io.Bytes;
import sys.FileSystem;
import sys.io.File;
import hxgiven.Steps;
import hxgiven.Scenario;

import discovery.test.integration.tools.TemporaryDir.*;

import org.hamcrest.Matchers.*;

class KeyStorageLowDbTest implements Scenario{
    @steps
    var steps:KeyStorageLowDbSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @After
    public function teardown() {
        steps.teardown();
    }

    @Test
    @scenario
    public function creating_db_file_in_specified_directory() {
        given().a_directory_path();
        given().a_keystorage_was_created_from_it();
        when().saving_a_key();
        then().the_database_dir_and_file_should_be_created();
        and().the_key_should_be_saved_on_that_file();
    }

    @Test
    @scenario
    public function creating_db_file_with_specified_directory_and_filename() {
        given().a_directory_path_and_filename();
        given().a_keystorage_was_created_from_it();
        when().saving_a_key();
        then().the_database_dir_and_file_should_be_created();
    }

    @Test
    @scenario
    public function persist_keys() {
        given().an_empty_key_storage_created_from_some_directory();
        given().a_key_was_saved();
        when().the_storage_is_restored();
        and().that_key_is_retrieved();
        then().loaded_key_should_be_equals_to_the_stored_key();
    }

    @Test
    @scenario
    public function add_multiple_keys() {
        given().an_empty_key_storage_created_from_some_directory();
        when().multiple_unique_keys_are_saved();
        and().that_keys_are_listed();
        then().all_saved_keys_should_be_returned();
    }

    @Test
    @scenario
    public function reload_storage(){
        given().an_existent_db_file_with_some_keys();
        when().the_db_is_reloaded();
        and().that_keys_are_listed();
        then().all_saved_keys_should_be_returned();
    }

    @Test
    @scenario
    public function get_key(){
        given().a_key_storage_with_some_key();
        when().getting_that_key();
        then().the_expected_key_data_should_be_returned();
    }
}

class KeyStorageLowDbSteps extends Steps<KeyStorageLowDbSteps> {
    var storage:KeyStorage;

    var lowDbDir:String = null;
    var lowDbFile:String = null;
    var dbCreateOptions:LowDbCreateOptions;

    var storedkey:KeyData;
    var restoredkey:KeyData;
    var foundKey:KeyData;

    var addedKeys:Array<KeyData>;
    var listedKeys:Array<KeyData>;

    public function new() {
        super();
        addedKeys = [];
    }

    public function teardown() {
        deleteDbFile();

        if(lowDbDir != null){
            if(FileSystem.exists(lowDbDir)){
                FileSystem.deleteDirectory(lowDbDir);
            }
        }
    }


    // given summary-------------------------------------------------

    @step
    public function an_existent_db_file_with_some_keys() {
        given().an_empty_key_storage_created_from_some_directory();
        when().multiple_unique_keys_are_saved();
    }

    @step
    public function the_db_is_reloaded() {
        given().a_keystorage_was_created_from_it();
    }

    // given ---------------------------------------------------------
    @step
    public function a_directory_path() {
        lowDbDir = tempdirPath('haxelowdb');

        dbCreateOptions = {
            dir: lowDbDir
        };
    }

    @step
    public function a_directory_path_and_filename() {
        given().a_directory_path();
        lowDbFile = RandomGen.primitives.name();

        dbCreateOptions = {
            dir: lowDbDir,
            file: lowDbFile
        };
    }

    @step
    function the_directory_is_empty() {
        deleteDbFile();
    }

    @step
    public function a_keystorage_was_created_from_it() {
        storage = new KeyStorageLowDb(dbCreateOptions);
    }

    @step
    public function some_key_data() {
        storedkey = {
            keyType: RSA({
                modulusLength: 1024
            }),
            publicKey: KeyFormats.PEM('-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtsuFucnMcwUlJeUh4C9a
SjTtaESDTams3V4UaivnIDy1Y7kmpZb3ASdfb5PpI5QCqvKHgUOOOFD43dCdxnYF
tk4cysOn4LYAfYsWHsklM8Xg0aiLRPf3vkjdaKhhlaawl/XsiGn3RvLZWO8gbUaj
dTLEbFKI12JgKu1cRpLxElU5Mi6Pvu9ZeAGti6SIqOVNScnvxAK7ZMGzGV8cOU+Q
TiMEuE0rym4rOh7V/O/9iVJ3Z15221zSqKc4tSW3Gt/LizQ/AW0lYQ/lZIOP8HHc
fb7GmGXBmYU2HqoFdvsBH+Yw1aVe0lkfU1PzUPreUwVnkNTj/l/2eYaDRuj4eSBe
awIDAQAB
-----END PUBLIC KEY-----'),
            privateKey: KeyFormats.PEM(
                '-----BEGIN PRIVATE KEY----- ... -----END PRIVATE KEY-----'
            ),
            name: "name example",
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex('f307e220857e996a69d6c1d8a32e3691560893623989e1501a916eb1863ba7d1')
            }
        }
    }

    @step
    public function an_empty_key_storage_created_from_some_directory() {
        given().a_directory_path();
        given().the_directory_is_empty();
        and().a_keystorage_was_created_from_it();
    }

    @step
    public function a_key_was_saved() {
        when().saving_a_key();
    }


    @step
    public function a_key_storage_with_some_key() {
        given().an_empty_key_storage_created_from_some_directory();
        given().a_key_was_saved();
        and().the_directory_was_restored();
    }

    @step
    public function the_directory_was_restored() {
        given().a_keystorage_was_created_from_it();
    }

    // when ---------------------------------------------------------
    @step
    public function saving_a_key() {
        given().some_key_data();

        when().adding_a_key(storedkey);
    }

    @step
    public function adding_a_key(key:KeyData) {
        storage.addKey(key);
        addedKeys.push(key);
    }

    @step
    public function that_key_is_retrieved() {
        restoredkey = storage.getKey(storedkey.fingerprint.hash);
    }

    @step
    public function the_storage_is_restored() {
        given().a_keystorage_was_created_from_it();
    }

    @step
    public function multiple_unique_keys_are_saved(numKeys:Int=5) {
        for(i in 0...numKeys){
            var key = genUniqueKey();

            when().adding_a_key(key);
            and().the_storage_is_restored();
        }
    }

    @step
    public function that_keys_are_listed() {
        listedKeys = storage.listKeys();
    }


    @step
    public function getting_that_key() {
        var keyPrefix = storedkey.fingerprint.hash.toBytes().toHex();
        foundKey = storage.getKey(keyPrefix);
    }

    // then ---------------------------------------------------------
    @step
    public function the_database_dir_and_file_should_be_created() {
        assertThat(FileSystem.exists(lowDbDir), is(true),
            'Database dir should be created'
        );
        var file = expectedDbFile();
        assertThat(FileSystem.exists(file), is(true),
            'Db file should have been created.\n' +
            '\tExpected file: ${file}'
        );
    }

    @step
    public function the_key_should_be_saved_on_that_file() {
        var dbContent = File.getContent(expectedDbFile());

        assertThat(dbContent, is(notNullValue()),
            "Database file content is null");
        assertThat(dbContent.length, is(greaterThan(0)),
            "Database file content should not be empty");
    }

    @step
    public function loaded_key_should_be_equals_to_the_stored_key() {
        assertThat(storedkey.keyType.equals(restoredkey.keyType), is(true),
            'keyTypes should be equal');
        assertThat(storedkey.fingerprint.equals(restoredkey.fingerprint), is(true),
            'fingerprints should be equal');
        assertThat(storedkey.name, is(equalTo(restoredkey.name)),
            'key names should be equal');
        assertThat(storedkey.privateKey.equals(restoredkey.privateKey), is(true),
        'privateKey: ${Std.string(storedkey.privateKey)}\n'
        + 'should be equal to: ${Std.string(restoredkey.privateKey)}');

        assertThat(storedkey.publicKey.equals(restoredkey.publicKey), is(true),
        'publicKeys should be equal');

        assertThat(restoredkey.equals(storedkey), is(true),
            'Restored key should be: ${Std.string(storedkey)}\n' +
            'but was: ${Std.string(restoredkey)}'
        );
    }

    @step
    public function all_saved_keys_should_be_returned() {
        assertThat(listedKeys, hasSize(addedKeys.length));

        for (i in 0...listedKeys.length){
            var k = listedKeys[i];
            var expected = addedKeys[i];


            assertThat(k, is(notNullValue()));
            assertThat(k.equals(expected), is(true),
                'key: ${k}\n'
                +'should be equals to: ${expected}'
            );
        }
    }

    // -----------------------------------------------

    function deleteDbFile() {
        var dbFile = expectedDbFile();
        if(dbFile != null && FileSystem.exists(dbFile)){
            FileSystem.deleteFile(dbFile);
        }
    }

    function expectedDbFile() {
        if(lowDbDir == null) return null;

        return LowDbCreateOptions.makeDbFilepath(lowDbDir, lowDbFile);
    }

    function genUniqueKey(): KeyData{
        return {
            keyType: Random.fromArray([
                RSA({
                    modulusLength: Random.fromArray([1024, 2048, 4096])
                }),
                EllipticCurve({
                    namedCurve: Random.string(Random.int(3,6))
                })
            ]),
            publicKey: KeyFormats.DER(RandomGen.bytes(10)),
            privateKey: KeyFormats.DER(RandomGen.bytes(10)),
            name: Random.string(9),
            fingerprint: RandomGen.crypto.fingerprint()
        };
    }

    @step
    public function the_expected_key_data_should_be_returned() {
        assertThat(foundKey, is(notNullValue()), "Not found expected key");

        checkExportedKey(foundKey.privateKey, storedkey.privateKey,
            'private key');
        checkExportedKey(foundKey.publicKey, storedkey.publicKey,
            'public key');

        assertThat(foundKey.equals(storedkey), is(true),
            'Found key: $foundKey\n' +
            'does not match the expected: $storedkey'
        );
    }

    function checkExportedKey(key: ExportedKey, expected: ExportedKey, keyname:String='key')
    {
        assertThat(key, is(notNullValue()), '$key should not be null');
        assertThat(key.equals(expected), is(true),
            'Expected: ${expected}\n' +
            'Found: ${key}'
        );
        assertThat(key.data, isA(TextWithFormat),
            '$key data should have encoding format specified'
        );
        assertThat(key.keyData(), is(equalTo(expected.keyData())),
            '$key data should be retrieved correctly'
        );
    }
}