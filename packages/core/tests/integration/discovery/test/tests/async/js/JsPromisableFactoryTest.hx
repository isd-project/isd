package discovery.test.tests.async.js;

import discovery.async.promise.PromiseInitCallback;
import haxe.ValueException;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.test.helpers.RandomGen;
import discovery.async.promise.Promisable;
import discovery.test.integration.tools.NextCallback;
import haxe.Exception;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;

import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class JsPromisableFactoryTest extends AsyncTests implements Scenario{
    @steps
    var steps: JsPromisableFactorySteps;

    #if js

    @AsyncTest
    @scenario
    public function create_and_resolve_promisable(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_promisable_factory_instance();
        when().creating_a_promisable_from_a_handler_function();
        when().the_promisable_is_resolved_to_some_value();
        and().a_thenable_is_chained_to_the_promise(next(()->{
            then().the_given_value_should_be_returned();
            done();
        }));
    }


    @AsyncTest
    @scenario
    public function create_and_reject_promisable(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_promisable_factory_instance();
        when().creating_a_promisable_from_a_handler_function();
        when().the_promisable_is_rejected_with_some_exception();
        and().the_promise_error_is_catched(next(()->{
            then().the_given_exception_should_be_returned();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function resolved_promisable(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_promisable_factory_instance();
        when().a_resolved_promise_is_created_from_some_value();
        and().a_thenable_is_chained_to_the_promise(next(()->{
            then().the_given_value_should_be_returned();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function rejected_promisable(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_promisable_factory_instance();
        when().creating_a_rejected_promise_from_some_exception();
        and().the_promise_error_is_catched(next(()->{
            then().the_given_exception_should_be_returned();
            done();
        }));
    }

    @AsyncTest
    @scenario
    public function merge_two_promises(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 300);

        given().a_promisable_factory_instance();
        when().merging_two_promises();
        and().resolving_the_resultant_promise(next(()->{
            then().the_expected_values_should_be_returned_on_an_array();
            done();
        }));
    }


    #end
}

class JsPromisableFactorySteps extends Steps<JsPromisableFactorySteps>{
    var promisableFactory:PromisableFactory;
    var promisable:Promisable<Any>;

    var resolver:PromiseResolver<Any>;
    var rejector:PromiseReject;

    var expectedValue:Any;
    var capturedValue:Any;
    var expectedError:Exception;
    var capturedError:Exception;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_promisable_factory_instance() {
        #if js
        promisableFactory = new JsPromisableFactory();
        #end
    }

    @step
    public function some_value(){
        expectedValue = RandomGen.name();
    }

    @step
    function some_exception() {
        var message = Random.string(Random.int(5, 10));
        expectedError = new Exception(message);
    }

    // when  ---------------------------------------------------

    @step
    public function creating_a_promisable_from_a_handler_function() {
        promisable = promisableFactory.promise((resolve, reject)->{
            this.resolver = resolve;
            this.rejector = reject;
        });
    }

    @step
    public function the_promisable_is_resolved_to_some_value() {
        given().some_value();

        assertThat(resolver, is(notNullValue()), "resolver should not be null");
        resolver(expectedValue);
    }

    @step
    public function the_promisable_is_rejected_with_some_exception() {
        given().some_exception();

        assertThat(rejector, is(notNullValue()), "rejector should not be null");
        rejector(expectedError);
    }


    @step
    public function a_resolved_promise_is_created_from_some_value() {
        given().some_value();
        promisable = promisableFactory.resolved(expectedValue);
    }

    @step
    public function a_thenable_is_chained_to_the_promise(next:NextCallback) {
        promisable.then((result)->{
            capturedValue = result;
            next();
        },(err)->{
            capturedError = err;
            next();
        });
    }

    @step
    public function creating_a_rejected_promise_from_some_exception() {
        given().some_exception();

        promisable = promisableFactory.rejected(expectedError);
    }

    @step
    public function the_promise_error_is_catched(next:NextCallback) {
        promisable.catchError(function(err):Any{
            capturedError = err;
            next();
            return err;
        });
    }


    @step
    public function merging_two_promises() {
        expectedValue = [1, 2];

        promisable = promisableFactory.mergeAll([
            promisableFactory.resolved(1),
            promisableFactory.resolved(2)
        ]);
    }

    @step
    public function resolving_the_resultant_promise(next: NextCallback) {
        when().a_thenable_is_chained_to_the_promise(next);
    }

    // then  ---------------------------------------------------

    @step
    public function the_given_value_should_be_returned() {
        assertThat(capturedValue, is(equalTo(expectedValue)),
            "Captured value does not match the expected");
    }

    @step
    public function the_given_exception_should_be_returned() {
        assertThat(capturedError, is(equalTo(capturedError)),
            "Captured exception does not match the expected value");
    }


    @step
    public function the_expected_values_should_be_returned_on_an_array() {
        var expectedArray:Array<Dynamic> = expectedValue;

        assertThat((capturedValue : Array<Dynamic>), is(array(expectedArray)),
            "Captured value does not match the expected"
        );
    }
}