package discovery.test.tests.utils;

import discovery.utils.time.TimeCount;
import discovery.utils.time.Chronometer;
import discovery.utils.time.TimeInterval;
import discovery.utils.time.DefaultTimerLoop;
import discovery.utils.time.TimerLoop;
import discovery.test.integration.tools.NextCallback;
import discovery.utils.time.Duration;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.time.TimeUnitTools;
using discovery.test.matchers.CommonMatchers;


class DefaultTimerLoopTest extends AsyncTests implements Scenario{
    @steps
    var steps: DefaultTimerLoopSteps;


    @AsyncTest
    @scenario
    public function delay_time(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 50);

        given().a_default_timer();
        when().delaying_a_task_to(15.millis(), next(()->{
            then().it_should_be_executed_after_the_given_time();
            done();
        }));
    }
}

class DefaultTimerLoopSteps extends Steps<DefaultTimerLoopSteps>{
    var timer:TimerLoop;

    var delayTime:Duration;
    var measuredTime:TimeInterval;

    var chronometer:Chronometer;
    var measure:TimeCount;

    public function new(){
        super();

        chronometer = new Chronometer();
    }

    // given ---------------------------------------------------

    @step
    public function a_default_timer() {
        timer = new DefaultTimerLoop();
    }

    // when  ---------------------------------------------------

    @step
    public function delaying_a_task_to(time:Duration, next: NextCallback) {
        delayTime = time;

        measure = chronometer.start();
        timer.delay(()->{
            measuredTime = measure.measure();
            next();
        }, delayTime.time());
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_be_executed_after_the_given_time() {
        measuredTime.duration().shouldBeGreaterThanOrEqualsTo(delayTime,
            'Measured delay time should be greater or equals than specified delay');
    }
}