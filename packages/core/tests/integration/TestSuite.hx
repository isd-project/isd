import massive.munit.TestSuite;

import discovery.test.tests.utils.DefaultTimerLoopTest;
import discovery.test.tests.async.js.JsPromisableFactoryTest;
import discovery.test.tests.keys.crypto.jscu.JscuSignAndVerifyTest;
import discovery.test.tests.keys.crypto.jscu.JscuLoadKeysTest;
import discovery.test.tests.keys.crypto.jscu.JscuExportKeysTest;
import discovery.test.tests.keys.crypto.jscu.GenerateKeysTest;
import discovery.test.tests.keys.storage.lowdb.KeyStorageLowDbTest;
import discovery.test.tests.discovery.NodeIpDiscoveryTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.tests.utils.DefaultTimerLoopTest);
		add(discovery.test.tests.async.js.JsPromisableFactoryTest);
		add(discovery.test.tests.keys.crypto.jscu.JscuSignAndVerifyTest);
		add(discovery.test.tests.keys.crypto.jscu.JscuLoadKeysTest);
		add(discovery.test.tests.keys.crypto.jscu.JscuExportKeysTest);
		add(discovery.test.tests.keys.crypto.jscu.GenerateKeysTest);
		add(discovery.test.tests.keys.storage.lowdb.KeyStorageLowDbTest);
		add(discovery.test.tests.discovery.NodeIpDiscoveryTest);
	}
}
