package discovery.test.crypto.jscu;

import discovery.keys.crypto.KeyGenerator;
import discovery.impl.keys.jscu.externs.JscuKeyPair;
import discovery.impl.keys.jscu.externs.PublicKeyCrypto;
import discovery.keys.crypto.KeyPair;
import massive.munit.Assert;
import discovery.async.promise.Promisable;
import discovery.keys.Keychain;
import discovery.keys.crypto.KeyTypes;
import hxgiven.Steps;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.mock;

import org.hamcrest.Matchers.*;

class JscuKeyGeneratorTest implements Scenario{
    @steps
    var steps:JscuKeyGeneratorSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function start_generating_key(){
        given().a_jscu_keygenerator_instance();
        when().generating_a_key_with_valid_arguments();
        then().the_key_generation_should_be_started_with_corresponding_args();
        and().a_promise_should_be_returned();
    }
}

typedef JscuGenKeyArgs = {
    keyType:String,
    params: Dynamic
};

class JscuKeyGeneratorSteps extends Steps<JscuKeyGeneratorSteps> {
    var keygenerator:KeyGenerator;
    var keyPromise:Promisable<KeyPair>;

    var pkcMock:PublicKeyCrypto;
    var genKeyArgs:JscuGenKeyArgs;
    var expectedKeyArgs:JscuGenKeyArgs;
    var promiseMock:Promisable<JscuKeyPair>;
    var keyPairPromiseMock:Promisable<KeyPair>;

    var genKeyOptions:GenerateKeyOptions;


    public function new() {
        super();

        promiseMock = mock(Promisable);
        keyPairPromiseMock = mock(Promisable);
        pkcMock = mock(PublicKeyCrypto);

        Mockatoo
            .when(pkcMock.generateKey(any, any))
            .thenCall(onGenerateKey);
    }

    function onGenerateKey(args: Array<Dynamic>) {
        genKeyArgs = {
            keyType: args[0],
            params: args[1]
        };

        Mockatoo.when(promiseMock.then(isNotNull)).thenReturn(keyPairPromiseMock);

        return promiseMock;
    }

    // given -------------------------------------------------------------

    @step
    public function a_jscu_keygenerator_instance() {
        keygenerator = new discovery.impl.keys.jscu.JscuKeyGenerator(pkcMock);
    }


    // when -------------------------------------------------------------

    @step
    public function generating_a_key_with_valid_arguments() {
        when().generating_a_key_with({
            keyType: EllipticCurve({
                namedCurve: "P-256"
            })
        });
        expectedKeyArgs = {
            keyType: 'EC',
            params: {
                namedCurve: "P-256"
            }
        };
    }

    @step
    public function generating_a_key_with(options: GenerateKeyOptions)
    {
        genKeyOptions = options;
        keyPromise = keygenerator.generateKey(options);
    }


    // then -------------------------------------------------------------

    @step
    public function the_key_generation_should_be_started_with_corresponding_args() {
        Mockatoo.verify(pkcMock.generateKey(isNotNull, isNotNull));

        assertThat(genKeyArgs, is(notNullValue()));

        assertThat(genKeyArgs.keyType, is(equalTo(expectedKeyArgs.keyType)),
            "keyType does not match the expected"
        );
        Assert.areEqual(genKeyArgs.params, expectedKeyArgs.params,
            "key generation params does not match the expected.\n" +
            '\t expected was: ${Std.string(expectedKeyArgs.params)}\n' +
            '\t but found: ${Std.string(genKeyArgs.params)}\n'
            );
    }

    @step
    public function a_promise_should_be_returned() {
        assertThat(keyPromise, is(notNullValue()), "a promise should have been returned");
    }
}