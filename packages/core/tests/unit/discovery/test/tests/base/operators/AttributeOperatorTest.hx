package discovery.test.tests.base.operators;

import discovery.domain.operators.basic.AttributeOperator;
import hxgiven.Scenario;


using discovery.test.matchers.CommonMatchers;


class AttributeOperatorTest implements Scenario{
    @steps
    var steps: AttributeOperatorSteps;

    @Test
    @scenario
    public function get_from_null_attribute(){
        given().a_discovered_service_with_these_attributes(null);
        given().an_attribute_operator_of("attribute");
        when().evaluating_the_operator();
        then().it_should_return(null);
    }

    @Test
    @scenario
    public function get_simple_attribute(){
        given().a_discovered_service_with_these_attributes({
            hello: "world",
            amount: 100
        });
        given().an_attribute_operator_of("hello");
        when().evaluating_the_operator();
        then().it_should_return("world");
    }


    @Test
    @scenario
    public function get_invalid_attribute(){
        given().a_discovered_service_with_these_attributes({
            hello: "world",
            amount: 100
        });
        given().an_attribute_operator_of("inexistent");
        when().evaluating_the_operator();
        then().it_should_return(null);
    }


    @Test
    @scenario
    public function get_hierarchical_attribute(){
        given().a_discovered_service_with_these_attributes({
            obj: {
                value: 100
            }
        });
        given().an_attribute_operator_of("obj.value");
        when().evaluating_the_operator();
        then().it_should_return(100);
    }

    @Test
    @scenario
    public function get_invalid_hierarchical_attribute(){
        given().a_discovered_service_with_these_attributes({
            obj: { value: 100}
        });
        given().an_attribute_operator_of("obj.obj.value");
        when().evaluating_the_operator();
        then().it_should_return(null);
    }
}

class AttributeOperatorSteps extends OperatorBaseSteps<AttributeOperatorSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function an_attribute_operator_of(attr:String) {
        op = new AttributeOperator(attr);
    }

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}