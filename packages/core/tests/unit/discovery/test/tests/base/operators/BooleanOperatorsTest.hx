package discovery.test.tests.base.operators;

import discovery.domain.operators.exceptions.InvalidOperandException;
import discovery.domain.operators.Operator;
import discovery.domain.operators.serialization.OperatorData;
import discovery.domain.operators.serialization.OperatorSerializer;
import discovery.domain.operators.serialization.OperatorData.OperandData;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
import discovery.test.mockups.FakeOperator.*;

using discovery.domain.operators.Operators;
using discovery.test.matchers.CommonMatchers;


class BooleanOperatorsTest implements Scenario{
    @steps
    var steps: BooleanOperatorsSteps;


    @Test
    @scenario
    public function and_operator(){
        test_evaluate_operator(AND(value(false), value(true)), false);
        test_evaluate_operator(AND(value(true), value(true)), true);

        test_evaluate_exception(
            AND(value(true), value(null)), InvalidOperandException);
    }


    @Test
    @scenario
    public function or_operator(){
        test_evaluate_operator(OR(value(false), value(true)), true);
        test_evaluate_operator(OR(value(false), value(false)), false);

        test_evaluate_exception(
            OR(value(true), value(null)), InvalidOperandException);
    }


    @Test
    @scenario
    public function not_operator(){
        test_evaluate_operator(not(value(false)), true);
        test_evaluate_operator(not(value(true)), false);


        test_evaluate_exception(
            not(value(null)), InvalidOperandException);
    }


    @scenario
    function test_evaluate_operator<T>(op:Operator<T>, expected:T) {
        steps
        .given('parameters: op: ${op}, expected: $expected', ()->{});
        given().this_operator(op);
        given().a_discovered_service_with_these_attributes({});
        when().evaluating_the_operator();
        then().it_should_return(expected);
    }


    @scenario
	function test_evaluate_exception(op:Operator<Bool>, type:Dynamic) {
        steps
        .given('parameters: op: ${op}, expected exception: $type', ()->{});
        given().this_operator(op);
        given().a_discovered_service_with_these_attributes({});
        when().trying_to_evaluate_the_operator();
        then().it_should_throw_an_exception_of(type);
    }

    // ---------------------------------------------------------

    @Test
    @scenario
    public function serialization(){
        given().this_operator(
            not(fakeOperator("X")).AND(
                fakeOperator("A").OR(fakeOperator("B"))
            )
        );
        when().serializing_it();
        then().this_serialized_operator_should_be_produced({
            type: And,
            operands: [
                {
                    type: Not,
                    operands: [{type: "X", operands:[]}]
                },
                {
                    type: Or,
                    operands: [
                        {type: "A", operands:[]},
                        {type: "B", operands:[]}
                    ]
                }
            ]
        });
    }
}

class BooleanOperatorsSteps extends OperatorBaseSteps<BooleanOperatorsSteps>{

    // given ---------------------------------------------------

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------
}