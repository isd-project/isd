package discovery.test.tests.base.operators;

import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
import discovery.test.mockups.FakeOperator.*;

using discovery.domain.operators.Operators;
using discovery.test.matchers.CommonMatchers;


class StringOperatorsTest implements Scenario{
    @steps
    var steps: StringOperatorsSteps;

    @Test
    @scenario
    public function contains_text(){
        given().this_operator(attr("A").containsText("hello".value()));
        given().a_discovered_service_with_these_attributes({
            A: "hello world!"
        });
        when().evaluating_the_operator();
        then().it_should_return(true);
    }

    @Test
    @scenario
    public function contains_text_with_null(){
        given().this_operator(attr("X").containsText("hello".value()));
        given().a_discovered_service_with_these_attributes({
            A: "hello world!"
        });
        when().evaluating_the_operator();
        then().it_should_return(false);
    }
}

class StringOperatorsSteps extends OperatorBaseSteps<StringOperatorsSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}