package discovery.test.tests.base.configuration;

import discovery.base.configuration.composite.CompositeBuilder;
import discovery.base.configuration.composite.CompositeMechanismBuilder;
import discovery.base.configuration.DiscoveryMechanismBuilder;
import discovery.base.CompositeDiscoveryMechanism;
import discovery.test.common.mockups.discovery.configuration.DiscoveryMechanismBuilderMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeMechanismBuilderTest implements Scenario{
    @steps
    var steps: CompositeMechanismBuilderSteps;

    @Test
    @scenario
    public function build_composite_from_mechanism_builders(){
        given().a_composite_builder();
        given().some_component_builders();
        when()
            .adding_the_component_builders();
            and().building_the_composite();
        then()
            .all_components_should_be_built();
            and().a_composite_mechanism_should_be_returned();
    }

    @Test
    @scenario
    public function filter_components_by_name(){
        given().a_composite_builder();
        given().some_named_component_mechanism_builders_were_added();
        when().filtering_out_some_components_by_name();
        and().building_the_composite();
        then().only_the_included_mechanisms_should_be_built();
    }

    @Test
    @scenario
    public function list_named_components(){
        given().a_composite_builder();
        given().some_named_component_mechanism_builders_were_added();
        when().listing_all_named_builders();
        then().all_added_builders_should_be_listed();
    }
}

class CompositeMechanismBuilderSteps
    extends CompositeBuilderBaseSteps<DiscoveryMechanismBuilder,
                                        DiscoveryMechanismBuilderMockup,
                                        CompositeMechanismBuilderSteps>
{
    var builtMechanism:DiscoveryMechanism;

    public function new(){
        super();

        builderMockups = new CompositeMockup(()->{
            return new DiscoveryMechanismBuilderMockup();
        });

        namedBuilders = new Map<String, DiscoveryMechanismBuilderMockup>();
        includedToBuild = [];
        excludedFromBuild = [];
    }

    function
        createCompositeBuilder():CompositeBuilder<DiscoveryMechanismBuilder>
    {
        return new CompositeMechanismBuilder();
    }

    function makeBuilderMockup():DiscoveryMechanismBuilderMockup {
        return new DiscoveryMechanismBuilderMockup();
    }

    function
        getBuilder(m:DiscoveryMechanismBuilderMockup):DiscoveryMechanismBuilder
    {
        return m.builder();
    }

    function
        buildComposite(composite:CompositeBuilder<DiscoveryMechanismBuilder>)
    {
        builtMechanism =
            cast(composite, CompositeMechanismBuilder).buildMechanism();
    }

    function verifyBuilt(m:DiscoveryMechanismBuilderMockup) {
        m.verifyBuilt();
    }

    function verifyNotBuilt(mockup:DiscoveryMechanismBuilderMockup) {
        mockup.verifyNotBuilt();
    }

    // given summary -------------------------------------------

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

    @step
    public function a_composite_mechanism_should_be_returned() {
        builtMechanism.shouldBeAn(CompositeDiscoveryMechanism);
    }
}