package discovery.test.tests.base.search;

import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.test.common.ListenerMock;
import discovery.test.common.mockups.SearchControlMockup;
import discovery.base.search.SearchBase;
import hxgiven.Steps;


class SearchBaseCommonSteps<Self:SearchBaseCommonSteps<Self>>
    extends Steps<Self>
{
    var search:SearchBase;
    var searchControl:SearchControlMockup;

    var onFoundListener:ListenerMock<Discovered>;

    public function new(){
        super();

        searchControl = new SearchControlMockup();
        onFoundListener = new ListenerMock();
    }


    // summary ---------------------------------------------------


    // given ---------------------------------------------------

    @step
    public function a_search_instance_with_a_search_control() {
        search = new SearchBase(searchControl.searchControl);
    }

    @step
    public function an_onFound_listener_was_registered() {
        search.onFound.listen(onFoundListener.onEvent);
    }

    // when  ---------------------------------------------------

    @step
    public function a_service_is_discovered() {
        var discovered = RandomGen.discovered();

        search.discovered(discovered);
    }

    // then  ---------------------------------------------------

    @step
    public function the_onFound_listener_should_not_be_called() {
        onFoundListener.assertWasCalledTheseTimes(0);
    }
}