package discovery.test.tests.base.search;

import haxe.exceptions.NotImplementedException;
import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.test.common.CallableMockup;
import discovery.test.common.ListenerMock;
import discovery.test.common.mockups.SearchControlMockup;
import discovery.base.search.SearchBase;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class SearchBaseStopTest implements Scenario{
    @steps
    var steps: SearchBaseStopSteps;

    @scenario
    @Test
    public function stop_search(){
        given().a_search_instance_with_a_search_control();
        when().stopping_the_search();
        then().the_search_control_should_be_stopped();
    }

    @scenario
    @Test
    public function notifying_stop_callback(){
        given().the_search_has_called_stop();
        when().the_search_control_notifies_it_stop();
        then().the_stop_callback_should_be_called();
    }

    @Test
    @scenario
    public function should_not_notify_discovery_after_stop_was_called(){
        given().a_search_instance_with_a_search_control();
        given().an_onFound_listener_was_registered();
        given().stop_was_called();
        when().a_service_is_discovered();
        then().the_onFound_listener_should_not_be_called();
    }

    @Test
    @scenario
    public function trying_to_stop_after_stop(){
        given().a_search_instance_was_stopped();
        when().trying_to_stop_again();
        then().the_search_control_should_be_stopped_only_once();
        but().the_stop_callback_should_be_called_again();
    }

    @Ignore
    @Test
    @scenario
    public function calling_stop_twice_before_completes(){
        given().a_search_instance_with_a_search_control();
        given().stop_was_called();
        when().stop_is_called_again();
        and().the_search_control_notifies_it_stop();
        then().the_search_control_should_be_stopped_only_once();
        but().the_stop_callback_should_be_called_again();
    }
}

class SearchBaseStopSteps extends SearchBaseCommonSteps<SearchBaseStopSteps>{
    var stopCallback:CallableMockup;

    public function new(){
        super();

        stopCallback = new CallableMockup();
    }


    // summary ---------------------------------------------------

    @step
    public function a_search_instance_was_stopped() {
        given().the_search_has_called_stop();
        and().the_search_control_notifies_it_stop();
    }

    @step
	public function the_search_has_called_stop() {
        given().a_search_instance_with_a_search_control();
        when().stopping_the_search();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function stop_was_called() {
        when().stopping_the_search();
    }

    @step
    public function stop_is_called_again() {
        when().stopping_the_search();
    }

    @step
    public function trying_to_stop_again() {
        when().stopping_the_search();
    }

    @step
    public function stopping_the_search() {
        search.stop(stopCallback.callable());
    }

    @step
    public function the_search_control_notifies_it_stop() {
        searchControl.notifyStop();
    }

    // then  ---------------------------------------------------

    @step
    public function the_search_control_should_be_stopped() {
        searchControl.assertWasStopped();
    }

    @step
    public function the_search_control_should_be_stopped_only_once() {
        searchControl.assertWasStoppedOnlyOnce();
    }

    @step
    public function the_stop_callback_should_be_called() {
        stopCallback.verifyCallbackCalled();
    }

    @step
    public function the_stop_callback_should_be_called_again() {
        stopCallback.verifyCallbackCalled(atLeast(2));
    }
}