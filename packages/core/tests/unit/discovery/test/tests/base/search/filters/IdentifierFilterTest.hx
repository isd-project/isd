package discovery.test.tests.base.search.filters;

import discovery.test.helpers.RandomGen;
import discovery.domain.Description;
import discovery.utils.filter.FilterResult;
import discovery.domain.Discovered;
import discovery.base.search.filters.IdentifierFilter;
import hxgiven.Steps;
import hxgiven.Scenario;
import haxe.io.Bytes;
import discovery.domain.Identifier;

import org.hamcrest.Matchers.*;


class IdentifierFilterTest implements Scenario{
    @steps
    var steps:IdentifierFilterSteps;

    @Test
    @scenario
    public function not_exclude_correspondent_service() {
        given().some_identifier();
        given().a_service_with_same_identifier_values();
        then().it_should_not_be_filtered_out();
    }

    @Test
    @scenario
    public function exclude_different_provider_id() {
        given().some_identifier();
        given().a_service_with_different_provider_id();
        then().it_should_be_filtered_out();
    }

    @Test
    @scenario
    public function should_not_exclude_different_instance_id_when_not_specified() {
        given().some_identifier_without_instance_id();
        given().a_service_with_some_instance_id();
        then().it_should_not_be_filtered_out();
    }

    @Test
    @scenario
    public function exclude_different_instance_id_when_specified() {
        given().some_identifier_with_instance_id();
        given().a_service_with_different_instance_id();
        then().it_should_be_filtered_out();
    }

    @Test
    @scenario
    public function does_not_exclude_service_with_same_instance_id() {
        given().some_identifier_with_instance_id();
        given().a_service_with_same_identifier_values();
        then().it_should_not_be_filtered_out();
    }

    @Test
    @scenario
    public function exclude_different_name() {
        given().some_identifier();
        given().a_service_with_different_name();
        then().it_should_be_filtered_out();
    }

    @Test
    @scenario
    public function exclude_different_types() {
        given().some_identifier();
        given().a_service_with_different_type();
        then().it_should_be_filtered_out();
    }
}

class IdentifierFilterSteps extends Steps<IdentifierFilterSteps>{
    var identifier:Identifier;
    var filter:IdentifierFilter;

    var discovered:Discovered;
    var srvDescription: Description;

    public function modifyBytes(bytes: Bytes): Bytes {
        var copy = bytes.sub(0, bytes.length);

        //bitwise negate last byte
        var lastIdx = bytes.length - 1;
        copy.set(lastIdx, ~bytes.get(lastIdx));

        return copy;
    }

    // given -------------------------------------------------------------

    @step
    public function some_identifier(?identifier: Identifier) {
        if(identifier == null){
            identifier = makeIdentifier();
        }

        this.identifier = identifier;
        filter = new IdentifierFilter(identifier);
    }

    @step
    public function some_identifier_without_instance_id(){
        given().some_identifier();
    }

    @step
    public function some_identifier_with_instance_id() {
        given().some_identifier();

        this.identifier.instanceId = Bytes.ofHex("127a3d");
    }

    function makeIdentifier(): Identifier {
        return {
            name: "my service name",
            type: "mytype",
            providerId: Bytes.ofHex("d9b4b6f7cc9582c6a328c833514aad0ea8377442942ee5b3d9e15b5478d74fd3")
        };
    }

    @step
    public function a_service_with_same_identifier_values() {
        discovered = genServiceFromIdentifier();
        srvDescription = discovered.description();
    }

    @step
    public function a_service_with_different_provider_id() {
        given().a_service_with_same_identifier_values();

        var providerId = srvDescription.providerId.id();
        srvDescription.providerId = modifyBytes(providerId);
    }

    @step
    public function a_service_with_some_instance_id() {
        given().a_service_with_same_identifier_values();
        // srvDescription.instanceId = Bytes.ofHex("dac34f");
        srvDescription.instanceId = RandomGen.binary.bytes(4);
    }

    @step
    public function a_service_with_different_instance_id() {
        given().a_service_with_same_identifier_values();
        srvDescription.instanceId = modifyBytes(srvDescription.instanceId);
    }

    @step
    public function a_service_with_different_name() {
        given().a_service_with_same_identifier_values();
        srvDescription.name = '${srvDescription.name}-modified';
    }

    @step
    public function a_service_with_different_type() {
        given().a_service_with_same_identifier_values();
        srvDescription.type = '${srvDescription.type}-2';
    }

    function genServiceFromIdentifier(): Discovered {
        var desc:Description = {
            name: identifier.name,
            type: identifier.type,
            providerId: identifier.providerId,
            instanceId: identifier.instanceId
        };
        return RandomGen.announcementFrom(desc);
    }

    // then -------------------------------------------------------------

    @step
    public function it_should_be_filtered_out() {
        then().should_exclude(this.discovered);
    }

    @step
    public function it_should_not_be_filtered_out() {
        then().should_not_exclude(this.discovered);
    }

    @step
    public function should_match_with_equals_identifier() {
        should_not_exclude(genServiceFromIdentifier());
    }


    @step
    public function should_exclude(discovered: Discovered) {
        verify_exclude(discovered, true);
    }

    @step
    public function should_not_exclude(discovered: Discovered) {
        verify_exclude(discovered, false);
    }

    function verify_exclude(discovered: Discovered, exclude:Bool) {
        var should = if(exclude) "should" else "should not";

        var expected:FilterResult = if(exclude) Exclude else Include;

        assertThat(filter.filter(discovered), is(expected),
            'Identifier filter for ${Std.string(identifier)} $should exclude Service ${Std.string(discovered)}'
        );
    }
}