package discovery.test.tests.base.publication;

import haxe.Exception;
import discovery.domain.PublishInfo;
import discovery.test.common.mockups.discovery.PublishListenersMockup;
import discovery.test.helpers.RandomGen;
import discovery.base.publication.CompositePublication;
import discovery.test.common.mockups.DiscoveryMechanismMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class CompositePublicationTest implements Scenario{
    @steps
    var steps: CompositePublicationSteps;

    @Test
    @scenario
    public function publish_all(){
        given().a_composite_publication_from_some_discovery_mechanisms();
        when().publishing_a_service();
        then().the_publication_should_be_delegated_to_all_mechanisms();
    }

    @Test
    @scenario
    public function notify_publication_once(){
        given().a_composite_publication_has_started();
        when().all_mechanisms_publish_with_success();
        then().the_publication_should_be_notified_only_once();
    }

    @Test
    @scenario
    public function notify_publication_finish(){
        given().a_composite_publication_has_started();
        when().all_mechanisms_finish();
        then().the_publication_end_should_be_notified();
    }

    @Test
    @scenario
    public function publication_with_partial_failures(){
        given().a_composite_publication_has_started();
        when().part_of_mechanisms_succeed_at_publication();
        but().other_mechanisms_end_publication_before_start();
        then().the_publication_start_should_be_notified();
    }

    @Test
    @scenario
    public function total_failure(){
        given().a_composite_publication_has_started();
        when().all_mechanisms_finish_without_publishing();
        then().the_publication_end_should_be_notified();
        but().the_publication_start_should_not_be_notified();
    }
}

class CompositePublicationSteps extends Steps<CompositePublicationSteps>{
    var compositePublisher:CompositePublication;

    var mechanisms:CompositeMockup<DiscoveryMechanismMockup>;
    var publishListeners:PublishListenersMockup;

    var publishInfo:PublishInfo;

    var nonStarted:Array<DiscoveryMechanismMockup>=[];

    public function new(){
        super();

        mechanisms = new CompositeMockup(()->new DiscoveryMechanismMockup());
        publishListeners = new PublishListenersMockup();

        mechanisms.populate();
    }


    // given summary ---------------------------------------------------

    @step
    public function a_composite_publication_has_started() {
        given().a_composite_publication_from_some_discovery_mechanisms();
        when().publishing_a_service();
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_publication_from_some_discovery_mechanisms() {
        var mechanisms = mechanisms.map((m)->m.mechanism);
        publishInfo = RandomGen.publishInfo();

        compositePublisher = new CompositePublication(
            mechanisms, publishInfo, publishListeners.listeners()
        );
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_a_service() {
        compositePublisher.start();
    }

    @step
    public function all_mechanisms_publish_with_success() {
        mechanisms.forEach((m)->m.notifyPublished());
    }

    @step
    public function all_mechanisms_finish() {
        mechanisms.forEach((m)->m.notifyPublicationFinished());
    }

    @step
    public function part_of_mechanisms_succeed_at_publication() {
        mechanisms.forSomeComponents(
            (m)->m.notifyPublished(),
            m->{
                nonStarted.push(m);
            }
        );
    }

    @step
    public function other_mechanisms_end_publication_before_start() {
        for(m in nonStarted){
            m.notifyPublicationFinished(new Exception('any error'));
        }
    }


    @step
    public function all_mechanisms_finish_without_publishing() {
        mechanisms.forEach((m) -> {
            m.notifyPublicationFinished(new Exception('any error'));
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_publication_should_be_delegated_to_all_mechanisms() {
        mechanisms.forEach((m)->m.checkPublishCalledWith(publishInfo));
    }

    @step
    public function the_publication_start_should_be_notified() {
        then().the_publication_should_be_notified_only_once();
    }

    @step
    public function the_publication_should_be_notified_only_once() {
        publishListeners.verifyOnPublishCalled(times(1));
    }

    @step
    public function the_publication_start_should_not_be_notified() {
        publishListeners.verifyOnPublishCalled(never);
    }

    @step
    public function the_publication_end_should_be_notified() {
        publishListeners.verifyOnFinishCalled(times(1));
    }
}