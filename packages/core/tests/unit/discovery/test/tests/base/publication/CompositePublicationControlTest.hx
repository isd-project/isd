package discovery.test.tests.base.publication;

import discovery.base.publication.CompositePublicationControl;
import discovery.test.common.mockups.discovery.PublicationControlMockup;
import discovery.test.common.mockups.CompositeMockup;
import discovery.base.publication.PublicationControl;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class CompositePublicationControlTest implements Scenario{
    @steps
    var steps: CompositePublicationControlSteps;

    @Test
    @scenario
    public function should_stop_all_components(){
        given().a_composite_of_publication_controls();
        when().stopping_it();
        then().all_component_controls_should_be_stopped();
    }
}

class CompositePublicationControlSteps
    extends Steps<CompositePublicationControlSteps>
{
    var compositeControl:PublicationControl;
    var controlsMockups:CompositeMockup<PublicationControlMockup>;

    public function new(){
        super();

        controlsMockups = new CompositeMockup(()->new PublicationControlMockup());
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_of_publication_controls() {
        controlsMockups.populate();

        var controls = controlsMockups.map((m)->m.control());

        compositeControl = new CompositePublicationControl(controls);
    }

    // when  ---------------------------------------------------

    @step
    public function stopping_it() {
        compositeControl.stop();
    }

    // then  ---------------------------------------------------

    @step
    public function all_component_controls_should_be_stopped() {
        controlsMockups.forEach((m)->{
            m.verifyStopCalled();
        });
    }
}