package discovery.test.tests.base.operators;

import discovery.domain.operators.exceptions.InvalidOperandException;
import haxe.Exception;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
import discovery.test.mockups.FakeOperator.*;

using discovery.domain.operators.Operators;
using discovery.test.matchers.CommonMatchers;


class ComparisonOperatorsTest implements Scenario{
    @steps
    var steps: ComparisonOperatorsSteps;

    @Test
    @scenario
    public function less_than_operator(){
        given().this_operator(
            attr("A").lessThan(attr("B"))
        );
        given().a_discovered_service_with_these_attributes({
            A: 1, B: 2
        });
        when().evaluating_the_operator();
        then().it_should_return(true);
    }


    @Test
    @scenario
    public function greater_than_operator(){
        given().this_operator(
            attr("A").greaterThan(attr("B"))
        );
        given().a_discovered_service_with_these_attributes({
            A: 1, B: 2
        });
        when().evaluating_the_operator();
        then().it_should_return(false);
    }


    @Test
    @scenario
    public function greater_than_or_equals_operator(){
        given().this_operator(
            attr("A").greaterThanOrEquals(attr("B")).AND(
                attr("B").greaterThanOrEquals(attr("B"))
            )
        );
        given().a_discovered_service_with_these_attributes({
            A: 2, B: 1
        });
        when().evaluating_the_operator();
        then().it_should_return(true);
    }


    @Test
    @scenario
    public function less_than_or_equals_operator(){
        given().this_operator(
            attr("A").lessThanOrEquals(attr("B")).AND(
                attr("B").lessThanOrEquals(attr("B"))
            )
        );
        given().a_discovered_service_with_these_attributes({
            A: 1, B: 2
        });
        when().evaluating_the_operator();
        then().it_should_return(true);
    }

    @Test
    @scenario
    public function comparing_with_invalid_value(){
        given().this_operator(
            attr("A").lessThan(attr("B"))
        );
        given().a_discovered_service_with_these_attributes({
            A: 1
        });
        when().trying_to_evaluate_the_operator();
        then().it_should_throw_an_exception_of(InvalidOperandException);
    }
}

class ComparisonOperatorsSteps
    extends OperatorBaseSteps<ComparisonOperatorsSteps>
{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}