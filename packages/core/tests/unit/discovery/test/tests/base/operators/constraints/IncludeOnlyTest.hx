package discovery.test.tests.base.operators.constraints;

import discovery.utils.filter.FilterResult;
import discovery.domain.operators.constraints.OperatorConstraint;
import discovery.domain.operators.Operator;
import discovery.domain.operators.constraints.IncludeOnly;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
import discovery.domain.operators.Operators.*;

using discovery.test.matchers.CommonMatchers;


class IncludeOnlyTest implements Scenario{
    @steps
    var steps: IncludeOnlySteps;

    @Test
    @scenario
    public function include_listed_types(){
        given().this_constraint(new IncludeOnly([Value, And, Equals]));
        when().applying_to_this_operator(value("example"));
        then().it_should_return(Include);
    }

    @Test
    @scenario
    public function exclude_unlisted_types(){
        given().this_constraint(new IncludeOnly([Value, And, Equals]));
        when().applying_to_this_operator(attr("myproperty"));
        then().it_should_return(Exclude);
    }

    @Test
    @scenario
    public function exclude_null(){
        given().this_constraint(new IncludeOnly([Value, And, Equals]));
        when().applying_to_this_operator(null);
        then().it_should_return(Exclude);
    }
}

class IncludeOnlySteps extends Steps<IncludeOnlySteps>{
    var constraint:OperatorConstraint;
    var result:FilterResult;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function this_constraint(constraint:OperatorConstraint) {
        this.constraint = constraint;
    }

    // when  ---------------------------------------------------

    @step
    public function applying_to_this_operator<T>(op:Operator<T>) {
        result = constraint.filter(op);
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_return(expected: FilterResult) {
        result.shouldBe(expected);
    }
}