package discovery.test.tests.base.configuration;

import discovery.base.configuration.composite.CompositeBuilder;
import discovery.keys.keydiscovery.CompositeKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.base.configuration.composite.CompositeKeyTransportBuilder;
import discovery.test.common.mockups.keytransport.KeyTransportBuilderMockup;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeKeyTransportBuilderTest implements Scenario{
    @steps
    var steps: CompositeKeyTransportBuilderSteps;

    @Test
    @scenario
    public function build_key_composite_from_key_transport_builders(){
        given().a_composite_builder();
        given().some_component_builders();
        when()
            .adding_the_component_builders();
            and().building_the_composite();
        then()
            .a_composite_key_transport_should_be_returned();
            and().all_components_should_be_built();
    }

    @Test
    @scenario
    public function filter_components_by_name(){
        given().a_composite_builder();
        given().some_named_component_mechanism_builders_were_added();
        when().filtering_out_some_components_by_name();
        and().building_the_composite();
        then().only_the_included_mechanisms_should_be_built();
    }

    @Test
    @scenario
    public function list_named_components(){
        given().a_composite_builder();
        given().some_named_component_mechanism_builders_were_added();
        when().listing_all_named_builders();
        then().all_added_builders_should_be_listed();
    }
}

class CompositeKeyTransportBuilderSteps
    extends CompositeBuilderBaseSteps<KeyTransportBuilder,
                                        KeyTransportBuilderMockup,
                                        CompositeKeyTransportBuilderSteps>
{
    var builtComposite:KeyTransport;

    public function new(){
        super();
    }

    // -------------------------------------------------------------------

    function createCompositeBuilder():CompositeBuilder<KeyTransportBuilder> {
        return new CompositeKeyTransportBuilder();
    }

    function makeBuilderMockup():KeyTransportBuilderMockup {
        return new KeyTransportBuilderMockup();
    }

    function getBuilder(m:KeyTransportBuilderMockup):KeyTransportBuilder {
        return m.builder();
    }

    function buildComposite(composite:CompositeBuilder<KeyTransportBuilder>) {
        builtComposite = cast(composite, KeyTransportBuilder)
                            .buildKeyTransport();
    }

    function verifyBuilt(m:KeyTransportBuilderMockup) {
        m.verifyBuildCalled();
    }

    function verifyNotBuilt(mockup:KeyTransportBuilderMockup) {
        mockup.verifyBuildCalled(never);
    }

    function verifyCompositeBuilt() {
        builtComposite.shouldBeAn(CompositeKeyTransport);
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

    @step
    public function a_composite_key_transport_should_be_returned() {
        builtComposite.shouldBeAn(CompositeKeyTransport);
    }
}