package discovery.test.tests.base.search;

import discovery.utils.filter.FilterResult;
import discovery.base.search.SearchFilter;
import discovery.test.common.mockups.SearchControlMockup;
import discovery.base.search.SearchBase;
import discovery.domain.Search;
import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class SearchBaseTest implements Scenario{
    @steps
    var steps: SearchBaseSteps;

    @Test
    @scenario
    public function apply_filter(){
        given().a_negate_filter();
        and().a_search_instance();
        when().the_filter_is_applied();
        and().a_service_is_discovered();
        then().the_service_should_be_filtered_out();
    }
}

class SearchBaseSteps extends Steps<SearchBaseSteps>{

    var discovered: Discovered;
    var foundServices: Array<Discovered>;

    var filter:SearchFilter;
    var search:SearchBase;
    var searchControl:SearchControlMockup;

    public function new(){
        super();

        foundServices = [];

        searchControl = new SearchControlMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_negate_filter() {
        filter = new NegateFilter();
    }

    @step
    public function a_search_instance() {
        search = new SearchBase(searchControl.searchControl);

        search.onFound.listen((discovered)->{
            this.foundServices.push(discovered);
        });
    }

    // when  ---------------------------------------------------

    @step
    public function the_filter_is_applied() {
        search.applyFilter(filter);
    }

    @step
    public function a_service_is_discovered() {
        discovered = RandomGen.discovered();

        search.discovered(discovered);
    }

    // then  ---------------------------------------------------

    @step
    public function the_service_should_be_filtered_out() {
        assertThat(foundServices, not(hasItem(discovered)));
    }
}

class NegateFilter implements SearchFilter {
    public function new() {}

    public function filter(discovered:Discovered):FilterResult {
        return Exclude;
    }
}