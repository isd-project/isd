package discovery.test.tests.base;

import discovery.test.common.CallableMockup;
import discovery.base.publication.PublicationControl;
import discovery.base.publication.CompositePublicationControl;
import discovery.domain.PublishListeners;
import discovery.test.common.mockups.discovery.PublishListenersMockup;
import discovery.domain.PublishInfo;
import discovery.domain.Identifier;
import discovery.domain.Search;
import discovery.base.search.CompositeSearch;
import discovery.test.helpers.RandomGen;
import discovery.domain.Query;
import discovery.base.CompositeDiscoveryMechanism;
import discovery.test.common.mockups.CompositeMechanismsMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeDiscoveryMechanismTest implements Scenario{
    @steps
    var steps: CompositeDiscoveryMechanismSteps;

    @Test
    @scenario
    public function composite_search(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().some_search_query();
        when().sending_the_query_to_the_composite();
        then().the_query_should_be_delegated_to_all_component_mechanisms();
        and().a_composite_search_should_be_returned();
    }

    @Test
    @scenario
    public function composite_localization(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().some_service_identity();
        when().locating_that_service_using_the_composite();
        then().the_locate_operation_should_be_delegated_to_the_mechanisms();
        and().a_composite_search_should_be_returned();
    }


    // publication -------------------------------------------------------

    @Test
    @scenario
    public function composite_publication(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().some_service_publication_info();
        when().publishing_that_service();
        then().the_publication_should_be_delegated_to_all_mechanisms();
        and().a_composite_publication_control_should_be_returned();
    }


    @Test
    @scenario
    public function composite_onPublish_notification(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().a_publish_listener();
        given().a_service_was_published();
        when().all_mechanisms_publish_the_service();
        then().the_on_publish_listener_should_be_called();
    }

    @Test
    @scenario
    public function finish_composite_publication(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().a_publication_has_started();
        when().all_components_finish_the_publication();
        then().the_publication_finish_should_be_notified();
    }

    // finish mechanisms --------------------------------------------------

    @Test
    @scenario
    public function finish_composite_mechanism(){
        given().a_composite_mechanism_with_some_mechanisms();
        when().finishing_the_mechanism();
        then().all_component_mechanisms_should_be_finished();
    }


    @Test
    @scenario
    public function notify_composite_mechanism_finish(){
        given().a_composite_mechanism_with_some_mechanisms();
        given().the_composite_mechanism_was_finished();
        when().all_component_mechanisms_finishes();
        then().the_finish_listener_should_be_notified();
    }
}

class CompositeDiscoveryMechanismSteps
    extends Steps<CompositeDiscoveryMechanismSteps>
{
    var compositeMechanism:CompositeDiscoveryMechanism;

    var mechanismsMockup:CompositeMechanismsMockup;
    var publishListenerMockup:PublishListenersMockup;

    var query:Query;
    var identifier:Identifier;
    var search:Search;

    var publishInfo:PublishInfo;
    var publishListener:PublishListeners;
    var publishControl:PublicationControl;

    var finishListener:CallableMockup;

    public function new(){
        super();

        mechanismsMockup = new CompositeMechanismsMockup();
        publishListenerMockup = new PublishListenersMockup();
        finishListener = new CallableMockup();
    }


    // summary ---------------------------------------------------

    @step
    public function a_publication_has_started() {
        given().a_publish_listener();
        given().a_service_was_published();
    }

    @step
    public function the_composite_mechanism_was_finished() {
        when().finishing_the_mechanism();
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_mechanism_with_some_mechanisms() {
        mechanismsMockup.populate();

        compositeMechanism = new CompositeDiscoveryMechanism(
            mechanismsMockup.mechanisms()
        );
    }

    @step
    public function some_search_query() {
        query = RandomGen.typeQuery();
    }

    @step
    public function some_service_identity() {
        identifier = RandomGen.identifier();
    }

    @step
    public function some_service_publication_info() {
        publishInfo = RandomGen.publishInfo();
    }

    @step
    public function a_publish_listener() {
        publishListener = publishListenerMockup.listeners();
    }

    @step
    public function a_service_was_published() {
        given().some_service_publication_info();
        when().publishing_that_service();
    }

    // when  ---------------------------------------------------

    @step
    public function sending_the_query_to_the_composite() {
        search = compositeMechanism.search(query);
    }

    @step
    public function locating_that_service_using_the_composite() {
        search = compositeMechanism.locate(identifier);
    }

    @step
    public function publishing_that_service() {
        publishControl = compositeMechanism.publish(publishInfo, publishListener);
    }

    @step
    public function all_mechanisms_publish_the_service() {
        mechanismsMockup.forEach((mockup)->{
            mockup.notifyPublished();
        });
    }

    @step
    public function all_components_finish_the_publication() {
        mechanismsMockup.forEach((m)->{
            m.notifyPublicationFinished();
        });
    }


    @step
    public function finishing_the_mechanism() {
        compositeMechanism.finish(finishListener.callable());
    }

    @step
    public function all_component_mechanisms_finishes() {
        mechanismsMockup.forEach((m)->{
            m.notifyFinished();
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_query_should_be_delegated_to_all_component_mechanisms() {
        mechanismsMockup.forEach((mockup)->{
            mockup.checkSearchCalledWith(query);
        });
    }

    @step
    public
    function the_locate_operation_should_be_delegated_to_the_mechanisms() {
        mechanismsMockup.forEach((mockup)->{
            mockup.checkLocateCalledWith(identifier);
        });
    }

    @step
    public function a_composite_search_should_be_returned() {
        search.shouldBeAn(CompositeSearch);
    }

    @step
    public function a_composite_publication_control_should_be_returned() {
        publishControl.shouldBeAn(CompositePublicationControl);
    }

    @step
    public function the_publication_should_be_delegated_to_all_mechanisms() {
        mechanismsMockup.forEach((mockup)->{
            mockup.checkPublishCalledWith(publishInfo);
        });
    }

    @step
    public function the_on_publish_listener_should_be_called() {
        publishListenerMockup.verifyOnPublishCalled();
    }

    @step
    public function the_publication_finish_should_be_notified() {
        publishListenerMockup.verifyOnFinishCalled();
    }

    @step
    public function all_component_mechanisms_should_be_finished() {
        mechanismsMockup.forEach((mockup)->{
            mockup.checkFinishCalled();
        });
    }

    @step
    public function the_finish_listener_should_be_notified() {
        finishListener.verifyCallbackCalled();
    }
}