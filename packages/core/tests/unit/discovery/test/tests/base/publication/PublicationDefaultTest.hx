package discovery.test.tests.base.publication;

import discovery.test.helpers.RandomGen;
import discovery.domain.PublishInfo;
import discovery.test.common.PublicationListenerMockups.PublicationFinishListener;
import discovery.domain.PublishListeners;
import discovery.base.publication.PublicationControl;
import discovery.test.common.PublicationListenerMockups.PublicationListener;
import haxe.Exception;
import discovery.domain.Publication;
import hxgiven.Steps;
import hxgiven.Scenario;
import discovery.base.publication.PublicationDefault;


import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;

class PublicationDefaultTest implements Scenario{
    @steps
    var steps:PublicationDefaultSteps;

    @Test
    @scenario
    public function creating_Publication() {
        given().a_discovery_mechanism_instance();
        when().creating_a_Publication_from_it();
        and().the_publication_events_should_be_valid();
    }

    @Test
    @scenario
    public function starting_Publication() {
        given().a_Publication_was_created();
        given().some_publish_info();
        when().starting_the_publication();
        then().the_publication_should_be_delegated_to_the_discovery_mechanism();
        and().the_publication_listeners_should_be_passed_to_the_mechanism();
    }

    @Test
    @scenario
    public function notify_publication_published() {
        given().a_Publication_was_started();
        and().some_listeners_was_registered_to_the_publication_events();
        when().the_mechanism_publishes_the_service();
        then().the_oncePublished_event_should_be_notified();
    }

    @Test
    @scenario
    public function notify_publication_finishes() {
        given().a_Publication_was_started();
        and().some_listeners_was_registered_to_the_publication_events();
        when().the_mechanism_finishes_the_service();
        then().the_onceFinish_event_should_be_notified();
    }

    @Test
    @scenario
    public function notify_publication_fails() {
        given().a_Publication_was_started();
        and().some_listeners_was_registered_to_the_publication_events();
        when().the_mechanism_fails();
        then().the_onceFinish_event_should_be_notified_with_the_error();
    }


    @Test
    @scenario
    public function stopping_publication() {
        given().a_Publication_was_started();
        and().some_listeners_was_registered_to_the_publication_events();
        when().stopping_the_publication();
        then().the_publication_control_should_be_cancelled();
    }
}



class PublicationDefaultSteps extends Steps<PublicationDefaultSteps> {
    var publication: PublicationDefault;
    var mechanism:DiscoveryMechanism;
    var publishInfo:PublishInfo;
    var control: PublicationControl;

    var capturedPublishInfo:PublishInfo;
    var capturedPublishListeners:PublishListeners;

    var capturedOnPublishInfo:PublishedEventInfo;
    var onStartListener:PublicationListener;
    var onFinishListener:PublicationFinishListener;

    var expectedError:Exception;

    public function new() {
        super();

        expectedError = null;
        control = mock(PublicationControl);
    }

    function onPublish(args: Array<Dynamic>) {
        assertThat(args, hasSize(greaterThanOrEqualTo(2)));

        capturedPublishInfo = args[0];
        capturedPublishListeners = args[1];
    }

    // given ------------------------------------------------------

    @step
    public function a_discovery_mechanism_instance() {
        mechanism = mock(DiscoveryMechanism);
        Mockatoo.when(mechanism.publish(any, any)).thenCall(onPublish);
    }

    @step
    public function some_publish_info() {
        publishInfo = PublishInfo.make(RandomGen.fullAnnouncement());
    }

    @step
    public function some_event_listeners() {
        onStartListener = mock(PublicationListener);
        onFinishListener = mock(PublicationListener);
    }

    @step
    public function a_Publication_was_created() {
        given().a_discovery_mechanism_instance();
        when().creating_a_Publication_from_it();
    }

    @step
    public function a_Publication_was_started() {
        given().a_Publication_was_created();
        given().some_publish_info();
        when().starting_the_publication();
    }

    @step
    public function some_listeners_was_registered_to_the_publication_events() {
        when().registering_some_listeners_to_events();
    }

    // when ------------------------------------------------------
    @step
    public function creating_a_Publication_from_it() {
        publication = new PublicationDefault(mechanism);
    }

    @step
    public function starting_the_publication() {
        publication.start(publishInfo);
    }

    @step
    public function registering_some_listeners_to_events() {
        given().some_event_listeners();

        function oncePublished(publishedInfo: PublishedEventInfo){
            capturedOnPublishInfo = publishedInfo;
        }

        publication.oncePublished.listen(oncePublished);
        publication.onceFinished.listen(onFinishListener.onEvent);
    }

    @step
    public function the_mechanism_publishes_the_service() {
        then().the_publication_listeners_should_be_passed_to_the_mechanism();

        capturedPublishListeners.onPublish();
    }

    @step
    public function the_mechanism_finishes_the_service() {
        when().the_mechanism_finishes_with();
    }

    @step
    public function the_mechanism_fails() {
        when().the_mechanism_finishes_with(new Exception("some exception"));
    }


    @step
    public function the_mechanism_finishes_with(?error: Exception) {
        then().the_publication_listeners_should_be_passed_to_the_mechanism();

        expectedError = error;
        capturedPublishListeners.onFinish(error);
    }

    @step
    public function stopping_the_publication() {
        publication.stop();
    }

    // then ------------------------------------------------------

    @step
    public function the_publication_events_should_be_valid() {
        assertThat(publication.oncePublished, is(notNullValue()));
        assertThat(publication.onceFinished, is(notNullValue()));
    }

    @step
    public function the_publication_should_be_delegated_to_the_discovery_mechanism() {
        var self = this;
        Mockatoo.verify(mechanism.publish(isNotNull, isNotNull));
    }

    @step
    public
    function the_publication_listeners_should_be_passed_to_the_mechanism(){
        assertThat(capturedPublishListeners, is(notNullValue()), "should have captured publish listeners");
    }

    @step
    public function the_oncePublished_event_should_be_notified() {
        assertThat(capturedOnPublishInfo, is(notNullValue()),
            "Published info was not captured");
        assertThat(capturedOnPublishInfo.publication, is(equalTo(publication)),
            "captured publication does not match expected"
        );
        assertThat(capturedOnPublishInfo.published, is(notNullValue()),
            "missing publish info"
        );
    }

    @step
    public function the_onceFinish_event_should_be_notified() {
        var matcher = function(result:PublicationFinished){
            return result.error == expectedError
                && result.publication == publication;
        };

        var self = this;
        verify(onFinishListener.onEvent(customMatcher(matcher)));
    }

    @step
    public function the_onceFinish_event_should_be_notified_with_the_error() {
        then().the_onceFinish_event_should_be_notified();
    }


    @step
    public function the_publication_control_should_be_cancelled() {

    }
}