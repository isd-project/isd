package discovery.test.tests.base.operators;

import discovery.domain.operators.serialization.OperatorSerializer;
import discovery.domain.operators.Operator;
import discovery.domain.operators.serialization.OperatorData;
import discovery.domain.operators.basic.ValueOperator;
import discovery.domain.operators.Operators;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;


class OperatorSerializerTest implements Scenario{
    @steps
    var steps: OperatorSerializerSteps;

    @Test
    @scenario
    public function serialize_value(){
        given().a_serializator_instance();
        when().serializing_this_operator(Operators.value("lorem ipsum"));
        then().it_should_produce_this_result({
            type: "value",
            operands: ["lorem ipsum"]
        });
    }


    @Test
    @scenario
    public function serialize_attribute(){
        given().a_serializator_instance();
        when().serializing_this_operator(Operators.attr("myproperty"));
        then().it_should_produce_this_result({
            type: "attribute",
            operands: ["myproperty"]
        });
    }


    @Test
    @scenario
    public function serialize_equals(){
        given().a_serializator_instance();
        when().serializing_this_operator(
            Operators.equals(Operators.attr("amount"), Operators.value(1)));
        then().it_should_produce_this_result({
            type: "equals",
            operands: [
                { type:"attribute", operands: ["amount"]},
                { type:"value", operands: [1]}
            ]
        });
    }

    @Test
    @scenario
    public function serialize_mixed(){
        given().a_serializator_instance();
        when().serializing_this_operator(
            Operators.equals(
                Operators.equals(Operators.attr("amount"), Operators.value(1)),
                Operators.value(false)
            )
        );
        then().it_should_produce_this_result({
            type: "equals",
            operands: [
                {
                    type: "equals",
                    operands: [
                        { type:"attribute", operands: ["amount"]},
                        { type:"value", operands: [1]}
                    ]
                },
                {   type: "value", operands: [false]}
            ]
        });
    }
}

class OperatorSerializerSteps extends Steps<OperatorSerializerSteps>{

    var serializer:OperatorSerializer;

    var result:OperatorData;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_serializator_instance() {
        serializer = new OperatorSerializer();
    }

    // when  ---------------------------------------------------

    @step
    public function serializing_this_operator<T>(op: Operator<T>) {
        result = serializer.serialize(op);
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_produce_this_result(expected:OperatorData) {
        result.shouldBeEqualsTo(expected);
    }
}