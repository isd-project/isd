package discovery.test.tests.base.search.filters;

import discovery.domain.operators.BooleanExpression;
import discovery.base.search.filters.ExpressionFilter;
import discovery.base.search.SearchFilter;
import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.test.mockups.ExpressionMockup;
import discovery.utils.filter.FilterResult;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ExpressionFilterTest implements Scenario{
    @steps
    var steps: ExpressionFilterSteps;

    @Test
    @scenario
    public function include_if_expression_evaluate_to_true(){
        given().a_filter_from_some_expression();
        given().a_discovered_service_that_evaluates_the_expression_to(true);
        when().filtering_that_discovered_service();
        then().that_service_should_be_included();
    }

    @Test
    @scenario
    public function exclude_if_expression_evaluate_to_false(){
        given().a_filter_from_some_expression();
        given().a_discovered_service_that_evaluates_the_expression_to(false);
        when().filtering_that_discovered_service();
        then().that_service_should_be_excluded();
    }

    @Test
    @scenario
    public function include_anything_if_expression_is_null(){
        given().a_filter_from_this_expression(null);
        given().any_discovery_service();
        when().filtering_that_discovered_service();
        then().that_service_should_be_included();
    }
}

class ExpressionFilterSteps extends Steps<ExpressionFilterSteps>{
    var filter:SearchFilter;

    var expressionMockup:ExpressionMockup;
    var filterResult:FilterResult;

    var discovered:Discovered;

    public function new(){
        super();

        expressionMockup = new ExpressionMockup();
    }

    // given ---------------------------------------------------
    @step
    public function a_filter_from_some_expression() {
        given().a_filter_from_this_expression(expressionMockup.expression());
    }

    @step
    public function a_filter_from_this_expression(expression: BooleanExpression) {
        filter = new ExpressionFilter(expression);
    }

    @step
    public function
        a_discovered_service_that_evaluates_the_expression_to(result:Bool)
    {
        given().any_discovery_service();

        expressionMockup.shouldEvaluateTo(result);
    }

    @step
    public function any_discovery_service() {
        discovered = RandomGen.discovered();
    }

    // when  ---------------------------------------------------

    @step
    public function filtering_that_discovered_service() {
        filterResult = filter.filter(discovered);
    }

    // then  ---------------------------------------------------

    @step
    public function that_service_should_be_included() {
        then().the_filter_result_should_be(Include);
    }

    @step
    public function that_service_should_be_excluded() {
        then().the_filter_result_should_be(Exclude);
    }

    @step
    function the_filter_result_should_be(expected:FilterResult) {
        assertThat(filterResult, is(equalTo(expected)),
            'Filter result (${filterResult}) does not match the expected (${expected})');
    }
}