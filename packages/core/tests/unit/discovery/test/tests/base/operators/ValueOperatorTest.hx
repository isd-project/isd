package discovery.test.tests.base.operators;

import discovery.domain.operators.basic.ValueOperator;
import discovery.domain.operators.Operators;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;


class ValueOperatorTest implements Scenario{
    @steps
    var steps: ValueOperatorSteps;

    @Test
    @scenario
    public function string_value(){
        var value = new ValueOperator("hello");

        value.evaluate(null).shouldBe("hello");
    }
}

class ValueOperatorSteps extends Steps<ValueOperatorSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}