package discovery.test.tests.base.search;

import discovery.test.common.CallableMockup;
import discovery.base.search.Stoppable.StopCallback;
import haxe.Exception;
import discovery.test.helpers.RandomGen;
import discovery.base.search.SearchBase.FinishInfo;
import discovery.domain.Search.FinishSearchEvent;
import discovery.test.common.ListenerMock;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class SearchBaseFinishTest implements Scenario{
    @steps
    var steps: SearchBaseFinishSteps;

    @Test
    @scenario
    public function notify_finish(){
        given().a_search_instance_with_a_search_control();
        given().a_finish_listener_is_registered();
        when().the_search_is_finished();
        then().the_finish_listener_should_be_notified();
    }


    @Test
    @scenario
    public function discovered_after_finished(){
        given().a_search_instance_was_finished();
        given().an_onFound_listener_was_registered();
        when().a_service_is_discovered();
        then().the_onFound_listener_should_not_be_called();
    }


    @Test
    @scenario
    public function received_finished_value(){
        given().a_search_instance_was_finished_with_some_value();
        when().a_finish_listener_is_registered();
        then().the_finish_listener_should_be_notified_with_the_expected_value();
    }

    @Test
    @scenario
    public function finish_after_stop(){
        given().a_search_instance_with_a_search_control();
        given().a_finish_listener_is_registered();
        given().a_stop_callback();
        when().the_search_is_stopped();
        then().the_search_should_be_finished();
        and().the_stop_callback_should_be_called();
    }
}

class SearchBaseFinishSteps extends SearchBaseCommonSteps<SearchBaseFinishSteps>{
    var finishListener:ListenerMock<FinishSearchEvent>;
    var finishInfo:FinishInfo;
    var stopCallback:StopCallback;
    var stopCallbackMockup:CallableMockup;

    public function new(){
        super();

        finishListener = new ListenerMock();
        stopCallbackMockup = new CallableMockup();
    }

    // summary ---------------------------------------------------

    @step
    public function a_search_instance_was_finished() {
        given().a_search_instance_was_finished_with();
    }

    @step
    public function a_search_instance_was_finished_with_some_value() {
        var finishInfo:FinishInfo = {
            error: RandomGen.primitives.maybe(new Exception("error"))
        };

        given().a_search_instance_was_finished_with(finishInfo);
    }

    @step
    function a_search_instance_was_finished_with(?value:FinishInfo) {
        given().a_search_instance_with_a_search_control();
        when().the_search_is_finished_with(value);
    }

    // given ---------------------------------------------------

    @step
    public function a_finish_listener_is_registered() {
        search.onceFinished.listen(finishListener.onEvent);
    }

    @step
    public function a_stop_callback() {
        stopCallback = stopCallbackMockup.callable();
    }

    // when  ---------------------------------------------------

    @step
    public function the_search_is_finished() {
        when().the_search_is_finished_with();
    }

    @step
    function the_search_is_finished_with(?value:FinishInfo) {
        this.finishInfo = value;

        search.finished(value);
    }

    @step
    public function the_search_is_stopped() {
        search.stop(stopCallback);
        searchControl.notifyStop();
    }

    // then  ---------------------------------------------------

    @step
    public function the_search_should_be_finished() {
        then().the_finish_listener_should_be_notified();
    }


    @step
    public function the_finish_listener_should_be_notified() {
        finishListener.assertWasCalled();
    }

    @step
    public function
        the_finish_listener_should_be_notified_with_the_expected_value()
    {
        finishListener.assertWasCalledWith(finishInfo);
    }

    @step
    public function the_stop_callback_should_be_called() {
        stopCallbackMockup.verifyCallbackCalled();
    }
}