package discovery.test.tests.base.configuration;

import discovery.base.configuration.composite.CompositeBuilder;
import discovery.base.configuration.NamedValue.Named;
import haxe.Constraints.IMap;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Steps;

using discovery.utils.IteratorTools;
using discovery.test.matchers.CommonMatchers;


abstract
class CompositeBuilderBaseSteps<Builder, BuilderMockup,
    Self:CompositeBuilderBaseSteps<Builder, BuilderMockup, Self>>
    extends Steps<Self>
{

    var compositeBuilder:CompositeBuilder<Builder>;
    var builderMockups:CompositeMockup<BuilderMockup>;

    var namedBuilders:IMap<String, BuilderMockup>;
    var listedBuilders:Iterable<Named<Builder>>;

    var includedToBuild:Array<BuilderMockup>;
    var excludedFromBuild:Array<BuilderMockup>;

    public function new(){
        super();

        builderMockups = new CompositeMockup(makeBuilderMockup);

        namedBuilders = new Map<String, BuilderMockup>();
        includedToBuild = [];
        excludedFromBuild = [];
    }

    abstract function createCompositeBuilder():CompositeBuilder<Builder>;
    abstract function makeBuilderMockup(): BuilderMockup;
    abstract function getBuilder(m:BuilderMockup):Builder;
    abstract function buildComposite(composite:CompositeBuilder<Builder>):Void;
    abstract function verifyBuilt(m:BuilderMockup):Void;
    abstract function verifyNotBuilt(mockup:BuilderMockup): Void;

    // given summary -------------------------------------------

    @step
    public function some_named_component_mechanism_builders_were_added() {
        given().some_component_builders();
        when().adding_the_component_builders();
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_builder() {
        compositeBuilder = createCompositeBuilder();
    }

    @step
    public function some_component_builders() {
        builderMockups.populate();
    }

    // when  ---------------------------------------------------

    @step
    public function adding_the_component_builders() {
        builderMockups.forEach((m)->{
            var name = RandomGen.primitives.name();

            compositeBuilder.addBuilder(getBuilder(m), name);
            namedBuilders.set(name, m);
        });
    }

    @step
    public function building_the_composite() {
        buildComposite(compositeBuilder);
    }


    @step
    public function filtering_out_some_components_by_name() {
        var toIncludeNames:Array<String> = [];

        var allBuilders = namedBuilders.keys().toArray();
        var numBuilders = allBuilders.length;
        var numberToInclude = RandomGen.primitives.int(1, numBuilders - 1);

        allBuilders = RandomGen.primitives.shuffle(allBuilders);

        for (i in 0...numBuilders){
            var name = allBuilders[i];
            var builder = namedBuilders.get(name);

            if(i < numberToInclude){
                toIncludeNames.push(name);
                includedToBuild.push(builder);
            }
            else{
                excludedFromBuild.push(builder);
            }
        }

        compositeBuilder.selectBuilders(toIncludeNames);
    }

    @step
    public function listing_all_named_builders() {
        listedBuilders = compositeBuilder.listBuilders();
    }

    // then  ---------------------------------------------------

    @step
    public function all_components_should_be_built() {
        builderMockups.forEach((m)->{
            verifyBuilt(m);
        });
    }

    @step
    public function only_the_included_mechanisms_should_be_built() {
        for(mockup in includedToBuild){
            verifyBuilt(mockup);
        }
        for(mockup in excludedFromBuild){
            verifyNotBuilt(mockup);
        }
    }

    @step
    public function all_added_builders_should_be_listed() {
        var expected:Array<Named<Builder>> = [
            for(k=>mock in namedBuilders.keyValueIterator()) {
                name: k,
                value: getBuilder(mock)
        }];

        listedBuilders.shouldContainsInAnyOrder(expected);
    }
}