package discovery.test.tests.base.operators;

import discovery.test.mockups.FakeOperator;
import discovery.utils.filter.FilterResult;
import discovery.test.mockups.FakeConstraint;
import discovery.domain.operators.constraints.OperatorConstraint;
import discovery.domain.operators.constraints.IncludeOnly;
import discovery.domain.operators.serialization.OperatorSerializer;
import discovery.domain.operators.OperatorType;
import discovery.domain.operators.Operator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
using discovery.domain.operators.Operators;

using discovery.test.matchers.CommonMatchers;


class ConstraintOperatorsTest implements Scenario{
    @steps
    var steps: ConstraintOperatorsSteps;

    @Test
    @scenario
    public function constraint_operators(){
        given().this_expression(
            attr("devicetype").equals(value("TV")).AND(
                attr("latency").lessThan(value(500))
            )
        );
        when().constraint_it_to_only_these_operators([
            Attribute, Value, Equals, And
        ]);
        then().this_expression_should_be_generated(
            attr("devicetype").equals(value("TV"))
        );
    }


    @Test
    @scenario
    public function return_operator_when_included_by_constraint(){
        given().this_expression(value("example"));
        given().a_constraint_that_includes_the_operator();
        when().applying_the_constraint();
        then().this_expression_should_be_generated(
            value("example")
        );
    }


    @Test
    @scenario
    public function return_null_when_operator_excluded_by_constraint(){
        given().this_expression(value("example"));
        given().a_constraint_that_excludes_the_operator();
        when().applying_the_constraint();
        then().this_expression_should_be_generated(null);
    }

    @Test
    @scenario
    public function recursive_constraint(){
        given().this_expression(
            customOperator("X").equals(customOperator("Y"))
        );
        when().constraint_it_to_only_these_operators([
            "X", Equals
        ]);
        then().this_expression_should_be_generated(
            null
        );
    }

    @Test
    @scenario
    public function transform_AND_operator(){
        given().this_expression(
            AND(cast customOperator("X"), cast customOperator("Y"))
        );
        when().constraint_it_to_only_these_operators([
            "X", And
        ]);
        then().this_expression_should_be_generated(
            customOperator("X")
        );
    }

    @Test
    @scenario
    public function transform_AND_operator_from_unsupported_operands(){
        given().this_expression(
            AND(cast customOperator("X"), cast customOperator("Y"))
        );
        when().constraint_it_to_only_these_operators([And]);
        then().this_expression_should_be_generated(null);
    }


    @Test
    @scenario
    public function keep_AND_operator_if_operands_are_valid(){
        var expr = AND(cast customOperator("X"), cast customOperator("Y"));

        given().this_expression(expr);
        when().constraint_it_to_only_these_operators([And, "X", "Y"]);
        then().this_expression_should_be_generated(expr);
    }


    @Test
    @scenario
    public function transform_OR_operartor_with_invalid_operand(){
        given().this_expression(
            OR(cast customOperator("X"), cast customOperator("Y")));
        when().constraint_it_to_only_these_operators([Or, "X"]);
        then().this_expression_should_be_generated(null);
    }

	function customOperator(type:String): Operator<Any> {
        return new FakeOperator(type);
	}
}

class ConstraintOperatorsSteps extends Steps<ConstraintOperatorsSteps>{
    var op:Operator<Any>;
    var transformed:Operator<Any>;

    var constraint:OperatorConstraint;

    var serializer:OperatorSerializer;

    public function new(){
        super();

        serializer = new OperatorSerializer();
    }

    // given ---------------------------------------------------

    @step
    public function this_expression<T>(op:Operator<T>) {
        this.op = op;
    }

    @step
    public function a_constraint_that_includes_the_operator() {
        given().a_constraint_that_returns(Include);
    }

    @step
    public function a_constraint_that_excludes_the_operator() {
        given().a_constraint_that_returns(Exclude);
    }

    @step
    function a_constraint_that_returns(result:FilterResult) {
        constraint = new FakeConstraint(result);
    }

    // when  ---------------------------------------------------

    @step
    public function
        constraint_it_to_only_these_operators(types:Array<OperatorType>)
    {
        when().applying_this_constraint(new IncludeOnly(types));
    }

    @step
    public function applying_the_constraint() {
        when().applying_this_constraint(constraint);
    }

    @step
    function applying_this_constraint(constraint: OperatorConstraint) {
        transformed = op.constrain(constraint);
    }

    // then  ---------------------------------------------------

    @step
    public function this_expression_should_be_generated<T>(expected:Operator<T>){
        var transformedExpr = serializer.serialize(transformed);
        var expectedExpr = serializer.serialize(expected);

        transformedExpr.shouldBeEqualsTo(expectedExpr);
    }
}