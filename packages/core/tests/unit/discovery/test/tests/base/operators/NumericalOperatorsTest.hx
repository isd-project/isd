package discovery.test.tests.base.operators;

import discovery.domain.operators.exceptions.InvalidOperandException;
import discovery.domain.operators.OperatorType;
import hxgiven.Context;
import discovery.test.helpers.RandomGen;
import discovery.domain.operators.serialization.OperatorData;
import discovery.domain.operators.Operator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
import discovery.test.mockups.FakeOperator.*;

using discovery.domain.operators.Operators;
using discovery.test.matchers.CommonMatchers;

typedef OperatorTestArguments<TInput, TOut>  = {
    op: Operator<TOut>,
    v1: TInput,
    v2: TInput,
    expectedResult: TOut,
    expectedSerialized: OperatorData
};

class NumericalOperatorsTest implements Scenario{
    @steps
    var steps: NumericalOperatorsSteps;

    @Test
    @scenario
    public function sum_operator(){
        var v1 = number();
        var v2 = number();

        test_operator({
            op: attr("A").plus(attr("B")),
            v1: v1,
            v2: v2,
            expectedResult: v1+v2,
            expectedSerialized: {
                type: Sum,
                operands: [
                    {type: Attribute, operands: ["A"]},
                    {type: Attribute, operands: ["B"]}
                ]
            }
        });
    }


    @Test
    @scenario
    public function subtraction_operator(){
        var v1 = number();
        var v2 = number();

        test_operator({
            op: attr("A").minus(attr("B")),
            v1: v1,
            v2: v2,
            expectedResult: v1-v2,
            expectedSerialized: {
                type: Subtraction,
                operands: [
                    {type: Attribute, operands: ["A"]},
                    {type: Attribute, operands: ["B"]}
                ]
            }
        });
    }


    @Test
    @scenario
    public function multiplication_operator(){
        var v1 = number();
        var v2 = number();

        test_operator({
            op: attr("A").times(attr("B")),
            v1: v1,
            v2: v2,
            expectedResult: v1*v2,
            expectedSerialized: {
                type: Multiplication,
                operands: [
                    {type: Attribute, operands: ["A"]},
                    {type: Attribute, operands: ["B"]}
                ]
            }
        });
    }


    @Test
    @scenario
    public function division_operator(){
        var v1 = number();
        var v2 = number();

        test_operator({
            op: attr("A").dividedBy(attr("B")),
            v1: v1,
            v2: v2,
            expectedResult: v1/v2,
            expectedSerialized: {
                type: Division,
                operands: [
                    {type: Attribute, operands: ["A"]},
                    {type: Attribute, operands: ["B"]}
                ]
            }
        });
    }

    function test_operator<TIn,TOut>(args:OperatorTestArguments<TIn, TOut>)
    {
        test_evaluation(args.op, args.v1, args.v2, args.expectedResult);
        test_serialization(args.op, args.expectedSerialized);
        test_invalid_operand(args.op, args.v1, args.v2);
    }


    function test_evaluation<TIn, TOut>(op:Operator<TOut>, v1: TIn, v2: TIn, expected: TOut)
    {
        given().this_operator(op);
        given().a_discovered_service_with_these_attributes({
            A: v1,
            B: v2
        });
        when().evaluating_the_operator();
        then().it_should_return(expected);
    }

    function test_serialization<T, V>(op:Operator<T>, expression:OperatorData)
    {

        given().this_operator(op);
        when().serializing_it();
        then().this_serialized_operator_should_be_produced(expression);
    }

    @scenario
	function test_invalid_operand<TIn, TOut>(op:Operator<TOut>, v1:TIn, v2:TIn) {
        if(RandomGen.primitives.boolean()) v1 = null; else v2 = null;

        steps
        .given('parameters: ${{op: op.type(), v1: v1, v2: v2}}', ()->{});
        given().this_operator(op);
        given().a_discovered_service_with_these_attributes({
            A: v1,
            B: v2
        });
        when().trying_to_evaluate_the_operator();
        then().it_should_throw_an_exception_of(InvalidOperandException);
    }

    function number() {
        return RandomGen.primitives.float(-100000,100000);
    }
}

class NumericalOperatorsSteps extends OperatorBaseSteps<NumericalOperatorsSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}