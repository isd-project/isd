package discovery.test.tests.base.operators;

import discovery.domain.operators.Operator;
import discovery.domain.operators.comparison.EqualsOperator;
import discovery.test.helpers.RandomGen;
import discovery.domain.operators.basic.ValueOperator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import discovery.domain.operators.Operators.*;
using discovery.domain.operators.Operators;



class EqualsOperatorTest implements Scenario{
    @steps
    var steps: EqualsOperatorSteps;

    @Test
    @scenario
    public function compare_integer(){
        var value1 = RandomGen.primitives.int();
        var value2 = value1 + RandomGen.primitives.int(1);

        test_operator(value(value1).equals(value(value1)), true);
        test_operator(value(value1).notEquals(value(value1)), false);

        test_operator(value(value1).equals(value(value2)), false);
        test_operator(value(value1).notEquals(value(value2)), true);
    }


    @Test
    @scenario
    public function compare_objects(){
        var obj1 = {a:1};
        var obj2 = {a:2};

        test_operator(value(obj1).equals(value(obj1)), true);
        test_operator(value(obj1).notEquals(value(obj1)), false);

        test_operator(value(obj1).equals(value(obj2)), false);
        test_operator(value(obj1).notEquals(value(obj2)), true);
    }

    function test_operator<T>(op:Operator<T>, expected:T) {
        given().this_operator(op);
        given().a_discovered_service_with_these_attributes({});
        when().evaluating_the_operator();
        then().it_should_return(expected);
    }
}

class EqualsOperatorSteps extends OperatorBaseSteps<EqualsOperatorSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------


}