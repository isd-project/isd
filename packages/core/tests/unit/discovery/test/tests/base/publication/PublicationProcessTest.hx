package discovery.test.tests.base.publication;

import discovery.domain.ErrorEventInfo;
import discovery.test.common.ListenerMock;
import discovery.domain.Publication;
import haxe.Exception;
import discovery.domain.Description;
import discovery.Discovery.DiscoveryDependencies;
import discovery.base.publication.PublicationProcess;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class PublicationProcessTest implements Scenario{
    @steps
    var steps: PublicationProcessSteps;

    @Test
    @scenario
    public function publish_service_description(){
        given().a_service_description_with_a_valid_key();
        when().publishing_that_service();
        then().the_service_description_should_be_published();
    }

    @Test
    @scenario
    public function should_publish_provider_key_to_pki(){
        given().a_service_description_with_a_valid_key();
        when().publishing_that_service();
        then().provider_key_should_be_published();
    }

    @Test
    @scenario
    public function notify_publish_key_internal_error(){
        given().a_service_description_with_a_valid_key();
        given().the_provider_key_publication_has_started();
        given().a_publish_error_listener_was_registered();
        when().the_pki_publication_notifies_an_internal_error();
        then().the_publication_should_notify_the_error();
    }
}

class PublicationProcessSteps extends Steps<PublicationProcessSteps>{

    var publicationProcess:PublicationProcess;

    var depsMockup:DiscoveryFactoryMockup;

    var publication:Publication;
    var srvDescription:Description;
    var keyId:Fingerprint;
    var notifiedError:Exception;

    var errorListener:ListenerMock<ErrorEventInfo>;

    public function new(){
        super();

        errorListener = new ListenerMock();
        depsMockup = new DiscoveryFactoryMockup();

        depsMockup.ipDiscoverMockup().resolvePublicIps();
    }

    // given ---------------------------------------------------

    @step
    public function a_service_description_with_a_valid_key() {
        given().some_provider_key_id();
        given().some_service_description();

        srvDescription.providerId = keyId;
    }

    @step
    function some_service_description() {
        srvDescription = RandomGen.fullDescription();
    }

    @step
    function some_provider_key_id() {
        keyId = keychainMockup().getKeyMockup().keyFingerprint;
        keychainMockup().resolveGetDefaultKey();
    }

    @step
    public function the_provider_key_publication_has_started() {
        pkiMockup().blockKeyPublication();

        when().publishing_that_service();
    }

    @step
    public function a_publish_error_listener_was_registered() {
        publication.onInternalError.listen(errorListener.listener());
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_that_service() {
        publicationProcess = new PublicationProcess(
                                    srvDescription, dependencies());
        publication = publicationProcess.publish();
    }

    @step
    public function the_pki_publication_notifies_an_internal_error() {
        notifiedError = new Exception('sample exception');

        var key = publishedKey();

        pkiMockup().notifyInternalPublicationError(key, notifiedError);
    }

    // then  ---------------------------------------------------

    @step
    public function provider_key_should_be_published() {
        pkiMockup().verifyKeyPublished(keychainMockup().defaultKey());
    }

    @step
    public function the_service_description_should_be_published() {
        depsMockup.mechanismMockup().checkDescriptionPublished(srvDescription);
    }

    @step
    public function the_publication_should_notify_the_error() {
        errorListener.assertWasCalled();

        errorListener.lastCall().error.shouldBe(notifiedError);
    }

    // ---------------------------------------------------------

    function dependencies():DiscoveryDependencies {
        return depsMockup.discoveryDependencies();
    }

    function pkiMockup() {
        return depsMockup.pkiMockup();
    }

    function keychainMockup() {
        return depsMockup.keychainMockup();
    }

    function publishedKey() {
        return keychainMockup().defaultKey();
    }
}