package discovery.test.tests.base.operators;

import haxe.Exception;
import discovery.domain.operators.serialization.OperatorData;
import discovery.domain.operators.serialization.OperatorSerializer;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description.Attributes;
import discovery.domain.operators.Operator;
import discovery.domain.Discovered;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;


class OperatorBaseSteps<Self:OperatorBaseSteps<Self>> extends Steps<Self>{
    var discovered:Discovered;
    var result:Any;
    var op:Operator<Any>;

    var serializer:OperatorSerializer;
    var serializedOp:OperatorData;

    var capturedException:Exception;

    public function new(){
        super();

        serializer = new OperatorSerializer();
    }

    // given ---------------------------------------------------

    @step
    public function this_operator<T>(op: Operator<T>)
    {
        this.op = cast op;
    }

    @step
    public function
        a_discovered_service_with_these_attributes(attrs: Attributes)
    {
        discovered = RandomGen.discoveredWithAttributes(attrs);
    }

    // when  ---------------------------------------------------

    @step
    public function evaluating_the_operator() {
        result = op.evaluate(discovered);
    }

    @step
    public function serializing_it() {
        serializedOp = serializer.serialize(op);
    }


    // then  ---------------------------------------------------

    @step
    public function it_should_return(expectedValue:Dynamic) {
        result.shouldBe(expectedValue);
    }

    @step
    public function this_serialized_operator_should_be_produced(expected:OperatorData)
    {
        serializedOp.shouldBeEqualsTo(expected);
    }


    // exception --------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function trying_to_evaluate_the_operator() {
        try{
            when().evaluating_the_operator();
        }
        catch(e){
            capturedException = e;
        }
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_throw_an_exception_of(type: Dynamic) {
        capturedException.shouldBeAn(type);
    }
}