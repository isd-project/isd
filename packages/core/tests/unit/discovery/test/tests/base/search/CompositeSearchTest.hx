package discovery.test.tests.base.search;

import haxe.Exception;
import discovery.base.search.Stoppable.StopCallback;
import discovery.test.common.CallableMockup;
import discovery.test.common.mockups.FilterMockup;
import discovery.test.helpers.RandomGen;
import discovery.base.search.CompositeSearch;
import discovery.domain.Discovered;
import discovery.test.common.ListenerMock;
import discovery.base.search.SearchBase;
import discovery.test.common.mockups.SearchControlMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;



class CompositeSearchTest implements Scenario{
    @steps
    var steps: CompositeSearchSteps;

    @Test
    @scenario
    public function notify_discovered(){
        given().a_composite_search_from_some_search_instances();
        given().a_discovery_listener_is_registered();
        when().any_search_notifies_a_discovery();
        then().the_listener_should_received_the_discovered_event();
    }


    @Test
    @scenario
    public function filter_duplicated_discovery(){
        given().a_composite_search_from_some_search_instances();
        given().a_discovery_listener_is_registered();
        given().a_discovered_is_notified_by_some_search();
        when().another_search_notifies_the_same_discovered();
        then().the_listener_should_receive_only_one_discovered_event();
    }

    @Test
    @scenario
    public function apply_filter(){
        given().a_composite_search_from_some_search_instances();
        given().a_discovery_listener_is_registered();
        given().some_filter_was_applied();
        when().any_search_notifies_a_discovery();
        then().the_filter_should_be_used();
    }


    @Test
    @scenario
    public function stop_all(){
        given().a_composite_search_from_some_search_instances();
        when().the_composite_is_stopped();
        then().all_search_components_should_be_stopped();
    }


    @Test
    @scenario
    public function notify_stop_when_all_callbacks_called(){
        given().a_composite_search_from_some_search_instances();
        given().an_stop_callback();
        when().the_composite_is_stopped();
        and().all_components_stops();
        then().the_stop_callback_should_be_called();
    }

    @Test
    @scenario
    public function not_notify_on_finish_before_all_search_components_concludes()
    {
        given().a_composite_search_from_some_search_instances();
        given().an_onFinish_callback_is_registered();
        when().only_some_components_finishes();
        then().the_search_finish_should_not_be_notified_yet();
    }

    @Test
    @scenario
    public function notify_on_finish_when_all_search_components_concludes(){
        given().a_composite_search_from_some_search_instances();
        given().an_onFinish_callback_is_registered();
        when().all_components_finishes_with_success();
        then().the_search_finish_should_be_notified_with_a_success_result();
    }


    @Test
    @scenario
    public function notify_on_finish_errors(){
        given().a_composite_search_from_some_search_instances();
        given().an_onFinish_callback_is_registered();
        when().some_components_fail_on_finish_and_other_succeeds();
        then().the_search_onFinish_should_be_called_with_a_failure_result();
    }
}


class CompositeSearchSteps extends Steps<CompositeSearchSteps>{
    var compositeSearch:CompositeSearch;

    var searchControlMockups:CompositeMockup<SearchControlMockup>;
    var searchBases:Array<SearchBase>;
    var discoveredListener: ListenerMock<Discovered>;
    var filter:FilterMockup<Discovered>;
    var stopCallbackMockup:CallableMockup;
    var stopCallback:StopCallback;
    var onFinishListener:ListenerMock<FinishInfo>;

    var discovered:Discovered;

    public function new(){
        super();

        searchControlMockups = new CompositeMockup(()->new SearchControlMockup());
        discoveredListener = new ListenerMock();
        stopCallbackMockup = new CallableMockup();
        onFinishListener   = new ListenerMock();

        filter = new FilterMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_search_from_some_search_instances() {
        searchControlMockups.populate();
        searchBases = searchControlMockups.map((control)->{
            return new SearchBase(control.searchControl);
        });

        compositeSearch = new CompositeSearch(cast searchBases);
    }

    @step
    public function a_discovery_listener_is_registered() {
        compositeSearch.onFound.listen(discoveredListener.onEvent);
    }

    @step
    public function a_discovered_is_notified_by_some_search() {
        when().any_search_notifies_a_discovery();
    }

    @step
    public function some_filter_was_applied() {
        compositeSearch.applyFilter(filter.filter());
    }


    @step
    public function an_stop_callback() {
        stopCallback = stopCallbackMockup.callable();
    }

    @step
    public function an_onFinish_callback_is_registered() {
        compositeSearch.onceFinished.listen(cast onFinishListener.onEvent);
    }

    // when  ---------------------------------------------------

    @step
    public function any_search_notifies_a_discovery() {
        discovered = RandomGen.discovered();

        when().any_search_notifies_this_discovered(discovered);
    }

    @step
    public function another_search_notifies_the_same_discovered() {
        when().any_search_notifies_this_discovered(discovered);
    }

    @step
    function any_search_notifies_this_discovered(discovered:Discovered) {
        var search = RandomGen.primitives.pickOne(searchBases);
        search.discovered(discovered);
    }

    @step
    public function the_composite_is_stopped() {
        compositeSearch.stop(stopCallback);
    }

    @step
    public function all_components_stops() {
        searchControlMockups.forEach((control)->control.notifyStop());
    }

    @step
    public function all_components_finishes_with_success() {
        for(s in searchBases){
            s.finished();
        }
    }

    @step
    public function only_some_components_finishes() {
        var some = partOf(searchBases);
        for(i in 0...some){
            searchBases[i].finished();
        }
    }

    @step
    public function some_components_fail_on_finish_and_other_succeeds() {
        var some = partOf(searchBases);
        for(i in 0...some){
            searchBases[i].finished({
                error: new Exception('Failure')
            });
        }
        for(i in some...searchBases.length){
            searchBases[i].finished();
        }
    }

    function partOf<T>(values: Array<T>){
        return RandomGen.primitives.int(1,values.length - 1);
    }

    // then  ---------------------------------------------------

    @step
    public function the_listener_should_received_the_discovered_event() {
        discoveredListener.assertWasCalledWith(discovered);
    }

    @step
    public function the_listener_should_receive_only_one_discovered_event() {
        discoveredListener.assertWasCalledTheseTimesWith(1, discovered);
    }

    @step
    public function the_filter_should_be_used() {
        filter.verifyFilterUsed();
    }

    @step
    public function all_search_components_should_be_stopped() {
        searchControlMockups.forEach((controlMockup)->{
            controlMockup.assertWasStopped();
        });
    }

    @step
    public function the_stop_callback_should_be_called() {
        stopCallbackMockup.verifyCallbackCalled();
    }


    @step
    public function the_search_finish_should_be_notified_with_a_success_result()
    {
        then().the_search_finish_should_be_notified();
        and().the_on_finish_result_should_be_successful();
    }


    @step
    public function the_search_onFinish_should_be_called_with_a_failure_result()
    {
        then().the_search_finish_should_be_notified();
        and().the_on_finish_result_should_contains_a_failure();
    }

    @step
    public function the_search_finish_should_be_notified() {
        onFinishListener.assertWasCalled();
    }

    @step
    public function the_search_finish_should_not_be_notified_yet() {
        onFinishListener.assertWasCalledTheseTimes(0);
    }

    @step
    function the_on_finish_result_should_be_successful() {
        var result = onFinishListener.captured;

        result.shouldNotBeNull();
        result.error.shouldBe(null);
    }

    function the_on_finish_result_should_contains_a_failure() {
        var result = onFinishListener.captured;

        result.shouldNotBeNull();
        result.error.shouldNotBeNull('No exception notified');
    }
}