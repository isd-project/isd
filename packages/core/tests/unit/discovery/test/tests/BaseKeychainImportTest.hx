package discovery.test.tests;

import discovery.keys.crypto.KeyLoader;
import discovery.keys.storage.ExportedKeyPair;
import discovery.async.promise.Promisable;
import discovery.keys.storage.KeyData;
import discovery.test.fakes.FakeKeyStorage;
import discovery.test.helpers.RandomGen;
import discovery.keys.BaseKeychain;
import discovery.test.fakes.FakePromisableFactory;
import discovery.keys.crypto.ExportedKey;
import discovery.test.common.mockups.crypto.KeyGeneratorMockup;
import discovery.keys.Keychain;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;
using discovery.keys.KeyTransformations;

class BaseKeychainImportTest implements Scenario{
    @steps
    var steps: BaseKeychainImportSteps;

    @Test
    @scenario
    public function import_key_data(){
        given().some_key_data();
        when().importing_that_key_data();
        then().the_key_data_should_be_stored();
        and().a_promise_with_the_stored_key_data_should_be_returned();
    }

    @Test
    @scenario
    public function import_a_pair_of_exported_keys(){
        when().importing_a_pair_of_exported_keys();
        then().the_key_type_and_fingerprint_should_be_loaded();
        and().the_resultant_key_data_should_be_stored();
        and().a_promise_with_the_stored_key_data_should_be_returned();
    }

    @Test
    @scenario
    public function import_key__from_public_key(){
        when().importing_a_key_only_with_the_public_key();
        then().the_key_type_and_fingerprint_should_be_loaded();
        and().the_resultant_key_data_should_be_stored();
        but().the_stored_key_should_not_have_private_key_info();
    }

    @Ignore('Missing: extract public key from private key')
    @Test
    @scenario
    public function import_key__from_private_key(){
        given().an_exported_private_key();
        when().importing_that_key();
        then().the_imported_key_should_be_loaded();
        then().a_public_key_should_be_derived();
        and().the_key_pair_should_be_stored();
    }
}

class BaseKeychainImportSteps extends Steps<BaseKeychainImportSteps>{

    var keystorage   = new FakeKeyStorage();
    var keygenMockup = new KeyGeneratorMockup();
    var promises     = new FakePromisableFactory();

    var keychain:Keychain;

    var keyData:KeyData;
    var expectedKeyData:KeyData;
    var storedKeyPair:ExportedKeyPair;
    var exportedKey:ExportedKey;
    var importedKey:ExportedKey;

    var keyDataPromise:Promisable<KeyData>;


    public function new() {
        super();

        keychain = new BaseKeychain(
                keygenMockup.keyGenerator(), keystorage, promises
            );
        keygenMockup.resolveLoadExportedKey();
    }

    function keyLoader():KeyLoader {
        return keygenMockup.keyLoader();
    }

    // given -------------------------------------------------------------

    @step
    public function some_key_data() {
        keyData = RandomGen.crypto.keyData();
    }

    @step
    public function an_exported_private_key() {
        exportedKey = RandomGen.crypto.exportedKey(true);
    }

    // when -------------------------------------------------------------

    @step
    public function importing_that_key_data() {
        keyDataPromise = keychain.importKey(keyData);
        expectedKeyData = keyData;
    }

    @step
    public function importing_a_pair_of_exported_keys() {
        storedKeyPair = {
            privateKey: RandomGen.crypto.exportedKey(true),
            publicKey : RandomGen.crypto.exportedKey(false)
        };

        keyDataPromise = keychain.importKey(storedKeyPair);

        fillExpectedKeyData(storedKeyPair);
    }

    @step
	public function importing_a_key_only_with_the_public_key() {
        var publicKey = RandomGen.crypto.exportedKey(false);

        keyDataPromise = keychain.importKey({
            publicKey: publicKey
        });

        fillExpectedKeyData({
            publicKey: publicKey
        });
    }

    function fillExpectedKeyData(keyPair:ExportedKeyPair) {
        var convertedKeyData = keyPair.exportedKeyPairToKeyData();
        keyLoader().fillKeyData(convertedKeyData).then((filledKey)->{
            expectedKeyData = filledKey;
        });
    }


    @step
    public function importing_that_key() {
        // keychain.importKey(exportedKey);

        importedKey = exportedKey;
    }

    // then -------------------------------------------------------------


    @step
    public function the_key_type_and_fingerprint_should_be_loaded() {
        expectedKeyData.fingerprint.shouldNotBeNull('Fingerprint');
        expectedKeyData.keyType.shouldNotBeNull('KeyType');
    }

    @step
    public function the_resultant_key_data_should_be_stored() {
        then().the_key_pair_should_be_stored();
    }

    @step
    public function the_key_data_should_be_stored() {
        var storedKey = keystorage.getKey(expectedKeyData.fingerprint.toFingerprint());

        storedKey.shouldBeEqualsTo(expectedKeyData);
    }

    @step
	public function the_stored_key_should_not_have_private_key_info() {
        var storedKey = keystorage.lastStoredKey();

        storedKey.privateKey.shouldBe(null);
    }


    @step
    public function a_promise_with_the_stored_key_data_should_be_returned() {
        keyDataPromise.shouldNotBeNull();
        keyDataPromise.then((resolvedKeyData)->{
            resolvedKeyData.shouldBeEqualsTo(expectedKeyData,
                'Resolved KeyData does not match the expected value');
        });
    }

    @step
    public function the_imported_key_should_be_loaded() {
        keygenMockup.shouldHaveLoadedExportedKey(importedKey);
    }

    @step
    public function a_public_key_should_be_derived() {
        // TO-DO: ...
    }

    @step
    public function the_key_pair_should_be_stored() {
        // TO-DO: ...
    }

    //----------------------------------------------------------------------

}