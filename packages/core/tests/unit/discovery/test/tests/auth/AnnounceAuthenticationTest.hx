package discovery.test.tests.auth;

import discovery.keys.crypto.signature.Signed;
import discovery.domain.Announcement;
import discovery.test.common.mockups.KeyMockup;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description;
import discovery.test.fakes.FakePromisableFactory;
import discovery.auth.DefaultAuthenticator;
import discovery.auth.Authenticator;
import hxgiven.Steps;
import hxgiven.Scenario;


class AnnounceAuthenticationTest implements Scenario{
    @steps
    var steps: AnnounceAuthenticationSteps;

    @Test
    @scenario
    public function sign_channel_key(){
        given().a_service_announce();
        given().the_service_has_a_channel_with_a_key();
        when().signing_that_announce();
        then().the_channel_key_should_be_included_on_signature();
    }


    @Test
    @scenario
    public function verify_signed_channel_key(){
        given().a_service_announce_with_a_channel_key();
        given().a_signed_announce_was_created();
        when().verifying_that_signed_announce();
        then().the_channel_key_should_also_be_verified();
    }
}

class AnnounceAuthenticationSteps extends Steps<AnnounceAuthenticationSteps>{

    var promises = new FakePromisableFactory();

    var authenticator:Authenticator;

    var description:Description;
    var announce:Announcement;
    var signedAnnouncement:Signed<Announcement>;
    var channelKey:String;
    var providerKey = new KeyMockup();

    public function new(){
        super();

        authenticator = new DefaultAuthenticator(promises);
    }


    // summary ---------------------------------------------------

    @step
    public function a_service_announce_with_a_channel_key() {
        given().a_service_announce();
        given().the_service_has_a_channel_with_a_key();
    }

    @step
    public function a_signed_announce_was_created() {
        when().signing_that_announce();
    }

    // given ---------------------------------------------------

    @step
    public function a_service_announce() {
        description = RandomGen.description();
        announce = RandomGen.announcementFrom(description);
    }

    @step
    public function the_service_has_a_channel_with_a_key() {
        var channel = RandomGen.channel();

        channelKey = RandomGen.name();
        channel.keys.push(channelKey);

        description.location.channels = [channel];
    }

    // when  ---------------------------------------------------

    @step
    public function signing_that_announce() {
        authenticator.sign(providerKey.key, announce)
            .then((signedAnnounce)->{
                this.signedAnnouncement = signedAnnounce;
            });

        providerKey.keySigner.resolveSignature();
    }

    @step
    public function verifying_that_signed_announce() {
        authenticator.validate(providerKey.key, signedAnnouncement);
    }

    // then  ---------------------------------------------------

    @step
    public function the_channel_key_should_be_included_on_signature() {
        providerKey.keySigner.verifyAppendValue(channelKey);
    }

    @step
    public function the_channel_key_should_also_be_verified() {
        providerKey.keyVerifier.verifyAppendValue(channelKey);
    }
}