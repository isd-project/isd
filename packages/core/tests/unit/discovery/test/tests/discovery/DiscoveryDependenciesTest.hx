package discovery.test.tests.discovery;

import haxe.Exception;
import discovery.utils.dependencies.MissingDependencyException;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import discovery.Discovery.DiscoveryDependencies;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class DiscoveryDependenciesTest implements Scenario{
    @steps
    var steps: DiscoveryDependenciesSteps;

    @Test
    public function verify_complete_dependencies(){
        given().complete_discovery_dependencies();
        when().verifying_the_dependencies();
        then().no_exception_should_be_thrown();
    }

    @Test
    public function verify_dependencies(){
        test_missing('mechanism');
        test_missing('keychain');
        test_missing('pki');
        test_missing('ipDiscover');
        test_missing('promises');
    }

    @scenario
    function test_missing(missingDependency:String) {
        given().the_discovery_dependencies_are_missing(missingDependency);
        when().verifying_the_dependencies();
        then().a_missing_dependency_exception_should_be_thrown();
    }
}

class DiscoveryDependenciesSteps extends Steps<DiscoveryDependenciesSteps>{
    var discoveryDependencies:DiscoveryDependencies;

    var discoveryFactory:DiscoveryFactoryMockup;

    var thrownException:Exception;

    public function new(){
        super();

        discoveryFactory = new DiscoveryFactoryMockup();
    }

    // given ---------------------------------------------------

    @step
    public function
        the_discovery_dependencies_are_missing(missingDependency:String)
    {
        given().complete_discovery_dependencies();
        given().this_dependency_is_removed(missingDependency);
    }

    @step
    public function complete_discovery_dependencies() {
        discoveryDependencies = discoveryFactory.discoveryDependencies();
    }

    @step
    function this_dependency_is_removed(field:String) {
        Reflect.setProperty(discoveryDependencies, field, null);
    }

    // when  ---------------------------------------------------

    @step
    public function verifying_the_dependencies() {
        try{
            discoveryDependencies.requireDependencies();
        }
        catch(e){
            thrownException = e;
        }
    }

    // then  ---------------------------------------------------

    @step
    public function no_exception_should_be_thrown() {
        thrownException.shouldBe(null);
    }

    @step
    public function a_missing_dependency_exception_should_be_thrown() {
        thrownException.shouldBeAn(MissingDependencyException);
    }
}