package discovery.test.tests.discovery;

import discovery.exceptions.KeyNotFoundException;
import haxe.Exception;
import discovery.async.promise.Promisable;
import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.test.common.mockups.PkiMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.base.search.DiscoveryValidator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DiscoveryValidatorTest implements Scenario{
    @steps
    var steps: DiscoveryValidatorSteps;

    @Test
    @scenario
    public function should_reject_an_expired_announcement(){
        given().a_discovered_announcement_which_already_expired();
        when().validating_it();
        then().it_should_be_rejected();
    }

    @Test
    @scenario
    public function should_reject_if_key_not_found(){
        given().some_discovered_service();
        given().that_the_discovered_key_cannot_be_found();
        when().validating_it();
        then().it_should_be_rejected_with_a(KeyNotFoundException);
    }
}

class DiscoveryValidatorSteps extends Steps<DiscoveryValidatorSteps>{
    var validator:DiscoveryValidator;

    var promisesFactory:FakePromisableFactory;
    var pkiMockup:PkiMockup;

    var discovered:Discovered;
    var validated :Discovered;
    var validationError: Exception;
    var validatedPromise :Promisable<Discovered>;

    public function new(){
        super();

        pkiMockup = new PkiMockup();
        promisesFactory = new FakePromisableFactory();

        validator = new DiscoveryValidator(pkiMockup.pki, promisesFactory);
    }

    // given ---------------------------------------------------

    @step
    public function a_discovered_announcement_which_already_expired() {
        discovered = RandomGen.crypto.signed(RandomGen.expiredAnnouncement());
    }

    @step
    public function some_discovered_service() {
        discovered = RandomGen.crypto.signed(RandomGen.fullAnnouncement());
    }

    @step
    public function that_the_discovered_key_cannot_be_found() {
        pkiMockup.resolveSearchKeyTo(null);
    }

    // when  ---------------------------------------------------

    @step
    public function validating_it() {
        when().starting_validation();
        and().resolving_validation();
    }

    @step
    function starting_validation() {
        validatedPromise = validator.validate(discovered);
    }

    @step
    function resolving_validation() {
        assertThat(validatedPromise, is(notNullValue()));

        validatedPromise.then((result)->{
            validated = result;
        }, (err)->{
            validationError = err;
            return null;
        });
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_be_rejected() {
        assertThat(validationError, is(notNullValue()),
            'Validation error should not be null'
        );
    }

    @step
    public function it_should_be_rejected_with_a(exceptionType: Dynamic) {
        assertThat(validationError, isA(exceptionType), 'Validation error');
    }
}