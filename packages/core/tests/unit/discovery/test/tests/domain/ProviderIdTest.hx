package discovery.test.tests.domain;

import discovery.test.helpers.RandomGen;
import discovery.domain.ProviderId;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

import Assertion.*;


class ProviderIdTest implements Scenario{
    @Test
    @scenario
    public function provider_id_from_fingerprint(){
        var fingerprint = RandomGen.crypto.fingerprint();
        var id:ProviderId = fingerprint;

        assert(id.fingerprint() == fingerprint);
        assert(id.isFingerprint() == true);
        assert(id.id().equals(fingerprint.hash));

        assert(id.keyId() == null);
        assert(id.isKeyId() == false);
    }


    @Test
    @scenario
    public function provider_id_from_key_id(){
        var keyId = RandomGen.crypto.keyIdentifier();
        var id:ProviderId = keyId;

        assert(id.keyId() == keyId);
        assert(id.isKeyId() == true);

        assert(id.fingerprint() == null);
        assert(id.isFingerprint() == false);
    }


    @Test
    @scenario
    public function provider_id_from_key_prefix(){
        var prefix = RandomGen.binary.hex(Random.int(5,15));
        var id:ProviderId = prefix;

        assert(id.isKeyId() == true);
        assert(id.keyId().isText());
        assert(id.keyId() == prefix);

        assert(id.fingerprint() == null);
        assert(id.isFingerprint() == false);
    }
}