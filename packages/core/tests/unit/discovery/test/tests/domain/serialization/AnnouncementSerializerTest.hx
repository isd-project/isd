package discovery.test.tests.domain.serialization;

import discovery.domain.serialization.HaxeSerializer;
import discovery.format.parsers.JsonParser;
import haxe.Json;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.serialization.SignedSerializer;
import discovery.domain.serialization.LocationSerializer;
import discovery.domain.serialization.ProviderIdSerializer;
import discovery.test.helpers.RandomGen;
import discovery.domain.ProviderId;
import haxe.io.Bytes;
import discovery.format.Binary;
import discovery.domain.serialization.DescriptionSerializer;
import discovery.domain.Description;
import discovery.domain.serialization.ValidityDeserializer;
import discovery.domain.serialization.Serializer;
import discovery.domain.Struct;
import discovery.domain.serialization.AnnouncementSerializer;
import discovery.domain.Validity;
import discovery.domain.Announcement;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class AnnouncementSerializerTest implements Scenario{
    @steps
    var steps: AnnouncementSerializerSteps;


    @Test
    @scenario
    public function signed_announcement(){
        given().this_deserializer_is_used(
            new HaxeSerializer(new SignedSerializer(new AnnouncementSerializer()))
        );
        given().a_json_serialization_of(
            Signed.make(
                RandomGen.fullAnnouncement(),
                RandomGen.crypto.signature()
            ));
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }

    @Test
    @scenario
    public function full_announcement(){
        given().this_deserializer_is_used(new AnnouncementSerializer());
        given().a_serialization_of(RandomGen.fullAnnouncement());
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }

    @Test
    @scenario
    public function validity_deserialization(){
        given().this_deserializer_is_used(new ValidityDeserializer());
        given().a_serialization_of(
            Validity.between(
                Date.fromString('2000-01-02'), Date.fromString('2000-01-03'))
        );
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }


    @Test
    @scenario
    public function minimal_description_deserialization(){
        given().this_deserializer_is_used(new DescriptionSerializer());
        given().a_serialization_of((({
            name: "name",
            type: "type"
        }) : Description)
        );
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }


    @Test
    @scenario
    public function identity_description_deserialization(){
        var desc:Description = {
            name: "name",
            type: "type",
            providerId: Binary.fromHex('abcd1234'),
            instanceId: Bytes.ofHex('deadbeef')
        };

        given().this_deserializer_is_used(new DescriptionSerializer());
        given().a_serialization_of(desc);
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }

    @Test
    @scenario
    public function description_with_location_deserialization(){
        var desc:Description = {
            name: "name",
            type: "type",
            providerId: Binary.fromHex('abcd1234'),
            instanceId: Bytes.ofHex('deadbeef'),
            location: {
                defaultPort: 1234,
                addresses: [
                    '10.9.8.7',
                    '2001:db8::1'
                ],
                channels: [
                    {
                        protocol: 'http',
                        port: 8080
                    },
                    {
                        protocol: 'tor',
                        address: 'http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion'
                    }
                ]
            }
        };

        given().this_deserializer_is_used(new DescriptionSerializer());
        given().a_serialization_of(desc);
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }


    @Test
    @scenario
    public function description_with_bytes_attributes(){
        var desc:Description = {
            name: "name",
            type: "type",
            providerId: Binary.fromHex('abcd1234'),
            instanceId: Bytes.ofHex('deadbeef'),
            attributes: {
                bytes: RandomGen.binary.bytes(5),
                binary: RandomGen.binary.binary(),
                text: RandomGen.primitives.name(),
                int: RandomGen.primitives.int(),
                float: RandomGen.primitives.float(-100000, 100000),
                bool: RandomGen.primitives.boolean()
            }
        };

        given().this_deserializer_is_used(
            new HaxeSerializer(new DescriptionSerializer()));
        given().a_json_serialization_of(desc);
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }

    @Test
    @scenario
    public function full_description_deserialization(){
        var desc:Description = RandomGen.fullDescription();

        given().this_deserializer_is_used(new DescriptionSerializer());
        given().a_serialization_of(desc);
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }


    @Test
    @scenario
    public function location_info_serialization(){
        given().this_deserializer_is_used(new LocationSerializer());
        given().a_json_serialization_of(
            new Location(
                1234,
                [ '10.9.8.7', '2001:db8::1' ],
                [
                    {
                        protocol: 'http',
                        port: 8080
                    },
                    {
                        protocol: 'tor',
                        address: 'http://2gzyxa5ihm7nsggfxnu52rck2vv4rvmdlkiu3zzui5du4xyclen53wid.onion'
                    }
                ]
            )
        );
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }


    @Test
    @scenario
    public function provider_id_from_fingerprint(){
        given().this_deserializer_is_used(new ProviderIdSerializer());
        given().a_json_serialization_of(ProviderId.fromFingerprint(
            RandomGen.crypto.fingerprint()
        ));
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }

    @Test
    @scenario
    public function provider_id_from_binary(){
        given().this_deserializer_is_used(new ProviderIdSerializer());
        given().a_serialization_of(ProviderId.fromBinary(
            RandomGen.binary.binary()
        ));
        when().deserializing_that_object();
        then().an_equal_object_should_be_produced();
    }
}


class AnnouncementSerializerSteps extends Steps<AnnouncementSerializerSteps>{

    var serializer:Serializer<Any>;

    var expected:Any;
    var serialized:Any;
    var deserialized:Any;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function this_deserializer_is_used(serializer:Serializer<Any>) {
        this.serializer = serializer;
    }

    @step
    public function a_serialization_of<T:{}>(value:T) {
        this.expected = value;
        this.serialized = serializer.serialize(value);
    }

    @step
    public function a_json_serialization_of<T:{}>(value:T) {
        this.expected = value;
        this.serialized = jsonSerialize(serializer.serialize(value));
    }

    // when  ---------------------------------------------------


    @step
    public function deserializing_that_object() {
        deserialized = serializer.deserialize(serialized);
    }

    // then  ---------------------------------------------------

    @step
    public function an_equal_object_should_be_produced() {
        deserialized.shouldBeEqualsTo(expected);
    }

    function jsonSerialize(value:Any):Any {
        var parser = new JsonParser();
        return parser.parse(parser.stringify(value));
    }
}