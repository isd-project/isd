package discovery.test.tests.domain;

import org.hamcrest.Matchers;
import discovery.test.common.FieldRetriever;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description;
import hxgiven.Steps;
import hxgiven.Scenario;

import Assertion.*;
import massive.munit.Assert;
import org.hamcrest.Matchers.*;

class DescriptionTest implements Scenario{

    @Test
    @scenario
    public function creation_minimal(){
        var desc:Description = {
            name: "name",
            type: "type",
            port: 12345
        };

        assert(desc.name == "name");
        assert(desc.type == "type");
        assert(desc.port == 12345);
        assert(desc.location != null);
        assert(desc.location.defaultPort == 12345);
    }


    @Test
    @scenario
    public function port_value(){
        var desc:Description = {
            name: "name",
            type: "type",
            port: 12345
        };
        var desc2:Description = {
            name: desc.name,
            type: desc.type,
            location: {
                defaultPort: desc.port
            }
        };

        var fieldRetriever = new FieldRetriever();
        var fields1 = fieldRetriever.mapSimpleFieldsOf(desc);
        var fields2 = fieldRetriever.mapSimpleFieldsOf(desc2);

        assertThat(desc.equals(desc2), is(true),
            '${desc}\n' +
            'should be equals to ${desc2}');

        var fieldsAreEquals = 'Fields of ${desc}\n' +
        'should be equals to fields of ${desc2}';

        Assert.areEqual(fields1, fields2, fieldsAreEquals);
    }

    @Test
    @scenario
    public function clone_description(){
        var desc:Description = RandomGen.fullDescription();
        var clone = desc.clone();

        assertThat(clone, is(notNullValue()), "clone should not be null");
        assertThat(clone, is(not(theInstance(desc))), "clone should not be the same instance");
    }
}