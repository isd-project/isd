package discovery.test.tests.domain;

import discovery.format.Formatters;
import discovery.format.Formats;
import haxe.io.Bytes;
import discovery.domain.Identifier;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class IdentifierTest implements Scenario{
    @steps
    var steps: IdentifierSteps;

    @Test
    @scenario
    public function convert_to_url(){
        var instanceIdHex = 'abcdef';
        var providerIdHex = "deadbeef";
        var providerId = Bytes.ofHex(providerIdHex);

        test_to_url("Simple identifier", {
            name: "service",
            type: "http",
            providerId: providerId
        }, 'srv://service.http.${toBase64Url(providerId)};fmt=b64u');

        test_to_url_formatted("Identifier with instance id", {
            instanceId: Bytes.ofHex(instanceIdHex),
            name: "service",
            type: "http",
            providerId: providerId
        },
        Base16,
        'srv://${instanceIdHex}.service.http.${providerIdHex};fmt=b16');
    }

    @Test
    @scenario
    public function convert_to_url_with_missing_fields(){
        test_to_url("Identifier without provider id", {
            name: "service",
            type: "http",
            providerId: null
        }, null);

        test_to_url("Identifier without name", {
            name: null,
            type: "http",
            providerId: Bytes.ofHex("72dd")
        }, null);

        test_to_url("Identifier without type", {
            name: "service",
            type: null,
            providerId: Bytes.ofHex("72dd")
        }, null);
    }

    function test_to_url(
        testCase:String, identifier:Identifier, expected:String)
    {
        test_to_url_formatted(testCase, identifier, null, expected);
    }

    function test_to_url_formatted(
        testCase:String, identifier:Identifier, format:Formats, expected:String)
    {
        assertThat(identifier.toUrl(format), is(equalTo(expected)), testCase);
    }

    function toBase64Url(bytes:Bytes): String {
        return Formatters.base64Url.encode(bytes);
    }
}

class IdentifierSteps extends Steps<IdentifierSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}