package discovery.test.tests.domain;

import discovery.utils.time.Duration;
import discovery.exceptions.IllegalArgumentException;
import haxe.Exception;
import discovery.domain.Validity;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ValidityTest implements Scenario{
    @steps
    var steps: ValiditySteps;

    @Test
    @scenario
    public function create_validity_from_two_dates(){
        given().an_initial_date();
        and().a_later_expiry_date();
        when().creating_a_validity_from_both_dates();
        then().it_should_be_valid_at_any_date_between_the_initial_and_expiry_date();
    }

    @Test
    @scenario
    public function create_validity_from_now(){
        given().a_future_expiry_date();
        when().creating_a_validity_from_this_date();
        then().it_should_be_valid_now();
    }

    @Test
    @scenario
    public function create_validity_from_duration(){
        given().some_validity_duration();
        when().creating_a_validity_from_this_duration();
        then().it_should_be_valid_now();
        but().it_should_expiry_after_the_given_duration();
    }

    @Test
    @scenario
    public function validity_should_be_valid_from_initial_until_final_date(){
        given().a_validity_created_from_two_dates();
        then().it_should_be_valid_from_initial_date();
        and().it_should_be_valid_until_final_date();
    }

    @Test
    @scenario
    public function should_throw_when_dates_are_inverted(){
        given().an_initial_date();
        and().an_expiry_date_before_the_initial();
        when().trying_to_create_a_validity_from_both_dates();
        then().an_exception_should_be_thrown(IllegalArgumentException);
    }
}

class ValiditySteps extends Steps<ValiditySteps>{
    var validity:Validity;

    var initialDate:Date;
    var expiryDate:Date;
    var duration:Duration;

    var capturedException:Exception;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_validity_created_from_two_dates() {
        given().an_initial_date();
        and().a_later_expiry_date();
        when().creating_a_validity_from_both_dates();
    }

    @step
    public function an_initial_date() {
        initialDate = RandomGen.time.anyDate();
    }

    @step
    public function a_later_expiry_date() {
        expiryDate = RandomGen.time.anyDateAfter(initialDate);
    }

    @step
    public function a_future_expiry_date() {
        expiryDate = RandomGen.time.anyDateAfter(Date.now());
    }

    @step
    public function an_expiry_date_before_the_initial() {
        expiryDate = RandomGen.time.anyDateBefore(initialDate);
    }

    @step
    public function some_validity_duration() {
        this.duration = RandomGen.time.duration();
    }

    // when  ---------------------------------------------------

    @step
    public function trying_to_create_a_validity_from_both_dates() {
        try{
            when().creating_a_validity_from_both_dates();
        }
        catch(err){
            capturedException = err;
        }
    }

    @step
    public function creating_a_validity_from_both_dates() {
        validity = new Validity(expiryDate, initialDate);
    }

    @step
    public function creating_a_validity_from_this_date() {
        validity = new Validity(expiryDate);
    }

    @step
    public function creating_a_validity_from_this_duration() {
        validity = Validity.validFor(duration);
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_be_valid_now() {
        assertThat(validity.isValid(), is(true),
            'Validity: ${validity}\n'+
            'should be valid now (${Date.now()})'
        );
    }

    @step
    public function it_should_be_valid_at_any_date_between_the_initial_and_expiry_date() {
        var dateBetween = RandomGen.time.anyDateBetween(initialDate, expiryDate);

        then().it_should_be_valid_at(dateBetween);
    }


    @step
    public function it_should_be_valid_from_initial_date() {
        then().it_should_be_valid_at(validity.validFrom);
    }

    @step
    public function it_should_be_valid_until_final_date() {
        then().it_should_be_valid_at(validity.validTo);
    }

    @step
    public function it_should_be_valid_at(date: Date){
        assertThat(validity.isValidAt(date), is(true),
            'Validity: ${validity}\n'+
            'should be valid at: ${date}'
        );
    }


    @step
    public function an_exception_should_be_thrown(exceptionType:Dynamic) {
        var typeName = Type.getClassName(exceptionType);

        assertThat(capturedException, isA(exceptionType),
            'An exception of type ${typeName} should have been thrown'
        );
    }

    @step
    public function it_should_expiry_after_the_given_duration() {
        assertThat(validity.validTo.getTime(),
            is(equalTo((duration + validity.validFrom).getTime())),
            'Validity expire datetime does not match the expected value'
        );
    }
}