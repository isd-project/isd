package discovery.test.tests.domain;

import discovery.domain.Identifier;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class IdentifierComparisonTest implements Scenario{
    @steps
    var steps: IdentifierComparisonSteps;

    @Test
    @scenario
    public function compare_random_identifier(){
        var id1 = RandomGen.identifier();
        var id2 = RandomGen.identifier();


        id1.compare(id1).shouldBe(0);
        id2.compare(id2).shouldBe(0);

        //should be opposites
        var cmp1 = id1.compare(id2);
        var cmp2 = id2.compare(id1);

        cmp1.shouldBe(-cmp2);
    }
}

class IdentifierComparisonSteps extends Steps<IdentifierComparisonSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}