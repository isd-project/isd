package discovery.test.tests.format;

import discovery.test.helpers.RandomGen;
import discovery.format.Formats;
import discovery.format.Formatters;
import discovery.format.EncodedData;
import haxe.io.Bytes;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;
using discovery.test.common.transformations.SerializationTransform;


class EncodedDataTest implements Scenario{

    @Test
    @scenario
    public function encodeDecodeDefault(){
        var data = Bytes.ofHex("88536d16af4ab69ef7fbf471dd9e62c4d7e0ad508209020baada067c7c0a7548");

        var encoded:EncodedData = data;

        assertThat(encoded, is(notNullValue()));
        assertThat(encoded.data, is(notNullValue()));
        assertThat(encoded.format, is(notNullValue()));

        assertThat(encoded.decode().toHex(), is(equalTo(data.toHex())));
    }

    @Test
    @scenario
    public function encodeWithFormat(){
        var data = Bytes.ofHex("0e71bebadd059b0cd8701a65582c67346b5b2397bf48c5e9248c20bc843d06d0");

        var encoded:EncodedData = EncodedData.encode(data, Formats.Base32);

        assertThat(encoded, is(notNullValue()));
        assertThat(encoded.data, is(notNullValue()));
        assertThat(encoded.format, is(Formats.Base32));

        assertThat(encoded.decode().toHex(), is(equalTo(data.toHex())));
        assertThat(encoded.toString(), is(equalTo(Formatters.base32.encode(data))));
    }

    @Test
    @scenario
    public function encodeUTF8(){
        var text = "Lorem ipsum dolor sit amet!";
        var data = Bytes.ofString(text);

        var encoded:EncodedData = EncodedData.encode(data, Formats.UTF8);

        assertThat(encoded, is(notNullValue()));
        assertThat(encoded.data, is(notNullValue()));
        assertThat(encoded.format, is(equalTo(Formats.UTF8)));

        assertThat(encoded.decode().toHex(), is(equalTo(data.toHex())));
        assertThat(encoded.toString(), is(equalTo(text)));
    }


    @Test
    @scenario
    public function fromEncodedSerialized(){
        var encoded = someEncodedData();
        var dynamicEncoded = encoded.toJsonObject();

        var parsed = EncodedData.fromDynamic(dynamicEncoded);

        parsed.shouldBeEqualsTo(encoded);
        parsed.shouldBeAn(TextWithFormat);
    }

    // ---------------------------------------------------------------

    function someEncodedData(){
        var formats = Formats.listFormats().filter((f)->f!=UTF8);
        var format = RandomGen.primitives.pickOne(formats);
        var data = RandomGen.binary.binary();

        return EncodedData.encode(data, format);
    }
}