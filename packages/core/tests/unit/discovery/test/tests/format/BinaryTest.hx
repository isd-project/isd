package discovery.test.tests.format;

import discovery.test.helpers.RandomGen;
import discovery.format.Binary;
import haxe.io.Bytes;
import hxgiven.Steps;
import hxgiven.Scenario;

import Assertion.*;
import org.hamcrest.Matchers.*;


class BinaryTest implements Scenario{

    @Test
    @scenario
    public function equals_bytes(){
        var bytes = Bytes.ofHex("a97f98");
        var binary:Binary = bytes;

        assert(Binary.areEquals(binary, bytes));
    }


    @Test
    @scenario
    public function from_to_int(){
        var expected  = Random.int(0, 10000000);
        var binary    = Binary.fromInt(expected);
        var converted = binary.toInt();

        assertThat(converted, is(equalTo(expected)),
            "Converted value should correspond to initial value");
    }


    @Test
    @scenario
    public function from_to_double(){
        var expected:Float = Random.float(0, 10000000);
        var binary    = Binary.fromDouble(expected);
        var converted = binary.toDouble();

        assertThat(converted, is(equalTo(expected)),
            "Converted value should correspond to initial value");
    }

    @Test
    @scenario
    public function clone(){
        var b1:Binary = RandomGen.binary.bytes(5);
        var b2 = b1.clone();

        assertThat(b1, is(equalTo(b2)), "Clone should be equals");
    }


}