package discovery.test.tests.format;

import discovery.format.Formats;
import org.hamcrest.Matchers;
import discovery.format.Formatters;
import massive.munit.Assert;
import discovery.format.EncodedData;
import haxe.io.Bytes;
import discovery.format.BytesOrText;

import org.hamcrest.Matchers.*;


class BytesOrTextTest {
    public function new() {}

    @Test
    public function fromTextToText() {
        var text = "Hello World!";
        var data:BytesOrText = text;
        var toText:String = data;

        assertThat(toText, is(equalTo(text)));
    }

    @Test
    public function fromBytesToBytes() {
        var input = Bytes.ofHex("e96e29c72f53ea02998e8bbe6fd55d86332700abbee315e0527eb32fd6083ca6");
        var data:BytesOrText = input;
        var toBytes:Bytes = data;

        assertThat(toBytes.toHex(), is(equalTo(input.toHex())));
    }

    @Test
    public function fromBytesToString() {
        var input = Bytes.ofHex("e96e29c72f53ea02998e8bbe6fd55d86332700abbee315e0527eb32fd6083ca6");
        var data:BytesOrText = input;
        var toString:String = data;

        assertThat(toString, is(equalTo(data.getFormatter().encode(input))));
    }

    @Test
    public function compare() {
        shouldBeEquals("Text", "Text");
        shouldBeEquals(Bytes.ofHex("55d95f"), Bytes.ofHex("55d95f"));
        shouldBeEquals(
            EncodedData.encode(Bytes.ofHex("55d95f"), Formats.Base32),
            EncodedData.encode(Bytes.ofHex("55d95f"), Formats.Base64));
    }

    function shouldBeEquals(value1:BytesOrText, value2:BytesOrText) {
        assertThat(value1.equals(value2), is(true),
            '$value1 should be equals to $value2'
        );
    }
}