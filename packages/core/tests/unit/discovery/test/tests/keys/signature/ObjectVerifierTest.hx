package discovery.test.tests.keys.signature;

import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.signature.Verifier.Verify;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.signature.ObjectSigner;
import discovery.keys.crypto.signature.ObjectVerifier;

import discovery.test.common.fakes.FakeVerifier;
import discovery.test.common.fakes.FakeSigner;

import haxe.DynamicAccess;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

class ObjectVerifierTest implements Scenario{
    @steps
    var steps: ObjectVerifierSteps;

    @Test
    @scenario
    public function verify_object(){
        given().a_signature_created_for_some_object({
            property: Random.string(Random.int(4, 20)),
            bool: Random.bool(),
            int: RandomGen.int(),
            bytes: RandomGen.binary.bytes(12),
            subobj: {
                a: RandomGen.int(),
                b: RandomGen.int()
            },
            array: [
                RandomGen.int(), RandomGen.int()
            ]
        });
        when().verifying_the_signature_against_the_same_object();
        then().the_same_fields_and_values_should_be_verified();
        and().a_verification_promise_should_be_produced();
    }
}

private typedef Object = DynamicAccess<Dynamic>;

class ObjectVerifierSteps extends Steps<ObjectVerifierSteps>{
    var objVerifier: ObjectVerifier;

    var fakeSigner:FakeSigner;
    var fakeVerifier:FakeVerifier;

    var signature:Signature;
    var signedData:Object;

    var verifyPromise:Promisable<Verify>;


    public function new(){
        super();

        fakeSigner = new FakeSigner();
        fakeVerifier = new FakeVerifier();

        objVerifier = new ObjectVerifier(fakeVerifier);
    }

    // given ---------------------------------------------------

    @step
    public function a_signature_created_for_some_object(obj: Dynamic) {
        signedData = obj;

        ObjectSigner.makeSignature(fakeSigner, obj);
        signature = fakeSigner.resolveSignature();
    }

    // when  ---------------------------------------------------

    @step
    public function verifying_the_signature_against_the_same_object() {
        verifyPromise = objVerifier.verify(signedData, signature);
    }

    // then  ---------------------------------------------------

    @step
    public function the_same_fields_and_values_should_be_verified() {
        assertThat(fakeVerifier.values(), is(array(fakeSigner.values())),
            'Verified values does not match the expected.');

        assertThat(fakeVerifier.capturedSignature, is(notNullValue()),
            "The signature was not verified");
        assertThat(fakeVerifier.capturedSignature, is(equalTo(signature)),
            "The verified signature does not match the expected");
    }

    @step
    public function a_verification_promise_should_be_produced() {
        assertThat(verifyPromise, is(equalTo(fakeVerifier.resultPromise)),
            "Returned verify promise does not match the expected");
    }
}