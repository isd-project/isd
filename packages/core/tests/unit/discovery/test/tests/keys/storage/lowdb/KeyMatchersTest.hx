package discovery.test.tests.keys.storage.lowdb;

import discovery.test.helpers.RandomGen;
import discovery.impl.keys.storage.lowdb.KeyDataStruct;
import discovery.keys.crypto.Fingerprint;
import discovery.impl.keys.storage.lowdb.matchers.FingerprintPrefixMatcher;
import discovery.impl.keys.storage.lowdb.matchers.FingerprintKeyMatcher;
import discovery.keys.storage.KeyData;
import discovery.impl.keys.storage.lowdb.matchers.Matcher;
import discovery.impl.keys.storage.lowdb.matchers.KeyMatchers.*;
import haxe.io.Bytes;
import discovery.keys.crypto.HashTypes;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

class KeyMatchersTest implements Scenario{
    @steps
    var steps:KeyMatchersSteps;

    @Test
    @scenario
    public function fingerprint_match() {
        var fingerprint: Fingerprint = {
            alg: HashTypes.Sha256,
            hash: Bytes.ofHex("582912f78536f7107e025636f2520de96283b71faea4e6c35584bf7bc69a6249")
        };

        test_fingerprint_match(
            fingerprint,
            new FingerprintKeyMatcher(fingerprint));
    }

    @Test
    @scenario
    public function fingerprint_hash_match() {
        var hash:String = "582912f78536f7107e025636f2520de96283b71faea4e6c35584bf7bc69a6249";

        given().this_matcher(new FingerprintKeyMatcher({
            alg: null,
            hash: Bytes.ofHex(hash)
        }));

        then().it_should_match({
            fingerprint: { alg: HashTypes.Sha256, hash: Bytes.ofHex(hash)}
        });
        and().it_should_match({
            fingerprint: { alg: HashTypes.Md5, hash: Bytes.ofHex(hash)}
        });
        but().it_should_not_match({
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex(hash.substr(-8) + "deadbeef")
            },
        },
        "hashs are different");
    }


    @Test
    @scenario
    public function fingerprint_prefix_match() {
        var prefix:String = "58291";
        test_prefix_match(prefix, new FingerprintPrefixMatcher(prefix));
    }

    @Test
    @scenario
    public function key_identifier_name() {
        var hash1:String = "582912f78536f7107e025636f2520de96283b71faea4e6c35584bf7bc69a6249";

        given().this_matcher(matchKeyIdentifier("my key"));

        then().it_should_match({
            name: "my key",
            fingerprint: { alg: Sha256, hash: Bytes.ofHex(hash1)}
        });
        but().it_should_not_match({
            name: "other key",
            fingerprint: { alg: Sha256, hash: Bytes.ofHex(hash1)}
        });
        and().it_should_not_match({
            fingerprint: { alg: Sha256, hash: Bytes.ofHex(hash1)}
        });
    }

    @Test
    @scenario
    public function key_identifier_match_prefix() {
        var prefix:String = "58291";

        test_prefix_match(prefix, matchKeyIdentifier(prefix));
    }



    @Test
    @scenario
    public function null_name_matcher_should_not_match_anything(){
        given().this_matcher(matchName(null));
        then().it_should_not_match({ name: Random.string(5)});
        and().it_should_not_match({ name: null });
        and().it_should_not_match({ fingerprint: RandomGen.crypto.fingerprint()});
    }

    // --------------------------------------------------------------------

    function test_fingerprint_match<K>(fingerprint:Fingerprint, matcher:Matcher<K>)
    {
        var hash = fingerprint.hash.toHex();

        given().this_matcher(matcher);

        then().it_should_match({
            fingerprint: fingerprint
        });
        but().it_should_not_match({
            fingerprint: {
                alg: fingerprint.alg,
                hash: Bytes.ofHex(hash.substr(-8) + "deadbeef")
            },
        },
        "hashs are different");
        and().it_should_not_match({
            fingerprint: {
                alg: HashTypes.Md5,
                hash: fingerprint.hash
            }
        },
        "algorithms are different");
    }

    function test_prefix_match<K>(prefix:String, matcher: Matcher<K>) {
        given().this_matcher(matcher);

        then().it_should_match({
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex(prefix + '2f78536f7107e025636f2520d')
            }
        });
        and().it_should_match({
            fingerprint: {
                alg: null,
                hash: Bytes.ofHex(prefix + 'b71faea4e6c35584f7bc69a6249')
            }
        });
        but().it_should_not_match({
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex(prefix.substr(-2))
            },
        },
        "incomplete prefix");

        and().it_should_not_match({
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex('123' + prefix)
            },
        },
        "prefix preceded by other value");
    }
}

class KeyMatchersSteps extends Steps<KeyMatchersSteps> {
    var matcher:Matcher<KeyData>;

    @step
    public function this_matcher<K>(matcher: Matcher<K>) {
        this.matcher = cast matcher;
    }

    @step
    public function it_should_match(key: KeyData) {
        then().the_match_should_be(key, true);
    }

    @step
    public function it_should_not_match(key: KeyData, ?reason: String) {
        then().the_match_should_be(key, false, reason);
    }

    @step
    public function the_match_should_be(key: KeyData, result:Bool, ?reason: String) {
        var should = if(result) 'should' else 'should not';

        assertThat(matcher.match(key), is(result),
            'Matcher ${Std.string(matcher)}, $should match key: ${Std.string(key)}'
            + if(reason != null) ' because: $reason' else ''
        );
    }
}