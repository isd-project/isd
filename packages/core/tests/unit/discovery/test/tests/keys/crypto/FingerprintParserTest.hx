package discovery.test.tests.keys.crypto;

import discovery.exceptions.UnsupportedParameterException;
import discovery.format.exceptions.InvalidValueException;
import discovery.format.exceptions.InvalidFormatException;
import haxe.Exception;
import discovery.keys.crypto.FingerprintParser;
import haxe.io.Bytes;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class FingerprintParserTest implements Scenario{
    @steps
    var steps: FingerprintParserSteps;

    @Test
    public function parse_fingerprint(){
        test_parse_fingerprint('SHA-256:b17e667675c7d53ba02a26',
            new Fingerprint(Sha256, Bytes.ofHex('b17e667675c7d53ba02a26')));

        //uppercase bytes
        test_parse_fingerprint('SHA-256:B17E667675C7D53BA02A26',
            new Fingerprint(Sha256, Bytes.ofHex('b17e667675c7d53ba02a26')));

        //lowercase alg
        test_parse_fingerprint('sha-256:b17e667675c7d53ba02a26',
            new Fingerprint(Sha256, Bytes.ofHex('b17e667675c7d53ba02a26')));

        //without fingerprint
        test_parse_fingerprint("57a136618936a608af",
            new Fingerprint(null, Bytes.ofHex("57a136618936a608af")));

        //Custom hash (we support other hashs here, so underlying implementation can handle then - or reject)
        test_parse_fingerprint("NewHash:4b2359c648",
            new Fingerprint(cast "NewHash", Bytes.ofHex('4b2359c648')));
    }


    @scenario
    public
        function test_parse_fingerprint(fingStr:String, expected:Fingerprint)
    {
        given().a_fingerprint_parser();
        when().parsing_this_fingerprint(fingStr);
        then().this_fingerprint_should_be_produced(expected);
    }

    @Test
    public function parse_invalid_fingerprint_template(){
        //without hash
        parse_invalid_fingerprint("SHA-256:", InvalidFormatException);

        //blank
        parse_invalid_fingerprint("", InvalidFormatException);

        //null
        parse_invalid_fingerprint(null, InvalidFormatException);

        //invalid (odd) hexadecimal
        parse_invalid_fingerprint("SHA-256:12345", InvalidValueException);
    }

    @scenario
    public function parse_invalid_fingerprint(fingString:String,
                                                excType:Class<Exception>)
    {
        given().a_fingerprint_parser();
        when().trying_to_parse_this_fingerprint(fingString);
        then().an_exception_should_be_throw_of_this_type(excType);
    }
}

class FingerprintParserSteps extends Steps<FingerprintParserSteps>{
    var parser:FingerprintParser;
    var parsedFing:Fingerprint;

    var capturedException:Exception;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_fingerprint_parser() {
        parser = new FingerprintParser();
    }

    // when  ---------------------------------------------------


    @step
    public function trying_to_parse_this_fingerprint(fingString:String) {
        try{
            when().parsing_this_fingerprint(fingString);
        }
        catch(e){
            capturedException = e;
        }
    }

    @step
    public function parsing_this_fingerprint(fingStr:String) {
        parsedFing = parser.parse(fingStr);
    }

    // then  ---------------------------------------------------

    @step
    public function this_fingerprint_should_be_produced(expected:Fingerprint)
    {
        parsedFing.shouldBeEqualsTo(expected);
    }

    @step
    public function
        an_exception_should_be_throw_of_this_type(excType:Class<Exception>)
    {
        capturedException.shouldBeAn(excType);
    }
}