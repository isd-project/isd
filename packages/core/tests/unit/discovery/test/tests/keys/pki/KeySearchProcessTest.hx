package discovery.test.tests.keys.pki;

import discovery.keys.pki.KeySearchProcess;
import haxe.Exception;
import discovery.test.common.mockups.keytransport.KeyTransportMockup;
import discovery.test.common.mockups.keytransport.KeyValidatorMockup;
import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.keys.Key;
import discovery.async.promise.Promisable;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class KeySearchProcessTest implements Scenario{
    @steps
    var steps: KeySearchProcessSteps;

    @Test
    @scenario
    public function start_search(){
        given().a_key_fingerprint_not_on_cache();
        given().a_key_search_for_that_fingerprint();
        when().start_searching_this_key();
        then().a_query_for_that_key_should_be_sent();
        and().a_key_promise_should_be_returned();
    }

    @Test
    @scenario
    public function once_a_valid_key_is_found_then_the_key_should_be_returned()
    {
        given().a_key_search_was_started();
        when().a_valid_key_is_found();
        then().the_resolved_key_should_be_returned();
    }


    @Test
    @scenario
    public function once_found_a_valid_key_the_search_should_be_stopped(){
        given().a_key_search_was_successful();
        then().the_key_search_should_be_stopped();
    }

    // cache --------------------------------------------------------

    @Test
    @scenario
    public function key_should_not_be_search_if_it_is_on_cache(){
        given().a_key_is_stored_on_cache();
        when().searching_for_that_key();
        then().key_query_should_not_be_started();
        and().the_key_should_be_retrieved_from_cache();
    }

    @Test
    @scenario
    public function once_search_succeeds_the_key_should_be_stored_on_cache(){
        given().a_key_fingerprint_not_on_cache();
        when().the_key_search_succeeds();
        then().the_key_should_be_stored_on_cache();
    }

    @Test
    @scenario
    public function key_should_not_be_stored_if_it_is_already_on_cache(){
        given().a_key_is_stored_on_cache();
        when().searching_for_that_key();
        then().the_key_should_not_be_stored_on_cache();
    }

    // --------------------------------------------------------

    @Test
    @scenario
    public function should_start_search_only_once()
    {
        given().a_key_search_was_started();
        when().starting_the_search_again();
        then().no_other_query_should_be_started();
        and().the_same_promise_should_be_returned();
    }

    @Test
    @scenario
    public function should_not_notify_resolution_more_than_once(){
        given().a_key_search_was_started();
        when().more_than_one_valid_key_is_retrieved();
        then().only_the_first_received_key_should_be_notified();
        and().the_key_search_should_be_stopped_only_once();
    }

    @Test
    @scenario
    public function notify_failure_on_key_search(){
        given().a_key_search_was_started();
        when().the_key_retrieval_ends_with_a_failure();
        then().the_key_search_should_finish_and_the_failure_notified();
    }

    @Test
    @scenario
    public function notify_failure_if_no_key_is_found(){
        given().a_key_search_was_started();
        when().the_key_retrieval_ends_without_finding_a_valid_key();
        then().the_key_search_should_finish_with_an_error();
    }
}

class KeySearchProcessSteps extends Steps<KeySearchProcessSteps>{
    var keySearch:KeySearchProcess;

    var keyTransportMockup:KeyTransportMockup;
    var fakePromises:FakePromisableFactory;
    var keyCache:KeychainMockup;
    var keyLoaderMockup:KeyLoaderMockup;
    var keyValidatorMockup:KeyValidatorMockup;

    var keyFingerprint:Fingerprint;
    var keyPromise: Promisable<Key>;
    var previousPromise: Promisable<Key>;

    var capturedKey: Key;
    var resolveCount:Int = 0;

    var searchFinished:Bool;
    var expectedFailure:Exception;
    var capturedError:Exception;

    public function new(){
        super();

        keyTransportMockup  = new KeyTransportMockup();
        fakePromises        = new FakePromisableFactory();
        keyCache            = new KeychainMockup();
        keyLoaderMockup     = new KeyLoaderMockup();
        keyValidatorMockup  = new KeyValidatorMockup();

        keyLoaderMockup.resolveLoadExportedKey();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_search_was_successful() {
        given().a_key_search_was_started();
        when().a_valid_key_is_found();
    }

    @step
    public function a_key_search_was_started() {
        given().a_key_fingerprint_not_on_cache();
        given().a_key_search_for_that_fingerprint();
        when().start_searching_this_key();
    }

    @step
    public function more_than_one_valid_key_is_retrieved() {
        given().a_valid_key_is_found();
        and().a_valid_key_is_found();
    }

    @step
    public function a_key_is_stored_on_cache() {
        given().a_key_fingerprint();
        given().a_key_search_for_that_fingerprint();
        given().this_key_is_stored_on_cache();
    }

    @step
    public function a_key_fingerprint_not_on_cache() {
        given().a_key_fingerprint();
        given().this_key_is_not_on_cache();
    }

    @step
    public function a_key_fingerprint() {
        keyFingerprint = RandomGen.crypto.fingerprint();
    }

    @step
    public function a_key_search_for_that_fingerprint() {
        keySearch = new KeySearchProcess(keyFingerprint, {
            keyTransport: keyTransportMockup.keyTransport(),
            promises: fakePromises,
            keyCache: keyCache.keychain(),
            keyLoader: keyLoaderMockup.keyLoader(),
            keyValidator: keyValidatorMockup.keyValidator()
        });
    }

    @step
    function this_key_is_stored_on_cache() {
        keyCache.resolveGetKeyToDefault(keyFingerprint);
    }

    @step
    function this_key_is_not_on_cache() {
        keyCache.resolveGetKeyTo(keyFingerprint, null);
    }

    // when  ---------------------------------------------------

    @step
    public function the_key_search_succeeds() {
        given().a_key_search_for_that_fingerprint();
        when().start_searching_this_key();
        when().a_valid_key_is_found();
    }

    @step
    public function searching_for_that_key() {
        when().start_searching_this_key();
    }

    @step
    public function start_searching_this_key() {
        keyPromise = keySearch.start();

        if(previousPromise == null){
            keyPromise.then((key)->{
                capturedKey = key;
                this.resolveCount++;
            }, (err)->{
                capturedError = err;
                return null;
            }).finally(()->searchFinished = true);
        }
    }

    @step
    public function starting_the_search_again() {
        previousPromise = keyPromise;

        when().start_searching_this_key();
    }


    @step
    public function a_valid_key_is_found() {
        var keyFound = RandomGen.crypto.publicKeyData();
        keyTransportMockup.notifySearchResultFor(keyFingerprint, {
            found: keyFound
        });
    }

    @step
    public function the_key_retrieval_ends_with_a_failure() {
        expectedFailure = new Exception('Example failure');

        when().the_key_retrieval_ends_with(expectedFailure);
    }

    @step
    public function the_key_retrieval_ends_without_finding_a_valid_key() {
        when().the_key_retrieval_ends_with(null);
    }

    @step
    function the_key_retrieval_ends_with(?err: Exception) {
        keyTransportMockup.notifySearchEnd(keyFingerprint, err);
    }

    // then  ---------------------------------------------------

    @step
    public function a_query_for_that_key_should_be_sent() {
        keyTransportMockup.verifySearchKeyCalled(keyFingerprint);
    }

    @step
    public function no_other_query_should_be_started() {
        keyTransportMockup.verifySearchKeyCalled(keyFingerprint, times(1));
    }

    @step
    public function key_query_should_not_be_started() {
        keyTransportMockup.verifySearchKeyCalled(keyFingerprint, never);
    }

    @step
    public function a_key_promise_should_be_returned() {
        assertThat(keyPromise, is(notNullValue()),
            "Returned keyPromise should not be null");
    }

    @step
    public function the_key_should_be_retrieved_from_cache() {
        then().the_resolved_key_should_be_returned();
    }

    @step
    public function the_resolved_key_should_be_returned() {
        assertThat(capturedKey, is(notNullValue()),
            "Key was not received");
    }

    @step
    public function the_key_search_should_finish_and_the_failure_notified() {
        then().the_key_search_should_finish_with_an_error();

        capturedError.shouldBeEqualsTo(expectedFailure,
            'Captured error is not the expected');
    }

    @step
    public function the_key_search_should_finish_with_an_error() {
        searchFinished.shouldBe(true, 'The search was not finished');
        capturedError.shouldNotBeNull('No captured error');
    }

    @step
    public function the_key_search_should_be_stopped() {
        keyTransportMockup.verifySearchStopped(keyFingerprint);
    }

    @step
    public function the_key_search_should_be_stopped_only_once() {
        keyTransportMockup.verifySearchStopped(keyFingerprint, times(1));
    }

    @step
    public function the_same_promise_should_be_returned() {
        assertThat(keyPromise, is(theInstance(previousPromise)),
            'The returned key promise was not the expected'
        );
    }

    @step
    public function only_the_first_received_key_should_be_notified() {
        assertThat(resolveCount, is(1),
            "The number ok keys received does not match the expected");
    }

    @step
    public function the_key_should_be_stored_on_cache() {
        this.keyCache.verifyKeyStored(capturedKey);
    }

    @step
    public function the_key_should_not_be_stored_on_cache() {
        this.keyCache.verifyKeyStored(capturedKey, times(0));
    }
}