package discovery.test.tests.keys;

import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import discovery.keys.storage.ExportedKeyPair;
import discovery.test.helpers.RandomGen;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.storage.KeyData;
import discovery.test.common.mockups.KeyMockup;
import discovery.keys.KeyTransformations;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class KeyTransformationsTest implements Scenario{
    @steps
    var steps: KeyTransformationsSteps;


    @Test
    public function key_to_key_data_test(){
        for(hasPrivateKey in [true, false]){
            key_to_key_data(hasPrivateKey);
        }
    }

    @scenario
    public function key_to_key_data(hasPrivateKey: Bool){
        given().a_key_that(hasPrivateKey);
        when().transforming_the_key_into_key_data();
        then().a_correspondent_key_data_should_be_generated();

        if(hasPrivateKey){
            and().the_private_key_should_also_be_exported();
        }
    }

    @Test
    @scenario
    public function extract_public_key_data_from_key_pair_data()
    {
        given().a_key_data_with_public_key_info();
        when().extracting_the_public_key_data();
        then().the_public_key_fields_should_be_extracted_from_the_key_data();
    }


    @Test
    @scenario
    public function convert_public_key_data_into_key_data()
    {
        given().a_public_key_data_instance();
        when().converting_the_public_key_data_to_key_data();
        then().the_public_key_fields_should_be_keeped();
        but().the_key_data_should_not_have_private_key_info();
        and().the_key_data_should_have_no_name();
    }

    @Test
    @scenario
    public function exported_key_pair_to_key_data(){
        var keyPair:ExportedKeyPair = {
            privateKey: RandomGen.crypto.exportedKey(true),
            publicKey: RandomGen.crypto.exportedKey(false)
        };

        var keyData = KeyTransformations.exportedKeyPairToKeyData(keyPair);

        keyData.shouldNotBeNull();
        keyData.privateKey.shouldBeEqualsTo(keyPair.privateKey);
        keyData.publicKey.shouldBeEqualsTo(keyPair.publicKey);

        keyData.fingerprint.shouldBeTheSame(null);
        keyData.keyType.shouldBeTheSame(null);
        keyData.name.shouldBeTheSame(null);
    }


    @Test
    @scenario
    public function fill_key_data__without_fingerprint(){
        given().a_key_loader();
        given().key_data_without_fingerprint();

        when().filling_key_data();

        then().the_public_key_should_be_loaded();
        then().the_key_name_and_exported_keys_should_be_the_same();
        but().the_fingerprint_should_be_calculated();
    }

    @Test
    @scenario
    public function fill_key_data__without_key_type(){
        given().a_key_loader();
        given().key_data_without_key_type();

        when().filling_key_data();

        then().the_public_key_should_be_loaded();
        then().the_key_name_and_exported_keys_should_be_the_same();
        but().the_key_type_should_be_loaded();
    }
}

class KeyTransformationsSteps extends Steps<KeyTransformationsSteps>{

    var key:KeyMockup;
    var keyLoader = new KeyLoaderMockup();

    var keyFormat:KeyFormat = Der;
    var keyData:KeyData;
    var partialKeyData:KeyData;
    var publicKeyData:PublicKeyData;

    public function new(){
        super();

        key = new KeyMockup();

        keyLoader.resolveLoadExportedKey();
    }

    // given ---------------------------------------------------

    //omit @step keyword, to create the message dynamically
    public function a_key_that(hasPrivateKey:Bool) {
        var hasPrivKeyMsg = if(hasPrivateKey) 'has' else 'has no';
        given('a key that ${hasPrivKeyMsg} private key', ()->{
            key = new KeyMockup({
                hasPrivateKey: hasPrivateKey
            });
        });

    }

    @step
    public function a_key_data_with_public_key_info() {
        keyData = RandomGen.crypto.keyData();
    }

    @step
    public function a_public_key_data_instance() {
        publicKeyData = RandomGen.crypto.publicKeyData();
    }

    @step
    public function a_key_loader() {}

    @step
    public function key_data_without_fingerprint() {
        given().a_partial_key_data();

        partialKeyData.fingerprint = null;
    }

    @step
    public function key_data_without_key_type() {
        given().a_partial_key_data();

        partialKeyData.keyType = null;
    }

    @step
    function a_partial_key_data() {
        partialKeyData = RandomGen.crypto.keyData();
    }

    // when  ---------------------------------------------------

    @step
    public function transforming_the_key_into_key_data() {
        KeyTransformations.toKeyData(key.key, keyFormat)
            .then((receivedKeyData)->{
                keyData = receivedKeyData;
            });
    }

    @step
    public function extracting_the_public_key_data() {
        publicKeyData = KeyTransformations.toPublicKeyData(keyData);
    }

    @step
    public function converting_the_public_key_data_to_key_data() {
        keyData = KeyTransformations.convertToKeyData(publicKeyData);
    }

    @step
    public function filling_a_complete_key_data() {
        partialKeyData = RandomGen.crypto.keyData();

        partialKeyData.fingerprint.shouldNotBeNull();
        partialKeyData.keyType.shouldNotBeNull();

        when().filling_key_data();
    }

    @step
    public function filling_key_data() {
        var loader = keyLoader.keyLoader();
        KeyTransformations.fillKeyData(loader, partialKeyData)
            .then((filledKey)->{
                keyData = filledKey;
            }, (err)->throw err);
    }


    // then  ---------------------------------------------------

    @step
    public function a_correspondent_key_data_should_be_generated() {
        assertThat(keyData, is(notNullValue()));
        assertThat(keyData.publicKey.format, is(equalTo(keyFormat)));
    }

    @step
    public function the_private_key_should_also_be_exported() {
        assertThat(keyData.privateKey, is(notNullValue()));
        assertThat(keyData.publicKey.format, is(equalTo(keyFormat)));
    }

    @step
    public
    function the_public_key_fields_should_be_extracted_from_the_key_data() {
        publicKeyData.fingerprint.shouldBeEqualsTo(keyData.fingerprint);
        publicKeyData.keyType.shouldBeEqualsTo(keyData.keyType);
        publicKeyData.key.shouldBeEqualsTo(keyData.publicKey);
    }

    @step
    public function the_public_key_fields_should_be_keeped() {
        keyData.fingerprint.shouldBeEqualsTo(publicKeyData.fingerprint);
        keyData.keyType.shouldBeEqualsTo(publicKeyData.keyType);
        keyData.publicKey.shouldBeEqualsTo(publicKeyData.key);
    }

    @step
    public function the_key_data_should_not_have_private_key_info() {
        keyData.privateKey.shouldBe(null);
    }

    @step
    public function the_key_data_should_have_no_name() {
        keyData.name.shouldBe(null);
    }

    @step
    public function the_public_key_should_be_loaded() {
        keyLoader.shouldHaveLoadedExportedKey(partialKeyData.publicKey);
    }

    @step
    public function the_key_name_and_exported_keys_should_be_the_same() {
        keyData.shouldNotBeNull('KeyData should not be null');

        keyData.name.shouldBeEqualsTo(partialKeyData.name);
        keyData.privateKey.shouldBeEqualsTo(partialKeyData.privateKey);
        keyData.publicKey.shouldBeEqualsTo(partialKeyData.publicKey);
    }

    @step
    public function the_fingerprint_should_be_calculated() {
        var expectedFingerprint = keyLoader.loadedKeyMockup().fingerprint();

        keyData.fingerprint.shouldBeEqualsTo(expectedFingerprint,
            'Fingerprint value does not match the expectation');
    }

    @step
    public function the_key_type_should_be_loaded() {
        var expectedKeyType = keyLoader.loadedKeyMockup().key().keyType;

        keyData.keyType.shouldBeEqualsTo(expectedKeyType, 'KeyType does not match the expected');
    }
}