package discovery.test.tests.keys;

import discovery.test.steps.SerializerTestSteps;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.serialization.PublicKeyDataSerializer;
import discovery.domain.serialization.Serializer;
import discovery.test.helpers.RandomGen;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class PublicKeyDataSerializerTest implements Scenario{
    @steps
    var steps: PublicKeyDataSerializerSteps;

    @Test
    @scenario
    public function serialize_and_unserialize(){
        given().a_serializer_instance();
        when().serializing_this(RandomGen.crypto.publicKeyData());
        and().deserializing_a_copy_back();
        then().a_new_equals_instance_should_be_produced();
    }
}

class PublicKeyDataSerializerSteps
    extends SerializerTestSteps<PublicKeyData, PublicKeyDataSerializerSteps>
{
    function buildSerializer():Serializer<PublicKeyData> {
        return new PublicKeyDataSerializer();
    }
}