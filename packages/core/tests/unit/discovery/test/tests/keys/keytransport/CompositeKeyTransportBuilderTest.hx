package discovery.test.tests.keys.keytransport;

import discovery.test.helpers.RandomGen;
import haxe.exceptions.NotImplementedException;
import discovery.test.common.mockups.keytransport.KeyTransportBuilderMockup;
import discovery.test.common.mockups.CompositeMockup;
import discovery.keys.keydiscovery.CompositeKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.base.configuration.composite.CompositeKeyTransportBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.IteratorTools;


class CompositeKeyTransportBuilderTest implements Scenario{
    @steps
    var steps: CompositeKeyTransportBuilderSteps;

    @Test
    @scenario
    public function build_composite(){
        given().a_composite_builder();
        when().building_the_composite();
        then().a_composite_keytransport_instance_should_be_created();
    }

    @Test
    @scenario
    public function composite_build(){
        given().a_composite_builder();
        when().adding_some_keytransport_builders_to_the_composite();
        and().building_the_composite();
        then().the_components_should_be_built();
        and().a_composite_keytransport_should_be_created_with_those_components();
    }

    @Test
    @scenario
    public function configure_builders(){
        given().a_composite_builder();
        when().adding_a_configurable_builder_named_as('example');
        and().configuring_the_composite_builder_with({
            example: {
                someConfig: true
            }
        });
        then().the_configurable_builder_should_be_configured_with({
            someConfig: true
        });
    }
}

class CompositeKeyTransportBuilderSteps
    extends Steps<CompositeKeyTransportBuilderSteps>
{
    var compositeBuilder:CompositeKeyTransportBuilder;
    var builtKeyTransport:KeyTransport;

    var builderMockups:CompositeMockup<KeyTransportBuilderMockup>;
    var configurableBuilderMockup:KeyTransportBuilderMockup;

    public function new(){
        super();

        builderMockups = new CompositeMockup(()->{
            return new KeyTransportBuilderMockup();
        });
    }

    // given ---------------------------------------------------

    @step
    public function a_composite_builder() {
        compositeBuilder = new CompositeKeyTransportBuilder();
    }

    // when  ---------------------------------------------------

    @step
    public function adding_some_keytransport_builders_to_the_composite() {
        builderMockups.populate();

        builderMockups.forEach((m)->{
            var name = RandomGen.primitives.name();
            compositeBuilder.addBuilder(m.builder(), name);
        });
    }

    @step
    public function building_the_composite() {
        builtKeyTransport = compositeBuilder.buildKeyTransport();
    }

    @step
    public function adding_a_configurable_builder_named_as(name:String) {
        configurableBuilderMockup = new KeyTransportBuilderMockup();

        compositeBuilder.addBuilder(configurableBuilderMockup.builder(), name);
    }

    @step
    public function configuring_the_composite_builder_with(config:Dynamic) {
        compositeBuilder.configure(config);
    }

    // then  ---------------------------------------------------

    @step
    public function a_composite_keytransport_instance_should_be_created()
    {
        builtKeyTransport.shouldBeAn(CompositeKeyTransport);
    }

    @step
    public function the_components_should_be_built() {
        builderMockups.forEach((m)->m.verifyBuildCalled());
    }

    @step
    public
    function a_composite_keytransport_should_be_created_with_those_components()
    {
        var expectedComponents = builderMockups.map((mBuilder)->{
            return mBuilder.builtValue();
        });

        var composite = cast(builtKeyTransport, CompositeKeyTransport);

        composite.components().toArray()
                 .shouldBeEqualsTo(expectedComponents);
    }

    @step
    public
    function the_configurable_builder_should_be_configured_with(config:Any)
    {
        configurableBuilderMockup.verifyConfiguredWith(config);
    }
}

