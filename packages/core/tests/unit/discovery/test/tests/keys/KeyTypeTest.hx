package discovery.test.tests.keys;

import haxe.io.Bytes;
import massive.munit.Assert;
import discovery.keys.crypto.KeyTypes;
import discovery.keys.crypto.KeyType;

import haxe.EnumTools;

class KeyTypeTest {

    @Test
    public function compare_types(){
        shouldNotBeEquals(RSA(),   EllipticCurve());
    }

    @Test
    public function rsa_from_parameters_equals(){
        shouldBeEquals(RSA({
            modulusLength: 1024
        }),
        KeyType.createByName("RSA", [
            {modulusLength: 1024}
        ]));
    }

    @Test
    public function rsa_compare_params(){
        shouldNotBeEquals(RSA({modulusLength: 1024}),   RSA());
        shouldNotBeEquals(RSA({
            modulusLength:2048,
            publicExponent: Bytes.ofHex("abcdef")}),
        RSA({
            modulusLength: 2048
        }));
    }


    @Test
    public function elliptic_curves_compare(){
        shouldBeEquals(EllipticCurve(),   EllipticCurve());
        shouldBeEquals(
            EllipticCurve({namedCurve: "X"}),
            EllipticCurve({namedCurve: "X"}));
        shouldNotBeEquals(
            EllipticCurve(),
            EllipticCurve({namedCurve: "X"}));
        shouldNotBeEquals(
            EllipticCurve({namedCurve: "X"}),
            EllipticCurve({namedCurve: "Y"}));
    }

    @Test
    public function elliptic_curves_from_parameters_compare(){
        shouldBeEquals(
            EllipticCurve(),
            KeyType.createByName("EllipticCurve", []));
        shouldBeEquals(
            EllipticCurve({namedCurve: "X"}),
            KeyType.createByName("EllipticCurve", [{namedCurve: "X"}]));
    }

    function shouldBeEquals(keyType1: KeyType, keyType2: KeyType) {
        var msg =
        'KeyType: ${Std.string(keyType1)}\n' +
        'should be equal to: ${Std.string(keyType2)}';

        Assert.isTrue(keyType1.equals(keyType2), msg);
        Assert.isTrue(keyType2.equals(keyType1), msg);
    }
    function shouldNotBeEquals(keyType1: KeyType, keyType2: KeyType) {
        var msg =
        'KeyType: ${Std.string(keyType1)}\n' +
        'should not be equal to: ${Std.string(keyType2)}';

        Assert.isFalse(keyType1.equals(keyType2), msg);
        Assert.isFalse(keyType2.equals(keyType1), msg);
    }
}