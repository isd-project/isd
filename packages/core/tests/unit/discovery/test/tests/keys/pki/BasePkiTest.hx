package discovery.test.tests.keys.pki;

import discovery.test.common.mockups.pki.KeyPublishListenerMockup;
import haxe.Exception;
import discovery.exceptions.composite.PartialFailureException;
import discovery.async.promise.Promisable;
import discovery.test.common.mockups.KeyMockup;
import discovery.keys.Key;
import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.common.mockups.keytransport.KeyTransportMockup;
import discovery.keys.pki.BasePki;
import discovery.test.common.mockups.pki.KeySearchProcessMockup;
import discovery.keys.PKI;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class BasePkiTest implements Scenario{
    @steps
    var steps: BasePkiSteps;

    @Test
    @scenario
    public function delegate_keychain_publication(){
        given().a_pki_instance();
        when().publishing_some_keychain();
        then().the_keychain_should_be_published_by_the_underlying_transport();
    }

    @Test
    @scenario
    public function delegate_key_search(){
        given().a_pki_instance();
        when().searching_for_some_key();
        then().the_key_should_be_searched_by_the_underlying_transport();
    }

    @Test
    @scenario
    public function publish_key(){
        given().a_pki_instance();
        given().some_key();
        when().publishing_that_key();
        then().the_key_should_be_exported_to_a_default_format_and_published();
    }

    @Test
    @scenario
    public function on_success_publication_should_resolve_the_publish_promise(){
        given().some_key_was_published();
        when().the_publication_completes();
        then().the_returned_promise_should_be_resolved_to_the_published_key();
    }

    @Test
    @scenario
    public function on_partial_failure_should_resolve_the_publish_promise(){
        given().a_key_publication_listener();
        given().some_key_was_published();
        when().the_publication_only_partially_fails();
        then().the_returned_promise_should_be_resolved_to_the_published_key();
        but().the_publication_errors_should_still_be_notified();
    }
}

class BasePkiSteps extends Steps<BasePkiSteps>{

    var pki:PKI;

    var keySearchBuilder:KeySearchProcessMockup;
    var keyTransportMockup:KeyTransportMockup;
    var keychainMockup:KeychainMockup;
    var keyMockup:KeyMockup;
    var keyPublishListener:KeyPublishListenerMockup;

    var keyFingerprint:Fingerprint;
    var keyToPublish:Key;
    var publishKeyPromise: Promisable<Key>;
    var expectedInternalErrors:Array<Exception>;

    public function new(){
        super();

        keySearchBuilder = new KeySearchProcessMockup();
        keyTransportMockup = new KeyTransportMockup();
        keychainMockup = new KeychainMockup();
        keyMockup = new KeyMockup();
        keyPublishListener = new KeyPublishListenerMockup();
    }

    // given summary -----------------------------------------------

    @step
    public function some_key_was_published() {
        given().a_pki_instance();
        given().some_key();
        when().publishing_that_key();
    }

    // given ---------------------------------------------------

    @step
    public function a_pki_instance() {
        pki = new BasePki({
            keyTransport: keyTransportMockup.keyTransport(),
            keySearchBuilder: keySearchBuilder
        });
    }

    @step
    public function some_key() {
        keyToPublish = keyMockup.key;
    }

    @step
    public function a_key_publication_listener()
    {
        //already set
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_some_keychain() {
        pki.publishKeychain(keychainMockup.keychain());
    }

    @step
    public function searching_for_some_key() {
        keyFingerprint = RandomGen.crypto.fingerprint();

        pki.searchKey(keyFingerprint);
    }

    @step
    public function publishing_that_key() {
        publishKeyPromise = pki.publishKey(keyToPublish,
                                            keyPublishListener.listener());
    }

    @step
    public function the_publication_completes() {
        keyTransportMockup.notifyPublication();
    }

    @step
    public function the_publication_only_partially_fails() {
        var err = new PartialFailureException([
            new Exception('failed 1'), null, new Exception('failed 2')
        ]);

        expectedInternalErrors = [err];
        keyTransportMockup.notifyPublicationWith(err);
    }

    // then  ---------------------------------------------------

    @step
    public function the_keychain_should_be_published_by_the_underlying_transport() {
        keyTransportMockup.verifyPublishedKeychain(keychainMockup.keychain());
    }

    @step
    public function the_key_should_be_searched_by_the_underlying_transport() {
        keySearchBuilder.verifyStartedSearchFor(keyFingerprint);
    }

    @step
    public function the_key_should_be_exported_to_a_default_format_and_published()
    {
        keyTransportMockup.verifyPublishedSomeKey();
    }

    @step
    public
        function the_returned_promise_should_be_resolved_to_the_published_key()
    {
        var promiseResolved = false;
        var capturedKey:Key = null;

        publishKeyPromise.then((key)->{
            capturedKey = key;
            promiseResolved = true;
        });

        promiseResolved.shouldBe(true, 'The promise was not resolved');
        capturedKey.shouldBe(keyToPublish, 'Captured key does not match');
    }

    @step
    public function the_publication_errors_should_still_be_notified() {
        keyPublishListener.verifyInternalErrorsReceived(expectedInternalErrors);
    }
}