package discovery.test.tests.keys.signature;

import discovery.keys.crypto.signature.Signed;
import discovery.format.Binary;
import discovery.async.promise.Promisable;
import discovery.test.common.fakes.FakeSigner;
import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.signature.ObjectSigner;
import discovery.keys.crypto.signature.ObjectDigestSymbols;

import discovery.keys.crypto.BinaryConverter;
import haxe.io.Bytes;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.signature.Signature;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


private enum MyEnum{ A; B; C; WithParams(params: String);}

class ObjectSignerTest implements Scenario{
    @steps
    var steps:ObjectSignerSteps;

    @Test
    @scenario
    public function produce_signed_object() {
        given().an_ObjectSigner_created_from_some_Signer();
        and().some_object();
        when().the_object_is_signed();
        and().the_signature_promise_is_resolved();
        then().a_signed_object_with_the_produced_signature_should_be_returned();
    }

    @Test
    @scenario
    public function sign_simple_object() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            example: "Hello!"
        });
        then().it_should_append_these_properties_to_the_signature([
            ["example", "Hello!"]
        ]
        );
    }

    @Test
    @scenario
    public function sign_should_be_ordered_by_properties_names() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            A: "1",
            C: "2",
            B: "3"
        });
        then().it_should_append_these_properties_to_the_signature([
            ["A", "1"],
            ["B", "3"],
            ["C", "2"]
        ]
        );
    }

    @Test
    @scenario
    public function sign_object_with_bytes() {
        var bytes = RandomGen.binary.bytes(Random.int(5, 10));

        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            bytes: bytes
        });
        then().it_should_append_these_properties_to_the_signature([
            ["bytes", bytes.toHex()]
        ]
        );
    }

    @Test
    @scenario
    public function sign_object_with_binary() {
        var bytes:Binary = RandomGen.binary.bytes(Random.int(5, 10));

        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            bytes: bytes
        });
        then().it_should_append_these_properties_to_the_signature([
            ["bytes", bytes.toHex()]
        ]
        );
    }

    @Test
    @scenario
    public function should_ignore_null_properties() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            example: "Hello!",
            nullvalue: null,
            othervalue: "other"
        });
        then().it_should_append_these_properties_to_the_signature([
            ["example", "Hello!"],
            ["othervalue", "other"]
        ]
        );
    }

    @Test
    @scenario
    public function sign_object_multiple_types() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            str : "string",
            int : 1,
            bool: true,
            float: 3.2
        });
        then().it_should_append_these_properties_to_the_signature(
            [
                ["bool", BinaryConverter.convert(true).toHex()],
                ["float", BinaryConverter.convert(3.2).toHex()],
                ["int", BinaryConverter.convert(1).toHex()],
                ["str", "string"]
            ]
        );
    }


    @Test
    @scenario
    public function sign_sub_object() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            subobject : {
                subprop: "hello!"
            }
        });
        then().it_should_append_these_values_to_the_signature(
            [
                "subobject", propertySeparator, startObject,
                    "subprop", propertySeparator, "hello!", endValue,
                endObject, endValue
            ]
        );
    }

    @Test
    @scenario
    public function sign_obj_with_array() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            values : [1,2,3]
        });
        then().it_should_append_these_values_to_the_signature(
            [
                "values", propertySeparator, startArray,
                    BinaryConverter.convert(1).toHex(), endValue,
                    BinaryConverter.convert(2).toHex(), endValue,
                    BinaryConverter.convert(3).toHex(), endValue,
                endArray, endValue
            ]
        );
    }


    @Test
    @scenario
    public function sign_simple_enum() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            myenum: MyEnum.A,
            otherenum: MyEnum.C
        });
        then().it_should_append_these_properties_to_the_signature(
            [
                ["myenum", "A"],
                ["otherenum", "C"]
            ]
        );
    }

    @Test
    @scenario
    public function sign_enum_with_parameters() {
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            enumparams: WithParams("Hello!")
        });
        then().it_should_append_these_values_to_the_signature(
            [
                "enumparams", propertySeparator, startObject,
                    "enumname", propertySeparator, "WithParams", endValue,
                    "params", propertySeparator, startArray,
                        "Hello!", endValue,
                    endArray, endValue,
                endObject, endValue
            ]
        );
    }

    @Test
    @scenario
    public function sign_object_instance(){
        given().an_ObjectSigner_created_from_some_Signer();
        when().signing_this_object({
            fingerprint: new Fingerprint(
                HashTypes.Sha256,
                Bytes.ofHex("ac9bfd3bc2320101")
            )
        });
        then().it_should_append_these_values_to_the_signature(
            [
                "fingerprint", propertySeparator, startObject,
                    "alg", propertySeparator, HashTypes.Sha256, endValue,
                    "hash", propertySeparator, "ac9bfd3bc2320101", endValue,
                endObject, endValue
            ]
        );
    }


    @Test
    @scenario
    public function transform_object_before_append(){
        given().an_ObjectSigner_created_from_some_Signer();
        given().this_transformation_was_set(Bytes, (bytes: Bytes)->{
            return "transformed bytes";
        });
        when().signing_this_object({
            integer: 10,
            binary: Bytes.ofHex("ac9bfd3bc2320101")
        });
        then().it_should_append_these_properties_to_the_signature(
            [
                ["binary", "transformed bytes"],
                ["integer", BinaryConverter.convert(10).toHex()]
            ]
        );
    }

    @Test
    @scenario
    public function transformations_with_Dynamic_should_apply_to_any_object(){
        given().an_ObjectSigner_created_from_some_Signer();
        given().this_transformation_was_set(Dynamic, (value: Dynamic)->{
            return "transformed!";
        });
        when().signing_this_object({
            integer: 10,
            binary: Bytes.ofHex("ac9bfd3bc2320101")
        });
        then().it_should_append_these_properties_to_the_signature(
            [
                ["binary", "transformed!"],
                ["integer", "transformed!"]
            ]
        );
    }
}


class ObjectSignerSteps extends Steps<ObjectSignerSteps> {
    var objSigner:ObjectSigner;

    var objToSign: Dynamic;
    var signedObject: Signed<Dynamic>;
    var signedPromise:Promisable<Signed<Any>>;

    var signer:FakeSigner;
    var signaturePromise:Promisable<Signature>;
    var signature:Signature;

    public function new() {
        super();

        signer = new FakeSigner();
    }


    // given ----------------------------------------------------------

    @step
    public function an_ObjectSigner_created_from_some_Signer() {
        objSigner = new ObjectSigner(signer);
    }

    @step
    public function some_object() {
        given().this_object(RandomGen.fullDescription());
    }

    @step
    public function this_object(obj: Dynamic) {
        objToSign = obj;
    }

    @step
    public function this_transformation_was_set<Tin, Tout>(
        type: Dynamic, transform:(Tin)->Tout)
    {
        objSigner.transform(type, transform);
    }

    // when ----------------------------------------------------------

    @step
    public function signing_this_object(obj:Dynamic) {
        given().this_object(obj);
        when().the_object_is_signed();
    }

    @step
    public function the_object_is_signed() {
        signedPromise = objSigner.signObject(objToSign);
    }

    @step
    public function the_signature_promise_is_resolved() {
        signature = signer.resolveSignature();

        signedPromise.then((signedObj)->{
            this.signedObject = signedObj;
        });
    }

    // then ----------------------------------------------------------

    @step
    public function a_signed_object_with_the_produced_signature_should_be_returned() {
        assertThat(signedObject, is(notNullValue()),
            "Signed object should not be null");
        assertThat(signedObject.signature, is(equalTo(signature)),
            "Signature does not match the expected value"
        );
        assertThat(signedObject.data, is(theInstance(objToSign)),
            "Signed object does not match the expected value"
        );
    }

    @step
    public function it_should_append_these_properties_to_the_signature(properties:Array<Array<Dynamic>>)
    {
        var expectedValues = [];

        for(prop in properties){
            expectedValues.push(prop[0]);
            expectedValues.push(propertySeparator);
            expectedValues.push(prop[1]);
            expectedValues.push(endValue);
        }

        then().it_should_append_these_values_to_the_signature(expectedValues);
    }

    @step
    public function it_should_append_these_values_to_the_signature(values:Array<Dynamic>) {
        assertThat(signer.values(), is(array(values)),
            'Wrong signed values: ${signer.values()}');
    }
}