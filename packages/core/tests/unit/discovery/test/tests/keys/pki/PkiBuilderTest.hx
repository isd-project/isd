package discovery.test.tests.keys.pki;

import discovery.test.fakes.FakePromisableFactory;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import discovery.async.promise.PromisableFactory;
import discovery.keys.PKI;
import discovery.test.common.mockups.keytransport.KeyTransportBuilderMockup;
import discovery.keys.pki.BasePkiBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class PkiBuilderTest implements Scenario{
    @steps
    var steps: PkiBuilderSteps;

    @Test
    @scenario
    public function build_pki_from_key_transport(){
        given().a_pki_builder();
        given().a_key_transport_builder_is_set();
        given().other_required_dependencies_are_set();
        when().building_the_pki();
        then().the_key_transport_should_be_built();
        and().a_valid_pki_instance_should_be_retrieved();
    }
}

class PkiBuilderSteps extends Steps<PkiBuilderSteps>{

    var pkiBuilder:BasePkiBuilder;

    var keyTransportBuilder:KeyTransportBuilderMockup;
    var promises:PromisableFactory;
    var keyLoader:KeyLoaderMockup;
    var keyCache:KeychainMockup;

    var pki:PKI;

    public function new(){
        super();

        keyTransportBuilder = new KeyTransportBuilderMockup();
        promises = new FakePromisableFactory();
        keyLoader = new KeyLoaderMockup();
        keyCache = new KeychainMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_pki_builder() {
        pkiBuilder = new BasePkiBuilder();
    }

    @step
    public function a_key_transport_builder_is_set() {
        pkiBuilder.transportBuilder(keyTransportBuilder.builder());
    }

    @step
    public function other_required_dependencies_are_set() {
        pkiBuilder
            .promises(promises)
            .keyLoader(keyLoader.keyLoader())
            .keyCache(keyCache.keychain());
    }

    // when  ---------------------------------------------------

    @step
    public function building_the_pki() {
        pki = pkiBuilder.buildPki();
    }

    // then  ---------------------------------------------------

    @step
    public function the_key_transport_should_be_built() {
        keyTransportBuilder.verifyBuildCalled();
    }

    @step
    public function a_valid_pki_instance_should_be_retrieved() {
        pki.shouldNotBeNull();
    }
}