package discovery.test.tests.keys.storage;

import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import discovery.impl.keys.storage.lowdb.KeyDataParser;
import hxgiven.Scenario;
import hxgiven.Steps;
import haxe.io.Bytes;
import discovery.keys.crypto.HashTypes;
import discovery.keys.storage.KeyData;

import org.hamcrest.Matchers.*;


class SerializableKeyDataTest implements Scenario{
    @steps
    var steps:SerializableKeyDataSteps;

    @Test
    @scenario
    public function toStructFromStruct() {
        given().some_KeyData();
        when().serializing_the_key();
        and().deserializing_the_key_data();
        then().the_KeyData_should_be_deserialized_correctly();
    }

    @Test
    @scenario
    public function toStructFromStructAutomatic() {
        given().some_KeyData();
        when().serializing_the_key_into_the_abstract();
        and().deserializing_the_key_data_from_the_abstract();
        then().the_KeyData_should_be_deserialized_correctly();
    }
}


class SerializableKeyDataSteps extends Steps<SerializableKeyDataSteps> {
    var keyData:KeyData;
    var deserializedKey:KeyData;
    var keyParser:KeyDataParser;
    var serialized:Dynamic;
    var serializableKeyData:SerializableKeyData;

    public function new() {
        super();
        keyParser = new KeyDataParser();
    }

    @step
    public function some_KeyData() {
        keyData = {
            keyType: RSA({
                    modulusLength: 2048,
                    // publicExponent: publicExponent
            }),
            publicKey: {
                format: Pem,
                data: "---BEGIN PUBLIC KEY--- ..."
            },
            privateKey: {
                format: Pem,
                data: "---BEGIN PRIVATE KEY--- ..."
            },
            name: "key name",
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex("a8d68aa3c5f972b8146a88a4eb20e646f591e56539f6d65c360bf1fb6a151a44")
            }
        };
    }

    @step
    public function serializing_the_key() {
        serialized = keyParser.toStructure(keyData);
    }

    @step
    public function serializing_the_key_into_the_abstract() {
        serializableKeyData = keyData;
    }

    @step
    public function deserializing_the_key_data() {
        deserializedKey = keyParser.toData(serialized);
    }

    @step
    public function deserializing_the_key_data_from_the_abstract() {
        deserializedKey = serializableKeyData;
    }

    // then --------------------------------------------------------------

    @step
    public function the_KeyData_should_be_deserialized_correctly() {
        assertThat(keyData.equals(deserializedKey), is(true),
        'expected: ${Std.string(keyData)}\n' +
        'found: ${Std.string(deserializedKey)}'
        );
    }
}