package discovery.test.tests.keys.storage;

import hxgiven.Scenario;
import discovery.format.EncodedData.TextWithFormat;
import discovery.test.helpers.RandomGen;
import haxe.exceptions.NotImplementedException;
import discovery.keys.storage.ExportedFingerprint;
import haxe.io.Bytes;
import discovery.keys.crypto.Fingerprint;

import org.hamcrest.Matchers.*;
using discovery.test.common.transformations.SerializationTransform;
using discovery.test.matchers.CommonMatchers;


class ExportedFingerprintTest implements Scenario{

    @Test
    @scenario
    public function from_to_fingerprint() {
        var fingerprint:Fingerprint = {
            alg: Sha256,
            hash: Bytes.ofHex("4b1dc57d2aee1de8ee7136597c860bd9798e3444d473eaa8e74df2ff463d9763")
        };

        var exportedFingerprint:ExportedFingerprint = fingerprint;
        var outFingerprint:Fingerprint = exportedFingerprint;

        assertThat(outFingerprint.alg, is(equalTo(fingerprint.alg)));
        assertThat(outFingerprint.hash.toHex(), is(equalTo(fingerprint.hash.toHex())));
    }


    @Test
    @scenario
    public function from_dynamic_to_fingerprint() {
        var fingerprint = RandomGen.crypto.fingerprint();
        var exported    = ExportedFingerprint.fromFingerprint(fingerprint);
        var dynFingerprint = exported.toJsonObject();

        var converted = ExportedFingerprint.fromDynamic(dynFingerprint);

        converted.shouldBeEqualsTo(exported);
        converted.hash.shouldBeAn(TextWithFormat);

        var fingConverted = converted.toFingerprint();
        fingConverted.shouldBeEqualsTo(fingerprint);
    }
}