package discovery.test.tests.keys;

import discovery.test.common.mockups.crypto.KeyGeneratorMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.helpers.RandomGen;
import discovery.keys.KeyIdentifier;
import discovery.keys.crypto.KeyFormats;
import discovery.keys.crypto.HashTypes;
import discovery.keys.Key;
import discovery.async.promise.Promisable;
import haxe.io.Bytes;

import discovery.keys.storage.KeyData;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.CryptoKey;
import discovery.test.fakes.FakePromisable;
import discovery.async.promise.PromiseHandler;
import discovery.keys.crypto.KeyPair;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.storage.KeyStorage;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.Keychain;
import discovery.keys.BaseKeychain;
import discovery.keys.crypto.KeyFormats.*;

import hxgiven.Steps;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class BaseKeychainTest implements Scenario{
    @steps
    var steps:BaseKeychainSteps;

    @Test
    @scenario
    public function start_generating_key() {
        given().a_keychain_instance();
        and().some_key_gen_options();
        when().generating_a_new_key();
        then().the_key_generation_should_start();
        and().a_key_promise_should_be_returned();
    }

    @Test
    @scenario
    public function export_and_store_key() {
        given().a_key_generation_was_started();
        when().a_key_is_generated();
        then().the_key_info_should_be_extracted();
        and().the_key_should_be_exported_and_stored();
        and().the_key_promise_should_be_resolved_to_a_valid_value();
    }


    @Test
    @scenario
    public function list_keys_delegation() {
        given().a_keychain_instance();
        when().listing_keys();
        then().the_keychain_should_return_the_stored_keyData();
    }

    @Test
    @scenario
    public function get_key_from_identifier() {
        given().a_keychain_instance();
        given().there_is_a_key_with_some_identifier();
        when().getting_a_key_from_this_identifier();
        then().the_key_should_be_retrieved_from_storage();
        and().the_key_should_be_loaded_and_returned();
    }

    // errors ---------------------

    @Test
    @scenario
    public function get_non_existent_key(){
        given().a_keychain_instance();
        given().an_identifier_that_does_not_match_to_any_existent_key();
        when().getting_a_key_from_this_identifier();
        then().a_null_key_should_be_retrieved();
    }
}

class BaseKeychainSteps extends Steps<BaseKeychainSteps> {

    var keychain:Keychain;
    var keystorage:KeyStorage;
    var keygenerator:KeyGenerator;
    var keygenMockup:KeyGeneratorMockup;
    var promisesFactory: FakePromisableFactory;

    var keyGenOptions:GenerateKeyOptions;

    var keypairMock: KeyPair;
    var keyFingerprint:Fingerprint;
    var keypairPromiseFake: FakePromisable<KeyPair>;
    var fingerprintPromiseFake: FakePromisable<Fingerprint>;
    var exportedKeyFakePromise: FakePromisable<ExportedKey>;
    var onFulfillKeypairPromise: PromiseHandler<KeyPair, Any>;

    var expectedKey:KeyData;
    var storedKey:KeyData;

    var mockedKeyList:Array<KeyData>;
    var listedKeys   :Array<KeyData>;

    var capturedKeyPromise:Promisable<Key>;
    var capturedKey:Key;

    var keyIdentifier:KeyIdentifier;

    public function new() {
        super();

        keystorage = mock(KeyStorage);
        keygenMockup = new KeyGeneratorMockup();
        keygenerator = keygenMockup.keyGenerator();
        promisesFactory = new FakePromisableFactory();

        keychain = new BaseKeychain(keygenerator, keystorage, promisesFactory);

        keypairPromiseFake = new FakePromisable<KeyPair>();
        mockedKeyList = [for(i in 0...8) makeKeyData()];

        Mockatoo.when(keygenerator.generateKey(isNotNull))
                .thenReturn(keypairPromiseFake);
        Mockatoo.when(keygenerator.loadKey(isNotNull))
                .thenReturn(keypairPromiseFake);
        Mockatoo
            .when(keystorage.addKey(any))
            .thenCall(onAddKey);
        Mockatoo
            .when(keystorage.listKeys())
            .thenReturn(mockedKeyList);
    }

    // -------------------------------------------------------------

    function mockKeyPair(key: KeyData) {
        return keypairMock = {
            publicKey: mockCryptoKey(key),
            privateKey: mockCryptoKey(key, false)
        };
    }

    function mockCryptoKey(keyData: KeyData, publicKey:Bool=true) {
        var key = mock(CryptoKey);

        Mockatoo
            .when(key.keyType)
            .thenReturn(keyData.keyType);
        Mockatoo
            .when(key.fingerprint())
            .thenReturn(new FakePromisable(keyData.fingerprint.toFingerprint()));

        var exportedKey = if(publicKey) keyData.publicKey else keyData.privateKey;
        Mockatoo
            .when(key.export(any))
            .thenReturn(new FakePromisable(exportedKey));

        return key;
    }

    function onAddKey(args: Array<Dynamic>) {
        assertThat(args, hasSize(greaterThan(0)));

        storedKey = args[0];
    }


    // given -------------------------------------------------------------

    @step
    public function a_keychain_instance()
    {}

    @step
    public function a_key_generation_was_started() {
        given().a_keychain_instance();
        and().some_key_gen_options();
        when().generating_a_new_key();
    }

    @step
    public function some_key_gen_options() {
        keyGenOptions = {
            keyType: RSA({modulusLength: 1024})
        };
    }

    @step
    public function there_is_a_key_with_some_identifier() {
        given().some_identifier();
        given().some_key_data();
        given().this_key_exist(keyIdentifier, expectedKey);

        this.keypairPromiseFake.resolve(mockKeyPair(expectedKey));
    }

    @step
    public function an_identifier_that_does_not_match_to_any_existent_key() {
        given().some_identifier();
        given().this_key_does_not_exist(keyIdentifier);
    }

    @step
    public function some_identifier() {
        keyIdentifier = RandomGen.crypto.keyIdentifier();
    }

    @step
    public function some_key_data() {
        expectedKey = RandomGen.crypto.keyData();
    }

    @step
    public function this_key_exist(keyIdentifier: KeyIdentifier, expectedKey:KeyData)
    {
        given().this_identifier_maps_to(keyIdentifier, expectedKey);
    }

    @step
    public function this_key_does_not_exist(keyIdentifier: KeyIdentifier)
    {
        given().this_identifier_maps_to(keyIdentifier, null);
    }

    @step
    function this_identifier_maps_to(keyId: KeyIdentifier, key:KeyData){
        var self = this;
        Mockatoo
            .when(self.keystorage.getKey(keyId))
            .thenReturn(key);
    }

    // when -------------------------------------------------------------
    @step
    public function generating_a_new_key() {
        capturedKeyPromise = keychain.generateKey(keyGenOptions);

        if(capturedKeyPromise != null){
            capturedKeyPromise.then((key)->{capturedKey = key;});
        }
    }
    @step
    public function a_key_is_generated(?expectedKey:KeyData) {
        if(expectedKey == null){
            expectedKey = {
                keyType: keyGenOptions.keyType,
                fingerprint: {
                    alg: HashTypes.Sha256,
                    hash: Bytes.ofHex("612ec6cb707a47b2711adff1c80b5bc6c0f6a123563bbb6140c48578db2ab21d")
                },
                privateKey: PEM("-----BEGIN PRIVATE KEY-----...."),
                publicKey: PEM("-----BEGIN PUBLIC KEY-----....")
            }
        }
        this.expectedKey = expectedKey;
        mockKeyPair(expectedKey);

        keypairPromiseFake.resolve(keypairMock);
    }


    @step
    public function listing_keys() {
        listedKeys = keychain.listKeys();
    }

    @step
    public function getting_a_key_from_this_identifier() {
        function captureKey(key: Key) {
            capturedKey = key;
        };

        var keyPromise = keychain.getKey(keyIdentifier);

        assertThat(keyPromise, is(notNullValue()),
            "Keychain.getKey should return a valid promise");

        keyPromise.then(captureKey);
    }

    // then -------------------------------------------------------------
    @step
    public function the_key_generation_should_start() {
        var self = this;
        Mockatoo.verify(self.keygenerator.generateKey(self.keyGenOptions));
    }

    @step
    public function a_key_promise_should_be_returned() {
        assertThat(capturedKeyPromise, is(notNullValue()));
    }

    @step
    public function the_key_info_should_be_extracted() {
        var self = this;
        Mockatoo.verify(self.keypairMock.publicKey.fingerprint());
    }
    @step
    public function the_key_should_be_exported_and_stored() {
        Mockatoo.verify(keypairMock.publicKey.export(any));
        Mockatoo.verify(keypairMock.privateKey.export(any));

        Mockatoo.verify(keystorage.addKey(isNotNull));
        assertThat(storedKey, is(notNullValue()));

        keysShouldBeEquals(storedKey, expectedKey);
    }


    @step
    public function the_key_promise_should_be_resolved_to_a_valid_value() {
        assertThat(capturedKey, is(notNullValue()));
    }

    @step
    public function a_null_key_should_be_retrieved() {
        assertThat(capturedKey, is(nullValue()), "Returned key should be null");
    }

    @step
    public function the_keychain_should_return_the_stored_keyData() {
        var self = this;
        Mockatoo.verify(self.keystorage.listKeys());

        assertThat(listedKeys, is(notNullValue()), "listedKeys");
        assertThat(listedKeys, is(equalTo(mockedKeyList)), "listedKeys");
    }

    @step
    public function the_key_should_be_retrieved_from_storage() {
        var self = this;
        Mockatoo.verify(keystorage.getKey(self.keyIdentifier));
    }

    @step
    public function the_key_should_be_loaded_and_returned() {
        Mockatoo.verify(keygenerator.loadKey(isNotNull));
        assertThat(capturedKey, is(notNullValue()),
            "Loaded key should have been returned");
    }

    //----------------------------------------------------------------------

    function keysShouldBeEquals(storedKey: KeyData, expectedKey: KeyData) {
        assertThat(storedKey.keyType, is(equalTo(expectedKey.keyType)), "keyType");

        assertThat(storedKey.fingerprint.equals(expectedKey.fingerprint), is(true), "fingerprints are not equal.\n" +
        'Expected: ${Std.string(expectedKey.fingerprint)}\n' +
        'But found: ${Std.string(storedKey.fingerprint)}\n');

        assertThat(storedKey.name, is(equalTo(expectedKey.name)));
        exportKeysAreEqual(
            storedKey.privateKey, expectedKey.privateKey, 'privateKey');
        exportKeysAreEqual(
            storedKey.publicKey, expectedKey.publicKey, 'publicKey');
    }

    function exportKeysAreEqual(key: ExportedKey, expected:ExportedKey, keyName:String) {
        assertThat(key, is(equalTo(expected)),
            'exported $keyName should be: ${Std.string(expected)}\n' +
            'but was: ${Std.string(key)}'
        );
    }

    function makeKeyData():KeyData {
        return {
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex("3f4a0c615b2e60e1af49ff6c071b6add2809ffe08e758def973ad6357bc6ca6f")
            },
            keyType: EllipticCurve({namedCurve: "XYZ"}),
            name: "example",
            publicKey: KeyFormats.PEM("---BEGIN PUBLIC KEY---..."),
            privateKey: KeyFormats.PEM("---BEGIN PRIVATE KEY---...")
        };
    }
}