package discovery.test.tests.keys;

import discovery.utils.collections.ComparableMap.Comparable;
import discovery.utils.collections.ComparableMap.ComparableFunction;
import discovery.utils.collections.ComparableMap.ComparatorFunction;
import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.storage.ExportedFingerprint;
import discovery.keys.storage.PublicKeyData;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class PublicKeyDataTest implements Scenario{
    @steps
    var steps: PublicKeyDataSteps;

    @Test
    @scenario
    public function clone_key(){
        given().this_public_key_data(RandomGen.crypto.publicKeyData());
        when().cloning_it();
        then().a_different_instance_with_equals_values_should_be_produced();
    }
}

class PublicKeyDataSteps extends Steps<PublicKeyDataSteps>{
    var publicKeyData: PublicKeyData;
    var other: PublicKeyData;

    var comparison:Int;
	var reverseComparison:Int;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function this_public_key_data(publicKeyData:PublicKeyData) {
        this.publicKeyData = publicKeyData;
    }

    // when  ---------------------------------------------------

    @step
    public function cloning_it() {
        this.other = publicKeyData.clone();
    }

    // then  ---------------------------------------------------

    @step
    public
    function a_different_instance_with_equals_values_should_be_produced()
    {
        other.shouldNotBeTheSame(publicKeyData);
        other.shouldBeEqualsTo(publicKeyData);
    }
}