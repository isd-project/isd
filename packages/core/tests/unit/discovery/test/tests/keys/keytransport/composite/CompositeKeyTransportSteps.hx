package discovery.test.tests.keys.keytransport.composite;

import discovery.keys.keydiscovery.CompositeKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.test.common.mockups.keytransport.KeyTransportMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Steps;

using discovery.test.matchers.CommonMatchers;


class CompositeKeyTransportSteps<Self:CompositeKeyTransportSteps<Self>>
    extends Steps<Self>
{
    var compositeKeyTransport:KeyTransport;

    var keyTransportMockups:CompositeMockup<KeyTransportMockup>;


    public function new(){
        super();

        keyTransportMockups = new CompositeMockup(()->new KeyTransportMockup());
    }

    // given summary -------------------------------------------


    // given ---------------------------------------------------

    @step
    public function a_composite_key_transport_from_some_components() {
        given().some_key_transport_components();
        given().a_composite_key_transport();
    }

    @step
    function some_key_transport_components() {
        keyTransportMockups.populate();
    }

    @step
    function a_composite_key_transport() {
        var components = keyTransportMockups.map((m)->m.keyTransport());

        compositeKeyTransport = new CompositeKeyTransport(components);
    }

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------

}