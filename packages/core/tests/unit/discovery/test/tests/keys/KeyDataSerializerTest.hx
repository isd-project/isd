package discovery.test.tests.keys;

import discovery.test.steps.SerializerTestSteps;
import discovery.keys.storage.KeyDataSerializer;
import discovery.keys.storage.KeyData;
import discovery.domain.serialization.Serializer;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class KeyDataSerializerTest implements Scenario{
    @steps
    var steps: KeyDataSerializerSteps;

    @Test
    @scenario
    public function serialize_and_unserialize(){
        given().a_serializer_instance();
        when().serializing_this(RandomGen.crypto.keyData());
        and().deserializing_a_copy_back();
        then().a_new_equals_instance_should_be_produced();
    }
}

class KeyDataSerializerSteps
    extends SerializerTestSteps<KeyData, KeyDataSerializerSteps>
{

    function buildSerializer():Serializer<KeyData> {
        return new KeyDataSerializer();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------
}