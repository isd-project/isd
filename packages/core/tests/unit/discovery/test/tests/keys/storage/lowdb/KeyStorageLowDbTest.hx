package discovery.test.tests.keys.storage.lowdb;

import haxe.Exception;
import org.hamcrest.Matchers;
import discovery.keys.KeyIdentifier;
import discovery.keys.storage.exceptions.EntryNotFoundException;
import discovery.keys.storage.exceptions.StorageException;
import discovery.keys.storage.exceptions.DuplicateEntryException;
import discovery.keys.storage.exceptions.NonUniqueIdentifierException;
import discovery.keys.storage.KeyData;
import discovery.impl.keys.storage.lowdb.KeyStorageLowDb;
import discovery.keys.storage.KeyStorage;
import discovery.keys.crypto.KeyFormats.*;
import discovery.keys.crypto.HashTypes;

import discovery.test.helpers.RandomGen;

import haxe.io.Bytes;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class KeyStorageLowDbTest implements Scenario{
    @steps
    var steps:KeyStorageLowDbSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function list_keys_empty() {
        given().an_empty_database();
        when().listing_keys();
        then().an_empty_array_should_be_returned();
    }

    @Test
    @scenario
    public function add_key(){
        given().an_empty_database();
        when().adding_a_key();
        and().listing_keys();
        then().the_added_key_should_be_listed();
    }

    @Test
    @scenario
    public function get_key_by_fingerprint(){
        given().a_database_with_some_keys();
        given().a_fingerprint_of_one_key();
        when().getting_a_key_using_these_arguments();
        then().the_expected_key_should_be_retrieved();
    }

    @Test
    @scenario
    public function get_key_by_name(){
        given().a_database_with_some_keys();
        given().the_name_of_one_key();
        when().getting_a_key_using_these_arguments();
        then().the_expected_key_should_be_retrieved();
    }

    @Test
    @scenario
    public function get_key_by_fingerprint_prefix(){
        given().a_database_with_some_keys();
        given().a_unique_prefix_of_one_key();
        when().getting_a_key_using_these_arguments();
        then().the_expected_key_should_be_retrieved();
    }

    @Test
    @scenario
    public function update_key() {
        given().a_database_with_some_keys();
        given().one_of_the_keys();
        when().updating_the_name_of_that_key();
        and().retrieving_the_key_again();
        then().the_expected_key_should_have_been_updated();
    }

    @Test
    @scenario
    public function remove_key() {
       given().a_database_with_some_keys();
       given().a_unique_prefix_of_one_key();
       when().removing_a_key_with_these_arguments();
       and().retrieving_the_key_again();
       then().the_key_should_not_be_found_anymore();
    }


    // exception -----------------------------------------------------

    @Test
    @scenario
    public function cannot_get_a_key_using_a_non_unique_prefix(){
        given().a_database_with_some_keys();
        given().a_non_unique_fingeprint_prefix();
        when().getting_a_key_using_these_arguments();
        then().an_exception_should_have_been_thrown_of_type(NonUniqueIdentifierException);
    }

    @Test
    @scenario
    public function cannot_add_two_keys_with_same_fingeprint() {
        given().a_database_with_some_keys();
        given().a_fingerprint_of_one_key();
        when().adding_a_key_with_same_fingeprint();
        then().an_exception_should_have_been_thrown_of_type(DuplicateEntryException);
    }

    @Test
    @scenario
    public function cannot_add_two_keys_with_same_name() {
        given().a_database_with_some_keys();
        given().the_name_of_one_key();
        when().adding_a_key_with_same_name();
        then().an_exception_should_have_been_thrown_of_type(DuplicateEntryException);
    }

    @Test
    @scenario
    public function cannot_name_a_key_with_an_existing_name() {
        given().a_database_with_some_keys();
        given().one_of_the_keys();
        when().changing_the_key_name_to_the_name_of_other_key();
        then().an_exception_should_have_been_thrown_of_type(DuplicateEntryException);
    }

    @Test
    @scenario
    public function cannot_update_a_non_existent_key() {
        given().a_database_with_some_keys();
        given().a_new_key();
        when().updating_this_key();
        then().an_exception_should_have_been_thrown_of_type(EntryNotFoundException);
    }

    @Test
    @scenario
    public function cannot_remove_a_non_existent_key() {
        given().a_database_with_some_keys();
        given().a_fingerprint_not_found_on_database();
        when().removing_a_key_with_these_arguments();
        then().an_exception_should_have_been_thrown_of_type(EntryNotFoundException);
    }
}

class KeyStorageLowDbSteps extends Steps<KeyStorageLowDbSteps>{
    var keyStorage:KeyStorage;
    var keysData: Array<KeyData>;

    var addedKey:KeyData;
    var expectedKeys:Array<KeyData>;
    var expectedKey:KeyData;

    var keyIdentifier:KeyIdentifier;
    var prefix:String;
    var foundKey:KeyData;
    var otherKeyName: String;

    var capturedException:Exception;

    // given -------------------------------------------------------------


    @step
    public function an_empty_database() {
        keyStorage = new KeyStorageLowDb(new HaxeLow());
    }

    @step
    public function a_database_with_some_keys() {
        expectedKeys = [
            {
                fingerprint: { alg: HashTypes.Sha256, hash: Bytes.ofHex("cb0fb5336c")},
                keyType: EllipticCurve(),
                name: "key1",
                publicKey: DER(Bytes.ofHex('deadbeef'))
            },
            {
                // no name
                fingerprint: { alg: Md5, hash: Bytes.ofHex("678910dab7")},
                keyType: RSA(),
                publicKey: DER(Bytes.ofHex('c00fee'))
            },
            {
                fingerprint: { alg: Sha3_256, hash: Bytes.ofHex("000abcde")},
                keyType: EllipticCurve(),
                name: "key3",
                publicKey: DER(Bytes.ofHex('f00d'))
            }
        ];

        given().an_empty_database();
        when().adding_some_keys(expectedKeys);
    }

    @step
    public function one_of_the_keys() {
        return given().one_of_the_keys_that();
    }

    @step
    public function one_of_the_keys_that(?matcher: (KeyData)->Bool) {
        var baseIdx = Std.random(expectedKeys.length);

        for(i in 0...expectedKeys.length){
            var idx = (baseIdx + i) % expectedKeys.length;

            var key = expectedKeys[idx];
            if(matcher == null || matcher(key)){
                expectedKey = key;
            }
        }

        return expectedKey;
    }

    @step
    public function a_fingerprint_of_one_key() {
        given().one_of_the_keys();
        when().using_the_key_fingerprint_as_argument();
    }

    @step
    public function using_the_key_fingerprint_as_argument() {
        keyIdentifier = expectedKey.fingerprint.hash;
    }

    @step
    public function the_name_of_one_key() {
        given().one_of_the_keys_that((key)->{return key.name != null;});

        keyIdentifier = expectedKey.name;
    }

    @step
    public function a_unique_prefix_of_one_key() {
        given().one_of_the_keys();

        var fingHash = expectedKey.fingerprint.hash.decode().toHex();

        //assumes all keys here have at least 4 unique characteres prefixes
        var prefixLen = 4 + Std.random(3);
        prefix = fingHash.substr(0, prefixLen);

        keyIdentifier = prefix;
    }

    @step
    public function a_non_unique_fingeprint_prefix() {
        given().a_unique_prefix_of_one_key();

        var commonFingerprint = this.prefix + "ff1a89d50025";
        if(commonFingerprint.length % 2 != 0){ //odd length is not a valid hex number
            commonFingerprint += '0';
        }

        var similarKey:KeyData = {
            fingerprint: { alg: Sha1, hash: Bytes.ofHex(commonFingerprint)},
            keyType: EllipticCurve(),
            name: "similarKey",
            publicKey: DER(Bytes.ofHex('1eaf'))
        };

        keyStorage.addKey(similarKey);
    }

    @step
    public function other_key_name() {
        for (k in expectedKeys){
            if(k != expectedKey && k.name != null){
                otherKeyName = k.name;
                return;
            }
        }
    }

    @step
    public function a_new_key() {
        expectedKey = {
            fingerprint: { alg: Sha1, hash: Bytes.ofHex("ac43ab240b")},
            keyType: RSA(),
            name: "new key",
            publicKey: DER(Bytes.ofHex('cf8ba9c7'))
        };
    }

    @step
    public function a_fingerprint_not_found_on_database() {
        keyIdentifier = RandomGen.hex(Random.int(32,64));
    }

    // when -------------------------------------------------------------

    @step
    public function listing_keys() {
        keysData = keyStorage.listKeys();
    }

    @step
    public function adding_a_key() {
        addedKey = {
            fingerprint: {
                alg: HashTypes.Sha256,
                hash: Bytes.ofHex("8c5f52837b79806e1b30d656a09fb1a9f498680021c2554c22c061559e957166")
            },
            keyType: EllipticCurve(),
            name: "my key",
            publicKey: PEM(
'-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtsuFucnMcwUlJeUh4C9a
SjTtaESDTams3V4UaivnIDy1Y7kmpZb3ASdfb5PpI5QCqvKHgUOOOFD43dCdxnYF
tk4cysOn4LYAfYsWHsklM8Xg0aiLRPf3vkjdaKhhlaawl/XsiGn3RvLZWO8gbUaj
dTLEbFKI12JgKu1cRpLxElU5Mi6Pvu9ZeAGti6SIqOVNScnvxAK7ZMGzGV8cOU+Q
TiMEuE0rym4rOh7V/O/9iVJ3Z15221zSqKc4tSW3Gt/LizQ/AW0lYQ/lZIOP8HHc
fb7GmGXBmYU2HqoFdvsBH+Yw1aVe0lkfU1PzUPreUwVnkNTj/l/2eYaDRuj4eSBe
awIDAQAB
-----END PUBLIC KEY-----')
        };
        keyStorage.addKey(addedKey);
    }

    @step
    public function adding_a_key_with_same_fingeprint() {
        addedKey = modifyKeyName(expectedKey);

        when().adding_that_key();
    }

    @step
    public function adding_a_key_with_same_name() {
        addedKey = {
            fingerprint: {
                alg: Sha256,
                hash: Bytes.ofHex("dbdd4e24a8cc1407cf27a0bc096a9c3cdb0aee19d406648f6774b2fd59c5d9fc")
            },
            keyType: RSA(),
            name: expectedKey.name,
            publicKey: DER(Bytes.ofHex('f00d'))
        };

        when().adding_that_key();
    }


    @step
    public function adding_that_key() {
        captureException(()->{
            keyStorage.addKey(addedKey);
        });
    }

    @step
    public function adding_some_keys(keys: Array<KeyData>) {
        for(k in keys){
            keyStorage.addKey(k);
        }
    }

    @step
    public function getting_a_key_using_these_arguments() {
        captureException(()->{
            foundKey = keyStorage.getKey(keyIdentifier);
        });
    }

    @step
    public function updating_this_key(){
        assertThat(expectedKey, is(notNullValue()));

        captureException(()->{
            keyStorage.update(expectedKey);
        });
    }

    @step
    public function updating_the_key_name_to(?newName: String){
        expectedKey = modifyKeyName(expectedKey, newName);

        when().updating_this_key();
    }

    @step
    public function updating_the_name_of_that_key() {
        when().updating_the_key_name_to();
    }

    @step
    public function changing_the_key_name_to_the_name_of_other_key() {
        given().other_key_name();
        when().updating_the_key_name_to(otherKeyName);
    }

    @step
    public function removing_a_key_with_these_arguments() {
        captureException(()->{
            keyStorage.remove(keyIdentifier);
        });
    }

    @step
    public function retrieving_the_key_again() {
        when().using_the_key_fingerprint_as_argument();
        and().getting_a_key_using_these_arguments();
    }


    // then -------------------------------------------------------------

    @step
    public function an_empty_array_should_be_returned() {
        assertThat(keysData, is(notNullValue()));
        assertThat(keysData.length, is(0));
    }

    @step
    public function the_added_key_should_be_listed() {
        assertThat(keysData.length, is(greaterThan(0)),
            "storage should not be empty"
        );

        var filtered = keysData.filter(k->addedKey.fingerprint.equals(k.fingerprint));
        var found = keysData.pop();

        assertThat(found, is(notNullValue()), "Key not found!");
        keysShouldBeEqual(found, addedKey);
    }

    @step
    public function the_expected_key_should_be_retrieved() {
        assertThat(foundKey, is(notNullValue()), "No key found with given arguments");

        keysShouldBeEqual(foundKey, expectedKey);
    }

    @step
    public function the_expected_key_should_have_been_updated() {
        then().the_expected_key_should_be_retrieved();
    }

    @step
    public function the_key_should_not_be_found_anymore() {
        assertThat(foundKey, is(null), "key should not be found anymore");
    }


    @step
    public function an_exception_should_have_been_thrown_of_type(type: Dynamic) {
        assertThat(capturedException, is(notNullValue()), "no exception was thrown");
        assertThat(capturedException, isA(type),
            'Captured exception type does not match the expected: ${type}'
        );
    }

    // ---------------------------------------------------------------------

    function modifyKeyName(key: KeyData, ?newName:String) {
        var modified = key.clone();

        if(newName != null){
            modified.name = newName;
        }
        else{
            modified.name = 'new ${key.name}';
        }


        return modified;
    }

    function captureException(callback: ()->Void) {
        try{
            callback();
        }
        catch(e:StorageException){
            capturedException = e;
        }
    }

    function keysShouldBeEqual(found: KeyData, expected:KeyData) {
        assertThat(found, is(equalTo(expected)),
            'Key: ${Std.string(found)}\n' +
            'should be equal to: ${Std.string(expected)}');
    }
}