package discovery.test.tests.keys.keytransport;

import discovery.keys.crypto.HashTypes;
import discovery.test.helpers.RandomGen;
import discovery.keys.keydiscovery.validation.ValidationFailure;
import discovery.keys.keydiscovery.validation.DefaultKeyValidator;
import discovery.keys.keydiscovery.validation.KeyFingerprintValidator;
import discovery.keys.crypto.Fingerprint;
import haxe.Exception;
import discovery.keys.crypto.CryptoKey;
import discovery.test.common.mockups.crypto.CryptoKeyMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

class KeyFingerprintValidatorTest implements Scenario{
    @steps
    var steps: KeyFingerprintValidatorSteps;

    @Test
    @scenario
    public function validate_valid_key(){
        given().a_validator_instance();
        given().an_expected_fingerprint();
        given().a_key_with_the_same_fingerprint();
        when().validating_the_key();
        then().the_same_key_should_be_returned();
    }

    @Test
    @scenario
    public function validate_key_with_any_algorithm(){
        given().a_validator_instance();
        given().an_expected_fingerprint_with_no_specified_algorithm();
        given().a_key_with_a_fingerprint_with_same_hash_but_some_algorithm();
        when().validating_the_key();
        then().the_key_should_be_considered_valid();
    }

    @Test
    @scenario
    public function validate_key_with_different_fingerprint(){
        given().a_validator_instance();
        given().an_expected_fingerprint();
        given().a_key_with_the_a_different_fingerprint();
        when().validating_the_key();
        then().a_validation_failure_should_be_returned();
    }


    @Test
    @scenario
    public function validate_key_with_unexpected_algorithm(){
        given().a_validator_instance();
        given().an_expected_fingerprint();
        given().a_key_with_the_same_fingerprint_hash_but_another_algorithm();
        when().validating_the_key();
        then().a_validation_failure_should_be_returned();
    }
}

class KeyFingerprintValidatorSteps extends Steps<KeyFingerprintValidatorSteps>{
    var validator:KeyFingerprintValidator;

    var keyMockup:CryptoKeyMockup;

    var expectedFingerprint:Fingerprint;
    var returnedKey:CryptoKey;
    var toValidateKey:CryptoKey;
    var capturedError:Exception;

    public function new(){
        super();

        keyMockup = new CryptoKeyMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_validator_instance() {
        validator = new DefaultKeyValidator();
    }


    @step
    public function an_expected_fingerprint_with_no_specified_algorithm() {
        given().an_expected_fingerprint();

        expectedFingerprint = {
            alg: null,
            hash: expectedFingerprint.hash
        };
    }

    @step
    public function an_expected_fingerprint() {
        expectedFingerprint = RandomGen.crypto.fingerprint();
    }

    @step
    public function a_key_with_the_same_fingerprint() {
        given().a_key_with_this_fingerprint(expectedFingerprint);
    }

    @step
    public
    function a_key_with_a_fingerprint_with_same_hash_but_some_algorithm()
    {
        given().a_key_with_this_fingerprint({
            hash: expectedFingerprint.hash,
            alg: RandomGen.crypto.hashType()
        });
    }

    @step
    public
    function a_key_with_the_same_fingerprint_hash_but_another_algorithm() {
        given().a_key_with_this_fingerprint({
            hash: expectedFingerprint.hash,
            alg: given().another_algorithm(expectedFingerprint.alg)
        });
    }

    @step
    function a_key_with_this_fingerprint(fingerprint:Fingerprint) {
        keyMockup.setFingerprintTo(fingerprint);
        toValidateKey = keyMockup.key();
    }

    function another_algorithm(initialHashType:HashTypes):HashTypes {
        var values = RandomGen.primitives.shuffle(HashTypes.values.copy());

        for(alg in values){
            if(alg != initialHashType){
                return alg;
            }
        }

        throw new Exception('No valid algorithm could be found');
    }


    @step
    public function a_key_with_the_a_different_fingerprint() {
        toValidateKey = keyMockup.key();
    }


    // when  ---------------------------------------------------

    @step
    public function validating_the_key() {
        validator.validateKey(toValidateKey, expectedFingerprint)
            .then((result)->{
                returnedKey = result;
            }, (err)->{
                capturedError = err;

                throw err;
            });
    }

    // then  ---------------------------------------------------

    @step
    public function the_key_should_be_considered_valid() {
        then().the_same_key_should_be_returned();
    }

    @step
    public function the_same_key_should_be_returned() {
        assertThat(returnedKey, is(theInstance(toValidateKey)));
    }

    @step
    public function a_validation_failure_should_be_returned() {
        assertThat(capturedError, isA(ValidationFailure));
    }
}