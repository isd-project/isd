package discovery.test.tests.keys.keytransport.composite;

import discovery.test.common.mockups.KeychainMockup;
import discovery.exceptions.composite.PartialFailureException;
import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.keys.storage.PublicKeyData;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.keytransport.KeyTransportMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeKeyPublicationTest implements Scenario{
    @steps
    var steps: CompositeKeyPublicationSteps;

    @Test
    @scenario
    public function start_publishing_key(){
        given().a_composite_key_transport_from_some_components();
        when().publishing_some_key();
        then().the_publish_key_should_be_delegated_to_all_components();
    }

    @Test
    @scenario
    public function notify_publish_key_success_when_all_components_succeed(){
        given().a_publication_listener();
        given().a_publication_has_started();
        when().all_components_publish_with_success();
        then().the_publication_listener_should_be_notified_with_no_errors();
    }

    @Test
    @scenario
    public function notify_publish_key_failure_when_all_components_fail(){
        given().a_publication_listener();
        given().a_publication_has_started();
        when().all_components_fail_to_publish_key();
        then().the_publication_listener_should_be_notified_with_a_failure();
    }


    @Test
    @scenario
    public function
        notify_publish_key_partial_failure_when_some_components_fail()
    {
        given().a_publication_listener();
        given().a_publication_has_started();
        when().some_components_fail_to_publish_key();
        then().the_publication_listener_should_be_notified_with_a_partial_failure();
    }

    // publish keychain ---------------------------------------

    @Test
    @scenario
    public function publish_keychain_in_all_mechanisms(){
        given().a_composite_key_transport_from_some_components();
        when().publishing_some_keychain();
        then().the_keychain_publication_should_be_delegated_to_all_components();
    }

    @Test
    @scenario
    public function notify_publish_keychain_success(){
        given().a_publication_listener();
        given().a_keychain_publication_has_started();
        when().all_components_publish_the_keychain_with_success();
        then().the_publication_listener_should_be_notified_with_no_errors();
    }
}

class CompositeKeyPublicationSteps
    extends CompositeKeyTransportSteps<CompositeKeyPublicationSteps>
{
    var publicationListenerMockup:ListenerMock<Exception>;

    var publishedKey:PublicKeyData;
    var publicationListener:DoneCallback;

    var keychain:KeychainMockup;

    public function new(){
        super();

        keyTransportMockups = new CompositeMockup(()->new KeyTransportMockup());
        publicationListenerMockup = new ListenerMock();

        keychain = new KeychainMockup();
    }

    // given summary -------------------------------------------

    @step
    public function a_publication_has_started() {
        given().a_composite_key_transport_from_some_components();
        when().publishing_some_key();
    }

    @step
    public function a_keychain_publication_has_started() {
        given().a_composite_key_transport_from_some_components();
        when().publishing_some_keychain();
    }

    // given ---------------------------------------------------

    @step
    public function a_publication_listener() {
        publicationListener = cast publicationListenerMockup.listener();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_some_key() {
        publishedKey = RandomGen.crypto.publicKeyData();

        compositeKeyTransport.publishKey(publishedKey, publicationListener);
    }

    @step
    public function publishing_some_keychain() {
        compositeKeyTransport.publishKeychain(keychain.keychain(), publicationListener);
    }

    @step
    public function all_components_publish_the_keychain_with_success() {
        keyTransportMockups.forEach((tMockup)->tMockup.notifyKeychainPublication());
    }

    @step
    public function all_components_publish_with_success() {
        keyTransportMockups.forEach((tMockup)->tMockup.notifyPublication());
    }

    @step
    public function all_components_fail_to_publish_key() {
        notify_failures(keyTransportMockups.numComponents());
    }

    @step
    public function some_components_fail_to_publish_key() {
        var max = keyTransportMockups.numComponents() - 1;

        var numFailures = RandomGen.primitives.int(1, max);

        notify_failures(numFailures);
    }

    function notify_failures(numFails: Int) {
        var mockups = keyTransportMockups.map((m)->m);
        mockups = RandomGen.primitives.shuffle(mockups);

        for(i in 0...mockups.length){
            var error:Exception = null;

            if(i < numFails){
                error = new Exception('failure ${i+1}');
            }

            mockups[i].notifyPublicationWith(error);
        }
    }

    // then  ---------------------------------------------------

    @step
    public function the_publish_key_should_be_delegated_to_all_components() {
        keyTransportMockups.forEach((m)->{
            m.verifyPublishedKey(publishedKey);
        });
    }

    @step
    public
    function the_keychain_publication_should_be_delegated_to_all_components() {
        keyTransportMockups.forEach((m)->{
            m.verifyPublishedKeychain(keychain.keychain());
        });
    }

    @step
    public function the_publication_listener_should_be_notified_with_no_errors() {
        publicationListenerMockup.assertWasCalledWith(null);
    }

    @step
    public function the_publication_listener_should_be_notified_with_a_partial_failure() {
        then().the_publication_listener_should_be_notified_with_a_failure();

        var failure = publicationListenerMockup.lastCall();

        failure.shouldBeAn(PartialFailureException);
    }

    @step
    public
    function the_publication_listener_should_be_notified_with_a_failure() {
        publicationListenerMockup.assertWasCalled();

        publicationListenerMockup.lastCall().shouldNotBeNull();
    }
}