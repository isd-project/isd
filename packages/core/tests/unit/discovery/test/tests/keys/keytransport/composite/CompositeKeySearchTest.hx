package discovery.test.tests.keys.keytransport.composite;

import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners.SearchKeyResult;
import discovery.keys.keydiscovery.SearchKeyListeners.KeyFoundListener;
import discovery.keys.crypto.Fingerprint;
import discovery.exceptions.composite.PartialFailureException;
import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.keys.storage.PublicKeyData;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.keytransport.KeyTransportMockup;
import discovery.test.common.mockups.CompositeMockup;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeKeySearchTest implements Scenario{
    @steps
    var steps: CompositeKeySearchSteps;

    @Test
    @scenario
    public function start_searching_key(){
        given().a_composite_key_transport_from_some_components();
        when().searching_some_key_by_fingerprint();
        then().the_key_search_should_be_delegated_to_all_components();
    }

    @Test
    @scenario
    public function notify_found_unique_keys(){
        given().a_key_found_listener();
        given().a_key_search_has_started();
        when().some_unique_keys_are_found_by_the_transport_components();
        then().the_listener_should_be_notified_once_for_each_unique_key();
    }

    @Test
    @scenario
    public function notify_found_duplicate_keys(){
        given().a_key_found_listener();
        given().a_key_search_has_started();
        when().some_unique_keys_are_found_by_the_transport_components();
        and().some_duplicated_keys_are_also_found();
        then().the_listener_should_be_notified_once_for_each_unique_key();
    }

    @Test
    @scenario
    public function notify_search_end(){
        given().a_key_end_listener();
        given().a_key_search_has_started();
        when().all_components_search_finishes();
        then().the_end_listener_should_be_notified_with_no_errors();
    }

    @Test
    @scenario
    public function notify_search_end_failure(){
        given().a_key_end_listener();
        given().a_key_search_has_started();
        when().the_search_ends_with_failure_in_some_components();
        then().the_end_listener_should_be_notified_with_a_partial_failure();
    }

    @Test
    @scenario
    public function return_valid_stop_control(){
        given().a_composite_key_transport_from_some_components();
        when().searching_some_key_by_fingerprint();
        then().a_valid_stoppable_should_be_returned();
    }

    @Test
    @scenario
    public function delegate_search_stop(){
        given().a_key_search_has_started();
        when().stopping_the_composite_search();
        then().the_search_on_components_should_be_stopped();
    }
}

class CompositeKeySearchSteps
    extends CompositeKeyTransportSteps<CompositeKeySearchSteps>
{
    var keyFoundListenerMockup:ListenerMock<SearchKeyResult>;
    var endListenerMockup:ListenerMock<Exception>;
    var endListener:DoneCallback;

    var searchedFingerprint:Fingerprint;
    var keyFoundListener:KeyFoundListener;

    var stoppable:Stoppable;

    var uniqueKeys:Array<PublicKeyData> = [];
    var expectedResults:Array<SearchKeyResult> = [];

    public function new(){
        super();

        keyTransportMockups = new CompositeMockup(()->new KeyTransportMockup());
        keyFoundListenerMockup = new ListenerMock();

        endListenerMockup = new ListenerMock();
    }

    // given summary -------------------------------------------

    @step
    public function a_key_search_has_started() {
        given().a_composite_key_transport_from_some_components();
        when().searching_some_key_by_fingerprint();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_found_listener() {
        keyFoundListener = cast keyFoundListenerMockup.listener();
    }

    @step
    public function a_key_end_listener() {
        endListener = cast endListenerMockup.listener();
    }

    // when  ---------------------------------------------------

    @step
    public function searching_some_key_by_fingerprint() {
        searchedFingerprint = RandomGen.crypto.fingerprint();

        stoppable = compositeKeyTransport.searchKey(searchedFingerprint, {
            onKey: keyFoundListener,
            onEnd: endListener
        });
    }

    @step
    public function some_unique_keys_are_found_by_the_transport_components() {
        expectedResults = keyTransportMockups.map((tMockup)->{
            var result = makeKeyResult();
            uniqueKeys.push(result.found);

            tMockup.notifySearchResultFor(searchedFingerprint, result);

            return result;
        });
    }

    @step
    public function some_duplicated_keys_are_also_found() {
        keyTransportMockups.forEach((tMockup)->{
            var key = RandomGen.primitives.pickOne(uniqueKeys);
            var duplicatedResult = makeKeyResult(key);
            var fing = searchedFingerprint;

            tMockup.notifySearchResultFor(fing, duplicatedResult);
        });
    }

    @step
    public function all_components_search_finishes() {
        var fing = searchedFingerprint;

        keyTransportMockups.forEach((m)->m.notifySearchEnd(fing));
    }

    @step
    public function the_search_ends_with_failure_in_some_components() {
        var fing = searchedFingerprint;

        keyTransportMockups.forSomeComponents(
            (m)->m.notifySearchEnd(fing, new Exception('failure')),
            (m)->m.notifySearchEnd(fing));
    }

    @step
    public function stopping_the_composite_search() {
        stoppable.stop();
    }

    // then  ---------------------------------------------------

    @step
    public function the_key_search_should_be_delegated_to_all_components() {
        keyTransportMockups.forEach((m)->{
            m.verifySearchKeyCalled(searchedFingerprint);
        });
    }


    @step
    public function the_listener_should_be_notified_once_for_each_unique_key() {
        for(result in expectedResults){
            keyFoundListenerMockup.assertWasCalledTheseTimesWith(1, result);
        }
    }

    @step
    public function the_end_listener_should_be_notified_with_no_errors() {
        endListenerMockup.assertWasCalledWith(null);
    }

    @step
    public function the_end_listener_should_be_notified_with_a_partial_failure(){
        var failure = endListenerMockup.lastCall();

        failure.shouldBeAn(PartialFailureException);
    }


    @step
    public function a_valid_stoppable_should_be_returned() {
        stoppable.shouldNotBeNull();
    }

    @step
    public function the_search_on_components_should_be_stopped() {
        keyTransportMockups.forEach((m)->{
            m.verifySearchStopped(searchedFingerprint);
        });
    }

    // ---------------------------------------------------------------------

    public function makeKeyResult(?key:PublicKeyData): SearchKeyResult {
        if(key == null){
            key = RandomGen.crypto.publicKeyData();
        }

        var result:SearchKeyResult = {
            found: key
        };

        return result;
    }
}