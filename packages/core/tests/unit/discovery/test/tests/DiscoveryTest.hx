package discovery.test.tests;

import discovery.test.common.mockups.DiscoveryFactoryMockup;
import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.domain.PublishInfo;
import discovery.keys.KeyIdentifier;
import haxe.io.Bytes;
import discovery.domain.Identifier;
import discovery.test.common.CallbackMock;
import discovery.domain.Publication;
import discovery.domain.Description;
import discovery.domain.Search;
import discovery.domain.Query;
import hxgiven.Scenario;
import hxgiven.Steps;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;

class DiscoveryTest implements Scenario{
    @steps
    var steps:DiscoverySteps;

    public function new() {}

    @scenario
    @Test
    public function delegate_search_to_mechanisms() {
        given().a_Discovery_instance_created_with_some_discovery_mechanism();
        when().starting_a_query();
        then().the_search_should_be_delegated_to_the_underlying_mechanism();
    }

    @scenario
    @Test
    public function delegate_locate_to_mechanisms() {
        given().a_Discovery_instance_created_with_some_discovery_mechanism();
        when().locating_some_identifier();
        then().the_location_should_be_delegated_to_the_underlying_mechanism();
    }

    @scenario
    @Test
    public function publish_service() {
        given().a_valid_Discovery_instance();
        given().an_identifier_to_a_key_on_keychain();
        when().publishing_a_service_using_that_identifier();
        then().the_discovery_should_get_that_key_from_keychain();
            and().should_update_the_provider_id_with_the_key_fingerprint();
            and().pass_the_publication_with_the_provider_key_to_the_discovery_mechanism();
    }

    @scenario
    @Test
    public function finishing_discovery(){
        given().a_Discovery_instance_created_with_some_discovery_mechanism();
        when().finishing_the_discovery();
        then().the_underling_mechanism_should_be_finished();
    }
}

class DiscoverySteps extends Steps<DiscoverySteps> {
    var discovery:Discovery;
    var mechanism:DiscoveryMechanism;
    var discoveryFactory:DiscoveryFactoryMockup;

    var keyIdentifier:KeyIdentifier;

    var query:Query;
    var identifier:Identifier;
    var startedSearch: Search;

    var description:Description;
    var mockPublication:Publication;
    var publication:Publication;

    var finishCallback:CallbackMock;

    public function new() {
        super();

        discoveryFactory = new DiscoveryFactoryMockup();

        finishCallback = mock(CallbackMock);
    }

    function keychainMockup(){
        return discoveryFactory.keychainMockup();
    }

    function mechanismMockup(){
        return discoveryFactory.mechanismMockup();
    }

    function keyFingerprint(): Fingerprint {
        return keychainMockup().getKeyMockup().keyFingerprint;
    }

    function publishedInfo(): PublishInfo{
        return mechanismMockup().getPublishedInfo();
    }

    /* given ----------------------------------------------------------- **/


    @step
    public function a_valid_Discovery_instance() {
        given().some_discovery_mechanism();
        when().creating_a_Discovery_instance();
    }

    @step
    public function a_Discovery_instance_created_with_some_discovery_mechanism()
    {
        given().a_valid_Discovery_instance();
    }

    @step
    public function some_discovery_mechanism() {
        mechanism = mechanismMockup().mechanism;
    }

    @step
    public function some_query() {
        query = mock(Query);
    }

    @step
    public function some_description() {
        description = RandomGen.description();
    }

    @step
    public function some_identifier() {
        identifier = {
            name: "example",
            type: "mytype",
            providerId: Bytes.ofHex("d9b4b6f7cc9582c6a328c833514aad0ea8377442942ee5b3d9e15b5478d74fd3"),
            instanceId: Bytes.ofHex("7a41d4")
        };
    }

    @step
    public function an_identifier_to_a_key_on_keychain() {
        keyIdentifier = 'deadbeef';

        keychainMockup().resolveGetKeyToDefault(keyIdentifier);
    }

    /* when ----------------------------------------------------------- **/

    @step
    public function creating_a_Discovery_instance() {
        discovery = discoveryFactory.buildWith({
            mechanism: mechanism
        });
    }

    @step
    public function starting_a_query() {
        given().some_query();

        startedSearch = this.discovery.search(this.query);
    }

    @step
    public function locating_some_identifier() {
        given().some_identifier();

        startedSearch = this.discovery.locate(identifier);
    }

    @step
    public function publishing_a_service_using_that_identifier() {
        given().some_description();
        when().publishing_it();
        when().the_ip_discovery_finishes();
    }

    @step
    public function publishing_it() {
        description.providerId = keyIdentifier;

        discovery.publish(description);
    }

    @step
    public function the_ip_discovery_finishes(){
        var someAddresses = RandomGen.addresses(RandomGen.int(1, 4));
        discoveryFactory.ipDiscoverMockup().resolvePublicIpsTo(someAddresses);
    }

    @step
    public function finishing_the_discovery(){
        discovery.finish(finishCallback.onCallback);
    }


    /* then ----------------------------------------------------------- **/

    @step
    public function the_search_should_be_delegated_to_the_underlying_mechanism()
    {
        var q = this.query;
        Mockatoo.verify(mechanism.search(query));
    }

    @step
    public function the_location_should_be_delegated_to_the_underlying_mechanism()
    {
        var id = identifier;
        Mockatoo.verify(mechanism.locate(id));
    }

    @step
    public function the_discovery_should_get_that_key_from_keychain() {
        keychainMockup().verifyGetKey(this.keyIdentifier);
    }

    @step
    public function should_update_the_provider_id_with_the_key_fingerprint() {
        assertThat(publishedInfo(), is(notNullValue()),
            "Should have published: missing published info");

        var description = publishedInfo().description();

        assertThat(description.providerId.fingerprint(),
            is(equalTo(keyFingerprint())),
            'Provider id ${description.providerId}\n' +
            'does not match the expected: ${keyFingerprint()}');
    }

    @step
    public function pass_the_publication_with_the_provider_key_to_the_discovery_mechanism() {
        then().the_publication_should_be_delegated_to_the_underlying_mechanism();
        then().the_published_info_should_match_the_expected();
    }

    @step
    public
    function the_publication_should_be_delegated_to_the_underlying_mechanism(){
        Mockatoo.verify(this.mechanism.publish(isNotNull, isNotNull));
    }

    @step
    public function the_underling_mechanism_should_be_finished(){
        Mockatoo.verify(mechanism.finish(isNotNull));
    }

    @step
    public function the_published_info_should_match_the_expected() {
        assertThat(publishedInfo(), is(notNullValue()));

        var publishedDesc = publishedInfo().description();
        var providerKey = publishedInfo().providerKey;
        var expectedKey  = keychainMockup().getKeyMockup().key;

        assertThat(publishedDesc, is(equalTo(description)),
            "Description does not match the expected.\n" +
            'found: ${publishedDesc}\n'+
            'expected: ${description}');
        assertThat(providerKey, is(equalTo(expectedKey)),
            "Provider key does not match the expected");
    }
}