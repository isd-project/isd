package discovery.test.tests.utils.functional;

import discovery.exceptions.composite.PartialFailureException;
import discovery.test.helpers.RandomGen;
import haxe.Exception;
import discovery.utils.functional.FailuresComposer;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class FailuresComposerTest implements Scenario{
    @steps
    var steps: FailuresComposerSteps;

    @Test
    @scenario
    public function composing_from_no_exception(){
        when().composing_these_exceptions([]);
        then().should_return(null);
    }


    @Test
    @scenario
    public function composing_from_null_exceptions(){
        when().composing_these_exceptions([null, null, null]);
        then().should_return(null);
    }


    @Test
    @scenario
    public function composing_from_one_from_a_total_of_one(){
        var err = new Exception('Exception');

        when().composing_these_exceptions([err]);
        then().should_return(err);
    }

    @Test
    @scenario
    public function composing_a_partial_failure(){
        var err = new Exception('Exception');
        var errors = Random.shuffle([err, null, null]);

        when().composing_these_exceptions(errors);
        then().should_return_a_partial_failure_from(errors);
    }

    @Test
    @scenario
    public function composing_from_multiple_exceptions(){
        var errors = [
            new Exception('Exception 1'),
            new Exception('Exception 2'),
            new Exception('Exception 3')
        ];

        when().composing_these_exceptions(errors);
        then().should_return_a_composite_exception_from(errors);
    }

    @Test
    @scenario
    public function composing_from_multiple_non_null_exceptions(){
        var expected = [
            new Exception('Exception 1'),
            new Exception('Exception 2'),
            new Exception('Exception 3')
        ];
        var errors = expected.copy();
        for(i in 0...3){
            var idx = RandomGen.int(0, errors.length);
            errors.insert(idx, null);
        }

        when().composing_these_exceptions(errors);
        then().should_return_a_partial_failure_from(errors);
    }
}

class FailuresComposerSteps extends Steps<FailuresComposerSteps>{
    var result:Null<Exception>;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function composing_these_exceptions(errs:Array<Null<Exception>>) {
        result = FailuresComposer.compose(errs);
    }

    // then  ---------------------------------------------------

    @step
    public function should_return(expected: Null<Exception>) {
        result.shouldBeEqualsTo(expected);
    }

    @step
    public function
        should_return_a_composite_exception_from(errors:Array<Exception>)
    {
        then().result_should_be(new CompositeException(errors));
    }

    @step
    public function
        should_return_a_partial_failure_from(errors:Array<Exception>)
    {
        then().result_should_be(new PartialFailureException(errors));
    }

    function result_should_be(expected:Exception) {
        CommonMatchers.shouldBeEqualsTo(result, expected);
    }
}