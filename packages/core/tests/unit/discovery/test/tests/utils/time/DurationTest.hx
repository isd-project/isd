package discovery.test.tests.utils.time;

import discovery.utils.time.Duration;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DurationTest implements Scenario{
    @steps
    var steps: DurationSteps;

    @Test
    @scenario
    public function empty_duration(){
        given().this_duration(Duration.empty());
        then().it_should_correspond_to_this_milliseconds(0);
    }

    @Test
    @scenario
    public function creating_duration_from_time_unit(){
        var parameters:Array<Array<Dynamic>> = [
            [Duration.of(100, Milliseconds), 100],
            [Duration.of(3, Seconds), 3000],
            [Duration.of(2, Minutes), 2 * 60 * 1000],
            [Duration.of(4, Hours),   4 * 60 * 60 * 1000],
            [Duration.of(5, Days),    5 * 24 * 60 * 60 * 1000]
        ];

        for(params in parameters){
            var duration = params[0];
            var millis = params[1];

            given().this_duration(duration);
            then().it_should_correspond_to_this_milliseconds(millis);
        }
    }

    @Test
    @scenario
    public function months_receive_approximate_values(){
        given().this_duration(Duration.of(1, Months));
        then().it_should_be_between_these_values(
                            Duration.of(30, Days), Duration.of(31, Days));
    }

    @Test
    @scenario
    public function years_receive_approximate_values(){
        given().this_duration(Duration.of(1, Years));
        then().it_should_be_between_these_values(
                            Duration.of(365, Days), Duration.of(366, Days));
    }


    @Test
    @scenario
    public function sum_duration_values(){
        given().this_duration(Duration.of(2, Days).plus(3, Days));
        then().it_should_be_equal_to(Duration.of(5, Days));
    }

    @Test
    @scenario
    public function sum_operator(){
        given().this_duration(Duration.of(1, Hours) + Duration.of(30, Minutes));
        then().it_should_be_equal_to(Duration.of(90, Minutes));
    }

    @Test
    @scenario
    public function subtract_duration_values(){
        given().this_duration(Duration.of(1, Minutes).minus(30, Seconds));
        then().it_should_be_equal_to(Duration.of(30, Seconds));
    }

    @Test
    @scenario
    public function sub_operator(){
        given().this_duration(Duration.of(1, Days) - Duration.of(1, Hours));
        then().it_should_be_equal_to(Duration.of(23, Hours));
    }


    @Test
    @scenario
    public function multiply_operator(){
        given().this_duration(Duration.of(1, Days) * 3);
        then().it_should_be_equal_to(Duration.of(3, Days));
    }


    @Test
    @scenario
    public function sum_with_date(){
        given().this_duration(Duration.of(1, Days));
        given().this_date(Date.fromString('2222-01-01'));
        when().summing_duration_with_date();
        then().this_date_should_be_returned(Date.fromString('2222-01-02'));
    }
}

class DurationSteps extends Steps<DurationSteps>{
    var duration:Duration;

    var date:Date;
    var resultantDate: Date;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function this_duration(duration: Duration) {
        this.duration = duration;
    }

    @step
    public function this_date(date:Date) {
        this.date = date;
    }


    // when  ---------------------------------------------------

    @step
    public function summing_duration_with_date() {
        resultantDate = this.duration  + this.date;
    }


    // then  ---------------------------------------------------

    @step
    public function it_should_be_equal_to(duration:Duration) {
        then().it_should_correspond_to_this_milliseconds(duration.time());
    }

    @step
    public function it_should_correspond_to_this_milliseconds(millis:MillisecondsTime)
    {
        assertThat(duration, is(equalTo(millis)),
            "Duration does not correspond to the expected milliseconds value");
    }

    @step
    public function it_should_be_between_these_values(
        min: MillisecondsTime, max:MillisecondsTime)
    {
        assertThat(duration, is(both(greaterThanOrEqualTo(min))
                                .and(lessThanOrEqualTo(max))),
            'Duration ($duration) should be between ${min}ms and ${max}ms');
    }

    @step
    public function this_date_should_be_returned(expected:Date) {
        assertThat(resultantDate.getTime(), is(equalTo(expected.getTime())),
            'Date: ${resultantDate}\n'+
            'should be: ${expected}'
        );
    }
}