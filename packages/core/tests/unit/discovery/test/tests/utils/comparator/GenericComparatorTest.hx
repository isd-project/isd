package discovery.test.tests.utils.comparator;

import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.KeyTypes;
import haxe.io.Bytes;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.comparator.GenericComparator;
using discovery.test.matchers.CommonMatchers;


class GenericComparatorTest implements Scenario{
    @steps
    var steps: GenericComparatorSteps;

    @Test
    @scenario
    public function compare_integers(){
        compare_values(1, 1, 0);
        compare_values(1, 0, 1);
        compare_values(0, 1, -1);
    }

    @Test
    @scenario
    public function compare_float(){
        compare_values(1.0, 1.0, 0);
        compare_values(1.1, 1.2, -1);
        compare_values(0.0, -0.1, 1);
    }

    @Test
    @scenario
    public function compare_boolean(){
        compare_values(true, true, 0);
        compare_values(false, false, 0);
        compare_unequals(true, false);
    }


    @Test
    @scenario
    public function compare_function(){
        function f1(){}
        function f2(){}

        //can only compare if are equals
        compare_values(f1, f1, 0);

        f1.compare(f2).shouldNotBe(0, makeMsg(f1, f2));
    }

    @Test
    @scenario
    public function compare_strings(){
        var a = 'a';
        var b = 'b';
        var ab = 'ab';

        compare_values(a, a, 0);
        compare_values(a, b, -1);
        compare_values(b, a, 1);
        compare_values(a, ab, -1);
    }

    @Test
    @scenario
    public function compare_bytes(){
        var b1 = Bytes.ofHex('abcdef');
        var b1copy = Bytes.ofHex('abcdef');
        var b2 = Bytes.ofHex('123456');

        compare_equals(b1, b1copy);
        compare_unequals(b1, b2);
    }

    @Test
    @scenario
    public function compare_dates(){
        var d1 = RandomGen.time.anyDate();
        var d1Clone = Date.fromTime(d1.getTime());
        var d2 = RandomGen.time.anyDate();

        compare_equals(d1, d1Clone);
        compare_unequals(d1, d2);
    }


    @Test
    @scenario
    public function compare_nulls(){
        compare_equals(null, null);
        compare_unequals(null, 'a');
        compare_unequals(null, 1);
    }

    @Test
    @scenario
    public function compare_arrays(){
        compare_equals([], []);
        compare_equals([1,2,3], [1,2,3]);
        compare_equals(['a', 'b'], ['a', 'b']);

        compare_unequals([], [1]);
        compare_unequals([1], [2]);
        compare_unequals(['a', 'b'], ['a', 'c']);
    }

    @Test
    @scenario
    public function compare_maps(){
        compare_equals([1=>2], [1=>2]);
        compare_equals(['a'=>true], ['a'=>true]);
        compare_equals(new Map<Int, Int>(), new Map<Int, Int>());

        compare_unequals([1=>2], []);
        compare_unequals([1=>2], [1=>3]);
        compare_unequals(['a'=>1], ['b'=>1]);
    }

    @Test
    @scenario
    public function compare_objects(){
        compare_equals({}, {});
        compare_equals({x:1}, {x:1});
        compare_equals({x:{y:'a'}}, {x:{y:'a'}});

        compare_unequals({}, {x:1});
        compare_unequals({x:1}, {x:2});

        compare_unequals(({x:'a'}: Any), ({y:'a'}: Any));
    }

    @Test
    @scenario
    public function custom_class(){
        var desc1 = RandomGen.description();
        var desc1Clone = desc1.clone();

        var desc2 = RandomGen.description();

        compare_equals(desc1, desc1Clone);
        compare_unequals(desc1, desc2);
    }

    @Test
    @scenario
    public function compare_enums(){
        var elliptCrvX1 = KeyTypes.EllipticCurve({namedCurve: 'X'});
        var elliptCrvX2 = KeyTypes.EllipticCurve({namedCurve: 'X'});
        var elliptCrvY = KeyTypes.EllipticCurve({namedCurve: 'Y'});

        compare_equals(KeyTypes.EllipticCurve(), KeyTypes.EllipticCurve());
        compare_equals(elliptCrvX1, elliptCrvX2);

        compare_unequals(elliptCrvX1, KeyTypes.EllipticCurve());
        compare_unequals(elliptCrvX1, elliptCrvY);
        compare_unequals(
            KeyTypes.EllipticCurve(),
            KeyTypes.RSA());
    }

    // ------------------------------------------------------

    function compare_equals<T>(v1:T, v2:T) {
        compare_values(v1, v2, 0);
        compare_values(v1, v1, 0);
    }

    function compare_values<T>(v1:T, v2:T, expected:Int) {
        v1.compare(v2).shouldBe(expected, 'With v1: ${v1}, v2: ${v2}');
    }

    function compare_unequals<T>(v1:T, v2:T) {
        var cmp1 = v1.compare(v2);
        var cmp2 = v2.compare(v1);

        cmp1.shouldNotBe(0, makeMsg(v1, v2));
        cmp2.shouldNotBe(0, makeMsg(v2, v1));
        cmp1.shouldBe(-cmp2, makeMsg(v1, v2));
    }

    function makeMsg<T>(v1:T, v2:T):Null<String> {
        return 'With v1: ${v1}, v2: ${v2}';
    }
}

class GenericComparatorSteps extends Steps<GenericComparatorSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}