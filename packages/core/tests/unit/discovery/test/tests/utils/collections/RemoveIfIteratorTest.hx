package discovery.test.tests.utils.collections;

import discovery.utils.collections.RemoveIfIterator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.IteratorTools;


class RemoveIfIteratorTest implements Scenario{
    @steps
    var steps: RemoveIfIteratorSteps;

    @Test
    @scenario
    public function iterate_simple(){
        var collection = [1=>1, 2=>2, 3=>3];

        var iterator = new RemoveIfIterator(collection.keys(), collection, (v)->false);

        var expected = collection.keys().toArray();
        var filteredArray = iterator.toArray();

        assertThat(filteredArray, is(array(expected)));
    }


    @Test
    @scenario
    public function filter_values(){
        var collection = [1=>1, 2=>2, 3=>3];

        var iterator = new RemoveIfIterator(collection.keys(), collection,
        (v)->{return v == 2;});

        var expected = [1, 3];
        var filteredArray = iterator.toArray();

        assertThat(filteredArray, is(array(expected)));
    }


    @Test
    @scenario
    public function should_remove_filtered_values(){
        var collection = [1=>1, 2=>2, 3=>3];

        var iterator = new RemoveIfIterator(collection.keys(), collection,
        (v)->{return v == 2;});

        //iterate over all values, removing the matched ones
        iterator.toArray();

        var expected = [1, 3];
        var currentKeys = collection.keys().toArray();

        assertThat(currentKeys, is(array(expected)));
    }
}

class RemoveIfIteratorSteps extends Steps<RemoveIfIteratorSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}