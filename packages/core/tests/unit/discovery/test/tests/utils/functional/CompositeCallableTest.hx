package discovery.test.tests.utils.functional;

import discovery.test.common.ListenerMock;
import discovery.utils.functional.CompositeFunction;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CompositeCallableTest implements Scenario{
    @steps
    var steps: CompositeCallableSteps;

    @Test
    @scenario
    public function make_callback(){
        given().some_composite_callable_options();
        when().making_a_callable();
        then().a_non_null_callback_should_be_retrieved();
    }

    @Test
    @scenario
    public function join_results_on_callback_count(){
        given().a_callable_was_made_for_these_number_of_calls(3);
        when().calling_it_with([0,1,2]);
        then().the_join_results_function_should_be_called_with([0,1,2]);
    }

    @Test
    @scenario
    public function call_results_when_complete(){
        given().a_callable_was_made_for_these_number_of_calls(3);
        when().calling_it_with([0,1,2]);
        then().the_on_results_callback_should_be_called_with_the_result();
    }
}

class CompositeCallableSteps extends Steps<CompositeCallableSteps>{

    var options:CompositeFunctionOptions<Any, Any>;
    var callback:(Any)->Void;
    var joinedArgs:Array<Any>;

    var expectedResult:Any;
    var listenerMockup:ListenerMock<Any>;

    public function new(){
        super();

        listenerMockup = new ListenerMock();
    }

    // given ---------------------------------------------------


    @step
    public function a_callable_was_made_for_these_number_of_calls(numCalls:Int)
    {
        given().some_composite_callable_options(numCalls);
        when().making_a_callable();
    }

    @step
    public function some_composite_callable_options(expectedCalls:Int = 3) {
        options = {
            expectedCalls: expectedCalls,
            join: (a:Array<Any>)->{
                joinedArgs = a;
                expectedResult = a.length;

                return expectedResult;
            },
            onResult: listenerMockup.onEvent
        };
    }

    // when  ---------------------------------------------------

    @step
    public function making_a_callable() {
        callback = CompositeFunction.make(options);
    }

    @step
    public function calling_it_with(calls:Array<Any>) {
        for(value in calls){
            callback(value);
        }
    }

    // then  ---------------------------------------------------

    @step
    public function a_non_null_callback_should_be_retrieved() {
        callback.shouldNotBeNull();
    }

    @step
    public function
        the_join_results_function_should_be_called_with(expected:Array<Any>)
    {
        joinedArgs.shouldBeEqualsTo(expected);
    }

    @step
    public function the_on_results_callback_should_be_called_with_the_result() {
        listenerMockup.assertWasCalledWith(expectedResult);
    }
}