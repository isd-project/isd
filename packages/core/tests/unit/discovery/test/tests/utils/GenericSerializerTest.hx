package discovery.test.tests.utils;

import haxe.EnumTools.EnumValueTools;
import discovery.utils.time.DateMaker;
import haxe.io.Bytes;
import discovery.domain.Identifier;
import discovery.utils.GenericSerializer;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;

enum ExampleEnum {
    SimpleValue;
}


class GenericSerializerTest implements Scenario{
    @steps
    var steps: GenericSerializerSteps;

    @Test
    @scenario
    public function anonymous_object(){
        var obj = {
            str: 'text',
            i: 1234,
            f: 0.2
        };

        when().serializing_this(obj);
        then().should_produce(obj);
    }


    @Test
    @scenario
    public function with_array(){
        var obj = {
            a: [1, 2, 3, 4],
            b: ['a', 'b', 'c']
        };

        when().serializing_this(obj);
        then().should_produce(obj);
    }

    @Test
    @scenario
    public function serialize_array(){
        var arr = [1, 2, 3, 4];

        when().serializing_this(arr);
        then().should_produce([1, 2, 3, 4]);
        and().result_should_be_an(Array);
    }


    @Test
    @scenario
    public function class_object(){
        var identifier:Identifier = {
            name: 'name',
            type: 'type1',
            providerId: Bytes.ofHex('deadbeef'),
            instanceId: Bytes.ofHex('c00feef00d'),
            parameters: [
                'a' => 'b'
            ]
        };

        when().serializing_this(identifier);
        then().should_produce({
            name: 'name',
            type: 'type1',
            providerId: Bytes.ofHex('deadbeef'),
            instanceId: Bytes.ofHex('c00feef00d'),
            parameters: [
                'a' => 'b'
            ]
        });
    }


    @Test
    @scenario
    public function should_not_serialize_date(){
        var d1 = DateMaker.fromValues(2050, 1, 31, 4, 3, 2, 1);
        var d2 = DateMaker.fromValues(2050, 2, 1, 4, 3, 2, 1);

        var obj = {
            start: d1,
            end: d2,
        };

        when().serializing_this(obj);
        then().should_produce({
            start: d1,
            end: d2
        });
        and().field_should_be_an('start', Date);
    }

    @Test
    @scenario
    public function serialize_object_with_enum(){
        var obj = {
            simple: SimpleValue
        };

        when().serializing_this(obj);
        then().should_produce({
            simple: 'SimpleValue'
        });
        and().field_should_be_an('simple', String);
    }
}

class GenericSerializerSteps extends Steps<GenericSerializerSteps>{
    var serializer = new GenericSerializer();

    var serialized:Dynamic;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function serializing_this<T>(obj:T) {
        serialized = serializer.serialize(obj);
    }

    // then  ---------------------------------------------------

    @step
    public function should_produce(expected: Dynamic) {
        CommonMatchers.shouldBeEqualsTo(serialized, expected);
    }

    @step
    public function result_should_be_an(type:Class<Dynamic>) {
        CommonMatchers.shouldBeAn(serialized, type);
    }

    @step
    public function field_should_be_an(field:String, type:Class<Dynamic>) {
        var fieldValue = Reflect.field(serialized, field);

        CommonMatchers.shouldBeAn(fieldValue, type,
            'field "${field}" does not have the expected type');
    }
}