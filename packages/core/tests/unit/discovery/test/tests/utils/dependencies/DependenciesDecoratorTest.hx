package discovery.test.tests.utils.dependencies;

import haxe.exceptions.NotImplementedException;
import discovery.utils.dependencies.MissingDependencyException;
import haxe.Exception;
import discovery.utils.dependencies.DependenciesDecorator;
import discovery.utils.dependencies.Dependencies;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DependenciesDecoratorTest implements Scenario{
    @steps
    var steps: DependenciesDecoratorSteps;

    @Test
    @scenario
    public function decorate_dependency(){
        when().decorating_this_object({
            mydependency: 'example'
        });
        then().a_non_null_value_should_be_returned();
        then().the_returned_value_should_keep_the_decorated_fields();
    }

    @Test
    @scenario
    public function verify_dependency(){
        given().this_object_was_decorated({
            mydependency: null
        });
        when().requiring_dependency('mydependency');
        then().an_missing_dependency_exception_should_be_thrown();
    }


    @Test
    @scenario
    public function verify_all_dependencies(){
        given().this_object_was_decorated({
            dep1: 'ok',
            dep2: true,
            dep3: 3,
            dep4: null
        });
        when().requiring_all_dependencies();
        then().an_missing_dependency_exception_should_be_thrown();
    }

    @Test
    @scenario
    public function should_add_require_functions_for_each_property(){
        when().decorating_this_object({
            dep1: 'ok',
            dep2: true,
            dep3: 3
        });
        then().these_functions_should_be_created([
            'requireDep1', 'requireDep2', 'requireDep3'
        ]);
    }


    @Test
    @scenario
    public function require_functions_should_return_the_object_value(){
        given().this_object_was_decorated({
            dep1: 'ok',
            dep2: true,
            dep3: 3
        });
        when().calling_require_function('requireDep1');
        then().it_should_return('ok');
    }


    @Test
    @scenario
    public function require_functions_should_verify_dependency(){
        given().this_object_was_decorated({
            dep1: 'ok',
            dep2: null,
            dep3: 3
        });
        when().calling_require_function('requireDep2');
        then().an_missing_dependency_exception_should_be_thrown();
    }
}

class DependenciesDecoratorSteps extends Steps<DependenciesDecoratorSteps>{

    var dependencyObject:Dynamic;
    var decorated:Dynamic;

    var capturedException:Exception;
    var capturedValue:Dynamic;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function this_object_was_decorated(dep:Dynamic) {
        when().decorating_this_object(dep);
    }

    // when  ---------------------------------------------------


    @step
    public function decorating_this_object(dep: Dynamic) {
        this.dependencyObject = dep;
        this.decorated = DependenciesDecorator.decorate(dep);
    }

    @step
    public function requiring_dependency(dependency:String) {
        try{
            this.decorated.require(dependency);
        }
        catch(e){
            capturedException = e;
        }
    }


    @step
    public function requiring_all_dependencies() {
        try{
            this.decorated.requireAll();
        }
        catch(e){
            capturedException = e;
        }
    }

    @step
    public function calling_require_function(requireFunction:String) {
        try{
            var requireMethod = Reflect.field(decorated, requireFunction);
            capturedValue = Reflect.callMethod(decorated, requireMethod, []);
        }
        catch(e:MissingDependencyException){
            capturedException = e;
        }
    }

    // then  ---------------------------------------------------

    @step
    public function an_missing_dependency_exception_should_be_thrown() {
        capturedException.shouldBeAn(MissingDependencyException);
    }

    @step
    public function a_non_null_value_should_be_returned() {
        CommonMatchers.shouldNotBeNull(decorated);
    }

    @step
    public function the_returned_value_should_keep_the_decorated_fields() {
        var initialFields = Reflect.fields(dependencyObject);

        then().decorated_fields_should_contain_in_any_order(initialFields);
    }

    @step
    public function these_functions_should_be_created(expectedFunctions:Array<String>)
    {
        then().decorated_fields_should_contain_in_any_order(expectedFunctions);
    }


    function decorated_fields_should_contain_in_any_order(expectedFunctions:Array<String>)
    {
        var decoratedFields = Reflect.fields(decorated);

        decoratedFields.shouldContainsInAnyOrder(expectedFunctions);
    }

    @step
    public function it_should_return(expectedValue:Dynamic) {
        CommonMatchers.shouldBeEqualsTo(capturedValue, expectedValue);
    }
}