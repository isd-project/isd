package discovery.test.tests.utils.dependencies;

import discovery.utils.dependencies.classmap.ClassInfo;
import discovery.utils.Comparator;
import haxe.io.Bytes;
import discovery.utils.dependencies.ClassMap;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using equals.Equal;


class ClassMapTest implements Scenario{

    var classMap:ClassMap<Any>;

    @Before
    public function setup() {
        classMap = new ClassMap();
    }

    @Test
    @scenario
    public function set_and_get(){
        classMap.set(String, 1);

        assertThat(classMap.get(String), is(1));
    }

    @Test
    @scenario
    public function exists(){
        classMap.set(String, 1);

        assertThat(classMap.exists(String), is(true));
    }


    @Test
    @scenario
    public function remove(){
        classMap.set(String, 1);
        var removed = classMap.remove(String);

        assertThat(classMap.exists(String), is(false));
        assertThat(classMap.get(String), is(null));
        assertThat(removed, is(true), 'Should return true on first remove');
    }


    @Test
    @scenario
    public function remove_non_existent(){
        var removed = classMap.remove(String);

        assertThat(classMap.exists(String), is(false));
        assertThat(removed, is(false),
            'Should return false when trying to remove a key non existent');
    }


    @Test
    @scenario
    public function clear(){
        classMap.set(String, 1);
        classMap.set(Type, 2);

        classMap.clear();

        assertThat(classMap.exists(String), is(false));
        assertThat(classMap.exists(Type), is(false));
    }

    @Test
    @scenario
    public function toStringTest(){
        classMap.set(String, 1);
        classMap.set(Type, 2);

        var str = classMap.toString();

        assertThat(str, is(notNullValue()));
        assertThat(str.length, is(greaterThan(0)));
    }

    @Test
    @scenario
    public function iterator_over_keys(){
        classMap.set(String, 2);
        classMap.set(Type, 3);
        classMap.set(Array, 1);

        var iterator   = classMap.keys();
        var classNames = listClassNames(iterator);

        var expected = [
            Type.getClassName(Array),
            Type.getClassName(String),
            Type.getClassName(Type)];

        assertThat(classNames, is(array(expected)));
    }


    @Test
    @scenario
    public function iterator_over_values(){
        classMap.set(String, 2);
        classMap.set(Type, 3);
        classMap.set(Array, 1);

        var iterator   = classMap.iterator();
        var values     = toArraySorted(iterator);

        var expected = [1, 2, 3];

        assertThat(values, is(array(expected)));
    }

    @Test
    @scenario
    public function iterator_over_key_value(){
        classMap.set(String, 2);
        classMap.set(Type, 3);
        classMap.set(Array, 1);

        var iterator   = classMap.keyValueIterator();
        var classNames:Array<String> = [];
        var values:Array<Int> = [];

        for(key=>value in iterator){
            classNames.push(key.name());
            values.push(value);
        }

        classNames.sort(Reflect.compare);
        values.sort(Reflect.compare);

        assertThat(values, is(array([1, 2, 3])));
        assertThat(classNames, is(array([
            Type.getClassName(Array),
            Type.getClassName(String),
            Type.getClassName(Type)])));
    }


    @Test
    @scenario
    public function copy(){
        classMap.set(String, 2);
        classMap.set(Type, 3);
        classMap.set(Array, 1);

        var copyMap:ClassMap<Any> = classMap.copy();

        assertThat(copyMap, is(not(theInstance(classMap))));

        var keys     = toArray(classMap.keys());
        var keysCopy = toArray(classMap.keys());

        assertThat(keysCopy, is(array(keys)));

        for(key in keys){
            var value = classMap.get(key);
            var otherValue = copyMap.get(key);

            assertThat(otherValue, is(equalTo(value)),
                'Value for key ${key.name()} does not match');
        }
    }


    @Test
    @scenario
    public function create_from_literal(){
        classMap = ClassMap.make([
            String => 1,
            Type => 2
        ]);

        assertThat(classMap.get(String), is(1));
        assertThat(classMap.get(Type), is(2));
    }


    // ----------------------------------------------------

    function listClassNames(iterator:Iterator<ClassInfo>) {
        var classNames:Array<String> = [];

        for(classInfo in iterator){
            classNames.push(classInfo.name());
        }

        classNames.sort(Reflect.compare);

        return classNames;
    }

    function toArraySorted<T>(iterator:Iterator<T>): Array<T>{
        var values = toArray(iterator);

        values.sort(Reflect.compare);

        return values;
    }
    function toArray<T>(iterator:Iterator<T>): Array<T>{
        var values:Array<T> = [];

        for(value in iterator){
            values.push(value);
        }

        return values;
    }
}