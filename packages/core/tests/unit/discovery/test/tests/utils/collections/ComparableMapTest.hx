package discovery.test.tests.utils.collections;

import discovery.format.Binary;
import discovery.format.Binary.BinaryData;
import discovery.test.helpers.RandomGen;
import discovery.utils.Comparator;
import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ComparableMapTest implements Scenario{
    @steps
    var steps: ComparableMapSteps;

    @Test
    @scenario
    public function empty_map(){
        var key = randomInt();

        given().a_map_with_a_comparation_function();
        then().this_key_should_not_exist(key);
    }

    @Test
    @scenario
    public function set_value(){
        var key = randomInt();
        var value = randomInt();

        given().a_map_with_a_comparation_function();
        when().set_key_value(key, value);
        then().this_key_should_exist(key);
    }

    @Test
    @scenario
    public function bytes_map(){
        var key:Binary = RandomGen.binary.bytes(5);

        given().this_map(ComparableMap.ofComparables(BinaryData));
        then().this_key_should_not_exist(key);

        when().set_key_value(key, 1);
        then().this_key_should_exist(key);
    }

    function randomInt() {
        return RandomGen.primitives.int(0);
    }
}

class ComparableMapSteps extends Steps<ComparableMapSteps>{
    var map:IMap<Any, Int>;



    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_map_with_a_comparation_function() {
        given().this_map(new ComparableMap(Comparator.compareInt));
    }

    @step
    public function this_map<K, V>(map:ComparableMap<K, V>) {
        this.map = cast map;
    }

    // when  ---------------------------------------------------

    @step
    public function set_key_value(key:Any, value:Int) {
        map.set(key, value);
    }

    // then  ---------------------------------------------------

    @step
    public function this_key_should_not_exist(key: Any) {
        then().key_existance_should_be(key, false);
    }

    @step
    public function this_key_should_exist(key:Any) {
        then().key_existance_should_be(key, true);
    }


    @step
    function key_existance_should_be(key: Any, exist:Bool) {
        var exists = map.exists(key);

        var shouldExist = if(exist) 'should exist' else 'should not exist';

        assertThat(exists, is(exist), 'Key ${key} $shouldExist in map');
    }
}