package discovery.test.tests.utils.dependencies;

import discovery.utils.dependencies.classmap.ClassInfo;
import discovery.utils.dependencies.ClassMap;
import haxe.io.Output;
import discovery.utils.dependencies.MissingDependencyException;
import haxe.Exception;
import discovery.async.promise.PromisableFactory;
import discovery.utils.dependencies.DependenciesMap;
import discovery.utils.dependencies.Dependencies;
import mockatoo.Mockatoo;
import discovery.keys.Keychain;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DependenciesMapTest implements Scenario{
    @steps
    var steps: DependenciesMapSteps;

    static var _deps:ClassMap<Any>;

    static function deps(): ClassMap<Any> {
        if(_deps == null){
            _deps = makeDeps();
        }

        return _deps;
    }

    static function makeDeps(): ClassMap<Any> {
        var classMap:ClassMap<Any> = ClassMap.makeAny([
            Keychain => Mockatoo.mock(Keychain),
            Output => Mockatoo.mock(Output)
        ]);

        return classMap;
    }

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function get_dependencies(){
        given().these_dependencies_map(deps());
        when().getting_a(Output);
        then().it_should_return(deps().get(Output));
    }

    @Test
    @scenario
    public function get_missing_dependency(){
        given().these_dependencies_map(deps());
        when().getting_a(PromisableFactory);
        then().it_should_return(null);
    }


    @Test
    @scenario
    public function require_dependencies(){
        given().these_dependencies_map(deps());
        when().requiring_a(PromisableFactory);
        then().an_exception_should_have_been_thrown();
    }
}

class DependenciesMapSteps extends Steps<DependenciesMapSteps>{
    var dependencies:Dependencies;

    var gotValue:Any=null;
    var capturedException:Exception;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function these_dependencies_map(depsMap:ClassMap<Any>) {
        dependencies = new DependenciesMap(depsMap);
    }

    // when  ---------------------------------------------------

    @step
    public function getting_a<T>(type:Class<T>) {
        gotValue = dependencies.get(type);
    }

    @step
    public function requiring_a<T>(type:Class<T>) {
        try{
            gotValue = dependencies.require(type);
        }
        catch(e:MissingDependencyException){
            capturedException = e;
        }
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_return(expected:Any) {
        assertThat(gotValue, is(theInstance(expected)), "get value does not match the expected");
    }

    @step
    public function an_exception_should_have_been_thrown() {
        assertThat(capturedException, is(notNullValue()),
            "No exception captured");
    }
}