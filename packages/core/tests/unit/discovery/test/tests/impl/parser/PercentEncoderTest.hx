package discovery.test.tests.impl.parser;

import massive.munit.Assert;
import discovery.format.PercentEncoder;

import org.hamcrest.Matchers.*;

class PercentEncoderTest {
    var encoder:PercentEncoder;

    public function new() {}

    @Before
    public function setup() {
        encoder = new PercentEncoder();
    }

    @Test
    public function testEncoding(){
        var parameters = [
            ["Hello World", "Hello%20World"],
            // ["special chars - /?#[@", "special%20chars%20-%20%2F%3F%23%5B%40"],
            ["/", "%2F"],
            ["?", "%3F"],
            ["#","%23"],
            ["[","%5B"],
            ["@", "%40"],

            // Sub-delimiters:
            ["!", "%21"],
            ["$", "%24"],
            ["&", "%26"],
            ["'", "%27"],
            ["(", "%28"],
            [")", "%29"],
            ["*", "%2A"],
            ["+", "%2B"],
            [",", "%2C"],
            [";", "%3B"],
            ["=", "%3D"],

            // Other characters
            ["ã", "%C3%A3"],
            ["é", "%C3%A9"],
            ["ç", "%C3%A7"]
        ];

        for(params in parameters){
            var input = params[0];
            var expected = params[1];

            checkEncodingAndDecoding(input, expected);
        }
    }


    // -------------------------------------------
    function checkEncodingAndDecoding(input:String, expected:String) {
        var encoded = encoder.encode(input);

        assertThat(encoded, is(equalTo(expected)),
            'For input "${input}", encoding should be "${expected}", but was "${encoded}"'
        );

        var decoded = encoder.decode(expected);
        Assert.areEqual(input, decoded,
            'For encoded text "${expected}", decoding result should be "${input}", but was "${decoded}"'
        );
    }
}