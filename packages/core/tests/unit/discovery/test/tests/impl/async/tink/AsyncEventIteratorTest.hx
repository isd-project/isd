package discovery.test.tests.impl.async.tink;

import massive.munit.Assert;
import discovery.utils.events.EventEmitter;
import discovery.impl.async.tink.AsyncEventIterator;
import discovery.impl.events.signal.SignalEventHandle;

import hxgiven.Steps;
import hxgiven.Scenario;

import tink.core.Future;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class AsyncEventIteratorTest implements Scenario{
    @steps
    var steps:AsyncEventIteratorSteps;

    public function new() {}

    @Test
    @scenario
    public function future_to_first_event() {
        given().an_async_iterator_for_some_event();
        when().calling_next();
        then().a_valid_future_should_be_returned();
        and().the_iterator_should_not_be_done_yet();
    }

    @Test
    @scenario
    public function calling_next_twice_should_return_the_same_future() {
        given().an_async_iterator_for_some_event();
        when().calling_next();
        and().calling_next_again();
        then().the_two_futures_should_be_the_same();
    }

    @Test
    @scenario
    public function receiving_event() {
        given().an_async_iterator_for_some_event();
        and().a_future_to_the_next_event();
        and().a_listener_registered_to_the_future();
        when().an_event_is_triggered();
        then().the_future_should_be_resolved_to_the_event_value();
    }

    @Test
    @scenario
    public function receiving_event_after_ocurred() {
        given().an_async_iterator_for_some_event();
        given().an_event_was_triggered();

        when().calling_next();
        and().registering_a_listener_to_the_future();
        then().the_future_should_be_resolved_to_the_event_value();
    }

    @Test
    @scenario
    public function getting_future_to_new_event() {
        given()
            .an_async_iterator_for_some_event();
            and().a_future_to_the_next_event();
        given().an_event_was_triggered();

        when().calling_next_again();
        then().a_new_future_should_have_been_returned();
    }

    @Test
    @scenario
    public function iterate_over_received_events() {
        given().an_async_iterator_for_some_event();
        given().multiple_events_was_received();
        when().iterating_over_the_async_events();
        then().all_values_should_have_been_received_in_order();
    }

    @Test
    @scenario
    public function after_stop_with_no_pending_events_should_be_done() {
        given().an_async_iterator_for_some_event();
        when().calling_stop();
        then().async_iterator_should_be_done();
    }

    @Test
    @scenario
    public function should_not_be_done_after_stop_if_there_is_pending_events() {
        given().an_async_iterator_for_some_event();
        given().an_event_was_triggered();
        when().calling_stop();
        then().the_iterator_should_not_be_done_yet();
    }

    @Test
    @scenario
    public function when_done_should_not_return_next_futures() {
        given().an_async_iterator_for_some_event();
        given().stop_was_called();
        when().calling_next();
        then().the_returned_future_should_be_null();
    }

    @Test
    @scenario
    public function stop_should_prevent_receiving_future_events() {
        given().an_async_iterator_for_some_event();
        given().stop_was_called();
        when().an_event_is_triggered();
        then().the_event_listener_should_have_been_removed();
        and().async_iterator_should_be_done();
    }
}

class AsyncEventIteratorSteps extends Steps<AsyncEventIteratorSteps> {
    var asyncEvtIterator:AsyncEventIterator<Any>;

    var event:EventEmitter<Any>;
    var nextFuture:Future<Any>;
    var previousFuture:Future<Any>;

    var receivedValue:Any;
    var expectedValue:Any;
    var counter:Int=0;

    var expectedValues:Array<Any>;
    var receivedValues:Array<Any>;

    public function new() {
        super();

        expectedValues = [];
        receivedValues = [];
    }

    /* given ----------------------------------------------------------- **/

    @step
    public function an_event_instance() {
        event = spy(SignalEventHandle);
    }

    @step
    public function an_async_iterator_for_some_event(){
        given().an_event_instance();

        asyncEvtIterator = new AsyncEventIterator(event);
    }

    @step
    public function a_future_to_the_next_event(){
        when().calling_next();
    }

    @step
    public function a_listener_registered_to_the_future(){
        when().registering_a_listener_to_the_future();
    }

    @step
    public function an_event_was_triggered(){
        when().an_event_is_triggered();
    }

    @step
    public function stop_was_called(){
        when().calling_stop();
    }

    @step
    public function multiple_events_was_received(){
        for(i in 0...5){
            when().an_event_is_triggered();
            expectedValues.push(expectedValue);
        }
    }

    /* when ----------------------------------------------------------- **/

    @step
    public function calling_next() {
        nextFuture = asyncEvtIterator.next();
    }

    @step
    public function calling_next_again() {
        previousFuture = nextFuture;

        when().calling_next();
    }

    @step
    public function registering_a_listener_to_the_future(){
        nextFuture.handle((result:Any)->{
            receivedValue = result;
        });
    }

    @step
    public function an_event_is_triggered(){
        ++counter;
        expectedValue = counter;
        event.notify(counter);
    }

    @step
    public function iterating_over_the_async_events(){
        for(i in 0...expectedValues.length){
            when().calling_next();
            when().registering_a_listener_to_the_future();
            receivedValues.push(receivedValue);
        }
    }

    @step
    public function calling_stop(){
        assertThat(asyncEvtIterator, is(notNullValue()));

        asyncEvtIterator.stop();
    }

    /* then ----------------------------------------------------------- **/

    @step
    public function an_event_listener_should_be_registered() {
        Mockatoo.verify(event.listen(isNotNull));
    }

    @step
    public function a_valid_future_should_be_returned() {
        assertThat(nextFuture, is(notNullValue()));
    }

    @step
    public function the_returned_future_should_be_null(){
        assertThat(nextFuture, is(nullValue()));
    }

    @step
    public function the_iterator_should_not_be_done_yet() {
        assertThat(asyncEvtIterator.done(), is(false),
            "Event iterator should not be done yet.");
    }

    @step
    public function async_iterator_should_be_done(){
        assertThat(asyncEvtIterator.done(), is(true),
            "Event iterator should be done."
        );
    }

    @step
    public function the_two_futures_should_be_the_same(){
        assertThat(nextFuture, is(previousFuture));
    }

    @step
    public function a_new_future_should_have_been_returned(){
        assertThat(nextFuture, is(not(previousFuture)));
    }

    @step
    public function the_future_should_be_resolved_to_the_event_value(){
        assertThat(receivedValue, is(notNullValue()), "listener should have received value");
        Assert.areEqual(expectedValue, receivedValue);
    }

    @step
    public function the_event_listener_should_have_been_removed() {
        Mockatoo.verify(event.unlisten(isNotNull));
    }

    @step
    public function all_values_should_have_been_received_in_order() {
        assertThat(expectedValues.length, is(receivedValues.length),
            'Received ${receivedValues.length} values, but was expecting ${expectedValues.length}');

        for (i in 0...expectedValues.length){
            var received = receivedValues[i];
            var expected = expectedValues[i];
            Assert.areEqual(expected, received,
                'Received value at index ${i} (${Std.string(received)}) does not match the expected value: ${Std.string(expected)}');
        }
    }
}