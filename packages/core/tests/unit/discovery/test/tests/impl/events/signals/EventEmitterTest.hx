package discovery.test.tests.impl.events.signals;

import discovery.utils.events.EventEmitterFactory;
import discovery.test.common.ListenerMock;
import discovery.impl.events.signal.SignalEventFactory;
import discovery.utils.events.EventEmitter;
import hxgiven.Steps;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;


class EventEmitterTest implements Scenario{
    @steps
    var steps:EventEmitterSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function listen_once() {
        given().an_event_emitter();
        when().listening_once();
        and().the_event_is_called_twice();
        then().the_listener_should_be_called_only_once();
    }

    @Test
    @scenario
    public function once_event() {
        given().an_once_event_emitter();
        when().a_listener_is_registered();
        and().the_event_is_called_twice();
        then().the_listener_should_be_called_only_once();
    }
}

class EventEmitterSteps extends Steps<EventEmitterSteps> {
    var eventEmitter:EventEmitter<Any>;
    var eventEmitterFactory:EventEmitterFactory;

    var listenerMockup:ListenerMock<Any>;

    public function new() {
        super();
        eventEmitterFactory = new SignalEventFactory();
        listenerMockup = mock(ListenerMock);
    }

    // given ---------------------------------------------------------------
    @step
    public function an_event_emitter() {
        eventEmitter = eventEmitterFactory.eventEmitter();
    }

    @step
    public function an_once_event_emitter() {
        eventEmitter = eventEmitterFactory.eventEmitter({once: true});
    }

    // when ---------------------------------------------------------------
    @step
    public function listening_once() {
        eventEmitter.listen(listenerMockup.onEvent, {once:true});
    }

    @step
    public function a_listener_is_registered() {
        eventEmitter.listen(listenerMockup.onEvent);
    }

    @step
    public function the_event_is_called_twice() {
        eventEmitter.notify(1);
        eventEmitter.notify(2);
    }

    // then ---------------------------------------------------------------
    @step
    public function the_listener_should_be_called_only_once() {
        var self = this;
        Mockatoo.verify(self.listenerMockup.onEvent(isNotNull), 1);
    }
}