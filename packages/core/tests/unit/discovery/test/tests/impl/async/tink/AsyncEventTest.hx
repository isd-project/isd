package discovery.test.tests.impl.async.tink;

import massive.munit.Assert;
import discovery.utils.events.EventEmitter;
import discovery.impl.async.tink.AsyncEvents;
import discovery.impl.events.signal.SignalEventHandle;

import tink.core.Future;
import hxgiven.Steps;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class AsyncEventTest implements Scenario{
    @steps
    var steps:AsyncEventSteps;

    public function new() {}

    @Test
    @scenario
    public function create_future_to_next_event() {
        given().an_event_instance();
        when().creating_a_future_to_the_next_event();
        then().an_event_listener_should_be_registered();
        and().a_valid_future_should_be_returned();
    }

    @Test
    @scenario
    public function receive_next_async_event() {
        given().a_future_to_the_next_event();
        and().a_listener_registered_to_the_future();
        when().the_event_is_triggered();
        then().the_future_listener_should_receive_the_value();
    }
}


class AsyncEventSteps extends Steps<AsyncEventSteps>{
    var event:EventEmitter<Any>;
    var nextFuture:Future<Any>;

    var receivedValue:Any;
    var expectedValue:Any;

    public function new() {
        super();
    }

    /* given ----------------------------------------------------------- **/

    @step
    public function an_event_instance() {
        event = spy(SignalEventHandle);
    }

    @step
    public function a_future_to_the_next_event(){
        given().an_event_instance();
        when().creating_a_future_to_the_next_event();
        then().a_valid_future_should_be_returned();
    }

    @step
    public function a_listener_registered_to_the_future(){
        nextFuture.handle((result:Any)->{
            receivedValue = result;
        });
    }

    /* when ----------------------------------------------------------- **/

    @step
    public function creating_a_future_to_the_next_event() {
        nextFuture = AsyncEvents.nextAsync(event);
    }

    @step
    public function the_event_is_triggered(){
        expectedValue = "Hello";
        event.notify(expectedValue);
    }

    /* then ----------------------------------------------------------- **/

    @step
    public function an_event_listener_should_be_registered() {
        Mockatoo.verify(event.listen(isNotNull, any));
    }

    @step
    public function a_valid_future_should_be_returned() {
        assertThat(nextFuture, is(notNullValue()),
            "received future should not be null");
    }

    @step
    public function the_future_listener_should_receive_the_value(){
        assertThat(receivedValue, is(notNullValue()), "listener should have received value");
        Assert.areEqual(expectedValue, receivedValue);
    }
}