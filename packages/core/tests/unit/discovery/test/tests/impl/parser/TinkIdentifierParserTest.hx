package discovery.test.tests.impl.parser;

import haxe.crypto.BaseCode;
import massive.munit.Assert;
import haxe.io.Bytes;
import discovery.impl.parser.TinkIdentifierParser;
import discovery.domain.Identifier;
import discovery.domain.IdentifierUrl;
import discovery.format.exceptions.InvalidFormatException;
import discovery.format.exceptions.MissingFieldException;
import discovery.format.exceptions.InvalidValueException;

import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

class TinkIdentifierParserTest{
    var parser:TinkIdentifierParser;

    public function new() {}

    @Before
    public function setup() {
        parser = new TinkIdentifierParser();
    }

    @Test
    public function parsing_simple() {
        checkParsing(
            "srv://example.http.E1UDUKhoHITIYarC5bRAFhwrM6Pk8wKsaAyltobeSN4=",
            {
                name: "example",
                type: "http",
                providerId: Bytes.ofHex("13550350a8681c84c861aac2e5b440161c2b33a3e4f302ac680ca5b686de48de")
            });
    }

    @Test
    public function parsing_with_application_id() {
        checkParsing(
            "srv://Ov_Ojw==.example.http.E1UDUKhoHITIYarC5bRAFhwrM6Pk8wKsaAyltobeSN4=",
            {
                name: "example",
                type: "http",
                providerId: Bytes.ofHex("13550350a8681c84c861aac2e5b440161c2b33a3e4f302ac680ca5b686de48de"),
                instanceId: Bytes.ofHex("3affce8f")
            });
    }

    @Test
    public function parsing_with_parameters() {
        checkParsing(
            "srv://14au.myserver.http.xEwKlsAC2fElzgZA6LefQ_FSavCMkj4XyKHzLY8hzkk;param1=example;param2=;cn=http,webrtc,https",
            {
                name: "myserver",
                type: "http",
                providerId: Bytes.ofHex("c44c0a96c002d9f125ce0640e8b79f43f1526af08c923e17c8a1f32d8f21ce49"),
                instanceId: Bytes.ofHex("d786ae"),
                parameters: [
                    "param1" => "example",
                    "param2" => "",
                    "cn"     => "http,webrtc,https"
                ]
            });
    }

    @Test
    public function parsing_utf8_name() {
        checkParsing(
            "srv://W46M.my%20%f0%90%8d%86anc%f0%90%8d%85%20%ce%bctf-8%20name.ftp.l3aimxGXVn7UpP_B3xEyfTjP4l5kJF_2s99EX-Q4OEE",
            {
                name: "my 𐍆anc𐍅 μtf-8 name",
                type: "ftp",
                providerId: Bytes.ofHex("9776a29b1197567ed4a4ffc1df11327d38cfe25e64245ff6b3df445fe4383841"),
                instanceId: Bytes.ofHex("5b8e8c")
            });
    }

    @Test
    public function parsing_base32() {
        checkParsing(
            "srv://LOHIY.anyname.ftp.S53KFGYRS5LH5VFE77A56EJSPU4M7YS6MQSF75VT35CF7ZBYHBAQ;fmt=b32",
            {
                name: "anyname",
                type: "ftp",
                providerId: Bytes.ofHex("9776a29b1197567ed4a4ffc1df11327d38cfe25e64245ff6b3df445fe4383841"),
                instanceId: Bytes.ofHex("5b8e8c"),
                parameters:["fmt" => "b32"]
            });
    }

    @Test
    public function parsing_base16() {
        checkParsing(
            "srv://5b8e8c.anyname.ftp.9776a29b1197567ed4a4ffc1df11327d38cfe25e64245ff6b3df445fe4383841;fmt=b16",
            {
                name: "anyname",
                type: "ftp",
                providerId: Bytes.ofHex("9776a29b1197567ed4a4ffc1df11327d38cfe25e64245ff6b3df445fe4383841"),
                instanceId: Bytes.ofHex("5b8e8c"),
                parameters:["fmt" => "b16"]
            });
    }

    @Test
    public function parsing_path_query_fragment() {
        checkParsing(
            "srv://example.http.OxDeMDe1fKMij972utY07JHSzdim_R8FTwvKAIn2aU0/my/resource/path?q=query&lorem=ipsum&boolean#myfragment",
            {
                name: "example",
                type: "http",
                providerId: Bytes.ofHex("3b10de3037b57ca3228fdef6bad634ec91d2cdd8a6fd1f054f0bca0089f6694d"),
                path: ["my","resource","path"],
                query: [
                    "q" => "query",
                    "lorem" => "ipsum",
                    "boolean" => ""
                ],
                fragment: "myfragment"
            });
    }

    @Test
    public function parsing_invalid_urls(){
        checkInvalid("", InvalidFormatException);
        checkInvalid("srv:", InvalidFormatException);
        checkInvalid("srv://", InvalidFormatException);
        checkInvalid("srv://example", MissingFieldException);
        checkInvalid("srv://example.http", MissingFieldException);
        checkInvalid("http://example.http.OxDeMDe1fKMij972utY07JHSzdim_R8FTwvKAIn2aU0", InvalidValueException);
        checkInvalid("srv://example.http.OxDeMDe1fKMij972utY07JHSzdim,R8FTwvKAIn2aU0", InvalidFormatException);
        checkInvalid("srv://example.http.OxDeMDe1fKMij972utY07JHSzdim_R8FTwvKAIn2aU0;fmt=b32", InvalidFormatException);
        checkInvalid("srv://(,).example.http.OxDeMDe1fKMij972utY07JHSzdim_R8FTwvKAIn2aU0", InvalidFormatException);


        // Maybe should throw: using spaces without percent-encoding
        // checkInvalid("srv://not encoded spaces.http.OxDeMDe1fKMij972utY07JHSzdim_R8FTwvKAIn2aU0", InvalidFormatException);
    }



    // ------------------------------------------------------

    function checkParsing(url:String, expected: IdentifierUrl){
        var result = parser.parseUrl(url);

        var err = 'For url "${url}": ';

        assertThat(result, is(notNullValue()), err + "parser returned null");

        compareField(url, "name", result.name, expected.name);
        compareField(url, "type", result.type, expected.type);
        compareBytes(url, "providerId", result.providerId, expected.providerId);
        compareBytes(url, "instanceId", result.instanceId, expected.instanceId);
        compareMap(url, "parameters", result.parameters, expected.parameters);

        compareField(url, "path", result.path, expected.path);
        compareMap(url, "query", result.query, expected.query);
        compareField(url, "fragment", result.fragment, expected.fragment);
    }

    function checkInvalid(invalidUrl:String, expectedException:Dynamic) {
        var err = 'For invalid url "${invalidUrl}" ';

        try{
            parser.parseUrl(invalidUrl);
        }
        catch(e: haxe.Exception){
            if(Std.isOfType(e, expectedException)){
                return; //passed
            }
            else{
                Assert.fail(err + 'expected exception of type ${Type.getClassName(expectedException)} but got ${Type.getClassName(Type.getClass(e))}: ${e.details()}');
            }
        }

        Assert.fail(err + "expected exception wasn't thrown!");
    }

    // ------------------------------------------------------------------------


    function compareField<T>(url:String, field:String, result: T, expected: T)
    {
        assertThat(result, is(equalTo(expected)),
                    errText(url, field, ' should be "${expected}"'));
    }

    function compareBytes(url:String, field:String, result: Bytes, expected: Bytes)
    {
        if(expected == null){
            assertThat(result, is(nullValue()));
        }
        else {
            assertThat(result, is(notNullValue()),
                        errText(url, field, 'should not be null')
            );
            assertThat(result.compare(expected), is(0),
                        errText(url, field,
                            'should be "${expected.toHex()}", but was ${result.toHex()}')
            );
        }
    }

    function compareMap<K, V>(url:String, field:String, result: Map<K, V>, expected: Map<K,V>)
    {
        if(expected == null){
            assertThat(result, is(expected));
        }
        else{
            assertThat(result, is(notNullValue()),
                errText(url, field, 'should not be null')
            );
            Assert.areEqual(expected, result);
        }
    }

    function errText(url: String, field: String, msg: String) {
        return 'For url "${url}": ${field} ${msg}';
    }
}