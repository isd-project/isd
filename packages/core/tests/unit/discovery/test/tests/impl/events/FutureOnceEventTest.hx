package discovery.test.tests.impl.events;

import discovery.test.helpers.RandomGen;
import discovery.impl.events.FutureOnceEvent;
import discovery.test.common.ListenerMock;
import discovery.utils.events.EventEmitter;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class FutureOnceEventTest implements Scenario{
    @steps
    var steps: FutureOnceEventSteps;

    @Test
    @scenario
    public function notify_value(){
        given().an_event_instance();
        given().a_listener_was_registered();
        when().a_value_is_received();
        then().the_listener_should_be_notified();
    }


    @Test
    @scenario
    public function receive_value_after_notified(){
        given().an_event_instance();
        given().a_value_was_received();
        when().a_listener_is_registered();
        then().the_listener_should_be_notified();
    }

    @Test
    @scenario
    public function remove_listener(){
        given().an_event_instance();
        given().a_listener_is_registered();
        when().the_listener_is_removed();
        and().a_value_is_received();
        then().the_listener_should_not_be_notified();
    }
}

class FutureOnceEventSteps extends Steps<FutureOnceEventSteps>{
    var event:EventEmitter<Any>;
    var listener:ListenerMock<Any>;

    var expectedValue:Any;

    public function new(){
        super();

        listener = new ListenerMock();
    }

    // given ---------------------------------------------------

    @step
    public function an_event_instance() {
        event = new FutureOnceEvent();
    }

    @step
    public function a_value_was_received() {
        when().a_value_is_received();
    }

    @step
    public function a_listener_was_registered() {
        when().a_listener_is_registered();
    }

    // when  ---------------------------------------------------

    @step
    public function a_listener_is_registered() {
        event.listen(listener.onEvent);
    }

    @step
    public function the_listener_is_removed() {
        event.unlisten(listener.onEvent);
    }

    @step
    public function a_value_is_received() {
        expectedValue = RandomGen.primitives.int();
        event.notify(expectedValue);
    }

    // then  ---------------------------------------------------

    @step
    public function the_listener_should_be_notified() {
        listener.assertWasCalledWith(expectedValue);
    }

    @step
    public function the_listener_should_not_be_notified() {
        listener.assertWasCalledTheseTimes(0);
    }
}