package discovery.test.tests.impl.parser;

import hxgiven.Scenario;
import discovery.format.formats.Base64UrlFormat;
import discovery.format.formats.Base16Format;
import discovery.format.formats.Base32Format;
import discovery.format.Formatter;
import haxe.io.Bytes;
import discovery.format.formats.Base64Format;

import org.hamcrest.Matchers.*;


class FormatTest implements Scenario{
    var formatter:Formatter;

    public function new() {}

    @Test
    @scenario
    public function base64_UTF8() {
        var parameters:Array<Array<String>> = [
            ["","", ""],
            ["1", "MQ", "=="],
            ["12", "MTI", "="],
            ["123", "MTIz", ""],
            ["Lorem ipsum", "TG9yZW0gaXBzdW0", "="],
            ["Nam vel lectus ut.", "TmFtIHZlbCBsZWN0dXMgdXQu", ""],
            ["Nulla tincidunt, massa vel convallis.", "TnVsbGEgdGluY2lkdW50LCBtYXNzYSB2ZWwgY29udmFsbGlzLg", "=="],
            ["Consectetur adipiscing elit, sed do eiusmod tempor", "Q29uc2VjdGV0dXIgYWRpcGlzY2luZyBlbGl0LCBzZWQgZG8gZWl1c21vZCB0ZW1wb3I","="]
        ];


        for (usingPadding in [true, false]){
            usingFormat(new Base64Format(usingPadding));
            testFormat(parameters, usingPadding);
        }
    }

    @Test
    @scenario
    public function base64_binary() {

        // arrays of: [<hex>, <base64>, <padding>]
        var parameters:Array<Array<String>> = [
            ["3affce8f", "Ov/Ojw", "=="],
            ["13550350a8681c84c861aac2e5b440161c2b33a3e4f302ac680ca5b686de48de", "E1UDUKhoHITIYarC5bRAFhwrM6Pk8wKsaAyltobeSN4", "="]
        ];

        for(usingPadding in [true, false]){
            usingFormat(new Base64Format(usingPadding));
            testFormatBinary(parameters, usingPadding);
        }
    }

    @Test
    @scenario
    public function base64Url_binary() {
        // arrays of: [<hex>, <base64>, <padding>]
        var parameters:Array<Array<String>> = [
            ["3affce8f", "Ov_Ojw", "=="],
            ["5b8e8c", "W46M", ""],
            ["13550350a8681c84c861aac2e5b440161c2b33a3e4f302ac680ca5b686de48de", "E1UDUKhoHITIYarC5bRAFhwrM6Pk8wKsaAyltobeSN4", "="]
        ];

        for(usingPadding in [true, false]){
            usingFormat(new Base64UrlFormat(usingPadding));
            testFormatBinary(parameters, usingPadding);
        }
    }

    @Test
    @scenario
    public function base32() {
        for(usingPadding in [true, false]){
            usingFormat(new Base32Format(usingPadding));

            var parameters:Array<Array<String>> = [
                ["","", ""],
                ["12345", "GEZDGNBV", ""],
                ["1234", "GEZDGNA", "="],
                ["123", "GEZDG", "==="],
                ["12", "GEZA", "===="],
                ["1", "GE", "======"],
                ["123456", "GEZDGNBVGY", "======"],
                ["Lorem ipsum", "JRXXEZLNEBUXA43VNU", "======"],
                ["Nam vel lectus ut.", "JZQW2IDWMVWCA3DFMN2HK4ZAOV2C4", "==="],
                ["Nulla tincidunt, massa vel convallis.", "JZ2WY3DBEB2GS3TDNFSHK3TUFQQG2YLTONQSA5TFNQQGG33OOZQWY3DJOMXA", "===="],
                ["Consectetur adipiscing elit, sed do eiusmod tempor", "INXW443FMN2GK5DVOIQGCZDJOBUXGY3JNZTSAZLMNF2CYIDTMVSCAZDPEBSWS5LTNVXWIIDUMVWXA33S", ""]
            ];
            testFormat(parameters, usingPadding);
        }
    }

    function usingFormat(formatter:Formatter) {
        this.formatter = formatter;
    }

    function testFormat(parameters: Array<Array<String>>, usingPadding:Bool) {
        for (param in parameters){
            assertThat(param, hasSize(3), 'input: $param');

            var input:String = param[0];
            var encoded:String = param[1];
            var padding:String = param[2];

            checkEncode(input, encoded, padding, usingPadding);
            checkDecode(encoded, input, padding);
        }
    }

    function testFormatBinary(parameters:Array<Array<String>>, usingPadding: Bool)
    {
        for (param in parameters){
            assertThat(param, hasSize(3), 'input: $param');

            var hex:String = param[0];
            var encoded:String = param[1];
            var padding:String = param[2];

            checkEncodeBinary(Bytes.ofHex(hex), encoded, padding, usingPadding);
            checkDecodeBinary(encoded, hex, padding);
        }
    }

    function checkEncode(
        input: String, expected: String, padding:String, usingPadding:Bool)
    {
        expected = if(usingPadding) expected + padding else expected;
        var usingPaddingMsg = if(usingPadding) "using" else "not using";

        var inputBytes = Bytes.ofString(input);

        var result = formatter.encode(inputBytes);

        assertThat(result, is(equalTo(expected)),
            'For input "${input}", failed to encode with ${formatterName()}, when $usingPaddingMsg');
    }

    function checkEncodeBinary(
        input: Bytes, expected: String, padding:String, usingPadding:Bool)
    {
        expected = if(usingPadding) expected + padding else expected;

        var result = formatter.encode(input);

        assertThat(result, is(equalTo(expected)),
            'For input "${input.toHex()}", failed to encode using ${formatterName()}');
    }


    function checkDecode(input: String, expected: String, padding:String) {
        for (usingPadding in [true, false]){
            var inputText = if(usingPadding) input + padding else input;
            var result = formatter.decode(inputText);
            var resultUTF8 = result.toString();

            assertThat(resultUTF8, is(equalTo(expected)),
                'For input "${inputText}", failed to decode using ${formatterName()}');
        }
    }

    function checkDecodeBinary(
        input: String, expectedHex: String, padding:String)
    {
        for (decodeInput in [input, input + padding]){
            var result = formatter.decode(decodeInput);

            assertThat(result.toHex(), is(equalTo(expectedHex)),
                'For input "${decodeInput}", failed to decode using ${formatterName()}');
        }
    }

    function formatterName() {
        return Type.getClassName(Type.getClass(formatter));
    }

}