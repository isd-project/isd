package discovery.test.mockups;

import discovery.domain.operators.Operator;
import discovery.domain.Discovered;
import discovery.domain.operators.Operator.Operand;
import discovery.domain.operators.common.BaseOperator;

class FakeOperator extends BaseOperator<Any>{
    public static function fakeOperator<T>(type:String): Operator<T> {
        return cast new FakeOperator(type);
	}

    public function new(type:String) {
        super(type);
    }

    public function evaluate(discovered:Discovered):Any {
        throw new haxe.exceptions.NotImplementedException();
    }

    public function operands():Array<Operand> {
        return [];
    }
}