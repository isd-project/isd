package discovery.test.mockups;

import discovery.domain.operators.Operator;
import discovery.domain.operators.constraints.OperatorConstraint;
import discovery.utils.filter.FilterResult;

class FakeConstraint implements OperatorConstraint{
    var result:FilterResult;

    public function new(value: FilterResult) {
        this.result = value;
    }

    public function filter(value:OperatorNode):FilterResult {
        return result;
    }
}