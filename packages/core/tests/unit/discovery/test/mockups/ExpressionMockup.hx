package discovery.test.mockups;

import discovery.domain.operators.BooleanExpression;
import mockatoo.Mockatoo;
import discovery.domain.operators.Expression;

class ExpressionMockup {
    var expressionMock:BooleanExpression;

    public function new() {
        expressionMock = Mockatoo.mock(Expression);
    }

    public function shouldEvaluateTo(result:Bool) {
        Mockatoo.when(expressionMock.evaluate(any)).thenReturn(result);
    }

    public function expression():BooleanExpression {
        return expressionMock;
    }
}