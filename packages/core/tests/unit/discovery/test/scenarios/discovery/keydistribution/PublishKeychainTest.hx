package discovery.test.scenarios.discovery.keydistribution;

import discovery.keys.Keychain;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class PublishKeychainTest implements Scenario{
    @steps
    var steps: PublishKeychainSteps;

    @Test
    @scenario
    public function on_first_publication_register_keychain(){
        given().a_discovery_instance_with_a_valid_pki();
        given().the_discovery_has_a_keychain();
        when().publishing_some_service();
        then().the_keychain_should_be_registered_at_the_pki();
    }

    @Test
    @scenario
    public function should_not_register_keychain_twice(){
        given().the_keychain_was_already_published();
        when().publishing_other_service();
        then().the_keychain_should_not_be_registered_twice();
    }
}

class PublishKeychainSteps extends Steps<PublishKeychainSteps>{
    var discovery: Discovery;
    var discoveryFactory: DiscoveryFactoryMockup;
    var discoveryKeychain: Keychain;

    public function new(){
        super();

        discoveryFactory = new DiscoveryFactoryMockup();
    }

    function pkiMockup(){
        return discoveryFactory.pkiMockup();
    }

    //summary --------------------------------------------------

    @step
    public function the_keychain_was_already_published() {
        given().a_discovery_instance_with_a_valid_pki();
        given().the_discovery_has_a_keychain();
        when().publishing_some_service();
    }

    // given ---------------------------------------------------

    @step
    public function a_discovery_instance_with_a_valid_pki() {
        discovery = discoveryFactory.build();
    }

    @step
    public function the_discovery_has_a_keychain() {
        discoveryKeychain = discoveryFactory.keychain();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_other_service() {
        when().publishing_some_service();
    }

    @step
    public function publishing_some_service() {
        var description = RandomGen.description();
        discovery.publish(description);
    }

    // then  ---------------------------------------------------

    @step
    public function the_keychain_should_be_registered_at_the_pki() {
        pkiMockup().checkKeychainPublished(discoveryKeychain);
    }

    @step
    public function the_keychain_should_not_be_registered_twice() {
        pkiMockup().checkKeychainPublished(discoveryKeychain, times(1));
    }
}