package discovery.test.scenarios.discovery;

import discovery.domain.operators.BooleanExpression;
import discovery.domain.operators.Expression;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description.Attributes;
import discovery.domain.Discovered;
import discovery.domain.operators.Operators;
import discovery.utils.EqualsComparator;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class SearchWithOperatorsTest implements Scenario{
    @steps
    var steps: SearchWithOperatorsSteps;

    @Test
    @scenario
    public function simple_operation(){
        given().a_service_with_these_attributes({
            myattrib: "example"
        });
        and().a_service_with_these_attributes({
            myattrib: "othervalue"
        });
        when().searching_a_service_using_this_expression(
            Operators.equals(Operators.attr("myattrib"), Operators.value("example"))
        );
        then().the_service_at_index_x_should_be_found(0);
        but().the_service_at_index_x_should_not_be_found(1);
    }
}

class SearchWithOperatorsSteps extends Steps<SearchWithOperatorsSteps>{
    var services: Array<Discovered>;
    var foundServices:Array<Discovered>;

    var discovery:Discovery;
    var discoveryFactory:DiscoveryFactoryMockup;


    public function new(){
        super();

        services = [];
        foundServices = [];

        discoveryFactory = new DiscoveryFactoryMockup();

        discovery = discoveryFactory.build();
    }

    // given ---------------------------------------------------

    @step
    public function a_service_with_these_attributes(attribs: Attributes) {
        var discovered = RandomGen.discovered();
        discovered.description().attributes = attribs;

        services.push(discovered);

        this.discoveryFactory.mechanismMockup().registerDiscovered(discovered);
    }

    // when  ---------------------------------------------------

    @step
    public function
        searching_a_service_using_this_expression(expression: BooleanExpression)
    {
        when().starting_the_search(expression);
        when().the_services_are_found();
    }

    @step
    function starting_the_search(expression:BooleanExpression) {
        discovery.search({
            query: expression
        }).onFound.listen((discovered)->{
            foundServices.push(discovered);
        });
    }

    @step
    function the_services_are_found() {
        discoveryFactory.mechanismMockup().notifyAllDiscoveredServices();
    }

    // then  ---------------------------------------------------

    @step
    public function the_service_at_index_x_should_be_found(idx:Int) {
        then().should_have_found_the_service_at_index(idx, true);
    }

    @step
    public function the_service_at_index_x_should_not_be_found(idx:Int) {
        then().should_have_found_the_service_at_index(idx, false);
    }

    function should_have_found_the_service_at_index(idx:Int, shouldFound:Bool) {
        var expected = services[idx];

        var matcher = if(shouldFound) hasItem(expected)
                        else not(hasItem(expected));

        var should = if(shouldFound) 'Should' else 'Should not';

        assertThat(foundServices, matcher,
            '$should found a service with these attributes: ${expected.description().attributes}'
        );
    }
}