package discovery.test.scenarios.discovery;

import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import discovery.test.common.mockups.utils.TimerLoopMockup;
import discovery.utils.time.Duration;
import discovery.base.search.SearchOptions;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.time.TimeUnitTools;


class SearchTimeoutTest implements Scenario{
    @steps
    var steps: SearchTimeoutSteps;


    @Test
    @scenario
    public function no_timeout_by_default(){
        given().search_options_with_no_timeout();
        when().starting_a_search();
        then().no_timeout_handler_should_have_been_scheduled();
    }

    @Test
    @scenario
    public function schedule_search_with_timeout(){
        given().search_options_with_some_timeout();
        when().starting_a_search();
        then().a_timeout_handler_should_be_scheduled();
    }


    @Test
    @scenario
    public function schedule_locate_with_timeout(){
        given().search_options_with_some_timeout();
        when().starting_a_locate_search();
        then().a_timeout_handler_should_be_scheduled();
    }

    @Test
    @scenario
    public function finish_search_after_timeout(){
        given().a_search_with_timeout_has_started();
        when().the_timeout_completes();
        then().the_search_should_be_finished();
    }

    @Test
    @scenario
    public function cancel_timer_if_search_finished_before(){
        given().a_search_with_timeout_has_started();
        when().the_search_finishes();
        then().the_timeout_timer_should_be_cancelled();
    }
}

class SearchTimeoutSteps extends Steps<SearchTimeoutSteps>{

    var discovery:Discovery;
    var discoveryFactory = new DiscoveryFactoryMockup();
    var timerLoop:TimerLoopMockup;

    var searchOptions:SearchOptions;
    var timeout:Duration;

    public function new(){
        super();

        timerLoop = discoveryFactory.timerLoopMockup();
        discovery = discoveryFactory.build();
    }

    function lastSearchMockup() {
        return discoveryFactory.mechanismMockup()
                        .getLastSearchMockup();
    }

    // given ---------------------------------------------------

    @step
    public function a_search_with_timeout_has_started() {
        given().search_options_with_some_timeout();
        when().starting_a_search();
    }

    // given ---------------------------------------------------

    @step
    public function search_options_with_no_timeout() {
        searchOptions = {};
    }

    @step
    public function search_options_with_some_timeout() {
        timeout = 15.seconds();

        searchOptions = {
            timeout: timeout
        };
    }

    // when  ---------------------------------------------------

    @step
    public function starting_a_search() {
        var query = RandomGen.typeQuery();

        discovery.search(query, searchOptions);
    }

    @step
    public function starting_a_locate_search() {
        var identifier = RandomGen.identifier();

        discovery.locate(identifier, searchOptions);
    }

    @step
    public function the_timeout_completes() {
        timerLoop.executeNextDelayed();
    }

    @step
    public function the_search_finishes() {
        lastSearchMockup().finishSearch();
    }

    // then  ---------------------------------------------------

    @step
    public function a_timeout_handler_should_be_scheduled() {
        timerLoop.verifyDelayCalledWith(timeout.time());
    }

    @step
    public function no_timeout_handler_should_have_been_scheduled() {
        timerLoop.verifyDelayCalled(never);
    }

    @step
    public function the_search_should_be_finished() {
        lastSearchMockup().verifySearchStopped();
    }

    @step
    public function the_timeout_timer_should_be_cancelled() {
        timerLoop.verifyPreviousDelayCancelled();
    }
}