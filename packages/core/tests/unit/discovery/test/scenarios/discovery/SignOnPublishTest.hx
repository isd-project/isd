package discovery.test.scenarios.discovery;

import discovery.format.Binary;
import haxe.io.Bytes;
import discovery.test.common.FieldRetriever;
import discovery.keys.crypto.signature.Signature;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class SignOnPublishTest implements Scenario{
    @steps
    var steps: SignOnPublishSteps;

    @Test
    @scenario
    public function sign_published_description(){
        given().a_discovery_instance();
        given().a_description_with_the_provider_key_identifier();
        when().publishing_it();
        and().the_publication_succeeds();
        then().a_signature_should_be_made_from_all_published_description_fields();
        and().the_signed_description_should_have_been_published();
    }
}

class SignOnPublishSteps extends Steps<SignOnPublishSteps>{
    var discovery:Discovery;
    var discoveryFactory:DiscoveryFactoryMockup;
    var description:Description;

    public function new(){
        super();

        discoveryFactory = new DiscoveryFactoryMockup();
    }

    function keyMockup() {
        return keychainMockup().getKeyMockup();
    }
    function keychainMockup() {
        return discoveryFactory.keychainMockup();
    }

    function publishedInfo(){
        return discoveryFactory.mechanismMockup().getPublishedInfo();
    }
    function publishedDescription(){
        return discoveryFactory.mechanismMockup().getPublishedDescription();
    }

    // given ---------------------------------------------------

    @step
    public function a_discovery_instance() {
        discovery = discoveryFactory.build();
    }

    @step
    public function a_description_with_the_provider_key_identifier() {
        description = RandomGen.fullDescription();
        description.providerId = RandomGen.crypto.keyIdentifier();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_it() {
        discovery.publish(description);
    }

    @step
    public function the_publication_succeeds() {
        when().the_provider_key_is_retrieved();
        and().the_node_ips_are_discovered();
    }

    @step
    function the_provider_key_is_retrieved() {
        var keyId = description.providerId;
        keychainMockup().resolveGetKeyToDefault(keyId);
    }

    @step
    function the_node_ips_are_discovered() {
        discoveryFactory.ipDiscoverMockup().resolvePublicIps();
    }

    // then  ---------------------------------------------------

    @step
    public
    function a_signature_should_be_made_from_all_published_description_fields() {
        then().a_signature_should_be_generated();
        and().all_description_fields_should_be_added_to_signature();
    }

    @step
    function a_signature_should_be_generated() {
        keyMockup().checkSignatureStarted();
    }

    @step
    function all_description_fields_should_be_added_to_signature() {
        var retriever = new FieldRetriever();
        var expectedFields = retriever.listSimpleFieldsOf(publishedDescription());

        var fakeSigner = keyMockup().keySigner;

        for(field in expectedFields){
            if(Std.isOfType(field.value, Bytes)){
                field.value = Binary.fromBytes(field.value);
            }

            if(field.value == null){
                continue;
            }

            if(field.key != null){ //key may be null on arrays
                fakeSigner.verifyAppendValue(field.key);
            }
            fakeSigner.verifyAppendValue(field.value);
        }
    }

    @step
    public function the_signed_description_should_have_been_published() {
        assertThat(this.publishedInfo(), is(notNullValue()),
            "No data was published");

        var signed = publishedInfo().signed();

        assertThat(signed, is(notNullValue()),
            "Missing signed description");
        assertThat(signed.signature, is(notNullValue()),
            "Signature was not published");
    }
}