package discovery.test.scenarios.discovery;

import discovery.domain.IpDiscover;
import mockatoo.Mockatoo;
import discovery.test.helpers.RandomGen;
import discovery.domain.Description;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class DiscoverIpsOnPublishTest implements Scenario{
    @steps
    var steps: DiscoverIpsOnPublishSteps;

    @Test
    @scenario
    public function start_listing_ips_on_publish(){
        given().a_discovery_instance_with_a_valid_ip_discoverer();
        given().some_service_description_to_publish();
        when().publishing_it();
        then().the_list_of_ips_should_be_retrieved();
    }

    @Test
    @scenario
    public function publish_discovered_ips_within_description(){
        given().a_service_publication_was_started();
        when().the_ip_discovery_finishes();
        and().the_service_publication_succeeds();
        then().the_discovered_ips_should_have_been_published();
    }
}

class DiscoverIpsOnPublishSteps extends Steps<DiscoverIpsOnPublishSteps>{
    var discovery:Discovery;
    var discoveryFactory:DiscoveryFactoryMockup;

    var description:Description;

    public function new(){
        super();

        discoveryFactory = new DiscoveryFactoryMockup();
    }

    public function ipDiscover(): IpDiscover {
        return discoveryFactory.ipDiscover();
    }

    // given ---------------------------------------------------

    @step
    public function a_discovery_instance_with_a_valid_ip_discoverer() {
        discovery = discoveryFactory.build();
    }

    @step
    public function some_service_description_to_publish() {
        description = RandomGen.description();
    }

    @step
    public function a_service_publication_was_started() {
        given().a_discovery_instance_with_a_valid_ip_discoverer();
        and().some_service_description_to_publish();
        when().publishing_it();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_it() {
        discovery.publish(description);
    }

    @step
    public function the_ip_discovery_finishes() {
        discoveryFactory.ipDiscoverMockup().resolvePublicIps();
    }

    @step
    public function the_service_publication_succeeds() {
        when().the_provider_key_is_found();
    }

    @step
    function the_provider_key_is_found() {
        var providerKeyId = description.providerId;
        discoveryFactory.keychainMockup()
            .resolveGetKeyToDefault(providerKeyId);
    }

    // then  ---------------------------------------------------

    @step
    public function the_list_of_ips_should_be_retrieved() {
        var ipDiscover = this.ipDiscover();
        Mockatoo.verify(ipDiscover.listPublicIps());
    }

    @step
    public function the_discovered_ips_should_have_been_published() {
        var desc = discoveryFactory.mechanismMockup().getPublishedDescription();
        var expectedIps = discoveryFactory.ipDiscoverMockup().getPublicIps();

        assertThat(desc, is(notNullValue()));
        assertThat(desc.location.addresses, is(equalTo(expectedIps)),
            "Published addresses does not match the expected value");
    }
}