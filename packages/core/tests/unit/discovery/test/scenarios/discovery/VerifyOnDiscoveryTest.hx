package discovery.test.scenarios.discovery;

import discovery.domain.Search;
import discovery.test.common.mockups.SearchListenersMockup;
import discovery.test.common.mockups.KeyMockup;
import discovery.domain.Discovered;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.DiscoveryFactoryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class VerifyOnDiscoveryTest implements Scenario{
    @steps
    var steps: VerifyOnDiscoverySteps;

    @Test
    @scenario
    public function start_verifying_discovered_service_on_search(){
        given().a_discovery_instance();
        given().a_search_was_started();
        when().a_signed_service_is_found();
        then().the_discovered_provider_key_should_be_searched();
    }

    @Test
    @scenario
    public function start_verifying_discovered_service_on_locate(){
        given().a_discovery_instance();
        given().a_location_search_was_started();
        when().a_signed_service_is_found();
        then().the_discovered_provider_key_should_be_searched();
    }

    @Test
    @scenario
    public function verify_signature_once_key_found(){
        given().the_search_process_has_started();
        and().the_process_of_verification_started();
        when().the_provider_key_is_found();
        then().the_service_signature_should_be_verified_using_the_provider_key();
    }

    @Test
    @scenario
    public function notify_service_found_after_verification(){
        given().the_search_process_has_started();
        and().the_provider_key_was_found();
        given().there_is_a_listener_to_the_OnFound_event();
        when().the_verification_process_succeeds();
        then().the_listener_should_be_notified_about_the_discovered_service();
    }

    // error cases --------------------

    @Test
    @scenario
    public function should_not_notify_if_verification_fails(){
        given().the_search_process_has_started();
        and().the_provider_key_was_found();
        given().there_is_a_listener_to_the_OnFound_event();

        when().the_verification_process_fails();
        then().the_listener_should_not_be_notified_about_this_service();
    }

    @Test
    @scenario
    @Ignore("not implemented yet")
    public function notify_unauthenticated_service(){
        // Should have a way to notify unauthenticated services
    }
}

class VerifyOnDiscoverySteps extends Steps<VerifyOnDiscoverySteps>{
    var discovery:Discovery;
    var discoveryFactory:DiscoveryFactoryMockup;

    var foundService: Discovered;
    var search: Search;
    var listenersMockup: SearchListenersMockup;

    var searchKeyMockup: KeyMockup;

    public function new(){
        super();

        discoveryFactory = new DiscoveryFactoryMockup();
        listenersMockup  = new SearchListenersMockup();
    }

    function mechanismMockup() {
        return discoveryFactory.mechanismMockup();
    }

    function pkiMockup() {
        return discoveryFactory.pkiMockup();
    }

    // given ---------------------------------------------------

    @step
    public function the_search_process_has_started() {
        given().a_discovery_instance();
        given().a_search_was_started();
        when().a_signed_service_is_found();
    }

    @step
    public function the_process_of_verification_started() {}


    @step
    public function a_discovery_instance() {
        discovery = discoveryFactory.build();
    }

    @step
    public function a_search_was_started() {
        search = discovery.search({
            type: RandomGen.name()
        });
    }

    @step
    public function a_location_search_was_started() {
        search = discovery.locate(RandomGen.identifier());
    }

    @step
    public function there_is_a_listener_to_the_OnFound_event() {
        listenersMockup.registerInto(search);
    }

    @step
    public function the_provider_key_was_found() {
        when().the_provider_key_is_found();
    }

    // when  ---------------------------------------------------

    @step
    public function a_signed_service_is_found() {
        var announce = RandomGen.fullAnnouncement();
        foundService = RandomGen.crypto.signed(announce);

        assertThat(foundService.hasSignature(), is(true),
            "Missing service signature");
        assertThat(foundService.providerId(), is(notNullValue()),
           "Missing service provider id");

        mechanismMockup().notifyFound(foundService);
    }

    @step
    public function the_provider_key_is_found() {
        searchKeyMockup = pkiMockup().resolveSearchKey();
    }

    @step
    public function the_verification_process_succeeds() {
        searchKeyMockup.resolveVerificationTo(true);
    }

    @step
    public function the_verification_process_fails() {
        searchKeyMockup.resolveVerificationTo(false);
    }

    // then  ---------------------------------------------------

    @step
    public function the_discovered_provider_key_should_be_searched() {
        var fingerprint = foundService.providerId().fingerprint();
        pkiMockup().checkSearchKey(fingerprint);
    }

    @step
    public function the_service_signature_should_be_verified_using_the_provider_key() {
        searchKeyMockup.checkVerificationStarted();
        searchKeyMockup.checkObjectVerified(foundService.signed());
    }

    @step
    public
    function the_listener_should_be_notified_about_the_discovered_service() {
        listenersMockup.checkFound(foundService);
    }

    @step
    public function the_listener_should_not_be_notified_about_this_service() {
        listenersMockup.checkNotFound(foundService);
    }
}