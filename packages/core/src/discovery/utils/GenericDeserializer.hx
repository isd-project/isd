package discovery.utils;

import haxe.DynamicAccess;

class GenericDeserializer{
    public function new() {

    }

    public static function
        makeArrayAs<T>(value:Dynamic, transformer:(Dynamic)->T):Array<T>
    {
        if(value == null || !Std.isOfType(value, Array))
            return null;

        return (value:Array<Dynamic>).map(transformer);
    }

    public static function makeClassFrom<T>(type: Class<T>, data: Dynamic): T {
        if(data == null) return null;

        var out:T;

        try{
            out = Type.createInstance(type, []);
        }
        catch(e){
            out = Type.createEmptyInstance(type);
        }

        return setFieldsFrom(out, type, data);
    }

    static function setFieldsFrom<T>(obj:T, cl:Class<T>, data:Dynamic): T {
        if(data == null) return obj;


        var out:DynamicAccess<Dynamic> = (obj: Dynamic);
        var src:DynamicAccess<Dynamic> = data;

        var fields = Type.getInstanceFields(cl);

        for(key in fields){
            if(src.exists(key)){
                var value = src.get(key);

                out.set(key, value);
            }
        }

        return obj;
    }
}