package discovery.utils.time.scheduler;

import discovery.utils.Cancellable;
import discovery.utils.time.Duration.TimeUnit;
import discovery.utils.time.DefaultTimerLoop;
import discovery.utils.time.TimerLoop;
import discovery.utils.functional.Callback;

typedef ScheduledTask = {
    task: Callback,
    time: TimeUnit
}

class TaskScheduler {
    var tasks:Array<ScheduledTask> = [];

    var timer:TimerLoop;
    var pendingDelay:Cancellable;

    public function new(?timer:TimerLoop) {
        if(timer == null) timer = new DefaultTimerLoop();

        this.timer = timer;
    }

    public function schedule(task:Callback, delayTime:TimeUnit) {
        tasks.push({
            task: task,
            time: delayTime
        });

        if(tasks.length == 1){//only the current task
            scheduleNext();
        }
    }

    function scheduleNext() {
        if(tasks.length == 0) return;

        var next = tasks[0];

        pendingDelay = this.timer.delay(this.executeNext, next.time);
    }

    public function executeNext(): Bool {
        if(tasks.length == 0) return false;

        var t = tasks.shift();

        if(t != null && t.task != null){
            t.task();
        }

        if(pendingDelay != null){
            pendingDelay.cancel();
        }

        scheduleNext();

        return true;
    }
}