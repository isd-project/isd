package discovery.utils.time;

import haxe.extern.EitherType;
import discovery.utils.time.Duration.TimeUnit;

private typedef Number = EitherType<Int, Float>;

class TimeUnitTools {
    public static function millis(value:Number) {
        return timeUnit(value, Milliseconds);
    }
    public static function seconds(value:Number) {
        return timeUnit(value, Seconds);
    }
    public static function minutes(value:Number) {
        return timeUnit(value, Minutes);
    }
    public static function hours(value:Number) {
        return timeUnit(value, Hours);
    }
    public static function days(value:Number) {
        return timeUnit(value, Days);
    }
    public static function months(value:Number) {
        return timeUnit(value, Months);
    }
    public static function years(value:Number) {
        return timeUnit(value, Years);
    }

    public static function timeUnit(value: Number, unit:TimeUnit) {
        return Duration.of(value, unit);
    }
}