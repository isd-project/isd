package discovery.utils.time;


class TimeInterval {
    public var start(default, null):Date;
    public var end(default, null):Date;

    public function new(from: Date, to: Date) {
        if(to.getTime() < from.getTime()){
            var tmp = to;
            to = from;
            from = tmp;
        }

        this.start = from;
        this.end   = to;
    }

    public function duration() {
        return Duration.between(start, end);
    }
}