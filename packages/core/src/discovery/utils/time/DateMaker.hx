package discovery.utils.time;

class DateMaker{
    /**
        @param month from 1 to 12 (NOTE: differ from Date.makeUtc, which is from 0 to 11)
        @param day from 1 to 31
        @param hour from 0 to 23
        @param min from 0 to 59
        @param sec from 0 to 59
        @param millis added to the timestamp
    **/
    public static function
        fromValues(year:Int, month:Int=1, day:Int=1, hour:Int=0, min:Int=0, sec:Int=0, millis:Int=0): Date
    {
        var time = DateTools.makeUtc(year, month-1, day, hour, min, sec);

        time += millis;

        return Date.fromTime(time);
    }
}