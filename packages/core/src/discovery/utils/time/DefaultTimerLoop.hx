package discovery.utils.time;

import haxe.exceptions.NotImplementedException;
import discovery.utils.time.Duration.TimeUnit;
import discovery.utils.functional.Callback;
import haxe.Timer;

class DefaultTimerLoop implements TimerLoop{
    public function new() {

    }

    public static function makeTimer(callback: Callback, intervalMillis:Int){
        var timer = new Timer(intervalMillis);
        timer.run = function () {
            callback();
        };

        return timer;
    }

    public function repeat(event:() -> Void, intervalMs:Int):Cancellable {
        return new TimerCancellable(makeTimer(event, intervalMs));
    }

    public function delay(cb:Callback, time:TimeUnit): Cancellable{
        var millis:Int = cast time.toTime();

        return new TimerCancellable(Timer.delay(cb, millis));
    }
}

class TimeRepeater implements Cancellable{
    var timer:Timer;

    public function new(callback: ()->Void, intervalMs: Int) {
        timer = new Timer(intervalMs);
        timer.run = function () {
            callback();
        };
    }

    public function cancel() {
        this.timer.stop();
    }
}


class TimerCancellable implements Cancellable{
    var timer:Timer;

    public function new(timer: Timer) {
        this.timer = timer;
    }

    public function cancel() {
        this.timer.stop();
    }
}
