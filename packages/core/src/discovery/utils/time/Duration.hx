package discovery.utils.time;


typedef MillisecondsTime = Float;

enum abstract TimeUnit(MillisecondsTime) from MillisecondsTime{
    var Milliseconds = 1;
    var Seconds = 1000 * Milliseconds.toTime();
    var Minutes = 60   * Seconds.toTime();
    var Hours   = 60   * Minutes.toTime();
    var Days    = 24   * Hours.toTime();
    var Months  = (1   * Years.toTime()) / 12;
    var Years   = 365.25 * Days.toTime();

    @:to
    inline
    public function toTime(): MillisecondsTime{
        return this;
    }
}


abstract Duration(MillisecondsTime) from MillisecondsTime to MillisecondsTime
{

    public static function empty(): Duration {
        return 0;
    }


    public static function of(value:Float, unit: TimeUnit):Duration {
        return value * unit.toTime();
    }


    public static function between(begin:Date, end:Date):Duration {
        var period = Math.abs(end.getTime() - begin.getTime());

        return Duration.of(period, Milliseconds);
    }

    @:to
    public function toTime(): TimeUnit {
        return time();
    }

    public function time(): MillisecondsTime {
        return this;
    }

    public function timeInt(): Int {
        return Math.round(time());
    }

    //comparison

    public function equals(other: Duration, epsilon:Duration=0.001) {
        return Math.abs(time() - other.time()) < epsilon.time();
    }

    @:op(A==B)
    public function equalsOp(other:Duration) {
        return equals(other);
    }

    @:op(A<B)
    public function lessThan(other:Duration) {
        return time() < other.time();
    }

    @:op(A>B)
    public function greaterThan(other: Duration) {
        return other.lessThan(this);
    }

    @:op(A>=B)
    public function greaterOrEquals(other: Duration) {
        return greaterThan(other) || equals(other);
    }

    @:op(A<=B)
    public function lessOrEquals(other: Duration) {
        return lessThan(other) || equals(other);
    }

    // arithmetics ----------------------------------------

    public function plus(value:Float, unit: TimeUnit): Duration{
        return plusDuration(Duration.of(value, unit));
    }

    @:op(A+B)
    public function plusDuration(duration: Duration): Duration{
        return this + duration.time();
    }

    @:op(A+B)
    public function plusDate(date: Date): Date{
        return Date.fromTime(this + date.getTime());
    }

    public function minus(value:Float, unit: TimeUnit): Duration{
        return minusDuration(Duration.of(value, unit));
    }

    @:op(A-B)
    public function minusDuration(duration: Duration): Duration{
        return this - duration.time();
    }

    @:op(A*B)
    public function multipliedBy(scale: Float): Duration{
        return this * scale;
    }
}