package discovery.utils.time;

import discovery.utils.time.TimeInterval;

class TimeCount {
    var startTime:Date;

    public function new(?startTime:Date) {
        if(startTime == null){
            startTime = Date.now();
        }
        this.startTime = startTime;
    }

    @:deprecated('use measure')
    public function finish():TimeInterval {
        return measure();
    }

    public function measure():TimeInterval {
        return new TimeInterval(startTime, Date.now());
    }
}