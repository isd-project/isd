package discovery.utils.time;

import discovery.utils.time.Duration.TimeUnit;
import discovery.utils.functional.Callback;

interface TimerLoop {
    function repeat(event:()->Void, intervalMs:Int): Cancellable;
    function delay(event:Callback, time:TimeUnit): Cancellable;
}