package discovery.utils.filter;

class FilterTools {
    public static function includes<T>(filter:Filter<T>, value:T): Bool {
        return filter.filter(value) == Include;
    }

    public static function excludes<T>(filter:Filter<T>, value:T): Bool {
        return filter.filter(value) == Exclude;
    }
}