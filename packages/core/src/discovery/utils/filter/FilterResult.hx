package discovery.utils.filter;

enum FilterResult {
    Include;
    Exclude;
}