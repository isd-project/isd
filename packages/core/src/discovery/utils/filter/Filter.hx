package discovery.utils.filter;

interface Filter<T> {
    function filter(value: T): FilterResult;
}