package discovery.utils.filter;

interface FiltersContainer<T> extends Filter<T>{
    function addFilter(filter: Filter<T>): Void;
    function removeFilter(filter: Filter<T>): Void;

    function filter(value: T): FilterResult;
}