package discovery.utils.filter;

import discovery.utils.filter.Filter;
import discovery.utils.filter.FiltersContainer;
import discovery.utils.filter.FilterResult;

class FiltersContainerDefault<T> implements FiltersContainer<T> {

    public var filters(default, null): List<Filter<T>>;

    public function new() {
        filters = new List();
    }

    public function filter(value: T): FilterResult {
        for (filter in filters){
            if(filter.filter(value) == Exclude){
                return Exclude;
            }
        }

        return Include;
    }

    public function addFilter(filter:Filter<T>) {
        filters.add(filter);
    }

    public function removeFilter(filter:Filter<T>) {
        filters.remove(filter);
    }
}