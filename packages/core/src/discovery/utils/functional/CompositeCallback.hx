package discovery.utils.functional;


class CompositeCallback {
    var remainingCalls:Int;
    var onResult:Callback;

	public static function make(expectedCalls:Int, onResult:Callback){
        return new CompositeCallback(expectedCalls, onResult).callback();
    }

    public function new(expectedCalls:Int, onResult:Callback) {
        this.remainingCalls = expectedCalls;
        this.onResult = onResult;
    }

    public function callback() {
        return onCall;
    }

    function onCall() {
        remainingCalls--;

        if(remainingCalls <=0 && onResult != null){
            onResult();
        }
    }
}