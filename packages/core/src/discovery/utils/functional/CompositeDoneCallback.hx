package discovery.utils.functional;

import discovery.async.DoneCallback;


class CompositeDoneCallback {
    public static function make(expectedCalls:Int, onResult:DoneCallback):DoneCallback {
        return cast CompositeFunction.make({
            expectedCalls: expectedCalls,
            join: FailuresComposer.compose,
            onResult: onResult
        });
    }
}