package discovery.utils.functional;

typedef CompositeFunctionOptions<TIn, TOut> = {
    expectedCalls: Int,
    join: (Array<TIn>)->TOut,
    ?onResult: (TOut)->Void
};

class CompositeFunction<TIn, TOut>{

    public static function make<TIn, TOut>(args: CompositeFunctionOptions<TIn, TOut>):TIn->Void
    {
        var callable = new CompositeFunction(args);
        return callable.callable();
    }

    var args:CompositeFunctionOptions<TIn, TOut>;
    var values:Array<TIn>;

    public function new(args: CompositeFunctionOptions<TIn, TOut>) {
        this.args = args;
        this.values = [];
    }

    public function callable(): TIn->Void {
        return Reflect.makeVarArgs(onCall);
    }

    function onCall(args: Array<Dynamic>) {
        var value = args[0];
        values.push(value);

        if(values.length == expectedCalls()){
            var result = joinValues();
            notifyResult(result);
        }
    }

    function expectedCalls():Int {
        return this.args.expectedCalls;
    }

    function joinValues(): TOut {
        return this.args.join(values);
    }

    function notifyResult(result:TOut) {
        if(this.args.onResult != null){
            this.args.onResult(result);
        }
    }
}