package discovery.utils.functional;

import discovery.exceptions.composite.PartialFailureException;
import haxe.Exception;

class FailuresComposer {
    public static function compose(failures: Array<Null<Exception>>): Null<Exception>
    {
        var nonNullExceptions = failures.filter((e)->e != null);

        final total        = failures.length;
        final successCount = total - nonNullExceptions.length;

        if(nonNullExceptions.length == 0){
            return null;
        }

        if(successCount > 0){
            return new PartialFailureException(failures);
        }
        else if(total == 1){//only one exception
            return nonNullExceptions[0];
        }
        else{
            return new CompositeException(nonNullExceptions);
        }
    }
}

class CompositeException extends Exception {
    public var exceptions(default, null): Array<Exception>;

    public function new(exceptions: Array<Exception>, ?msg:String) {
        super(composeMessage(exceptions, msg));

        this.exceptions = exceptions;
    }

    static
    function composeMessage(exceptions:Array<Exception>, ?msg: String):String
    {
        if(msg != null) return msg;

        return 'These exceptions occurred: ${exceptions}';
    }
}