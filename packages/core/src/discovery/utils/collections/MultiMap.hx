package discovery.utils.collections;

import discovery.domain.operators.basic.Value;
import haxe.Constraints.IMap;

class MultiMap<K, V> {
    var map:IMap<K, Array<V>>;

    public function new() {
        map = new ComparableMap<K, Array<V>>();
    }

    public function push(key:K, value:V) {
        values(key).push(value);
    }

    public function items(key:K): Iterable<V> {
        return values(key);
    }

    function values(key:K) {
        var values = map.get(key);

        if(values == null){
            values = [];
            map.set(key, values);
        }

        return values;
    }
}