package discovery.utils.collections;

import haxe.Constraints.IMap;

@:forward(exists, clear, remove, get)
abstract SetAdapter<T>(IMap<T, T>)
{
    public function new<T>(map:IMap<T,T>) {
        this = cast map;
    }

    public function add(value: T): Bool {
        if(this.exists(value)){
            return false;
        }
        set(value);

        return true;
    }
    public function set(value: T) {
        this.set(value, value);
    }

    public function values():Iterator<T> {
        return this.keys();
    }

    public function isEmpty():Bool {
        return this.keys().hasNext() == false;
    }
}