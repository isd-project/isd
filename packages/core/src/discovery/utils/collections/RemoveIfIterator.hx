package discovery.utils.collections;

typedef Removable<T> = {
    function remove(item: T):Bool;
};

class RemoveIfIterator<T> {
    var matcher: (T)->Bool;
    var iterator: Iterator<T>;
    var collection: Removable<T>;

    var current:Null<T>;

    public function new<Collection:Removable<T>>(
        iterator:Iterator<T>, collection:Collection, matcher:(T)->Bool)
    {
        this.matcher = matcher;
        this.collection = collection;
        this.iterator = iterator;

        this.current = null;
    }

    public function hasNext(): Bool {
        if(current != null) return true;

        return filterNextValues();
    }

    public function next(): T {
        if(!hasNext()) return null;

        var nextValue = current;
        current = null;

        return nextValue;
    }

    function filterNextValues():Bool {
        while(iterator.hasNext()){
            var value = iterator.next();

            if(shouldRemove(value)){
                removeValue(value);
            }
            else{
                current = value;
                return true;
            }
        }

        return false;
    }

    function shouldRemove(value:T) {
        return this.matcher(value);
    }

    function removeValue(value:T) {
        this.collection.remove(value);
    }
}
