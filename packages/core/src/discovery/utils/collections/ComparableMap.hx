package discovery.utils.collections;

import discovery.utils.comparator.GenericComparator;
import haxe.ds.BalancedTree;
import haxe.Constraints.IMap;

typedef ComparatorFunction<K> = (K, K)->Int;

typedef Comparable<T>= {
    function compare(other: T):Int;
};

@:callable
abstract ComparableFunction<T>(ComparatorFunction<T>)
    from ComparatorFunction<T>
    to   ComparatorFunction<T>
{
    public function new(?comparator: ComparatorFunction<T>) {
        if(comparator == null){
            comparator = Reflect.compare;
        }

        this = comparator;
    }

    @:from
    public static function ofComparables<K:Comparable<K>>(?keyClass:Class<K>)
        : ComparableFunction<K>
    {
        return (v1:K, v2:K)->{
            return v1.compare(v2);
        };
    }
}

@:forward
@:transitive
abstract ComparableMap<K, V>(ComparableTreeMap<K, V>)
    to IMap<K, V>
{
    public function new(?comparator: ComparatorFunction<K>){
        if(comparator == null){
            comparator = GenericComparator.compare;
        }

        this = new ComparableTreeMap(comparator);
    }

    public static function ofComparables<K:Comparable<K>, V>(?keyClass:Class<K>){
        return new ComparableMap(ComparableFunction.ofComparables(keyClass));
    }
}

class ComparableTreeMap<K, V> extends BalancedTree<K, V>{
    var comparator:ComparatorFunction<K>;

    public function new(comparator: ComparatorFunction<K>) {
        super();
        this.comparator = comparator;
    }

    override function compare(k1:K, k2:K):Int {
        return comparator(k1, k2);
    }

    public function isEmpty() {
        return !this.keys().hasNext();
    }
}