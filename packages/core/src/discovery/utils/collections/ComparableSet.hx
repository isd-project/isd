package discovery.utils.collections;

import discovery.utils.collections.ComparableMap.ComparableFunction;


@:forward()
abstract ComparableSet<T>(SetAdapter<T>) {
    public function new(func:ComparableFunction<T>) {
        this = new SetAdapter<T>(new ComparableMap(func));
    }
}