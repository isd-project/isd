package discovery.utils;

import haxe.EnumTools.EnumValueTools;
import discovery.utils.collections.ComparableMap;
import haxe.io.Bytes;
import discovery.utils.comparator.GenericComparator;
import haxe.DynamicAccess;
import discovery.utils.traversal.ObjectTransformation;

class GenericSerializer{
    public static function doSerialization<T>(value: T): Dynamic{
        return new GenericSerializer().serialize(value);
    }

    var transformations = new ObjectTransformation();

    public function new()
    {}

    public function serialize<T>(value: T): Dynamic {
        return new Serialization(value, transformations).serialize();
    }

    public function
        transform<Tin, Tout>(type:Dynamic, transformation:Tin->Tout)
    {
        transformations.transform(type, transformation);
    }
}

class Serialization{
    var transform:ObjectTransformation;

    var notSerializableTypes:Array<Class<Dynamic>> = [Date];
    var visitedObjs:ComparableMap<Dynamic, Dynamic>;

    var value:Dynamic;

    public function new(value:Dynamic, ?transformations:ObjectTransformation) {
        this.value = value;

        transform = new ObjectTransformation();
        transform.include(transformations);
        transform.transform(Dynamic, serializeObj);

        visitedObjs = new ComparableMap(GenericComparator.compare);
    }

    public function serialize(): Dynamic {
        return transform.apply(value);
    }

    function serializeObj(obj: Dynamic): Dynamic{
        if(isEnum(obj)){
            return EnumValueTools.getName(obj);
        }

        if(!Reflect.isObject(obj)
            || Std.isOfType(obj, String)
            || Std.isOfType(obj, Bytes)
            || notSerializableTypes.contains(Type.getClass(obj)))
        {
            return obj;
        }

        if(Std.isOfType(obj, Array)){
            return serializeArray(obj);
        }

        if(visitedObjs.exists(obj)){
            return visitedObjs.get(obj);
        }

        var cpy = copy(obj);

        visitedObjs.set(obj, cpy);
        visitedObjs.set(cpy, cpy);

        return cpy;
    }

    function serializeArray(array:Array<Dynamic>):Dynamic {
        return [for(obj in array) serializeObj(obj)];
    }

    function copy(obj:Dynamic) {
        if(Std.isOfType(obj, Array)){
            return (obj: Array<Dynamic>).map(serializeObj);
        }

        return shallowCopy(obj);
    }

    function shallowCopy(obj:Dynamic): Dynamic {
        return (obj: DynamicAccess<Dynamic>).copy();
    }

    function isEnum(value:Dynamic) {
        switch (Type.typeof(value)){
            case TEnum(e): return true;
            default: return false;
        }
    }
}