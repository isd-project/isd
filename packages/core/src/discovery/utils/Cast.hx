package discovery.utils;

class Cast {
    public static function castIfIs<T>(value: Dynamic, type: Dynamic): T {
        return if(isOfType(value, type)) value else null;
    }
    public static function isOfType(value: Dynamic, type: Dynamic): Bool {
        return Std.isOfType(value, type);
    }
}