package discovery.utils.events;

typedef EventEmitterBuildOptions = {
    /**
        Listeners will be called only once
    **/
    ?once: Bool
};

private typedef BuildOptions = EventEmitterBuildOptions;

interface EventEmitterFactory {
    function eventEmitter<T>(?options: BuildOptions): EventEmitter<T>;
}