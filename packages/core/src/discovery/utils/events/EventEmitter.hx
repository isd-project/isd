package discovery.utils.events;

import discovery.domain.Event;

interface EventEmitter<T> extends Event<T> {
    function notify(value: T): Void;
}