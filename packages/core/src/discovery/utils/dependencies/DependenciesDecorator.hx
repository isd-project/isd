package discovery.utils.dependencies;

import haxe.DynamicAccess;

using discovery.utils.dependencies.DependenciesVerifications;


class DependenciesDecorator {
    public static function decorate(dep:Dynamic):Dynamic {
        return new DependenciesDecorator(dep).decorated();
    }

    public static function requireAllDependencies<T:{}>(deps: T) {
        deps.requireAll();
    }

    var dependenciesObject: DynamicAccess<Dynamic>;

    public function new(dependenciesObject: Dynamic) {
        this.dependenciesObject = copy(dependenciesObject);

        applyDecoration();
    }

    function copy(obj:DynamicAccess<Dynamic>):Dynamic {
        return obj.copy();
    }

    function applyDecoration() {
        for(field in Reflect.fields(this.dependenciesObject)){
            addRequireFunction(field);
        }

        this.dependenciesObject.set('require', this.require);
        this.dependenciesObject.set('requireAll', this.requireAll);
    }

    function addRequireFunction(field:String) {
        var requireName =
            'require'
            + field.charAt(0).toUpperCase()
            + field.substr(1);

        this.dependenciesObject.set(requireName, this.require.bind(field));
    }

    public function decorated(): Dynamic {
        return dependenciesObject;
    }

    public function require(fieldName: String) {
        return dependenciesObject.requireField(fieldName);
    }

    public function requireAll(){
        dependenciesObject.requireAll();
    }
}