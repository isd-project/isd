package discovery.utils.dependencies;

import discovery.utils.dependencies.classmap.ClassInfo;
import discovery.utils.dependencies.classmap.ClassMapType;


@:forward
@:forward.new
abstract ClassMap<T>(ClassMapType<T>) from ClassMapType<T>{
    @:from
    public static function make<T>(deps: Map<ClassInfo, T>): ClassMap<T>
    {
        var map = new ClassMapType();

        for(type=>value in deps){
            map.set(type, value);
        }

        return map;
    }

    @:from
    public static function makeAny(deps: Map<ClassInfo, Any>): ClassMap<Any>{
        return make(deps);
    }
}
