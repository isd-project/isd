package discovery.utils.dependencies;

import haxe.Exception;

class MissingDependencyException extends Exception
{
    public var missingType(default, null):Dynamic;

    public static function requireValue<T>(value: T, fieldName:String) {
        if(value != null) return;

        throw new MissingDependencyException(null, 'Missing dependency: ${fieldName}');
    }

    public
    function new(missingType: Dynamic, ?message:String, ?previous:Exception)
    {
        super(makeMessage(message, missingType), previous);

        this.missingType = missingType;
    }

    static function makeMessage(message:Null<String>, missingType: Dynamic):String
    {
        return if(message != null) message else 'Missing dependency of type: ${missingType}';
    }
}