package discovery.utils.dependencies;

import discovery.utils.dependencies.ClassMap;


class DependenciesMap implements Dependencies{

    var classMap:ClassMap<Any>;

    public static function make(dependenciesMap: ClassMap<Any>) {
        return new DependenciesMap(dependenciesMap);
    }

    public function new(dependenciesMap: ClassMap<Any>) {
        this.classMap = dependenciesMap.copy();
    }

    public function provide<T>(type:Class<T>, value:T) {
        this.classMap.set(type, value);
    }

    public function get<T>(type:Class<T>, ?defaultValue: T):T {
        var value =  this.classMap.get(type);

        return if(value != null) value else defaultValue;
    }

    public function require<T>(type:Class<T>):T {
        var dep = this.get(type);

        if(dep != null){
            return dep;
        }
        else {
            throw new MissingDependencyException(type);
        }
    }
}