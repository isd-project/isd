package discovery.utils.dependencies.classmap;

class MapValuesIterator<V>{
    var iterator:KeyValueIterator<Any,V>;

    public function new<K>(iterator: KeyValueIterator<K,V>) {
        this.iterator = cast iterator;
    }

    public function hasNext(): Bool {
        return iterator.hasNext();
    }

    public function next(): V{
        return iterator.next().value;
    }
}