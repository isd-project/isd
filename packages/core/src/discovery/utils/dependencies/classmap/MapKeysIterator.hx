package discovery.utils.dependencies.classmap;

class MapKeysIterator<K>{
    var iterator:KeyValueIterator<K,Any>;

    public function new<V>(iterator: KeyValueIterator<K,V>) {
        this.iterator = cast iterator;
    }

    public function hasNext(): Bool {
        return iterator.hasNext();
    }

    public function next(): K{
        return iterator.next().key;
    }
}