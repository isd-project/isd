package discovery.utils.dependencies.classmap;


@:forward
abstract ClassInfo(ClassInfoType) from ClassInfoType to ClassInfoType
{
    @:from
    public static function fromClass<T>(classObj: Class<T>): ClassInfo {
        return new ClassInfoType(classObj);
    }

    @:to
    public function toClassObject<T>(){
        return this.classInfo;
    }
}

@:structInit
class ClassInfoType{
    public var classInfo: Class<Dynamic>;

    public function new(classInfo: Class<Dynamic>) {
        this.classInfo = classInfo;
    }

    public function name(): String{
        return Type.getClassName(classInfo);
    }
}