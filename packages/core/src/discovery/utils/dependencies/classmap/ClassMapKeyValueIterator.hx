package discovery.utils.dependencies.classmap;

private typedef KeyValue<K, V> = {
    key: K,
    value: V
};

class ClassMapKeyValueIterator<T>{
    var iterator:Iterator<ClassData<T>>;

    public function new(iterator: Iterator<ClassData<T>>) {
        this.iterator = iterator;
    }

    public function hasNext(): Bool {
        return iterator.hasNext();
    }

    public function next(): KeyValue<ClassInfo, T>{
        var classData = iterator.next();

        return if(classData == null) {key: null, value: null}
            else {
                key: classData.classInfo,
                value: classData.data
            };
    }
}