package discovery.utils.dependencies.classmap;

import haxe.Constraints.IMap;


class ClassMapType<T> implements IMap<ClassInfo, T>{
    var map: Map<String, ClassData<T>>;

    public function new() {
        map = new Map();
    }

    function key(classInfo: ClassInfo): String {
        return if(classInfo == null) null else classInfo.name();
    }

    function data(classInfo: ClassInfo, data: T): ClassData<T> {
        return { classInfo: classInfo, data: data};
    }

    public function get(k:ClassInfo):Null<T> {
        var classData = map.get(key(k));
        return if(classData == null) null else classData.data;
    }

    public function set(k:ClassInfo, v:T) {
        map.set(key(k), data(k, v));
    }

    public function exists(k:ClassInfo):Bool {
        return map.exists(key(k));
    }

    public function remove(k:ClassInfo):Bool {
        return map.remove(key(k));
    }

    public function keys():Iterator<ClassInfo> {
        return new MapKeysIterator(keyValueIterator());
    }

    public function iterator():Iterator<T> {
        return new MapValuesIterator(keyValueIterator());
    }

    public function keyValueIterator():KeyValueIterator<ClassInfo, T> {
        return new ClassMapKeyValueIterator(map.iterator());
    }

    public function copy():ClassMapType<T> {
        var copyMap = new ClassMapType<T>();

        for(key=>value in keyValueIterator()){
            copyMap.set(key, value);
        }

        return copyMap;
    }

    public function toString():String {
        var strBuf = new StringBuf();
        strBuf.add('ClassMap: [\n');

        for(key=>value in keyValueIterator()){
            strBuf.add('\tClass<${key.name()}> => ${value}\n');
        }

        strBuf.add(']');
        return strBuf.toString();
    }

    public function clear() {
        this.map.clear();
    }
}