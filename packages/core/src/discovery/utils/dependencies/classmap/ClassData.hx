package discovery.utils.dependencies.classmap;

typedef ClassData<T> = {
    classInfo: ClassInfo,
    data: T
};