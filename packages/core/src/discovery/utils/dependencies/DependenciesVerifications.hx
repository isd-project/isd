package discovery.utils.dependencies;

class DependenciesVerifications {
    public static function requireAll(deps: Dynamic) {
        for(field in Reflect.fields(deps)){
            if(isDependencyField(deps, field)){
                requireField(deps, field);
            }
        }
    }

    static function isDependencyField(deps:Dynamic, field:String) {
        return !Reflect.isFunction(getProperty(deps, field));
    }

    static function getProperty(obj:Dynamic, field:String):Dynamic {
        return Reflect.getProperty(obj, field);
    }

    public static function requireField(deps:Dynamic, field:String) {
        var value = getProperty(deps, field);

        MissingDependencyException.requireValue(value, field);

        return value;
    }
}