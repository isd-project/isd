package discovery.utils.dependencies;


interface Dependencies {
    function get<T>    (type:Class<T>, ?defaultValue:T):T;
    function require<T>(type:Class<T>):T;
    function provide<T>(type:Class<T>, value: T): Void;
}