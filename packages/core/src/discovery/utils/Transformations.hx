package discovery.utils;

import haxe.Constraints.Function;

class Transformations {
    var transformations:TypeMap<Function>;

    public function new() {
        transformations = new TypeMap();
    }

    public function keyValueIterator(): KeyValueIterator<Dynamic, Function> {
        return transformations.keyValueIterator();
    }

    public function include(other:Transformations) {
        if(other == null) return;

        for(type=>transform in other.keyValueIterator()){
            this.transformations.set(type, transform);
        }
    }

    public function set<Tin, Tout>(type:Dynamic, transformation:(Tin)->Tout)
    {
        transformations.set(type, transformation);
    }

    public function transform<T>(value: T){
        //Only applies transformation to exact type, maybe try
        //matching also superclasses of current type
        var transformation = transformations.getFromTypeOf(value);

        if(transformation == null){
            transformation = transformations.get(Dynamic);
        }

        if(transformation != null){
            return transformation(value);
        }

        return value;
    }
}