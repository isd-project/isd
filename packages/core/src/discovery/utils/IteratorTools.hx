package discovery.utils;

import haxe.extern.EitherType;

typedef IterX<T> = EitherType<Iterator<T>, Iterable<T>>;

abstract IterType<T>(Iterator<T>)
    to Iterator<T>
{
    public function new(iterator:Iterator<T>){
        this = iterator;
    }

    @:from
    public static function fromIterator<T, I:Iterator<T>>(iterator: I): IterType<T>
    {
        return new IterType(iterator);
    }

    @:from
    public static function fromIterable<T, I:Iterable<T>>(iterable: I): IterType<T>
    {
        if(iterable == null) return null;

        return iterable.iterator();
    }


    @:from
    public static function fromIterX<T>(iter: IterX<T>): IterType<T>
    {
        if(iter == null) return null;

        var iterDyn:Dynamic = iter;

        if(Reflect.isFunction(iterDyn.iterator)){//should be iterable
            return fromIterable(iter);
        }
        else if(Reflect.isFunction(iterDyn.hasNext)
            && Reflect.isFunction(iterDyn.next)) //should be iterator
        {
            return fromIterator(iter);
        }

        return null;
    }
}



class IteratorTools {
    public static function toArray<T, I:IterX<T>>(iter: I): Array<T> {
        if(iter == null) return null;

        var iterator:IterType<T> = iter;

        return [for(v in iterator) v];
    }

    public static function forEach<T>(iterable:Iterable<T>, cb:(T)->Void) {
        for(item in iterable){
            cb(item);
        }
    }

    public static function
        findWhere<T, I:Iterable<T>>(collection:I, matcher:(T)->Bool):T
    {
        for(item in collection){
            if(matcher(item)) return item;
        }

        return null;
    }
}