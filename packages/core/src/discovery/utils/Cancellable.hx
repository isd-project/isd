package discovery.utils;

interface Cancellable {
    function cancel():Void;
}