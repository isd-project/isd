package discovery.utils;

class TypeMap<T>{
    var map:Map<Dynamic, T>;

    public function new() {
        map = new Map<Dynamic, T>();
    }

    public function keyValueIterator(): KeyValueIterator<Dynamic, T> {
        return map.keyValueIterator();
    }

    public function getFromTypeOf<X>(value: X): T{
        return get(typeOf(value));
    }

    public function get(type: Dynamic): T{
        return map.get(type);
    }

    public function setFromTypeOf<X>(obj: X, value: T){
        set(typeOf(obj), value);
    }

    public function set(type: Dynamic, value: T){
        map.set(type, value);
    }


    public function typeOf<X>(value: X): Dynamic{
        return switch (Type.typeof(value)){
            case TBool    : Bool;
            case TInt     : Int;
            case TFloat   : Float;
            case TClass(c): c;
            case TEnum(e) : e;

            //TO-DO: handle anonymous objects and functions
            case TObject  : null;
            case TFunction: null;
            case TNull    : null;
            case TUnknown : null;
        };
    }
}