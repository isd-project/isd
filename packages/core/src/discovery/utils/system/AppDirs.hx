/**
    This module is based on python AppDirs library (https://github.com/ActiveState/appdirs).
    So below is reproduced their copyright notice and (MIT) license:

    Copyright (c) 2010 ActiveState Software Inc.
    Copyright (c) 2013 Eddy Petrișor

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

package discovery.utils.system;

import sys.FileSystem;
import haxe.io.Path;

using StringTools;


class AppDirs {
    var appDirs:SystemAppDirs = null;

    var appname:String;
    var appauthor:String;
    var version:String;
    var roaming:Bool;
    var multipath:Bool;

    /**
        @param "appname" is the name of application.
            If None, just the system directories are returned.
        @param "appauthor" (only used on Windows) is the name of the
            appauthor or distributing body for this application. Typically
            it is the owning company name. This falls back to appname.
            Use an empty string "", to disable it.
        @param "version" is an optional version path element to append to the
            path. You might want to use this if you want multiple versions
            of your app to be able to run independently. If used, this
            would typically be "<major>.<minor>".
            Only applied when appname is present.
        @param "roaming" (boolean, default False) can be set True to use the Windows
            roaming appdata directory. That means that for users on a Windows
            network setup for roaming profiles, this user data will be
            sync'd on login. See
            <http://technet.microsoft.com/en-us/library/cc766489(WS.10).aspx>
            for a discussion of issues.
    **/
    public function new(
        ?appname:String, ?appauthor:String, ?version:String,
        roaming:Bool=false, multipath:Bool=false)
    {
        this.appname = appname;
        this.appauthor = appauthor;
        this.version = version;
        this.roaming = roaming;
        this.multipath = multipath;

        appDirs = switch(Sys.systemName()){
            case "Windows": new WindowsAppDirs();
            case "Mac"    : new MacAppDirs();
            default       : new LinuxAppDirs();
        };
    }

    /**
        Return full path to the user-specific data dir for this application.

    Typical user data directories are:
        Mac OS X:               ~/Library/Application Support/<AppName>
        Unix:                   ~/.local/share/<AppName>    # or in $XDG_DATA_HOME, if defined
        Win XP (not roaming):   C:\Documents and Settings\<username>\Application Data\<AppAuthor>\<AppName>
        Win XP (roaming):       C:\Documents and Settings\<username>\Local Settings\Application Data\<AppAuthor>\<AppName>
        Win 7  (not roaming):   C:\Users\<username>\AppData\Local\<AppAuthor>\<AppName>
        Win 7  (roaming):       C:\Users\<username>\AppData\Roaming\<AppAuthor>\<AppName>
    For Unix, we follow the XDG spec and support $XDG_DATA_HOME.
    That means, by default "~/.local/share/<AppName>".
    **/
    public function userDataDir() {
        var path:String = this.appDirs.getUserDataDir(
            appname, appauthor, roaming
        );

        if (appname != null && version != null){
            path = Path.join([path, version]);
        }

        return path;
    }

    public static function user_data_dir(
        ?appname:String, ?appauthor:String, ?version:String, ?roaming:Bool)
    {
        return new AppDirs(appname, appauthor, version, roaming).userDataDir();
    }
}

private class AppDirsHelper {
    public static function expanduser(path:String, home:String): String {
        //Note: does not support '~user' pattern (yet)
        if(home == null || !path.startsWith('~')){
            return path;
        }

        var tildeRegex = ~/~/;
        return tildeRegex.replace(path, home);
    }
}

interface SystemAppDirs{
    function getUserDataDir(?appname:String, ?appauthor:String, ?roaming:Bool): String;
    function getHome():String;
}

class WindowsAppDirs implements SystemAppDirs{
    public function new() {}

    public function getUserDataDir(?appname:String, ?appauthor:String, ?roaming:Bool):String {
        if (appauthor == null){
            appauthor = appname;
        }
        var csidl_name = if(roaming) "CSIDL_APPDATA";
                        else        "CSIDL_LOCAL_APPDATA";
        var path = FileSystem.absolutePath(_get_win_folder(csidl_name));

        if (appname != null){
            if(appauthor != ''){
                path = Path.join([path, appauthor, appname]);
            }
            else{
                path = Path.join([path, appname]);
            }
        }

        return path;
    }

    /**Expand ~ and ~user constructs.
    If user or $HOME is unknown, do nothing.**/
    public function expanduser(path:String){
        if (!path.startsWith('~')){
            return path;
        }

        return AppDirsHelper.expanduser(path, getHome());
    }

    public function getHome(){
        var userprofile = Sys.getEnv('USERPROFILE');
        var homepath    = Sys.getEnv('HOMEPATH');
        var drive       = Sys.getEnv('HOMEDRIVE');

        if(userprofile != null){
            return userprofile;
        }
        else if(homepath != null){
            if(drive != null){
                return Path.join([drive, homepath]);
            }

            return homepath;
        }

        return null;
    }

    /**
        get windows folder from environment variables (python AppDirs, has other possibilities, like using registries).
    **/
    function _get_win_folder(csidl_name:String): String {
        var env_var_name = [
            "CSIDL_APPDATA" => "APPDATA",
            "CSIDL_COMMON_APPDATA" => "ALLUSERSPROFILE",
            "CSIDL_LOCAL_APPDATA"=> "LOCALAPPDATA",
        ][csidl_name];

        return Sys.getEnv(env_var_name);
    }

}

class PosixAppDirs implements SystemAppDirs {
    public function getUserDataDir(?appname:String, ?appauthor:String, ?roaming:Bool):String {
        throw new haxe.exceptions.NotImplementedException();
    }

    public function expandUser(path:String): String {
        if (path == null || !path.startsWith('~')){
            return path;
        }

        return AppDirsHelper.expanduser(path, getHome());
    }

    public function getHome() {
        return Sys.getEnv('HOME');
    }
}

class MacAppDirs extends PosixAppDirs{
    public function new() {}

    override public function getUserDataDir(
        ?appname:String, ?appauthor:String, ?roaming:Bool):String
    {
        var path = expandUser('~/Library/Application Support/');
        if (appname != null){
            path = Path.join([path, appname]);
        }

        return path;
    }
}

class LinuxAppDirs extends PosixAppDirs{
    public function new() {}

    override public function getUserDataDir(?appname:String, ?appauthor:String, ?roaming:Bool):String
    {
        var path = Sys.getEnv('XDG_DATA_HOME');

        if(path == null){
            path = expandUser("~/.local/share");
        }

        if (appname != null){
            path = Path.join([path, appname]);
        }
        return path;
    }
}