package discovery.utils.traversal;

class SortedIterable<T> {
    var items:Array<T>;

    public function new(iterable:Iterable<T>, comparator:(T,T)->Int) {
        items = [for (item in iterable) item];
        items.sort(comparator);
    }

    public function iterator(): Iterator<T> {
        return items.iterator();
    }
}