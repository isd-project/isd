package discovery.utils.traversal;

import discovery.utils.Transformations;
import discovery.format.Binary;
import discovery.format.Binary.BinaryData;
import discovery.utils.traversal.SortedKeyIterator.SortedIterable;
import haxe.io.Bytes;
import haxe.DynamicAccess;


class ObjectTraversal {
    var visitor:ObjectVisitor;
    public var transformations(default, null):Transformations;

    public var storeTransformations(default, default):Bool = false;


    public static function doTraversal(visitor:ObjectVisitor, object:Dynamic) {
        new ObjectTraversal(visitor).traverse(object);
    }

    public function new(visitor: ObjectVisitor) {
        this.visitor = visitor;
        transformations = new Transformations();
    }



    public function include(transformations:Transformations) {
        this.transformations.include(transformations);
    }

    public function transform<Tin, Tout>(
        type:Dynamic, transformation:(Tin)->Tout)
    {
        transformations.set(type, transformation);
    }

    public function traverse(object:DynamicAccess<Dynamic>){
        var iterable = new SortedIterable<Dynamic>(object.keys(),
                            Comparator.compareStrings);

        for (key in iterable){
            var value = object.get(key);
            var newvalue = visitValue(key, value);

            if(storeTransformations && valueChanged(value, newvalue))
            {
                object.set(key, newvalue);
            }
        }
    }

    function valueChanged(value:Dynamic, newvalue:Dynamic) {
        // Check also type, because some types (like enums) may compare to
        // others (like strings) as equals
        return Type.typeof(value) != Type.typeof(newvalue)
            || value != newvalue;
    }

    function visitValue(key:String, value:Dynamic): Dynamic {
        value = applyTransformation(value);

        if(visitor.onValue(key, value) == Skip){
            return value;
        }

        switch (Type.typeof(value)){
            case TNull    : visitor.onNull(key);
            case TBool    : visitor.onBool(key, value);
            case TInt     : visitor.onInt(key, value);
            case TFloat   : visitor.onFloat(key, value);
            case TObject  : visitObject(key, value);
            case TClass(c): visitClass(key, value, c);
            case TEnum(e) : visitor.onEnum(key, value, e);

            case TFunction:
            case TUnknown:
        };

        return value;
    }

    function visitArray(key:String, array:Array<Any>) {
        visitor.onStartArray(key, array);

        for(i in 0...array.length){
            var value = array[i];
            var newvalue = visitValue(null, value);

            if(storeTransformations && value != newvalue){
                array[i] = newvalue;
            }
        }

        visitor.onEndArray(key, array);
    }

    function visitClass(key:String, value:Dynamic, c:Class<Any>) {
        switch (c){
            case String: visitor.onString(key, value);
            case Array : visitArray(key, value);
            case Bytes : visitor.onBytes(key, value);
            case BinaryData: visitor.onBytes(key, (value:Binary).toBytes());
            default    : visitObject(key, value, c);
        }
    }


    function visitObject(key:String, value:Dynamic, ?classObj: Class<Any>) {
        visitor.onStartObject(key, value, classObj);
        traverse(value);
        visitor.onEndObject(key, value, classObj);
    }

    function applyTransformation(value:Dynamic):Dynamic {
        return transformations.transform(value);
    }
}