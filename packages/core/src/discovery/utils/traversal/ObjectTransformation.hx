package discovery.utils.traversal;

import discovery.utils.traversal.ObjectVisitor.VisitStatus;
import haxe.DynamicAccess;

class ObjectTransformation {
    var visitor:ObjectTransformationVisitor;
    public var traversal(default, null):ObjectTraversal;

    public function new() {
        visitor = new ObjectTransformationVisitor();
        traversal = new ObjectTraversal(visitor);
        traversal.storeTransformations = true;
    }

    public static function doTransformation<Tin, Tout>(
        type: Dynamic, transformation:Tin->Tout)
    {
        return new ObjectTransformation().transform(type, transformation);
    }


    public function include(other:Null<ObjectTransformation>) {
        if(other == null) return;

        this.traversal.include(other.traversal.transformations);
    }

    public function transform<Tin, Tout>(
        type:Dynamic, transformation:Tin->Tout)
    {
        traversal.transform(type, transformation);
        return this;
    }

    public function onValue(
        callback: (String, Dynamic)->VisitStatus): ObjectTransformation
    {
        visitor.onValueCallback = callback;
        return this;
    }

    public function apply(object:DynamicAccess<Dynamic>) {
        traversal.traverse(object);

        return object;
    }
}

class ObjectTransformationVisitor extends BaseObjectVisitor{
    public var onValueCallback:(String, Dynamic)->VisitStatus;

    override function onValue(key:String, value:Dynamic):VisitStatus {
        if(onValueCallback != null){
            return onValueCallback(key, value);
        }

        return Continue;
    }
}