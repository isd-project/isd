package discovery.utils.traversal;

import discovery.utils.traversal.ObjectVisitor.VisitStatus;
import haxe.io.Bytes;

class BaseObjectVisitor implements ObjectVisitor{
    public function new() {}

    public function onValue(key:String, value:Dynamic):VisitStatus {
        return Continue;
    }

    public function onNull(key:String) {}

    public function onInt(key:String, value:Int) {}

    public function onFloat(key:String, value:Float) {}

    public function onBool(key:String, value:Bool) {}

    public function onString(key:String, value:String) {}

    public function onBytes(key:String, value:Bytes) {}

    public function onEnum(key:String, e:EnumValue, enumType:Enum<Any>) {}

    public function onStartObject(key:String, value:Dynamic, ?classObj:Class<Any>) {}

    public function onEndObject(key:String, value:Dynamic, ?classObj:Class<Any>) {}

    public function onStartArray(key:String, value:Array<Any>) {}

    public function onEndArray(key:String, value:Array<Any>) {}

}