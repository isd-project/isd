package discovery.utils.traversal;

import haxe.DynamicAccess;
import haxe.iterators.DynamicAccessKeyValueIterator;


/**
    This Key/Value iterator can be used to iterate over `haxe.DynamicAccess`, but the keys are ensured to be sorted.
**/
class SortedDynamicKeyValueIterator<T> extends DynamicAccessKeyValueIterator<T>{
    public inline function new(access:DynamicAccess<T>) {
        super(access);

        keys.sort(Comparator.compareStrings);
    }
}
