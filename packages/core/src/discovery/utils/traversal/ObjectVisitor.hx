package discovery.utils.traversal;

import haxe.io.Bytes;

enum VisitStatus {
    Continue;
    Skip;
}


interface ObjectVisitor {
    function onValue(key:String, value: Dynamic): VisitStatus;

    function onNull  (key:String):Void;
    function onInt   (key:String, value:Int):Void;
    function onFloat (key:String, value:Float):Void;
    function onBool  (key:String, value:Bool):Void;
    function onString(key:String, value:String):Void;
    function onBytes (key:String, value:Bytes):Void;

    function onEnum  (key:String, e:EnumValue, enumType:Enum<Any>):Void;

    function onStartObject(key:String, value:Dynamic, ?classObj:Class<Any>):Void;
    function onEndObject  (key:String, value:Dynamic, ?classObj:Class<Any>):Void;

    function onStartArray(key:String, value:Array<Any>):Void;
    function onEndArray  (key:String, value:Array<Any>):Void;
}