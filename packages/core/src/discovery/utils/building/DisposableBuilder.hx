package discovery.utils.building;

class DisposableBuilder<T> {
    var builder: ()->T;
    var disposer: (T)->Void;
    var items: Array<T>=[];

    public function new<TImpl:T>(builder: ()->TImpl, disposer: (T)->Void) {
        this.builder = builder;
        this.disposer = disposer;
    }

    public function build(): T {
        var item = builder();

        items.push(item);

        return item;
    }

    public function disposeAll() {
        var toDispose = items;
        items = [];

        for(item in toDispose){
            disposer(item);
        }
    }
}