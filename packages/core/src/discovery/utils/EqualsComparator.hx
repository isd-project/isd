package discovery.utils;

import discovery.utils.comparator.GenericComparator;


class EqualsComparator extends GenericComparator{
    public static function equals<T>(a: T, b: T): Bool {
        return new EqualsComparator().areEquals(a, b);
    }

    public function areEquals<T>(a: T, b: T): Bool {
        return compareBoth(a, b) == 0;
    }
}