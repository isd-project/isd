package discovery.utils;

import discovery.format.Binary;
import haxe.io.Bytes;

class Comparator {
    public static function compareStrings(a: String, b:String): Int {
        var commonLength = Std.int(Math.min(a.length, b.length));

        for (i in 0...commonLength){
            var cmp = compareInt(a.charCodeAt(i), b.charCodeAt(i));

            if(cmp != 0){
                return cmp;
            }
        }

        return compareInt(a.length, b.length);
    }

    public static function compareInt(a: Int, b: Int) {
        if(a < b){
            return -1;
        }
        else if(a > b){
            return 1;
        }
        else{
            return 0;
        }
    }

    public static function compareNull<T>(v1: Null<T>, v2: Null<T>): Null<Int> {
        if(v1 == v2) return 0;
        if(v1 == null) return -1;
        if(v2 == null) return 1;

        return null;
    }

    public static function compareBinary(bin1:Binary, bin2:Binary):Null<Int> {
        var cmp = compareNull(bin1, bin2);

        if(cmp == null) cmp = bin1.compare(bin2);

        return cmp;
    }
}