package discovery.utils.comparator;

import haxe.ds.ObjectMap;

class ComparisonsCache {
    var cache:ObjectMap<Dynamic, ObjectMap<Dynamic, Bool>>;

    public function new() {
        cache = new ObjectMap();
    }

    public function hasCompared(a: Any, b: Any) {
        return contains(a, b) || contains(b,a);
    }

    public function markComparison(a: Any, b: Any) {
        var aComparisons = cache.get(a);

        if(aComparisons == null){
            aComparisons = new ObjectMap();
            cache.set(a, aComparisons);
        }

        aComparisons.set(b, true);
    }

    function contains(a:Any, b:Any): Bool {
        var aComparisons = cache.get(a);

        if(aComparisons != null){
            return aComparisons.exists(b);
        }

        return false;
    }
}