package discovery.utils.comparator;

import discovery.utils.comparator.ComparisonsCache;
import haxe.Constraints.IMap;
import haxe.io.Bytes;

import discovery.utils.Comparator.*;
using discovery.utils.IteratorTools;


class GenericComparator {
    public static function compare<T>(a: T, b: T): Int {
        return new GenericComparator().compareBoth(a, b);
    }

    var transformations:Transformations;

    public function new() {
        transformations = new Transformations();
    }

    public function
        transformOn<Tin, Tout>(type: Dynamic, transformation:Tin->Tout)
    {
        transformations.set(type, transformation);
    }

    public function compareBoth<T>(a: T, b: T): Int {
        return new GenericComparison(transformations).compareBoth(a, b);
    }
}


class GenericComparison {
    var transformations:Transformations;

    var comparisons:ComparisonsCache;

    public function new(?transformations:Transformations) {
        if(transformations == null){
            transformations = new Transformations();
        }

        this.transformations = transformations;

        this.comparisons = new ComparisonsCache();
    }

    public function compareBoth<T>(a: T, b: T): Int {
        if(a == b) return 0;
        if (isNull(a) || isNull(b)) return compareNull(a, b);

        var a1 = applyTransformations(a);
        var b1 = applyTransformations(b);

        return switch (Type.typeof(a1)){
            case TFunction: functionCompares(a1, b1);
            case TClass(c): compareInstances(a1, b1, c);
            case TObject  : compareObjects(a1, b1);
            case TEnum(_) : compareEnums(cast a1, cast b1);

            // case TNull, TInt, TFloat, TBool, TUnknown:
            default: reflectCompare(a1, b1);
        }
    }

    function applyTransformations<T>(value:T) {
        return transformations.transform(value);
    }

    function compareInstances<T>(a:T, b:T, classType:Class<Dynamic>):Int {
        if(isA(a, String) && isA(b, String)) return reflectCompare(a, b);
        if(isA(a, Date)  && isA(b,Date))   return compareAsDates(a, b);
        if(isA(a, Bytes) && isA(b, Bytes)) return compareBytes(a, b);
        if(isA(a, Array) && isA(b, Array)) return compareAsArrays(a, b);
        if(isA(a, IMap)  && isA(b, IMap))  return compareAsMaps(a, b);

        return compareObjects(a, b);
    }

    inline function isA<T>(value: T, type: Dynamic){
        return Std.isOfType(value, type);
    }

    static function isNull (a:Dynamic) : Bool {
        return switch (Type.typeof(a)) {
            case TNull: true;
            default: false;
        }
    }

    static function isFunction(a:Dynamic) : Bool {
        return switch (Type.typeof(a)) {
            case TFunction: true;
            default: false;
        }
    }


    function compareObjects<T>(a:T, b:T):Int {
        //avoid recursive structures
        if(comparisons.hasCompared(a, b)){
            return 0;
        }
        comparisons.markComparison(a, b);

        var fieldsA = Reflect.fields(a);
        var fieldsB = Reflect.fields(b);

        var cmp = compareKeys(fieldsA, fieldsB);

        if(cmp != 0) return cmp;

        for (field in fieldsA) {
            cmp = compareFields(a, b, field);

            if(cmp != 0) return cmp;
        }

        return cmp;
    }

    function compareFields<T>(a:T, b:T, field:String):Int {
        var aValue = Reflect.field(a, field);
        var bValue = Reflect.field(b, field);

        if (isFunction(aValue) || isFunction(bValue)) {
            // ignore function as only physical equality can be tested, unless null
            if (isNull(aValue) || isNull(bValue))
                return compareNull(aValue, bValue);
        }


        return compareBoth(aValue, bValue);
    }


    function compareAsMaps<T>(a:T, b:T):Int {
        return compareMaps(
            cast(a, IMap<Dynamic, Dynamic>),
            cast(b, IMap<Dynamic, Dynamic>));
    }

    function compareMaps<K, V>(a: IMap<K, V>, b: IMap<K, V>): Int{
        var a_keys = a.keys().toArray();
        var b_keys = b.keys().toArray();

        var cmp = compareKeys(a_keys, b_keys);

        if (cmp != 0) { return cmp; }

        for (key in a_keys) {
            cmp = compareBoth(a.get(key), b.get(key));

            if(cmp != 0){
                return cmp;
            }
        }

        return cmp;
    }

    function compareKeys<T>(a_keys: Array<T>, b_keys: Array<T>) {
        a_keys.sort(compareBoth);
        b_keys.sort(compareBoth);

        return compareArrays(a_keys, b_keys);
    }

    function compareAsArrays<T>(a: T, b: T): Int {
        return compareArrays(cast(a, Array<Dynamic>),
                           cast(b, Array<Dynamic>));
    }

    function compareArrays<T>(a: Array<T>, b: Array<T>): Int{
        if (a.length != b.length) { return compareInt(a.length, b.length); }

        for (i in 0...a.length) {
            var cmp = compareBoth(a[i], b[i]);
            if (cmp != 0) {
                return cmp;
            }
        }

        return 0;
    }

    function compareEnums(a:EnumValue, b:EnumValue):Int {
        if (a.getIndex() != b.getIndex()) {
            return compareInt(a.getIndex(), b.getIndex());
        }
        var a_args = a.getParameters();
        var b_args = b.getParameters();

        return compareBoth(a_args, b_args);
    }

    function functionCompares<T>(a:T, b:T):Int {
        // only physical equality can be tested for function
        if(Reflect.compareMethods(a, b)){
            return 0;
        }

        return Reflect.compare(a, b);
    }

    function compareAsDates<T>(a:T, b:T):Int {
        return compareBoth(
                    cast(a, Date).getTime(),
                    cast(b, Date).getTime());
    }

    function compareBytes<T>(a:T, b:T):Int {
        return cast(a, Bytes).compare(cast(b, Bytes));
    }

    function reflectCompare<T>(a1:T, b1:T):Int {
        return Reflect.compare(a1, b1);
    }
}