package discovery.domain.operators;

import discovery.domain.operators.constraints.OperatorConstraint;
import haxe.extern.EitherType;
import discovery.domain.operators.basic.Value;

typedef Operand = EitherType<Operator<Any>, Value>;

interface OperatorNode {
    function type():String;
    function operands():Array<Operand>;
}

interface Operator<T> extends Expression<T> extends OperatorNode
{
    function constrain(constraint:OperatorConstraint):Operator<T>;
}