package discovery.domain.operators;


@:forward()
abstract Number(Float) from Float to Float from Int
{

    public static function fromInt(value:Int):Number {
        return value;
    }

    public static function fromFloat(value: Float):Number {
        return value;
    }

    @:to
    public function toInt(): Int {
        return Math.round(this);
    }

    @:op(A+B)
    public function sum(other:Number): Number{
        return toFloat() + other.toFloat();
    }

    @:op(A-B)
    public function minus(other:Number): Number{
        return toFloat() - other.toFloat();
    }

    @:op(A*B)
    public function times(other:Number): Number{
        return toFloat() * other.toFloat();
    }

    @:op(A/B)
    public function dividedBy(other:Number): Number{
        return toFloat() / other.toFloat();
    }

    inline public function toFloat(): Float {
        return this;
    }
}