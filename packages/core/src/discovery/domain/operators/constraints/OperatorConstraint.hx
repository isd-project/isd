package discovery.domain.operators.constraints;

import discovery.domain.operators.Operator.OperatorNode;
import discovery.utils.filter.Filter;

@:using(discovery.utils.filter.FilterTools)
interface OperatorConstraint extends Filter<OperatorNode>{
}