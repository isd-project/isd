package discovery.domain.operators.constraints;

import discovery.domain.operators.Operator.OperatorNode;
import discovery.utils.filter.FilterResult;

class IncludeOnly implements OperatorConstraint{
    var typesToInclude:Map<OperatorType, OperatorType>;

    public function new(types: Array<OperatorType>) {
        typesToInclude = [ for(t in types) t => t];
    }

    public function filter(op:OperatorNode):FilterResult {
        if(op != null && typesToInclude.exists(op.type())){
            return Include;
        }

        return Exclude;
    }
}