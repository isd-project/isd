package discovery.domain.operators.comparison;

class LessThanOrEqualsOperator extends ComparisonOperator{
    public function new(v1: Operator<Number>, v2: Operator<Number>) {
        super(LessThanOrEquals, v1, v2);
    }

    function compare(v1:Float, v2:Float):Bool {
        return v1 <= v2;
    }
}