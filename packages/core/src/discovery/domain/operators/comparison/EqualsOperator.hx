package discovery.domain.operators.comparison;

import discovery.domain.operators.common.BinaryOperator;
import discovery.utils.EqualsComparator;

class EqualsOperator extends BinaryOperator<Bool, Any>{

    public function new<T1, T2>(operand1: Operator<T1>, operand2: Operator<T2>) {
        super(Equals, operand1, operand2);
    }

    function composeResult(value1:Any, value2:Any):Bool {
        return EqualsComparator.equals(value1, value2);
    }
}