package discovery.domain.operators.comparison;

import discovery.domain.operators.common.NumericalBinaryOperator;
import discovery.domain.operators.exceptions.InvalidOperandException;

abstract class ComparisonOperator extends NumericalBinaryOperator<Bool>{
    public function new(type: OperatorType, v1: Operator<Number>, v2: Operator<Number>) {
        super(type, v1, v2);
    }

    function composeResult(v1:Number, v2:Number):Bool {
        return compare(v1, v2);
    }

    abstract function compare(v1:Float, v2:Float):Bool;
}