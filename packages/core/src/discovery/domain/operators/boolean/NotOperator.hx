package discovery.domain.operators.boolean;

import discovery.domain.operators.exceptions.InvalidOperandException;
import discovery.domain.operators.common.BaseOperator;
import discovery.domain.operators.Operator.Operand;

class NotOperator extends BaseOperator<Bool>{

    var operand:Operator<Bool>;

    public function new(operand: Operator<Bool>) {
        super(Not);

        this.operand = operand;
    }

    public function operands():Array<Operand> {
        return [operand];
    }

    public function evaluate(discovered:Discovered):Bool {
        var value = operand.evaluate(discovered);

        if(value == null){
            throw new InvalidOperandException(
                'Failed to evaluate operator (${type()}). ' +
                'Value is null.');
        }

        return !value;
    }
}