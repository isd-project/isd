package discovery.domain.operators.boolean;

import discovery.domain.operators.common.BinaryOperator;
import discovery.domain.operators.Operator.Operand;

class AndOperator extends BooleanBinaryOperator{
    public function new(op1: Operator<Bool>, op2: Operator<Bool>) {
        super(And, op1, op2);
    }

    override function makeConstrained(operands:Array<Null<Operand>>):Operator<Bool> {
        var validOperands = [for(op in operands) if(op != null) op];

        switch (validOperands.length){
            case 2: return this;
            case 1: return cast validOperands[0];
            default: return null;
        }
    }

    function composeResult(v1:Bool, v2:Bool):Bool {
        return v1 && v2;
    }
}