package discovery.domain.operators.boolean;

class OrOperator extends BooleanBinaryOperator{
    public function new(op1: Operator<Bool>, op2: Operator<Bool>) {
        super(Or, op1, op2);
    }

    function composeResult(v1:Bool, v2:Bool):Bool {
        return v1 || v2;
    }
}