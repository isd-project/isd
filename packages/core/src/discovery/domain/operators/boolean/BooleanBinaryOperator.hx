package discovery.domain.operators.boolean;

import discovery.domain.operators.exceptions.InvalidOperandException;
import discovery.domain.operators.common.BinaryOperator;
import discovery.domain.operators.Operator.Operand;

abstract class BooleanBinaryOperator extends BinaryOperator<Bool, Bool>{
    public function new(type:OperatorType, op1: Operator<Bool>, op2: Operator<Bool>) {
        super(type, op1, op2);
    }

    override function checkOperands(v1:Bool, v2:Bool) {
        if(v1 == null || v2 == null){
            throw new InvalidOperandException(
                'Failed to evaluate operator (${type()}). ' +
                'Cannot compare ${v1} and ${v2}');
        }
    }
}