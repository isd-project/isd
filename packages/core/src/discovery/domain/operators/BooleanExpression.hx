package discovery.domain.operators;

import discovery.domain.operators.Expression;

typedef BooleanExpression = Expression<Bool>;