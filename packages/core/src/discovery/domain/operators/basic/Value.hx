package discovery.domain.operators.basic;

import haxe.io.Bytes;
import discovery.format.Binary;

using discovery.utils.Cast;


abstract Value(Dynamic)
    from Bool   to Bool
    from Int    to Int
    from Float  to Float
    from String to String
    from Binary to Binary
{
    function new(value: Dynamic){
        this = value;
    }

    @:from
    public static function fromBytes(bytes: Bytes): Value{
        return Binary.fromBytes(bytes);
    }

    @:from
    public static function fromObject<T:{}>(obj: T): Value {
        return new Value(obj);
    }

    @:from
    public static function fromArray<T>(array: Array<T>): Value {
        return new Value(array);
    }

    public function asArray<T>(): Array<T> {
        return this.castIfIs(Array);
    }

    public function asString(): String {
        return this.castIfIs(String);
    }

    public function asNumber(): Number {
        switch (Type.typeof(this)){
            case TInt: return Number.fromInt(cast this);
            case TFloat: return Number.fromFloat(this);
            default: return null;
        };
    }

    public function asObject(): Any {
        if (isObject()){
            return this;
        }

        return null;
    }

    public function isObject(): Bool {
        switch (Type.typeof(this)){
            case TObject|TClass(_): return true;
            default: return false;
        }
    }
}