package discovery.domain.operators.basic;

import discovery.domain.operators.common.BaseOperator;
import discovery.domain.operators.Operator.Operand;

using discovery.utils.Cast;


class ValueOperator extends BaseOperator<Value>{
    var value:Value;

    public function new(value: Value) {
        super(Value);

        this.value = value;
    }

    public function evaluate(_: Discovered): Value{
        return this.value;
    }

    public function operands():Array<Operand> {
        return [value];
    }
}