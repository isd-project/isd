package discovery.domain.operators.basic;

import discovery.domain.operators.common.BaseOperator;
import discovery.domain.operators.basic.Value;
import discovery.domain.operators.Operator.Operand;

class AttributeOperator extends BaseOperator<Value> {
    var attrName:String;

    public function new(attr:String) {
        super(Attribute);

        this.attrName = attr;
    }

    public function evaluate(discovered:Discovered):Value {
        return discovered.resolveAttribute(attrName);
    }

    public function operands():Array<Operand> {
        return [attrName];
    }
}