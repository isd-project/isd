package discovery.domain.operators.textual;

import discovery.domain.operators.common.BinaryOperator;
import discovery.domain.operators.basic.Value;

using StringTools;

class ContainsTextOperator extends BinaryOperator<Bool, Value>{

    public function new(op1: Operator<Value>, op2: Operator<Value>) {
        super(ContainsText, op1, op2);
    }

    function composeResult(v1:Value, v2:Value):Bool {
        var text1 = v1.asString();
        var text2 = v2.asString();

        return text1 != null
            && text2 != null
            && text1.contains(text2);
    }
}