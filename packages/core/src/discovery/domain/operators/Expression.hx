package discovery.domain.operators;

interface Expression<T> {
    function evaluate(discovered:Discovered): T;
}