package discovery.domain.operators.numerical;

import discovery.domain.operators.common.NumericalBinaryOperator;

class DivisionOperator extends NumericalBinaryOperator<Number>{
    public function new(op1: NumericalOperator, op2: NumericalOperator) {
        super(Division, op1, op2);
    }

    function composeResult(n1:Number, n2:Number):Number {
        return n1 / n2;
    }
}