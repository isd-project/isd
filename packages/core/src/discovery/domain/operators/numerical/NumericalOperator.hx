package discovery.domain.operators.numerical;

import discovery.domain.operators.constraints.OperatorConstraint;
import discovery.domain.operators.Operator.Operand;
import discovery.test.tests.base.operators.NumericalOperatorsTest.NumericalOperatorsSteps;
import haxe.extern.EitherType;
import discovery.domain.operators.basic.Value;

typedef NumberOrValue = EitherType<Number, Value>;

@:forward()
abstract NumericalOperator(Operator<Number>)
    from Operator<Number>
    to Operator<Number>
{

    @:from
    public static function
        fromValueOperator<T:NumberOrValue>(op: Operator<T>): NumericalOperator
    {
        return new NumberParserOperator(cast op);
    }
}

class NumberParserOperator implements Operator<Number>{
    var delegate:Operator<NumberOrValue>;

    public function new(op:Operator<NumberOrValue>) {
        this.delegate = op;
    }

    public function operands():Array<Operand> {
        return delegate.operands();
    }

    public function evaluate(discovered:Discovered):Number {
        var value:Value = delegate.evaluate(discovered);

        return value.asNumber();
    }

    public function type():String {
        return delegate.type();
    }

    public function constrain(constraint:OperatorConstraint):Operator<Number> {
        return delegate.constrain(constraint);
    }
}