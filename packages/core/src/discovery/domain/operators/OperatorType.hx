package discovery.domain.operators;

enum abstract OperatorType(String) from String to String
{
    var Attribute = 'attribute';
    var Value = 'value';

    // boolean ----------------------------------

    var And = 'and';
    var Or = 'or';
    var Not = 'not';

    // comparison ----------------------------------

    var Equals              = 'equals';
    var NotEquals           = "!=";

    var LessThan            = "<";
    var LessThanOrEquals    = "<=";
    var GreaterThan         = ">";
    var GreaterThanOrEquals = ">=";

    // numerical -------------------------------------

    var Sum            = "+";
    var Subtraction    = "-";
    var Multiplication = "*";
    var Division       = "/";

    // textual -------------------------------------

    var ContainsText = "containsText";

}