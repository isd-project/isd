package discovery.domain.operators.serialization;

import discovery.domain.operators.serialization.OperatorData.OperandData;
import discovery.domain.operators.basic.Value;
import haxe.extern.EitherType;
import discovery.domain.operators.Operator.Operand;

using discovery.utils.Cast;


class OperatorSerializer {
    public function new() {

    }

    public function serialize<T>(op:Operator<T>):OperatorData {
        if(op == null) return null;

        return {
            type: op.type(),
            operands: op.operands().map(serializeOperands)
        };
    }

    function serializeOperands(operand: Operand): OperandData{
        var op:Operator<Any> = operand.castIfIs(Operator);

        if(op != null){
            return serialize(op);
        }

        return operand;
    }
}