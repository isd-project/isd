package discovery.domain.operators.serialization;

import discovery.domain.operators.basic.Value;
import haxe.extern.EitherType;

typedef OperandData = EitherType<OperatorData, Value>;

typedef OperatorData = {
    type: OperatorType,
    operands: Array<OperandData>
};