package discovery.domain.operators;

import discovery.domain.operators.comparison.NotEqualsOperator;
import discovery.domain.operators.basic.ValueOperator;
import discovery.domain.operators.basic.AttributeOperator;
import discovery.domain.operators.comparison.EqualsOperator;
import discovery.domain.operators.numerical.DivisionOperator;
import discovery.domain.operators.numerical.MultiplicationOperator;
import discovery.domain.operators.numerical.MinusOperator;
import discovery.domain.operators.numerical.SumOperator;
import discovery.domain.operators.numerical.NumericalOperator;
import discovery.domain.operators.textual.ContainsTextOperator;
import discovery.domain.operators.comparison.LessThanOrEqualsOperator;
import discovery.domain.operators.comparison.GreaterThanOrEqualsOperator;
import discovery.domain.operators.comparison.GreaterThanOperator;
import discovery.domain.operators.comparison.LessThanOperator;
import discovery.domain.operators.boolean.NotOperator;
import discovery.domain.operators.boolean.OrOperator;
import discovery.domain.operators.boolean.AndOperator;
import discovery.domain.operators.basic.Value;

class Operators {


    // core ------------------------------------------------------------

    public static function attr(attrName:String) {
        return new AttributeOperator(attrName);
    }

    public static function value(value:Value){
        return new ValueOperator(value);
    }

    // boolean ------------------------------------------------------------

    public static
        function AND(op1:Operator<Bool>, op2:Operator<Bool>): Operator<Bool>
    {
        return new AndOperator(op1, op2);
    }

    public static
        function OR(op1:Operator<Bool>, op2:Operator<Bool>): Operator<Bool>
    {
        return new OrOperator(op1, op2);
    }

    public static
        function not(op1:Operator<Bool>): Operator<Bool>
    {
        return new NotOperator(op1);
    }

    // relational ------------------------------------------------------------

    public static function equals<T>(op1: Operator<T>, op2: Operator<T>):Operator<Bool> {
        return new EqualsOperator(op1, op2);
    }

    public static function notEquals<T>(op1: Operator<T>, op2: Operator<T>):Operator<Bool> {
        return new NotEqualsOperator(op1, op2);
    }

    public static function
        lessThan(op1: Operator<Value>, op2: Operator<Value>):Operator<Bool>
    {
        return new LessThanOperator(cast op1, cast op2);
    }

    public static function
        greaterThan(op1: Operator<Value>, op2: Operator<Value>):Operator<Bool>
    {
        return new GreaterThanOperator(cast op1, cast op2);
    }


    public static function
        greaterThanOrEquals(op1: Operator<Value>, op2: Operator<Value>):Operator<Bool> {
        return new GreaterThanOrEqualsOperator(cast op1, cast op2);
    }

    public static function
        lessThanOrEquals(op1: Operator<Value>, op2: Operator<Value>):Operator<Bool>
    {
        return new LessThanOrEqualsOperator(cast op1, cast op2);
    }

    // textual ---------------------------------------------------------------

    public static function
        containsText(op1: Operator<Value>, op2: Operator<Value>):Operator<Bool>
    {
        return new ContainsTextOperator(cast op1, cast op2);
    }


    // numerical ---------------------------------------------------------------


    public static function
        plus<T:NumberOrValue>(op1: Operator<T>, op2: NumericalOperator):Operator<Number>
    {
        return new SumOperator(op1, op2);
    }

    public static function
        minus<T:NumberOrValue>(op1: Operator<T>, op2: NumericalOperator):Operator<Number>
    {
        return new MinusOperator(op1, op2);
    }


    public static function
        times<T:NumberOrValue>(op1: Operator<T>, op2: NumericalOperator):Operator<Number>
    {
        return new MultiplicationOperator(op1, op2);
    }

    public static function
        dividedBy<T:NumberOrValue>(op1: Operator<T>, op2: NumericalOperator):Operator<Number>
    {
        return new DivisionOperator(op1, op2);
    }


}