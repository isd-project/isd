package discovery.domain.operators.common;

import discovery.domain.operators.Operator.Operand;

abstract class BinaryOperator<Tout, Tinput> extends BaseOperator<Tout>{
    var operand1:Operator<Tinput>;
    var operand2:Operator<Tinput>;

    public function new<T1, T2>(
        type:OperatorType, operand1: Operator<T1>, operand2: Operator<T2>)
    {
        super(type);

        this.operand1 = cast operand1;
        this.operand2 = cast operand2;
    }

    public function evaluate(discovered:Discovered):Tout{
        var v1 = operand1.evaluate(discovered);
        var v2 = operand2.evaluate(discovered);

        checkOperands(v1, v2);

        return composeResult(v1, v2);
    }

    function checkOperands(v1:Tinput, v2:Tinput) {
        //To override in subclasses if needed
    }

    abstract function composeResult(v1: Tinput, v2: Tinput): Tout;


    public function operands():Array<Operand> {
        return [operand1, operand2];
    }

}