package discovery.domain.operators.common;

import discovery.domain.operators.exceptions.InvalidOperandException;

abstract class NumericalBinaryOperator<T> extends BinaryOperator<T, Number>{
    override function checkOperands(v1:Number, v2:Number) {
        if(v1 == null || v2 == null){
            throw new InvalidOperandException(
                'Failed to evaluate operator (${type()}). ' +
                'Cannot compare ${v1} and ${v2}'
            );
        }
    }

    abstract function composeResult(v1:Number, v2:Number):T;

}