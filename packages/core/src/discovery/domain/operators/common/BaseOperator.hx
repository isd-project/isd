package discovery.domain.operators.common;

import discovery.domain.operators.Operator.Operand;
import discovery.domain.operators.constraints.OperatorConstraint;

using discovery.utils.Cast;


abstract class BaseOperator<T> implements Operator<T> {
    var opType:OperatorType;

    function new(type: OperatorType) {
        this.opType = type;
    }

    abstract public function evaluate(discovered:Discovered):T;

    public function type():String {
        return opType;
    }

    public function constrain(constraint:OperatorConstraint):Operator<T>{
        if(constraint.excludes(this)){
            return null;
        }

        var constrainedOps = operands().map((op)->{
            return constrainOperand(op, constraint);
        });

        return makeConstrained(constrainedOps);
    }

    function constrainOperand(operand: Operand, constraint: OperatorConstraint)
    {
        if(!operand.isOfType(Operator)) return operand;

        return if(constraint.excludes(operand)) null else operand;
    }

    function makeConstrained(constrainedOperands: Array<Null<Operand>>): Operator<T>
    {
        for (operand in constrainedOperands){
            if(operand == null){
                return null;
            }
        }

        return this;
    }
}