package discovery.domain;

import discovery.utils.functional.Callback;
import discovery.async.DoneCallback;

typedef OnPublishListener = Callback;

@:structInit
class PublishListeners{
    @:optional
    public var onPublish(default, default): OnPublishListener;

    @:optional
    public var onFinish(default, default) : DoneCallback;
}