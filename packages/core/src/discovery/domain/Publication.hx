package discovery.domain;

import discovery.domain.PublishInfo;
import haxe.Exception;

typedef PublishedEventInfo = {
    published: PublishInfo,
    publication: Publication
};
typedef PublicationFinished = { ?error: Exception, publication: Publication };

interface Publication {
    /**
        Event called when the object was published.
    **/
    var oncePublished(default, null)  :Event<PublishedEventInfo>;

    /**
        Event called when the publication finished.
        Publication finish may be caused by stop or a failure.
    **/
    var onceFinished(default, null) :Event<PublicationFinished>;

    /**
        Event called when an error ocurr internally, that does
        not prevent the publication to continue.
    **/
    var onInternalError(default, null): Event<ErrorEventInfo>;

    // --------------------------------------------

    function stop(): Void;
}