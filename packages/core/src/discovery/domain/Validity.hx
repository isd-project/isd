package discovery.domain;

import discovery.utils.time.Duration;
import discovery.exceptions.IllegalArgumentException;


class Validity {
    public var validFrom(default, null): Date;
    public var validTo  (default, null): Date;


    public static function validUntil(validTo:Date):Validity {
        return new Validity(validTo);
    }

    public static function validFor(duration:Duration):Validity {
        var validFrom = Date.now();
        var validTo = duration + validFrom;

        return new Validity(validTo, validFrom);
    }

    public static function between(validFrom: Date, validTo: Date) {
        return new Validity(validTo, validFrom);
    }


    public function new(validTo: Date, ?validFrom: Date) {
        validFrom = if(validFrom != null) validFrom else Date.now();
        this.validFrom = validFrom;

        this.validTo = validTo;

        IllegalArgumentException.require(validFrom != null,
            'IllegalArgument: "validFrom" date is null');
        IllegalArgumentException.require(validTo != null,
            'IllegalArgument: "validTo" date is null');
        IllegalArgumentException.require(
            validFrom.getTime() <= validTo.getTime(),
            'IllegalArguments: initial valid date (${validFrom}) occurs after the final valid date (${validTo})');

    }

    public function isValid():Bool {
        return isValidAt(Date.now());
    }

    public function isValidAt(dateTime:Date):Bool {
        return validFrom.getTime() <= dateTime.getTime()
            && dateTime.getTime() <= validTo.getTime();
    }

    public static function areEquals(a:Validity, b:Validity) {
        return a == b
            || (a != null
            &&  b != null
            && a.equals(b));
    }

    public function equals(other:Validity): Bool {
        return other != null
            && validFrom.getTime() == other.validFrom.getTime()
            && validTo.getTime() == other.validTo.getTime();
    }

    public function duration(): Duration{
        return Duration.between(validFrom, validTo);
    }
}