package discovery.domain;

import discovery.domain.Description.Attributes;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.signature.Signed;

@:forward
abstract Discovered(DiscoveredData)
    from DiscoveredData to DiscoveredData
{
    public function new(discovered: DiscoveredData) {
        this = discovered;
    }

    @:from
    public static
    function fromAnnouncement(announcement: Announcement): Discovered{
        if(announcement == null) return null;

        return fromSignedAnnouncement(Signed.make(announcement, null));
    }

    @:from
    public static
    function fromSignedAnnouncement(signed: Signed<Announcement>): Discovered
    {
        if(signed == null) return null;

        return new DiscoveredData(signed);
    }

    @:to
    public function signedAnnouncement(): Signed<Announcement>{
        return this.signed();
    }

    @:to
    public function description(): Description{
        return this.description();
    }
}

class DiscoveredData{
    var _signed: Signed<Announcement>;

    public function new(announcement: Signed<Announcement>) {
        this._signed = announcement;
    }

    public function providerId(): ProviderId{
        return if(hasDescription()) description().providerId else null;
    }

    public function keyFingerprint(): Fingerprint{
        var id = providerId();

        return if(id != null) id.fingerprint() else null;
    }

    function hasDescription(){
        return description() != null;
    }

    public function identifier(): Identifier {
        return if(hasDescription()) description().identifier() else null;
    }

    public function attribute(key:String, ?defaultValue:Dynamic) {
        var result = attributes().get(key);

        return orDefault(result, defaultValue);
    }

    public function resolveAttribute(key:String, ?defaultValue:Dynamic) {
        var result = attributes().resolve(key);

        return orDefault(result, defaultValue);
    }

    public function attributes(): Attributes {
        var d = description();
        if(d != null && d.attributes != null)
            return d.attributes;

        return {};
    }

    inline function orDefault(value:Any, ?defaultValue:Any) {
        return if(value != null) value else defaultValue;
    }

    public function description(): Description {
        return announcement();
    }
    public function announcement(): Announcement {
        return signed();
    }

    public function signed(): Signed<Announcement> {
        return _signed;
    }

    public function hasSignature(){
        return hasDescription()
            && signed().signature != null;
    }
}