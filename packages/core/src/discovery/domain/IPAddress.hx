package discovery.domain;

abstract IPAddress(String) from String{
    public function toString() {
        return this;
    }
}