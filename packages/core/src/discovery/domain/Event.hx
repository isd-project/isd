package discovery.domain;

typedef ListenOptions = {
    once: Bool
};

typedef EventListenOptions = ListenOptions;

interface Event<T> {
    function listen(callback: T->Void, ?options: ListenOptions):Void;
    function unlisten(callback: T->Void):Void;
}