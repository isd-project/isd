package discovery.domain;

import haxe.PosInfos;
import haxe.Exception;

typedef ErrorEventInfo = {
    error: Exception,
    ?context: String,
    ?posInfo: PosInfos
};