package discovery.domain;

import discovery.domain.operators.BooleanExpression;

typedef Query =  {
    ?type: String,
    ?query: BooleanExpression
};
