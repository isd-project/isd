package discovery.domain;

import discovery.async.promise.Promisable;


interface IpDiscover {
    function listPublicIps(): Promisable<Array<IPAddress>>;
}