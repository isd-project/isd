package discovery.domain;

import discovery.domain.Identifier;


@:structInit
class IdentifierUrl extends Identifier{

    @:optional
    public var path(default, default): Array<String>;

    @:optional
    public var query(default, default): Map<String, String>;

    @:optional
    public var fragment(default, default): String;

    public static function fromIdentifier(id:Identifier):IdentifierUrl {
        if(id == null) return null;

        return {
            instanceId: id.instanceId,
            providerId: id.providerId,
            type: id.type,
            name: id.name,
            parameters: id.parameters
        };
    }
}
