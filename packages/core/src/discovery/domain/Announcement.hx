package discovery.domain;

import discovery.utils.time.Duration;
import discovery.domain.Validity;

@:forward
@:forward.new
@:forwardStatics
abstract Announcement(AnnouncementData)
    from AnnouncementData
    to AnnouncementData
{
    @:to
    public function toDescription(){
        return this.description;
    }
}

@:forward
@:forward.new
abstract AnnouncementValidity(Validity)
    from Validity to Validity
{
    @:from
    public static function validFor(duration: Duration): AnnouncementValidity{
        return Validity.validFor(duration);
    }

    @:from
    public static function validUntil(date: Date): AnnouncementValidity{
        return Validity.validUntil(date);
    }
}

class AnnouncementData {
    public var description(default, null):Description;
    public var validity(default, null): AnnouncementValidity;

    public static function make(
        desc:Description, validity:AnnouncementValidity):AnnouncementData
    {
        return new AnnouncementData(desc, validity);
    }

    public function new(
        description: Description, validity: AnnouncementValidity)
    {
        this.description = description;
        this.validity = validity;
    }

    public function isValid() {
        return validity != null && validity.isValid();
    }

    public function equals(other:AnnouncementData): Bool{
        return Description.areEquals(description, other.description)
            && Validity.areEquals(validity, other.validity);
    }
}