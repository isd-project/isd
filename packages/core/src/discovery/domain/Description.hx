package discovery.domain;

import discovery.exceptions.IllegalArgumentException;
import discovery.utils.EqualsComparator;
import haxe.io.Bytes;
import haxe.Json;
import discovery.format.Formatters;
import equals.Equal;
import discovery.format.Binary;

using StringTools;

@:structInit
class Description{
    //TO-DO: should encapsulate these attributes in a class (ServiceInfo?)
    public var name: String;
    public var type: String;
    @:optional
    public var providerId:ProviderId;
    @:optional
    public var instanceId:Binary;

    // Maybe: exclude port (synchronization with location.defaultPort is not perfect)
    public var port(get, set): Int;

    @:optional
    public var location(get, default): Location;

    @:optional
    public var attributes: Attributes;

    public function new(
        name:String, type: String,
        ?providerId: ProviderId, ?instanceId: Binary,
        ?port: Int,
        ?location: Location,
        ?attributes: Attributes
    )
    {
        this.name = name;
        this.type = type;
        this.providerId = providerId;
        this.instanceId = instanceId;
        this.location = if(location == null) {} else location;
        this.attributes = if(attributes == null) {} else attributes;

        if(port != null){
            this.location.defaultPort = port;
        }
    }

    public function identifier(): Identifier {
        return {
            name: name,
            type: type,
            providerId: providerId.id(),
            instanceId: instanceId
        };
    }

    public function set_identifier(identifier:Identifier) {
        this.name = identifier.name;
        this.type = identifier.type;
        this.providerId = identifier.providerId;
        this.instanceId = identifier.instanceId;
    }

    function get_location(): Location {
        //ensure non null location
        if(this.location == null){
            this.location = {};
        }
        return this.location;
    }

    function get_port(): Int{
        return get_location().defaultPort;
    }

    function set_port(port: Int): Int{
        return get_location().defaultPort = port;
    }

    public function addresses() {
        return if(location == null) [] else location.expandAddresses();
    }


    public static function areEquals(a:Description, b:Description) {
        return a == b
            || (a != null && a.equals(b));
    }

    public function equals(other: Description) {
        return other != null
            && name == other.name
            && type == other.type
            && (ProviderId.areEquals(providerId, other.providerId))
            && (Binary.areEquals(instanceId, other.instanceId))
            && Location.areEquals(location, other.location)
            && Attributes.areEquals(attributes, other.attributes);
    }

    public function clone():Description {
        return {
            name: this.name,
            type: this.type,
            providerId: this.providerId,
            instanceId: this.instanceId,
            location: this.location,
            attributes: this.attributes
        };
    }

    public function toString() {
        return Json.stringify(this, function(key, value): Dynamic{
            if(Std.isOfType(value, Bytes)){
                return (value:Bytes).toHex();
            }
            else {
                return value;
            }
        }, '  ');
    }
}


@:forward
@:transitive
abstract Attributes(Struct) from Struct to Struct
{
    public function new(struct: Struct) {
        this = struct;
    }

    public function resolve(attribute: String) {
        if(attribute == null)
            throw new IllegalArgumentException('attribute name is null');

        var parts = attribute.split('.');
        var obj:Struct = this;

        for (part in parts){
            if(obj == null) return null;

            obj = obj.get(part);
        }

        return obj;
    }

    public static function areEquals(a: Attributes, b: Attributes){
        var equalsComp = new EqualsComparator();
        equalsComp.transformOn(BinaryData, (binary:BinaryData)->binary.toBytes());

        return equalsComp.areEquals(a, b);
    }
}



@:structInit
class Location{
    @:optional
    public var defaultPort: Int;
    @:optional
    public var addresses: Array<IPAddress>;
    @:optional
    public var channels: Array<Channel>;


    public function new(?defaultPort: Int,
                        ?addresses:Array<IPAddress>, ?channels:Array<Channel>)
    {
        this.defaultPort = defaultPort;
        this.addresses = addresses;
        this.channels = channels;
    }

    public function expandAddresses(): Array<ServiceAddress> {
        return [for (addr in addresses)
                    {
                        address: addr.toString(),
                        port: defaultPort
                    }
                ];
    }

    public static function areEquals(location:Location, other:Location) {
        if(location == null || other == null) return location == other;

        return location.defaultPort == other.defaultPort
            && Equal.equals(location.addresses, other.addresses)
            && Equal.equals(location.channels, other.channels)
            ;
    }
}

@:structInit
class ServiceAddress{
    public var address(default, default): String;
    public var port(default, default): Int;

    public function new(address:String, port: Int) {
        this.address = address;
        this.port = port;
    }
}

typedef ChannelKey = String;

@:structInit
class Channel{
    @:optional
    public var protocol: String;
    @:optional
    public var address: String;
    @:optional
    public var port: Int;
    @:optional
    public var keys(default, null):Array<ChannelKey>;

    public function new(
        ?protocol:String, ?address:String, ?port:Int, ?keys:Array<ChannelKey>)
    {
        this.protocol = protocol;
        this.address  = address;
        this.port     = port;
        this.keys     = if(keys == null) []  else keys.copy();
    }

    public function toString(): String {
        return string(protocol)
            +  ':' + percentEncode(string(address))
            +  ':' + string(port);
            //TO-DO: include keys
    }

    inline function percentEncode(value: String){
        return Formatters.percentEncoding.encode(value);
    }

    function string<T>(value: T): String {
        return if(value == null) '' else Std.string(value);
    }

    public function isEmpty() {
        return protocol == null
            && address == null
            && port == null;
            //TO-DO: check keys
    }

    public function equals(other: Channel){
        return other != null
            && port == other.port
            && protocol == other.protocol
            && address == other.address;
            //TO-DO: compare keys
    }

    public static function fromString(value:String):Channel {
        if(value == null) return null;

        value = value.trim();

        var firstSepIdx = value.indexOf(':');
        var lastSepIdx  = value.lastIndexOf(':');

        if(firstSepIdx < 0) {//invalid: no separator
            //maybe should throw
            return null;
        }

        var protocol = value.substring(0, firstSepIdx);
        var address = value.substring(firstSepIdx + 1, lastSepIdx).trim();
        var portStr = if(lastSepIdx <= firstSepIdx) null
                      else value.substring(lastSepIdx + 1);

        //TO-DO: parse keys

        return {
            protocol: if(empty(protocol)) null else protocol,
            address : if(empty(address))  null else address,
            port    : if(empty(portStr))  null else Std.parseInt(portStr)
        };
    }

    static function empty(str: String) {
        return str == null ||  str.length == 0;
    }
}