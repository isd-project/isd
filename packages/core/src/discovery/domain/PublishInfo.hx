package discovery.domain;

import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.Fingerprint;
import haxe.io.Bytes;

import discovery.keys.Key;

using discovery.utils.Cast;


@:transitive
@:forward
abstract MaybeSigned<T>(Signed<T>) from Signed<T> to Signed<T>{

    @:from
    public static function fromSigned<T>(signed: Signed<T>): MaybeSigned<T>{
        return signed;
    }

    @:from
    public static function fromData<T>(value: T): MaybeSigned<T>{
        return fromSigned(Signed.make(value, null));
    }
}


class PublishInfo{
    @:optional
    public var providerKey: Key;

    var _announcement:Signed<Announcement>;

    public static function make(
        announcement:MaybeSigned<Announcement>, ?providerKey:Key):PublishInfo
    {
        return new PublishInfo(announcement, providerKey);
    }

    public function new(
        announcement:MaybeSigned<Announcement>, ?providerKey:Key)
    {

        this._announcement = announcement;
        this.providerKey = providerKey;
    }

    public function description(): Description {
        return announcement().description;
    }

    public function announcement(): Announcement {
        return signed();
    }

    public function signed(): Signed<Announcement> {
        return _announcement;
    }


    public function getIdentifier():Identifier {
        return {
            name: description().name,
            type: description().type,
            providerId: getProviderId(),
            instanceId: description().instanceId
        };
    }

    public function getProviderId():Bytes {
        //FIXME: should require providerKey
        return if(providerKey != null) providerKey.fingerprint.hash
               else description().providerId.id();
    }

    public function getProviderFingerprint():Fingerprint {
        //FIXME: should require providerKey
        return if(providerKey != null) providerKey.fingerprint
               else description().providerId.fingerprint();
    }

    public function name():String {
        return description().name;
    }

    public function instanceId() {
        return description().instanceId;
    }
}