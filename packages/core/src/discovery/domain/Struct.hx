package discovery.domain;

import haxe.DynamicAccess;

using equals.Equal;


typedef StructBaseType<T> = DynamicAccess<T>;

@:forward
abstract StructTyped<T> (StructBaseType<T>)
    from StructBaseType<T> to StructBaseType<T>
    from Dynamic
{
    public function new(?struct: StructBaseType<T>){
        this = if(struct != null) struct else new StructBaseType();
    }

    public function tryGet(name:String, defaultValue:T){
        var value = this.get(name);
        return if(value != null) value else defaultValue;
    }

    @:op(a.b)
    public function getField(name:String){
        return this.get(name);
    }

    @:op(a.b)
    public function setField(name:String, value:T){
        return this.set(name, value);
    }

    public function equals(other: StructTyped<T>):Bool{
        return areEquals(this, other);
    }

    public static function areEquals<T>(a: StructTyped<T>, b: StructTyped<T>){
        return Equal.equals(a, b);
    }
}

typedef Struct = StructTyped<Dynamic>;