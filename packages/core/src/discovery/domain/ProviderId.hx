package discovery.domain;

import discovery.keys.storage.ExportedFingerprint;
import haxe.io.Bytes;
import discovery.keys.crypto.HashTypes;
import discovery.format.EncodedData.TextWithFormat;
import discovery.format.Binary;
import discovery.keys.KeyIdentifier;
import discovery.keys.crypto.Fingerprint;

typedef ProviderIdType = Dynamic;

abstract ProviderId(ProviderIdType)
{
    function new(providerId: ProviderIdType){
        this = providerId;
    }

    @:from
    public static
    function fromFingerprint(fingerprint: Fingerprint): ProviderId {
        return new ProviderId(fingerprint);
    }

    @:from
    public static
    function fromExportedFingerprint(fing: ExportedFingerprint): ProviderId {
        return fromFingerprint(fing);
    }

    @:from
    public static
    function fromKeyId(keyId: KeyIdentifier): ProviderId {
        return new ProviderId(keyId);
    }

    @:from
    public static
    function fromBinary(binary: Binary): ProviderId {
        return fromKeyId(binary);
    }

    @:from
    public static
    function fromBytes(bytes: Bytes): ProviderId {
        return fromKeyId(bytes);
    }

    @:from
    public static
    function fromString(prefixOrKeyName: String): ProviderId {
        return fromKeyId(prefixOrKeyName);
    }

    public function isKeyId() {
        return Std.isOfType(this, TextWithFormat);
    }

    public function isFingerprint(){
        return Std.isOfType(this, Fingerprint);
    }


    @:to
    public function fingerprint(): Fingerprint {
        return if(isFingerprint()) this else null;
    }

    public function keyId(): KeyIdentifier{
        return if(isKeyId()) this else null;
    }

    @:to
    public function toKeyId(): KeyIdentifier {
        if(isKeyId()) return keyId();

        var fing = fingerprint();
        return if(fing != null) fing.hash else null;
    }

    public function hashAlg(): Null<HashTypes>{
        return if(isFingerprint()) fingerprint().alg else null;
    }

    public function id(): Binary {
        if(this == null) return null;

        if(isFingerprint()){
            return fingerprint().hash;
        }
        if(isKeyId() && !keyId().isText()){
            return keyId().toBytes();
        }

        return null;
    }

    public function equals(other: ProviderId) {
        if(this == null) return other == null;
        else if(other == null) return false;

        return hashAlg() == other.hashAlg()
            && Binary.areEquals(id(), other.id());
    }

    public function toString(): String{
        return if(isFingerprint()) return fingerprint().toString();
               else if(isKeyId())  return keyId().toString();
               else null;
    }

    public static function areEquals(p1:ProviderId, p2:ProviderId) {
        return p1.equals(p2);
    }
}