package discovery.domain.serialization;

interface Serializer<T> {
    function deserialize(serialized:Dynamic):T;
    function serialize(value:T):Any;
}