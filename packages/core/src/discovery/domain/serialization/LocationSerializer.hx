package discovery.domain.serialization;

import discovery.domain.Description.Channel;
import discovery.domain.Description.Location;

using discovery.utils.Cast;


class LocationSerializer implements Serializer<Location> {
    public function new() {

    }

    public function serialize(value:Location):Any {
        var obj:Struct = value;
        return obj.copy();
    }

    public function deserialize(serialized:Dynamic):Location {
        var obj:Struct = serialized;
        var loc:Location = {};

        loc.defaultPort = serialized.defaultPort.castIfIs(Int);

        loc.channels  = deserializeArray(serialized.channels, toChannel);
        loc.addresses = deserializeArray(serialized.addresses, toAddress);

        return loc;
    }

    function
        deserializeArray<T>(arrayObj:Dynamic, transform:Dynamic->T):Array<T>
    {
        var values:Array<Dynamic> = arrayObj.castIfIs(Array);

        if(values == null) return null;

        return values.map(transform).filter((c)->c != null);
    }

    function toChannel(value: Dynamic):Channel{
        if(value == null) return null;

        var protocol = value.protocol.castIfIs(String);
        var address  = value.address.castIfIs(String);
        var port     = value.port.castIfIs(Int);

        if(protocol != null || address != null || port != null)
        {
            return new Channel(protocol, address, port);
        }

        return null;
    }

    function toAddress(value: Dynamic):IPAddress{
        return value.castIfIs(String);
    }
}