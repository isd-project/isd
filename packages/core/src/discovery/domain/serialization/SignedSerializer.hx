package discovery.domain.serialization;

import discovery.keys.crypto.signature.Signed;

class SignedSerializer<Data> implements Serializer<Signed<Data>> {
    var dataSerializer:Serializer<Data>;
    var binarySerializer:BinarySerializer;

    public function new(dataSerializer: Serializer<Data>) {
        this.dataSerializer = dataSerializer;
        this.binarySerializer = new BinarySerializer();
    }

    public function serialize(value:Signed<Data>):Any {
        return {
            signature: binarySerializer.serialize(value.signature),
            data: dataSerializer.serialize(value.data)
        };
    }

    public function deserialize(serialized:Dynamic):Signed<Data> {
        return Signed.make(
            dataSerializer.deserialize(serialized.data),
            binarySerializer.deserialize(serialized.signature)
        );
    }
}