package discovery.domain.serialization;

import discovery.format.exceptions.ParseException;
using DateTools;


class ValidityDeserializer implements Serializer<Validity> {
    public function new() {

    }

    public function serialize(value:Validity):Any {
        return {
            validTo  : getTime(value.validTo),
            validFrom: getTime(value.validFrom)
        };
    }

    public function deserialize(serialized:Dynamic):Validity {
        var validTo = fromTime(serialized.validTo);
        var validFrom = fromTime(serialized.validFrom);

        try{
            return new Validity(validTo, validFrom);
        }
        catch(e){
            throw new ParseException('Cannot deserialize value (${serialized}) into Validity', e);
        }
    }

    function getTime(date:Date): Null<Float> {
        if(date == null) return null;

        return date.getTime();
    }

    function fromTime(time: Null<Float>): Date {
        if(time == null) return null;

        return Date.fromTime(time);
    }
}