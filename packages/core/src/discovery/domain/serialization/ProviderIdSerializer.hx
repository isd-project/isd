package discovery.domain.serialization;

import discovery.keys.crypto.Fingerprint;
import discovery.keys.KeyIdentifier;
import discovery.keys.storage.ExportedFingerprint;
import discovery.format.Binary;

class ProviderIdSerializer implements Serializer<ProviderId> {
    public function new() {

    }

    public function serialize(providerId:ProviderId):Any {
        if(providerId == null) return null;
        if(providerId.isFingerprint()){
            return ExportedFingerprint.serialize(
                providerId.fingerprint());
        }

        return providerId.toKeyId();
    }

    public function deserialize(serialized:Dynamic):ProviderId {
        var bin = Binary.fromDynamic(serialized);
        if(bin != null){
            return ProviderId.fromBinary(bin);
        }

        var keyId = KeyIdentifier.fromDynamic(serialized);
        if(keyId != null){
            return ProviderId.fromKeyId(keyId);
        }

        var fing:Fingerprint = ExportedFingerprint.fromDynamic(serialized);

        return fing;
    }
}