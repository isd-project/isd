package discovery.domain.serialization;

import discovery.utils.Cast;
import haxe.Unserializer;

class HaxeSerializer<T> implements Serializer<T> {
    var delegate:Serializer<T>;

    var haxeSerializer:haxe.Serializer;

    public function new(delegate:Serializer<T>) {
        this.delegate = delegate;

        haxeSerializer = new haxe.Serializer();
    }

    public function serialize(value:T):Any {
        var obj = this.delegate.serialize(value);

        var serializer = new haxe.Serializer();
        serializer.useCache = true;
        serializer.serialize(obj);

        return serializer.toString();
    }

    public function deserialize(serialized:Dynamic):T {
        var buf:String = Cast.castIfIs(serialized, String);
        var unserializer = new Unserializer(buf);
        var obj = unserializer.unserialize();

        return this.delegate.deserialize(obj);
    }
}