package discovery.domain.serialization;

import discovery.format.EncodedData;
import discovery.format.Binary;

class BinarySerializer implements Serializer<Binary>{
    public function new() {

    }

    public function serialize(value:Binary):Any {
        return EncodedData.encodeDefault(value);
    }

    public function deserialize(serialized:Dynamic):Binary {
        var value = EncodedData.fromDynamic(serialized);

        return if(value != null) value.toBinary() else null;
    }
}