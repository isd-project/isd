package discovery.domain.serialization;

import discovery.domain.Announcement.AnnouncementValidity;


class AnnouncementSerializer implements Serializer<Announcement> {
    var descriptionSerializer:DescriptionSerializer;
    var validitySerializer:ValidityDeserializer;

    public function new() {
        descriptionSerializer = new DescriptionSerializer();
        validitySerializer = new ValidityDeserializer();
    }

    public function serialize(value:Announcement):Any {
        if(value == null) return null;

        return {
            description: descriptionSerializer.serialize(value.description),
            validity   : validitySerializer.serialize(value.validity)
        };
    }

    public function deserialize(serialized:Dynamic):Announcement {
        if(serialized == null) return null;

        return Announcement.make(
            descriptionSerializer.deserialize(serialized.description),
            validitySerializer.deserialize(serialized.validity)
        );
    }
}
