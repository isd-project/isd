package discovery.domain.serialization;

import discovery.domain.Description.Attributes;
import discovery.domain.Description.Location;

class DescriptionSerializer implements Serializer<Description>{
    var locationSerializer:Serializer<Location>;
    var providerIdSerializer: ProviderIdSerializer;
    var binarySerializer:BinarySerializer;

    public function new() {
        locationSerializer = new LocationSerializer();
        providerIdSerializer = new ProviderIdSerializer();
        binarySerializer = new BinarySerializer();
    }

    public function serialize(value:Description):Any {
        return {
            name: value.name,
            type: value.type,
            providerId: providerIdSerializer.serialize(value.providerId),
            instanceId: binarySerializer.serialize(value.instanceId),

            location  : locationSerializer.serialize(value.location),
            attributes: value.attributes
        };
    }

    public function deserialize(serialized:Dynamic):Description {
        return {
            name: serialized.name,
            type: serialized.type,
            providerId: providerIdSerializer.deserialize(serialized.providerId),
            instanceId: binarySerializer.deserialize(serialized.instanceId),

            location: locationSerializer.deserialize(serialized.location),
            attributes: new Attributes(serialized.attributes)
        };
    }
}