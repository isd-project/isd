package discovery.domain;

import discovery.base.search.Stoppable;
import haxe.Exception;
import discovery.base.search.SearchFilter;

typedef FinishSearchEvent = {
    ?error: Exception
};

interface Search extends Stoppable{
    /**
        Event called when the object was published.
    **/
    var onFound(default, null) : Event<Discovered>;

    /**
        Event called when the search finishes.
    **/
    var onceFinished(default, null) : Event<FinishSearchEvent>;


    /**
        Add filter to exclude the discovered instances that match some condition.

        @return the own search instance, to allow chaining calls.
    **/
    function applyFilter(filter:SearchFilter):Search;

    function stop(?callback:()->Void): Void;
}
