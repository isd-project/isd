package discovery.exceptions;

import haxe.Exception;

/**
    Thrown when a parameter for some operation, despite may not be invalid,
    is not supported by the current implementation.
**/
class UnsupportedParameterException extends Exception{
}