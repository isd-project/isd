package discovery.exceptions;

import haxe.Exception;

class IllegalStateException extends Exception{
    public static
    function require(condition:Bool, errMsg:String) {
        if(!condition){
            throw new IllegalStateException(errMsg);
        }
    }
}