package discovery.exceptions;

import discovery.keys.storage.exceptions.EntryNotFoundException;

class KeyNotFoundException extends EntryNotFoundException
{}