package discovery.exceptions;

import haxe.Exception;

class IllegalArgumentException extends ClientErrorException{
    inline public static
        function verify(requiredCondition:Bool, argument:String, cause:String)
    {
        require(requiredCondition, buildMessage(argument, cause));
    }

    inline public static
        function require(requiredCondition:Bool, message: String)
    {
        if(!requiredCondition){
            throw new IllegalArgumentException(message);
        }
    }

    public static function buildMessage(argument:String, cause:String) {
        return 'Argument "$argument" is invalid because: $cause';
    }
}