package discovery.exceptions.composite;

import haxe.Exception;
import discovery.utils.functional.FailuresComposer.CompositeException;

class PartialFailureException extends CompositeException{
    public var totalOperations(default, null):Int;

    public function new(exceptions: Array<Null<Exception>>, ?msg:String) {
        var filteredExceptions = filterExceptions(exceptions);

        if(msg == null)
            msg = makeMessage(filteredExceptions, exceptions.length);

        super(filteredExceptions, msg);

        this.totalOperations = exceptions.length;
    }

    static function makeMessage(exceptions:Array<Exception>, total: Int)
    {
        var numFailures = exceptions.length;

        return 'Partial failure: ${numFailures} of ${total} failed.'
            + failureMessages(exceptions);
    }

    static function
        filterExceptions(exceptions:Array<Null<Exception>>):Array<Exception>
    {
        return exceptions.filter((e)->e != null);
    }

    static function failureMessages(exceptions:Array<Exception>) {
        var buf = new StringBuf();

        for (i in 0...exceptions.length){
            var e = exceptions[i];

            buf.add('\n\t');
            buf.add(exceptionSummary(e, i + 1));
        }

        return buf.toString();
    }

    override function details():String {
        var buf = new StringBuf();

        buf.add(getTypename(this));
        buf.add(':\n');

        for (i in 0...exceptions.length){
            var e = exceptions[i];

            buf.add('\n');
            buf.add(exceptionSummary(e, i + 1));
            buf.add('\n');
            buf.add(e.details());
        }

        return buf.toString();
    }

    static function exceptionSummary(e: Exception, number: Int) {
        var typename = getTypename(e);

        return 'failure ${number} = ${typename}: "${e}"';
    }

    static function getTypename(obj: Dynamic) {
        return Type.getClassName(Type.getClass(obj));
    }
}