package discovery.exceptions;

import haxe.Exception;

/** Represents an error caused by the client of current class **/
class ClientErrorException extends Exception{}