package discovery.exceptions;

import haxe.Exception;

class NullAttributeException extends Exception{
    public static function verify(dependency:Dynamic, dependencyName:String) {
      if(dependency == null){
          throw new NullAttributeException(buildMessage(dependencyName));
      }
    }

    public static function buildMessage(dependencyName:String) {
        return 'Required class attribute "$dependencyName" is null!';
    }
}