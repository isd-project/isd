package discovery.exceptions.authentication;

import haxe.Exception;

class AuthenticationFailure extends Exception
{}