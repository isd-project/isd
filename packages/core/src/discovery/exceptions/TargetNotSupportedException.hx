package discovery.exceptions;

import haxe.Exception;

class TargetNotSupportedException extends Exception{}