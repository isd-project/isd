package discovery.exceptions;

import haxe.Exception;

class InternalErrorException extends Exception{}