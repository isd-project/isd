package discovery.exceptions;

import haxe.Exception;

class PublicationException extends Exception{
    public var cause(default, null):Dynamic;

    public function new(msg:String, ?cause:Dynamic, ?previous:Exception) {
        super(msg, previous);

        this.cause = cause;
    }
}