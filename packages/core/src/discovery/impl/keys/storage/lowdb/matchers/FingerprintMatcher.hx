package discovery.impl.keys.storage.lowdb.matchers;

import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import discovery.exceptions.IllegalArgumentException;
import haxe.extern.EitherType;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.storage.KeyData;
import discovery.impl.keys.storage.lowdb.matchers.Matcher;


private typedef SKeyData = SerializableKeyData;

@:forward
abstract FingerprintMatcher(Matcher<SKeyData>) from Matcher<SKeyData> to Matcher<SKeyData>{
    public function new(m: Matcher<SKeyData>) {
        this = m;
    }

    @:from
    public static function fromFingeprintOrPrefix(value: EitherType<Fingerprint, String>): FingerprintMatcher {
        if(Std.isOfType(value, Fingerprint)){
            return fromFingerprint(value);
        }
        else if(Std.isOfType(value, String) || value == null){
            return fromPrefix(value);
        };

        throw new IllegalArgumentException('${Std.string(value)} is should be a Fingeprint or prefix String');
    }

    @:from
    public static function fromFingerprint(f: Fingerprint): FingerprintMatcher {
        return new FingerprintKeyMatcher(f);
    }


    @:from
    public static function fromPrefix(prefix: String): FingerprintMatcher {
        return new FingerprintPrefixMatcher(prefix);
    }
}