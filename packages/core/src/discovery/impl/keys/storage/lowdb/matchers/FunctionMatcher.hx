package discovery.impl.keys.storage.lowdb.matchers;

class FunctionMatcher<T> implements Matcher<T>{
    var callback: (T)->Bool;

    public function new(callback: (T)->Bool) {
        this.callback = callback;
    }

    public function match(value:T):Bool {
        return this.callback(value);
    }
}