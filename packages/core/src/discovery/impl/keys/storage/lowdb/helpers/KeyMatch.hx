package discovery.impl.keys.storage.lowdb.helpers;

import discovery.impl.keys.storage.lowdb.matchers.Matcher;


private typedef SKeyData = SerializableKeyData;

@:structInit
class KeyMatch {
    public var keys:Array<SKeyData>;

    @:optional
    public var idx:Null<Int>;

    @:optional
    public var matcher:Matcher<SKeyData>;

    public var key(get, never):SKeyData;


    public function new(keys: Array<SKeyData>, ?matcher:Matcher<SKeyData>, ?idx:Int) {
        this.keys = keys;
        this.matcher = matcher;
        this.idx = idx;
    }

    function get_key(): SKeyData {
        return if(idx == null) null else keys[idx];
    }

    public function found(): Bool {
        return idx != null;
    }

    public function set(idx:Int) {
        this.idx = idx;
    }
}