package discovery.impl.keys.storage.lowdb.matchers;

import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import haxe.io.Bytes;
import discovery.keys.storage.KeyData;

using StringTools;


private typedef SKeyData = SerializableKeyData;

class FingerprintPrefixMatcher implements Matcher<SKeyData>{
    var prefix:String;

    public function new(prefix: String) {
        this.prefix = prefix;
    }

    public function match(key:SKeyData):Bool {
        return prefix == null
            || (key != null
            && key.fingerprint != null
            && matchPrefix(key.fingerprint.hash));
    }

    function matchPrefix(hash:Bytes) {
        return hash != null
            && hash.toHex().startsWith(prefix);
    }
}