package discovery.impl.keys.storage.lowdb;

import discovery.keys.storage.KeyData;

@:forward
abstract SerializableKeyData(KeyDataStruct) from KeyDataStruct to KeyDataStruct{
    public function new(struct: KeyDataStruct) {
        this = struct;
    }

    @:from
    public static function fromKeyData(keyData: KeyData): SerializableKeyData {
        return new KeyDataParser().toStructure(keyData);
    }

    @:to
    public function toKeyData():KeyData {
        return new KeyDataParser().toData(this);
    }

    public static function fromDynamic(serialized:Dynamic):SerializableKeyData {
        return KeyDataStruct.fromDynamic(serialized);
    }
}

