package discovery.impl.keys.storage.lowdb;

import discovery.keys.serialization.SerializedKeyType;
import discovery.keys.serialization.SerializedKey;
import discovery.keys.storage.ExportedFingerprint;

using discovery.utils.Cast;


@:structInit
class KeyDataStruct{
    public var keyType: SerializedKeyType;

    @:optional
    public var fingerprint: ExportedFingerprint;

    @:optional
    public var name: String;

    @:optional
    public var privateKey: Null<SerializedKey>;

    @:optional
    public var publicKey: SerializedKey;

    public static function fromDynamic(dynObj:Dynamic):KeyDataStruct {
        if(dynObj == null) return null;

        return {
            name: Cast.castIfIs(dynObj.name, String),

            keyType    : SerializedKeyType.fromDynamic(dynObj.keyType),
            fingerprint: ExportedFingerprint.fromDynamic(dynObj.fingerprint),

            privateKey: SerializedKey.fromDynamic(dynObj.privateKey),
            publicKey : SerializedKey.fromDynamic(dynObj.publicKey)
        };
    }
}