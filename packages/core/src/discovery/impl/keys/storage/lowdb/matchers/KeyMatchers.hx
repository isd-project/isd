package discovery.impl.keys.storage.lowdb.matchers;

import discovery.keys.KeyIdentifier;
import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import discovery.keys.crypto.Fingerprint;
import haxe.extern.EitherType;

import discovery.impl.keys.storage.lowdb.matchers.Matchers.*;

private typedef FingerprintOrPrefix = EitherType<Fingerprint, String>;

private typedef SKeyData = SerializableKeyData;

class KeyMatchers {
    public static function matchName(name: String): Matcher<SKeyData> {
        return new FunctionMatcher((key: SKeyData)->{
            return key != null
                && name != null //does not match if name is null
                && name == key.name;
        });
    }

    public static function matchFingerprint
        <T:EitherType<Fingerprint, String>>(queryOrPrefix: T): Matcher<SKeyData>
    {
        var matcher:FingerprintMatcher = queryOrPrefix;
        return matcher;
    }

    public static
    function matchFingerprintOrName(fing: FingerprintOrPrefix, name:String)
    {
        return or(
            matchFingerprint(fing),
            matchName(name)
        );
    }

    public static
    function matchKeyIdentifier(keyIdentifier:KeyIdentifier):Matcher<SKeyData> {
        if(keyIdentifier.isText()){
            var prefixOrName = keyIdentifier.toString();
            return or(
                matchFingerprint(prefixOrName),
                matchName(prefixOrName)
            );
        }
        else{
            return matchFingerprint(keyIdentifier.toBytes().toHex());
        }
    }
}