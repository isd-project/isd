package discovery.impl.keys.storage.lowdb.matchers;

import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import haxe.io.Bytes;
import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.storage.KeyData;

private typedef SKeyData = SerializableKeyData;


class FingerprintKeyMatcher implements Matcher<SKeyData>{
    var fingerprint:Fingerprint;

    public function new(fingeprint: Fingerprint) {
        this.fingerprint = fingeprint;
    }

    public function match(key:SKeyData):Bool {
        return fingerprint == null
            || (key != null
            && key.fingerprint != null
            && matchAlg(key.fingerprint.alg)
            && matchHash(key.fingerprint.hash));
    }

    function matchAlg(alg:HashTypes): Bool {
        return fingerprint.alg == null || fingerprint.alg == alg;
    }

    function matchHash(hash:Bytes) {
        return fingerprint.hash == null
            || (hash != null
            && hash.compare(fingerprint.hash) == 0);
    }
}