package discovery.impl.keys.storage.lowdb;

import discovery.keys.serialization.SerializedKeyType;
import discovery.keys.serialization.SerializedKey;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.storage.KeyData;

using haxe.EnumTools.EnumValueTools;
using haxe.EnumTools.EnumTools;


class KeyDataParser {
    public function new() {}
    public function toStructure(keyData: KeyData): KeyDataStruct {
        if(keyData == null) return null;

        return {
            keyType: SerializedKeyType.fromKeyType(keyData.keyType),
            name: keyData.name,
            fingerprint: keyData.fingerprint,
            privateKey: keyToStructure(keyData.privateKey),
            publicKey: keyToStructure(keyData.publicKey)
        };
    }

    public function toData(serialized:KeyDataStruct):KeyData {
        if(serialized == null) return null;

        return {
            keyType: SerializedKeyType.convertToKeyType(serialized.keyType),
            name: serialized.name,
            fingerprint: serialized.fingerprint,
            publicKey: keyFromStructure(serialized.publicKey),
            privateKey: keyFromStructure(serialized.privateKey)
        };
    }

    function keyToStructure(key: ExportedKey): SerializedKey{
        return SerializedKey.fromExportedKey(key);
    }

    function keyFromStructure(key: SerializedKey): ExportedKey{
        if(key == null) return null;

        return key.toExportedKey();
    }
}