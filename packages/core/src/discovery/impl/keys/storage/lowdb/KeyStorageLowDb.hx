package discovery.impl.keys.storage.lowdb;

import discovery.impl.keys.storage.lowdb.helpers.KeyMatch;
import discovery.impl.keys.storage.lowdb.helpers.LowDbCreateOptions;
import discovery.keys.KeyIdentifier;
import discovery.impl.keys.storage.lowdb.KeyDataStruct;
import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import discovery.impl.keys.storage.lowdb.matchers.Matcher;
import discovery.keys.storage.exceptions.NonUniqueIdentifierException;
import discovery.keys.storage.exceptions.DuplicateEntryException;
import discovery.keys.storage.exceptions.EntryNotFoundException;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.storage.KeyData;
import discovery.keys.storage.KeyStorage;


import discovery.impl.keys.storage.lowdb.matchers.KeyMatchers.*;

private typedef SKeyData = SerializableKeyData;


class KeyStorageLowDb implements KeyStorage {
    static public var dbfilename(default, null): String = 'keys.json';

    var db: HaxeLow;

    public function new(dbCreator: LowDbCreateOptions) {
        IllegalArgumentException.require(dbCreator != null,
            'KeyStorageLowDb: create options should not be null');

        this.db = dbCreator.build();
    }

    public function listKeys():Array<KeyData> {
        return [for(k in _listKeys()) convertKey(k)];
    }

    function convertKey(k:Any):KeyData {
        return SerializableKeyData.fromDynamic(k);
    }

    public function addKey(key:KeyData) {
        requireKeyHasFingerprint(key);

        var match = findKey(matchFingerprintOrName(key.fingerprint, key.name));

        if(match.found()){
            throw new DuplicateEntryException("Key already exist");
        }

        match.keys.push(key);
        db.save();
    }

    public function getKey(keyIdentifier:KeyIdentifier):KeyData {
        requireValidKeyIdentifier(keyIdentifier);

        return findOne(matchKeyIdentifier(keyIdentifier));
    }

    function findOne(matcher: Matcher<SKeyData>) {
        var match = findKey(matcher);

        requireNotDuplicated(match);

        return match.key;
    }

    public function update(key:KeyData) {
        requireKeyHasFingerprint(key);

        var match = findKey(matchFingerprintOrName(key.fingerprint, key.name));

        requireKeyFound(match);
        requireUpdateKeepUniqueness(match, key);

        var idx = match.idx;
        match.keys[idx] = key;
        db.save();
    }

    public function remove(keyId: KeyIdentifier) {
        requireValidKeyIdentifier(keyId);

        var match = findKey(matchKeyIdentifier(keyId));

        requireKeyFound(match);

        match.keys.splice(match.idx, 1);
        db.save();
    }

    // ---------------------------------------------------------------------

    function findKey(query: Matcher<SKeyData>, ?options: {startIdx: Int}): KeyMatch {
        options = if(options != null) options else {startIdx: 0};

        var keys = _listKeys();
        var match = new KeyMatch(keys, query);
        var start = options.startIdx;

        for (i in start...keys.length){
            var key = keys[i];

            if(query.match(key)){
                match.set(i);
                return match;
            }
        }

        return match;
    }

    function continueFind(match: KeyMatch): KeyMatch{
        if(!match.found()){
            return match;
        }
        return findKey(match.matcher, {startIdx: match.idx + 1});
    }


    function _listKeys(): Array<SKeyData> {
        var keys = db.col(KeyDataStruct);

        return keys;
    }

    // ---------------------------------------------------------------------

    function requireKeyFound(match: KeyMatch) {
        if(!match.found()){
            throw new EntryNotFoundException('Key was not found');
        }
    }

    function requireKeyHasFingerprint(key: KeyData) {
        IllegalArgumentException.verify(key !=null, "key", "key is null");
        IllegalArgumentException.verify(
            key.fingerprint !=null && key.fingerprint.hash != null,
            "key", "key has no fingerprint");
    }

    function requireNotDuplicated(match: KeyMatch) {
        if(continueFind(match).found()){
            throw new NonUniqueIdentifierException("Query was not unique");
        }
    }

    function requireUpdateKeepUniqueness(match: KeyMatch, updateKey: KeyData)
    {
        if(updateKey.name == null){
            return;
        }

        if(match.found() && continueFind(match).found()){
            throw new DuplicateEntryException("cannot update key, because name was already taken");
        }
    }

    function requireValidKeyIdentifier(keyIdentifier:KeyIdentifier) {
        IllegalArgumentException.verify(keyIdentifier !=null, "keyIdentifier",
            "keyIdentifier should not be null");
    }
}