package discovery.impl.keys.storage.lowdb.matchers;

class Matchers {
    public static function and<T>(m1: Matcher<T>, m2: Matcher<T>) {
        return new FunctionMatcher((value: T)->{
            return m1.match(value) && m2.match(value);
        });
    }
    public static function or<T>(m1: Matcher<T>, m2: Matcher<T>) {
        return new FunctionMatcher((value: T)->{
            return m1.match(value) || m2.match(value);
        });
    }
}