package discovery.impl.keys.storage.lowdb.matchers;

interface Matcher<T> {
    function match(value: T): Bool;
}