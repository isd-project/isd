package discovery.impl.keys.storage.lowdb.helpers;

import discovery.exceptions.IllegalArgumentException;
import haxe.io.Path;
import sys.FileSystem;
import HaxeLow.HaxeLowDisk;

using StringTools;

typedef LowDbCreator = ()->HaxeLow;

typedef FileAndDir = {
    ?file: String,
    dir: String
};

abstract LowDbCreateOptions(LowDbCreator) from LowDbCreator to LowDbCreator{
    public function build(): HaxeLow {
        return if(this == null) null else this();
    }

    @:from
    public static function fromLowDb(haxelow: HaxeLow): LowDbCreateOptions {
        return ()->{ return haxelow;};
    }

    @:from
    public static function fromDirPath(dirpath: String) {
        return fromFileAndDir({
            dir: dirpath
        });
    }


    @:from
    public static function
        fromFileAndDir(fileAndDir: FileAndDir): LowDbCreateOptions
    {
        var dirpath = fileAndDir.dir;
        var filename = fileAndDir.file;

        IllegalArgumentException.require(dirpath != null,
            'Cannot create LowDb with null directory'
            );

        var disk:HaxeLowDisk = null;
        #if (sys || nodejs)
            disk = new HaxeLow.SysDisk();
        #elseif(js)
            disk = new HaxeLow.LocalStorageDisk();
        #else
            throw new haxe.Exception("Default disk not supported in this target");
        #end

        var filepath = makeDbFilepath(dirpath, filename);
        return ()->{
            return new HaxeLow(filepath, disk);
        };
    }

    public static function makeDbFilepath(dirpath: String, ?file:String) {
        if(file == null){
            file = KeyStorageLowDb.dbfilename;
        }
        else{
            file = fixExtension(file);
        }

        FileSystem.createDirectory(dirpath);
        return Path.join([dirpath, file]);
    }

    static function fixExtension(file: String){
        if(!file.endsWith('.json')){
            return file + '.json';
        }

        return file;
    }
}