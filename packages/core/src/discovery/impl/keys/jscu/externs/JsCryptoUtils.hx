package discovery.impl.keys.jscu.externs;

import discovery.impl.keys.jscu.externs.JscuKey.JscuExternKey;
import discovery.keys.crypto.ExportedKey;

class JsCryptoUtils {
    #if js
    public static function pkc(): PublicKeyCrypto {
        return js.Lib.require("js-crypto-utils").pkc;
    }

    public static function makeKey(exportedKey:ExportedKey):JscuKey {
        var formatName = exportedKey.format.getName().toLowerCase();
        var data = JscuKeyData.fromExportedKey(exportedKey);

        return new JscuExternKey(formatName, data);
    }

    #end

}