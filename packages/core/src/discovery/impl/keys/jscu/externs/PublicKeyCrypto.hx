package discovery.impl.keys.jscu.externs;

import discovery.async.promise.PromiseType;
import discovery.async.promise.Promisable;

interface PublicKeyCrypto {
    function generateKey(keyType: String, params: Dynamic): PromiseType<JscuKeyPair>;
}