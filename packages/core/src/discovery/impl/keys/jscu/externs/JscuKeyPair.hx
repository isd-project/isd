package discovery.impl.keys.jscu.externs;

typedef JscuKeyPair = {
    publicKey: JscuKey,
    privateKey: JscuKey
};