package discovery.impl.keys.jscu.externs;

import discovery.impl.keys.jscu.externs.JscuKeyData;
import discovery.async.promise.PromiseType;


typedef KeyFormat = String;

typedef JscuHashTypes = String;
typedef JwkThumbprintFormat = String;

private typedef Promise<T> = PromiseType<T>;


interface JscuKey {
    // properties ---------------------------
    var isEncrypted(default, null): Bool;
    var isPrivate(default, null)  : Bool;
    var keyType(default, null):Promise<String>;

    // thumbprint ------------------------------------------

    function jwkThumbprint(): Promise<EitherByteOrString>;
    function getJwkThumbprint(alg: JscuHashTypes ='SHA-256', output: JwkThumbprintFormat='binary'): Promise<EitherByteOrString>;

    // encrypt/decrypt ------------------------------------------

    function encrypt (passphrase: String): Promise<Bool>;
    function decrypt (passphrase: String): Promise<Bool>;

    // export ------------------------------------------
    function der(): Promise<DER>;
    function pem(): Promise<PEM>;
    function jwk(): Promise<JsonWebKey>;
    function oct(): Promise<OctetEC>;

    function export(format: KeyFormat = 'jwk', ?options: KeyExportOptions): Promise<JscuKeyData>;
}

@:jsRequire('js-crypto-utils', 'Key')
extern class JscuExternKey implements JscuKey{
    public function new(format: KeyFormat, keyData: ExportType);

    public var isEncrypted(default, null):Bool;
    public var isPrivate(default, null):Bool;
    public var keyType(default, null):Promise<String>;

    public function jwkThumbprint():Promise<EitherByteOrString>;
    public function getJwkThumbprint(?alg:JscuHashTypes, ?output:JwkThumbprintFormat):Promise<EitherByteOrString>;

    public function encrypt(passphrase:String):Promise<Bool>;
    public function decrypt(passphrase:String):Promise<Bool>;

    public function der():Promise<DER>;
    public function pem():Promise<PEM>;
    public function jwk():Promise<JsonWebKey>;
    public function oct():Promise<OctetEC>;

    public function export(?format:KeyFormat, ?options:KeyExportOptions):Promise<JscuKeyData>;
}