package discovery.impl.keys.jscu.externs;


/**
 * @typedef {Object} KeyExportOptions - Export options for Key Class.
 * @property {boolean} [outputPublic] - Derive public key from private key if true.
 * @property {boolean} [compact] - Generate compressed EC public key when format = 'der', 'pem' or 'oct', only for EC key if true.
 * @property {OctetFormat} [output='binary'] - Active only for OctetEC key. 'binary' or 'string'. Default value would be 'binary'.
 * @property {AsnEncryptOptionsWithPassphrase} [encryptParams] - Generate encrypted der/pem private key when format = 'der' or 'pem'.
 */

 typedef KeyExportOptions = {
    ?outputPublic : Bool,
    ?compact      : Bool,
    ?output       : OctetFormat,
    ?encryptParams: AsnEncryptOptionsWithPassphrase
};

/**
 * @typedef {'string'|'binary'} OctetFormat - Representation of SEC1 Octet EC key.
 */
enum abstract OctetFormat(String){
    var string = 'string';
    var binary = 'binary';
}

/**
 * @typedef {Object} AsnEncryptOptionsWithPassphrase - Encryption options for exported key in key Class.
 * @property {String} passphrase - (Re-)generate encrypted der/pem with the given passphrase
 * @property {String} [algorithm='pbes2'] - 'pbes2' (default), 'pbeWithMD5AndDES-CBC' or 'pbeWithSHA1AndDES'
 * @property {String} [prf='hmacWithSHA256] - 'hmacWithSHA256' (default), 'hmacWithSHA384', 'hmacWithSHA512' or 'hmacWithSHA1' when if algorithm = 'pbes2'.
 * @property {Number} [iterationCount = 2048] - Iteration count for PBKDF 1/2.
 * @property {String} [cipher='aes256-cbc'] - 'aes256-cbc' (default), 'aes128-cbc' or 'des-ede3-cbc'.
 */
typedef AsnEncryptOptionsWithPassphrase = {
  passphrase     : String,
  ?algorithm     : AsnEncryptPassphraseAlgorithms,
  ?prf           : AsnEncryptPassphrasePrf,
  ?iterationCount: Int,
  ?cipher        : AsnEncryptPassphraseCipher
};

enum abstract AsnEncryptPassphraseAlgorithms(String){
    var pbes2                = 'pbes2';
    var pbeWithMD5AndDES_CBC = 'pbeWithMD5AndDES-CBC';
    var pbeWithSHA1AndDES    = 'pbeWithSHA1AndDES';
}

enum abstract AsnEncryptPassphrasePrf(String){
    var hmacWithSHA256 = 'hmacWithSHA256';
    var hmacWithSHA384 = 'hmacWithSHA384';
    var hmacWithSHA512 = 'hmacWithSHA512';
    var hmacWithSHA1   = 'hmacWithSHA1';
}

enum abstract AsnEncryptPassphraseCipher(String){
    var aes256_cbc   = 'aes256-cbc';
    var aes128_cbc   = 'aes128-cbc';
    var des_ede3_cbc = 'des-ede3-cbc';
}