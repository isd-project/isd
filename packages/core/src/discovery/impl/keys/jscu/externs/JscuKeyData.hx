package discovery.impl.keys.jscu.externs;

import haxe.io.Bytes;
import discovery.format.BytesOrText;
import haxe.Json;
import discovery.keys.crypto.ExportedKey;
import haxe.io.UInt8Array;
import haxe.extern.EitherType;

private typedef Object = Dynamic;

typedef EitherByteOrString = EitherType<UInt8Array, String>;

typedef JsonWebKey = Object;
typedef PEM        = String;
typedef DER        = UInt8Array;
typedef OctetEC    = EitherByteOrString;
typedef ExportType = EitherType<
                        EitherType<PEM, DER>,
                        EitherType<JsonWebKey, OctetEC>>;

abstract JscuKeyData(ExportType) from ExportType to ExportType {

    @:from
    public static function fromExportedKey(exportedKey:ExportedKey):JscuKeyData{
        var keyData:JscuKeyData = exportedKey.keyData();

        if(exportedKey.format == Jwk && Std.isOfType(keyData, String)){
            keyData = Json.parse(keyData);
        }
        else if(exportedKey.format == Der && Std.isOfType(keyData, Bytes)){
            keyData = UInt8Array.fromBytes(keyData);
        }

        return keyData;
    }

    public function toExportedKey(
        format: discovery.keys.crypto.KeyFormat): ExportedKey
    {
        var data = toBytesOrText();
        return new ExportedKey(data, format);
    }

    @:to
    public function toBytesOrText(): BytesOrText {
        if(Std.isOfType(this, String)){
            return (this:String);
        }
        else if(Std.isOfType(this, UInt8ArrayData)){//binary
            var binaryData = UInt8Array.fromData((this:UInt8ArrayData));
            return binaryData.view.buffer;
        }
        else{ //Jwk (object)
            return Json.stringify(this);
        }
    }
}