package discovery.impl.keys.jscu.crypto;

import js.node.crypto.Sign;
import discovery.keys.crypto.ExportedKey;
import js.node.Buffer;
import discovery.keys.crypto.CryptoKey;
import js.node.Crypto;
import discovery.keys.crypto.HashTypes;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.signature.Signature;
import haxe.io.Bytes;
import discovery.keys.crypto.signature.Signer;

#if (js && nodejs)
class JscuNodeSigner implements Signer{

    var key:CryptoKey;
    public var nodeSign:js.node.crypto.Sign;

    public function new(key: CryptoKey, ?alg: HashTypes, ?sign:Sign) {
        alg = if(alg != null) alg else HashTypes.Sha256;

        this.key = key;
        nodeSign = Crypto.createSign(NodeHashAlgs.convert(alg));
    }

    public function end():Promisable<Signature> {
        return key.export(Pem).then(function (exportedKey:ExportedKey):Signature{
            var binary:Buffer = nodeSign.sign(exportedKey.keyData());
            return binary.hxToBytes();
        });
    }

    public function appendData(data:Bytes) {
        var buffer = Buffer.hxFromBytes(data);
        nodeSign.update(buffer);
    }

    public function appendString(value:String) {
        nodeSign.update(value);
    }
}
#end