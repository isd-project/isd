package discovery.impl.keys.jscu.crypto;

import discovery.keys.crypto.HashTypes;

class NodeHashAlgs {
    public static function convert(alg:HashTypes):String {
        //Algorithms supported on Node 16.0.0
        // 'RSA-MD4', 'RSA-MD5', 'RSA-MDC2', 'RSA-RIPEMD160', 'RSA-SHA1', 'RSA-SHA1-2', 'RSA-SHA224', 'RSA-SHA256', 'RSA-SHA3-224', 'RSA-SHA3-256', 'RSA-SHA3-384', 'RSA-SHA3-512', 'RSA-SHA384', 'RSA-SHA512', 'RSA-SHA512/224', 'RSA-SHA512/256', 'RSA-SM3', 'blake2b512', 'blake2s256', 'id-rsassa-pkcs1-v1_5-with-sha3-224', 'id-rsassa-pkcs1-v1_5-with-sha3-256', 'id-rsassa-pkcs1-v1_5-with-sha3-384', 'id-rsassa-pkcs1-v1_5-with-sha3-512', 'md4', 'md4WithRSAEncryption', 'md5', 'md5-sha1', 'md5WithRSAEncryption', 'mdc2', 'mdc2WithRSA', 'ripemd', 'ripemd160', 'ripemd160WithRSA', 'rmd160', 'sha1', 'sha1WithRSAEncryption', 'sha224', 'sha224WithRSAEncryption', 'sha256', 'sha256WithRSAEncryption', 'sha3-224', 'sha3-256', 'sha3-384', 'sha3-512', 'sha384', 'sha384WithRSAEncryption', 'sha512', 'sha512-224', 'sha512-224WithRSAEncryption', 'sha512-256', 'sha512-256WithRSAEncryption', 'sha512WithRSAEncryption', 'shake128', 'shake256', 'sm3', 'sm3WithRSAEncryption', 'ssl3-md5', 'ssl3-sha1', 'whirlpool'

        return switch (alg){
            case Sha256  : 'sha256';
            case Sha384  : 'sha384';
            case Sha512  : 'sha512';
            case Sha3_512: 'sha3-512';
            case Sha3_384: 'sha3-384';
            case Sha3_256: 'sha3-256';
            case Sha3_224: 'sha3-224';
            case Sha1    : 'sha1';
            case Md5     : 'md5';

            //allows the client to specify an algorithm not defined by the enum
            default: (alg : String);
        };
    }
}