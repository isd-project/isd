package discovery.impl.keys.jscu.crypto;

import js.node.Buffer;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.HashTypes;
import haxe.io.Bytes;
import discovery.keys.crypto.signature.Signature;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.signature.Verifier;

#if (js && nodejs)
class JscuNodeVerifier implements Verifier{

    var key:CryptoKey;
    public var nodeVerify:js.node.crypto.Verify;

    public function new(key: CryptoKey, ?alg: HashTypes) {
        alg = if(alg != null) alg else HashTypes.Sha256;

        this.key = key;
        nodeVerify = js.node.Crypto.createVerify(NodeHashAlgs.convert(alg));
    }

    public function appendData(data:Bytes) {
        var buffer = Buffer.hxFromBytes(data);
        nodeVerify.update(buffer);
    }

    public function appendString(value:String) {
        nodeVerify.update(value);
    }

    public function verify(signature:Signature):Promisable<Verify> {
        return key.export(Pem).then(function (exportedKey:ExportedKey):Verify{
            var keyData = exportedKey.keyData();
            var signatureBuffer:Buffer = Buffer.hxFromBytes(signature);

            return nodeVerify.verify(keyData, signatureBuffer);
        });
    }
}
#end