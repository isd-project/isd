package discovery.impl.keys.jscu.crypto;

import discovery.keys.crypto.signature.Verifier;
import discovery.exceptions.UnsupportedParameterException;
import discovery.impl.keys.jscu.externs.JsCryptoUtils;
import haxe.Json;
import discovery.keys.crypto.KeyFormats;
import discovery.format.BytesOrText;
import discovery.keys.crypto.KeyFormat;
import discovery.exceptions.TargetNotSupportedException;
import discovery.keys.crypto.signature.Signer;
import haxe.io.UInt8Array;
import haxe.io.Bytes;
import js.lib.Uint8Array;
import discovery.async.promise.js.JsPromiseAdapter;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.Fingerprint;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.KeyTypes;
import discovery.impl.keys.jscu.externs.JscuKey;
import discovery.impl.keys.jscu.externs.JscuKeyData;
import discovery.keys.crypto.CryptoKey;

using discovery.impl.keys.jscu.helpers.JscuKeyTransform;

class JscuCryptoKey implements CryptoKey{
    var key: JscuKey;

    public static
    function makeKey(exportedKey:ExportedKey):Promisable<CryptoKey>
    {
        return JsCryptoUtils
                .makeKey(exportedKey)
                .toCryptoKey();
    }

    public function new(jscuKey: JscuKey, keyType: KeyTypes) {
        this.key = jscuKey;
        this.keyType = keyType;
    }

    public var keyType(default, null): KeyTypes;

    public function isPrivate(): Bool{
        return key.isPrivate;
    }


    public function startSignature(?alg:discovery.keys.crypto.HashTypes):Signer {
        #if(js && nodejs)
            return new JscuNodeSigner(this, alg);
        #else
            throw new TargetNotSupportedException("Only node.js supported for now");
        #end
    }

    public function
        startVerification(?alg:discovery.keys.crypto.HashTypes):Verifier
    {
        #if(js && nodejs)
            return new JscuNodeVerifier(this, alg);
        #else
            throw new TargetNotSupportedException("Only node.js supported for now");
        #end
    }

    public function fingerprint(): Promisable<Fingerprint>{
        var alg = discovery.keys.crypto.HashTypes.Sha256;
        return JsPromiseAdapter.wrap(
                    key.getJwkThumbprint(
                        cast alg,
                        "binary")
                    )
                .then(thumbprintToFingerprint.bind(_, alg));
    }

    function thumbprintToFingerprint(
        thumbprint:EitherByteOrString,
        alg:discovery.keys.crypto.HashTypes):Fingerprint
    {
        var hash:Bytes = null;
        if(Std.isOfType(thumbprint, Uint8Array)){
            var arrayBytes:UInt8Array = thumbprint;
            hash = arrayBytes.get_view().buffer;
        }
        else{
            hash = Bytes.ofString(thumbprint);
        }

        return {
            alg: alg,
            hash: hash
        };
    }

    public function export(?format:discovery.keys.crypto.KeyFormat):Promisable<ExportedKey> {
        if(format == null) format = Jwk;

        var jscuFormat = convertJscuKeyFormat(format);

        return JsPromiseAdapter
                .wrap(this.key.export(jscuFormat))
                .then((keyData)->{return keyData.toExportedKey(format);});
    }

    function exportedKeyDataToExportedKey(exported: JscuKeyData, format:discovery.keys.crypto.KeyFormat): ExportedKey
    {
        return new ExportedKey(exported.toBytesOrText(), format);
    }

    static function convertJscuKeyFormat(format:Null<discovery.keys.crypto.KeyFormat>): String
    {
        return switch (format){
            case Pem: 'pem';
            case Der: 'der';
            case Jwk: 'jwk';
            default: throw new UnsupportedParameterException('format "$format" is not supported by JscuCryptoKey.export');
        }
    }
}