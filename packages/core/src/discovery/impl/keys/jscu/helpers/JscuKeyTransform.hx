package discovery.impl.keys.jscu.helpers;

import discovery.keys.crypto.KeyPair;
import discovery.impl.keys.jscu.externs.JscuKeyPair;
import discovery.async.promise.PromiseAbs;
import discovery.keys.crypto.KeyTypes;
import discovery.impl.keys.jscu.crypto.JscuCryptoKey;
import discovery.keys.crypto.CryptoKey;
import discovery.async.promise.Promisable;
import discovery.exceptions.UnsupportedParameterException;
import discovery.impl.keys.jscu.externs.JscuKey;

class JscuKeyTransform {
    public static
    function toCryptoKey(jscuKey:JscuKey): Promisable<CryptoKey>
    {
        return PromiseAbs.wrap(
            jscuKey.keyType.then(composeCryptoKey.bind(jscuKey, _))
        );
    }

    public static function nameToKeyType(keyTypeName:String) {
        return switch(keyTypeName.toUpperCase()){
            case 'EC' : EllipticCurve();
            case 'RSA': RSA();
            default   : null;
        };
    }

    public static function keyTypeToString(keyType: KeyTypes): String{
        return switch (keyType){
            case EllipticCurve(_): return 'EC';
            case RSA(_)          : return 'RSA';
            default: throw new UnsupportedParameterException(
                'Key type ${keyType} is not supported by js-crypto-utils');
        }
    }

    public static function toCryptoKeyPair(
        jsKeyPair: JscuKeyPair, keyType:KeyTypes): KeyPair
    {
        return {
            privateKey: new JscuCryptoKey(jsKeyPair.privateKey, keyType),
            publicKey: new JscuCryptoKey(jsKeyPair.publicKey, keyType)
        };
    }

    // -------------------------------------------------------------------

    static function composeCryptoKey(
        jscuKey: JscuKey, keyTypeName: String): CryptoKey
    {
        var keyType =  nameToKeyType(keyTypeName);
        return new JscuCryptoKey(jscuKey, keyType);
    }
}