package discovery.impl.keys.jscu;

import discovery.async.promise.js.JsPromiseAdapter;
import discovery.keys.crypto.KeyType;
import discovery.async.promise.Promisable;
import discovery.async.promise.PromiseAbs;
import discovery.exceptions.NullAttributeException;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.crypto.KeyPair;
import discovery.keys.storage.KeyData;
import discovery.impl.keys.jscu.externs.JsCryptoUtils;
import discovery.impl.keys.jscu.externs.PublicKeyCrypto;
import discovery.impl.keys.jscu.helpers.JscuKeyTransform;
import discovery.impl.keys.jscu.helpers.JscuKeyTransform.*;


class JscuKeyGenerator implements KeyGenerator{
    var pkc:PublicKeyCrypto;

    public function new(?pkc: PublicKeyCrypto) {
        if(pkc == null){
            pkc = JsCryptoUtils.pkc();
        }

        this.pkc = pkc;
    }

    public function generateKey(options:GenerateKeyOptions):Promisable<KeyPair> {
        NullAttributeException.verify(pkc, "pkc <PublicKeyCrypto>");

        var keyTypeName = keyTypeToString(options.keyType);
        var keyParams   = genKeyParams(options);

        return PromiseAbs
                    .wrap(pkc.generateKey(keyTypeName, keyParams)
                    .then(toCryptoKeyPair.bind(_, options.keyType))
        );
    }

    public function loadKey(keyData:KeyData):Promisable<KeyPair> {
        if(keyData == null){
            return resolved(null);
        }
        return loadKeys(keyData)
                .then(combineIntoKeyPair);
    }

    function loadKeys(keyData:KeyData) {
        return loadPrivateKey(keyData.privateKey)
        .mergeWith([
            loadExportedKey(keyData.publicKey)
        ]);
    }

    function
        loadPrivateKey(exportedKey:Null<ExportedKey>): Promisable<CryptoKey>
    {
        if(exportedKey == null) return resolved(null);

        return loadExportedKey(exportedKey);
    }

    public
    function loadExportedKey(exportedKey:ExportedKey):Promisable<CryptoKey>
    {
        validateExportedKeyArgument(exportedKey);

        #if (js)
            var jscuKey = JsCryptoUtils.makeKey(exportedKey);
            return JscuKeyTransform.toCryptoKey(jscuKey);
        #else
            throw new discovery.exceptions.TargetNotSupportedException(
                "JscuKeyGenerator only supports Js");
        #end
    }

    // --------------------------------------------------------------------


    function genKeyParams(options: GenerateKeyOptions): Dynamic{
        var keyType = new KeyType(options.keyType);

        return keyType.getParamsObject();
    }

    function validateExportedKeyArgument(exportedKey:ExportedKey) {
        NullAttributeException.verify(exportedKey, "exportedKey");
        NullAttributeException.verify(exportedKey.format, "exportedKey.format");
        NullAttributeException.verify(exportedKey.data  , "exportedKey.data");
    }

    function combineIntoKeyPair(keys: Array<CryptoKey>):KeyPair{
        return {
            privateKey: keys[0],
            publicKey: keys[1]
        };
    }

    function resolved<T>(value: T):Promisable<T> {
        return JsPromiseAdapter.resolve(value);
    }
}