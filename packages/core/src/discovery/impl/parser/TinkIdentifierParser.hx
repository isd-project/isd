package discovery.impl.parser;

import haxe.ValueException;
import haxe.Exception;

import discovery.base.IdentifierParser;
import discovery.domain.IdentifierUrl;
import discovery.format.Formatter;
import discovery.format.Formatters;
import discovery.format.exceptions.InvalidFormatException;
import discovery.format.exceptions.InvalidValueException;
import discovery.format.exceptions.MissingFieldException;
import discovery.format.exceptions.ParseException;

import tink.Url;


private typedef SrvUrlParts = {
    nameParts: Array<String>,
    paramParts: Array<String>
};

class TinkIdentifierParser implements IdentifierParser{
    public function new() {}

    public function parseUrl(urlString:String):IdentifierUrl {
        var decoder = new SrvUrlDecoder(urlString);

        return decoder.decode();
    }
}

private class SrvUrlDecoder{
    var url:Url;

    var nameParts: Array<String>;
    var paramParts: Array<String>;

    public function new(urlString: String) {
        this.url = Url.parse(urlString, (err: String)->{
            throw new InvalidFormatException('Failed to parse url due to error: ${err}');
        });
    }

    public function decode():IdentifierUrl {
        splitUrl();
        verify(this.url.scheme == "srv", new InvalidValueException('scheme invalid "${url.scheme}"'));

        var parameters = parseParameters();
        var formatter  = getFormatter(parameters);

        return {
            name      : decodeName(),
            type      : decodeType(),
            providerId: decodeProviderId(formatter),
            instanceId: decodeInstanceId(formatter),
            parameters: parameters,
            path      : decodePath(),
            query     : decodeQuery(),
            fragment  : url.hash
        };
    }

    function verify(condition:Bool, exception:Exception) {
        if(!condition){
            throw exception;
        }
    }

    function splitUrl() {
        verify(url.host != null && url.host.toString().length > 0,
            new MissingFieldException("host"));

        // tink_url splits "," as different hosts, so we join them again
        var hostStr = url.hosts.map((host) -> host.toString()).join(",");

        // then, use parameters delimiter (";") to split url host parts
        var hostParts = hostStr.split(";");
        // the first part contains the "domain" (instance id, name, type, provider id) -> so we remove and return it from array
        var domain = hostParts.shift();

        this.nameParts = domain.split(".");
        nameParts.reverse();

        verify(nameParts.length >= 2, new MissingFieldException("type"));
        verify(nameParts.length >= 3, new MissingFieldException("name"));

        // the rest of the array contains the parameters
        this.paramParts = hostParts;
    }

    function parseParameters() {
        if(paramParts.length == 0){
            return null;
        }

        var parameters = new Map<String, String>();
        for (param in paramParts){
            var paramParts = param.split("=");
            var key = paramParts[0];
            var value = if(paramParts.length > 1) paramParts[1] else "";

            parameters.set(key, value);
        }

        return parameters;
    }

    function getFormatter(parameters: Map<String, String>) {
        var format    = if(parameters==null) null else parameters.get("fmt");

        var formatter = Formatters.getFormatter(format);

        if(formatter == null){
            //TO-DO: use specific exception
            throw new haxe.Exception('Unknown format "${format}"');
        }

        return formatter;
    }


    function tryDecode(formatter:Formatter, text:String, errMsg:String) {
        try{
            return formatter.decode(text);
        }
        catch(e: haxe.ValueException){
            throw new InvalidFormatException(errMsg, e);
        }
    }

    function decodeProviderId(formatter: Formatter) {
        return tryDecode(formatter, nameParts[0],
                            "Provider id value is invalid");
    }

    function decodeName() {
        try{
            return Formatters.percentEncoding.decode(nameParts[2]);
        }
        catch(e: haxe.Exception){
            throw new ParseException("Failed to decode identifier name", e);
        }
    }

    function decodeType() {
        return nameParts[1];
    }

    function decodeInstanceId(formatter: Formatter) {
        if(nameParts.length > 3){
            return tryDecode(formatter, nameParts[3],
                "Instance Id value is invalid");
        }

        return null;
    }

    function decodePath(): Array<String> {
        if(this.url.path == null){
            return null;
        }
        var parts = url.path.parts().toStringArray();

        return if(parts.length > 0) parts else null;
    }

    function decodeQuery(): Map<String, String> {
        var queryMap = this.url.query.toMap();

        if(!queryMap.keys().hasNext()){
            return null;
        }

        return [for(k=>v in queryMap) k=>v.toString()];
    }
}