package discovery.impl;

import discovery.impl.events.signal.SignalEventFactory;

typedef EventEmitterFactory = SignalEventFactory;

private typedef IEventEmitterFactory = discovery.utils.events.EventEmitterFactory;

class Defaults {


    public static function
        eventFactory(?factory:Null<IEventEmitterFactory>):IEventEmitterFactory
    {
        if(factory == null){
            factory = new EventEmitterFactory();
        }

        return factory;
    }
}