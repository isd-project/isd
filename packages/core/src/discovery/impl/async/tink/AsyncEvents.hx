package discovery.impl.async.tink;

import tink.core.Future;
import discovery.domain.Event;

class AsyncEvents {
    static public function nextAsync<T>(event: Event<T>): Future<T> {
        return new AsyncEventIterator(event, { once: true }).next();
    }
    
    static public function asyncIterator<T>(event: Event<T>): AsyncEventIterator<T> 
    {
        return new AsyncEventIterator(event);
    }
}