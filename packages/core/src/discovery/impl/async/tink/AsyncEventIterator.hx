package discovery.impl.async.tink;

import discovery.domain.Event;
import tink.core.Future;

private typedef Options = {
    once: Bool
};
typedef AsyncEventIteratorOptions = Options;

class AsyncEventIterator<T> implements AsyncIterator<T>{
    var event:Event<T>;
    var resolved:List<Future<T>>;
    var waiting:Null<FutureTrigger<T>>;
    var stopped:Bool=false;


    public function new(event: Event<T>, ?options:Options) {
        if (options == null){
            options = {once:false};
        }

        this.resolved = new List();

        this.event = event;
        this.event.listen(onEvent);
    }

    function onEvent(value: T) {
        // TO-DO: handle concurrency in waiting;
        if (waiting != null){
            waiting.trigger(value);
            waiting = null;
        }
        else {
            resolved.add(Future.sync(value));
        }
    }

    public function done():Bool {
        return stopped && resolved.isEmpty();
    }

    public function stop() {
        stopped = true;
        this.event.unlisten(onEvent);
    }

    public function next():Future<T> {
        // TO-DO: handle concurrency with resolved and waiting

        if(done()){
            return null;
        }

        var f = resolved.pop();
        if (f != null){
            return f;
        }
        else if (waiting == null){
            waiting = Future.trigger();
        }

        return waiting.asFuture();
    }
}