package discovery.impl.async.tink;

import tink.core.Future;

interface AsyncIterator<T> {
    function done(): Bool;
    function next(): Future<T>;
}