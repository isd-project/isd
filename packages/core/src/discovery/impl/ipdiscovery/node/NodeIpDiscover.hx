package discovery.impl.ipdiscovery.node;

import js.node.Os;
import discovery.async.promise.js.JsPromiseAdapter;
import discovery.async.promise.Promisable;
import discovery.domain.IPAddress;
import discovery.domain.IpDiscover;

using StringTools;


class NodeIpDiscover implements IpDiscover{
    public function new() {

    }

    public function listPublicIps():Promisable<Array<IPAddress>> {
        return JsPromiseAdapter.makePromise((resolve, reject)->{
            resolve(listPublicIpsSync());
        });
    }

    public function listPublicIpsSync(): Array<IPAddress> {
        var interfaces = Os.networkInterfaces();

        var results:Array<IPAddress> = [];

        for(netname => netInterface in interfaces.keyValueIterator()){
            for(netAddress in netInterface){
                if(!netAddress.internal && !isLinkLocal(netAddress)){
                    results.push(netAddress.address);
                }
            }
        }

        return results;
    }

    function isLinkLocal(addr:NetworkInterfaceAddressInfo): Bool {
        return addr.family ==  IPv6
            && addr.address.startsWith("fe80:");
    }
}