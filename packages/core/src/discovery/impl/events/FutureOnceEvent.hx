package discovery.impl.events;

import discovery.async.Future;
import discovery.domain.Event.EventListenOptions;
import discovery.utils.events.EventEmitter;

class FutureOnceEvent<T> implements EventEmitter<T> {
    var future:Future<T>;

    public function new() {
        future = new Future();
    }

    public function notify(value:T) {
        future.notify(value);
    }

    public function listen(callback:T -> Void, ?options:EventListenOptions) {
        future.onValue(callback);
    }

    public function unlisten(callback:T -> Void) {
        future.unlisten(callback);
    }
}