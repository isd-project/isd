package discovery.impl.events.signal;

import discovery.utils.events.EventEmitterFactory.EventEmitterBuildOptions;
import discovery.domain.Event;
import discovery.domain.Event.EventListenOptions;
import signals.Signal1;
import discovery.utils.events.EventEmitter;

class SignalEventHandle<T> implements EventEmitter<T> {
    var signal:Signal1<T>;
    var once:Bool=false;

    public function new(?options: EventEmitterBuildOptions) {
        signal = new Signal1<T>();

        if(options != null){
            this.once = options.once;
        }
    }

    public function listen(callback:T -> Void, ?options: EventListenOptions) {
        if(options == null){
            options = {once:false};
        }

        //Does not repeat if should be called once
        var repeat = if(once || options.once) 0 else -1;

        signal.add(callback).repeat(repeat);
    }

    public function unlisten(callback:T -> Void) {
        signal.remove(callback);
    }

    public function notify(value:T) {
        signal.dispatch(value);
    }

    public function hasListeners():Bool {
        return signal.hasListeners;
    }
}

private class ListenerDelegator<T> {
    var listener:(T)->Void;
    var event:Event<T>;
    var once:Bool=false;

    public function new(
        listener:T->Void, event:Event<T>, options:EventListenOptions)
    {
        this.listener=listener;
        this.event=event;

        if(options != null){
            this.once  = options.once;
        }
    }

    public function onEvent(value: T){
        this.listener(value);

        if(once){
            this.event.unlisten(this.onEvent);
        }
    }
}