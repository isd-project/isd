package discovery.impl.events.signal;

import discovery.utils.events.EventEmitter;
import discovery.utils.events.EventEmitterFactory;

class SignalEventFactory implements EventEmitterFactory{

    public function new() {}

    public function eventEmitter<T>(?options: EventEmitterBuildOptions):EventEmitter<T>
    {
        return new SignalEventHandle<T>(options);
    }
}