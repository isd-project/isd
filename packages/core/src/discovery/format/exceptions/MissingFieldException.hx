package discovery.format.exceptions;

import haxe.Exception;

class MissingFieldException extends InvalidFormatException{
    public function new(fieldMissing: String, ?previous:Exception) {
        super('Invalid format: missing field ${fieldMissing}', previous);
    }
}