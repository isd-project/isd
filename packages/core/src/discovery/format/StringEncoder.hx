package discovery.format;

interface StringEncoder {
    public function encode(text   : String):String;
    public function decode(encoded: String):String;
}