package discovery.format;

import haxe.io.Bytes;

interface Formatter {
    function encode(bytes: Bytes) : String;
    function decode(text : String): Bytes;
}