package discovery.format;

import haxe.DynamicAccess;
import haxe.io.Bytes;

import discovery.format.exceptions.InvalidFormatException;
import discovery.utils.Comparator.*;


@:structInit
class TextWithFormat {
    public var data(default, default): String;
    public var format(default,default):Formats;


    public function getFormatter(): Formatter {
        if(this.format == null) return null;

        return this.format.getFormatter();
    }

    public function decode(): Bytes {
        return if(this.format == null) null
               else getFormatter().decode(this.data);
    }

    inline public function toBytes(){
        return decode();
    }

    public function toBinary(): Binary{
        return toBytes();
    }

    public function toString() {
        return this.data;
    }

    public function equals(other:TextWithFormat) {
        return compare(other) == 0;
    }

    public function compare(other: TextWithFormat): Int {
        if(this == null || other == null) return compareNull(this, other);

        if(this.format == other.format){
            /*
                If the format are the same, we could compare directly.
                However, in some cases, slightly different representations
                could correpond to the same binary data.
                So we only return here, if data are equals.
            */
            if(compareStrings(this.data, other.data) == 0){
                return 0;
            }
        }

        //Otherwise, first decode to compare bytes
        return decode().compare(other.decode());
    }

    public function clone(): TextWithFormat {
        return { //values should be immutable
            data: data,
            format: format
        };
    }
}

@:forward
abstract EncodedData(TextWithFormat) from TextWithFormat to TextWithFormat {
    public function new(textWithFormat: TextWithFormat) {
        this = textWithFormat;
    }

    public static function fromDynamic(dyn:Dynamic): EncodedData {
        if(dyn == null) return null;

        var obj:DynamicAccess<Dynamic> = dyn;

        if(obj.exists('data') && obj.exists('format')){
            var textWithFormat:TextWithFormat = {
                data: dyn.data,
                format: dyn.format
            };
            var data:EncodedData = textWithFormat;

            if(data.getFormatter() != null){
                return data;
            }
        }

        return null;
    }

    @:from
    public static function fromBytes(data: Bytes): EncodedData{
        return encodeDefault(data);
    }

    @:from
    public static function fromBinary(data: Binary): EncodedData{
        return encodeDefault(data);
    }

    public static function encodeDefault(data: Bytes): EncodedData{
        if(data == null){
            return null;
        }

        return encode(data, Formats.Base64Url);
    }

    public static function encode(data: Bytes, ?format:Formats): EncodedData
    {
        if(format == null){
            format = Formats.Base64Url;
        }

        var formatter = format.getFormatter();

        if(formatter == null){
            throw new InvalidFormatException('format $format is not valid');
        }

        return new EncodedData({
            data: formatter.encode(data),
            format: format
        });
    }

    @:to
    inline public function toBytes(): Bytes {
        return if(this == null) null else this.toBytes();
    }

    @:to
    inline public function toString() {
        return if(this == null) null else this.toString();
    }

    public function equals(other: EncodedData){
        return if(this == null) other == null;
               else this.equals(other);
    }

    public static function compareBoth(a:EncodedData, b:EncodedData):Int {
        if(a == null || b == null){
            return compareNull(a, b);
        }

        return a.compare(b);
    }
}