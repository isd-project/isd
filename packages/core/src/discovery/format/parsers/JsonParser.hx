package discovery.format.parsers;

import discovery.domain.Struct;
import discovery.format.Binary.BinaryData;
import haxe.io.Bytes;
import haxe.Json;
import haxe.DynamicAccess;

typedef JsonValueReplacer = (key: Dynamic, value:Dynamic)->Dynamic;
private typedef Replacer = JsonValueReplacer;

class JsonParser {
    public function new() {

    }

    public function parse(txtText:String) {
        return Json.parse(txtText);
    }

    public function stringify(obj: Dynamic, ?replacer:Replacer, ?space:String): String
    {
        replacer = doTransformations([replacer, transformBinary]);
        return Json.stringify(obj, replacer, space);
    }

    function doTransformations(transformations: Array<Replacer>) {
        return function replacer(key: String, value: Dynamic): Dynamic {
            for(transform in transformations){
                if(transform != null){
                    value = transform(key, value);
                }
            }

            return value;
        }
    }

    function transformBinary(key:String, value:Dynamic): Dynamic{
        if(Std.isOfType(value, Bytes) || Std.isOfType(value, BinaryData)){
            return stringifyBinary(value);
        }
        else {
            return value;
        }
    }

    function stringifyBinary(value:Dynamic):String{
        var binary = Binary.fromDynamic(value);
        return Formatters.base64Url.encode(binary);
    }
}