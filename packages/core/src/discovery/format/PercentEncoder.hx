package discovery.format;

using StringTools;

class PercentEncoder implements StringEncoder{
    public function new() {}


    public function encode(text:String):String {
        var encoded = text.urlEncode();

        var toTranslate = [
            "!" => "%21",
            "'" => "%27",
            "(" => "%28",
            ")" => "%29",
            "*" => "%2A"
        ];

        //TO-DO: use solution more efficient
        for (from => to in toTranslate){
            encoded = encoded.replace(from, to);
        };

        return encoded;
    }

    public function decode(encoded:String):String {
        return encoded.urlDecode();
    }
}