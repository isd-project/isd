package discovery.format.formats;

import haxe.io.Bytes;

/** Use this format only with UTF8 strings **/
class UTF8Formatter implements Formatter{
    public function new() {}

    public function encode(bytes:Bytes):String {
        return bytes.toString();
    }

    public function decode(text:String):Bytes {
        return Bytes.ofString(text);
    }
}