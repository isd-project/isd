package discovery.format.formats;

import haxe.io.Bytes;

class Base32Format extends BasicFormatter{
    public static var CHARS(default, null)= "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";

    public function new(usePadding: Bool=false) {
        super(CHARS, usePadding, 8);
    }

    override function decode(input:String):Bytes {
        if(input == null) return null;

        return super.decode(input.toUpperCase());
    }
}