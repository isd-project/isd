package discovery.format.formats;

import haxe.crypto.BaseCode;
import haxe.io.Bytes;

class BasicFormatter implements Formatter{
    var baseCode:BaseCode;
    var paddingBlock:Null<Int>;
    public var usePadding(default, default):Bool;

    public function new(charset:String, usePadding:Bool=false, ?paddingBlock:Int) {
        this.baseCode = new haxe.crypto.BaseCode(Bytes.ofString(charset));
        this.usePadding = usePadding;
        this.paddingBlock = paddingBlock;
    }

    public function encode(input:Bytes):String {
        var encoded = this.baseCode.encodeBytes(input).toString();

        return if(!usePadding || input.length == 0) encoded else addPadding(encoded);
    }

    public function decode(input:String):Bytes {
        //TO-DO: throw specific exception when input is invalid
        var text = removePadding(input);
        return this.baseCode.decodeBytes(Bytes.ofString(text));
    }

    function addPadding(output: String) {
        if(!usePadding
            || paddingBlock == null
            || output.length % paddingBlock == 0)
        {
            return output;
        }

        var numPadding = paddingBlock - (output.length % paddingBlock);
        var padding = new StringBuf();

        for (i in 0...numPadding){
            padding.add("=");
        }

        return output + padding.toString();
    }

    function removePadding(text: String): String{
        var countPadding:Null<Int>= 0;

        var idx = text.length - 1;
        while(idx >= 0 && text.charCodeAt(idx) == "=".code){
            countPadding += 1;

            idx-=1;
        }

        var removeLen = if(countPadding > 0) -countPadding else null;

        return text.substr(0, removeLen);
    }
}