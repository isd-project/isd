package discovery.format.formats;

import haxe.io.Bytes;

class Base16Format implements Formatter{
    public function new() {}

    public function encode(bytes:Bytes):String {
        return bytes.toHex();
    }

    public function decode(text:String):Bytes {
        return Bytes.ofHex(text);
    }
}