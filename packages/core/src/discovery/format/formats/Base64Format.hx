package discovery.format.formats;

import haxe.io.Bytes;
import haxe.crypto.Base64;

class Base64Format extends BasicFormatter{
    public function new(usePadding: Bool=false) {
        super(Base64.CHARS, usePadding, 4);
    }
}