package discovery.format.formats;

class Base64UrlFormat extends BasicFormatter{
    public static var CHARS(default, null)= "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

    public function new(usePadding: Bool=false) {
        super(CHARS, usePadding, 4);
    }
}