package discovery.format;

import haxe.io.Bytes;

@:forward
@:forwardStatics
abstract Binary(BinaryData)
    from BinaryData to BinaryData
{
    @:from
    public static function fromBytes(bytes: Bytes): Binary{
        if(bytes == null) return null;

        return new BinaryData(bytes);
    }

    #if(js && nodejs)
    @:from
    public static function fromBuffer(buffer: js.node.Buffer) {
        if(buffer == null) return null;

        return fromBytes(buffer.hxToBytes());
    }
    #end

    @:from
    inline public static function fromText(value:String):Binary {
        return BinaryData.fromText(value);
    }

    inline public static function fromHex(hex: String): Binary {
        if(hex == null) return null;

        return BinaryData.fromHex(hex);
    }

    inline public static function fromInt(value:Int):Binary {
        return BinaryData.fromInt(value);
    }
    inline public static function fromFloat(value:Float):Binary {
        return BinaryData.fromFloat(value);
    }

    public static function fromDynamic(value:Null<Dynamic>): Binary {
        if(Std.isOfType(value, BinaryData)){
            return value;
        }
        else if(Std.isOfType(value, Bytes)){
            return fromBytes(value);
        }
        else if(Std.isOfType(value, String)){
            return fromText(value);
        }
        else if(Std.isOfType(value, Int)){
            return fromInt(value);
        }
        else if(Std.isOfType(value, Float)){
            return fromFloat(value);
        }

        return null;
    }


    inline public static function toInteger(value:Binary):Null<Int> {
        if(value == null) return null;

        return value.toInt();
    }


    @:to
    public function toBytes(): Bytes{
        if(this == null) return null;

        return this.toBytes();
    }

    public function equals(other: Binary){
        return this.equals(other);
    }

    public function compare(other: Binary){
        return this.compare(other);
    }

    public static function areEquals(b1:Binary, b2:Binary) {
        if(b1 == b2) return true;
        else if(b1 == null || b2 == null) return false;

        return b1.equals(b2);
    }
}

class BinaryData{
    static final INT32_SIZE = 4;
    static final FLOAT_SIZE = 4;
    static final DOUBLE_SIZE = 8;

    var bytes: Bytes;

    public var length(get, never): Int;

    public static inline function emptyBytes(): Bytes {
        return Bytes.alloc(0);
    }

    public static inline function emptyBinary():BinaryData {
        return build(emptyBytes());
    }

    inline static function build(bytes:Bytes) {
        return new BinaryData(bytes);
    }

    public function new(bytes: Bytes) {
        this.bytes = if(bytes == null) Bytes.alloc(0) else bytes;
    }

    function get_length(): Int {
        return bytes.length;
    }

    public function get(idx: Int) {
        return bytes.get(idx);
    }

    public function set(idx: Int, byte: Int) {
        bytes.set(idx, byte);
    }

    public function equals(other: BinaryData): Bool {
        return compare(other) == 0;
    }

    public function compare(other: BinaryData): Int {
        return bytes.compare(other.bytes);
    }

    public function clone(): BinaryData {
        var copy = Bytes.alloc(this.bytes.length);
        copy.blit(0, this.bytes, 0, bytes.length);

        return new BinaryData(copy);
    }

    //---------------------------------------------------------

    public function encode(formatter: Formatter){
        return formatter.encode(this.bytes);
    }

    public static function decode(data:String, formatter: Formatter){
        return build(formatter.decode(data));
    }


    public function toBytes(): Bytes {
        return bytes;
    }

    public function toString():String {
        return toHex();
    }

    public function toHex() {
        return bytes.toHex();
    }

    public function toUTF8() {
        return bytes.toString();
    }

    public static function fromHex(hex: String) {
        return build(Bytes.ofHex(hex));
    }

    public static function fromText(text: String) {
        return build(Bytes.ofString(text));
    }

    public static function fromInt(value:Null<Int>):BinaryData {
        return fromNumber(value, INT32_SIZE,
                (bytes)->{bytes.setInt32(0, value);});
    }


    public static function fromFloat(value:Float):BinaryData {
        return fromNumber(value, FLOAT_SIZE,
                (bytes)->{bytes.setFloat(0, value);});
    }

    public static function fromDouble(value:Float):BinaryData {
        return fromNumber(value, DOUBLE_SIZE,
                (bytes)->{bytes.setDouble(0, value);});
    }

    inline static function fromNumber<T>(number: T, size: Int, setValue: (Bytes)->Void){
        if(number == null) return null;

        var bytes = Bytes.alloc(size);
        setValue(bytes);

        return build(bytes);
    }

    public function toInt():Null<Int> {
        return this.bytes.getInt32(0);
    }

    public function toFloat():Null<Float> {
        return this.bytes.getFloat(0);
    }


    public function toDouble():Null<Float> {
        return this.bytes.getDouble(0);
    }
}