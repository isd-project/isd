package discovery.format;

import discovery.format.formats.UTF8Formatter;
import discovery.format.formats.Base16Format;
import discovery.format.formats.Base32Format;
import discovery.format.formats.Base64UrlFormat;
import discovery.format.formats.Base64Format;

class Formatters {
    public static var base64(default, null):Formatter = new Base64Format();
    public static var base64Url(default, null):Formatter = new Base64UrlFormat();
    public static var base32(default, null):Formatter = new Base32Format();
    public static var base16(default, null):Formatter = new Base16Format();

    public static var percentEncoding(default, null):StringEncoder = new PercentEncoder();

    public static var utf8(default, null):Formatter = new UTF8Formatter();

    public static function getFormatter(format: String) {
        var defaultFormat = "b64u";

        if(format == null || format == ""){
            format = defaultFormat;
        }
        else {
            format = format.toLowerCase();
        }

        return switch (format){
            case "b16" : base16;
            case "b32" : base32;
            case "b64u": base64Url;
            default    : null; //Throw exception?
        }
    }
}