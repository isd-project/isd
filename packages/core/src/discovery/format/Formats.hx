package discovery.format;

import haxe.io.Bytes;

enum abstract Formats(String) {
    var Base16    = "b16";
    var Base32    = "b32";
    var Base64    = "b64";
    var Base64Url = "b64u";
    var UTF8      = "utf8";

    public static function listFormats(): Array<Formats> {
        return [Base16, Base32, Base64, Base64Url, UTF8];
    }

    public function getFormatter(): Formatter {
        return switch (this.toLowerCase()){
            case "b16" : Formatters.base16;
            case "b32" : Formatters.base32;
            case "b64" : Formatters.base64;
            case "b64u": Formatters.base64Url;
            case "utf8": Formatters.utf8;
            //TO-DO: support the use of class names to instantiate custom formatters
            default    : null; //Throw exception?
        }
    }

    public function encode(bytes: Bytes): String{
        var formatter = getFormatter();
        return if(formatter == null) null else formatter.encode(bytes);
    }

    public function toString() {
        return this;
    }
}