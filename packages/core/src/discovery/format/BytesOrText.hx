package discovery.format;

import discovery.format.EncodedData;

@:forward
@:forwardStatics
abstract BytesOrText(EncodedData)
    from EncodedData to EncodedData
    from TextWithFormat
{
    public function new(data: EncodedData) {
        this = data;
    }

    @:from
    public static function fromBinary(binary: Binary): BytesOrText {
        return EncodedData.fromBinary(binary);
    }

    @:from
    public static function fromText(text: String): BytesOrText {
        if(text == null){
            return null;
        }

        return new BytesOrText({
            data: text,
            format: Formats.UTF8
        });
    }

    public static function fromDynamic(dyn:Dynamic):BytesOrText{
        return EncodedData.fromDynamic(dyn);
    }

    public function isText(): Bool {
        return this != null && this.format == Formats.UTF8;
    }

    public function clone(): BytesOrText{
        return if(this == null) null else this.clone();
    }
}