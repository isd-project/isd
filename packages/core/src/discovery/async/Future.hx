package discovery.async;

import haxe.Constraints.Function;
import hx.concurrent.lock.RLock;
import hx.concurrent.lock.Acquirable;
import haxe.Exception;

typedef OnResultCallback<Result> = (Result, ?Exception)->Void;

class Future<Result> {
    var result: Result;
    var listeners:FutureListenersCollection<Result>;

    var lock:Acquirable;
    var resolved:Bool=false;

    public function new() {
        lock = new RLock();

        listeners = new FutureListenersCollection();
    }

    function resetListeners():Iterable<FutureListener<Result>> {
        return listeners.reset();
    }

    public function onValue(callback:Result -> Void) {
        if(callback == null) return;

        var onResult = (result, ?err)->{
            if(err == null){
                callback(result);
            }
        }

        this.listen(onResult, callback);
    }

    public function onResult(callback: OnResultCallback<Result>) {
        listen(callback, callback);
    }

    function listen(callback:OnResultCallback<Result>, key:Any) {
        if(callback == null) return;

        if(!delayedResult(callback, key) && resolved){
            callback(result);
        }
    }

    function delayedResult(callback: OnResultCallback<Result>, key: Any)
    {
        if(resolved) return false;

        var registered = false;

        withLock(()->{
            if(!resolved){//listener was not called yet
                registered = true;
                listeners.add(callback, key);
            }
        });

        return registered;
    }


    public function unlisten(callback:Function) {
        withLock(()->{
           listeners.remove(callback);
        });
    }

    public function notify(result: Result, ?err: Exception) {
        if(resolved) return;

        var oldListeners:Iterable<FutureListener<Result>>;

        withLock(()->{
            if(!resolved){
                resolved = true;

                this.result = result;
            }

            oldListeners = resetListeners();
        });

        for(l in oldListeners){
            l.notify(result, err);
        }
    }

    function withLock(callback:() -> Void) {
        lock.acquire();
        try{
            callback();
        }
        catch(e){
            lock.release();

            throw e;
        }

        lock.release();
    }
}


private class FutureListener<T> {
    public var listener:OnResultCallback<T>;
    public var key:Any;

    public function new(listener: OnResultCallback<T>, key: Any) {
        this.listener = listener;
        this.key = key;
    }

    public function notify(result:T, ?err: Exception) {
        this.listener(result, err);
    }
}

class FutureListenersCollection<T> {
    var listeners:Array<FutureListener<T>>;

    public function new() {
        listeners = [];
    }

    public function reset() {
        var oldArray = listeners;
        listeners = [];

        return oldArray;
    }

    public function add(callback:OnResultCallback<T>, key:Any) {
        var l = new FutureListener(callback, key);
        listeners.push(l);
    }

    public function remove(key:Any) {
        var idx = indexOf(key);
        if(idx >= 0){
            listeners.splice(idx, 1);
        }
    }

    function indexOf(key:Any) {
        for (i in 0...listeners.length){
            if(listeners[i].key == key){
                return i;
            }
        }
        return -1;
    }
}