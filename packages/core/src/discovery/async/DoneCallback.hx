package discovery.async;

import haxe.Exception;

typedef DoneCallback = (?err:Exception)->Void;