package discovery.async.promise;

import haxe.Exception;


interface PromisableFactory {

    function promise<T>(callback:PromiseInitCallback<T>): Promisable<T>;

    function resolved<T>(value:T):Promisable<T>;
    function rejected<T>(error:Exception):Promisable<T>;

    function mergeAll(promises:Array<Promisable<Any>>):Promisable<Array<Any>>;
}