package discovery.async.promise;

typedef PromiseType<T> =
#if js
    js.lib.Promise<T>;
#else
    Promisable<T>;
#end