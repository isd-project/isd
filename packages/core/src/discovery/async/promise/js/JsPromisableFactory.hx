package discovery.async.promise.js;

import haxe.Exception;

class JsPromisableFactory implements PromisableFactory{

    public function new() {}

    public function promise<T>(callback:PromiseInitCallback<T>):Promisable<T> {
        return JsPromiseAdapter.makePromise(callback);
    }

    public function resolved<T>(value:T):Promisable<T> {
        return JsPromiseAdapter.resolve(value);
    }

    public function rejected<T>(error:Exception):Promisable<T> {
        return JsPromiseAdapter.reject(error);
    }

    public function mergeAll(
        promises:Array<Promisable<Any>>):Promisable<Array<Any>>
    {
        return JsPromiseAdapter.mergeAll(promises);
    }
}