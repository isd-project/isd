package discovery.async.promise.js;

import haxe.Exception;
#if js

class JsPromiseAdapter<T> implements Promisable<T> {

    static final promiseFactory = new JsPromisableFactory();

    var promise: js.lib.Promise<T>;

    inline public static
    function makePromise<T>(init: PromiseInitCallback<T>): JsPromiseAdapter<T>{
        return wrap(new js.lib.Promise<T>(init));
    }

    public function factory():PromisableFactory {
        return promiseFactory;
    }

    inline public function new(promise: js.lib.Promise<T>) {
        this.promise = promise;
    }

    inline public function then<TOut>(
        onFulfilled:Null<PromiseHandler<T, TOut>>,
        ?onRejected:PromiseHandler<Dynamic, TOut>):Promisable<TOut>
    {
        return wrap(promise.then(cast onFulfilled, cast onRejected));
    }

    inline public function catchError(onRejected:PromiseHandler<Dynamic, T>):Promisable<T>{
        return wrap(promise.catchError(cast onRejected));
    }

    inline public function finally(onFinally:() -> Void):Promisable<T>{
        return wrap(promise.finally(onFinally));
    }

    public static function wrap<T>(promise: js.lib.Promise<T>) {
        return new JsPromiseAdapter<T>(promise);
    }

    public function mergeWith(promisables:Array<Promisable<Dynamic>>):Promisable<Array<Dynamic>> {
        var all = [(this : Promisable<Dynamic>)].concat(promisables);
        return mergeAll(all);
    }

    public static function resolve<T>(value:T):Promisable<T> {
        return wrap(js.lib.Promise.resolve(value));
    }

    public static function reject<T>(error:Exception):Promisable<T> {
        return wrap(js.lib.Promise.reject(error));
    }

    public static function mergeAll(
        promisables:Array<Promisable<Any>>):Promisable<Array<Any>>
    {
        return wrap(js.lib.Promise.all(promisables));
    }
}
#end
