package discovery.async.promise;

import discovery.async.promise.Promisable;

#if js
import discovery.async.promise.js.JsPromiseAdapter;
#end

@:forward
abstract PromiseAbs<T>(Promisable<T>) from Promisable<T> to Promisable<T> {
    public function new(promisable: Promisable<T>) {
        this = promisable;
    }

    /** Used to automatically translate from a Promise to a promisable **/
    public static function wrap<T>(p: PromiseAbs<T>) {
        return p;
    }

    #if js

    @:from
    public static function fromPromiseJs<T>(p: js.lib.Promise<T>) {
        return new PromiseAbs<T>(new JsPromiseAdapter<T>(p));
    }

    #end
}