package discovery.async.promise;


abstract PromiseHandler<T, TOut>(T->Dynamic) // T->Dynamic, so the compiler always knows the type of the argument and can infer it for then/catch callbacks
    from T->Promisable<TOut> // support Promise explicitly as it doesn't work transitively through Thenable at the moment
#if js
    from T->js.lib.Promise<TOut> // support Promise explicitly as it doesn't work transitively through Thenable at the moment
#end
    from T->Thenable<TOut> // although the checking order seems to be reversed at the moment, see https://github.com/HaxeFoundation/haxe/issues/7656
    from T->TOut // order is important, because Promise<TOut> return must have priority
    from T->Any
{}
// #end