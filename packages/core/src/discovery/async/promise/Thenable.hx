package discovery.async.promise;

typedef Thenable<T> = {
        function then<TOut, ThenableType:Thenable<TOut>>(
        onFulfilled:Null<PromiseHandler<T, TOut>>,
        ?onRejected:PromiseHandler<Dynamic, TOut>):Thenable<TOut>;
};