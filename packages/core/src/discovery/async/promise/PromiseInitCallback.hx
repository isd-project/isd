package discovery.async.promise;

typedef PromiseResolver<T> = (T)->Void;
typedef PromiseReject      = (Dynamic)->Void;

typedef PromiseInitCallback<T> = (PromiseResolver<T>, PromiseReject) -> Void;
