package discovery.async.promise;

import discovery.async.promise.PromiseHandler;


interface Promisable<T> {

    function factory(): PromisableFactory;

    function then<TOut>(
        onFulfilled:Null<PromiseHandler<T, TOut>>,
        ?onRejected:PromiseHandler<Dynamic, TOut>):Promisable<TOut>;

    function catchError(onRejected:PromiseHandler<Dynamic, T>):Promisable<T>;

    function finally(onFinally:() -> Void):Promisable<T>;

    /**
        Combines the current promisable with the others in the array.

        In `js` target, this is equivalent to calling `Promise.all` with the current `Promisable` and the others `promisables` specified.

        @see js.lib.Promise.all
    **/
    function mergeWith(promisables: Array<Promisable<Dynamic>>):
        Promisable<Array<Dynamic>>;
}