package discovery.auth;

import discovery.keys.crypto.signature.ObjectVerifier;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.signature.ObjectSigner;
import discovery.async.promise.PromisableFactory;
import discovery.keys.Key;
import discovery.domain.Announcement;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.signature.Signed;

class DefaultAuthenticator implements Authenticator{

    var promises:PromisableFactory;

    public function new(promises:PromisableFactory) {
        this.promises = promises;
    }


    public function sign(
        key:Key, announcement:Announcement):Promisable<Signed<Announcement>>
    {

        if(key == null){ //should throw error
            return promises.resolved(Signed.make(announcement, null));
        }

        var signer = key.startSignature();

        return ObjectSigner.makeSigned(signer, announcement);
    }

    public function validate(
        key:Key, announce:Signed<Announcement>):Promisable<Bool>
    {
        if(key == null){
            throw new IllegalArgumentException(
                'Missing key to verify signature');
        }

        var verifier = key.startVerification();

        return ObjectVerifier.verifySigned(verifier, announce)
                .then((result)->result == true);
    }
}