package discovery.auth;

import discovery.keys.crypto.signature.Signed;
import discovery.async.promise.Promisable;
import discovery.domain.Announcement;
import discovery.keys.Key;

interface Authenticator {
    function sign(
        key:Key, announcement: Announcement): Promisable<Signed<Announcement>>;

    function validate(
        key: Key, announcement:Signed<Announcement>): Promisable<Bool>;
}