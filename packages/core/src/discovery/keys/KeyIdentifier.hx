package discovery.keys;

import discovery.format.Binary;
import discovery.keys.crypto.Fingerprint;
import discovery.format.BytesOrText;

@:forward
@:forwardStatics
@:transitive
abstract KeyIdentifier(BytesOrText) from BytesOrText to BytesOrText
{
    @:from
    public static
    function fromFingerprint(fingerprint: Fingerprint): KeyIdentifier {
        return if(fingerprint == null) null else fingerprint.hash.toBytes();
    }

    @:from
    public static function fromBinary(binary:Binary): KeyIdentifier {
        return BytesOrText.fromBinary(binary);
    }
}