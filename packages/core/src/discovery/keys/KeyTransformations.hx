package discovery.keys;

import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.KeyLoader;
import discovery.keys.storage.ExportedKeyPair;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.crypto.ExportedKey;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.storage.KeyData;
import discovery.keys.crypto.Fingerprint;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.KeyPair;

class KeyTransformations {
    public static function toKey(keyPair:KeyPair): Promisable<Key> {
        return keyPair.publicKey.fingerprint()
                .then(function(fingerprint: Fingerprint):Key{
                    var key:BaseKey = {
                        fingerprint : fingerprint,
                        keyType     : keyPair.publicKey.keyType,
                        privateKey  : keyPair.privateKey,
                        publicKey   : keyPair.publicKey
                    };

                    return key;
                });
    }

    public static function
        toKeyData(key: Key, ?format:KeyFormat): Promisable<KeyData>
    {
        IllegalArgumentException.require(key != null,
            "Cannot transform null Key into KeyData");
        IllegalArgumentException.require(key.publicKey != null,
            "Cannot transform Key without public key into KeyData");

        var keyData:KeyData = {
            keyType: key.keyType,
            fingerprint: key.fingerprint,
            name: key.name
        };

        return exportKeys(key, format)
                .then((exportedKeys)->{
                    keyData.publicKey = exportedKeys[0];
                    keyData.privateKey = exportedKeys[1];

                    return keyData;
                });
    }

    static function
        exportKeys(key: Key, format:KeyFormat): Promisable<Array<ExportedKey>>
    {
        var p1 = key.publicKey.export(format);

        if(key.privateKey != null){
            var p2 = key.privateKey.export(format);
            return cast p1.mergeWith([p2]);
        }
        else{
            return p1.then((exportedKey)->{
                return [exportedKey, null];
            });
        }

    }

    public static function toPublicKeyData(keyData:KeyData): PublicKeyData {
        return {
            fingerprint: keyData.fingerprint,
            keyType: keyData.keyType,
            key: keyData.publicKey
        };
    }

    public static function convertToKeyData(pubKeyData:PublicKeyData):KeyData {
        return {
            fingerprint: pubKeyData.fingerprint,
            publicKey: pubKeyData.key,
            keyType: pubKeyData.keyType
        };
    }

    public static
        function exportedKeyPairToKeyData(storedKeyPair:ExportedKeyPair):KeyData
    {
        if(storedKeyPair == null) return null;

        return {
            privateKey: storedKeyPair.privateKey,
            publicKey: storedKeyPair.publicKey
        };
    }

    public static function
        fillKeyData(loader:KeyLoader, keyData:KeyData):Promisable<KeyData>
    {
        var filler = new KeyFiller(loader, keyData);

        return filler.fillData();
    }
}

class KeyFiller {
    var keyLoader:KeyLoader;
    var keyData:KeyData;

    public function new(keyLoader:KeyLoader, keyData:KeyData) {
        this.keyLoader = keyLoader;
        this.keyData = keyData.clone();
    }

    public function fillData():Promisable<KeyData> {
        return keyLoader.loadExportedKey(keyData.publicKey)
            .then(fillDataFromPublicKey)
            .then((_)->keyData);
    }

    function fillDataFromPublicKey(publicKey:CryptoKey) {
        keyData.keyType = publicKey.keyType;

        return fillFingerprint(publicKey);
    }

    function fillFingerprint(publicKey:CryptoKey):Promisable<CryptoKey> {
        return publicKey.fingerprint().then((fingerprint)->{
            keyData.fingerprint = fingerprint;

            return publicKey;
        });
    }
}