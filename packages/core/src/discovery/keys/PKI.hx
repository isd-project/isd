package discovery.keys;

import discovery.keys.pki.KeyPublishListener;
import discovery.base.search.Stoppable;
import discovery.keys.crypto.Fingerprint;
import discovery.async.promise.Promisable;

interface PKI extends Stoppable{
    function searchKey(keyId:Fingerprint): Promisable<Key>;

    function publishKeychain(keychain:Keychain): Void;
    function publishKey(key: Key,
                        ?listener: KeyPublishListener): Promisable<Key>;
}