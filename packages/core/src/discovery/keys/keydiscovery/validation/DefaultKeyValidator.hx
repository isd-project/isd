package discovery.keys.keydiscovery.validation;

import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.CryptoKey;
import discovery.async.promise.Promisable;

class DefaultKeyValidator implements KeyFingerprintValidator{
    public function new() {

    }

    public function
        validateKey(key:CryptoKey, expected:Fingerprint):Promisable<CryptoKey>
    {
        return key.fingerprint().then((fing)->{
            if(fingerprintMatchs(fing, expected)){
                return key;
            }
            else{
                throw new ValidationFailure('Key does not match expected fingerprint.\n'
                + '\tExpected: ${expected}\n'
                + '\tbut found: ${fing}');
            }
        });
    }

    function fingerprintMatchs(found:Fingerprint, expected:Fingerprint) {
        return found != null
            && (expected.alg == null || expected.alg == found.alg)
            && found.hash.equals(expected.hash);
    }
}