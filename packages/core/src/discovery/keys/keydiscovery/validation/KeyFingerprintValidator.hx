package discovery.keys.keydiscovery.validation;

import discovery.async.promise.Promisable;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.CryptoKey;

interface KeyFingerprintValidator {
    function validateKey(key: CryptoKey, expected: Fingerprint): Promisable<CryptoKey>;
}