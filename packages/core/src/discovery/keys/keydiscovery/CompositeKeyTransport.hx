package discovery.keys.keydiscovery;

import discovery.base.search.CompositeStoppable;
import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.keys.keydiscovery.SearchKeyListeners.SearchKeyResult;
import discovery.utils.functional.CompositeDoneCallback;
import discovery.keys.storage.PublicKeyData;
import discovery.async.DoneCallback;
import discovery.keys.crypto.Fingerprint;

using discovery.utils.IteratorTools;


class CompositeKeyTransport implements KeyTransport{
    var _components:Array<KeyTransport>;

    public function new(components: Iterable<KeyTransport>) {
        this._components = [for(c in components) if(c != null) c];
    }

    public function components():Iterable<KeyTransport> {
        return _components;
    }

    public function publishKeychain(keychain:Keychain, ?done:DoneCallback) {
        var callback = CompositeDoneCallback.make(_components.length, done);

        this._components.forEach((transport)->{
            transport.publishKeychain(keychain, callback);
        });
    }

    public function publishKey(key:PublicKeyData, ?done:DoneCallback) {
        var callback = CompositeDoneCallback.make(_components.length, done);

        this._components.forEach((transport)->{
            transport.publishKey(key, callback);
        });
    }

    public function searchKey(fing:Fingerprint, listeners:SearchKeyListeners) {
        var expectedCalls = _components.length;
        var compositeListener =
            new CompositeSearchKeyListener(listeners, expectedCalls);

        var stoppables = _components.map((transport)->{
            return transport.searchKey(fing, {
                onKey: compositeListener.onKey,
                onEnd: compositeListener.onEnd
            });
        });

        return new CompositeStoppable(stoppables);
    }
}

class CompositeSearchKeyListener{
    var delegateListeners:SearchKeyListeners;

    var compositeEndCallback:DoneCallback;

    var foundKeys:IMap<PublicKeyData, Bool>;

    public function new(listeners: SearchKeyListeners, expectedCalls:Int) {
        this.delegateListeners = listeners;

        this.foundKeys = new ComparableMap();

        this.compositeEndCallback = CompositeDoneCallback.make(
            expectedCalls, listeners.onEnd
        );
    }

    public function onKey(result: SearchKeyResult) {
        if(foundKeys.exists(result.found) == false){
            foundKeys.set(result.found, true);

            delegateListeners.onKey(result);
        }
    }

    public function onEnd(?err) {
        compositeEndCallback(err);
    }
}