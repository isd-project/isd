package discovery.keys.keydiscovery;

interface KeyTransportBuilder {
    public function buildKeyTransport(): KeyTransport;
}