package discovery.keys.keydiscovery;

import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.keys.crypto.Fingerprint;
import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;

interface KeyTransport {
    function publishKeychain(keychain: Keychain, ?done: DoneCallback): Void;
    function publishKey(key:PublicKeyData, ?done: DoneCallback):Void;
    function searchKey(fin:Fingerprint, listeners:SearchKeyListeners):Stoppable;
}