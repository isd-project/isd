package discovery.keys.keydiscovery;

import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;

typedef SearchKeyListeners = {
    onKey: KeyFoundListener,
    ?onEnd: DoneCallback
};

typedef KeyFoundListener = (SearchKeyResult)->Void;

typedef SearchKeyResult = {
    found: PublicKeyData
};