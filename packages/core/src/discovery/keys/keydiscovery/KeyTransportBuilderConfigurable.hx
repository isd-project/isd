package discovery.keys.keydiscovery;

import discovery.base.configuration.Configurable;

interface KeyTransportBuilderConfigurable<T>
    extends KeyTransportBuilder
    extends Configurable<T>
{}