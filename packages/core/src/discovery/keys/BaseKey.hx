package discovery.keys;

import discovery.keys.crypto.signature.Verifier;
import discovery.exceptions.IllegalStateException;
import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.signature.Signer;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.KeyTypes;
import discovery.keys.crypto.Fingerprint;

using haxe.EnumTools.EnumValueTools;

@:structInit
class BaseKey implements Key{
    public var keyType(default, null):KeyTypes;

    public var fingerprint(default, null):Fingerprint;

    @:optional
    public var name(default, null):String;

    @:optional
    public var privateKey(default, null): CryptoKey;

    @:optional
    public var publicKey(default, null): CryptoKey;

    public function startSignature(?hashType:HashTypes):Signer {
        IllegalStateException.require(privateKey != null,
            "Cannot start signature without private key");

        return privateKey.startSignature(hashType);
    }

    public function startVerification(?alg:HashTypes):Verifier {
        IllegalStateException.require(publicKey != null,
            "Cannot start verification without public key");

        return publicKey.startVerification(alg);
    }

    public function toString():String {
        return 'key:\t${if(name!=null) name else ""}\n'
            +   '    - type: ${keyType.getName()}\n'
            +   '    - fingerprint: $fingerprint\n';
    }
}