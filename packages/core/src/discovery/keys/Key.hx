package discovery.keys;

import discovery.keys.crypto.signature.VerifierCreator;
import discovery.keys.crypto.signature.SignerCreator;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.KeyTypes;
import discovery.keys.crypto.Fingerprint;

interface Key extends SignerCreator
              extends VerifierCreator
{
    var fingerprint(default, null): Fingerprint;
    var name(default, null): String;
    var keyType(default, null): KeyTypes;

    var privateKey(default, null): Null<CryptoKey>;
    var publicKey(default, null): Null<CryptoKey>;

    function toString():String;

    //To-Do: define other fields
}