package discovery.keys.serialization;

import discovery.keys.crypto.ExportedKey;
import discovery.keys.storage.ExportedFingerprint;
import discovery.keys.crypto.KeyType;
import discovery.keys.storage.PublicKeyData;
import discovery.domain.serialization.Serializer;

class PublicKeyDataSerializer implements Serializer<PublicKeyData>{
    public function new()
    {}

    public function serialize(pubKey:PublicKeyData):Any {
        if(pubKey == null) return null;

        return {
            keyType: SerializedKeyType.fromKeyType(pubKey.keyType),
            fingerprint: pubKey.fingerprint,
            key: SerializedKey.fromExportedKey(pubKey.key)
        };
    }

    public function deserialize(serialized:Dynamic):PublicKeyData {
        if(serialized == null) return null;

        return {
            keyType: parseKeyType(serialized.keyType),
            fingerprint: ExportedFingerprint.fromDynamic(serialized.fingerprint),
            key: keyFromDynamic(serialized.key)
        };
    }

    function parseKeyType(keyTypeDyn:Dynamic):Null<KeyType> {
        var keyTypeStruct = SerializedKeyType.fromDynamic(keyTypeDyn);

        return if(keyTypeStruct == null) null else keyTypeStruct.toKeyType();
    }

    function keyFromDynamic(keyDyn:Dynamic):Null<ExportedKey> {
        var key = SerializedKey.fromDynamic(keyDyn);

        return if(key == null) null else key.toExportedKey();
    }
}