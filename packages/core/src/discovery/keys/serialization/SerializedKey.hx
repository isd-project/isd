package discovery.keys.serialization;

import discovery.keys.crypto.ExportedKey;
import discovery.format.BytesOrText;

using discovery.utils.Cast;


@:structInit
class SerializedKey{
    public var format(default, default): String;
    public var data(default, default): BytesOrText;

    public static function fromDynamic(keyObj: Dynamic): SerializedKey
    {
        if(keyObj == null) return null;

        var format:String = Cast.castIfIs(keyObj.format, String);
        var data:BytesOrText = BytesOrText.fromDynamic(keyObj.data);

        if(format == null && data == null) return null;

        return {
            data: data,
            format: format
        };
    }

    public static function fromExportedKey(key:ExportedKey): SerializedKey {
        if(key == null) return null;

        return {
            format: key.format.getName(),
            data: key.data
        };
    }

    public inline function toExportedKey():ExportedKey {
        return {
            format: KeyFormat.createByName(this.format),
            data: this.data
        };
    }

    public static function keyToExportedKey(key: SerializedKey): ExportedKey{
        if(key == null) return null;

        return key.toExportedKey();
    }
}