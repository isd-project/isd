package discovery.keys.serialization;

import discovery.keys.crypto.KeyTypes;

using discovery.utils.Cast;


@:structInit
class SerializedKeyType{
    public var name(default, default): String;
    public var params(default, default): Array<Dynamic>;

    public static function fromDynamic(dynObj: Dynamic): SerializedKeyType{
        if(dynObj == null) return null;

        return {
            name: Cast.castIfIs(dynObj.name, String),
            params: Cast.castIfIs(dynObj.params, Array)
        };
    }

    public static function fromKeyType(keyType:KeyTypes):SerializedKeyType{
        return {
            name: keyType.getName(),
            params: keyType.getParameters()
        };
    }

    public inline function toKeyType():Null<KeyTypes> {
        return KeyTypes.createByName(this.name, this.params);
    }

    public static function convertToKeyType(kType: SerializedKeyType) {
        if(kType == null) return null;

        return kType.toKeyType();
    }
}