package discovery.keys.pki;

import haxe.Exception;

typedef KeyPublishListener = {
    ?onInternalError: (Exception)->Void
}