package discovery.keys.pki;

import discovery.async.promise.Promisable;
import discovery.async.promise.PromisableFactory;
import discovery.async.promise.PromiseInitCallback;
import discovery.base.search.Stoppable;
import discovery.exceptions.KeyNotFoundException;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.KeyLoader;
import discovery.keys.crypto.KeyPair;
import discovery.keys.Key;
import discovery.keys.Keychain;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.validation.DefaultKeyValidator;
import discovery.keys.keydiscovery.validation.KeyFingerprintValidator;
import discovery.keys.KeyTransformations;
import haxe.Exception;
import hx.concurrent.atomic.AtomicBool;

typedef KeySearchDeps = {
    keyTransport: KeyTransport,
    promises:PromisableFactory,
    keyCache: Keychain,
    keyLoader: KeyLoader,
    ?keyValidator: KeyFingerprintValidator
};

class KeySearchProcess {
    var deps:KeySearchDeps;
    var keyFingerprint:Fingerprint;

    var searchPromise: Promisable<Key>;
    var resolve: PromiseResolver<Key>;
    var reject: PromiseReject;

    var searchStop: Stoppable;

    var finished:AtomicBool;

    public function new(keyFingerprint:Fingerprint, deps: KeySearchDeps) {
        this.keyFingerprint = keyFingerprint;
        this.deps = deps;

        if(deps.keyValidator == null){
            deps.keyValidator = new DefaultKeyValidator();
        }

        finished = new AtomicBool(false);
    }

    public function start(): Promisable<Key>{
        if(searchPromise != null){
            //search already started
            return searchPromise;
        }

        searchPromise = initPromise();

        startSearch(keyFingerprint);

        return searchPromise;
    }

    function initPromise():Promisable<Key> {
        return promises().promise((resolve, reject)->{
            this.resolve = resolve;
            this.reject = reject;
        });
    }

    function startSearch(keyFingerprint: Fingerprint) {
        searchOnCache(keyFingerprint)
            .then((key)->{
                if(key != null) finishSearch(key);
                else startKeyQuery();
                return key;
            });
    }

    function searchOnCache(keyFingerprint:Fingerprint): Promisable<Key>{
        return keyCache()
                .getKey(keyFingerprint);
    }

    function startKeyQuery() {
        searchStop = keyTransport().searchKey(keyFingerprint, {
            onKey: (result)->onKeyFound(result.found.key),
            onEnd: (?err)->finishSearch(null, err)
        });
    }

    function onKeyFound(exportedKey:ExportedKey) {
        if(finished) return;

        loadAndValidateKey(exportedKey)
            .then(convertKeyToKeyPair)
            .then(KeyTransformations.toKey)
            .then(saveKey)
            .then(finishSearch.bind(_, null))
            .catchError(notifyKeyValidationError.bind(_, exportedKey));
    }

    function notifyKeyValidationError(
        error: Exception, exportedKey: ExportedKey)
    {
        //TO-DO: implement
    }

    function loadAndValidateKey(key: ExportedKey): Promisable<CryptoKey>
    {
        return loadKey(key).then(validateKey);
    }

    function loadKey(exportedKey: ExportedKey) {
        return this.keyLoader().loadExportedKey(exportedKey);
    }

    function validateKey(cryptoKey: CryptoKey) {
        return deps.keyValidator.validateKey(cryptoKey, keyFingerprint);
    }

    function convertKeyToKeyPair(cryptoKey: CryptoKey) {
        //Review: maybe we should return only the public key

        return new KeyPair(cryptoKey, null);
    }

    function saveKey(key:Key): Promisable<Key> {
        //TO-DO: handle concurrency. Should lock when saving a key, to prevent two concurrent saves
        return keyCache().storeKey(key);
    }

    function finishSearch(key:Key, ?err:Exception) {
        if(finished.getAndSet(true) == true){ //already finished
            return;
        }

        stopSearch();

        if(err != null){
            reject(err);
        }
        else if(key == null){//no key found
            reject(new KeyNotFoundException('The search finished, but no key was found.'));
        }
        else{
            resolve(key);
        }
    }

    function stopSearch(){
        if(searchStop != null){
            searchStop.stop(()->{});
        }
    }

    // properties -----------------------------------------------------------

    function promises(): PromisableFactory{
        return deps.promises;
    }

    function keyCache() {
        return deps.keyCache;
    }

    function keyLoader() {
        return deps.keyLoader;
    }

    function keyTransport() {
        return deps.keyTransport;
    }
}