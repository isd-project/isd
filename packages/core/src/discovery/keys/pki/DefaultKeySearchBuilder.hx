package discovery.keys.pki;

import discovery.keys.pki.KeySearchProcess;
import discovery.keys.pki.KeySearchProcess.KeySearchDeps;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.Keychain;

using discovery.utils.dependencies.DependenciesVerifications;


class DefaultKeySearchBuilder implements KeySearchBuilder{
    var keySearchDeps: KeySearchDeps;

    public function new(keyCache: Keychain, keySearchDeps: KeySearchDeps)
    {
        keySearchDeps.requireAll();

        this.keySearchDeps = keySearchDeps;
    }

    public function build(fingerprint:Fingerprint):KeySearchProcess {
        return new KeySearchProcess(fingerprint, keySearchDeps);
    }
}