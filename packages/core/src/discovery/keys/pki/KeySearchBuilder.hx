package discovery.keys.pki;

import discovery.keys.pki.KeySearchProcess;
import discovery.keys.crypto.Fingerprint;

interface KeySearchBuilder {
    public function build(fingerprint: Fingerprint): KeySearchProcess;
}