package discovery.keys.pki;

import haxe.Exception;
import discovery.async.promise.PromiseInitCallback.PromiseResolver;
import discovery.async.promise.PromiseInitCallback.PromiseReject;
import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;
import discovery.async.promise.PromisableFactory;
import discovery.exceptions.composite.PartialFailureException;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.pki.KeySearchBuilder;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.Keychain;
import discovery.async.promise.Promisable;
import discovery.keys.Key;
import discovery.keys.PKI;

using discovery.keys.KeyTransformations;


typedef BasePkiDependencies = {
    keyTransport: KeyTransport,
    keySearchBuilder: KeySearchBuilder
};

class BasePki implements PKI {
    var keyTransport: KeyTransport;
    var keySearchBuilder: KeySearchBuilder;

    public var defaultKeyFormat(default, default): KeyFormat=Der;

    public function new(deps: BasePkiDependencies) {
        this.keyTransport = deps.keyTransport;
        this.keySearchBuilder = deps.keySearchBuilder;
    }


    public function stop(?callback:() -> Void) {
        //TO-DO: ...
    }

    public function searchKey(keyId:Fingerprint):Promisable<Key> {
        var keySearch = keySearchBuilder.build(keyId);
        return keySearch.start();
    }

    public function publishKeychain(keychain:Keychain) {
        keyTransport.publishKeychain(keychain);
    }

    public function
        publishKey(key:Key, ?listener:KeyPublishListener):Promisable<Key>
    {
        var p = exportKey(key);

        var factory = p.factory();

        var publication = new PkiKeyPublication(
            keyTransport,
            factory,
            listener
        );

        return p.then(publication.publish)
                .then((_)->return key);
    }

    function exportKey(key: Key): Promisable<PublicKeyData> {
        return key.toKeyData(defaultKeyFormat)
                .then(KeyTransformations.toPublicKeyData);
    }
}

class PkiKeyPublication {
    var keyTransport:KeyTransport;
    var keyPublishListener:KeyPublishListener;
    var promises:PromisableFactory;

    public function new(
        keyTransport: KeyTransport,
        promises: PromisableFactory,
        ?listener: KeyPublishListener)
    {
        this.keyTransport = keyTransport;
        this.promises = promises;
        this.keyPublishListener = listener;
    }

    public function publish(pubKey:PublicKeyData): Promisable<Any>{
        return promises.promise((resolve, reject)->{

            keyTransport.publishKey(pubKey, (?err)->{
                if(handleError(err)) resolve(null);
                else reject(err);
            });
        });
    }

    function handleError(err:Null<Exception>): Bool {
        if(err == null || Std.isOfType(err, PartialFailureException))
        {
            notifyError(err);
            return true;
        }

        return false;
    }

    function notifyError(err:Null<Exception>) {
        if(err == null) return;
        if(keyPublishListener == null
            || keyPublishListener.onInternalError == null)
            return;

        keyPublishListener.onInternalError(err);
    }
}