package discovery.keys.pki;

import discovery.base.configuration.PkiBuilder;
import discovery.keys.Keychain;
import discovery.keys.crypto.KeyLoader;
import discovery.async.promise.PromisableFactory;
import discovery.keys.crypto.KeyLoader;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.KeyTransportBuilder;

typedef BasePkiBuilderRequirements = {
    ?transportBuilder: KeyTransportBuilder,
    ?keyLoader: KeyLoader,
    ?keyCache: Keychain,
    ?promises: PromisableFactory
};

class BasePkiBuilder implements PkiBuilder{

    var keyTransportBuilder:KeyTransportBuilder;

    var _keyLoader:KeyLoader;
    var _keyCache:Keychain;
    var _promises:PromisableFactory;


    public function new(?deps: BasePkiBuilderRequirements) {
        if(deps == null) deps = {};

        this.keyTransportBuilder = deps.transportBuilder;
        this._keyLoader = deps.keyLoader;
        this._keyCache  = deps.keyCache;
        this._promises  = deps.promises;
    }

    public function buildPki(): PKI {
        var transport = buildTransport();

        return new BasePki({
            keyTransport: transport,
            keySearchBuilder: buildSearchBuilder(transport)
        });
    }

    public function transportBuilder(keyTransportBuilder:KeyTransportBuilder) {
        this.keyTransportBuilder = keyTransportBuilder;

        return this;
    }

    function buildTransport():KeyTransport {
        return keyTransportBuilder.buildKeyTransport();
    }

    function buildSearchBuilder(keyTransport: KeyTransport):KeySearchBuilder {
        return new DefaultKeySearchBuilder(_keyCache, {
            keyTransport: keyTransport,
            keyLoader: _keyLoader,
            keyCache: _keyCache,
            promises: _promises
        });
    }

    public function promises(promises:PromisableFactory) {
        this._promises = promises;
        return this;
    }

    public function keyLoader(keyLoader:KeyLoader) {
        this._keyLoader = keyLoader;
        return this;
    }

    public function keyCache(keyCache:Keychain) {
        this._keyCache = keyCache;
        return this;
    }
}