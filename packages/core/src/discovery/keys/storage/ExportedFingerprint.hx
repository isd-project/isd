package discovery.keys.storage;

import haxe.DynamicAccess;
import discovery.format.Binary;
import haxe.io.Bytes;
import discovery.keys.crypto.Fingerprint;
import discovery.format.EncodedData;
import discovery.keys.crypto.HashTypes;

import discovery.utils.Comparator.*;


typedef SerializableFingerprint = {
    alg: HashTypes,
    hash: EncodedData
}

@:forward
@:transitive
@:structInit
abstract ExportedFingerprint(SerializableFingerprint)
    from SerializableFingerprint
    to SerializableFingerprint
{
    public function new(fingerprint: SerializableFingerprint) {
        this = fingerprint;
    }

    public static function build(alg:HashTypes, hash: EncodedData) {
        return new ExportedFingerprint({
            alg: alg,
            hash: hash
        });
    }

    @:from
    public static function fromFingerprint(fingerprint: Fingerprint) {
        return build(fingerprint.alg, fingerprint.hash);
    }

    @:from
    public static function fromStructureByte(fingerprint: {alg:HashTypes, hash: Bytes}) {
        return build(fingerprint.alg, fingerprint.hash);
    }

    @:from
    public static function fromStructureBinary(fingerprint: {alg:HashTypes, hash: Binary}) {
        return build(fingerprint.alg, fingerprint.hash);
    }

    @:from
    public static function serialize(fingerprint: Fingerprint) {
        if(fingerprint == null){
            return null;
        }
        return build(fingerprint.alg, fingerprint.hash);
    }

    @:to
    public function toFingerprint(): Fingerprint {
        if(this == null) return null;

        return {
            alg: this.alg,
            hash: if(this.hash == null) null else this.hash.decode()
        };
    }

    public function equals(other:ExportedFingerprint) {
        if(other == null || this == null){
            return this == other;
        }

        return this.alg == other.alg
                && (this.hash == other.hash
                    || (this.hash != null && this.hash.equals(other.hash)));
    }

    public function clone(): ExportedFingerprint{
        if(this == null) return null;

        return {
            alg: this.alg, //immutable,
            hash: if(this.hash == null) null else this.hash.clone()
        };
    }

    public function toString() {
        return '${this.alg}:'
            + if(this.hash != null)'${this.hash.format}:${this.hash.data}'
              else 'null';
    }


    public static function fromDynamic(value:Dynamic):ExportedFingerprint {
        if(value == null) return null;

        var obj:DynamicAccess<Dynamic> = value;

        if(obj.exists('alg') && obj.exists('hash')){
            var hash = EncodedData.fromDynamic(value.hash);

            if(hash != null){
                return build(value.alg, hash);
            }
        }

        return null;
    }


    public static
    function compareBoth(a:ExportedFingerprint, b:ExportedFingerprint):Int
    {
        return a.compare(b);
    }

    public function compare(other:ExportedFingerprint):Int
    {
        if(this == null || other == null)
            return compareNull(this, other);

        var cmp = compareStrings(this.alg, other.alg);

        if(cmp == 0){
            cmp = EncodedData.compareBoth(this.hash, other.hash);
        }

        return cmp;
    }
}