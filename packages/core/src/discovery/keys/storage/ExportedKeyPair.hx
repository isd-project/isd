package discovery.keys.storage;

import discovery.keys.crypto.ExportedKey;

typedef ExportedKeyPair = {
    ?privateKey:ExportedKey,
    ?publicKey:ExportedKey
};