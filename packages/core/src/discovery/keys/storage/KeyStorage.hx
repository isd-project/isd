package discovery.keys.storage;

/**
    KeyStorage is responsible for persist the data related to the keys used by the system
**/
interface KeyStorage {
    function listKeys(): Array<KeyData>;
    function addKey(key: KeyData): Void;
    function getKey(keyIdentifier:KeyIdentifier):KeyData;
    function update(key: KeyData): Void;
    function remove(key: KeyIdentifier): Void;
}