package discovery.keys.storage;

import discovery.impl.keys.storage.lowdb.SerializableKeyData;
import discovery.domain.serialization.Serializer;
import discovery.keys.storage.KeyData;

using discovery.utils.Cast;


class KeyDataSerializer implements Serializer<KeyData>{

    public function new()
    {}

    public function serialize(value:KeyData):Any {
        return SerializableKeyData.fromKeyData(value);
    }

    public function deserialize(serialized:Dynamic):KeyData {
        return SerializableKeyData.fromDynamic(serialized);
    }
}