package discovery.keys.storage;

import discovery.keys.storage.KeyData;

@:forward
abstract ImportableKey(KeyData)
    from KeyData to KeyData
{
    public function toKeyData(): KeyData {
        return this;
    }

    @:from
    public static
        function fromExportedKeyPair(keyPair:ExportedKeyPair): ImportableKey
    {
        return KeyTransformations.exportedKeyPairToKeyData(keyPair);
    }
}