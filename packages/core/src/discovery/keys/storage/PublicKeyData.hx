package discovery.keys.storage;

import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.KeyType;

using discovery.utils.comparator.GenericComparator;


@:structInit
class PublicKeyData {
    @:optional
    public var keyType(default, default): KeyType;

    @:optional
    public var fingerprint(default, default): ExportedFingerprint;

    @:optional
    public var key(default, default): ExportedKey;

    public function equals(other: PublicKeyData) {
        return other != null
            && keyType.compare(other.keyType) == 0
            && fingerprint.equals(other.fingerprint)
            && key.equals(other.key);
    }

    public function clone():PublicKeyData {
        return {
            keyType: keyType, //should be immutable, so okay to copy
            fingerprint: if(fingerprint == null) null else fingerprint.clone(),
            key: if(key == null) null else key.clone()
        };
    }
}