package discovery.keys.storage;

import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.ExportedKey;

@:structInit
class KeyData {

    @:optional
    public var keyType(default, default): KeyType;

    @:optional
    public var fingerprint(default, default): ExportedFingerprint;

    @:optional
    public var name(default, default): String;

    @:optional
    public var privateKey(default, default): Null<ExportedKey>;

    @:optional
    public var publicKey(default, default): ExportedKey;

    public function equals(other: KeyData): Bool {
        return other != null
            && (keyType == other.keyType
                || if(keyType != null) keyType.equals(other.keyType) else false)
            && name == other.name
            && fingerprint.equals(other.fingerprint)
            && exportKeyEquals(privateKey, other.privateKey)
            && exportKeyEquals(publicKey, other.publicKey);
    }

    inline function exportKeyEquals(key:ExportedKey, otherKey:ExportedKey) {
        return (key == otherKey)
            || (key !=null && key.equals(otherKey));
    }

    /** Create a shallow copy of this instance. **/
    public function clone(): KeyData {
        return {
            keyType     : this.keyType,
            fingerprint : this.fingerprint,
            name        : this.name,
            privateKey  : this.privateKey,
            publicKey   : this.publicKey
        };
    }
}