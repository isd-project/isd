package discovery.keys.storage.exceptions;

import haxe.Exception;

class StorageException extends Exception{}