package discovery.keys;

import discovery.keys.storage.ImportableKey;
import discovery.keys.crypto.CryptoKey;
import discovery.keys.crypto.ExportedKey;
import discovery.async.promise.PromisableFactory;
import discovery.keys.storage.KeyData;
import discovery.keys.storage.KeyStorage;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.keys.crypto.KeyPair;
import discovery.async.promise.Promisable;

using discovery.keys.KeyTransformations;

class BaseKeychain implements Keychain{
    var keygenerator: KeyGenerator;
    var keystorage: KeyStorage;
    var promises: PromisableFactory;

    public function new(
        keygenerator:KeyGenerator,
        keystorage: KeyStorage,
        promises: PromisableFactory)
    {
        this.keygenerator = keygenerator;
        this.keystorage = keystorage;
        this.promises = promises;
    }

    public function getKey(keyIdentifier:KeyIdentifier):Promisable<Key> {
        var keyData = keystorage.getKey(keyIdentifier);

        if(keyData != null){
            return keygenerator.loadKey(keyData)
                    .then(convertToKey);
        }

        return promises.resolved(null);
    }

    public function listKeys(): Array<KeyData>{
        return keystorage.listKeys();
    }

    public function generateKey(options: GenerateKeyOptions): Promisable<Key>{
        return keygenerator
                .generateKey(options)
                .then(convertToKey)
                .then(exportAndStoreKey);
    }

    public function importKey(key:ImportableKey):Promisable<KeyData> {
        return fillKeyData(key.toKeyData())
            .then((filledKey)->{
                return storeKeyData(filledKey);
            });
    }

    function fillKeyData(keyData: KeyData): Promisable<KeyData>{
        if(keyData.fingerprint == null || keyData.keyType == null){
            return keygenerator.fillKeyData(keyData);
        }

        return promises.resolved(keyData);
    }


    public function storeKey(key:Key):Promisable<Key> {
        return key.toKeyData()
                .then((keyData)->{
                    storeKeyData(keyData);
                    return key;
                });
    }

    // -----------------------------------------------------------------------

    function convertToKey(keyPair: KeyPair): Promisable<Key> {
        return keyPair.toKey();
    }

    function exportAndStoreKey(key: Key) {
        return key.toKeyData()
            .then(storeKeyData)
            .then((_)->{return key;});
    }

    function storeKeyData(key: KeyData) {
        keystorage.addKey(key);
        return key;
    }
}