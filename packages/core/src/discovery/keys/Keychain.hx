package discovery.keys;

import discovery.keys.storage.ImportableKey;
import discovery.keys.storage.KeyData;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.async.promise.Promisable;

/**
    Keychain provides a facade to the storage and generation of keys used by the system.
**/
interface Keychain {
    function listKeys(): Array<KeyData>;

    function generateKey(options: GenerateKeyOptions): Promisable<Key>;

    function getKey(keyIdentifier:KeyIdentifier):Promisable<Key>;

    function storeKey(key:Key): Promisable<Key>;

    function importKey(key:ImportableKey): Promisable<KeyData>;
}