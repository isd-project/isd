package discovery.keys.crypto;

import discovery.keys.crypto.signature.VerifierCreator;
import discovery.keys.crypto.signature.SignerCreator;
import discovery.async.promise.Promisable;

interface CryptoKey extends SignerCreator
                    extends VerifierCreator
{
    var keyType(default, null): KeyTypes;

    function isPrivate(): Bool;

    function fingerprint(): Promisable<Fingerprint>;

    function export(?format:KeyFormat): Promisable<ExportedKey>;
}