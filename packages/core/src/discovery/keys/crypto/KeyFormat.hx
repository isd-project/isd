package discovery.keys.crypto;

enum KeyFormat {
    Pem;
    Der;
    Jwk;
}