package discovery.keys.crypto.signature;

import discovery.async.promise.Promisable;

interface Signer extends Digester{
    function end():Promisable<Signature>;
}