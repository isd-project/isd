package discovery.keys.crypto.signature;

interface SignerCreator {
    function startSignature(?alg: HashTypes): Signer;
}