package discovery.keys.crypto.signature;

import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.signature.Verifier.Verify;
import discovery.async.promise.Promisable;
import haxe.DynamicAccess;

class ObjectVerifier {
    var objectDigester:ObjectDigester;
    var verifier: Verifier;

    public static function verifySigned<T>(
        verifier: Verifier, signed:Signed<T>): Promisable<Verify>
    {
        var data:Dynamic = signed.data;

        return verifySignature(verifier, data, signed.signature);
    }

    public static function verifySignature(
        verifier: Verifier,
        data: DynamicAccess<Dynamic>,
        signature: Signature): Promisable<Verify>
    {
        return new ObjectVerifier(verifier).verify(data, signature);
    }


    public function new(verifier: Verifier) {
        IllegalArgumentException.require(verifier != null,
            "Cannot create ObjectVerifier with null verifier"
            );

        this.verifier = verifier;

        objectDigester = new ObjectDigester(verifier);
    }

    public function verify(signedData:DynamicAccess<Dynamic>, signature:Signature) : Promisable<Verify>
    {
        objectDigester.digest(signedData);

        return verifier.verify(signature);
    }

    public function transform<Tin, Tout>(
        type:Dynamic, transformation:(Tin)->Tout): ObjectVerifier
    {
        objectDigester.transform(type, transformation);
        return this;
    }
}