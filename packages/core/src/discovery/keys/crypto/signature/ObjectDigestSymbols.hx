package discovery.keys.crypto.signature;

enum abstract ObjectDigestSymbols(String) to String{
    final propertySeparator = ":";
    final endValue    = ";";
    final startObject = "{";
    final endObject   = "}";
    final startArray  = "[";
    final endArray    = "]";
}