package discovery.keys.crypto.signature;

import discovery.async.promise.Promisable;


typedef Verify = Bool;

interface Verifier extends Digester{
    function verify(signature: Signature): Promisable<Verify>;
}