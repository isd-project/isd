package discovery.keys.crypto.signature;

interface VerifierCreator {
    function startVerification(?alg:HashTypes):Verifier;
}