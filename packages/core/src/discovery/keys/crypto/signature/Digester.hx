package discovery.keys.crypto.signature;

import haxe.io.Bytes;

interface Digester {
    function appendData  (data:Bytes):Void;
    function appendString(value:String):Void;
}