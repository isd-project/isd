package discovery.keys.crypto.signature;

import haxe.io.Encoding;
import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import haxe.io.Output;


class DigesterWriter extends Output{
    var digester:Digester;
    var _buffer:BytesBuffer;

    public function new(digester: Digester) {
        this.digester = digester;
    }

    public function writeInt64(value:Int) {
        buffer().addInt64(value);
    }

    override function writeByte(byte: Int) {
        buffer().addByte(byte);
    }

    override function write(bytes: Bytes) {
        flush();
        digester.appendData(bytes);
    }

    override function writeString(str:String, ?encoding:Encoding) {
        flush();
        digester.appendString(str);
    }

    override function writeBytes(buf:Bytes, pos, len):Int {
        writeFullBytes(buf, pos, len);
        return len;
    }


    override function writeFullBytes(buf:Bytes, pos, len):Void {
        buffer().addBytes(buf, pos, len);
        flush();
    }


    override function flush() {
        if(hasBuffer()){
            digester.appendData(extractBuffer());
        }
    }

    override function close() {
        flush();
    }

    //---------------------------------------------------

    function hasBuffer(){
        return _buffer != null && _buffer.length > 0;
    }

    function buffer(): BytesBuffer {
        if(_buffer == null){
            _buffer = new BytesBuffer();
        }
        return _buffer;
    }

    function extractBuffer():Bytes {
        var data = buffer().getBytes();
        _buffer = null;

        return data;
    }
}