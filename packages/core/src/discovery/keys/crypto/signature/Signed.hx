package discovery.keys.crypto.signature;

@:forward
@:generic
abstract Signed<T>(SignedData<T>) from SignedData<T> to SignedData<T>
{
    public function new(signed: SignedData<T>){
        this = signed;
    }

    public static function make<T>(data: T, signature: Signature): Signed<T>
    {
        return new SignedData(data, signature);
    }

    @:to
    public function toData(): T{
        return this.data;
    }
}

@:structInit
class SignedData<T> {
    public var data(default, null): T;
    public var signature(default, null): Signature;

    public function new(data: T, signature: Signature) {
        this.data = data;
        this.signature = signature;
    }

    public function isSigned(): Bool{
        return this.signature != null;
    }
}