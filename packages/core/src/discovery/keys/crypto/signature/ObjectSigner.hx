package discovery.keys.crypto.signature;

import discovery.exceptions.IllegalArgumentException;
import discovery.async.promise.Promisable;
import haxe.DynamicAccess;

class ObjectSigner{
    var signer:Signer;
    var objectDigester:ObjectDigester;

    public static function makeSignature(
        signer: Signer, data: DynamicAccess<Dynamic>): Promisable<Signature>
    {
        return new ObjectSigner(signer).sign(data);
    }

    public static
    function makeSigned<T>(signer: Signer, data: T): Promisable<Signed<T>>
    {
        return new ObjectSigner(signer).signObject(data);
    }

    public function new(signer:Signer) {
        IllegalArgumentException.require(signer != null,
            "Cannot create signature with null signer");

        this.signer = signer;

        objectDigester = new ObjectDigester(signer);
    }

    public function sign(obj:DynamicAccess<Dynamic>):Promisable<Signature> {
        objectDigester.digest(obj);
        return signer.end();
    }

    public function signObject<T>(obj: T): Promisable<Signed<T>> {
        return sign((obj: Dynamic)).then(function(signature):Signed<T>{
            return Signed.make(obj, signature);
        });
    }

    public function transform<Tin, Tout>(
        type:Dynamic, transformation:(Tin)->Tout): ObjectSigner
    {
        objectDigester.transform(type, transformation);
        return this;
    }
}