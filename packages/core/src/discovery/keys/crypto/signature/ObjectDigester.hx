package discovery.keys.crypto.signature;

import discovery.utils.traversal.BaseObjectVisitor;
import discovery.format.Binary.BinaryData;
import discovery.async.promise.Promisable;
import haxe.Constraints.Function;
import haxe.io.Bytes;
import discovery.utils.traversal.ObjectTraversal;
import discovery.utils.traversal.ObjectVisitor;
import haxe.io.Encoding;
import haxe.io.Bytes;
import haxe.io.BytesBuffer;
import haxe.io.Output;
import haxe.DynamicAccess;

import discovery.keys.crypto.signature.ObjectDigestSymbols;


class ObjectDigester extends BaseObjectVisitor{

    var digester:Digester;
    var digesterWriter:DigesterWriter;
    var traversal:ObjectTraversal;

    public function new(digester:Digester) {
        super();

        this.digester = digester;
        digesterWriter = new DigesterWriter(digester);
        traversal = new ObjectTraversal(this);
        traversal.transform(BinaryData, (binary)->binary.toBytes());
    }

    public function digest(obj:DynamicAccess<Dynamic>) {
        traverse(obj);
        digesterWriter.close();
    }

    override public function onNull(key:String) {
        //null properties are treated as if they were excluded from the object
    }

    override public function onString(name:String, value:String) {
        appendPropertyName(name);
        appendString(value);
        endProperty(name);
    }

    override public function onBytes(name:String, value:Bytes) {
        appendPropertyName(name);
        digesterWriter.write(value);
        endProperty(name);
    }

    override public function onInt(name:String, value:Int) {
        appendPropertyName(name);
        digesterWriter.writeInt32(value);
        endProperty(name);
    }

    override public function onFloat(name:String, value:Float) {
        appendPropertyName(name);
        digesterWriter.writeFloat(value);
        endProperty(name);
    }

    override public function onBool(name:String, value:Bool) {
        appendPropertyName(name);
        digesterWriter.writeByte(if(value) 1 else 0);
        endProperty(name);
    }

    // enum --------------------------------------------------------------

    override public function onEnum(name:String, e:EnumValue, enumType:Enum<Any>) {
        if(!enumHasParameters(e)){
            onSimpleEnum(name, e);
        }
        else{
            onEnumWithParameters(name, e);
        }
    }

    function onSimpleEnum(name:String, e:EnumValue) {
        appendPropertyName(name);
        appendString(e.getName());
        endProperty(name);
    }

    function onEnumWithParameters(name:String, e:EnumValue) {
        appendPropertyName(name);
        appendString(startObject);

        traverse({
            enumname: e.getName(),
            params: e.getParameters()
        });

        appendString(endObject);
        endProperty(name);
    }


    // object --------------------------------------------------------------

    override public function onStartObject(
        key:String, value:Dynamic, ?classObj:Class<Any>)
    {
        appendPropertyName(key);
        appendString(startObject);
    }

    override public function onEndObject(
        name:String, value:Dynamic, ?classObj:Class<Any>)
    {
        appendString(endObject);
        endProperty(name);
    }

    // array --------------------------------------------------------------

    override public function onStartArray(key:String, value:Array<Any>) {
        appendPropertyName(key);
        appendString(startArray);
    }

    override public function onEndArray(name:String, value:Array<Any>) {
        appendString(endArray);
        endProperty(name);
    }

    // --------------------------------------------------------------

    function traverse(obj:Dynamic) {
        traversal.traverse(obj);
    }

    function appendPropertyName(name:String) {
        if(name == null){ //not property
            return;
        }

        appendString(name);
        appendString(propertySeparator);
    }

    function appendString(value:String) {
        digesterWriter.writeString(value);
    }

    function endProperty(name:String) {
        appendString(endValue);
    }

    function enumHasParameters(e:EnumValue) {
        return e.getParameters() != null
            && e.getParameters().length > 0;
    }

    public function transform<Tin, Tout>(
        type:Dynamic, transformation:(Tin)->Tout): ObjectDigester
    {
        traversal.transform(type, transformation);

        return this;
    }
}