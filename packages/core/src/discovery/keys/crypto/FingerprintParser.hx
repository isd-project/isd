package discovery.keys.crypto;

import discovery.exceptions.UnsupportedParameterException;
import discovery.format.exceptions.InvalidValueException;
import haxe.ValueException;
import discovery.format.exceptions.InvalidFormatException;
import haxe.io.Bytes;

class FingerprintParser {
    public function new() {}

    public function parse(fingStr: String): Fingerprint {
        final algPattern = '[\\w\\-]+';
        final hexPattern = '[abcdefABCDEF\\d]+';
        var fingRegex = new EReg('((${algPattern}):)?(${hexPattern})', 'g');

        if(!fingRegex.match(fingStr)){
            throw new InvalidFormatException(
                'String "${fingStr}" is not a valid Fingerprint representation');
        }

        var alg = fingRegex.matched(2);
        var hex = fingRegex.matched(3);

        return new Fingerprint(toHashType(alg), toBytes(hex));
    }


    function toHashType(alg:String):HashTypes {
        if(alg == null) return null;

        var value = HashTypes.fromString(alg.toUpperCase());

        if(value == null){
            return cast alg;
        }

        return value;
    }

    function toBytes(hex:String):Bytes {
        try{
            return Bytes.ofHex(hex);
        }
        catch(e:ValueException){
            var msg = 'Invalid hexadecimal value: ${hex}';

            throw new InvalidValueException(msg, e);
        }
    }
}