package discovery.keys.crypto;


enum abstract HashTypes(String) to String{
    var Sha256   = 'SHA-256';
    var Sha384   = 'SHA-384';
    var Sha512   = 'SHA-512';
    var Sha3_512 = 'SHA3-512';
    var Sha3_384 = 'SHA3-384';
    var Sha3_256 = 'SHA3-256';
    var Sha3_224 = 'SHA3-224';
    var Sha1     = 'SHA-1';
    var Md5      = 'MD5';

    public static final values:Array<HashTypes>=[
        Sha256, Sha384, Sha512,
        Sha3_512, Sha3_384, Sha3_256, Sha3_224,
        Sha1, Md5
    ];

    public static function fromString(value: String): Null<HashTypes> {
        for (v in values){
            if(v == value){
                return v;
            }
        }

        return null;
    }
}