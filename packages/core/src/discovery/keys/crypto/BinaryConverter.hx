package discovery.keys.crypto;

import haxe.io.BytesBuffer;
import haxe.io.Bytes;

/**
    Converts basic types into binary
**/
abstract BinaryConverter(Bytes) from Bytes to Bytes{

    public static function convert(data: BinaryConverter): Bytes{
        return data;
    }

    @:from
    public static function fromInt(value: Int): BinaryConverter{
        var buffer = new BytesBuffer();
        buffer.addInt32(value);
        return buffer.getBytes();
    }
    @:from
    public static function fromFloat(value: Float): BinaryConverter{
        var buffer = new BytesBuffer();
        buffer.addFloat(value);
        return buffer.getBytes();
    }
    @:from
    public static function fromBool(value: Bool): BinaryConverter{
        var buffer = new BytesBuffer();
        buffer.addByte(if(value) 1 else 0);
        return buffer.getBytes();
    }
    @:from
    public static function fromString(value: String): BinaryConverter{
        var buffer = new BytesBuffer();
        buffer.addString(value);
        return buffer.getBytes();
    }
}