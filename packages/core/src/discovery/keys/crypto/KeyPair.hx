package discovery.keys.crypto;

import discovery.keys.crypto.CryptoKey;

@:structInit
class KeyPair{
    public var publicKey: CryptoKey;
    public var privateKey: CryptoKey;

    public function new(publicKey: CryptoKey, privateKey: CryptoKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }
}