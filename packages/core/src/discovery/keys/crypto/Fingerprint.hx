package discovery.keys.crypto;

import discovery.format.Binary;
import discovery.keys.crypto.HashTypes;
import haxe.io.Bytes;

@:structInit
class Fingerprint{
    public var alg(default, null) : HashTypes;

    /** Data of the generated fingerprint**/
    public var hash(default, null): Binary;

    //TO-DO: add type of fingerprint algorithm (default to JWK thumbprint)

    public function new(alg: HashTypes, hash: Bytes) {
        this.alg = alg;
        this.hash = hash;
    }

    public function equals(other: Fingerprint): Bool {
        return other != null
            && alg == other.alg
            && if(hash != null && other.hash != null)
               {hash.compare(other.hash) == 0;}
               else hash == other.hash;
    }

    public function toString(): String {
        return '$alg:'
                + if(hash == null) 'null' else hash.toHex();
    }
}