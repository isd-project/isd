package discovery.keys.crypto;

import discovery.async.promise.Promisable;
import discovery.keys.storage.KeyData;

interface KeyLoader {
    function loadKey(keyData:KeyData): Promisable<KeyPair>;
    function loadExportedKey(exportedKey:ExportedKey):Promisable<CryptoKey>;
}