package discovery.keys.crypto;

import haxe.EnumTools;
import discovery.keys.crypto.KeyTypes;

@:forward
abstract KeyType(KeyTypes) from KeyTypes to KeyTypes{
    public function new(keyType:KeyTypes){
        this = keyType;
    }

    public function equals(other:KeyType) {
        if(this == null || other == null) return this==other;

        return this.getIndex() == other.getIndex()
            && paramsAreEquals(other);
    }

    function paramsAreEquals(other:KeyType) {
        if(getParams() == null){
            return other.getParams() == null;
        }
        else {
            return getParams().equals(other.getParams());
        }
    }

    public function getParams(): KeyParameters{
        switch(this){
            case RSA(params): return RSAKeyParameters.from(params);
            case EllipticCurve(params): return EllipticCurveParameters.from(params);
            default: return null;
        };
    }

    public function getParamsObject(): Dynamic {
        var params = getParams();
        return if(params != null) params.toObject() else null;
    }

    public static function createByName(name: String, params:Array<Dynamic>):KeyType
    {
        return EnumTools.createByName(KeyTypes, name, params);
    }

}