package discovery.keys.crypto;

import haxe.EnumTools;

using StringTools;

class KeyFormatTools {
    public static function getByName(e: Enum<KeyFormat>, name:String):Null<KeyFormat> {
        if(name == null) return null;

        name = name.trim().toLowerCase();

        var map:Map<String, KeyFormat> = [
            'pem' => Pem,
            'der' => Der,
            'jwk' => Jwk
        ];

        return map.get(name);
    }
}