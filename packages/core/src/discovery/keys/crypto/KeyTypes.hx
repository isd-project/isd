package discovery.keys.crypto;

import haxe.io.Bytes;

enum KeyTypes {
    EllipticCurve(?params: EllipticCurveParameters);
    RSA(?params: RSAKeyParameters);

    /** A custom key type to provide extendability. **/
    OtherKeyType(typename:String, ?params: Dynamic);
}

interface KeyParameters{
    function equals(other: KeyParameters): Bool;
    function toObject():Dynamic;
}

@:structInit
class RSAKeyParameters implements KeyParameters{
    public var modulusLength: Int;
    @:optional
    public var publicExponent: Bytes;


    public static function from(params: Dynamic): RSAKeyParameters{
        if(params == null) return null;
        return {
            modulusLength: params.modulusLength,
            publicExponent: params.publicExponent
        };
    }

    public function equals(otherParam:KeyParameters):Bool {
        var other = Std.downcast(otherParam, RSAKeyParameters);

        return other != null
            && modulusLength == other.modulusLength
            && (
                publicExponent == other.publicExponent
                ||
                (
                    publicExponent != null
                    && other.publicExponent != null
                    && publicExponent.compare(other.publicExponent) == 0
                )
            );
    }

    public function toObject():Dynamic {
        var obj:Dynamic = {};
        if(modulusLength != null){
            obj.modulusLength = modulusLength;
        }
        if(publicExponent != null){
            obj.publicExponent = publicExponent;
        }

        return obj;
    }
}

@:structInit
class EllipticCurveParameters implements KeyParameters{
    @:optional
    public var namedCurve: String;

    public function equals(otherParam:KeyParameters):Bool {
        var other = Std.downcast(otherParam, EllipticCurveParameters);

        return other != null
            && namedCurve == other.namedCurve;
    }

    public static function from(params:Null<EllipticCurveParameters>):EllipticCurveParameters
    {
        if(params == null) return null;

        return {
            namedCurve: params.namedCurve
        };
    }

    public function toObject():Dynamic {
        var obj:Dynamic = {};
        if(namedCurve != null){
            obj.namedCurve = namedCurve;
        }

        return obj;
    }
}