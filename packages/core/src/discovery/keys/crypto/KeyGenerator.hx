package discovery.keys.crypto;

import discovery.async.promise.Promisable;


typedef GenerateKeyOptions = {
    keyType: KeyTypes,
    // ?options: Dynamic //maybe: specify possible options
}

/**
    KeyGenerator is responsible for generating and instantiating the CryptoKeys used by the system.
**/
interface KeyGenerator extends KeyLoader{
    function generateKey(options: GenerateKeyOptions): Promisable<KeyPair>;
}