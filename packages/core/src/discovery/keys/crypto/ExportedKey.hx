package discovery.keys.crypto;

import discovery.format.EncodedData;
import haxe.io.Bytes;
import haxe.extern.EitherType;
import discovery.format.BytesOrText;

import discovery.utils.Comparator.*;


@:structInit
class ExportedKey {
    public var format(default, null): KeyFormat;
    public var data(default, null):BytesOrText;

    public function new(data:BytesOrText, format:KeyFormat) {
        this.data = data;
        this.format = format;
    }

    public function keyData(): EitherType<Bytes, String>{
        return switch (format){
            case Pem: data.toString();
            case Der: data.toBytes();
            case Jwk: data.toString();
        }
    }

    public function equals(other: ExportedKey) {
        return other != null
            && format == other.format
            && data.equals(other.data);
    }

    public function compare(other: ExportedKey) {
        return compareBoth(this, other);
    }

    public static function compareBoth(a:ExportedKey, b:ExportedKey):Int {
        if(a == null || b == null)
            return compareNull(a, b);

        var cmp = Reflect.compare(a.format, b.format);

        if(cmp == 0){
            cmp = EncodedData.compareBoth(a.data, b.data);
        }

        return cmp;
    }

    public function clone():Null<ExportedKey> {
        return {
            format: this.format,
            data: if(this.data == null) null else data.clone()
        }
    }
}