package discovery.keys.crypto;

import discovery.format.BytesOrText;
import haxe.io.Bytes;

class KeyFormats {
    public static function PEM(text: String): ExportedKey {
        return {
            format: KeyFormat.Pem,
            data  : text
        };
    }

    public static function DER(bytes: Bytes): ExportedKey {
        return {
            format: KeyFormat.Der,
            data  : bytes
        };
    }
}