package discovery;

import discovery.utils.time.DefaultTimerLoop;
import discovery.utils.time.TimerLoop;
import discovery.base.search.SearchOptions;
import discovery.utils.dependencies.MissingDependencyException;
import discovery.domain.operators.BooleanExpression;
import discovery.base.search.filters.ExpressionFilter;
import discovery.base.search.SearchFilter;
import discovery.keys.PKI;
import discovery.base.search.SearchProcess;
import discovery.base.publication.PublicationProcess;
import discovery.async.promise.PromisableFactory;
import discovery.domain.IpDiscover;
import discovery.DiscoveryMechanism;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.domain.Identifier;
import discovery.domain.Description;
import discovery.domain.Publication;
import discovery.keys.Keychain;


class Discovery{
    var deps: DiscoveryDependencies;
    var keychainPublisher: KeychainPublisher;

    public function new(deps: DiscoveryDependencies) {
        this.deps = deps;
        this.deps.requireDependencies();

        keychainPublisher = new KeychainPublisher(deps);
    }

    public function keychain():Keychain{
        return deps.requireKeychain();
    }

    public function pki():PKI {
        return deps.requirePKI();
    }

    public function search(query:Query, ?options: SearchOptions):Search {
        var search = mechanism()
                        .search(query)
                        .applyFilter(expressionFilter(query.query));

        return doSearch(search, options);
    }

    public function locate(id:Identifier, ?options: SearchOptions):Search {
        return doSearch(mechanism().locate(id), options);
    }

    function doSearch(search:Search, ?options: SearchOptions):Search {
        var searchProcess = new SearchProcess(search, deps);
        return searchProcess.start(options);
    }

    public function publish(d:Description):Publication {
        keychainPublisher.publishKeychain();

        var publicationProcess = new PublicationProcess(d, deps);
        return publicationProcess.publish();
    }

    public function finish(callback:() -> Void) {
        mechanism().finish(callback);
    }

    // -------------------------------------------------------------------

    function mechanism()        { return deps.requireMechanism();}
    function requireKeychain()  { return deps.requireKeychain();}
    function requireIpDiscover(){ return deps.requireIpDiscover();}
    function requirePromises()  { return deps.requirePromises();}

    function expressionFilter(expression:Null<BooleanExpression>):SearchFilter {
        return new ExpressionFilter(expression);
    }
}

class KeychainPublisher {
    var deps: DiscoveryDependencies;
    var keychainPublished:Bool=false;

    public function new(deps: DiscoveryDependencies) {
        this.deps = deps;
    }

    public function publishKeychain() {
        if(keychainPublished == false){
            var keychain = deps.requireKeychain();

            pki().publishKeychain(keychain);

            keychainPublished = true;
        }
    }

    function pki(){
        return deps.requirePKI();
    }
}


@:structInit
class DiscoveryDependencies{
    @:optional public var mechanism : DiscoveryMechanism;
    @:optional public var keychain  : Keychain;
    @:optional public var pki       : PKI;
    @:optional public var ipDiscover: IpDiscover;
    @:optional public var promises  : PromisableFactory;
    @:optional public var timerLoop : TimerLoop;

    public function requireMechanism() {
        return
        requireField(mechanism, "Discovery mechanism", DiscoveryMechanism);
    }

    public function requireKeychain() {
        return requireField(keychain, "Keychain", Keychain);
    }

    public function requirePKI() {
        return requireField(pki, "PKI", PKI);
    }

    public function requireIpDiscover(): IpDiscover{
        return requireField(ipDiscover, "IpDiscover", IpDiscover);
    }

    public function requirePromises() {
        return requireField(promises, "Promises factory", PromisableFactory);
    }

    public function getTimerLoop() {
        if(timerLoop == null) timerLoop = new DefaultTimerLoop();

        return timerLoop;
    }

    function requireField<T>(field: T, name: String, type:Class<T>): T{
        if(field == null){
            throw new MissingDependencyException(type,
                'Missing Discovery dependency: $name');
        }

        return field;
    }

    public function requireDependencies() {
        requireMechanism();
        requireKeychain();
        requirePKI();
        requireIpDiscover();
        requirePromises();
    }
}