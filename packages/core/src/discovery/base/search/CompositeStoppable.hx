package discovery.base.search;

import discovery.utils.functional.CompositeCallback;
import discovery.base.search.Stoppable.StopCallback;

using discovery.utils.IteratorTools;


class CompositeStoppable implements Stoppable {
    var stoppables:Array<Stoppable>;

    public function new<T:Stoppable>(stoppables: Array<T>) {
        this.stoppables = [for(s in stoppables) s];
    }

    public function stop(?onStop:StopCallback) {
        var callback = CompositeCallback.make(stoppables.length, onStop);

        stoppables.forEach((s)->s.stop(callback));
    }
}