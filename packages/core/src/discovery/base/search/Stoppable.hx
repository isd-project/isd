package discovery.base.search;

typedef StopCallback = ()->Void;

interface Stoppable {
    function stop(?callback:StopCallback):Void;
}