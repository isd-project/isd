package discovery.base.search;

import discovery.auth.DefaultAuthenticator;
import discovery.auth.Authenticator;
import discovery.exceptions.KeyNotFoundException;
import discovery.exceptions.authentication.AuthenticationFailure;
import discovery.exceptions.authentication.ValidationExpiredException;
import discovery.async.promise.Promisable;
import discovery.async.promise.PromisableFactory;
import discovery.domain.Discovered;
import discovery.keys.crypto.signature.ObjectVerifier;
import discovery.keys.crypto.signature.Verifier.Verify;
import discovery.keys.Key;
import discovery.keys.PKI;

class DiscoveryValidator {

    var pki: PKI;
    var promises: PromisableFactory;
    var auth:Authenticator;

    public function new(pki: PKI, promises: PromisableFactory) {
        this.pki = pki;
        this.promises = promises;

        auth = new DefaultAuthenticator(promises);
    }


    public function validate(found:Discovered): Promisable<Discovered>{
        //FIXME: (once authentication is fully implemented) should fail if cannot verify
        if(!found.hasSignature() || found.providerId() == null){
            return promises.resolved(found);
        }

        if(!found.announcement().isValid()){
            return promises.rejected(
                        new ValidationExpiredException("Announcement expired"));
        }

        return searchKey(found)
                .then(validateSignature.bind(_, found));
    }

    function searchKey(discovered: Discovered): Promisable<Key>{
        var fingerprint = discovered.keyFingerprint();

        return pki.searchKey(fingerprint);
    }

    function validateSignature(key: Key, discovered: Discovered){
        if(key == null){
            var fing = discovered.keyFingerprint();
            throw new KeyNotFoundException(
                'Failed to find key with fingerprint: ${fing}');
        }

        return auth.validate(key, discovered.signed())
                .then(onVerificationResult.bind(_, discovered));
    }

    function onVerificationResult(result: Verify, discovered: Discovered) {
        if(result == false){
            throw new AuthenticationFailure(
                    'Failed to validate discovered description: signature does not match');
        }

        return discovered;
    }
}