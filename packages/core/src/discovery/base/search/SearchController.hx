package discovery.base.search;

import discovery.base.search.StopOnce;
import discovery.domain.Discovered;
import discovery.base.search.SearchBase;
import discovery.base.search.SearchControl;
import discovery.base.search.Stoppable;
import discovery.domain.Search;

using discovery.utils.time.TimeUnitTools;


typedef SearchListener = {
    discovered: (Discovered)->Void
};
typedef SearchOperation = (SearchListener)->Stoppable;

class SearchController implements SearchControl{
    var searchOp: SearchOperation;
    var stopSearch:StopOnce;
    var search:SearchBase;

    var stopped:Bool=false;

    public function new(searchOperation: SearchOperation) {
        this.searchOp = searchOperation;
        this.search = new SearchBase(this);
    }

    public function start(): Search {
        if(!stopped){
            var s:Stoppable = searchOp({
                discovered: onDiscovered
            });
            stopSearch = new StopOnce(s);
        }
        return search;
    }

    public function stop(?callback:StopCallback) {
        this.stopped = true;

        if(stopSearch != null){
            this.stopSearch.stop(callback);
        }
        else { //Not started yet
            if(callback != null)
                callback();
        }
    }

    function onDiscovered(discovered: Discovered) {
        this.search.discovered(discovered);
    }
}