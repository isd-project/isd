package discovery.base.search;

import discovery.utils.filter.FilterResult;
import discovery.impl.events.FutureOnceEvent;
import hx.concurrent.atomic.AtomicBool;
import discovery.impl.Defaults;
import discovery.utils.filter.FiltersContainerDefault;
import discovery.base.search.filters.SearchFiltersContainer;
import discovery.domain.Discovered;
import discovery.utils.events.EventEmitter;
import discovery.utils.events.EventEmitterFactory;
import discovery.domain.Search;


abstract class AbstractSearch implements Search{

    var eventFactory:EventEmitterFactory;

    var stopSearch:StopOnce;
    var finishedFlag:AtomicBool;

    public var filters(default, null): SearchFiltersContainer;

    public function new(stoppable:Stoppable, ?eventFactory:EventEmitterFactory)
    {
        this.filters = new FiltersContainerDefault();

        this.eventFactory = Defaults.eventFactory(eventFactory);
        this.onFound      = this.eventFactory.eventEmitter();
        this.onceFinished = new FutureOnceEvent();

        this.stopSearch = new StopOnce(stoppable);
        this.stopSearch.onceStop(this.finished.bind(null));

        finishedFlag = new AtomicBool(false);
    }

    public var onFound(default, null):EventEmitter<Discovered>;
    public var onceFinished(default, null):EventEmitter<FinishSearchEvent>;

    public function applyFilter(filter:SearchFilter): Search {
        this.filters.addFilter(filter);

        return this;
    }

    public function discovered(discovered: Discovered) {
        if(isNotifying()
            && filters.filter(discovered) == Include
            && filterDiscovered(discovered) == Include)
        {
            onFound.notify(discovered);
        }
    }

    function filterDiscovered(discovered:Discovered): FilterResult {
        return Include;
    }

    public function stop(?callback:()->Void) {
        this.stopSearch.stop(callback);
    }

    function finished(?info: FinishSearchEvent) {
        if(finishedFlag.getAndSet(true)){
            return; //already finished
        }

        if(info == null){
            info = {};
        }

        this.onceFinished.notify(info);
    }


    function isNotifying() {
        return !stopWasCalled() && !hasFinished();
    }

    function stopWasCalled() {
        return stopSearch.stopWasCalled();
    }

    function hasStopped() {
        return stopSearch.hasStopped();
    }

    function hasFinished() {
        return finishedFlag.value;
    }
}

