package discovery.base.search;

import discovery.utils.events.EventEmitterFactory;
import discovery.domain.Search;


typedef FinishInfo = FinishSearchEvent;

class SearchBase extends AbstractSearch{

    var control:SearchControl;

    public function new(
        control: SearchControl,
        ?eventFactory:EventEmitterFactory)
    {
        super(control, eventFactory);

        this.control = control;
    }

    //exposes from superclass
    public override function finished(?info:FinishSearchEvent) {
        super.finished(info);
    }
}

