package discovery.base.search.filters;

import discovery.utils.filter.FilterResult;
import discovery.domain.Discovered;
import discovery.format.Binary;
import discovery.domain.Identifier;


class IdentifierFilter implements SearchFilter{
    var identifier:Identifier;

    public function new(identifier: Identifier) {
        this.identifier = identifier;
    }

    public function filter(discovered:Discovered):FilterResult {
        if(discovered == null || discovered.description() == null)
            return Exclude;

        var description = discovered.description();

        if(!binaryMatch(identifier.providerId, description.providerId.id())
            || !binaryMatch(identifier.instanceId, description.instanceId)
            || identifier.name != description.name
            || identifier.type != description.type
        )
        {
            return Exclude;
        }

        return Include;
    }

    function binaryMatch(expected: Binary, found: Binary): Bool {
        return expected == null
            || Binary.areEquals(expected, found);
    }

    function stringEquals(expected: String, found: String): Bool {
        return expected == found;
    }
}