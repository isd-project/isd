package discovery.base.search.filters;

import discovery.domain.Discovered;
import discovery.utils.filter.FiltersContainer;

typedef SearchFiltersContainer = FiltersContainer<Discovered>;