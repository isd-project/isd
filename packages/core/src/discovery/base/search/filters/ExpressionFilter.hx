package discovery.base.search.filters;

import discovery.domain.operators.BooleanExpression;
import discovery.domain.Discovered;
import discovery.utils.filter.FilterResult;

class ExpressionFilter implements SearchFilter {
    var expression:BooleanExpression;

    public function new(expression: BooleanExpression) {
        this.expression = expression;
    }

    public function filter(discovered:Discovered):FilterResult {
        if(expression == null) return Include;

        return if(expression.evaluate(discovered)) Include else Exclude;
    }
}