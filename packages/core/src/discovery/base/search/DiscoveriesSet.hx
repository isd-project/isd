package discovery.base.search;


import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.domain.Identifier;
import discovery.domain.Discovered;

using discovery.utils.IteratorTools;
using discovery.utils.EqualsComparator;


class DiscoveriesSet {
    var discoveredCache:IMap<Identifier, List<Discovered>>;

    public function new() {
        discoveredCache = ComparableMap.ofComparables(Identifier);
    }

    public function addIfUnique(d: Discovered): Bool {
        if(d == null) return false;

        var id = d.identifier();
        var discoveries = discoveryList(id);

        if(findDuplicate(d, discoveries) == null){
            discoveries.add(d);

            return true;
        }

        return false;
    }

    function discoveryList(id:Identifier) {
        var discoveries = discoveredCache.get(id);

        if(discoveries == null){
            discoveries = new List();
            discoveredCache.set(id, discoveries);
        }

        return discoveries;
    }

    function findDuplicate(d:Discovered, list:List<Discovered>) {
        return list.findWhere((discovery)->{
            return discovery.equals(d);
        });
    }
}