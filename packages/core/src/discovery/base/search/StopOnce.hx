package discovery.base.search;

import discovery.async.Future;
import discovery.base.search.Stoppable.StopCallback;
import hx.concurrent.atomic.AtomicBool;

typedef StopFunction = (?StopCallback)->Void;

@:callable
abstract StopCommand(StopFunction)
    from StopFunction
    to StopFunction
{
    @:from
    public static function fromStoppable(stoppable:Stoppable): StopCommand
    {
        return stoppable.stop;
    }
}

class StopOnce {
    var stopped:AtomicBool;
    var stopCalled:AtomicBool;

    var stopCommand:StopCommand;

    var stopFuture:Future<Bool>;

    public function new(stopCommand: StopCommand)
    {
        this.stopCommand = stopCommand;
        this.stopped = new AtomicBool(false);
        this.stopCalled = new AtomicBool(false);

        stopFuture = new Future();
    }

    public function stop(?callback: StopCallback) {
        onceStop(callback);

        if(stopCalled.getAndSet(true) == true){
            return;
        }

        this.stopCommand(()->{
            stopped.value = true;

            stopFuture.notify(true);
        });
    }

    public function onceStop(callback: Null<StopCallback>) {
        if(callback == null) return;

        stopFuture.onResult(parseCallback(callback));
    }

    public function stopWasCalled(): Bool{
        return stopCalled.value;
    }

    public function hasStopped(): Bool{
        return stopped;
    }

    function parseCallback(callback:StopCallback):OnResultCallback<Bool> {
        return (result, ?err)->{
            if(err == null){
                callback();
            }
        };
    }
}