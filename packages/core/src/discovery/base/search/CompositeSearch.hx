package discovery.base.search;

import discovery.utils.functional.FailuresComposer;
import discovery.base.search.SearchBase.FinishInfo;
import discovery.utils.functional.CompositeFunction;
import discovery.utils.filter.FilterResult;
import discovery.domain.Discovered;
import discovery.domain.Search;
import discovery.utils.events.EventEmitterFactory;


class CompositeSearch extends AbstractSearch{

    var discoveriesStorage:DiscoveriesSet;

    var compositeFinish:CompositeFunction<FinishInfo, FinishInfo>;

    public function new(searches: Array<Search>, ?factory:EventEmitterFactory) {
        super(new CompositeStoppable(searches), factory);

        discoveriesStorage = new DiscoveriesSet();

        var notifySearchFinished = CompositeFunction.make({
            expectedCalls: searches.length,
            join: joinOnFinishResults,
            onResult: finished
        });

        for (s in searches){
            s.onFound.listen(discovered);
            s.onceFinished.listen(notifySearchFinished);
        }
    }

    override function filterDiscovered(discovered:Discovered):FilterResult {
        //TO-DO: handle concurrency
        if(discoveriesStorage.addIfUnique(discovered)){
            return Include;
        }

        return Exclude;
    }

    function joinOnFinishResults(results: Array<FinishSearchEvent>):FinishSearchEvent
    {
        var errors = results.map((r)->r.error);
        var composedError = FailuresComposer.compose(errors);

        return {
            error: composedError
        };
    }
}
