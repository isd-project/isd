package discovery.base.search;

import discovery.utils.filter.Filter;
import discovery.domain.Discovered;

typedef SearchFilter = Filter<Discovered>;