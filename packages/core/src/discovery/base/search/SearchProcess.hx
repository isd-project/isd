package discovery.base.search;

import discovery.utils.Cancellable;
import discovery.utils.time.TimerLoop;
import discovery.domain.Discovered;
import discovery.exceptions.IllegalArgumentException;
import discovery.Discovery.DiscoveryDependencies;
import discovery.domain.Search;

class SearchProcess implements SearchControl{
    var delegateSearch:Search;
    var filteredSearch:SearchBase;

    var validator:DiscoveryValidator;

    var timerLoop:TimerLoop;
    var timeoutCancellable:Cancellable;

    public function new(search: Search, deps: DiscoveryDependencies) {
        IllegalArgumentException
            .require(search != null, "Cannot start search process with null search");

        this.validator = new DiscoveryValidator(deps.pki, deps.promises);
        this.timerLoop = deps.getTimerLoop();

        this.filteredSearch = new SearchBase(this);
        this.delegateSearch = search;

        this.delegateSearch.onFound.listen(onFound);
        this.delegateSearch.onceFinished.listen(onFinish);
    }

    // -------------------------------------------

    public function start(?searchOptions: SearchOptions):Search {
        setTimeout(searchOptions);

        return filteredSearch;
    }

    function setTimeout(?searchOptions:SearchOptions) {
        if(searchOptions == null || searchOptions.timeout == null)
            return;

        timeoutCancellable = timerLoop.delay(()->{
            stop();
        }, searchOptions.timeout);
    }


    public function stop(?callback:() -> Void) {
        delegateSearch.stop(callback);
    }

    function onFinish(_: FinishSearchEvent):Void{
        if(timeoutCancellable != null){
            timeoutCancellable.cancel();
        }

        this.filteredSearch.finished();
    }

    function onFound(found:Discovered):Void{
        //TO-DO: should have a 'unauthenticated' event, that notify all services (even the unauthenticated)

        //Maybe: define async filter to allow validation

        validator.validate(found)
            .then(notifyDiscovered)
            .catchError((err)->{
                trace('key verification failed: ${err}');
            });
    }

    function notifyDiscovered(discovered: Discovered) {
        filteredSearch.discovered(discovered);
    }
}