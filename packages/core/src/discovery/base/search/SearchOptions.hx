package discovery.base.search;

import discovery.utils.time.Duration;

typedef SearchOptions = {
    ?timeout: Duration
};