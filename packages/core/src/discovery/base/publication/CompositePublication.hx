package discovery.base.publication;

import hx.concurrent.atomic.AtomicBool;
import discovery.utils.functional.CompositeDoneCallback;
import discovery.utils.functional.CompositeCallback;
import discovery.domain.PublishListeners;
import discovery.domain.PublishInfo;

class CompositePublication {
    var mechanisms:Array<DiscoveryMechanism>;
    var listeners:PublishListeners;
    var publishInfo:PublishInfo;

    var compositeListener:PublishListeners;

    var publicationCount:Int=0;

    public function new(
        mechanisms : Array<DiscoveryMechanism>,
        publishInfo: PublishInfo,
        ?listeners : PublishListeners)
    {
        this.mechanisms = mechanisms;
        this.publishInfo = publishInfo;
        this.listeners = if(listeners == null) {} else listeners;

        this.compositeListener = makeCompositeListener();
    }

    public function start(): PublicationControl{
        var controls = mechanisms.map((m)->{
            return m.publish(publishInfo, makeListenerFor(m));
        });

        return new CompositePublicationControl(controls);
    }

    function makeCompositeListener():PublishListeners {
        var l:PublishListeners = {};
        var expectedCalls = mechanisms.length;

        l.onPublish = CompositeCallback.make(expectedCalls, ()->{
            if(listeners.onPublish != null && publicationCount > 0)
                listeners.onPublish();
        });

        l.onFinish = CompositeDoneCallback.make(expectedCalls, (?err)->{
            if(listeners.onFinish != null)
                listeners.onFinish(err);
        });

        return l;
    }

    function makeListenerFor(m:DiscoveryMechanism):Null<PublishListeners> {
        var published = new AtomicBool();

        function notifyPublished(realPublication:Bool) {
            if(realPublication)
                publicationCount++;
            if(!published.getAndSet(true))
                compositeListener.onPublish();
        }

        return {
            onPublish: notifyPublished.bind(true),
            onFinish: (?err)->{
                notifyPublished(false);
                compositeListener.onFinish(err);
            }
        };
    }

}