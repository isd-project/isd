package discovery.base.publication;

import discovery.domain.ErrorEventInfo;
import discovery.utils.events.EventEmitter;
import discovery.utils.events.EventEmitterFactory;
import discovery.domain.PublishInfo;
import haxe.Exception;
import discovery.domain.Publication;


class PublicationDefault implements Publication {

    var mechanism:DiscoveryMechanism;
    var eventFactory:EventEmitterFactory;
    var control:PublicationControl;

    public function new(
        mechanism: DiscoveryMechanism,
        ?eventFactory: EventEmitterFactory)
    {
        this.mechanism = mechanism;
        this.control = null;

        if (eventFactory == null){
            eventFactory = new discovery.impl.Defaults.EventEmitterFactory();
        }
        this.eventFactory = eventFactory;

        this.oncePublished = this.eventFactory.eventEmitter({once:true});
        this.onceFinished  = this.eventFactory.eventEmitter({once:true});
        this.onInternalError = this.eventFactory.eventEmitter();
    }

    public var oncePublished(default, null): EventEmitter<PublishedEventInfo>;
    public var onceFinished(default, null)   :EventEmitter<PublicationFinished>;
    public var onInternalError(default, null):EventEmitter<ErrorEventInfo>;

    public function start(publishInfo: PublishInfo) {
        control = mechanism.publish(publishInfo,
        {
            onPublish: started.bind(publishInfo),
            onFinish: finished
        });
    }

    public function stop() {
        if(control == null){//not started yet
            return;
        }

        this.control.stop(finished.bind(null));
    }

    public function started(publishedInfo: PublishInfo) {
        this.oncePublished.notify({
            published: publishedInfo,
            publication: this
        });
    }

    public function finished(?err:Exception) {
        this.onceFinished.notify({error: err, publication: this});
    }

    public function notifyInternalError(errorInfo:ErrorEventInfo) {
        onInternalError.notify(errorInfo);
    }
}