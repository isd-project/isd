package discovery.base.publication;

import discovery.auth.DefaultAuthenticator;
import discovery.auth.Authenticator;
import haxe.PosInfos;
import haxe.Exception;
import discovery.utils.time.Duration;
import discovery.domain.Announcement;
import discovery.domain.PublishInfo;
import discovery.keys.crypto.signature.Signed;
import discovery.async.promise.Promisable;
import discovery.keys.Key;
import discovery.domain.IPAddress;
import discovery.domain.Publication;
import discovery.domain.Description;
import discovery.Discovery.DiscoveryDependencies;

class PublicationProcess {
    public static final defaultAnnouncementValidity = Duration.of(12, Hours);

    var description:Description;
    var deps:DiscoveryDependencies;

    var providerKey: Key;
    var publication:PublicationDefault;

    var auth:Authenticator;

    public function new(description: Description, deps: DiscoveryDependencies) {
        this.description = description;
        this.deps = deps;

        this.auth = new DefaultAuthenticator(deps.requirePromises());
    }

    public function publish(): Publication {
        this.publication = new PublicationDefault(mechanism());

        //TO-DO: handle promise error
        getAndPublishKey()
            .mergeWith([listIps()])
            .then(updateDescription)
            .then(makeAnnouncement)
            .then(signAnnouncement)
            .then(startPublication)
            ;

        return publication;
    }

    function getAndPublishKey(): Promisable<Key> {
        return getKey().then(publishKey);
    }

    function getKey(): Promisable<Null<Key>> {
        if(description.providerId == null){
            return promises().resolved(null);
        }

        return requireKeychain()
                .getKey(description.providerId)
                .catchError(function(err):Key{
                    /* Note: "fixing" error by returning null key.
                    We are still allowing publication without key,
                    but it should be required (once the implementation is finished). */

                    return null;
                })
                ;
    }

    function publishKey(key: Null<Key>) {
        if(key == null) return promises().resolved(key);

        //TO-DO: listen and notify publication internal errors
        return pki().publishKey(key, {
            onInternalError: (err)->{
                notifyError(err, 'Publishing key');
            }
        });
    }

    function listIps() {
        return requireIpDiscover().listPublicIps();
    }

    function updateDescription(results:Array<Dynamic>) {
        providerKey = results[0];
        var ips:Array<IPAddress> = results[1];

        updateProviderId(providerKey);
        updateDescriptionIps(ips);

        return description;
    }

    function updateProviderId(providerKey: Key) {
        if(providerKey != null){
            description.providerId = providerKey.fingerprint;
        }
    }

    function updateDescriptionIps(ips:Array<IPAddress>) {
        //TO-DO: change only if application have not specified(?), to allow fixed IPs
        description.location.addresses = ips;
    }

    function makeAnnouncement(description: Description){
        //TO-DO: allow to configure, announcement validity
        //TO-DO: should resend announcement before validation expires
        return new Announcement(description, defaultAnnouncementValidity);
    }

    function signAnnouncement(
        announcement: Announcement): Promisable<Signed<Announcement>>
    {
        if(providerKey == null){//TO-DO: require key
            return promises().resolved(Signed.make(announcement, null));
        }

        return auth.sign(providerKey, announcement);
    }

    function startPublication(announcement: Signed<Announcement>)
    {
        var publishInfo = new PublishInfo(announcement, providerKey);
        publication.start(publishInfo);

        return publication;
    }

    function notifyError(error:Exception, ?context:String, ?posInfo:PosInfos) {
        this.publication.notifyInternalError({
            error: error,
            posInfo: posInfo,
            context: context
        });
    }

    // -------------------------------------------------------------------

    function mechanism()        { return deps.requireMechanism();}
    function requireKeychain()  { return deps.requireKeychain();}
    function requireIpDiscover(){ return deps.requireIpDiscover();}
    function promises()         { return deps.requirePromises();}
    function pki()              { return deps.requirePKI();}
}