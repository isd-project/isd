package discovery.base.publication;

import discovery.base.search.CompositeStoppable;
import discovery.base.search.Stoppable.StopCallback;

class CompositePublicationControl implements PublicationControl{
    var compositeStoppable:CompositeStoppable;

    public function new(controls:Array<PublicationControl>) {
        compositeStoppable = new CompositeStoppable(controls.map((c)->c));
    }

	public function stop(?callback:StopCallback) {
        compositeStoppable.stop(callback);
    }
}