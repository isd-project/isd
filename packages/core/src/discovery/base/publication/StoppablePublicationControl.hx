package discovery.base.publication;

import discovery.base.search.Stoppable;


class StoppablePublicationControl implements PublicationControl{
    var stoppable: Stoppable;

    public function new(stoppable: Stoppable) {
        this.stoppable = stoppable;
    }

    public function stop(?callback:StopCallback) {
        this.stoppable.stop(callback);
    }
}