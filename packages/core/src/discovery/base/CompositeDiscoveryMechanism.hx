package discovery.base;

import discovery.base.publication.CompositePublication;
import discovery.utils.functional.CompositeCallback;
import discovery.base.search.CompositeSearch;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.domain.Identifier;
import discovery.domain.PublishInfo;
import discovery.domain.PublishListeners;
import discovery.base.publication.PublicationControl;

using discovery.utils.IteratorTools;


class CompositeDiscoveryMechanism implements DiscoveryMechanism{
    var mechanisms:Array<DiscoveryMechanism>;

    public function new(mechanisms: Array<DiscoveryMechanism>) {
        this.mechanisms = mechanisms;
    }

    public function finish(callback:() -> Void) {
        var composed = CompositeCallback.make(mechanisms.length, callback);

        mechanisms.forEach((m)->m.finish(composed));
    }

    public function search(query:Query):Search {
        return compositeSearch((m)->m.search(query));
    }

    public function locate(id:Identifier):Search {
        return compositeSearch((m)->m.locate(id));
    }

    function compositeSearch(makeSearch: (DiscoveryMechanism)->Search) {
        var searches = mechanisms.map(makeSearch);

        return new CompositeSearch(searches);
    }

    public function publish(info:PublishInfo, ?listeners:PublishListeners):PublicationControl
    {
        var publication = new CompositePublication(
                                mechanisms.copy(), info, listeners);

        return publication.start();
    }
}