package discovery.base;

import discovery.domain.IdentifierUrl;

interface IdentifierParser{
    /**
        Parse an identifier url.

        General url syntax:
            `srv://[<instance id>.]<name>.<type>.<id>[;<attrib>=<value>]*[/<path>][?<query>][#<fragment>]`


        @throws InvalidFormatException if the url is invalid.
        @throws ParseException if another error ocurred on parsing.
    **/
    function parseUrl(url:String):IdentifierUrl;
}