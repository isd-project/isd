package discovery.base.configuration.composite;

import discovery.base.configuration.NamedValue.Named;

interface CompositeBuilder<Builder> {
    function addBuilder(builder:Builder, name:String): Void;
    function listBuilders(): Iterable<Named<Builder>>;

    function selectBuilders(toIncludeNames:Array<String>): Void;
    function listSelected():Iterable<String>;
}