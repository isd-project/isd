package discovery.base.configuration.composite;

import discovery.base.configuration.DiscoveryMechanismBuilder;
import discovery.base.configuration.NamedValue.Named;


typedef NamedMechanismBuilder = Named<DiscoveryMechanismBuilder>;