package discovery.base.configuration.composite;

import haxe.DynamicAccess;
import discovery.base.configuration.composite.BaseCompositeBuilder;
import discovery.keys.keydiscovery.CompositeKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.KeyTransportBuilder;

class CompositeKeyTransportBuilder
    extends BaseCompositeBuilder<KeyTransportBuilder>
    implements KeyTransportBuilder
{
    public function configure(config:DynamicAccess<Any>) {
        for(k=>config in config.keyValueIterator()){
            var builder = buildersMap.get(k);

            if(Std.isOfType(builder, Configurable)){
                var c = cast(builder, Configurable<Dynamic>);
                c.configure(config);
            }
        }
    }

    public function buildKeyTransport():KeyTransport {
        var components = filteredBuilders().map((builder)->{
            return builder.buildKeyTransport();
        });

        return new CompositeKeyTransport(components);
    }
}