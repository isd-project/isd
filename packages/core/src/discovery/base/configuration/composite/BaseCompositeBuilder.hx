package discovery.base.configuration.composite;

import discovery.base.configuration.NamedValue.Named;
import discovery.exceptions.IllegalArgumentException;


class BaseCompositeBuilder<BuilderT> implements CompositeBuilder<BuilderT> {
    var builders:Array<BuilderT>;
    var buildersMap:Map<String, BuilderT>;

    var selectedBuilders:Array<String>;

    public function new() {
        builders = [];
        buildersMap = new Map();

        selectedBuilders = null;
    }

    public function addBuilder(builder:BuilderT, name:String) {
        if(name != null && buildersMap.exists(name)){
            throw new IllegalArgumentException(
                'There is already a builder named "${name}"');
        }

        builders.push(builder);

        if(name != null){
            buildersMap.set(name, builder);
        }
    }

    public function selectBuilders(toIncludeNames:Array<String>) {
        this.selectedBuilders = toIncludeNames;
    }


    function filteredBuilders(): Array<BuilderT>{
        if(selectedBuilders == null) return builders;

        var filtered:Array<BuilderT> = [];

        for (selected in selectedBuilders){
            var builder = buildersMap.get(selected);

            if(builder != null){
                filtered.push(builder);
            }
        }

        return filtered;
    }

    public function listBuilders():Iterable<Named<BuilderT>> {
        return [for(key=>value in buildersMap.keyValueIterator()) {
            name: key,
            value: value
        }];
    }

    public function listSelected():Iterable<String>{
        return selectedBuilders;
    }
}