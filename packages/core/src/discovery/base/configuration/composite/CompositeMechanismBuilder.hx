package discovery.base.configuration.composite;

import discovery.base.configuration.NamedValue;
import discovery.base.configuration.composite.BaseCompositeBuilder;
import discovery.base.configuration.DiscoveryMechanismBuilder;


class CompositeMechanismBuilder
    extends BaseCompositeBuilder<DiscoveryMechanismBuilder>
    implements DiscoveryMechanismBuilder
{
    public function new() {
        super();
    }

    /**
        Returns one of the builders named with "name", or null if there is none.
    **/
    public function getBuilder(name:String): DiscoveryMechanismBuilder {
        return buildersMap.get(name);
    }

    public function buildMechanism():DiscoveryMechanism {
        var filtered = filteredBuilders();
        var mechanisms = filtered.map((b)->b.buildMechanism());

        return new CompositeDiscoveryMechanism(mechanisms);
    }
}