package discovery.base.configuration;

interface Configurable<T> {
    function configure(config: T): Void;
}