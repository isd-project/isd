package discovery.base.configuration;

import discovery.base.configuration.composite.NamedMechanismBuilder;


interface DiscoveryBuilder{
    function build():Discovery;
    function addMechanism(builder:DiscoveryMechanismBuilder, ?name:String):Void;
    function listMechanismBuilders():Iterable<NamedMechanismBuilder>;
}