package discovery.base.configuration;

typedef NamedValue<T> = {
    name: String,
    value: T
};

typedef Named<T> = NamedValue<T>;