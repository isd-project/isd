package discovery.base.configuration;

typedef GetterFunction<T> = ()->T;

@:callable
abstract AbstractGetter<T>(GetterFunction<T>)
    from GetterFunction<T>
    to GetterFunction<T>
{
    public function new(getter: GetterFunction<T>) {
        this = getter;
    }

    @:from
    public static function fromValue<T>(value: T): AbstractGetter<T> {
        return ()->value;
    }


    @:from
    public static function fromSuperValue<T, TSuper:T>(value: TSuper): AbstractGetter<T> {
        return ()->value;
    }
}