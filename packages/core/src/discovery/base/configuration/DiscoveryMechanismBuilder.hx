package discovery.base.configuration;

interface DiscoveryMechanismBuilder {
    function buildMechanism(): DiscoveryMechanism;
}