package discovery.base.configuration;

import discovery.keys.PKI;

interface PkiBuilder {
    function buildPki(): PKI;
}