package discovery;

import discovery.base.publication.PublicationControl;
import discovery.domain.*;


interface DiscoveryMechanism{
    /**
        Terminates any discovery process and releases open resources
    **/
    function finish  ( callback: ()->Void)    : Void;

    function search  ( query     : Query     )  : Search;
    function locate  ( id        : Identifier)  : Search;
    function publish ( info  : PublishInfo,
                        ?listeners: PublishListeners) : PublicationControl;
}
