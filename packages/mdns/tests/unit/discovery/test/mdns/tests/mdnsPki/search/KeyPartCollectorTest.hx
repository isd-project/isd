package discovery.test.mdns.tests.mdnsPki.search;

import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.storage.PublicKeyData;
import discovery.test.integration.data.KeysTestData;
import discovery.mdns.pki.MdnsPkiMessages;
import discovery.mdns.pki.publish.KeySplitStrategy;
import discovery.mdns.pki.search.KeyPartCollector;
import discovery.keys.storage.KeyData;
import discovery.mdns.pki.publish.RangeKeySplitStrategy;
import discovery.mdns.pki.publish.KeyDescriptorBuilder;
import discovery.keys.crypto.ExportedKey;
import discovery.test.helpers.RandomGen;
import discovery.mdns.pki.KeyDescriptor;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;
using discovery.keys.KeyTransformations;


class KeyPartCollectorTest implements Scenario{
    @steps
    var steps: KeyPartCollectorSteps;


    @Test
    @scenario
    public function get_first_part_to_fetch(){
        given().a_key_part_collector_for_some_key();
        when().getting_the_next_part_to_fetch();
        then().the_part_number_x_should_be_returned(0);
    }

    @Test
    @scenario
    public function get_second_part_to_fetch(){
        given().a_key_part_collector_for_some_key();
        when().adding_the_next_part();
        when().getting_the_next_part_to_fetch();
        then().the_part_number_x_should_be_returned(1);
    }


    @Test
    @scenario
    public function next_part_should_return_null_when_complete(){
        given().a_key_part_collector_for_some_key();
        when().adding_all_key_parts();
        when().getting_the_next_part_to_fetch();
        then().a_null_part_should_be_returned();
    }


    @Test
    @scenario
    public function should_detect_when_completed_parts(){
        given().a_key_descriptor_for_some_key();
        given().a_key_part_collector_for_that_key_descriptor();
        when().adding_all_key_parts();
        then().collector_should_detect_it_has_completed();
    }

    @Test
    @scenario
    public function join_parts(){
        given().a_key_part_collector_for_some_key();
        when().adding_all_key_parts();
        and().joining_the_parts();
        then().the_expected_key_should_be_created();
    }


    @Test
    @scenario
    public function join_key(){
        given().a_key_part_collector_for_this_key(KeysTestData.rsaKey4096);
        when().adding_all_key_parts();
        and().joining_the_parts();
        then().the_expected_key_should_be_created();
    }
}

class KeyPartCollectorSteps extends Steps<KeyPartCollectorSteps>{

    var partCollector:KeyPartCollector;

    var splitStrategy:KeySplitStrategy;
    var descriptorBuilder:KeyDescriptorBuilder;

    var keyData:KeyData;
    var exportedKey:ExportedKey;
    var joinedKey:ExportedKey;

    var keyDescriptor:KeyDescriptor;
    var nextPart:KeyPartReference;

    public function new(){
        super();

        splitStrategy = new RangeKeySplitStrategy(MdnsPkiMessages.defaultKeyPartLength());
        descriptorBuilder = new KeyDescriptorBuilder(splitStrategy);
    }

    // given summary --------------------------------------------

    @step
    public function a_key_part_collector_for_some_key() {
        given().a_key_descriptor_for_some_key();
        given().a_key_part_collector_for_that_key_descriptor();
    }

    @step
    public function a_key_part_collector_for_this_key(pubKey:PublicKeyData) {
        given().a_key_descriptor_for_this_key(pubKey.convertToKeyData());
        given().a_key_part_collector_for_that_key_descriptor();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_descriptor_for_some_key() {
        keyData = RandomGen.crypto.keyData();

        given().a_key_descriptor_for_this_key(keyData);
    }

    @step
    function a_key_descriptor_for_this_key(keyData:KeyData) {
        exportedKey = keyData.publicKey;

        keyDescriptor = descriptorBuilder.buildDescriptor(keyData);
    }

    @step
    public function a_key_part_collector_for_that_key_descriptor() {
        partCollector = new KeyPartCollector(keyDescriptor);
    }

    // when  ---------------------------------------------------

    @step
    public function adding_all_key_parts() {
        for(partRef in keyDescriptor.keyParts){
            var part = splitStrategy.extractKeyPart(exportedKey, partRef.part);

            partCollector.addPart(part);
        }
    }

    @step
    public function adding_the_next_part() {

        var nextPart = partCollector.nextPartRef();

        var part = splitStrategy.extractKeyPart(exportedKey, nextPart.part);

        partCollector.addPart(part);
    }

    @step
    public function joining_the_parts() {
        joinedKey = partCollector.joinParts();
    }

    @step
    public function getting_the_next_part_to_fetch() {
        nextPart = partCollector.nextPartRef();
    }

    // then  ---------------------------------------------------

    @step
    public function collector_should_detect_it_has_completed() {
        partCollector.hasAllParts().shouldBe(true,
            'Part collector should have all parts');
    }

    @step
    public function the_expected_key_should_be_created() {
        joinedKey.equals(exportedKey).shouldBe(true,
            'Expected: ${exportedKey}\n'
            +'but produced: ${joinedKey}'
        );
    }

    @step
    public function the_part_number_x_should_be_returned(idx:Int) {
        var expectedPart = keyDescriptor.keyParts[idx];

        nextPart.shouldBeEqualsTo(expectedPart);
    }

    @step
    public function a_null_part_should_be_returned() {
        nextPart.shouldBe(null);
    }
}