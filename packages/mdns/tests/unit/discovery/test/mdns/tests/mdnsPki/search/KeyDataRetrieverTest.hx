package discovery.test.mdns.tests.mdnsPki.search;

import discovery.format.Binary;
import discovery.test.fakes.FakePromisableFactory;
import discovery.mdns.pki.search.KeyDataRetriever;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.keys.crypto.ExportedKey;
import discovery.test.helpers.RandomGen;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.KeyDescriptor;
import discovery.test.mdns.mockups.pki.KeyEnquirerMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class KeyDataRetrieverTest implements Scenario{
    @steps
    var steps: KeyDataRetrieverSteps;

    @Test
    @scenario
    public function retrieve_simple_key(){
        given().a_key_descriptor_of_one_part();
        when().the_key_retrieval_starts();
        and().the_queries_are_resolved();
        then().an_exported_key_should_be_produced_from_that_key_part();
    }

    @Test
    @scenario
    public function retrieve_key_with_multiple_parts(){
        given().a_key_descriptor_with_multiple_parts();
        when().the_key_retrieval_starts();
        and().the_queries_are_resolved();
        then().an_exported_key_should_be_produced_from_joining_the_key_parts();
    }
}

class KeyDataRetrieverSteps extends Steps<KeyDataRetrieverSteps>{
    var retriever: KeyDataRetriever;

    var enquirer:KeyEnquirerMockup;
    var promises:FakePromisableFactory;
    var mdnsPkiGenerator:MdnsPkiGenerator;

    var keyDescriptor:KeyDescriptor;
    var keyParts: Array<KeyPart>;
    var expectedKey: ExportedKey;
    var retrievedKey: ExportedKey;

    public function new(){
        super();

        enquirer = new KeyEnquirerMockup();
        promises = new FakePromisableFactory();
        mdnsPkiGenerator = new MdnsPkiGenerator();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_descriptor_of_one_part() {
        given().a_key_descriptor_with(1, 1);
    }

    @step
    public function a_key_descriptor_with_multiple_parts() {
        given().a_key_descriptor_with(2); //2 or more parts
    }

    @step
    public function a_key_descriptor() {
        given().a_key_descriptor_with();
    }

    @step
    function a_key_descriptor_with(?minParts:Int, ?maxParts:Int) {
        keyDescriptor = mdnsPkiGenerator.keyDescriptor(minParts, maxParts);

        given().a_key_divided_in_x_parts(keyDescriptor.keyParts.length);
    }

    function a_key_divided_in_x_parts(numParts:Int) {
        var format = keyDescriptor.keyFormat;

        expectedKey = RandomGen.crypto.exportedKeyWithFormat(format);
        keyParts = splitParts(expectedKey, numParts);

        enquirer.whenGettingKeyPartsReturn(keyParts);
    }

    @step
    public function a_key_retrieval_for_that_descriptor() {
        retriever = new KeyDataRetriever(keyDescriptor, {
            enquirer: enquirer.enquirer(),
            promises: promises
        });
    }

    // when  ---------------------------------------------------

    @step
    public function starting_the_key_retrieval() {
        retriever.getKeyData().then((exportedKey)->{
            retrievedKey = exportedKey;
        });
    }

    @step
    public function the_key_retrieval_starts() {
        given().a_key_retrieval_for_that_descriptor();
        when().starting_the_key_retrieval();
    }

    @step
    public function the_queries_are_resolved() {
        //queries already resolved
    }

    // then  ---------------------------------------------------

    @step
    public function a_query_should_be_sent_to_get_the_first_key_part() {
        enquirer.assertGetKeyPartWasCalledWith(keyDescriptor.keyParts[0]);
    }

    @step
    public function
        an_exported_key_should_be_produced_from_joining_the_key_parts()
    {
        then().the_expected_key_should_be_produced();
    }

    @step
    public function an_exported_key_should_be_produced_from_that_key_part() {
        then().the_expected_key_should_be_produced();
    }

    @step
    function the_expected_key_should_be_produced() {
        assertThat(retrievedKey, is(notNullValue()), "No key retrieved");
        assertThat(retrievedKey.format,
            is(equalTo(expectedKey.format)), "Retrieved key format is not the expected");

        var retrievedData:Binary = retrievedKey.data.toBytes();
        var expectedData:Binary  = expectedKey.data.toBytes();

        assertThat(retrievedData, is(equalTo(expectedData)),
            'Retrieved key data does not match the expected value'
            );
    }


    // ------------------------------------------------------------------------

    function splitParts(expectedKey:ExportedKey, numParts:Int):Array<KeyPart> {
        var data = expectedKey.data.toBytes();
        var partsSize:Int = Math.ceil(data.length/numParts);
        var dataPos = 0;

        var parts:Array<KeyPart> = [];

        for(i in 0...numParts){
            var partSize:Int = partsSize;

            if(partSize + dataPos > data.length){
                partSize = data.length - dataPos;
            }

            var partData = data.sub(dataPos, partSize);

            dataPos += partSize;

            parts.push({
                data: partData
            });
        }

        return parts;
    }
}