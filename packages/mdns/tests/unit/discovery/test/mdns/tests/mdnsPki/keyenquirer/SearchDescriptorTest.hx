package discovery.test.mdns.tests.mdnsPki.keyenquirer;

import discovery.base.search.Stoppable;
import discovery.test.fakes.FakePromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.MdnsPkiMessages;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.search.defaults.DefaultKeyEnquirer;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import discovery.mdns.pki.search.KeyEnquirer;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class SearchDescriptorTest implements Scenario{
    @steps
    var steps: SearchDescriptorSteps;

    @Test
    @scenario
    public function query_key_descriptor(){
        given().a_key_enquirer_instance();
        given().some_key_fingerprint();
        when().searching_for_descriptors_of_this_key();
        then().a_request_for_the_query_descriptors_should_be_sent();
        and().a_handle_to_stop_the_query_should_be_returned();
    }

    @Test
    @scenario
    public function notify_key_descriptor_found(){
        given().a_query_for_some_key_descriptor_started();
        when().a_key_descriptor_response_is_received();
        then().the_key_descriptor_should_be_delivered_to_the_query_listener();
    }
}

class SearchDescriptorSteps extends Steps<SearchDescriptorSteps>{
    var enquirer:KeyEnquirer;

    var mdnsEngine:MdnsEngineMockup;
    var promises: PromisableFactory;

    var pkiMessages:MdnsPkiMessages;
    var generator:MdnsPkiGenerator;

    var keyFingerprint:Fingerprint;
    var expectedKeyDescriptor:KeyDescriptor;
    var receivedKeyDescriptor:KeyDescriptor;
    var returnedStoppable:Stoppable;


    public function new(){
        super();

        mdnsEngine = new MdnsEngineMockup();
        promises = new FakePromisableFactory();

        pkiMessages = new MdnsPkiMessages();
        generator = new MdnsPkiGenerator();
    }

    // summary -----------------------------------

    @step
    public function a_query_for_some_key_descriptor_started() {
        given().a_key_enquirer_instance();
        and().some_key_fingerprint();
        when().searching_for_descriptors_of_this_key();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_enquirer_instance() {
        enquirer = new DefaultKeyEnquirer(mdnsEngine.engine(), promises);
    }

    @step
    public function some_key_fingerprint() {
        keyFingerprint = RandomGen.crypto.fingerprint();
    }


    // when  ---------------------------------------------------

    @step
    public function searching_for_descriptors_of_this_key() {
        returnedStoppable = enquirer.searchKeyDescriptor(keyFingerprint, {
            onFound: (found: KeyDescriptorFound)->{
                receivedKeyDescriptor = found.descriptor;
            }
        });
    }


    @step
    public function a_key_descriptor_response_is_received() {
        given().a_key_descriptor();

        mdnsEngine.respondLastQueryWith(
            pkiMessages.descriptorTxt(expectedKeyDescriptor));
    }

    function a_key_descriptor() {
        expectedKeyDescriptor = generator.keyDescriptor();
    }


    // then  ---------------------------------------------------

    @step
    public function a_request_for_the_query_descriptors_should_be_sent() {
        var expectedName = pkiMessages.descriptorUrl(keyFingerprint);
        var expectedType = DnsRecordType.TXT;

        mdnsEngine.last_query_should_match(expectedName, expectedType);
    }

    @step
    public function a_handle_to_stop_the_query_should_be_returned() {
        assertThat(returnedStoppable, is(notNullValue()));
    }

    @step
    public
    function the_key_descriptor_should_be_delivered_to_the_query_listener()
    {
        assertThat(receivedKeyDescriptor, is(notNullValue()),
            "no key descriptor was received");

        assertThat(expectedKeyDescriptor.equals(receivedKeyDescriptor),
            is(true), "Key descriptor does not match expected value");
    }
}