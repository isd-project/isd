package discovery.test.mdns.tests.mdnsPki.publish;

import discovery.mdns.pki.KeyDescriptor;
import discovery.test.helpers.RandomGen;
import discovery.keys.storage.KeyData;
import discovery.test.mdns.mockups.pki.FakeKeySplitStrategy;
import discovery.mdns.pki.publish.KeySplitStrategy;
import discovery.mdns.pki.publish.KeyDescriptorBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class KeyDescriptorBuilderTest implements Scenario{
    @steps
    var steps: KeyDescriptorBuilderSteps;

    @Test
    @scenario
    public function build_descriptor_for_key_data(){
        given().a_descriptor_builder_with_some_key_split_strategy();
        given().some_key_data();
        when().building_a_key_descriptor_from_it();
        then().a_correspondent_key_descriptor_should_be_generated();
        and().the_key_parts_reference_should_be_produced();
    }
}

class KeyDescriptorBuilderSteps extends Steps<KeyDescriptorBuilderSteps>{
    var splitStrategy:KeySplitStrategy;
    var descriptorBuilder:KeyDescriptorBuilder;

    var keyData:KeyData;
    var keyDescriptor:KeyDescriptor;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_descriptor_builder_with_some_key_split_strategy() {
        splitStrategy = new FakeKeySplitStrategy();
        descriptorBuilder = new KeyDescriptorBuilder(splitStrategy);
    }

    @step
    public function some_key_data() {
        keyData = RandomGen.crypto.keyData();
    }

    // when  ---------------------------------------------------

    @step
    public function building_a_key_descriptor_from_it() {
        keyDescriptor = descriptorBuilder.buildDescriptor(keyData);
    }

    // then  ---------------------------------------------------

    @step
    public function a_correspondent_key_descriptor_should_be_generated() {
        assertThat(keyDescriptor, is(notNullValue()), "no key descriptor built");

        var expectedFingerprint = keyData.fingerprint.toFingerprint();
        var expectedData = keyData.publicKey.data.toBytes();

        assertThat(keyDescriptor.keyType, is(keyData.keyType), "key type");
        assertThat(keyDescriptor.fingerprint, is(equalTo(expectedFingerprint)),
            "fingerprint does not match");
        assertThat(keyDescriptor.keyFormat, is(keyData.publicKey.format),
            "Key format does not match");
        assertThat(keyDescriptor.keySize, is(expectedData.length),
            "Key size does not match");
    }

    @step
    public function the_key_parts_reference_should_be_produced() {
        var expected = splitStrategy.partitionKey(
                                        keyData.publicKey, keyData.fingerprint);

        assertThat(keyDescriptor.keyParts.equals(expected), is(true),
            'Key parts references does not match\n'+
            '\texpected: ${expected}\n'+
            '\tbut found: ${keyDescriptor.keyParts}'
        );
    }
}