package discovery.test.mdns.tests.mdnsTxtConverter;

import discovery.domain.Validity;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.HashTypes;
import discovery.utils.traversal.ObjectTransformation;
import haxe.io.Bytes;
import discovery.domain.ProviderId;
import discovery.format.Binary;
import discovery.mdns.MdnsTxtConverter;
import discovery.domain.Description;
import discovery.test.helpers.RandomGen;
import discovery.mdns.TxtFields;
import discovery.mdns.MdnsTxt;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class TxtToDescriptionTest implements Scenario{
    @steps
    var steps: TxtToDescriptionSteps;

    @Test
    @scenario
    public function convert_from_simple_txt(){
        given().txt_with_basic_description();
        when().converting_to_a_description_instance();
        then().this_description_should_be_produced({
            name: txtField(TxtFields.name),
            type: txtField(TxtFields.type),
            providerId: ProviderId.fromFingerprint({
                alg: HashTypes.fromString(txtField(TxtFields.providerIdHashAlg)),
                hash: txtField(TxtFields.providerId).toBinary()
            }),
            instanceId: txtField(TxtFields.instanceId)
        });
    }

    @Test
    @scenario
    public function convert_from_txt_with_attributes(){
        given().a_txt_from_description({
            name: 'example',
            type: 'type',
            attributes: {
                text: "Hello",
                binary: RandomGen.binary.binary()
            }
        });
        when().converting_to_a_description_instance();
        then().the_converted_description_should_be_equals_to_the_specified();
    }

    @Test
    @scenario
    public function convert_from_txt_binary(){
        given().this_txt(MdnsTxt.fromMap([
            TxtFields.providerId => RandomGen.binary.bytes(8),
            TxtFields.providerIdHashAlg => Bytes.ofString(RandomGen.crypto.hashType()),
            TxtFields.name => Bytes.ofString("name"),
            TxtFields.type => Bytes.ofString("type"),
            TxtFields.addresses + "+0" => Bytes.ofString("1.2.3.4"),
            TxtFields.channels + "+0" => Bytes.ofString(new Channel("tcp", "2.3.4.5", 1234).toString()),
        ]));
        when().converting_to_a_description_instance();
        then().this_description_should_be_produced({
            name: "name",
            type: "type",
            providerId: ProviderId.fromFingerprint({
                hash: txtField(TxtFields.providerId).toBinary(),
                alg: HashTypes.fromString(txtField(TxtFields.providerIdHashAlg).toText())
            }),
            instanceId: null,
            location: {
                addresses: ["1.2.3.4"],
                channels: [new Channel("tcp", "2.3.4.5", 1234)]
            }
        });
    }

    @Test
    @scenario
    public function convert_from_txt_with_location(){
        given().a_txt_from_description(RandomGen.fullDescription());
        when().converting_to_a_description_instance();
        then().the_converted_description_should_be_equals_to_the_specified();
    }

    @Test
    @scenario
    public function converting_txt_to_signed_description(){
        given().a_txt_from_a_signed_description();
        when().converting_to_a_signed_description_instance();
        then().the_converted_signed_description_should_be_equals_to_the_specified();
    }

    @Test
    @scenario
    public function converting_announcement(){
        given().a_txt_from_an_announcement_instance();
        when().converting_back_to_an_announcement();
        then().both_announcement_instances_should_be_equals();
    }


    function txtField(key:String): MdnsTxtValue {
        return steps.mdnsTxt().get(key);
    }
}

class TxtToDescriptionSteps extends Steps<TxtToDescriptionSteps>{
    var txt:MdnsTxt;
    var description:Description;
    var signedDescription:Signed<Description>;
    var sourceDescription:Description;
    var expectedSignature:Signature;

    var sourceAnnouncement:Announcement;
    var convertedAnnouncement:Announcement;

    public function new(){
        super();
    }

    public function mdnsTxt():MdnsTxt{
        return txt;
    }

    // given ---------------------------------------------------

    @step
    public function txt_with_basic_description() {
        given().this_txt(MdnsTxt.fromMap([
            TxtFields.name => RandomGen.name(),
            TxtFields.type => RandomGen.name(),
            TxtFields.providerId => RandomGen.binary.bytes(10),
            TxtFields.providerIdHashAlg => RandomGen.crypto.hashType(),
            TxtFields.instanceId => RandomGen.binary.bytes(8)
        ]));
    }

    @step
    public function a_txt_from_description(desc:Description) {
        sourceDescription = cast ObjectTransformation
                                    .doTransformation(Bytes, Binary.fromBytes)
                                    .apply((desc : Dynamic));

        given().this_txt(MdnsTxtConverter.toTxt(sourceDescription));
    }

    @step
    public function a_txt_from_a_signed_description() {
        sourceDescription = RandomGen.fullDescription();
        var signed = RandomGen.crypto.signed(sourceDescription);
        expectedSignature = signed.signature;

        given().this_txt(MdnsTxtConverter.signedToTxt(signed));
    }

    @step
    public function a_txt_from_an_announcement_instance() {
        sourceAnnouncement = RandomGen.fullAnnouncement();

        var txt = MdnsTxtConverter.builder()
                    .addAnnouncement(sourceAnnouncement).build();

        given().this_txt(txt);
    }

    @step
    public function this_txt(txt:MdnsTxt) {
        this.txt = txt;
    }

    // when  ---------------------------------------------------

    @step
    public function converting_to_a_description_instance() {
        description = MdnsTxtConverter.toDescription(txt);
    }

    @step
    public function converting_to_a_signed_description_instance() {
        signedDescription = MdnsTxtConverter.toSignedDescription(txt);
        description = signedDescription;
    }


    @step
    public function converting_back_to_an_announcement() {
        convertedAnnouncement = MdnsTxtConverter.parser()
                                    .extractAnnouncement(txt);
    }

    // then  ---------------------------------------------------

    @step
    public
    function the_converted_description_should_be_equals_to_the_specified() {
        then().this_description_should_be_produced(sourceDescription);
    }

    @step
    public function the_converted_signed_description_should_be_equals_to_the_specified() {
        then().this_description_should_be_produced(signedDescription.data);
        and().the_signature_should_be_equals_to_the_specified();
    }

    @step
    public function this_description_should_be_produced(expected:Description) {
        assertThat(description, is(notNullValue()),
            "description should not be null");
        assertThat(expected.equals(description), is(true),
            'description: $description\n' +
            'should be: $expected');
    }

    @step
    function the_signature_should_be_equals_to_the_specified() {
        assertThat(signedDescription.signature.toHex(),
            is(equalTo(expectedSignature)), "Signature does not match");
    }

    @step
    public function both_announcement_instances_should_be_equals() {
        validityAreEquals(
            convertedAnnouncement.validity,
            sourceAnnouncement.validity);

        assertThat(convertedAnnouncement.equals(sourceAnnouncement), is(true),
            'Expected: ${sourceAnnouncement}\n' +
            'found: ${convertedAnnouncement}'
        );
    }

	function validityAreEquals(value:Validity, expected:Validity) {
        assertThat(Validity.areEquals(value, expected), is(true),
            'Found: ${value}\n' +
            'Expected: ${expected}'
        );
    }
}