package discovery.test.mdns.tests.mdnsengine;

import discovery.test.helpers.RandomGen;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.mdnsengine.MdnsQueryDispatcher;
import discovery.mdns.DnsRecord.DnsRecordType;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class MdnsQueryDispatcherTest implements Scenario{
    @steps
    var steps: MdnsQueryDispatcherSteps;

    @Test
    @scenario
    public function match_query_prefix(){
        given().a_handler_registered_at('*._keys.local', TXT);
        when().dispatching_a_query_for('example._keys.local', TXT);
        then().the_request_should_be_received();
    }

    @Test
    @scenario
    public function simple_prefix_should_not_match_multi_level(){
        given().a_handler_registered_at('*._keys.local', TXT);
        when().dispatching_a_query_for('1.2.3.example._keys.local', TXT);
        then().no_request_should_be_received();
    }

    @Test
    @scenario
    public function match_query_prefix_multi_level(){
        given().a_handler_registered_at('**._keys.local', TXT);
        when().dispatching_a_query_for('1.2.3.example._keys.local', TXT);
        then().the_request_should_be_received();
    }

    @Test
    @scenario
    public function match_the_most_exact(){
        given().a_handler_registered_at('**._keys.local', TXT);
        and().a_handler_registered_at('**.example._keys.local', TXT);
        when().dispatching_a_query_for('1.2.3.example._keys.local', TXT);
        then().request_should_be_delivered_to('**.example._keys.local');
    }


    @Test
    @scenario
    public function match_multiple_wildcard_segments(){
        given().a_handler_registered_at('*.*._keys.local', TXT);
        when().dispatching_a_query_for('a.b._keys.local', TXT);
        then().the_request_should_be_received();
    }
}

class MdnsQueryDispatcherSteps extends Steps<MdnsQueryDispatcherSteps>{
    var dispatcher:MdnsQueryDispatcher<Any>;

    var expectedValue:Any;
    var receivedValue:Any;

    var matchedHandler:String;

    public function new(){
        super();

        dispatcher = new MdnsQueryDispatcher();
    }

    // given ---------------------------------------------------

    @step
    public function
        a_handler_registered_at(queryDomain:String, recordType:DnsRecordType)
    {
        var query = new DnsQuery(queryDomain, recordType);

        dispatcher.listen(query, (value)->{
            matchedHandler = queryDomain;
            receivedValue = value;
        });
    }

    // when  ---------------------------------------------------

    @step
    public function
        dispatching_a_query_for(queryDomain:String, recordType:DnsRecordType)
    {
        expectedValue = RandomGen.primitives.int();
        dispatcher.notify(new DnsQuery(queryDomain, recordType), expectedValue);
    }

    // then  ---------------------------------------------------

    @step
    public function the_request_should_be_received() {
        assertThat(receivedValue, is(equalTo(expectedValue)),
            'Value dispatched was not received'
        );
    }

    @step
    public function request_should_be_delivered_to(expectedHandler:String) {
        assertThat(matchedHandler, is(equalTo(expectedHandler)));
    }

    @step
    public function no_request_should_be_received() {
        assertThat(matchedHandler, is(nullValue()), 'matchedHandler');
        assertThat(receivedValue, is(nullValue()), 'receivedValue');
    }
}