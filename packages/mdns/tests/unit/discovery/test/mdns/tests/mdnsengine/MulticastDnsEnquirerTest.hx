package discovery.test.mdns.tests.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.mdns.dns.DnsQuery;
import discovery.test.common.ListenerMock;
import discovery.mdns.mdnsengine.MulticastDnsEnquirer;
import discovery.mdns.externs.multicastdns.MulticastDns;
import discovery.test.mdns.mockups.MulticastDnsMockup;
import hxgiven.Steps;
import hxgiven.Scenario;


class MulticastDnsEnquirerTest implements Scenario{
    @steps
    var steps: MulticastDnsEnquirerSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function register_response_listener_on_creation(){
        given().a_multicast_dns_instance();
        when().creating_an_enquirer_from_it();
        then().a_listener_to_queries_responses_should_be_registered();
    }

    @Test
    @scenario
    public function do_query(){
        given().an_mdns_enquirer_instance();
        when().a_query_is_executed();
        then().the_query_should_be_forwarded_to_the_multicast_dns_instance();
    }

    @Test
    @scenario
    public function receive_response(){
        given().a_query_was_executed();
        when().a_compatible_response_is_received();
        then().the_response_should_be_forwarded_to_the_query_listener();
    }
}

class MulticastDnsEnquirerSteps extends Steps<MulticastDnsEnquirerSteps>{
    var enquirer:MulticastDnsEnquirer;

    var multicastDnsMockup:MulticastDnsMockup;
    var multicastDns:MulticastDns;

    var query:DnsQuery;
    var queryListener:ListenerMock<DnsResponse>;
    var queryResponse: DnsResponse;

    public function new(){
        super();

        multicastDnsMockup = new MulticastDnsMockup();
        queryListener = new ListenerMock();
    }

    // given ---------------------------------------------------

    @step
    public function a_query_was_executed() {
        given().an_mdns_enquirer_instance();
        when().a_query_is_executed();
    }

    @step
    public function an_mdns_enquirer_instance() {
        given().a_multicast_dns_instance();
        when().creating_an_enquirer_from_it();
    }

    @step
    public function a_multicast_dns_instance(){
        multicastDns = multicastDnsMockup.multicastDns;
    }

    // when  ---------------------------------------------------

    @step
    public function creating_an_enquirer_from_it() {
        enquirer = new MulticastDnsEnquirer(multicastDns);
    }

    @step
    public function a_query_is_executed() {
        query = MdnsRandomGen.mdns.dnsQuery();
        enquirer.query(query, queryListener.onEvent);
    }

    @step
    public function a_compatible_response_is_received() {
        queryResponse = MdnsRandomGen.mdns.responseForQuery(query);

        multicastDnsMockup.notifyResponse(queryResponse);
    }

    // then  ---------------------------------------------------

    @step
    public function a_listener_to_queries_responses_should_be_registered() {
        multicastDnsMockup.verifyRegisteredResponseListener();
    }

    @step
    public function the_query_should_be_forwarded_to_the_multicast_dns_instance() {
        multicastDnsMockup.verifyQuerySent(query);
    }

    @step
    public function the_response_should_be_forwarded_to_the_query_listener() {
        queryListener.assertWasCalled();
    }
}