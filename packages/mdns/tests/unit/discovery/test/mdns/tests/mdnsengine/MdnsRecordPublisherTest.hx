package discovery.test.mdns.tests.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import massive.munit.Assert;
import discovery.test.mdns.mockups.MulticastDnsMockup;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import discovery.mdns.MdnsEngine;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.DnsRecord;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.test.mdns.mockups.MdnsServerMockup;
import discovery.mdns.mdnsengine.MdnsRecordPublisher;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class MdnsRecordPublisherTest implements Scenario{
    @steps
    var steps: MdnsRecordPublisherSteps;

    @Test
    @scenario
    public function publish_record(){
        given().a_mdns_server();
        and().a_record_publisher_created_from_it();
        when().publishing_some_dns_record();
        then().the_record_should_be_registered_at_the_mdns_server();
    }

    @Test
    @scenario
    public function handle_received_query(){
        given().a_record_publisher_instance();
        when().a_query_handler_is_registered();
        and().a_matching_query_is_received();
        then().the_handler_should_receive_that_query();
    }

    @Test
    @scenario
    public function ignore_non_matching_query(){
        given().a_record_publisher_instance();
        when().a_query_handler_is_registered();
        and().a_non_matching_query_is_received();
        then().the_handler_should_not_receive_that_query();
    }

    @Test
    @scenario
    public function respond_query(){
        given().a_query_handler_was_registered();
        and().a_matching_query_was_received();
        when().the_handler_respond();
        then().the_response_should_be_sent();
    }
}

class MdnsRecordPublisherSteps extends Steps<MdnsRecordPublisherSteps>{
    var recordPublisher:MdnsRecordPublisher;

    var mdnsServerMockup:MdnsServerMockup;
    var multicastDns: MulticastDnsMockup;

    var publishedRecord: DnsRecord;
    var listeningQuery: DnsQuery;
    var receivedQuery: DnsQuery;
    var receivedQueryEvent: QueryEvent;
    var response: DnsResponse;

    public function new(){
        super();

        mdnsServerMockup = new MdnsServerMockup();
        multicastDns = new MulticastDnsMockup();
    }

    // summary ------------------------------------------------


    @step
    public function a_record_publisher_instance() {
        given().a_mdns_server();
        given().a_record_publisher_created_from_it();
    }


    @step
    public function a_query_handler_was_registered() {
        given().a_record_publisher_instance();
        when().a_query_handler_is_registered();
    }

    @step
    public function a_matching_query_was_received() {
        when().a_matching_query_is_received();
    }

    // given ---------------------------------------------------

    @step
    public function a_mdns_server() {}

    @step
    public function a_record_publisher_created_from_it() {
        recordPublisher = new MdnsRecordPublisher(
            mdnsServerMockup.server,
            multicastDns.multicastDns);
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_some_dns_record() {
        publishedRecord = MdnsRandomGen.mdns.dnsRecord();

        recordPublisher.publishRecord(publishedRecord);
    }

    @step
    public function a_query_handler_is_registered() {
        listeningQuery = MdnsRandomGen.mdns.dnsQuery();
        recordPublisher.onQuery(listeningQuery, onQueryHandler);
    }

    function onQueryHandler(evt: QueryEvent) {
        receivedQuery = evt.query();
        receivedQueryEvent = evt;
    }

    @step
    public function a_matching_query_is_received() {
        multicastDns.receiveQuery(listeningQuery.clone());
    }

    @step
    public function a_non_matching_query_is_received() {
        var query = MdnsRandomGen.mdns.dnsQuery();
        //there is some chance that the generated query be equals
        //To-Do: should ensure that this does not happen, instead of failing
        Assert.areNotEqual(listeningQuery, query);

        multicastDns.receiveQuery(query);
    }


    @step
    public function the_handler_respond() {
        response = MdnsRandomGen.mdns.responseForQuery(receivedQuery);

        receivedQueryEvent.respond(response);
    }

    // then  ---------------------------------------------------

    @step
    public function the_record_should_be_registered_at_the_mdns_server() {
        this.mdnsServerMockup.verifyRegisteredRecord(publishedRecord);
    }

    @step
    public function the_handler_should_receive_that_query() {
        assertThat(receivedQuery.equals(listeningQuery), is(true),
            "Received query does not match the expected value");
    }

    @step
    public function the_handler_should_not_receive_that_query() {
        assertThat(receivedQuery, is(nullValue()),
            "should not have received query");
    }

    @step
    public function the_response_should_be_sent() {
        multicastDns.verifyResponseSent(response);
    }


}