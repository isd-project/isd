package discovery.test.mdns.tests.mdnsPki.mdnskeytransport;

import discovery.test.mdns.mockups.pki.MdnsKeyPublisherMockup;
import discovery.mdns.pki.MdnsKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Scenario;


class MdnsKeyTransportSearchTest implements Scenario{
    @steps
    var steps: MdnsKeyTransportSearchSteps;

    @Test
    @scenario
    public function start_search(){
        given().a_key_fingerprint();
        when().start_searching_this_key();
        then().a_query_for_the_key_descriptor_should_be_sent();
    }

    @Test
    @scenario
    public function once_a_key_descriptor_is_found_start_the_key_retrieval(){
        given().a_key_search_was_started();
        when().a_key_descriptor_is_found();
        then().the_key_retrieval_should_start();
    }

    @Test
    @scenario
    public function once_a_key_data_retrieval_succeeds_then_the_found_key_should_be_returned()
    {
        given().a_key_found_listener();
        given().a_key_search_was_started();
        and().a_key_descriptor_was_found();
        when().the_key_retrieval_succeeds();
        then().the_found_key_should_be_received();
    }

    @Test
    @scenario
    public function should_stop_pending_key_data_retrievals(){
        given().a_key_search_was_started();
        given().more_than_one_key_descriptor_was_found();
        when().one_key_retrieval_succeeds();
        and().the_search_is_stopped();
        then().all_pending_key_data_retrievals_should_be_stopped();
    }
}

class MdnsKeyTransportSearchSteps
    extends MdnsKeySearchBaseSteps<MdnsKeyTransportSearchSteps>
{

    var mdnsKeyTransport:KeyTransport;

    var keyPublisherMockup:MdnsKeyPublisherMockup;

    public function new() {
        super();

        keyPublisherMockup = new MdnsKeyPublisherMockup();

        mdnsKeyTransport = new MdnsKeyTransport({
            enquirer: keyEnquirer.enquirer(),
            retrievalFactory: keyRetrievalFactory.factory(),
            keyPublisher: keyPublisherMockup.keyPublisher()
        });
    }

    function startKeySearch(
        keyFingerprint:Fingerprint, listeners:SearchKeyListeners):Stoppable
    {
        return mdnsKeyTransport.searchKey(keyFingerprint, listeners);
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------



    // then  ---------------------------------------------------

}