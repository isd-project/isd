package discovery.test.mdns.tests.mdnsengine.bonjour;

import haxe.Exception;
import discovery.test.mdns.mockups.bonjour.BonjourServiceMockup;
import discovery.mdns.externs.bonjour.engine.BonjourServiceControl;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;



class BonjourServiceControlTest implements Scenario{
    @steps
    var steps: BonjourServiceControlSteps;

    var service:BonjourServiceMockup;
    var control:BonjourServiceControl;

    var publishDone:Bool = false;
    var finished:Bool = false;
    var finishError:Exception = null;

    @Before
    public function reset() {
        publishDone = false;
        finishError = null;
        finished = false;
    }

    @Before
    public function setup() {
        reset();

        service = new BonjourServiceMockup();
        control = new BonjourServiceControl(service.service(), {
            onPublish: ()->{
                publishDone = true;
            },
            onFinish: (?err)->{
                finished = true;
                finishError = err;
            }
        });
    }

    @Test
    @scenario
    public function register_listeners(){
        service.verifyRegisterEventListener('up');
        service.verifyRegisterEventListener('error');
    }

    @Test
    @scenario
    public function notify_publish_done(){
        service.notifyOnUp();

        publishDone.shouldBe(true, 'should have notified publication');
        finishError.shouldBe(null, 'no publish error expected');
    }

    @Test
    @scenario
    public function notify_publish_error(){
        service.notifyError(new Exception('error'));

        verifyFinishedWithError();
    }

    @Test
    @scenario
    public function on_stop_complete__notify_finished_and_callback(){
        var callbackCalled = false;

        control.stop(()->{
            callbackCalled = true;
        });
        service.callbackStop();

        callbackCalled.shouldBe(true, 'should have called stop callback');
        verifyFinished();
    }

    function verifyFinishedWithError() {
        verifyFinished(true);
    }

    function verifyFinished(failed=false) {
        finished.shouldBe(true, 'should have notified publication finished');

        if(!failed){
            finishError.shouldBe(null,
                'no error expected on finish');
        }
        else{
            finishError.shouldNotBeNull(
                'expected error on finish');
        }
    }
}

class BonjourServiceControlSteps extends Steps<BonjourServiceControlSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}