package discovery.test.mdns.tests.mdnsPki.keyenquirer;

import discovery.test.fakes.FakePromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.async.promise.Promisable;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.MdnsPkiMessages;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.search.defaults.DefaultKeyEnquirer;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import discovery.mdns.pki.search.KeyEnquirer;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class GetKeyPartTest implements Scenario{
    @steps
    var steps: GetKeyPartSteps;

    @Test
    @scenario
    public function request_key_part(){
        given().a_key_enquirer_instance();
        given().a_reference_for_some_key_part();
        when().getting_this_key_part();
        then().a_txt_request_should_be_sent_to_the_part_address();
        and().a_key_part_promise_should_be_returned();
    }

    @Test
    @scenario
    public function on_key_part_response(){
        given().a_query_for_a_key_part_was_sent();
        when().a_key_part_response_is_received();
        then().the_key_part_promise_should_resolve_to_the_received_value();
    }
}

class GetKeyPartSteps extends Steps<GetKeyPartSteps>{
    var enquirer:KeyEnquirer;

    var mdnsEngine:MdnsEngineMockup;
    var promises: PromisableFactory;

    var pkiMessages:MdnsPkiMessages;
    var generator:MdnsPkiGenerator;

    var expectedKeyDescriptor:KeyDescriptor;

    var keyPartReference:KeyPartReference;
    var keyPartPromise: Promisable<KeyPart>;
    var expectedKeyPart:KeyPart;
    var receivedKeyPart:KeyPart;

    public function new(){
        super();

        mdnsEngine = new MdnsEngineMockup();
        promises = new FakePromisableFactory();

        pkiMessages = new MdnsPkiMessages();
        generator = new MdnsPkiGenerator();
    }

    // summary -----------------------------------

    @step
    public function a_query_for_a_key_part_was_sent() {
        given().a_key_enquirer_instance();
        given().a_reference_for_some_key_part();
        when().getting_this_key_part();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_enquirer_instance() {
        enquirer = new DefaultKeyEnquirer(mdnsEngine.engine(), promises);
    }


    @step
    public function a_reference_for_some_key_part() {
        keyPartReference = generator.keyPartReference();
    }

    // when  ---------------------------------------------------


    @step
    public function a_key_descriptor_response_is_received() {
        given().a_key_descriptor();

        mdnsEngine.respondLastQueryWith(
            pkiMessages.descriptorTxt(expectedKeyDescriptor));
    }

    function a_key_descriptor() {
        expectedKeyDescriptor = generator.keyDescriptor();
    }


    @step
    public function getting_this_key_part() {
        keyPartPromise = enquirer.getKeyPart(keyPartReference);
    }

    @step
    public function a_key_part_response_is_received() {
        given().a_key_descriptor();

        expectedKeyPart = generator.keyPartsFor(expectedKeyDescriptor)[0];

        mdnsEngine.respondLastQueryWith(expectedKeyPart.data);
    }

    // then  ---------------------------------------------------

    @step
    public function a_txt_request_should_be_sent_to_the_part_address() {
        var query = pkiMessages.keyPartQuery(keyPartReference);
        mdnsEngine.last_query_should_match(query.name, query.type);
    }

    @step
    public function a_key_part_promise_should_be_returned() {
        assertThat(keyPartPromise, is(notNullValue()));
    }

    @step
    public function the_key_part_promise_should_resolve_to_the_received_value() {
        keyPartPromise.then((keyPart)->{
            receivedKeyPart = keyPart;
        });

        assertThat(expectedKeyPart.equals(receivedKeyPart),
            'Expected key part: ${expectedKeyPart}\n' +
            'found: ${receivedKeyPart}');
    }
}