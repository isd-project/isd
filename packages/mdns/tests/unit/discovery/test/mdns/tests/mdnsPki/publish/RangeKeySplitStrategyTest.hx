package discovery.test.mdns.tests.mdnsPki.publish;

import haxe.Exception;
import discovery.format.exceptions.InvalidValueException;
import discovery.format.Binary;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.keys.storage.ExportedFingerprint;
import discovery.mdns.pki.publish.RangeKeySplitStrategy;
import discovery.keys.crypto.KeyFormat;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.storage.KeyData;
import discovery.mdns.pki.publish.KeySplitStrategy;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.EqualsComparator;


class RangeKeySplitStrategyTest implements Scenario{
    @steps
    var steps: RangeKeySplitStrategySteps;

    @Test
    @scenario
    public function single_part_split(){
        var keyId = RandomGen.crypto.fingerprint();

        given().a_key_strategy_with_a_max_length_of(200);
        given().a_key_with_this_fingerprint(
            keyId
        );
        given().the_exported_key_has_this_format(Der);
        and().the_exported_key_has_a_length_of(150);
        when().splitting_this_key();
        then().it_should_produce_these_parts([{
            part: '0-149-Der',
            keyId: keyId
        }]);
    }


    @Test
    @scenario
    public function multipart_part_split(){
        var keyId = RandomGen.crypto.fingerprint();

        given().a_key_strategy_with_a_max_length_of(100);
        given().a_key_with_this_fingerprint(
            keyId
        );
        given().the_exported_key_has_this_format(Der);
        and().the_exported_key_has_a_length_of(250);
        when().splitting_this_key();
        then().it_should_produce_these_parts([
            { part: '0-99-Der', keyId: keyId, },
            { part: '100-199-Der', keyId: keyId, },
            { part: '200-249-Der', keyId: keyId, }
        ]);
    }


    @Test
    @scenario
    public function extract_key_part(){
        given().a_key_splitter_strategy();
        given().an_exported_key_with_at_least_x_bytes(200);
        given().a_key_part_reference_of('0-99-Der');
        when().extracting_that_key_part();
        then().it_should_produce_a_key_part_with_these_range_of_bytes(0, 99);
    }

    @Test
    @scenario
    public function extract_key_part_should_fail_with_invalid_range(){
        given().a_key_splitter_strategy();
        given().an_exported_key_with_at_least_x_bytes(200);
        given().a_key_part_reference_of('1000-1100-Der');
        when().extracting_that_key_part();
        then().it_should_fail_with(InvalidValueException);
    }
}

class RangeKeySplitStrategySteps extends Steps<RangeKeySplitStrategySteps>{
    var keySplitter: KeySplitStrategy;

    var keyData:KeyData;
    var exportedKeyFormat:KeyFormat;

    var keyPartReferences:Array<KeyPartReference>;

    var exportedKey:ExportedKey;
    var keyPartRef:KeyPartReference;
    var keyPart:KeyPart;

    var capturedException:Exception;


    public function new(){
        super();

        keyData = {};
    }

    // given ---------------------------------------------------

    @step
    public function a_key_splitter_strategy() {
        given().a_key_strategy_with_a_max_length_of(100);
    }

    @step
    public function a_key_strategy_with_a_max_length_of(maxLength:Int) {
        keySplitter = new RangeKeySplitStrategy(maxLength);
    }

    @step
    public function a_key_with_this_fingerprint(fingerprint:Fingerprint) {
        keyData.fingerprint = ExportedFingerprint.fromFingerprint(fingerprint);
    }

    @step
    public function the_exported_key_has_this_format(format: KeyFormat) {
        exportedKeyFormat = format;
    }

    @step
    public function the_exported_key_has_a_length_of(length:Int) {
        keyData.publicKey = RandomGen.crypto.exportedKeyWith({
            length: length,
            format: exportedKeyFormat
        });
    }

    @step
    public function an_exported_key_with_at_least_x_bytes(numBytes:Int) {
        exportedKey = RandomGen.crypto.exportedKeyWith({
            length: numBytes
        });
    }

    @step
    public function a_key_part_reference_of(part:String) {
        keyPartRef = KeyPartReference.make(part, RandomGen.crypto.fingerprint());
    }

    // when  ---------------------------------------------------

    @step
    public function splitting_this_key() {
        keyPartReferences = keySplitter.partitionKey(
                                        keyData.publicKey, keyData.fingerprint);
    }

    @step
    public function extracting_that_key_part() {
        try{
            keyPart = keySplitter.extractKeyPart(exportedKey, keyPartRef.part);
        }
        catch(err){
            capturedException = err;
        }
    }

    // then  ---------------------------------------------------

    @step
    public function
        it_should_produce_these_parts(expected:Array<KeyPartReference>)
    {
        assertThat(keyPartReferences.equals(expected), is(true),
            'Generated keys'
        );
    }

    @step
    public function
        it_should_produce_a_key_part_with_these_range_of_bytes(minIdx:Int,
                                                               maxIdx:Int)
    {
        var keyData = exportedKey.data.toBytes();
        var expectedPart:Binary = keyData.sub(minIdx, maxIdx - minIdx + 1);

        keyPart.shouldNotBeNull('KeyPart');
        keyPart.data.shouldBeEqualsTo(expectedPart);
    }

    @step
    public function it_should_fail_with<T>(exceptionType:Class<T>) {
        assertThat(capturedException, isA(exceptionType), 'Captured exception');
    }
}