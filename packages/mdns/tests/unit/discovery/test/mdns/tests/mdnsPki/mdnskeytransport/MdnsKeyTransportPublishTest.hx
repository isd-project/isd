package discovery.test.mdns.tests.mdnsPki.mdnskeytransport;

import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.helpers.RandomGen;
import discovery.keys.storage.PublicKeyData;
import discovery.test.mdns.mockups.pki.MdnsKeyTransportDependenciesMockup;
import discovery.mdns.pki.MdnsKeyTransport;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class MdnsKeyTransportPublishTest implements Scenario{
    @steps
    var steps: MdnsKeyTransportPublishSteps;

    @Test
    @scenario
    public function delegate_publish_key(){
        given().an_mdns_key_transport_from_a_key_publisher();
        given().a_publish_listener();
        when().publishing_some_key();
        then().the_key_publication_should_be_delegated_to_the_key_publisher();
    }

    @Test
    @scenario
    public function delegate_publish_keychain(){
        given().an_mdns_key_transport_from_a_key_publisher();
        given().a_publish_listener();
        when().publishing_a_keychain();
        then().the_publication_should_be_delegated_to_the_key_publisher();
    }
}

class MdnsKeyTransportPublishSteps extends Steps<MdnsKeyTransportPublishSteps>{
    var keyTransport:MdnsKeyTransport;

    var keyTransportDeps:MdnsKeyTransportDependenciesMockup;
    var keychainMockup:KeychainMockup;

    var publishListenerMockup:ListenerMock<Null<Exception>>;
    var publishListener:DoneCallback;

    var publishedKey:PublicKeyData;

    public function new(){
        super();

        keyTransportDeps = new MdnsKeyTransportDependenciesMockup();
        keychainMockup  = new KeychainMockup();
        publishListenerMockup = new ListenerMock();
    }


    function publisherMockup() {
        return keyTransportDeps.keyPublisher();
    }

    // given ---------------------------------------------------

    @step
    public function an_mdns_key_transport_from_a_key_publisher() {
        keyTransport = keyTransportDeps.makeKeyTransport();
    }

    @step
	public function a_publish_listener() {
        publishListener = cast publishListenerMockup.listener();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_some_key() {
        publishedKey = RandomGen.crypto.publicKeyData();

        keyTransport.publishKey(publishedKey, publishListener);
    }

    @step
    public function publishing_a_keychain() {
        keyTransport.publishKeychain(keychainMockup.keychain(), publishListener);
    }

    // then  ---------------------------------------------------

    @step
    public
    function the_key_publication_should_be_delegated_to_the_key_publisher() {
        publisherMockup().verifyPublishedKey(publishedKey, publishListener);
    }

    @step
    public function the_publication_should_be_delegated_to_the_key_publisher() {
        publisherMockup().verifyPublishedKeychain(keychainMockup.keychain(), publishListener);
    }
}

