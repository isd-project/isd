package discovery.test.mdns.tests.mdnsPki.publish;


import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.async.promise.PromisableFactory;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.Keychain;
import discovery.keys.storage.KeyData;
import discovery.keys.storage.PublicKeyData;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.DnsRecord;
import discovery.mdns.MdnsEngine;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.MdnsPkiMessages;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.mdns.pki.publish.KeyDescriptorBuilder;
import discovery.mdns.pki.publish.KeySplitStrategy;
import discovery.mdns.pki.publish.MdnsKeyPublisher;
import discovery.mdns.pki.publish.RangeKeySplitStrategy;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.common.mockups.KeyMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import hxgiven.Scenario;
import hxgiven.Steps;

import org.hamcrest.Matchers.*;
import discovery.test.matchers.CommonMatchers.*;

using discovery.keys.KeyTransformations;
using discovery.utils.EqualsComparator;
using discovery.test.matchers.CommonMatchers;


class KeyPublisherTest implements Scenario{

    @steps
    var steps:KeyPublisherSteps;

    @Test
    @scenario
    public function publish_keychain_and_respond_key_descriptor_query(){
        given().a_keychain_with_some_key_was_published();
        when().a_query_is_received_for_a_key_on_the_keychain();
        then().a_key_descriptor_should_be_sent_to_the_key_on_keychain();
    }

    @Test
    @scenario
    public function get_part_of_published_key(){
        given().a_keychain_with_some_key_was_published();
        given().some_key_part_reference();
        when().a_query_for_that_key_part_is_received();
        then().that_key_part_should_be_sent_as_response();
    }


    @Test
    @scenario
    public function publish_and_retrieve_key(){
        given().a_key_publisher();
        given().some_key_is_published();
        when().a_query_is_received_for_that_key();
        then().a_key_descriptor_should_be_sent_to_the_published_key();
    }

    @Test
    @scenario
    public function publish_and_retrieve_key_not_in_keychain(){
        given().a_keychain_with_some_key_was_published();
        given().another_key_not_in_keychain_is_published();
        when().a_query_is_received_for_that_key();
        then().a_key_descriptor_should_be_sent_to_the_published_key();
    }

    @Test
    @scenario
    public function publish_and_retrieve_key_also_in_keychain(){
        given().a_keychain_with_some_key_was_published();
        given().the_same_key_is_published();
        when().a_query_is_received_for_that_key();
        then().a_key_descriptor_should_be_sent_to_the_published_key();
    }

    @Test
    @scenario
    public function not_respond_queries_for_unknown_keys(){
        given().an_unknown_key_fingerprint();
        when().a_query_is_received_for_that_fingerprint();
        then().no_response_should_be_sent();
    }

    // -----------------------

    @Test
    @scenario
    public function notify_key_published(){
        given().a_key_publisher();
        given().a_publish_listener();
        when().some_key_is_published();
        then().the_listener_should_be_notified();
    }

    @Test
    @scenario
    public function notify_keychain_published(){
        given().a_key_publisher();
        given().a_publish_listener();
        when().some_keychain_is_published();
        then().the_listener_should_be_notified();
    }
}

class KeyPublisherSteps extends Steps<KeyPublisherSteps>
{
    var mdnsEngine:MdnsEngineMockup;
    var keychain:KeychainMockup;
    var keyMockup:KeyMockup;
    var publishListenerMockup:ListenerMock<Null<Exception>>;
    var publishListener:DoneCallback;

    var promises:FakePromisableFactory;
    var splitStrategy:KeySplitStrategy;
    var pkiMessages:MdnsPkiMessages;
    var keyDescriptorBuilder:KeyDescriptorBuilder;

    var descriptorQuery: DnsQuery;
    var keyPartQuery: DnsQuery;

    var keyPartRef: KeyPartReference;

    var keyPublisher:MdnsKeyPublisher;

    var keyFingerprint:Fingerprint;
    var publicKeyData:PublicKeyData;


    public function new(){
        super();

        mdnsEngine = new MdnsEngineMockup();
        keychain = new KeychainMockup();
        keyMockup = new KeyMockup();
        publishListenerMockup = new ListenerMock();

        pkiMessages = new MdnsPkiMessages();

        promises = new FakePromisableFactory();
        splitStrategy = new RangeKeySplitStrategy(
                                pkiMessages.maxKeyPartLength());
        keyDescriptorBuilder = new KeyDescriptorBuilder(splitStrategy);
    }

    // --------------------------------------------------------------

    function buildKeyPublisher(
        engine: MdnsEngine,
        splitStrategy: KeySplitStrategy,
        promises: PromisableFactory)
    {
        keyPublisher = new MdnsKeyPublisher(engine, splitStrategy, promises);
    }

    // properties ---------------------------------------------------

    function keyOnKeychain(): KeyData {
        return keyData();
    }

    function keyData() {
        return keyMockup.keyData(Der);
    }

    function exportedKey() {
        return keyData().publicKey;
    }

    // given ---------------------------------------------------

    @step
    public function a_keychain_with_some_key_was_published() {
        given().a_key_publisher();
        given().some_keychain_is_published();
    }

    @step
    public function some_keychain_is_published() {
        given().a_keychain_with_some_key();
        given().the_keychain_was_published();
    }

    @step
    function a_keychain_with_some_key() {
        keychain.resolveGetKeyTo(
                    keyMockup.key.fingerprint, keyMockup.key);
    }

    @step
    public function a_key_publisher() {
        buildKeyPublisher(mdnsEngine.engine(), splitStrategy, promises);
    }

    @step
    function the_keychain_was_published() {
        publishKeychain(keychain.keychain());
    }

    function publishKeychain(keychain: Keychain) {
        keyPublisher.publishKeychain(keychain, publishListener);
    }

    @step
    public function some_key_part_reference() {
        var keyData = this.keyData();

        var keyParts = splitStrategy.partitionKey(
                        keyData.publicKey, keyData.fingerprint.toFingerprint());
        keyPartRef = keyParts[0];
    }

    @step
    public function another_key_not_in_keychain_is_published() {
        given().some_key_is_published();
    }

    @step
    public function some_key_is_published() {
        given().a_random_public_key();
        when().publishing_this_key();
    }

    @step
    function a_random_public_key() {
        publicKeyData = RandomGen.crypto.publicKeyData();

        keychain.resolveGetKeyTo(publicKeyData.fingerprint.toFingerprint(), null);
    }


    @step
    public function the_same_key_is_published() {
        publicKeyData = keyOnKeychain().toPublicKeyData();

        when().publishing_this_key();
    }

    @step
    function a_key_with_same_fingerprint() {
        publicKeyData = RandomGen.crypto.publicKeyData();
        publicKeyData.fingerprint = keyOnKeychain().fingerprint;
    }

    @step
    public function an_unknown_key_fingerprint() {
        keyFingerprint = RandomGen.crypto.fingerprint();
    }

    @step
    public function a_publish_listener() {
        publishListener = cast publishListenerMockup.listener();
    }

    // when  ---------------------------------------------------

    @step
    public function a_query_is_received_for_a_key_on_the_keychain() {
        var keyFingerprint = keyMockup.keyFingerprint;

        when().a_query_is_received_for_this_fingerprint(keyFingerprint);
    }

    @step
    function a_query_is_received_for_this_fingerprint(fing:Fingerprint) {
        descriptorQuery = makeKeyQueryFor(fing);
        this.mdnsEngine.receiveQuery(descriptorQuery);
    }

    @step
    public function a_query_for_that_key_part_is_received() {
        keyPartQuery = makeKeyPartQuery();
        this.mdnsEngine.receiveQuery(keyPartQuery);
    }

    @step
    function publishing_this_key() {
        keyPublisher.publishKey(publicKeyData, publishListener);
    }

    @step
    public function a_query_is_received_for_that_key() {
        when().a_query_is_received_for_this_fingerprint(publicKeyData.fingerprint);
    }

    @step
    public function a_query_is_received_for_that_fingerprint() {
        when().a_query_is_received_for_this_fingerprint(keyFingerprint);
    }

    // then  ---------------------------------------------------

    @step
    public function a_key_descriptor_should_be_sent_to_the_key_on_keychain() {
        then().a_key_descriptor_should_be_sent_for_this_key(keyOnKeychain());
    }

    @step
    function a_key_descriptor_should_be_sent_for_this_key(keyData:KeyData) {
        var record = shouldHaveResponseRecord();
        var descriptor = pkiMessages.descriptorFromTxt(record.data);

        var expected = makeKeyDescriptorFor(keyData);

        assertThat(descriptor.equals(expected), is(true),
            'Expected key descriptor: ${expected}\n'+
            'but found: ${descriptor}'
        );
    }

    @step
    public function that_key_part_should_be_sent_as_response() {
        var record = shouldHaveResponseRecord();
        var keyPartRef = pkiMessages.parseKeyPart(record.data);

        var expected = makeExpectedKeyPart();

        shouldBeEqualsTo(keyPartRef, expected);
    }

    @step
    public function no_response_should_be_sent() {
        mdnsEngine.lastResponse().shouldBe(null);
    }

    @step
    public function a_key_descriptor_should_be_sent_to_the_published_key() {
        var keyData = KeyTransformations.convertToKeyData(publicKeyData);

        then().a_key_descriptor_should_be_sent_for_this_key(keyData);
    }

    @step
    public function the_listener_should_be_notified() {
        publishListenerMockup.assertWasCalled();
    }

    // -----------------------------------------------------------------------

    function makeKeyQueryFor(fingerprint: Fingerprint): DnsQuery{
        return {
            name: pkiMessages.descriptorUrl(fingerprint),
            type: TXT
        };
    }

    function makeKeyPartQuery():DnsQuery {
        return pkiMessages.keyPartQuery(keyPartRef);
    }

    function makeKeyDescriptorFor(keyData: KeyData): KeyDescriptor{
        return keyDescriptorBuilder.buildDescriptor(keyData);
    }

    function makeExpectedKeyPart(): KeyPart{
        return splitStrategy.extractKeyPart(exportedKey(), keyPartRef.part);
    }

    function shouldHaveResponseRecord(): DnsRecord {
        var response = mdnsEngine.lastResponse();

        assertThat(response, is(notNullValue()), "No response sent");
        assertThat(response.record, is(notNullValue()),
            "Response has no record");

        return response.record;
    }
}