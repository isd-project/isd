package discovery.test.mdns.tests.mdnsPki.publish;

import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.CompositeMockup;
import discovery.keys.storage.PublicKeyData;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.common.mockups.KeychainMockup;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.pki.publish.KeyRepository;
import hxgiven.Steps;
import hxgiven.Scenario;


using discovery.keys.KeyTransformations;
using discovery.test.matchers.CommonMatchers;



class KeyRepositoryTest implements Scenario{
    @steps
    var steps: KeyRepositorySteps;

    @Test
    @scenario
    public function get_key_from_keychain(){
        given().a_key_repository();
        given().a_keychain_with_some_key_was_registered();
        when().getting_that_key();
        then().the_expected_key_should_be_retrieved();
    }


    @Test
    @scenario
    public function get_key_from_multiple_keychains(){
        given().a_key_repository();
        given().multiple_keychains_were_registered();
        when().getting_a_key_from_one_of_the_keychains();
        then().the_expected_key_should_be_retrieved();
    }

    @Test
    @scenario
    public function return_null_when_key_not_found(){
        given().a_key_repository();
        given().multiple_keychains_were_registered();
        when().getting_a_key_not_on_any_keychains();
        then().a_null_key_should_be_retrieved();
    }

    @Test
    @scenario
    public function retrieve_registered_key(){
        given().a_key_repository();
        given().a_key_was_registered();
        when().getting_the_registered_key_by_fingerprint();
        then().the_expected_key_should_be_retrieved();
    }
}

class KeyRepositorySteps extends Steps<KeyRepositorySteps>{

    var keyRepository:KeyRepository;

    var promises:PromisableFactory;
    var keychains:CompositeMockup<KeychainMockup>;

    var registeredKey:PublicKeyData;

    var callbackCalled:Bool=false;
    var expectedKey:PublicKeyData;
    var capturedKey:PublicKeyData;

    public function new(){
        super();

        promises = new FakePromisableFactory();
        keychains = new CompositeMockup(()->new KeychainMockup());
    }

    // given ---------------------------------------------------

    @step
    public function a_key_repository() {
        keyRepository = new KeyRepository(promises);
    }

    @step
    public function a_keychain_with_some_key_was_registered() {
        registerKeychains(1);
    }

    @step
    public function multiple_keychains_were_registered() {
        registerKeychains();
    }

    function registerKeychains(?number: Int) {
        keychains.populate(number);
        keychains.forEach(registerKeychain);
    }

    function registerKeychain(keychain: KeychainMockup) {
        keychain.resolveGetDefaultKey();
        keyRepository.registerKeychain(keychain.keychain());
    }

    @step
    public function a_key_was_registered() {
        registeredKey = RandomGen.crypto.publicKeyData();

        keyRepository.registerKey(registeredKey);
    }

    // when  ---------------------------------------------------

    @step
    public function getting_that_key() {
        when().getting_a_key_from_one_of_the_keychains();
    }

    @step
    public function getting_a_key_from_one_of_the_keychains() {
        var keychain = keychains.pickOne();
        expectedKey = keychain.getKeyMockup().publicKeyData(Der);

        when().getting_this_key(expectedKey.fingerprint);
    }

    @step
    public function getting_a_key_not_on_any_keychains() {
        var fing = RandomGen.crypto.fingerprint();

        keychains.forEach((k)->k.resolveGetKeyTo(fing, null));

        when().getting_this_key(fing);
    }

    @step
    public function getting_the_registered_key_by_fingerprint() {
        expectedKey = registeredKey;

        getting_this_key(registeredKey.fingerprint);
    }

    @step
    function getting_this_key(fingerprint: Fingerprint){
        keyRepository.getPublicKeyData(fingerprint).then((key)->{
            callbackCalled = true;
            capturedKey = key;
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_expected_key_should_be_retrieved() {
        capturedKey.shouldBeEqualsTo(expectedKey);
    }

    @step
    public function a_null_key_should_be_retrieved() {
        callbackCalled.shouldBe(true, 'result was not received');
        capturedKey.shouldBe(null, 'Captured key should be null');
    }
}