package discovery.test.mdns.tests.mdnsPki.publish;

import discovery.mdns.pki.publish.rangesplitter.Range;
import discovery.mdns.pki.publish.rangesplitter.RangePartitionIterator;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.IteratorTools;

class RangePartitionIteratorTest implements Scenario{

    var partitioner:RangePartitionIterator;

    function withPartition(partSize: Int, dataLength: Int){
        partitioner = new RangePartitionIterator(partSize, dataLength);
    }

    @Test
    @scenario
    public function start(){
        withPartition(10, 10);

        partitioner.hasNext().shouldBe(true);
        partitioner.next().shouldBe(Range.make(0, 9));
    }

    @Test
    @scenario
    public function next(){
        withPartition(10, 10);

        partitioner.next();

        partitioner.hasNext().shouldBe(false);
    }

    @Test
    @scenario
    public function multi_partition(){
        var partitioner = new RangePartitionIterator(10, 40);

        partitioner.toArray().shouldBeEqualsTo([
            Range.make(0, 9), Range.make(10, 19),
            Range.make(20, 29), Range.make(30, 39),
        ]);
    }


    @Test
    @scenario
    public function smaller_partition(){
        var partitioner = new RangePartitionIterator(10, 1);

        partitioner.toArray().shouldBeEqualsTo([
            Range.make(0, 0)
        ]);
    }


    @Test
    @scenario
    public function inexact_partition(){
        var partitioner = new RangePartitionIterator(10, 25);

        partitioner.toArray().shouldBeEqualsTo([
            Range.make(0, 9), Range.make(10, 19), Range.make(20, 24)
        ]);
    }
}