package discovery.test.mdns.tests.mdnsengine.bonjour;

import discovery.test.helpers.RandomGen;
import discovery.mdns.externs.bonjour.Bonjour.BonjourDiscoveredService;
import discovery.mdns.DiscoveredService;
import discovery.test.mdns.generators.MdnsGenerator;
import discovery.mdns.DnsSd.DnsSdResolveQuery;
import discovery.mdns.externs.bonjour.engine.BrowserControl;
import discovery.mdns.MdnsQuery;
import discovery.mdns.DnsSd.DnsSdQuery;
import discovery.mdns.externs.bonjour.engine.BonjourServiceControl;
import discovery.mdns.externs.bonjour.engine.BonjourDnsSd;
import discovery.base.search.Stoppable;
import discovery.mdns.externs.bonjour.BonjourPublishArgs;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.test.mdns.mockups.bonjour.BonjourMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class BonjourDnsSdTest implements Scenario{
    @steps
    var steps: BonjourDnsSdSteps;

    @Test
    @scenario
    public function publish_service(){
        when().publishing_this_service({
            name: "example",
            type: "http",
            port: 12345
        });
        then().the_service_should_be_published_by_bonjour();
        and().it_should_return_a_service_control();
    }

    @Test
    @scenario
    public function notify_publication(){
        given().a_publication_has_started();
        when().the_service_is_published();
        then().the_publication_should_be_notified();
    }

    @Test
    @scenario
    public function search_for_service(){
        when().searching_for_services_matching({
            type: 'typeX',
            subtypes: ['mysubtype'],
            protocol: 'udp'
        });
        then().this_search_should_be_executed_on_bonjour({
            type: 'typeX',
            subtypes: ['mysubtype'],
            protocol: 'udp',
            txt: {binary: true} //use binary txt by default
        });
        and().it_should_return_a_browser_control();
    }

    @Test
    @scenario
    public function notify_service_discovery(){
        given().a_search_for_some_service_started();
        when().a_service_is_found();
        then().the_listeners_should_receive_the_discovered_service();
    }

    @Test
    @scenario
    public function resolve_service(){
        when().resolving_a_service_matching({
            name: 'srvname',
            type: 'typeX',
            protocol: 'udp'
        });
        then().this_search_should_be_executed_on_bonjour({
            type: 'typeX',
            protocol: 'udp',
            txt: {binary: true} //use binary txt by default
        });
        and().it_should_return_a_browser_control();
    }


    @Test
    @scenario
    public function resolve_service_should_filter_invalid_services(){
        given().started_to_resolve_service({
            name: 'srvname',
            type: 'typeX',
            protocol: 'udp'
        });
        when().founding_this_service({
            name: 'othername',
            type: 'typeX',
            protocol: 'udp',
            addresses: [],
            port: RandomGen.port()
        });
        then().the_service_should_be_filtered_out();
    }

    @Test
    @scenario
    public function resolve_should_match_only_the_first_service(){
        given().started_resolving_some_service();
        when().a_matching_service_is_located();
        and().another_matching_service_is_located();
        then().only_the_first_service_should_be_returned();
        and().the_query_should_be_stopped();
    }
}

class BonjourDnsSdSteps extends Steps<BonjourDnsSdSteps>{

    var bonjour = new BonjourMockup();

    var bonjourDnsSd:BonjourDnsSd;

    var publishData:MdnsPublishData;
    var serviceControl:Stoppable;
    var browserControl:Stoppable;

    var generator = new MdnsGenerator();

    var published:Bool = false;
    var resolveQuery:DnsSdResolveQuery;
    var foundService:DiscoveredService;
    var countResolved:Int = 0;

    public function new(){
        super();

        bonjourDnsSd = new BonjourDnsSd(bonjour.bonjour());
    }


    // summary ---------------------------------------------------


    @step
    public function a_publication_has_started() {
        when().publishing_this_service(generator.publishData());
    }

    @step
    public function a_search_for_some_service_started() {
        when().searching_for_services_matching(generator.dnsSdSearchQuery());
    }

    @step
    public function started_resolving_some_service() {
        when().resolving_a_service_matching(generator.dnsSdResolveQuery());
    }

    @step
    public function started_to_resolve_service(query:DnsSdResolveQuery) {
        when().resolving_a_service_matching(query);
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function publishing_this_service(desc: MdnsPublishData) {
        this.publishData = desc;

        serviceControl = bonjourDnsSd.publish(desc, {
            onPublish: ()->{
                published = true;
            }
        });
    }

    @step
    public function the_service_is_published() {
        bonjour.notifyLastServicePublished();
    }

    @step
    public function searching_for_services_matching(query:DnsSdQuery) {
        browserControl = bonjourDnsSd.find(query, (found)->{
            foundService = found;
        });
    }

    @step
    public function resolving_a_service_matching(query:DnsSdResolveQuery) {
        resolveQuery = query;
        browserControl = bonjourDnsSd.resolve(query, (found)->{
            foundService = found;

            ++countResolved;
        });
    }

    @step
    public function a_service_is_found() {
        var srv = generator.bonjourDiscoveredService();

        when().founding_this_service(srv);
    }

    @step
    public function another_matching_service_is_located() {
        when().a_matching_service_is_located();
    }

    @step
    public function a_matching_service_is_located() {
        var matchingService= generator.bonjourDiscoveredService();

        matchingService.name = resolveQuery.name;
        matchingService.type = resolveQuery.type;

        if(resolveQuery.protocol != null){
            matchingService.protocol = resolveQuery.protocol;
        }

        when().founding_this_service(matchingService);
    }

    @step
    public function founding_this_service(srv:BonjourDiscoveredService) {
        bonjour.notifyServiceFoundForLastQuery(srv);
    }

    // then  ---------------------------------------------------

    @step
    public function the_service_should_be_published_by_bonjour() {
        bonjour.verifyPublished(
            BonjourPublishArgs.fromMdnsPublishData(publishData));
    }

    @step
    public function the_publication_should_be_notified() {
        published.shouldBe(true, 'The service publication was not notified');
    }

    @step
    public function it_should_return_a_service_control() {
        serviceControl.shouldBeAn(BonjourServiceControl);
    }

    @step
    public function this_search_should_be_executed_on_bonjour(query:MdnsQuery)
    {
        bonjour.verifySearchFor(query);
    }

    @step
    public function it_should_return_a_browser_control() {
        browserControl.shouldBeAn(BrowserControl,
            'Returned control for search operator is invalid');
    }

    @step
    public function the_listeners_should_receive_the_discovered_service() {
        foundService.shouldNotBeNull('No service received');
    }

    @step
    public function the_service_should_be_filtered_out() {
        foundService.shouldBe(null, 'No service was expected');
    }

    @step
    public function only_the_first_service_should_be_returned() {
        countResolved.shouldBe(1, 'Expecting only one resolved service');
    }

    @step
    public function the_query_should_be_stopped() {
        bonjour.lastBrowser().verifyStopped();
    }
}