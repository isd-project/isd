package discovery.test.mdns.tests.mdnsPki.mdnskeytransport;

import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.base.search.Stoppable;
import discovery.test.mdns.mockups.pki.KeyDataRetrieverMockup;
import discovery.keys.crypto.ExportedKey;
import discovery.test.common.mockups.keytransport.SearchKeyListenersMockup;
import discovery.mdns.pki.search.MdnsKeySearch;
import discovery.test.mdns.mockups.pki.KeyRetrievalFactoryMockup;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.KeyDescriptor;
import discovery.test.mdns.mockups.pki.KeyEnquirerMockup;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class MdnsKeySearchTest implements Scenario{
    @steps
    var steps: MdnsKeySearchSteps;

    @Test
    @scenario
    public function start_search(){
        given().a_key_fingerprint();
        when().start_searching_this_key();
        then().a_query_for_the_key_descriptor_should_be_sent();
    }

    @Test
    @scenario
    public function once_a_key_descriptor_is_found_start_the_key_retrieval(){
        given().a_key_search_was_started();
        when().a_key_descriptor_is_found();
        then().the_key_retrieval_should_start();
    }

    @Test
    @scenario
    public function once_a_key_data_retrieval_succeeds_then_the_found_key_should_be_returned()
    {
        given().a_key_found_listener();
        given().a_key_search_was_started();
        and().a_key_descriptor_was_found();
        when().the_key_retrieval_succeeds();
        then().the_found_key_should_be_received();
    }

    @Test
    @scenario
    public function should_stop_pending_key_data_retrievals(){
        given().a_key_search_was_started();
        given().more_than_one_key_descriptor_was_found();
        when().one_key_retrieval_succeeds();
        and().the_search_is_stopped();
        then().all_pending_key_data_retrievals_should_be_stopped();
    }

    @Test
    @scenario
    public function notify_search_end_when_is_stopped(){
        given().a_key_end_listener();
        given().a_key_search_was_started();
        when().the_search_is_stopped();
        then().the_end_listener_should_be_called();
    }
}

class MdnsKeySearchSteps
    extends MdnsKeySearchBaseSteps<MdnsKeySearchSteps>
{

    function startKeySearch(
        keyFingerprint:Fingerprint, listeners:SearchKeyListeners):Stoppable
    {
        var mdnsKeySearch = new MdnsKeySearch({
            enquirer: keyEnquirer.enquirer(),
            retrievalFactory: keyRetrievalFactory.factory()
        });

        mdnsKeySearch.start(keyFingerprint, listeners);

        return mdnsKeySearch;
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------
}