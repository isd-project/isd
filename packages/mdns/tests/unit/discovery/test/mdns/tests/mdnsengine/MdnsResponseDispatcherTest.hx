package discovery.test.mdns.tests.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.externs.multicastdns.MulticastDnsResponses;
import discovery.mdns.mdnsengine.MdnsResponseDispatcher;
import discovery.test.common.ListenerMock;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.mdns.dns.DnsQuery;
import hxgiven.Steps;
import hxgiven.Scenario;


class MdnsResponseDispatcherTest implements Scenario{
    @steps
    var steps: MdnsResponseDispatcherSteps;

    @Test
    @scenario
    public function redirect_response_for_record(){
        given().a_response_dispatcher();
        given().a_listener_registered_for_some_query();
        when().a_response_for_that_query_is_received();
        then().the_listener_should_be_called();
    }
}

class MdnsResponseDispatcherSteps extends Steps<MdnsResponseDispatcherSteps>{
    var responseDispatcher: MdnsResponseDispatcher;

    var dnsQuery:DnsQuery;
    var listener:ListenerMock<DnsResponse>;

    var responsePacket:MulticastDnsResponses;

    public function new(){
        super();

        listener = new ListenerMock();
    }

    // given ---------------------------------------------------

    @step
    public function a_response_dispatcher() {
        responseDispatcher = new MdnsResponseDispatcher();
    }

    @step
    public function a_listener_registered_for_some_query() {
        given().some_query();
        given().a_listener_was_registered_for_that_query();
    }

    @step
    function some_query() {
        dnsQuery = MdnsRandomGen.mdns.dnsQuery();
    }

    @step
    function a_listener_was_registered_for_that_query() {
        responseDispatcher.listen(dnsQuery, listener.onEvent);
    }



    // when  ---------------------------------------------------

    @step
    public function a_response_for_that_query_is_received() {
        responsePacket = MdnsRandomGen.mdns.responseForQuery(dnsQuery);

        responseDispatcher.onResponse(responsePacket);
    }

    // then  ---------------------------------------------------

    @step
    public function the_listener_should_be_called() {
        listener.assertWasCalled();
    }
}