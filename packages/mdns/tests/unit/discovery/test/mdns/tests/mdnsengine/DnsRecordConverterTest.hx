package discovery.test.mdns.tests.mdnsengine;

import discovery.mdns.externs.multicastdns.MulticastDns;
import massive.munit.Assert;
import discovery.mdns.externs.multicastdns.MulticastDnsRecord;
import discovery.mdns.externs.multicastdns.MulticastDnsRecord.MdnsRecordData;
import discovery.mdns.DnsRecord;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import haxe.io.Bytes;
import discovery.format.Binary;
import discovery.mdns.externs.bonjour.BinaryBuffer;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class DnsRecordConverterTest implements Scenario{
    @steps
    var steps: DnsRecordConverterSteps;

    @Test
    public function string_records(){
        testTextConversion(PTR, 'ptr.example.local');
        testTextConversion(A, '1.2.3.4');
        testTextConversion(AAAA, 'dead::c00f:f00d');
    }


    @Test
    public function txt_records(){
        var text = RandomGen.primitives.name();
        var textBytes = Bytes.ofString(text);
        var bytes  = RandomGen.binary.bytes(Random.int(3,10));
        var binary = Binary.fromBytes(bytes);

        testConversion(TXT, binary, [BinaryBuffer.fromBytes(bytes)]);
        testConversion(TXT, bytes,  [BinaryBuffer.fromBytes(bytes)]);
        testConversion(TXT, text,   [BinaryBuffer.fromBytes(textBytes)]);
    }

    function testTextConversion(type: DnsRecordType, data:String) {
        this.testConversion(type, data, data);
    }

    function testConversion(type: DnsRecordType, data:Binary, expected:Dynamic) {
        testEncondingToMulticastDns(type, data, expected);
        testDecodingToDnsRecord(type, expected, data);
    }

    @scenario
    public function testEncondingToMulticastDns(
        type: DnsRecordType, data:Binary, expected:Dynamic)
    {
        steps
        .given('params: type=${type}, data=${data}, expected=${expected}', ()->{});
        given().a_record_of(type, data);
        when().converting_to_a_multicast_dns_record();
        then().the_multicast_dns_record_should_have_this_data(expected);
    }

    @scenario
    public function testDecodingToDnsRecord(
        type:DnsRecordType, encoded:Dynamic, expected:Binary)
    {
        steps
        .given('params: type=${type}, encoded=${encoded}, expected=${expected}', ()->{});
        given().a_multicast_dns_with(type, encoded);
        when().converting_it_to_a_dns_record();
        then().the_dns_record_should_have_this_data(expected);
    }
}

class DnsRecordConverterSteps extends Steps<DnsRecordConverterSteps>{
    var dnsRecord:DnsRecord;
    var multicastDnsRecord: MulticastDnsRecord;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_record_of(type:DnsRecordType, data:Binary) {
        dnsRecord = MdnsRandomGen.mdns.dnsRecordWith(type, data);
    }

    @step
    public function a_multicast_dns_with(type:DnsRecordType, encoded:Dynamic) {
        var record = MdnsRandomGen.mdns.dnsRecord(type);
        record.data = null;

        multicastDnsRecord = MulticastDnsRecord.fromDnsRecord(record);
        multicastDnsRecord.data = encoded;
    }

    // when  ---------------------------------------------------

    @step
    public function converting_to_a_multicast_dns_record() {
        multicastDnsRecord = MulticastDnsRecord.fromDnsRecord(dnsRecord);
    }

    @step
    public function converting_it_to_a_dns_record() {
        dnsRecord = multicastDnsRecord.toDnsRecord();
    }

    // then  ---------------------------------------------------

    @step
    public function
        the_multicast_dns_record_should_have_this_data(expected:Dynamic)
    {
        var data = multicastDnsRecord.data;
        var dataType = Type.typeof(data);
        var expectedType = Type.typeof(expected);

        assertEquals(data, expected);
        assertEquals(dataType, expectedType);
    }


    @step
    public function the_dns_record_should_have_this_data(expected:Binary) {
        assertEquals(dnsRecord.data, expected);
    }

    function assertEquals<T>(value: T, expected: T) {
        Assert.areEqual(value, expected,
            'Expected: ${Std.string(expected)}\n'+
            'found: ${Std.string(value)}'
        );
    }
}