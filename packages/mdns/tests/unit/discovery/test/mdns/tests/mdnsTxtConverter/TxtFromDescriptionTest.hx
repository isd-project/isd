package discovery.test.mdns.tests.mdnsTxtConverter;

import discovery.domain.Announcement;
import discovery.mdns.TxtFields;
import discovery.format.Formatters;
import discovery.test.helpers.RandomGen;
import discovery.format.Binary;
import discovery.keys.crypto.HashTypes;
import massive.munit.Assert;
import discovery.mdns.MdnsTxt;
import discovery.mdns.MdnsTxtConverter;
import discovery.domain.Description;
import haxe.io.Bytes;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class TxtFromDescriptionTest implements Scenario{
    @steps
    var steps: TxtFromDescriptionSteps;

    @Test
    @scenario
    public function convert_from_basic_description(){
        given().this_description({
            name: "my service",
            type: "mytype",
            providerId: new Fingerprint(Sha256, Bytes.ofHex(
                "30d33d45f7c4b3a30b3db06dbf98076bc7ca99be23ee5698062947e09cdd12e1")),
            instanceId: RandomGen.binary.bytes(RandomGen.int(4, 12))
        });
        when().building_an_mdns_txt();
        then().this_txt_should_be_produced(MdnsTxt.fromMap([
            TxtFields.name => desc().name,
            TxtFields.type => desc().type,
            TxtFields.providerId => desc().providerId.id(),
            TxtFields.providerIdHashAlg => desc().providerId.hashAlg(),
            TxtFields.instanceId => desc().instanceId
        ]));
    }

    @Test
    @scenario
    public function convert_from_description_with_null_instance_id(){
        var description = RandomGen.basic_description();
        description.instanceId = null;

        given().this_description(description);
        when().building_an_mdns_txt();
        then().this_txt_should_be_produced(MdnsTxt.fromMap([
            TxtFields.name => desc().name,
            TxtFields.type => desc().type,
            TxtFields.providerId => desc().providerId.id(),
            TxtFields.providerIdHashAlg => desc().providerId.hashAlg()
        ]));
        and().txt_should_not_contain_this_field(TxtFields.instanceId);
    }

    @Test
    @scenario
    public function convert_from_description_with_location(){
        var desc:Description = {
            name: RandomGen.name(),
            type: RandomGen.name(),
            providerId: RandomGen.crypto.fingerprint(),
            location: {
                defaultPort: 12345,
                addresses: [ "192.168.0.10", "dead::beef"],
                channels: [
                    { protocol: "tcp" },
                    { protocol: "udp", port: 54321 },
                    {
                        protocol: "ipfs",
                        address: "ipfs://gdjt2rpxysz2gcz5wbw37gahnpd4vgn6epxfngagffd6bhg5clqqyavhwqa"
                    },
                ]
            }
        };

        given().this_description(desc);
        when().building_an_mdns_txt();
        then().this_txt_should_be_produced(MdnsTxt.fromMap([
            TxtFields.name => desc.name,
            TxtFields.type => desc.type,
            TxtFields.providerId => desc.providerId.id(),
            TxtFields.providerIdHashAlg => desc.providerId.hashAlg(),
            TxtFields.defaultPort => Std.string(desc.location.defaultPort),

            TxtFields.addresses+'+0' => desc.location.addresses[0].toString(),
            TxtFields.addresses+'+1' => desc.location.addresses[1].toString(),

            TxtFields.channels+'+0' => "tcp::",
            TxtFields.channels+'+1' => "udp::54321",
            TxtFields.channels+'+2' =>
                'ipfs:' + Formatters.percentEncoding.encode(desc.location.channels[2].address) + ':',

        ]));
    }

    @Test
    @scenario
    public function convert_from_description_with_properties(){
        given().a_basic_description();
        given().these_custom_attributes({
            text : "example",
            binary: Binary.fromBytes(RandomGen.binary.bytes(10)),
            bytes: RandomGen.binary.bytes(10),
            int: 123,
            float: 12.3,
            object: {
                prop1: "A",
                prop2: "B"
            },
            list: ["a", "b", "c"],
            list_objs: ([
                {v1: 1, v2: 2},
                {v1: 3, v4: 4}
            ]: Array<Dynamic>)
        });
        when().building_an_mdns_txt();
        then().this_txt_should_be_produced(MdnsTxt.fromMap([
            TxtFields.name => desc().name,
            TxtFields.type => desc().type,
            TxtFields.providerId => desc().providerId.id(),
            TxtFields.providerIdHashAlg => desc().providerId.hashAlg(),

            attrKey("text") => "example",
            attrKey("binary") => desc().attributes.binary,
            attrKey("bytes") => desc().attributes.bytes,
            attrKey("int") => Binary.fromInt(123),
            attrKey("float") => Binary.fromFloat(12.3),
            attrKey("object.prop1") =>  "A",
            attrKey("object.prop2") => "B",
            attrKey("list+0") => "a",
            attrKey("list+1") => "b",
            attrKey("list+2") => "c",
            attrKey("list_objs+0.v1") => Binary.fromInt(1),
            attrKey("list_objs+0.v2") => Binary.fromInt(2),
            attrKey("list_objs+1.v1") => Binary.fromInt(3),
            attrKey("list_objs+1.v4") => Binary.fromInt(4)
        ]));
    }

    @Test
    @scenario
    public function convert_from_announcement(){
        given().a_valid_announcement();
        when().converting_the_announcement_to_txt();
        then().the_txt_should_contains_the_announcement_fields();
    }

    inline function desc(): Description{
        return steps.description;
    }

    function attrKey(key:String):String {
        return TxtFields.attributes + '.' + key;
    }
}

class TxtFromDescriptionSteps extends Steps<TxtFromDescriptionSteps>{
    var converter:MdnsTxtConverter;
    var txt:MdnsTxt;

    public var description:Description;
    var announcement:Announcement;

    public function new(){
        super();

        converter = new MdnsTxtConverter();
    }

    // given ---------------------------------------------------

    @step
    public function this_description(srvDescription: Description) {
        description = srvDescription;
    }

    @step
    public function a_basic_description() {
        description = {
            name: RandomGen.name(),
            type: RandomGen.name(),
            providerId: RandomGen.crypto.fingerprint(),
        };
    }

    @step
    public function these_custom_attributes(attributes: Dynamic) {
        description.attributes = attributes;
    }

    @step
    public function a_valid_announcement() {
        announcement = RandomGen.fullAnnouncement();
    }

    // when  ---------------------------------------------------

    @step
    public function building_an_mdns_txt() {
        txt = converter.convertToTxt(description);
    }

    @step
    public function converting_the_announcement_to_txt() {
        txt = converter.announcementToTxt(announcement);
    }

    // then  ---------------------------------------------------

    @step
    public function this_txt_should_be_produced(expected:MdnsTxt) {
        Assert.isTrue(txt.equals(expected),
            'Expected: ${Std.string(expected)}\n' +
            'Found: ${Std.string(txt)}');
    }

    @step
    public function the_txt_should_contains_the_announcement_fields() {
        then().this_txt_should_contains_validity_fields();
    }

    @step
    function this_txt_should_contains_validity_fields() {
        then().txt_should_contain_this_field(TxtFields.validTo);
        then().txt_should_contain_this_field(TxtFields.validFrom);

        var validTo = txt.get(TxtFields.validTo).toBinary().toDouble();
        var validFrom = txt.get(TxtFields.validFrom).toBinary().toDouble();

        var validity = announcement.validity;

        assertThat(validTo, is(equalTo(validity.validTo.getTime())),
            "Validity validTo");

        assertThat(validFrom, is(equalTo(validity.validFrom.getTime())),
            "Validity validFrom");

    }

    @step
    public function txt_should_not_contain_this_field(field:String) {
        field_exists_in_txt_should_be(field, false);
    }

    @step
    function txt_should_contain_this_field(field:String) {
        field_exists_in_txt_should_be(field, true);
    }

    function field_exists_in_txt_should_be(field:String, expected:Bool) {
        var should = if(expected) 'should' else 'should not';

        Assert.areEqual(txt.exists(field), expected,
            'Field "${field}" $should exist on produced txt');
    }
}