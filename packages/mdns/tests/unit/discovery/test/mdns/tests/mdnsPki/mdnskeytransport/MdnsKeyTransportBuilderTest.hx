package discovery.test.mdns.tests.mdnsPki.mdnskeytransport;

import discovery.mdns.MdnsEngine;
import discovery.test.common.mockups.discovery.configuration.GetterMockup;
import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import discovery.mdns.pki.MdnsKeyTransport;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.mdns.pki.MdnsKeyTransportBuilder;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class MdnsKeyTransportBuilderTest implements Scenario{
    @steps
    var steps: MdnsKeyTransportBuilderSteps;

    @Test
    @scenario
    public function build_key_transport(){
        given().a_key_transport_builder();
        given().the_required_dependencies_are_setup();
        when().building_a_key_transport();
        then().should_be_created_an_instance_of(MdnsKeyTransport);
    }

    @Test
    @scenario
    public function build_key_transport_with_isolated_engines(){
        given().a_key_transport_builder_with_required_dependencies();
        given().the_mdns_engine_is_set_to_an_engine_builder();
        when().building_a_key_transport();
        then().a_new_engine_should_be_built();
    }
}

class MdnsKeyTransportBuilderSteps extends Steps<MdnsKeyTransportBuilderSteps>{

    var keyTransportBuilder:MdnsKeyTransportBuilder;
    var keyTransport:KeyTransport;

    var engine:MdnsEngineMockup;
    var promises:PromisableFactory;
    var keyLoader:KeyLoaderMockup;

    var engineBuilder:GetterMockup<MdnsEngine>;

    public function new(){
        super();

        engine = new MdnsEngineMockup();
        promises = new FakePromisableFactory();
        keyLoader = new KeyLoaderMockup();

        engineBuilder = new GetterMockup(engine.engine());
    }

    // given summary -------------------------------------------

    @step
    public function a_key_transport_builder_with_required_dependencies() {
        given().a_key_transport_builder();
        given().the_required_dependencies_are_setup();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_transport_builder() {
        keyTransportBuilder = new MdnsKeyTransportBuilder();
    }

    @step
    public function the_required_dependencies_are_setup() {
        keyTransportBuilder
            .mdnsEngine(engine.engine())
            .promisesFactory(promises)
            .keyLoader(keyLoader.keyLoader());
    }

    @step
    public function the_mdns_engine_is_set_to_an_engine_builder() {
        keyTransportBuilder.mdnsEngine(engineBuilder.getter());
    }

    // when  ---------------------------------------------------

    @step
    public function building_a_key_transport() {
        keyTransport = keyTransportBuilder.buildKeyTransport();
    }

    // then  ---------------------------------------------------

    @step
    public function should_be_created_an_instance_of(type:Class<KeyTransport>) {
        keyTransport.shouldBeAn(type);
    }

    @step
    public function a_new_engine_should_be_built() {
        engineBuilder.assertWasCalled();
    }
}