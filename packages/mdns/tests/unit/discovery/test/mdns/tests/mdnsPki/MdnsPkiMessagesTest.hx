package discovery.test.mdns.tests.mdnsPki;

import discovery.format.Formats;
import discovery.test.mdns.mockups.MdnsEngineMockup;
import discovery.mdns.pki.messages.CompactKeyDescriptor;
import discovery.format.Binary;
import discovery.mdns.dns.DnsQuery;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.format.Formatters;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.MdnsPkiMessages;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;
using discovery.test.matchers.CommonMatchers;



class MdnsPkiMessagesTest implements Scenario{
    var messages:MdnsPkiMessages;

    var pkiGenerator:MdnsPkiGenerator;
    var mdnsEngine:MdnsEngineMockup;

    @Before
    public function setup() {
        messages = new MdnsPkiMessages();

        pkiGenerator = new MdnsPkiGenerator();
        mdnsEngine = new MdnsEngineMockup();
    }

    @Test
    @scenario
    public function fingerprint_to_descriptor_url(){
        var fingHash = RandomGen.binary.bytes(8);
        var fingerprint = new Fingerprint(Sha256, fingHash);

        var url = messages.descriptorUrl(fingerprint);

        var fingBase32 = Formatters.base32.encode(fingHash);

        assertThat(url, equalTo('${fingBase32}._keys.local'));
    }

    @Test
    @scenario
    public function convert_descriptor_to_txt(){
        var descriptor = pkiGenerator.keyDescriptor();

        var descriptorTxt = messages.descriptorTxt(descriptor);
        var parsedDescriptor = messages.descriptorFromTxt(descriptorTxt);

        shouldBeEquals(parsedDescriptor, descriptor);
    }


    @Test
    @scenario
    public function convert_descriptor_to_txt_with_params(){
        var descriptor = pkiGenerator.keyDescriptor();
        descriptor.keyType = EllipticCurve({namedCurve: 'P-256'});

        var descriptorTxt = messages.descriptorTxt(descriptor);
        var parsedDescriptor = messages.descriptorFromTxt(descriptorTxt);

        shouldBeEquals(parsedDescriptor, descriptor);
    }


    @Test
    @scenario
    public function conversion_with_compact_key_descriptor(){
        var descriptor = pkiGenerator.keyDescriptor();

        var compact   = CompactKeyDescriptor.fromDescriptor(descriptor);
        var converted = compact.toDescriptor();

        shouldBeEquals(converted, descriptor);
    }

    @Test
    @scenario
    public function parse_key_query_descriptor(){
        var keyId = 'xphfzb3llqvcldf6bgt2hq3bn6765nps7wnkehhdtmofimy6xzna';
        var descriptor_query = new DnsQuery(
            messages.keysUrl(keyId),
            TXT
        );


        var keyQuery = messages.parseKeyQuery(descriptor_query);

        assertThat(keyQuery, is(notNullValue()), 'Query should not be null');
        assertThat(keyQuery.keyId, is(notNullValue()),
            'KeyId should not be null');

        var expectedKeyId:Binary = Formatters.base32.decode(keyId);
        var keyId:Binary = keyQuery.keyId.toBytes();

        assertThat(keyId, is(equalTo(expectedKeyId)), 'Key id');
    }


    @Test
    @scenario
    public function parse_key_part_query(){
        var someKeyId   = RandomGen.binary.bytes(8);
        var fmtKeyId    = Formats.Base32.encode(someKeyId);
        var someKeyPart = pkiGenerator.keyPartReference(someKeyId).part;
        var keyPartQuery = new DnsQuery(
            messages.keysUrl(someKeyPart, fmtKeyId),
            TXT
        );

        var keyQuery = messages.parseKeyQuery(keyPartQuery);

        keyQuery.shouldNotBeNull();

        var foundKeyId = keyQuery.keyId.toBinary();

        foundKeyId.shouldBeEqualsTo(someKeyId, 'key id');
        keyQuery.part.shouldBeEqualsTo(someKeyPart, 'key part');
    }

    @Test
    @scenario
    public function respond_with_key_part(){
        var keyPart = pkiGenerator.someKeyPart();
        var anyQuery = MdnsRandomGen.mdns.dnsQuery(TXT);
        var queryEvent = mdnsEngine.makeQueryEvent(anyQuery);

        messages.respondWithKeyPart(queryEvent, keyPart);

        var response = mdnsEngine.lastResponse();

        assertThat(response, is(notNullValue()), "Last response");
        shouldBeEquals(response.record.data, keyPart.data);
    }

	function shouldBeEquals<T>(found:T, expected:T) {
        assertThat(found.equals(expected), is(true),
            'Expected: ${Std.string(expected)}\n'+
            'but found: ${Std.string(found)}'
        );
    }
}