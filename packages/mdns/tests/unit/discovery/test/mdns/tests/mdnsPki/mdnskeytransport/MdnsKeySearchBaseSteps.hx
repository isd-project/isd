package discovery.test.mdns.tests.mdnsPki.mdnskeytransport;

import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.test.mdns.mockups.pki.KeyDataRetrieverMockup;
import discovery.keys.crypto.ExportedKey;
import discovery.test.common.mockups.keytransport.SearchKeyListenersMockup;
import discovery.mdns.pki.search.MdnsKeySearch;
import discovery.test.mdns.mockups.pki.KeyRetrievalFactoryMockup;
import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.KeyDescriptor;
import discovery.test.mdns.mockups.pki.KeyEnquirerMockup;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


abstract class MdnsKeySearchBaseSteps<Self:MdnsKeySearchBaseSteps<Self>>
    extends Steps<Self>
{
    var searchStop:Stoppable;

    var searchKeyListenerMockup:SearchKeyListenersMockup;
    var keyEnquirer:KeyEnquirerMockup;
    var keyRetrievalFactory:KeyRetrievalFactoryMockup;

    var mdnsPkiGenerator:MdnsPkiGenerator;

    var keyFingerprint:Fingerprint;

    var keyDescriptor:KeyDescriptor;
    var keyDescriptors: Array<KeyDescriptor>;

    var succeededKeyRetriever: KeyDataRetrieverMockup;
    var retrievedKey: ExportedKey;

    public function new(){
        super();

        keyDescriptors = [];

        mdnsPkiGenerator = new MdnsPkiGenerator();

        searchKeyListenerMockup = new SearchKeyListenersMockup();
        keyEnquirer         = new KeyEnquirerMockup();
        keyRetrievalFactory = new KeyRetrievalFactoryMockup();
    }


    abstract function startKeySearch(
        keyFingerprint:Fingerprint, listeners:SearchKeyListeners):Stoppable;

    // given ---------------------------------------------------


    @step
    public function a_key_search_was_started() {
        given().a_key_fingerprint();
        when().start_searching_this_key();
    }

    @step
    public function more_than_one_key_descriptor_was_found() {
        given().a_key_descriptor_was_found();
        and().a_key_descriptor_was_found();
    }

    @step
    public function a_key_descriptor_was_found() {
        when().a_key_descriptor_is_found();
    }


    @step
    public function a_key_fingerprint() {
        keyFingerprint = RandomGen.crypto.fingerprint();
    }

    @step
    public function a_key_found_listener()
    {
        //already set by default
    }

    @step
    public function a_key_end_listener() {
        //already set by default
    }

    // when  ---------------------------------------------------

    @step
    public function start_searching_this_key() {
        searchStop = startKeySearch(keyFingerprint, searchKeyListenerMockup.listener());
    }


    @step
    public function a_key_descriptor_is_found() {
        given().a_key_descriptor();

        keyEnquirer.notifyKeyDescriptorFound(keyDescriptor);
    }

    @step
    function a_key_descriptor() {
        keyDescriptor = mdnsPkiGenerator.keyDescriptor();

        keyDescriptors.push(keyDescriptor);
    }

    @step
    public function one_key_retrieval_succeeds() {
        when().the_key_retrieval_succeeds();
    }

    @step
    public function the_key_retrieval_succeeds() {
        retrievedKey = RandomGen.crypto.exportedKey();

        succeededKeyRetriever = keyRetrievalFactory.lastKeyDataRetriever();
        succeededKeyRetriever.resolveKeyData(retrievedKey);
    }

    @step
    public function the_search_is_stopped() {
        searchStop.stop();

        keyEnquirer.searchKeyControlMockup().notifyStop();
    }

    // then  ---------------------------------------------------

    @step
    public function a_query_for_the_key_descriptor_should_be_sent() {
        keyEnquirer.assertSearchKeyDescriptorWasCalled(keyFingerprint);
    }

    @step
    public function the_key_retrieval_should_start() {
        keyRetrievalFactory.assertDataRetrieverBuiltWith(keyDescriptor);
        keyRetrievalFactory.lastKeyDataRetriever().assertDataRetrieved();
    }

    @step
    public function the_found_key_should_be_received() {
        var keyFound = searchKeyListenerMockup.lastFoundKey();

        keyFound.shouldNotBeNull('Key found is null');
        keyFound.shouldBeEqualsTo({
            fingerprint: keyDescriptor.fingerprint,
            keyType: keyDescriptor.keyType,
            key: retrievedKey
        });
    }

    @step
    public function all_pending_key_data_retrievals_should_be_stopped() {
        for(keyRetrievalMockup in keyRetrievalFactory.keyDataRetrievals()){
            if(keyRetrievalMockup != succeededKeyRetriever){
                keyRetrievalMockup.assertStoppedWasCalled();
            }
        }
    }

    @step
    public function the_end_listener_should_be_called() {
        searchKeyListenerMockup.endListener().assertWasCalled();
    }
}