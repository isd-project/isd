package discovery.test.mdns.mockups.pki;

import discovery.keys.crypto.ExportedKey;
import discovery.test.fakes.FakePromisable;
import mockatoo.Mockatoo;
import discovery.mdns.pki.search.KeyDataRetriever;

class KeyDataRetrieverMockup {
    var retriever:KeyDataRetriever;

    public var keyDataPromise:FakePromisable<ExportedKey>;

    public function new() {
        keyDataPromise = new FakePromisable();

        retriever = Mockatoo.mock(KeyDataRetriever);
        Mockatoo.when(retriever.getKeyData()).thenReturn(keyDataPromise);
    }

    public function keyDataRetriever(): KeyDataRetriever {
        return retriever;
    }

    public function resolveKeyData(exportedKey:ExportedKey) {
        keyDataPromise.resolve(exportedKey);
    }

    public function assertDataRetrieved() {
        Mockatoo.verify(retriever.getKeyData());
    }

    public function shouldHaveBeenStopped() {
        Mockatoo.verify(retriever.stop());
    }

    public function assertStoppedWasCalled(?mode: VerificationMode) {
        Mockatoo.verify(retriever.stop(), mode);
    }
}