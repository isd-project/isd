package discovery.test.mdns.mockups;

import discovery.mdns.externs.multicastdns.MulticastDnsRecord;
import discovery.mdns.DnsRecord;
import mockatoo.Mockatoo;
import discovery.mdns.externs.bonjour.MdnsServer;

import org.hamcrest.Matchers.*;


class MdnsServerMockup {
    public var server(default, null):MdnsServer;

    var registeredRecords:Array<MulticastDnsRecord> = [];

    public function new() {
        server = Mockatoo.mock(MdnsServer);

        Mockatoo.when(server.register(isNotNull)).thenCall(onServerRegister);
    }

    function onServerRegister(args: Array<Dynamic>) {
        var records:Array<MulticastDnsRecord> = args[0];

        registeredRecords = registeredRecords.concat(records);
    }

	public function verifyRegisteredRecord(record:MulticastDnsRecord) {
        Mockatoo.verify(server.register(isNotNull));

        assertThat(registeredRecords, hasItem(record),
            'Record should have been published: ${Std.string(record)}');
    }
}