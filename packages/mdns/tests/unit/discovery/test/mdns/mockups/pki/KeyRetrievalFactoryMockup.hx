package discovery.test.mdns.mockups.pki;

import mockatoo.Mockatoo;
import discovery.mdns.pki.search.KeyRetrievalFactory;
import discovery.mdns.pki.KeyDescriptor;

class KeyRetrievalFactoryMockup{

    var factoryMock:KeyRetrievalFactory;
    var keyDataRetrieverMockups: Array<KeyDataRetrieverMockup>;

    public function new() {
        keyDataRetrieverMockups = [];

        factoryMock  = Mockatoo.mock(KeyRetrievalFactory);

        Mockatoo.when(factoryMock.dataRetriever(any))
                .thenCall(onBuildDataRetriever);
    }

    function onBuildDataRetriever(args: Array<Dynamic>){
        var mockup = new KeyDataRetrieverMockup();

        keyDataRetrieverMockups.push(mockup);

        return mockup.keyDataRetriever();
    }

    public function factory(): KeyRetrievalFactory{
        return factoryMock;
    }

    public function assertDataRetrieverBuiltWith(keyDescriptor:KeyDescriptor) {
        Mockatoo.verify(factoryMock.dataRetriever(keyDescriptor));
    }

    public function lastKeyDataRetriever() {
        return keyDataRetrieverMockups[keyDataRetrieverMockups.length - 1];
    }

    public function keyDataRetrievals(): Array<KeyDataRetrieverMockup> {
        return keyDataRetrieverMockups;
    }
}