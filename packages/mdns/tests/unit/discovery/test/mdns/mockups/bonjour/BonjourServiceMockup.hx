package discovery.test.mdns.mockups.bonjour;

import discovery.base.search.Stoppable.StopCallback;
import haxe.Exception;
import discovery.impl.events.FutureOnceEvent;
import haxe.Constraints.Function;
import discovery.impl.Defaults;
import discovery.utils.events.EventEmitter;
import mockatoo.Mockatoo;
import discovery.mdns.externs.bonjour.BonjourService;


class BonjourServiceMockup{
    var srv = Mockatoo.mock(BonjourService);

    var upEvent:EventEmitter<Any>;
    var errorEvent:EventEmitter<Exception>;

    var stopCallback:StopCallback;

    public function new() {
        upEvent = new FutureOnceEvent();
        errorEvent = new FutureOnceEvent();

        Mockatoo.when(srv.on('up', any)).thenCall(onUp);
        Mockatoo.when(srv.on('error', any)).thenCall(onError);

        Mockatoo.when(srv.stop(any)).thenCall(onStop);
    }

    function onUp(args:Array<Dynamic>) {
        var callback:Function = args[1];

        if(callback != null){
            upEvent.listen((_)->callback());
        }
    }

    function onError(args:Array<Dynamic>) {
        var callback:Function = args[1];

        if(callback != null){
            errorEvent.listen((err)->callback(err));
        }
    }

    function onStop(args: Array<Dynamic>) {
        stopCallback = args[0];
    }

    public function service():BonjourService {
        return srv;
    }

    public function verifyRegisterEventListener(evt:String) {
        Mockatoo.verify(srv.on(evt, isNotNull));
    }

    public function notifyOnUp() {
        upEvent.notify(true);
    }

    public function notifyError(err:Exception) {
        errorEvent.notify(err);
    }

    public function verifyStoppedCalledWith(callback:() -> Void) {
        Mockatoo.verify(srv.stop(callback));
    }

    public function callbackStop() {
        if(stopCallback != null){
            stopCallback();
        }
    }
}