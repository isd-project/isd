package discovery.test.mdns.mockups.pki;

import discovery.mdns.pki.MdnsKeyTransport;

class MdnsKeyTransportDependenciesMockup {
    var enquirer:KeyEnquirerMockup;
    var retrieverFactory:KeyRetrievalFactoryMockup;
    var publisherMockup:MdnsKeyPublisherMockup;

    public function new() {
        enquirer = new KeyEnquirerMockup();
        retrieverFactory = new KeyRetrievalFactoryMockup();
        publisherMockup = new MdnsKeyPublisherMockup();
    }

    public function makeKeyTransport():MdnsKeyTransport {
        return new MdnsKeyTransport({
            enquirer: enquirer.enquirer(),
            retrievalFactory: retrieverFactory.factory(),
            keyPublisher: publisherMockup.keyPublisher()
        });
    }

    public function keyPublisher() {
        return publisherMockup;
    }
}