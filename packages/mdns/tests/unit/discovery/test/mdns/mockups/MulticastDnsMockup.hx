package discovery.test.mdns.mockups;

import discovery.test.fakes.JsEventEmitterFake;
import discovery.mdns.dns.DnsResponse;
import discovery.mdns.externs.multicastdns.MulticastDnsQueries;
import discovery.mdns.externs.multicastdns.MulticastDnsResponses;
import discovery.mdns.externs.multicastdns.MdnsResponseInfo.MdnsRemoteInfo;
import discovery.test.helpers.RandomGen;
import discovery.mdns.DnsRecord;
import discovery.mdns.dns.DnsQuery;
import mockatoo.Mockatoo;
import discovery.mdns.externs.multicastdns.MulticastDns;

using equals.Equal;


class MulticastDnsMockup {
    public var multicastDns: MulticastDns;

    var eventEmitter:JsEventEmitterFake;

    public function new() {
        eventEmitter = new JsEventEmitterFake();

        multicastDns = Mockatoo.mock(MulticastDns);
        Mockatoo
            .when(multicastDns.on(isNotNull, isNotNull))
            .thenCall(registerEvent);
    }

    function registerEvent(args: Array<Dynamic>) {
        eventEmitter.on(args[0], args[1]);
    }

    public function receiveQuery(query:DnsQuery) {
        var mdnsQuery = MulticastDnsQueries.fromQuery(query);
        eventEmitter.emit('query', mdnsQuery);
    }

    public function notifyResponse(
        queryResponse:DnsResponse, ?remoteInfo: MdnsRemoteInfo)
    {
        if(remoteInfo == null){
            remoteInfo = anyRemoteInfo();
        }

        var responsePacket = MulticastDnsResponses.fromResponse(queryResponse);

        eventEmitter.emit("response", responsePacket, remoteInfo);
    }

    function anyRemoteInfo():Null<MdnsRemoteInfo> {
        return {
            port: RandomGen.port(),
            size: RandomGen.primitives.int(4, 100),
            address: RandomGen.address().toString(),
            family: RandomGen.primitives.choice('IPv4', 'IPv6')
        };
    }

    public function verifyRegisteredResponseListener() {
        Mockatoo.verify(multicastDns.on('response', isNotNull));
    }

    public function verifyQuerySent(query:DnsQuery) {
        var expectedQuery = MulticastDnsQueries.fromQuery(query);

        Mockatoo.verify(multicastDns.query(equalsMatcher(expectedQuery)));
    }

    public function verifyResponseSent(response:DnsResponse) {
        var expectedResponse = MulticastDnsResponses.fromResponse(response);

        Mockatoo.verify(multicastDns.respond(equalsMatcher(expectedResponse)));
    }

    function equalsMatcher<T>(expected: T) {
        function matcher(value: T) {
            return expected.equals(value);
        }

        return customMatcher(matcher);
    }
}