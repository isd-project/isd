package discovery.test.mdns.mockups.bonjour;

import discovery.mdns.DiscoveredService;
import discovery.mdns.externs.bonjour.Browser;
import mockatoo.Mockatoo;

class BonjourBrowserMockup{
    var browserMock = Mockatoo.mock(Browser);

    public function new() {
    }

    public function browser(): Browser {
        return browserMock;
    }

    public function verifyStopped() {
        Mockatoo.verify(browserMock.stop());
    }
}