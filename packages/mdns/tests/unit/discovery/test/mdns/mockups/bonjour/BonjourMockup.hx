package discovery.test.mdns.mockups.bonjour;

import discovery.test.mdns.mockups.bonjour.BonjourBrowserMockup;
import discovery.mdns.DiscoveredService;
import discovery.mdns.externs.bonjour.BonjourQuery;
import discovery.mdns.MdnsQuery;
import discovery.mdns.externs.bonjour.BonjourPublishArgs;
import discovery.mdns.externs.bonjour.Bonjour;
import mockatoo.Mockatoo;

import discovery.test.common.matchers.MockatooMatchers.*;
using discovery.test.matchers.CommonMatchers;

typedef FindQuery = {
    options: BonjourQuery,
    listener: OnFoundCallback,
    browser: BonjourBrowserMockup
};

class BonjourMockup {
    var bonjourMock = Mockatoo.mock(Bonjour);

    // Note: only tracking the last for now
    var lastFind:FindQuery;
    var lastPublished:BonjourServiceMockup;

    public function new() {
        Mockatoo.when(bonjourMock.find(any, any)).thenCall(onFind);
        Mockatoo.when(bonjourMock.publish(any)).thenCall(onPublish);
    }

    function onFind(args: Array<Dynamic>) {
        var browser = new BonjourBrowserMockup();

        lastFind = {
            options: args[0],
            listener: args[1],
            browser: browser
        };

        return browser.browser();
    }

    function onPublish(args: Array<Dynamic>) {
        lastPublished = new BonjourServiceMockup();

        return lastPublished.service();
    }

    public function bonjour(): Bonjour {
        return bonjourMock;
    }


    public function notifyLastServicePublished() {
        lastPublished.shouldNotBeNull('Should have published service before');

        lastPublished.notifyOnUp();
    }

    public function
        notifyServiceFoundForLastQuery(srv:BonjourDiscoveredService)
    {
        requireLastQuery();

        lastFind.listener.shouldNotBeNull('Query has a null listener');

        lastFind.listener(srv);
    }

    public function lastBrowser() {
        var q = requireLastQuery();

        return q.browser;
    }

    function requireLastQuery() {
        lastFind.shouldNotBeNull('No query was executed');

        return lastFind;
    }

    public function verifyPublished(publishArgs:BonjourPublishArgs) {
        Mockatoo.verify(bonjourMock.publish(equalsMatcher(publishArgs)));
    }

    public function verifySearchFor(query:BonjourQuery) {
        Mockatoo.verify(bonjourMock.find(equalsMatcher(query), any));
    }
}