package discovery.test.mdns.mockups.pki;

import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.ExportedKey;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.mdns.pki.publish.KeySplitStrategy;

class FakeKeySplitStrategy implements KeySplitStrategy{
    var partsMaxSize:Int = 200;

    public function new() {

    }

    public function partitionKey(
        key:ExportedKey, keyId:KeyPartKeyId):Array<KeyPartReference>
    {
        IllegalArgumentException.require(keyId != null,
            "Key id should not be null");
        IllegalArgumentException.require(key != null,
            "Key should not be null");


        var keySize = key.data.toBytes().length;
        var countLength = 0;

        var parts:Array<KeyPartReference> = [];

        while (countLength < keySize){
            var partRef:KeyPartReference = {
                part: '${countLength}',
                keyId: keyId
            };

            parts.push(partRef);

            countLength += partsMaxSize;
        }

        return parts;
    }

    public function extractKeyPart(key:ExportedKey, part:PartRef):KeyPart
    {
        throw new haxe.exceptions.NotImplementedException();
    }
}