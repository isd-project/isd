package discovery.test.mdns.mockups;

import discovery.mdns.mdnsengine.QueryHandler;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import discovery.mdns.mdnsengine.MdnsQueryDispatcher;
import discovery.mdns.dns.DnsResponse;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.DnsRecord;
import discovery.base.search.Stoppable;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.format.Binary;
import mockatoo.Mockatoo;
import discovery.mdns.MdnsEngine;

import org.hamcrest.Matchers.*;


private typedef QueryArgs = {
    query: DnsQuery,
    onResponse: OnDnsResponse
};

class MdnsEngineMockup {
    var mdnsEngine:MdnsEngine;

    var publishedQueries: Array<QueryArgs>;
    var sentResponses: Array<DnsResponse>;
    var queryStop: Stoppable;

    var queryDispatcher:MdnsQueryDispatcher<QueryEvent>;

    public function new() {
        publishedQueries = [];
        sentResponses = [];
        queryDispatcher = new MdnsQueryDispatcher();

        queryStop = Mockatoo.mock(Stoppable);
        mdnsEngine = Mockatoo.mock(MdnsEngine);
        Mockatoo.when(mdnsEngine.query(any, any, any)).thenCall(onQuery);
        Mockatoo.when(mdnsEngine.onQuery(any, any)).thenCall(onListenQuery);
    }

    function onQuery(args: Array<Dynamic>) {
        this.publishedQueries.push({
            query: args[0],
            onResponse: args[1]
        });

        return queryStop;
    }

    function onListenQuery(args: Array<Dynamic>) {
        var query:DnsQuery = args[0];
        var listener:QueryHandler = args[1];

        queryDispatcher.listen(query, listener);
    }

    public function engine():MdnsEngine {
        return mdnsEngine;
    }

    public function lastQuery(): DnsQuery{
        var args = lastQueryArgs();

        return if(args == null) null else args.query;
    }
    function lastQueryArgs(): QueryArgs{
        if(publishedQueries.length == 0){
            return null;
        }

        return publishedQueries[publishedQueries.length - 1];
    }

    public function respondLastQueryWith(data:Binary) {
        var queryArgs = lastQueryArgs();

        if(queryArgs != null){
            var response = MdnsRandomGen.mdns.responseForQuery(queryArgs.query);
            response.record.data = data;
            queryArgs.onResponse(response);
        }
    }

    public function last_query_should_match(
        expectedName:String, expectedType:DnsRecordType)
    {
        var query = lastQuery();

        assertThat(query, is(notNullValue()), "no query sent");
        assertThat(query.type, is(equalTo(expectedType)), 'query type');
        assertThat(query.name, equalTo(expectedName), 'query name');
    }

    public function receiveQuery(query:DnsQuery) {
        queryDispatcher.notify(query, makeQueryEvent(query));
    }

    function sendResponse(response:DnsResponse) {
        this.sentResponses.push(response);
    }

    public function lastResponse(): DnsResponse{
        if(sentResponses.length == 0) return null;

        return sentResponses[sentResponses.length - 1];
    }

	public function makeQueryEvent(query:DnsQuery):QueryEvent {
        return new FakeQueryEvent(query, this.sendResponse);
	}
}

class FakeQueryEvent implements QueryEvent{
    var dnsQuery:DnsQuery;
    var responder:(DnsResponse)->Void;

    public function new(query:DnsQuery, responder:(DnsResponse)->Void) {
        this.dnsQuery = query;
        this.responder = responder;
    }

	public function query():DnsQuery {
        return dnsQuery;
	}

	public function respond(response:DnsResponse) {
        responder(response);
    }
}