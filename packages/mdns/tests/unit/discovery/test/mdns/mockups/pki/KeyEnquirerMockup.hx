package discovery.test.mdns.mockups.pki;

import discovery.keys.crypto.KeyPair;
import discovery.test.fakes.FakePromisable;
import discovery.async.promise.Promisable;
import discovery.test.common.mockups.SearchControlMockup;
import discovery.mdns.pki.search.QueryListener;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.search.KeyEnquirer;

import mockatoo.Mockatoo;
import org.hamcrest.Matchers.*;


class KeyEnquirerMockup {
    var keyEnquirer:KeyEnquirer;

    var searchKeyDescriptorListener: QueryListener<KeyDescriptorFound>;
    var searchKeyControl:SearchControlMockup;

    var nextKeyParts:List<FakePromisable<KeyPart>>;

    public function new() {
        searchKeyControl = new SearchControlMockup();
        nextKeyParts = new List();

        keyEnquirer = Mockatoo.mock(KeyEnquirer);

        Mockatoo.when(keyEnquirer.searchKeyDescriptor(isNotNull, isNotNull))
                .thenCall(onSearchKeyDescriptor);
        Mockatoo.when(keyEnquirer.getKeyPart(any))
                .thenCall(onGetKeyPart);
    }

    function onSearchKeyDescriptor(args: Array<Dynamic>) {
        searchKeyDescriptorListener = args[1];

        return searchKeyControl.searchControl;
    }

    function onGetKeyPart(args: Array<Dynamic>): Promisable<KeyPart> {
        if(nextKeyParts.isEmpty()){
            return new FakePromisable(null, "Failed: no key parts found");
        }

        return nextKeyParts.pop();
    }

    public function enquirer():KeyEnquirer {
        return keyEnquirer;
    }

    public function searchKeyControlMockup(): SearchControlMockup {
        return searchKeyControl;
    }

    public function notifyKeyDescriptorFound(keyDescriptor:KeyDescriptor) {
        assertThat(searchKeyDescriptorListener, is(notNullValue()),
            'Failed to notify keyDescriptor discovery: no listener found'
        );

        searchKeyDescriptorListener.onFound({
            descriptor: keyDescriptor
        });
    }

    public
    function assertSearchKeyDescriptorWasCalled(keyFingerprint:Fingerprint){
        Mockatoo.verify(
            keyEnquirer.searchKeyDescriptor(keyFingerprint, isNotNull));
    }

    public function assertSearchKeyDescriptorWasCalledOnlyOnce() {
        verifySearchKeyDescriptorCalled(1);
    }

    public function searchKeyDescriptorShouldNotHaveBeenCalled() {
        verifySearchKeyDescriptorCalled(0);
    }

    function verifySearchKeyDescriptorCalled(nTimes: Int) {
        Mockatoo.verify(keyEnquirer.searchKeyDescriptor(any, any), times(nTimes));
    }


    public function assertSearchKeyStopped() {
        searchKeyControl.assertWasStopped();
    }


    public function whenGettingKeyPartsReturn(keyParts:Array<KeyPart>) {
        for(part in keyParts){
            this.nextKeyParts.add(new FakePromisable(part));
        }
    }

    public function assertGetKeyPartWasCalledWith(keyPart:KeyPartReference) {
        Mockatoo.verify(keyEnquirer.getKeyPart(keyPart));
    }
}