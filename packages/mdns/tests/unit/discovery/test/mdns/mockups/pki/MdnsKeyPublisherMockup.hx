package discovery.test.mdns.mockups.pki;

import discovery.async.DoneCallback;
import discovery.keys.Keychain;
import discovery.mdns.pki.publish.MdnsKeyPublisher;
import mockatoo.Mockatoo;
import discovery.mdns.pki.publish.MdnsKeyPublisher;
import discovery.keys.storage.PublicKeyData;

class MdnsKeyPublisherMockup {
    var publisherMock:MdnsKeyPublisher;

    public function new() {
        publisherMock = Mockatoo.mock(MdnsKeyPublisher);
    }

    public function keyPublisher():MdnsKeyPublisher {
        return publisherMock;
    }

    public function verifyPublishedKey(key:PublicKeyData, ?done:DoneCallback) {
        Mockatoo.verify(publisherMock.publishKey(key, done));
    }

    public
    function verifyPublishedKeychain(keychain:Keychain, ?done:DoneCallback)
    {
        Mockatoo.verify(publisherMock.publishKeychain(keychain, done));
    }
}