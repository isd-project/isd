package discovery.test.mdns.scenarios;

import discovery.test.helpers.RandomGen;
import discovery.format.Binary;
import discovery.test.mdns.scenarios.steps.MdnsSearchSteps;
import discovery.format.Formatters;
import discovery.domain.Identifier;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class LocateServiceTest implements Scenario{
    @steps
    var steps: LocateServiceSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function query_for_service_with_identifier() {
        given().some_service_identifier();
        when().locating_services_from_that_identifier();
        then().a_query_should_be_sent_based_on_the_identifier();
    }

    @Test
    @scenario
    public function notify_service_located() {
        given().a_locating_search_was_started();
        and().a_listener_was_registered_to_the_found_event();
        when().a_service_with_a_matching_identifier_is_found();
        then().the_listener_should_be_notified_with_a_correspondent_service();
    }

    @Test
    @scenario
    public function filter_out_located_services() {
        given().a_locating_search_was_started();
        and().a_listener_was_registered_to_the_found_event();
        when().a_service_that_does_not_fully_match_the_identifier_is_found();
        then().the_listener_should_not_be_notified();
    }
}

class LocateServiceSteps extends MdnsSearchSteps<LocateServiceSteps>
{
    var identifier:Identifier;

    public function new() {
        super();
    }

    // given ---------------------------------------------------------------

    @step
    public function some_service_identifier() {
        identifier = RandomGen.identifier();
    }

    @step
    public function a_locating_search_was_started() {
        given().some_service_identifier();
        when().locating_services_from_that_identifier();
        then().a_query_should_be_sent_based_on_the_identifier();
    }

    // when ---------------------------------------------------------------

    @step
    public function locating_services_from_that_identifier() {
        search = mdnsDiscovery.locate(identifier);
    }

    @step
    public function a_service_with_a_matching_identifier_is_found() {
        var addresses = RandomGen.addresses(RandomGen.int(0, 3));

        when().a_matching_service_is_found({
            name: identifier.name,
            type: identifier.type,
            txt: {
                id: identifier.providerId,
                instance: Binary.fromBytes(identifier.instanceId)
            },
            //address and port does not matter
            port: RandomGen.port(),
            addresses: [for(a in addresses) a.toString()]
        });
    }

    @step
    public function a_service_that_does_not_fully_match_the_identifier_is_found()
    {
        var providerId = identifier.providerId;

        //clone
        var modifiedId = providerId.sub(0, providerId.length);

        //change last byte
        var lastIdx = modifiedId.length-1;
        modifiedId.set(lastIdx, ~modifiedId.get(lastIdx));

        when().a_matching_service_is_found({
            name: identifier.name,
            type: identifier.type,
            txt: {
                id: modifiedId,
                instance: identifier.instanceId
            },
            //address and port does not matter
            port: 12345,
            addresses: []
        });
    }

    // then ---------------------------------------------------------------

    @step
    public function a_query_should_be_sent_based_on_the_identifier() {
        verify(mdns.find(isNotNull, any));

        assertThat(findOptions, is(notNullValue()));
        assertThat(findOptions.type, is(notNullValue()),
            "type was not specified");
        assertThat(findOptions.subtypes, is(notNullValue()),
            "subtypes should not be null");
        assertThat(findOptions.subtypes.length, is(greaterThan(0)),
            "subtypes should not be empty");

        var subtype = findOptions.subtypes[0];
        var providerIdEncoded = Formatters.base32.encode(identifier.providerId);
        assertThat(subtype, equalTo(providerIdEncoded),
            "Subtype should match provider id in base32 encoding");
    }
}