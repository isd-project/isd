package discovery.test.mdns.scenarios.steps;

import discovery.test.helpers.RandomGen;
import discovery.mdns.MdnsTxt;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.domain.PublishInfo;
import discovery.keys.Key;
import haxe.io.Bytes;
import discovery.mdns.TxtFields;
import haxe.Constraints.Function;
import haxe.Exception;
import discovery.test.common.ListenerMock;
import discovery.test.common.CallbackMock;
import discovery.domain.PublishListeners;
import discovery.base.publication.PublicationControl;
import discovery.domain.Description;
import discovery.mdns.externs.bonjour.BonjourService;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class MdnsPublishSteps<SubStep:MdnsPublishSteps<Any>>
    extends MdnsSteps<SubStep>
{
    var mockService:BonjourService;


    var description:Description;
    var providerKey:Key;
    var publishInfo:PublishInfo;

    var publicationControl: PublicationControl;
    var publishListeners:PublishListeners;
    var publishListenerMockup:CallbackMock;
    var finishListenerMockup:ListenerMock<Null<Exception>>;

    var capturedOnUpCallback:Function;
    var capturedOnErrorCallback:Function;
    var capturedStopCallback:Function;
    var capturedError:Exception;

    var publishedInfo:MdnsPublishData;

    public function new() {
        super();

        mockBonjour();
        mockService = mockBonjourService();

        publishListeners = {};

        publishListenerMockup = mock(CallbackMock);
        finishListenerMockup  = mock(ListenerMock);
    }

    function mockBonjour(){
        Mockatoo
            .when(mdns.publish(isNotNull))
            .thenCall((args: Array<Dynamic>)->{
                publishedInfo = args[0];
                return mockService;
            });
    }

    function mockBonjourService() {
        var mockService = mock(BonjourService);

        Mockatoo
            .when(mockService.on(isNotNull, any)).thenCall(registerServiceEvent);
        Mockatoo
            .when(mockService.stop(isNotNull)).thenCall(registerStopCallback);

        return mockService;
    }

    function registerServiceEvent(args: Array<Dynamic>) {
        var eventType:String = args[0];
        var callback:Function = args[1];
        if(eventType == "up"){
            this.capturedOnUpCallback = callback;
        }
        else if(eventType == "error"){
            this.capturedOnErrorCallback = callback;
        }
    }

    function registerStopCallback(args:Array<Dynamic>) {
        capturedStopCallback = args[0];
    }

    // given ---------------------------------------------------------------
    @step
    public function there_is_a_listener_to_the_publish_event() {
        publishListeners.onPublish = publishListenerMockup.onCallback;
    }

    @step
    public function there_was_a_listener_to_the_finish_event() {
        publishListeners.onFinish = (?err: Exception)->{
            capturedError = err;
            finishListenerMockup.onEvent(err);
        };
    }

    // when ---------------------------------------------------------------

    @step
    public function publishing_it() {
        if(publishInfo == null){
            var announcement = RandomGen.announcementFrom(description);
            publishInfo = PublishInfo.make(announcement, providerKey);
        }

        publicationControl = mdnsDiscovery.publish(publishInfo,
                                                   publishListeners);
    }

    @step
    public function the_publication_succeeds() {
        capturedOnUpCallback();
    }


    // then ---------------------------------------------------------------

    // then: published info ----------------------------------------------------

    @step
    public function a_service_txt_should_be_published() {
        assertThat(publishedInfo, is(notNullValue()), "should have published description");

        assertThat(publishedInfo.txt, is(notNullValue()), "published service txt should not be null");
    }

    @step
    public function the_service_txt_should_contain_the_service_name() {
        assertThat(publishedInfo.txt.name, is(equalTo(description.name)),
            "txt should contain the service name"
        );
    }

    @step
    public function the_service_txt_should_contain_the_provider_id() {
        var providerId:Bytes = getTxtBytes(TxtFields.providerId);
        var expectedProviderId:Bytes = publishInfo.getProviderId();

        assertThat(providerId, is(notNullValue()),
            "txt should contain the provider id"
        );
        assertThat(providerId.toHex(), is(equalTo(expectedProviderId.toHex())),
            'published provider id does not match the expected'
        );
    }


    @step
    public function the_service_txt_should_contain_the_instance_id() {
        var instanceId:Bytes = getTxtBytes(TxtFields.instanceId);
        assertThat(instanceId, is(notNullValue()),
            "txt should contain the instance id"
        );
        assertThat(instanceId.toHex(), is(equalTo(description.instanceId.toHex())),
            "the instance id should be published at the service txt"
        );
    }

    // --------------------------------------------------------------

    function getTxtBytes(fieldName:String): MdnsTxtValue {
        var txt:MdnsTxt = publishedInfo.txt;

        if(!txt.exists(fieldName)){
            return null;
        }

        return txt.get(fieldName);
    }
}