package discovery.test.mdns.scenarios;

import discovery.test.helpers.RandomGen;
import discovery.test.mdns.scenarios.steps.MdnsPublishSteps;
import discovery.exceptions.PublicationException;
import discovery.test.common.CallbackMock;
import discovery.format.Formatters;
import haxe.io.Bytes;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class PublishServiceTest implements Scenario{
    @steps
    var steps:PublishServiceSteps;

    @Before
    public function setup(){
        resetSteps();
    }

    // publish -------------------------------------------------------------

    @Test
    @scenario
    public function start_service_publication() {
        given().a_basic_service_description();
        when().publishing_it();
        then().a_valid_PublicationControl_instance_should_be_returned();
        and().the_service_should_have_been_published_in_mdns();
        and().listeners_should_be_registered_to_the_published_service_events();
    }

    @Test
    @scenario
    public function receive_publish_notification() {
        given().there_is_a_listener_to_the_publish_event();
        when().a_service_is_published();
        and().the_publication_succeeds();
        then().the_publication_listener_should_be_notified();
    }

    // publish info -----------------------------------------------------------

    @Test
    @scenario
    public function published_mdns_txt() {
        given().a_basic_service_description();
        when().publishing_it();
        then().a_service_txt_should_be_published();
        and().the_service_txt_should_contain_the_service_name();
    }

    @Test
    @scenario
    public function published_service_ids() {
        given().a_complete_service_description();
        when().publishing_it();
        then().the_service_txt_should_contain_the_provider_id();
        and().the_service_txt_should_contain_the_instance_id();
    }

    @Test
    @scenario
    public function registered_service_subtype_from_provider_id() {
        given().a_complete_service_description();
        when().publishing_it();
        then().the_service_should_be_registered_using_a_subtype_derived_from_the_provider_id();
    }

    // stop -------------------------------------------------------------

    @Test
    @scenario
    public function stop_publication(){
        given().a_service_was_published();
        when().stopping_the_publication();
        then().the_mdns_service_stop_should_be_called();
    }

    @Test
    @scenario
    public function finish_notification(){
        given().there_was_a_listener_to_the_finish_event();
        and().a_service_was_published();
        when().the_publication_is_stopped();
        then().finish_listener_should_be_notified();
        and().the_stop_callback_listener_should_also_be_called();
    }

    // on failure -----------------------------------------------------------

    @Test
    @scenario
    public function notify_failure() {
        given().there_was_a_listener_to_the_finish_event();
        and().a_service_was_published();
        when().an_error_is_notified_by_the_mdns_service();
        then().finish_listener_should_be_notified_with_an_exception();
    }
}

class PublishServiceSteps extends MdnsPublishSteps<PublishServiceSteps> {
    var onStopCallback:CallbackMock;

    public function new() {
        super();

        onStopCallback        = mock(CallbackMock);
    }

    // given ---------------------------------------------------------------

    @step
    public function a_basic_service_description() {
        description = {
            name: "My service",
            type: "http",
            port: 5000
        };
    }

    @step
    public function a_complete_service_description() {
        description = {
            name: "Example name",
            type: "ftp",
            port: 12345,
            providerId: RandomGen.crypto.fingerprint(),
            instanceId: Bytes.ofHex("efdd6c")
        };
    }

    @step
    public function a_service_was_published() {
        when().a_service_is_published();
    }

    // when ---------------------------------------------------------------

    @step
    public function a_service_is_published() {
        given().a_basic_service_description();
        when().publishing_it();
        then().a_valid_PublicationControl_instance_should_be_returned();
    }


    @step
    public function an_error_is_notified_by_the_mdns_service() {
        assertThat(capturedOnErrorCallback, is(notNullValue()));

        #if js
        capturedOnErrorCallback(new js.lib.Error("Something failed"));
        #else
        capturedOnErrorCallback("Any error");
        #end
    }

    @step
    public function stopping_the_publication() {
        publicationControl.stop(onStopCallback.onCallback);
    }

    @step
    public function the_publication_is_stopped() {
        this.when().stopping_the_publication();

        assertThat(capturedStopCallback, is(notNullValue()));

        capturedStopCallback();
    }


    // then ---------------------------------------------------------------

    @step
    public function a_valid_PublicationControl_instance_should_be_returned() {
        assertThat(publicationControl, is(notNullValue()));
    }

    @step
    public function the_service_should_have_been_published_in_mdns() {
        verify(mdns.publish(isNotNull));
    }

    @step
    public function listeners_should_be_registered_to_the_published_service_events() {
        function eventMatcher(evtName:String, expected:String) {
            return evtName == expected;
        }
        var isUp    = eventMatcher.bind(_, "up");
        var isError = eventMatcher.bind(_, "error");

        Mockatoo.verify(mockService.on(customMatcher(isUp), any));
        Mockatoo.verify(mockService.on(customMatcher(isError), any));
    }

    @step
    public function the_publication_listener_should_be_notified() {
        var self = this;
        verify(self.publishListenerMockup.onCallback());
    }

    @step
    public function the_mdns_service_stop_should_be_called() {
        verify(mockService.stop(isNotNull));
    }

    @step
    public function finish_listener_should_be_notified() {
        verify(finishListenerMockup.onEvent(any));
    }

    @step
    public function finish_listener_should_be_notified_with_an_exception() {
        verify(finishListenerMockup.onEvent(isNotNull));

        assertThat(capturedError, is(notNullValue()));
        assertThat(capturedError, isA(PublicationException), "Error should be a PublicationException");
    }

    @step
    public function the_stop_callback_listener_should_also_be_called() {
        var self = this;
        verify(self.onStopCallback.onCallback());
    }

    // then: published info ----------------------------------------------------

    @step
    public function the_service_should_be_registered_using_a_subtype_derived_from_the_provider_id()
    {
        var subtypes = publishedInfo.subtypes;

        assertThat(subtypes, is(notNullValue()), "Should publish subtypes");
        assertThat(subtypes.length, is(greaterThan(0)), "Should have a subtype");

        var subtype = subtypes[0];
        var expectedSubtype = Formatters.base32.encode(description.providerId.id());

        assertThat(subtype.length, is(lessThanOrEqualTo(63)));
        assertThat(subtype, is(equalTo(expectedSubtype.substr(0, 63))));
    }

    // --------------------------------------------------------------

}