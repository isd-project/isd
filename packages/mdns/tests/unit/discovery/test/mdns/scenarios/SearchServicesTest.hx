package discovery.test.mdns.scenarios;

import discovery.test.mdns.scenarios.steps.MdnsSearchSteps;
import hxgiven.Scenario;


class SearchServicesTest implements Scenario{
    @steps
    var steps:SearchServicesSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function start_search() {
        given().a_query_for_services_with_some_type();
        when().starting_a_search_using_that_query();
        then().a_valid_Search_instance_should_be_returned();
        and().the_mdns_search_should_be_started();
    }

    @Test
    @scenario
    public function found_service() {
        given().a_search_was_started();
        and().a_listener_was_registered_to_the_found_event();
        when().a_matching_service_is_found({
            name: "my server name",
            port: 12345,
            type: "http",
            addresses: [
                '192.168.0.123',
                'fe80::dead:beef:f00d:c0fe'
            ]
        });
        then().the_listener_should_be_notified_with_a_correspondent_service();
    }

    @Test
    @scenario
    public function stop_search() {
        given().a_search_was_started();
        and().a_listener_was_registered_to_the_finish_event();
        when().stopping_the_search();
        then().the_mdns_browser_should_be_stopped();
        and().the_stop_callback_should_be_called();
        and().the_finish_event_should_be_triggered();
    }

    @Test
    @scenario
    public function stop_only_once() {
        given().a_search_was_started();
        when().stopping_the_search();
        and().stopping_the_search_again();
        then().the_search_should_be_stopped_only_once();
    }
}

class SearchServicesSteps extends MdnsSearchSteps<SearchServicesSteps> {
}