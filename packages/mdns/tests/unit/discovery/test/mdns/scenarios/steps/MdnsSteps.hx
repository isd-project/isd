package discovery.test.mdns.scenarios.steps;

import discovery.mdns.DnsSdEngine;
import discovery.mdns.MdnsDiscovery;
import hxgiven.Steps;

import mockatoo.Mockatoo.*;


class MdnsSteps<SubStep:MdnsSteps<Any>> extends Steps<SubStep> {
    var mdns:DnsSdEngine;
    var mdnsDiscovery:DiscoveryMechanism;

    public function new() {
        super();

        mdns = mock(DnsSdEngine);
        mdnsDiscovery = new MdnsDiscovery(mdns);
    }
}