package discovery.test.mdns.scenarios.authentication;

import discovery.mdns.MdnsTxtConverter;
import discovery.mdns.DiscoveredService;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.Description;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.scenarios.steps.MdnsSearchSteps;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;



class DiscoverySignedTest implements Scenario{
    @steps
    var steps: DiscoverySignedSteps;

    @Test
    @scenario
    public function discover_signed_description(){
        given().a_search_was_started();
        and().a_listener_was_registered_to_the_found_event();
        when().a_signed_service_is_found();
        then().the_signed_description_should_be_discovered();
    }
}

class DiscoverySignedSteps extends MdnsSearchSteps<DiscoverySignedSteps>{
    var expectedDescription: Signed<Description>;

    public function new(){
        super();
    }

    // given ---------------------------------------------------


    // when  ---------------------------------------------------

    @step
    public function a_signed_service_is_found() {
        var description = RandomGen.fullDescription();
        expectedDescription = RandomGen.crypto.signed(description);

        var srv = descriptionToService(expectedDescription);

        when().a_matching_service_is_found(srv);
    }

    // then  ---------------------------------------------------

    @step
    public function the_signed_description_should_be_discovered() {
        then().the_notified_service_should_correspond_to_the_discovered_service();

        and().the_found_service_should_have_the_expected_signature();
    }

	function the_found_service_should_have_the_expected_signature() {
        var signed = foundService.signed();

        assertThat(signed, is(notNullValue()),
            "Signed description should not be null"
        );
        assertThat(signed.signature, is(notNullValue()),
            "Missing signature"
        );
        assertThat(signed.signature.toHex(),
            is(equalTo(expectedDescription.signature.toHex())),
            "Signature does not match the expected value"
        );
    }

    function descriptionToService(
        signedDescription:Signed<Description>) : DiscoveredService
    {
        var desc = signedDescription.data;
        var txt = MdnsTxtConverter.signedToTxt(signedDescription);

        return {
            name: desc.name,
            type: desc.type,
            port: desc.port,
            addresses: [for(addr in desc.location.addresses) addr.toString()],
            txt: txt
        };
    }
}