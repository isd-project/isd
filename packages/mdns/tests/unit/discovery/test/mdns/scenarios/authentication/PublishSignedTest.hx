package discovery.test.mdns.scenarios.authentication;

import discovery.domain.Announcement;
import discovery.domain.PublishInfo;
import discovery.mdns.MdnsTxtConverter;
import discovery.domain.Description;
import discovery.keys.crypto.signature.Signed;
import discovery.format.Binary;
import discovery.mdns.MdnsTxt;
import discovery.keys.crypto.Fingerprint;
import discovery.test.common.fakes.FakeSigner;
import haxe.io.Bytes;
import discovery.mdns.TxtFields;
import haxe.DynamicAccess;
import mockatoo.Mockatoo;
import discovery.test.helpers.RandomGen;
import discovery.test.mdns.scenarios.steps.MdnsPublishSteps;
import discovery.keys.Key;
import hxgiven.Scenario;


import org.hamcrest.Matchers.*;

class PublishSignedTest implements Scenario{
    @steps
    var steps:PublishSignedSteps;

    @Test
    @scenario
    public function publish_signed_announcement() {
        given().a_signed_announcement();
        when().publishing_it();
        then().a_txt_should_be_published_with_the_description_data();
        and().the_txt_should_contain_the_description_signature();
    }

    @Ignore("not implemented yet")
    @Test
    @scenario
    public function txt_should_include_key_info() {
        given().some_publication_info_with_the_provider_key();
        when().publishing_it();
        then().a_service_txt_should_be_published();
        and().the_txt_should_contains_the_publication_info();
        and().the_txt_should_contains_the_provider_key_info();
    }
}

class PublishSignedSteps extends MdnsPublishSteps<PublishSignedSteps>{

    var signedAnnouncement:Signed<Announcement>;

    var fakeSigner:FakeSigner;

    var keyFingerprint:Fingerprint;

    public function new() {
        super();

        fakeSigner = new FakeSigner();
    }

    // given -------------------------------------------------------------------

    @step
    public function a_signed_announcement() {
        var announcement  = RandomGen.fullAnnouncement();
        signedAnnouncement = RandomGen.crypto.signed(announcement);

        publishInfo = new PublishInfo(signedAnnouncement, providerKey);
    }

    @step
    public function some_provider_key() {
        keyFingerprint = RandomGen.crypto.fingerprint();

        providerKey = Mockatoo.mock(Key);
        Mockatoo.when(providerKey.startSignature(any)).thenReturn(fakeSigner);
        Mockatoo.when(providerKey.fingerprint).thenReturn(keyFingerprint);
    }

    @step
    public function some_description_with_provider_id() {
        description = RandomGen.description();
        description.providerId = keyFingerprint;
    }

    @step
    public function some_publication_info_with_the_provider_key() {
        given().some_provider_key();
        given().some_description_with_provider_id();
    }


    // when -------------------------------------------------------------------


    // then -------------------------------------------------------------------

    @step
    public function a_txt_should_be_published_with_the_description_data() {
        then().a_service_txt_should_be_published();
        and().the_txt_should_contains_the_description_data();
    }

    @step
    function the_txt_should_contains_the_description_data() {
        var description = MdnsTxtConverter.toDescription(publishedInfo.txt);
        var expectedDescription = signedAnnouncement.data.description;

        assertThat(description, is(notNullValue()),
            "Could not parse description from txt");

        assertThat(description.equals(expectedDescription), is(true),
            'Description: ${description.toString()}\n' +
            'does not match the expected: ${Std.string(expectedDescription)}'
        );
    }

    @step
    public function the_txt_should_contains_the_publication_info() {
        then().the_service_txt_should_contain_the_service_name();
        and().the_service_txt_should_contain_the_instance_id();
    }

    @step
    public function the_txt_should_contains_the_provider_key_info() {

    }


    @step
    public function the_txt_should_contain_the_description_signature() {
        then().published_txt_should_contain_non_null_field(TxtFields.signature);

        var signature = publishedInfo.txt.get(TxtFields.signature);

        assertThat(signature, is(notNullValue()));

        var signatureBinary = signature.toBinary();
        assertThat(signatureBinary.toHex(),
            is(equalTo(signedAnnouncement.signature.toHex())),
            "Signature does not match the expected");
    }


    @step
    public
    function published_txt_should_contain_non_null_field(fieldName:String)
    {
        var txt:DynamicAccess<Dynamic> = publishedInfo.txt;

        assertThat(txt.exists(fieldName), is(true),
            'Published txt should contain field "${fieldName}".'
            // 'But found only: ${publishedInfo.txt}'
        );
        assertThat(txt.get(fieldName), is(notNullValue()),
            'Published txt field "${fieldName}" should not be null.'
            // 'But found: ${publishedInfo.txt}'
        );
    }

}