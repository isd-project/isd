package discovery.test.mdns.scenarios.steps;

import discovery.domain.Discovered;
import discovery.mdns.MdnsQuery;
import discovery.mdns.DiscoveredService;
import discovery.test.common.SearchListenerMockups.FinishSearchListener;
import discovery.test.common.CallbackMock;
import discovery.test.common.SearchListenerMockups.SearchListener;
import discovery.domain.Search;
import discovery.domain.Query;
import discovery.mdns.externs.bonjour.Browser;

import haxe.Constraints.Function;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;

class MdnsSearchSteps<T:MdnsSearchSteps<Any>> extends MdnsSteps<T> {
    var bonjourBrowser:Browser;

    var query:Query;
    var search:Search;

    var searchListenerMockup:SearchListener;

    var findOptions:MdnsQuery;
    var onFoundCallback:Function;

    var receivedService:DiscoveredService;
    var foundService:Discovered;

    var stopCallback:CallbackMock;
    var finishListener:FinishSearchListener;

    public function new() {
        super();

        bonjourBrowser = mock(Browser);

        searchListenerMockup = mock(SearchListener);
        stopCallback         = mock(CallbackMock);
        finishListener       = new FinishSearchListener();

        Mockatoo.when(mdns.find(any, any)).thenCall(onBonjourFind);
    }

    function onBonjourFind(args:Array<Dynamic>) {
        findOptions = args[0];
        onFoundCallback = args[1];

        return bonjourBrowser;
    }

    // given ---------------------------------------------------------------

    @step
    public function a_query_for_services_with_some_type() {
        query = {
            type: "http"
        };
    }

    @step
    public function a_search_was_started() {
        this.given().a_query_for_services_with_some_type();
        this.when().starting_a_search_using_that_query();
    }

    @step
    public function a_listener_was_registered_to_the_found_event() {
        search.onFound.listen((srv:Discovered)->{
            this.foundService = srv;
        });
        search.onFound.listen(searchListenerMockup.onEvent);
    }

    @step
    public function a_listener_was_registered_to_the_finish_event(){
        search.onceFinished.listen(finishListener.onEvent);
    }

    // when ---------------------------------------------------------------

    @step
    public function starting_a_search_using_that_query() {
        search = mdnsDiscovery.search(query);
    }

    @step
    public function a_matching_service_is_found(found: DiscoveredService) {
        this.receivedService = found;

        onFoundCallback(found);
    }

    @step
    public function stopping_the_search(){
        search.stop(stopCallback.onCallback);
    }

    @step
    public function stopping_the_search_again(){
        when().stopping_the_search();
    }

    // then ---------------------------------------------------------------

    @step
    public function a_valid_Search_instance_should_be_returned() {
        assertThat(search, is(notNullValue()));
    }

    @step
    public function the_mdns_search_should_be_started() {
        verify(mdns.find(isNotNull, any));

        assertThat(onFoundCallback, is(notNullValue()),
            "mdnsDiscovery should have start listening for results");
    }

    @step
    public
    function the_listener_should_be_notified_with_a_correspondent_service()
    {
        this.then().the_listener_should_be_notified();
        this.and().the_notified_service_should_correspond_to_the_discovered_service();
    }


    @step
    public function the_listener_should_be_notified() {
        verify(searchListenerMockup.onEvent(isNotNull));
    }

    @step
    public function the_listener_should_not_be_notified() {
        verify(searchListenerMockup.onEvent(any), never);
    }

    @step
    public function the_notified_service_should_correspond_to_the_discovered_service(){
        assertThat(foundService, is(notNullValue()), "No service found");

        var srvDescription = foundService.description();

        assertThat(srvDescription.name, is(equalTo(receivedService.name)));
        assertThat(srvDescription.type, is(equalTo(receivedService.type)));
        assertThat(srvDescription.addresses().length,
            is(equalTo(receivedService.addresses.length)),
            "Number of services does not match");

        var receivedAddresses = receivedService.addresses;

        for (i in 0...srvDescription.addresses().length){
            var foundAddress = srvDescription.addresses()[i];
            var idx = receivedAddresses.indexOf(foundAddress.address);

            assertThat(idx>=0, is(true),
                'found address ${foundAddress.address} is not one of received addresses: ${receivedAddresses}');

            assertThat(foundAddress.port, is(equalTo(receivedService.port)));
        }
    }

    @step
    public function the_mdns_browser_should_be_stopped() {
        var browser = bonjourBrowser;
        Mockatoo.verify(browser.stop());
    }

    @step
    public function the_stop_callback_should_be_called(){
        var callback = stopCallback;
        Mockatoo.verify(callback.onCallback());
    }

    @step
    public function the_finish_event_should_be_triggered(){
        finishListener.assertWasCalled();
    }

    @step
    public function the_search_should_be_stopped_only_once(){
        var self = this;
        Mockatoo.verify(self.bonjourBrowser.stop(), 1);
    }
}