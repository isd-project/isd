package discovery.test.mdns.scenarios;

import discovery.test.mdns.scenarios.steps.MdnsSteps;
import discovery.test.common.CallbackMock;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.*;

import org.hamcrest.Matchers.*;


class FinishDiscoveryTest implements Scenario{
    @steps
    var steps:FinishDiscoverySteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function finish_discovery() {
        given().the_mdns_discovery_mechanism_was_created();
        when().closing_it();
        then().the_mdns_instance_should_be_finished();
    }
}

class FinishDiscoverySteps extends MdnsSteps<FinishDiscoverySteps>{
    var onFinish:CallbackMock;

    public function new() {
        super();
        onFinish = mock(CallbackMock);
    }

    // given ---------------------------------------------------------------

    @step
    public function the_mdns_discovery_mechanism_was_created() {
        assertThat(mdnsDiscovery, is(notNullValue()));
    }

    // when ---------------------------------------------------------------

    @step
    public function closing_it() {
        mdnsDiscovery.finish(onFinish.onCallback);
    }

    // then ---------------------------------------------------------------

    @step
    public function the_mdns_instance_should_be_finished() {
        var self = this;
        Mockatoo.verify(self.mdns.finish(self.onFinish.onCallback));
    }
}