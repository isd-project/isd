import massive.munit.TestSuite;

import discovery.test.mdns.tests.mdnsPki.search.KeyDataRetrieverTest;
import discovery.test.mdns.tests.mdnsPki.search.KeyPartCollectorTest;
import discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportSearchTest;
import discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportPublishTest;
import discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportBuilderTest;
import discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeySearchTest;
import discovery.test.mdns.tests.mdnsPki.keyenquirer.SearchDescriptorTest;
import discovery.test.mdns.tests.mdnsPki.keyenquirer.GetKeyPartTest;
import discovery.test.mdns.tests.mdnsPki.publish.KeyPublisherTest;
import discovery.test.mdns.tests.mdnsPki.publish.RangePartitionIteratorTest;
import discovery.test.mdns.tests.mdnsPki.publish.KeyDescriptorBuilderTest;
import discovery.test.mdns.tests.mdnsPki.publish.KeyRepositoryTest;
import discovery.test.mdns.tests.mdnsPki.publish.RangeKeySplitStrategyTest;
import discovery.test.mdns.tests.mdnsPki.MdnsPkiMessagesTest;
import discovery.test.mdns.tests.mdnsTxtConverter.TxtToDescriptionTest;
import discovery.test.mdns.tests.mdnsTxtConverter.TxtFromDescriptionTest;
import discovery.test.mdns.tests.mdnsengine.MdnsResponseDispatcherTest;
import discovery.test.mdns.tests.mdnsengine.MulticastDnsEnquirerTest;
import discovery.test.mdns.tests.mdnsengine.bonjour.BonjourDnsSdTest;
import discovery.test.mdns.tests.mdnsengine.bonjour.BonjourServiceControlTest;
import discovery.test.mdns.tests.mdnsengine.MdnsQueryDispatcherTest;
import discovery.test.mdns.tests.mdnsengine.DnsRecordConverterTest;
import discovery.test.mdns.tests.mdnsengine.MdnsRecordPublisherTest;
import discovery.test.mdns.scenarios.LocateServiceTest;
import discovery.test.mdns.scenarios.FinishDiscoveryTest;
import discovery.test.mdns.scenarios.PublishServiceTest;
import discovery.test.mdns.scenarios.SearchServicesTest;
import discovery.test.mdns.scenarios.authentication.PublishSignedTest;
import discovery.test.mdns.scenarios.authentication.DiscoverySignedTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.mdns.tests.mdnsPki.search.KeyDataRetrieverTest);
		add(discovery.test.mdns.tests.mdnsPki.search.KeyPartCollectorTest);
		add(discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportSearchTest);
		add(discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportPublishTest);
		add(discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeyTransportBuilderTest);
		add(discovery.test.mdns.tests.mdnsPki.mdnskeytransport.MdnsKeySearchTest);
		add(discovery.test.mdns.tests.mdnsPki.keyenquirer.SearchDescriptorTest);
		add(discovery.test.mdns.tests.mdnsPki.keyenquirer.GetKeyPartTest);
		add(discovery.test.mdns.tests.mdnsPki.publish.KeyPublisherTest);
		add(discovery.test.mdns.tests.mdnsPki.publish.RangePartitionIteratorTest);
		add(discovery.test.mdns.tests.mdnsPki.publish.KeyDescriptorBuilderTest);
		add(discovery.test.mdns.tests.mdnsPki.publish.KeyRepositoryTest);
		add(discovery.test.mdns.tests.mdnsPki.publish.RangeKeySplitStrategyTest);
		add(discovery.test.mdns.tests.mdnsPki.MdnsPkiMessagesTest);
		add(discovery.test.mdns.tests.mdnsTxtConverter.TxtToDescriptionTest);
		add(discovery.test.mdns.tests.mdnsTxtConverter.TxtFromDescriptionTest);
		add(discovery.test.mdns.tests.mdnsengine.MdnsResponseDispatcherTest);
		add(discovery.test.mdns.tests.mdnsengine.MulticastDnsEnquirerTest);
		add(discovery.test.mdns.tests.mdnsengine.bonjour.BonjourDnsSdTest);
		add(discovery.test.mdns.tests.mdnsengine.bonjour.BonjourServiceControlTest);
		add(discovery.test.mdns.tests.mdnsengine.MdnsQueryDispatcherTest);
		add(discovery.test.mdns.tests.mdnsengine.DnsRecordConverterTest);
		add(discovery.test.mdns.tests.mdnsengine.MdnsRecordPublisherTest);
		add(discovery.test.mdns.scenarios.LocateServiceTest);
		add(discovery.test.mdns.scenarios.FinishDiscoveryTest);
		add(discovery.test.mdns.scenarios.PublishServiceTest);
		add(discovery.test.mdns.scenarios.SearchServicesTest);
		add(discovery.test.mdns.scenarios.authentication.PublishSignedTest);
		add(discovery.test.mdns.scenarios.authentication.DiscoverySignedTest);
	}
}
