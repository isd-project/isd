import massive.munit.TestSuite;

import discovery.test.tests.mdns.discovery.PublishAndSearchTest;
import discovery.test.tests.mdns.configuration.NBonjourMechanismsBuilderTest;
import discovery.test.tests.mdns.pki.MdnsEngineTest;
import discovery.test.tests.mdns.pki.MdnsKeyTransportTest;
import discovery.test.tests.mdns.pki.MdnsPkiPublishIntegrationTest;
import discovery.test.tests.mdns.pki.MdnsPkiTest;
import discovery.test.tests.mdns.pki.FakeKeyStorageTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.tests.mdns.discovery.PublishAndSearchTest);
		add(discovery.test.tests.mdns.configuration.NBonjourMechanismsBuilderTest);
		add(discovery.test.tests.mdns.pki.MdnsEngineTest);
		add(discovery.test.tests.mdns.pki.MdnsKeyTransportTest);
		add(discovery.test.tests.mdns.pki.MdnsPkiPublishIntegrationTest);
		add(discovery.test.tests.mdns.pki.MdnsPkiTest);
		add(discovery.test.tests.mdns.pki.FakeKeyStorageTest);
	}
}
