package discovery.test.tests.mdns.pki;

import discovery.test.fakes.FakeKeyStorage;
import discovery.keys.storage.KeyStorage;
import discovery.test.helpers.RandomGen;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class FakeKeyStorageTest implements Scenario{
    var keystorage:KeyStorage;

    @Before
    public function setup() {
        keystorage = new FakeKeyStorage();
    }

    @Test
    @scenario
    public function add_get(){
        var key = RandomGen.crypto.keyData();

        keystorage.addKey(key);

        var gotKey = keystorage.getKey(key.fingerprint.hash);

        assertThat(gotKey, is(notNullValue()));
        assertThat(gotKey.equals(key), is(true));
    }
}