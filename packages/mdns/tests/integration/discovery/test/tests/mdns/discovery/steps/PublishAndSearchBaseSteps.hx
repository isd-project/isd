package discovery.test.tests.mdns.discovery.steps;


import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.format.Binary;
import discovery.mdns.externs.bonjour.BonjourDnsSdEngine;
import discovery.domain.PublishInfo;
import discovery.base.publication.PublicationControl;
import discovery.test.integration.tools.NextCallback;
import discovery.domain.Identifier;
import haxe.io.Bytes;
import haxe.Exception;
import discovery.domain.Search;
import discovery.domain.Description;
import discovery.mdns.MdnsDiscovery;

import hxgiven.Steps;

import org.hamcrest.Matchers.*;


class PublishAndSearchBaseSteps<Sub:PublishAndSearchBaseSteps<Any>>
    extends Steps<Sub>
{
    var publisher:MdnsDiscovery;
    var consumer:MdnsDiscovery;

    var description:Description;
    var identifier:Identifier;
    var publicationControl:PublicationControl;
    var publishInfo:PublishInfo;

    var search:Search;
    var foundService:Discovered;

    public function new() {
        super();
    }

    public function teardown() {
        if(publicationControl != null){
            publicationControl.stop();
        }
        if(publisher != null){
            publisher.finish(()->{});
        }
        if(search != null){
            search.stop();
        }
        if(consumer != null){
            consumer.finish(()->{});
        }
    }

    // given ---------------------------------------------------------------

    @step
    public function a_publisher_node() {
        publisher = build_node();
    }

    @step
    public function a_consumer_node() {
        consumer = build_node();
    }

    public function build_node() {
        return new MdnsDiscovery(new BonjourDnsSdEngine());
    }

    @step
    public function a_service_description(?description:Description) {
        if(description == null){
            description = {
                name: "my service name",
                type: "http",
                port: 12345,
                providerId: Bytes.ofHex("bbf4665e9cf747dac3780461bae9e357553517ce27479c498d275159092bfd49"),
                instanceId: Bytes.ofHex("a97f98")
            };
        }

        this.description = description;
    }

    @step
    public function a_identifier_from_the_published_description() {
        assertThat(description, is(notNullValue()), "should have published a description");

        identifier = publishInfo.getIdentifier();
    }

    // when ---------------------------------------------------------------

    @step
    public function publishing_a_service(next:NextCallback) {
        given().a_service_description();
        when().starting_publication(next);
    }

    @step
    public function starting_publication(next:NextCallback) {
        if(publishInfo == null){
            var announcement = RandomGen.announcementFrom(description);
            publishInfo = PublishInfo.make(announcement);
        }

        var started = false;
        publicationControl = publisher.publish(publishInfo, {
            onPublish: ()->{
                started = true;
                next();
            },
            onFinish: (?err:Exception)->{
                if(err != null && !started){
                    next(err);
                }
            }
        });
    }

    @step
    public function searching_for_that_service_type(next:NextCallback) {
        search = consumer.search({
            type: description.type
        });

        listenForDiscovery(next);
    }

    @step
    public function locating_that_service_from_its_identifier(next:NextCallback)
    {
        given().a_identifier_from_the_published_description();

        search = consumer.locate(identifier);

        listenForDiscovery(next);
    }

    function listenForDiscovery(next: NextCallback) {
        search.onFound.listen((found:Discovered)->{
            foundService = found;

            if(found.description().name == description.name){
                search.stop();
                next();
            }
        });

        search.onceFinished.listen((evt)->{
            if(foundService == null){
                next(new haxe.Exception("Finished before find service"));
            }
        });
    }

    // then ---------------------------------------------------------------

    @step
    public function the_service_should_be_discovered() {
        assertThat(foundService, is(notNullValue()), "should have found service");

        var srvDescription = foundService.description();

        assertThat(srvDescription.name, is(equalTo(description.name)),
            "Service name should match description"
        );
        assertThat(srvDescription.type, is(equalTo(description.type)),
            "Service type should match description"
        );
        if(publishInfo.getProviderId() != null){
            assertThat(foundService.providerId, is(notNullValue()),
                "Found service should have published providerId"
            );
            assertThat(srvDescription.providerId.id().toHex(),
                is(equalTo(publishInfo.getProviderId().toHex())),
                "Provider id should match description"
            );
        }
        if(description.instanceId != null){
            var instanceEquals = Binary.areEquals(
                description.instanceId, srvDescription.instanceId);

            assertThat(instanceEquals, is(true),
                'Found instance id: ${srvDescription.instanceId}\n' +
                'should match the published value: ${description.instanceId}'
            );
        }
    }
}