package discovery.test.tests.mdns.pki;

import discovery.mdns.pki.MdnsKeyTransportBuilder;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.keys.pki.BasePkiBuilder;
import discovery.mdns.pki.search.KeyEnquirer;
import discovery.mdns.pki.search.defaults.DefaultKeyEnquirer;
import discovery.test.fakes.FakeKeyStorage;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.keys.storage.KeyStorage;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.BaseKeychain;
import discovery.keys.Keychain;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.externs.bonjour.BonjourMdnsEngine;
import discovery.mdns.MdnsEngine;
import discovery.keys.PKI;

class MdnsPkiTestBuilder {

    var keyCache: Keychain;

    var keyloader:KeyGenerator;
    var promises:PromisableFactory;

    var resources: List<MdnsEngine>;

    var pkiBuilder:BasePkiBuilder;

    public function new() {
        #if js
        keyloader = new JscuKeyGenerator();
        promises = new JsPromisableFactory();
        #end

        keyCache = buildKeychain();

        resources = new List();

        pkiBuilder = new BasePkiBuilder()
            .keyLoader(keyloader)
            .promises(promises)
            .keyCache(keyCache);
    }

    public function close() {
        while(!resources.isEmpty()){
            var resource = resources.pop();

            if(resource != null){
                resource.finish();
            }
        }
    }

    public function buildKeychain(?keystorage: KeyStorage) {
        if(keystorage == null){
            keystorage = new FakeKeyStorage();
        }

        return new BaseKeychain(
            keyloader,
            keystorage,
            promises
        );
    }

    public function buildPki(?keyCache: Keychain): PKI {
        if(keyCache != null){
            this.pkiBuilder.keyCache(keyCache);
        }

        return pkiBuilder
                .transportBuilder(makeTransportBuilder())
                .buildPki();
    }

	function makeTransportBuilder():KeyTransportBuilder {
        return new MdnsKeyTransportBuilder({
            promises: promises,
            engine: buildMdns(),
            keyLoader: keyloader
        });
	}

    public function buildKeyEnquirer():KeyEnquirer {
        return new DefaultKeyEnquirer(buildMdns(), promises);
    }

    function buildMdns():MdnsEngine {
        var engine = new BonjourMdnsEngine();

        resources.add(engine);

        return engine;
    }
}