package discovery.test.tests.mdns.pki;

import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.KeyDescriptor;
import discovery.keys.storage.KeyData;
import discovery.test.integration.data.KeysTestData;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.fakes.FakeKeyStorage;
import discovery.keys.storage.KeyStorage;
import discovery.keys.Keychain;
import discovery.mdns.pki.search.KeyEnquirer;
import discovery.keys.PKI;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class MdnsPkiPublishIntegrationTest extends AsyncTests implements Scenario{
    @steps
    var steps: MdnsPkiPublishIntegrationSteps;

    @After
    public function teardown(){
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function publish_keychain(asyncFactory: AsyncFactory){
        makeHandler(asyncFactory, 1500);

        given().a_mdns_pki_instance();
            and().a_keychain_with_some_key();
        given().a_key_enquirer();
        when().the_keychain_is_published_with_the_pki();
        and().a_query_is_sent_to_a_key_on_the_keychain(next(()->{
            then().a_key_descriptor_response_should_be_received_by_the_client();
            done();
        }));
    }
}

class MdnsPkiPublishIntegrationSteps
    extends Steps<MdnsPkiPublishIntegrationSteps>
{
    var pki:PKI;

    var pkiBuilder:MdnsPkiTestBuilder;
    var keyEnquirer:KeyEnquirer;

    var publisherKeyData:KeyData;
    var publisherKeychain:Keychain;
    var publisherStorage :KeyStorage;

    var expectedFingerprint:Fingerprint;
    var capturedKeyDescriptor:KeyDescriptor;


    public function new(){
        super();

        pkiBuilder = new MdnsPkiTestBuilder();
        publisherStorage = new FakeKeyStorage();
    }

    public function teardown() {
        if(pki != null){
            pki.stop();
        }

        pkiBuilder.close();
    }

    // given ---------------------------------------------------

    @step
    public function a_mdns_pki_instance() {
        pki = pkiBuilder.buildPki();
    }

    @step
    public function a_keychain_with_some_key() {
        publisherKeyData = KeysTestData.elliptCurveKey;
        publisherStorage.addKey(publisherKeyData);
        publisherKeychain = pkiBuilder.buildKeychain(publisherStorage);
    }

    @step
    public function a_key_enquirer() {
        keyEnquirer = pkiBuilder.buildKeyEnquirer();
    }


    // when  ---------------------------------------------------

    @step
    public function the_keychain_is_published_with_the_pki() {
        pki.publishKeychain(publisherKeychain);
    }

    @step
    public function a_query_is_sent_to_a_key_on_the_keychain(next: NextCallback) {
        expectedFingerprint = publisherKeyData.fingerprint.toFingerprint();
        keyEnquirer.searchKeyDescriptor(expectedFingerprint, {
            onFound: (descriptorFound)->{
                capturedKeyDescriptor = descriptorFound.descriptor;
                next();
            }
        });
    }

    // then  ---------------------------------------------------

    @step
    public function a_key_descriptor_response_should_be_received_by_the_client()
    {
        var capturedFingerprint = capturedKeyDescriptor.fingerprint;
        var expectedKeyType = publisherKeyData.keyType;

        assertThat(capturedKeyDescriptor, is(notNullValue()),
            "no key descriptor received");
        assertThat(capturedFingerprint.equals(expectedFingerprint), is(true),
            'Expected key fingerprint: ${expectedFingerprint}\n'+
            'but found: ${capturedFingerprint}'
        );
        assertThat(capturedKeyDescriptor.keySize, is(greaterThan(0)),
            "key size");

        /* Review: current key loader implementation cannot load key parameters
           maybe we should remove key parameters from keytype or fix the key loader implementation */
        assertThat(capturedKeyDescriptor.keyType.getName(),
            is(expectedKeyType.getName()),
            "key type");
        assertThat(capturedKeyDescriptor.keyFormat, is(notNullValue()),
            "key format");
        assertThat(capturedKeyDescriptor.keyParts, hasSize(greaterThan(0)),
            "key parts");
    }
}