package discovery.test.tests.mdns.pki;

import discovery.utils.building.DisposableBuilder;
import discovery.mdns.MdnsEngine;
import discovery.keys.KeyTransformations;
import discovery.test.integration.data.KeysTestData;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.mdns.externs.bonjour.BonjourMdnsEngine;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.mdns.pki.MdnsKeyTransportBuilder;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class MdnsKeyTransportTest extends AsyncTests implements Scenario{
    @steps
    var steps: MdnsKeyTransportSteps;

#if(js && nodejs)

    @After
    public function teardown() {
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function publish_and_get_key(factory: AsyncFactory){
        makeHandler(factory, 1500);

        given().a_key_publisher();
        and().a_key_discoverer();
        when().the_key_publisher_publishes_some_key(next(()->{
            and().the_key_client_searches_for_that_key(next(()->{
                then().the_expected_key_should_be_found();
                done();
            }));
        }));
    }
#end
}


class MdnsKeyTransportSteps extends Steps<MdnsKeyTransportSteps>{

    var keyTransportBuilder:KeyTransportBuilder;

    var publisher:KeyTransport;
    var client:KeyTransport;

    var publishedKey:PublicKeyData;
    var foundKey:PublicKeyData;

    var keygen:KeyGenerator;

    var mdnsDisposer:DisposableBuilder<MdnsEngine>;

    public function new(){
        super();

        #if(js && nodejs)

        keygen = new JscuKeyGenerator();

        mdnsDisposer = new DisposableBuilder(
            ()->new BonjourMdnsEngine(),
            (engine:MdnsEngine)->engine.finish()
        );

        keyTransportBuilder = new MdnsKeyTransportBuilder({
            promises: new JsPromisableFactory(),
            keyLoader: keygen,
            engine: mdnsDisposer.build
        });
        #end
    }

    public function teardown() {
        mdnsDisposer.disposeAll();
    }

    // given ---------------------------------------------------

    @step
    public function a_key_publisher() {
        publisher = keyTransportBuilder.buildKeyTransport();
    }

    @step
    public function a_key_discoverer() {
        client = keyTransportBuilder.buildKeyTransport();
    }

    // when  ---------------------------------------------------

    @step
    public function the_key_publisher_publishes_some_key(next: NextCallback) {
        publishedKey = KeysTestData.rsaKey4096;

        publisher.publishKey(publishedKey, next);
    }

    @step
    public function the_key_client_searches_for_that_key(next: NextCallback) {
        client.searchKey(publishedKey.fingerprint, {
            onKey: (result)->{
                foundKey = result.found;
                next();
            }
        });
    }

    // then  ---------------------------------------------------

    @step
    public function the_expected_key_should_be_found() {
        foundKey.equals(publishedKey).shouldBe(true,
            'Expected: ${publishedKey}\n' +
            'but found: ${foundKey}'
        );
    }
}