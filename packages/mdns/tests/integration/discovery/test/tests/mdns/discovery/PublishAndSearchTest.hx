package discovery.test.tests.mdns.discovery;

import discovery.test.helpers.RandomGen;
import discovery.domain.PublishInfo;
import discovery.keys.Key;
import discovery.test.integration.data.KeysTestData;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.keys.crypto.KeyGenerator;
import discovery.test.integration.tools.NextCallback;
import discovery.test.tests.mdns.discovery.steps.PublishAndSearchBaseSteps;
import discovery.test.integration.tools.AsyncTests;
import massive.munit.async.AsyncFactory;

import hxgiven.Scenario;

using discovery.keys.KeyTransformations;


class PublishAndSearchTest extends AsyncTests implements Scenario{
    @steps
    var steps:PublishAndSearchSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @After
    public function teardown() {
        steps.teardown();
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    public function publish_and_discovery_by_type(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_consumer_node();
        next(()->{
            when().publishing_a_service(next(()->{
            and().searching_for_that_service_type(next(()->{
                then().the_service_should_be_discovered();
                done();
            }));
            }));
        })();
    }

    #if(js && nodejs)
    @AsyncTest
    #end
    @scenario
    // FIXME: unstable test
    public function publish_and_locate_service(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_consumer_node();
        given().the_publisher_has_some_key(next(()->{
            when().publishing_a_service_using_that_key(next(()->{
            and().locating_that_service_from_its_identifier(next(()->{
                then().the_service_should_be_discovered();
                done();
            }));
            }));
        }));
    }
}



class PublishAndSearchSteps
    extends PublishAndSearchBaseSteps<PublishAndSearchSteps>
{
    var keygen:KeyGenerator;
    var publisherKey:Key;

    public function new() {
        super();
        keygen = new JscuKeyGenerator();
    }

    //given -------------------------------------------------------------

    @step
    public function the_publisher_has_some_key(next: NextCallback) {
        keygen.loadKey(KeysTestData.elliptCurveKey)
              .then((keyPair)->keyPair.toKey())
              .then((key)->{
                  publisherKey = key;
                  next();
              }, (err)->{
                  next(err);
              });
    }


    @step
    public function a_service_description_with_fingerprint_as_provider_id() {
        given().a_service_description();
        description.providerId = publisherKey.fingerprint;
    }

    @step
	function a_publish_info_from_description_and_key() {
        var announcement = RandomGen.announcementFrom(description);
        publishInfo = PublishInfo.make(announcement, publisherKey);
    }

    //when -------------------------------------------------------------

    @step
    public function publishing_a_service_using_that_key(next: NextCallback) {
        given().a_service_description_with_fingerprint_as_provider_id();
        given().a_publish_info_from_description_and_key();

        when().starting_publication(next);
    }
}