package discovery.test.tests.mdns.pki;

import discovery.test.integration.data.KeysTestData;
import discovery.keys.Key;
import discovery.keys.storage.KeyData;
import discovery.test.fakes.FakeKeyStorage;
import discovery.keys.storage.KeyStorage;
import discovery.keys.Keychain;
import discovery.keys.PKI;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class MdnsPkiTest extends AsyncTests implements Scenario{
    @steps
    var steps: MdnsPkiSteps;

    #if(js && nodejs)

    @After
    public function teardown(){
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function publish_and_discovery_key(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_client_node();
        given().a_keychain_with_some_valid_public_key(next(()->{
        when().the_publisher_register_this_keychain(next(()->{
            when().the_client_searches_for_that_key(next(()->{
            then().the_published_key_should_be_discovered(next(()->{
                done();
            }));
            }));
        }));
        }));
    }

    #end
}

class MdnsPkiSteps extends Steps<MdnsPkiSteps>{
    var publisherPki:PKI;
    var clientPki:PKI;

    var publisherKeychain:Keychain;
    var keystorage:KeyStorage;
    var publisherKeyCache:Keychain;

    var publisherKeyData:KeyData;
    var capturedKey:Key;

    var pkiBuilder: MdnsPkiTestBuilder;

    public function new(){
        super();
        pkiBuilder = new MdnsPkiTestBuilder();
        keystorage = new FakeKeyStorage();
        publisherKeyCache = pkiBuilder.buildKeychain(keystorage);
    }

    public function teardown(){
        pkiBuilder.close();
    }

    // given ---------------------------------------------------
    @step
    public function a_publisher_node() {
        publisherPki = makePki(publisherKeyCache);
    }

    @step
    public function a_client_node() {
        clientPki = makePki();
    }

    function makePki(?keyCache: Keychain):PKI {
        return pkiBuilder.buildPki();
    }

    @step
    public function a_keychain_with_some_valid_public_key(next: NextCallback) {
        given().a_valid_keychain();
        given().some_valid_public_key();

        next();
    }

    function a_valid_keychain() {
        publisherKeychain = pkiBuilder.buildKeychain(keystorage);
    }

    function some_valid_public_key() {
        publisherKeyData = KeysTestData.elliptCurveKey;
        keystorage.addKey(publisherKeyData);
    }

    // when  ---------------------------------------------------

    @step
    public function the_publisher_register_this_keychain(next: NextCallback) {
        publisherPki.publishKeychain(this.publisherKeychain);
        next();
    }

    @step
    public function the_client_searches_for_that_key(next: NextCallback) {
        clientPki.searchKey(publisherKeyData.fingerprint.toFingerprint())
            .then((key)->{
                capturedKey = key;
                next();
            }, (err)->{
                next(err);
                throw err;
            });
    }

    // then  ---------------------------------------------------

    @step
    public function the_published_key_should_be_discovered(next: NextCallback) {
        var expectedFingerprint = publisherKeyData.fingerprint.toFingerprint();

        assertThat(capturedKey, is(notNullValue()), "no key discovered");
        assertThat(capturedKey.fingerprint, is(equalTo(expectedFingerprint)),
            "Captured key does not has the expected fingerprint");

        next();
    }
}