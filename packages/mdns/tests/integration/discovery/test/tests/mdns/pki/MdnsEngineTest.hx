package discovery.test.tests.mdns.pki;

import discovery.test.helpers.generators.MdnsPkiGenerator;
import discovery.mdns.pki.MdnsPkiMessages;
import discovery.test.fakes.FakePromisable;
import discovery.mdns.dns.DnsResponse;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import discovery.mdns.dns.DnsQuery;
import discovery.test.mdns.generators.MdnsRandomGen;
import discovery.mdns.DnsRecord;
import discovery.mdns.externs.bonjour.BonjourMdnsEngine;
import discovery.mdns.MdnsEngine;
import discovery.test.integration.tools.NextCallback;
import discovery.test.integration.tools.AsyncTests;
import massive.munit.async.AsyncFactory;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;

using discovery.utils.EqualsComparator;


class MdnsEngineTest extends AsyncTests implements Scenario{
    @steps
    var steps: MdnsEngineSteps;

    #if (js && nodejs)

    @Before
    public function setup(){
        resetSteps();
    }

    @After
    public function teardown() {
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function publish_and_query_txt_record(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_client_node();
        when().the_publisher_publishes_a_txt_record(next(()->{
            and().the_client_search_for_that_record(next(()->{
                then().the_expected_record_should_be_found();
                done();
            }));
        }));
    }

    @AsyncTest
    @scenario
    public function publish_and_query_record(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_client_node();
        when().the_publisher_publishes_some_record(next(()->{
            and().the_client_search_for_that_record(next(()->{
                then().the_expected_record_should_be_found();
                done();
            }));
        }));
    }

    @AsyncTest
    @scenario
    public function query_and_respond(asyncFactory: AsyncFactory) {
        makeHandler(asyncFactory, 1500);

        given().a_publisher_node();
        and().a_client_node();
        given().the_publisher_registered_a_query_listener();
        when().the_client_send_a_matching_query();
        when().the_publisher_receives_the_query(next(()->{
            and().the_publisher_responds();
            then().the_client_should_receive_the_response(next(()->{
                done();
            }));
        }));

    }

    #end

}

class MdnsEngineSteps extends Steps<MdnsEngineSteps>{
    var publisherMdns: MdnsEngine;
    var clientMdns   : MdnsEngine;

    var publishedRecord: DnsRecord;
    var responseRecord: DnsRecord;

    var pkiMessages:MdnsPkiMessages;
    var generator:MdnsPkiGenerator;

    var listeningQuery:DnsQuery;
    var gotQueryPromise:FakePromisable<QueryEvent>;
    var receivedQueryEvt:QueryEvent;
    var receivedResponse:DnsResponse;
    var expectedResponse:DnsResponse;
    var responsePromise:FakePromisable<DnsResponse>;

    public function new(){
        super();

        gotQueryPromise = new FakePromisable();
        responsePromise = new FakePromisable();

        generator = new MdnsPkiGenerator();
        pkiMessages = new MdnsPkiMessages();
    }

    public function teardown(){
        finishMdns(publisherMdns);
        finishMdns(clientMdns);
    }

    function finishMdns(mdns: MdnsEngine) {
        if(mdns != null){
            mdns.finish();
        }
    }

    // given ---------------------------------------------------
    @step
    public function a_publisher_node() {
        publisherMdns = makeMdnsEngine();
    }

    @step
    public function a_client_node() {
        clientMdns = makeMdnsEngine();
    }

    function makeMdnsEngine():MdnsEngine {
        return new BonjourMdnsEngine();
    }

    @step
    function a_txt_record() {
        given().a_record_of_type(TXT);
    }

    @step
    function some_record() {
        given().a_record_of_type(Random.fromArray([A, AAAA, TXT, PTR]));
    }

    @step
    function a_record_of_type(type: DnsRecordType) {
        publishedRecord = MdnsRandomGen.mdns.dnsRecordWith(type);
    }

    @step
    public function
        the_publisher_registered_a_query_listener()
    {
        listeningQuery = MdnsRandomGen.mdns.dnsQuery(TXT);
        publisherMdns.onQuery(listeningQuery, (queryEvt)->{
            this.receivedQueryEvt = queryEvt;
            gotQueryPromise.resolve(queryEvt);
        });
    }

    // when  ---------------------------------------------------

    @step
    public function the_publisher_publishes_a_txt_record(next: NextCallback) {
        given().a_txt_record();
        when().publishing_this_record(next);
    }

    @step
    public function the_publisher_publishes_some_record(next: NextCallback) {
        given().some_record();
        when().publishing_this_record(next);
    }

    @step
    function publishing_this_record(next:NextCallback) {
        publisherMdns.publish(publishedRecord, next);
    }

    @step
    public function the_client_search_for_that_record(next: NextCallback) {
        var query:DnsQuery = {
            type: publishedRecord.type,
            name: publishedRecord.name
        };

        clientMdns.query(query, (response)->{
            if(responseRecord == null){//receive only first
                responseRecord = response.record;
            }
            next();
        }, (err)->{
            next(err);
        });
    }


    @step
    public function the_client_send_a_matching_query() {
        clientMdns.query(listeningQuery, (response)->{
            receivedResponse = response;
            responsePromise.resolve(response);
        });
    }

    @step
    public function the_publisher_receives_the_query(next: NextCallback) {
        gotQueryPromise.then((queryEvt)->{
            next();
        })
        .catchError((err)->{
            next(err);
        });
    }

    @step
    public function the_publisher_responds() {
        var query = receivedQueryEvt.query();

        var descriptor = generator.keyDescriptor(0,0);
        var descriptorTxt = pkiMessages.descriptorTxt(descriptor);

        expectedResponse = DnsResponse.makeFromQuery(query, {
            data: descriptorTxt,
            ttl: 3000
        });

        receivedQueryEvt.respond(expectedResponse);
    }


    // then  ---------------------------------------------------

    @step
    public function the_expected_record_should_be_found() {
        assertThat(responseRecord, is(notNullValue()),
            "Response record should not be null");

        assertThat(responseRecord.equals(publishedRecord), is(true),
            'Expecting: ${publishedRecord}\n' +
            'but found: ${responseRecord}'
        );
    }

    @step
    public function the_client_should_receive_the_response(next: NextCallback) {
        responsePromise.then((response)->{
            assertThat(response.equals(expectedResponse),
                'Responses does not match');
            next();
        })
        .catchError((err)->{
            next(err);
        });
    }
}