package discovery.test.tests.mdns.configuration;

import haxe.Exception;
import discovery.utils.dependencies.MissingDependencyException;
import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.mdns.building.NBonjourMechanismsBuilder;
import discovery.base.configuration.DiscoveryMechanismBuilder;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class NBonjourMechanismsBuilderTest implements Scenario{
    @steps
    var steps: NBonjourMechanismsBuilderSteps;

    @Test
    @scenario
    public function build_discvoery_mechanism_from_dependencies(){
        given().all_requirements_are_set();
        given().a_builder_instance();
        when().building_a_discovery_mechanism();
        then().a_valid_discovery_mechanism_should_be_returned();
    }

    @Test
    public function test_missing_dependencies(){
        for(dep in ['promises', 'keyloader', 'keycacheGetter']){
            should_throw_when_missing_dependency(dep);
        }
    }

    @scenario
    function should_throw_when_missing_dependency(dep:String) {
        given().this_dependency_is_missing(dep);
        when().trying_to_create_the_builder();
        then().a_missing_dependency_exception_should_be_thrown();
    }
}

class NBonjourMechanismsBuilderSteps
    extends Steps<NBonjourMechanismsBuilderSteps>
{
    var builder:DiscoveryMechanismBuilder;

    var dependencies:NBonjourMechanismsDependencies;
    var missingDependency:String;
    var builtMechanism:DiscoveryMechanism;

    var keycacheGetterMockup:KeychainMockup;
    var keyloaderMockup:KeyLoaderMockup;

    var capturedException:Exception;

    public function new(){
        super();

        keycacheGetterMockup = new KeychainMockup();
        keyloaderMockup = new KeyLoaderMockup();
    }

    // given ---------------------------------------------------

    @step
    public function this_dependency_is_missing(dep:String) {
        given().all_requirements_are_set();
        given().this_dependency_is_removed(dep);
    }

    @step
    public function all_requirements_are_set() {
        dependencies = {
            promises: new FakePromisableFactory(),
            keycacheGetter: keycacheGetterMockup.keychain,
            keyloader: keyloaderMockup.keyLoader()
        };
    }

    @step
    function this_dependency_is_removed(dep:String) {
        Reflect.hasField(dependencies, dep)
                .shouldBe(true, 'Field ${dep} not found');
        Reflect.setField(dependencies, dep, null);

        this.missingDependency = dep;
    }

    @step
    public function a_builder_instance() {
        builder = new NBonjourMechanismsBuilder(dependencies);
    }

    // when  ---------------------------------------------------


    @step
	public function trying_to_create_the_builder() {
        try{
            given().a_builder_instance();
        }
        catch(e){
            capturedException = e;
        }
    }

    @step
    public function trying_to_build_a_discovery_mechanism() {
        try{
            when().building_a_discovery_mechanism();
        }
        catch(e){
            capturedException = e;
        }
    }

    @step
    public function building_a_discovery_mechanism() {
        builtMechanism = builder.buildMechanism();
    }

    // then  ---------------------------------------------------

    @step
    public function a_valid_discovery_mechanism_should_be_returned() {
        builtMechanism.shouldNotBeNull();
    }

    @step
    public function a_missing_dependency_exception_should_be_thrown() {
        capturedException.shouldBeAn(MissingDependencyException,
            'Should have thrown exception for missing dependency: "${missingDependency}"');
    }
}