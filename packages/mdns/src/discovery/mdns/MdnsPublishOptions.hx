package discovery.mdns;

import haxe.DynamicAccess;

typedef MdnsPublishDataStruct = {
    name: String,
    type: String,
    port: Int,
    ?host: String,
    ?subtypes: Array<String>,
    ?protocol: String,
    ?txt: MdnsTxt
};

@:forward
abstract MdnsPublishData(MdnsPublishDataStruct)
    from MdnsPublishDataStruct
    to MdnsPublishData
{
    public function clone(): MdnsPublishData {
        var objAccess:DynamicAccess<Dynamic> = (this : Dynamic);
        var optionsData:MdnsPublishDataStruct = (objAccess.copy() : Dynamic);

        return optionsData;
    }
}