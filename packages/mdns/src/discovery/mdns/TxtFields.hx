package discovery.mdns;

class TxtFields {
    public static final name       = "name";
    public static final type       = "type";
    public static final providerId = "id";
    public static final instanceId = "instance";

    public static final signature  = "signature";

    public static final providerIdHashAlg = "id_hash";

    public static final defaultPort = "port";
    public static final addresses = "addrs";
    public static final channels = "cn";

    public static final attributes = "_";

    public static final validTo     = "vld_to";
    public static final validFrom   = "vld_from";
}