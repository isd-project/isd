package discovery.mdns;

import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signed;
import discovery.mdns.conversion.TxtParser;
import discovery.mdns.conversion.TxtBuilder;
import discovery.mdns.MdnsTxt.MdnsTxtValue;
import haxe.io.Bytes;
import discovery.utils.traversal.ObjectVisitor;
import discovery.utils.traversal.ObjectTraversal;
import discovery.format.Binary;
import discovery.domain.Description;

using StringTools;

class MdnsTxtConverter {
    public function new() {

    }

    public static function signedToTxt(description:Signed<Description>):MdnsTxt{
        return new MdnsTxtConverter().convertSignedToTxt(description);
    }

    public static function toTxt(description: Description): MdnsTxt {
        return new MdnsTxtConverter().convertToTxt(description);
    }

    public static function toDescription(txt:MdnsTxt):Description {
        return new MdnsTxtConverter().convertToDescription(txt);
    }

    public static function toSignedDescription(txt:MdnsTxt):Signed<Description> {
        return new MdnsTxtConverter().convertToSignedDescription(txt);
    }

    public static function builder(): TxtBuilder{
        return new TxtBuilder();
    }

    public function
        convertSignedToTxt(description: Signed<Description>): MdnsTxt
    {
        return builder()
                .addDescription(description)
                .addSignature(description)
                .build()
            ;
    }

    public function convertToTxt(description:Description):MdnsTxt {
        return new TxtBuilder().buildTxt(description);
    }

    public function announcementToTxt(announcement:Announcement):MdnsTxt {
        // throw new haxe.exceptions.NotImplementedException();
        return builder()
                .addDescription(announcement.description)
                .addValidity(announcement.validity)
                .build();
    }

    public function convertToDescription(txt: MdnsTxt): Description{
        return new TxtParser().extractDescription(txt);
    }

    public function convertToSignedDescription(txt:MdnsTxt):Signed<Description> {
        return new TxtParser().extractSignedDescription(txt);
    }

    public static function parser(): TxtParser{
        return new TxtParser();
    }
}


