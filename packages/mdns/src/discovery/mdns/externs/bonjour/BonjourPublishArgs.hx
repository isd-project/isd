package discovery.mdns.externs.bonjour;

import discovery.format.Binary.BinaryData;
import js.node.buffer.Buffer;
import haxe.io.Bytes;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import haxe.DynamicAccess;



@:forward
abstract BonjourPublishArgs(BonjourPublishArgsData)
    from BonjourPublishArgsData
    to   BonjourPublishArgsData
{
    @:from
    public static
    function fromMdnsPublishData(opts:MdnsPublishData): BonjourPublishArgs
    {
        if(opts == null) return null;

        var copy:Dynamic = opts.clone();
        copy.txt = transformTxt(opts.txt);

        return (copy : BonjourPublishArgsData);
    }

    static function transformTxt(txt:Null<MdnsTxt>):Dynamic {
        if(txt == null) return null;

        var bonjourTxt = new DynamicAccess();

        for(k => value in txt.keyValueIterator()){
            var outValue:Dynamic = value;
            if(Std.isOfType(value, Bytes)){
                outValue = BinaryBuffer.fromBytes((value:Bytes));
            }
            else if(Std.isOfType(value, BinaryData)){
                outValue = BinaryBuffer.fromBytes((value:BinaryData).toBytes());
            }
            bonjourTxt.set(k, outValue);
        }

        return bonjourTxt;
    }

}

typedef BonjourPublishArgsData = {
    name: String,
    type: String,
    port: Int,
    ?host: String,
    ?subtypes: Array<String>,
    ?protocol: String,
    ?txt: Dynamic
}