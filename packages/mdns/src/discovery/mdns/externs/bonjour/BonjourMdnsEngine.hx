package discovery.mdns.externs.bonjour;

import discovery.mdns.mdnsengine.QueryHandler;
import discovery.base.search.Stoppable;
import discovery.mdns.mdnsengine.MulticastDnsEnquirer;
import discovery.mdns.mdnsengine.MdnsRecordPublisher;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.MdnsEngine.OnDnsResponse;
import discovery.mdns.MdnsEngine.OnErrorCallback;
import discovery.mdns.MdnsEngine.OnPublishRecord;

class BonjourMdnsEngine implements MdnsEngine{

    var bonjour: Bonjour;

    var recordsPublisher:MdnsRecordPublisher;
    var mdnsEnquirer:MulticastDnsEnquirer;

    public function new(?bonjour:Bonjour) {
        if(bonjour == null){
            bonjour = Bonjour.create();
        }

        this.bonjour = bonjour;

        var server = bonjour.server();
        var multicastDns = bonjour.multicastdns();

        recordsPublisher = new MdnsRecordPublisher(server, multicastDns);
        mdnsEnquirer = new MulticastDnsEnquirer(multicastDns);
    }

    public function publish(record:DnsRecord, ?onPublish:OnPublishRecord) {
        recordsPublisher.publishRecord(record);

        if(onPublish != null){
            onPublish();
        }
    }

    public function query(
        query:DnsQuery, onResponse:OnDnsResponse, ?onError:OnErrorCallback): Stoppable
    {
        return mdnsEnquirer.query(query, onResponse, onError);
    }

    public function finish() {
        bonjour.destroy();
    }

    public function onQuery(query:DnsQuery, onQuery:QueryHandler) {
        recordsPublisher.onQuery(query, onQuery);
    }
}