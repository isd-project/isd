package discovery.mdns.externs.bonjour;

import discovery.mdns.externs.multicastdns.MulticastDnsRecord;
import discovery.mdns.externs.multicastdns.MulticastDns;

@:jsRequire("nbonjour", "Server")
@:native("nbonjour.Server")
extern class MdnsServer {
    public var mdns:MulticastDns;

    public function register(records: Array<MulticastDnsRecord>): Void;
}