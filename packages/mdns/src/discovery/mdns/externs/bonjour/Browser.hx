package discovery.mdns.externs.bonjour;

@:jsRequire("nbonjour", "Browser")
@:native("nbonjour.Browser")
extern class Browser {
    function start():Void;
    function stop():Void;
    function update():Void;
}