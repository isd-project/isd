package discovery.mdns.externs.bonjour;

typedef BonjourQuery = {
    ?type: String,
    ?subtypes: Array<String>,
    ?types:Dynamic,
    ?protocol:String,
    ?txt: Dynamic
}