package discovery.mdns.externs.bonjour;

interface BonjourService {

    function on(eventType:String, callback:Dynamic): Void;

    function stop(callback: ()->Void):Void;
}