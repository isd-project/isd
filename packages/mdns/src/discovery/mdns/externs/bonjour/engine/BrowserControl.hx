package discovery.mdns.externs.bonjour.engine;

import discovery.base.search.Stoppable;

class BrowserControl implements Stoppable{
    var browser:Browser;

    public function new(browser:Browser) {
        this.browser = browser;
    }

    public function stop(?callback:StopCallback) {
        browser.stop();

        if(callback != null) callback();
    }
}