package discovery.mdns.externs.bonjour.engine;

import discovery.impl.events.FutureOnceEvent;
import discovery.utils.events.EventEmitter;
import haxe.Exception;
import discovery.exceptions.PublicationException;
import discovery.mdns.DnsSd.DnsSdPublishListener;
import discovery.base.search.Stoppable;

class BonjourServiceControl implements Stoppable{
    var service: BonjourService;

    var publishedEvent:EventEmitter<Any>;
    var finishedEvent:EventEmitter<Null<Exception>>;

    public function new(srv:BonjourService, ?listeners:DnsSdPublishListener) {
        this.service = srv;

        publishedEvent = new FutureOnceEvent();
        finishedEvent = new FutureOnceEvent();

        registerListeners(listeners);

        service.on("up", this.onUp);
        service.on("error", this.onError);
    }

    function registerListeners(listeners:DnsSdPublishListener) {
        if(listeners == null) return;

        if(listeners.onPublish != null)
            publishedEvent.listen((_)->listeners.onPublish());
        if(listeners.onFinish != null)
            finishedEvent.listen(listeners.onFinish);
    }

    function onUp() {
        publishedEvent.notify(true);
    }

    function onError(err:Dynamic) {
        var exception = new PublicationException("Mdns publication received an error", err);

        finish(exception);
    }

	function finish(?error: Exception) {
        finishedEvent.notify(error);
    }

    public function stop(?callback:StopCallback) {
        if(callback == null) callback = ()->{};

        service.stop(()->{
            callback();
            finish();
        });
    }

}