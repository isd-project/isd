package discovery.mdns.externs.bonjour.engine;

import discovery.mdns.DnsSd.DnsSdQuery;
import discovery.mdns.DnsSd.DnsSdResolveQuery;
import discovery.mdns.DnsSd.MdnsServiceFound;
import discovery.base.search.Stoppable;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.mdns.DnsSd.DnsSdPublishListener;
import discovery.utils.functional.Callback;

class BonjourDnsSd implements DnsSd{

    var bonjour:Bonjour;

    var engine:BonjourDnsSdEngine;

    public function new(bonjour: Bonjour) {
        this.bonjour = bonjour;
        this.engine = new BonjourDnsSdEngine(bonjour);
    }

    public function finish(callback:Callback) {}

    public function publish(
        publishArgs:MdnsPublishData, ?listener:DnsSdPublishListener):Stoppable
    {
        var bonjourSrv = engine.publish(publishArgs);

        return new BonjourServiceControl(bonjourSrv, listener);
    }

    public function
        find(findOpt:DnsSdQuery, onFound:MdnsServiceFound):Stoppable
    {
        return doQuery(translateQuery(findOpt), onFound);
    }

    public function
        resolve(query:DnsSdResolveQuery, onFound:MdnsServiceFound):Stoppable
    {
        var resolved= false;
        var queryStop:Stoppable;
        queryStop = doQuery(translateResolveQuery(query), (srv)->{
            if(resolved) return;

            if(onFound != null && resolveMatches(srv, query)){
                resolved = true;
                if(queryStop != null) queryStop.stop();

                onFound(srv);
            }
        });

        return queryStop;
    }

    function resolveMatches(
        srv:DiscoveredService, query:DnsSdResolveQuery): Bool
    {
        if(srv == null || query == null) return false;

        return queryMatch(query.name, srv.name)
            && queryMatch(query.type, srv.type)
            && queryMatch(query.protocol, srv.protocol);
    }


    function queryMatch(queryPart:String, srvPart:String) {
        if(queryPart == null) return true;
        if(srvPart == null) return false;

        return queryPart.toLowerCase() == srvPart.toLowerCase();
    }

    function doQuery(bonjourQuery: BonjourQuery, onFound:MdnsServiceFound) {
        var browser = engine.find(bonjourQuery, onFound);

        return new BrowserControl(browser);
    }

    function translateQuery(findOpt:DnsSdQuery):MdnsQuery {
        return {
            type: findOpt.type,
            subtypes: findOpt.subtypes,
            protocol: findOpt.protocol,
            txt: {binary: true}
        };
    }

    function translateResolveQuery(query:DnsSdResolveQuery):MdnsQuery {
        return {
            type: query.type,
            protocol: query.protocol,
            txt: {binary: true}
        };
    }
}