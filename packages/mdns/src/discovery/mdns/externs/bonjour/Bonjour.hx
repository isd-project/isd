package discovery.mdns.externs.bonjour;

import discovery.mdns.externs.multicastdns.MulticastDns;
import discovery.mdns.externs.bonjour.BonjourService;

@:jsRequire("nbonjour")
extern class Bonjour {
    static
    function create (?args:Dynamic): Bonjour;
    function destroy(): Void;

    inline public function server(): MdnsServer{
        return untyped this._server;
    }
    inline public function multicastdns(): MulticastDns {
        return server().mdns;
    }

    function find(options:BonjourQuery, ?onFound:OnFoundCallback):Browser;
    function publish(options:BonjourPublishArgs): BonjourService;
}


typedef OnFoundCallback = (BonjourDiscoveredService)->Void;

typedef BonjourDiscoveredService = {
    name: String,
    port: Int,
    type: String,
    addresses: Array<String>,

    ?fqdn: String,
    ?host: String,
    ?rawTxt: Dynamic, //js.node.Buffer,
    ?txt: Dynamic,
    ?referer: Referer,
    ?protocol: String,
    ?subtypes: Array<Dynamic>
}


typedef Referer = {
    address: String,
    family: String,
    port: Int,
    size: Int
}