package discovery.mdns.externs.bonjour;

import haxe.io.Bytes;

#if (js && nodejs)
// #if nodejs
import js.node.Buffer;
private typedef BinaryBase = Buffer;

#else
private typedef BinaryBase = Bytes;
#end

typedef BinaryBufferType = BinaryBase;

/**
    This abstract is intended to easy conversion between node Buffer and haxe Bytes.
**/
@:forward()
abstract BinaryBuffer(BinaryBase) from BinaryBase to BinaryBase {
    public function new(bytes: BinaryBase) {
        this = bytes;
    }

    #if (js && nodejs)
    @:from
    public static function fromBytes(bytes: Bytes): BinaryBuffer {
        if(bytes == null) return null;

        return new BinaryBuffer(Buffer.hxFromBytes(bytes));
    }

    @:to
    public function toBytes(): Bytes {
        return this.hxToBytes();
    }

    #else

    public static function fromBytes(bytes: Bytes) {
        return new BinaryBuffer(bytes);
    }

    public function toBytes() {
        return this;
    }
    #end
}