package discovery.mdns.externs.bonjour;

import discovery.format.Binary;
import discovery.mdns.externs.bonjour.BinaryBuffer.BinaryBufferType;
import haxe.DynamicAccess;
import discovery.mdns.externs.bonjour.Bonjour.BonjourDiscoveredService;
import discovery.mdns.DnsSdEngine.MdnsOnFound;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;

class BonjourDnsSdEngine implements DnsSdEngine{
    var bonjour:Bonjour;

    public function new(?bonjour: Bonjour) {
        this.bonjour = if(bonjour != null) bonjour else Bonjour.create();
    }

    public function find(findOpt:MdnsQuery, onFound: MdnsOnFound):Browser {
        findOpt.txt = {binary: true};

        return bonjour.find(findOpt, onFoundService.bind(_, onFound));
    }

    public function publish(publishData:MdnsPublishData):BonjourService {
        return bonjour.publish(BonjourPublishArgs.fromMdnsPublishData(publishData));
    }

    public function finish(callback:() -> Void) {
        bonjour.destroy();

        if(callback != null){
            callback();
        }
    }


    // -----------------------------------------------------------------

    function onFoundService(
        bonjourService:BonjourDiscoveredService,
        onFound:MdnsOnFound)
    {
        if(onFound != null){
            var discovered = convertBonjourService(bonjourService);
            onFound(discovered);
        }
    }

    function convertBonjourService(
        bonjourService:BonjourDiscoveredService):DiscoveredService
    {
        return {
            name: bonjourService.name,
            subtypes: bonjourService.subtypes,
            protocol: bonjourService.protocol,
            type: bonjourService.type,
            port: bonjourService.port,
            addresses: bonjourService.addresses,
            fqdn: bonjourService.fqdn,
            host: bonjourService.host,
            txt: convertTxt(bonjourService.txt)
        };
    }

    function convertTxt(bonjourTxt:DynamicAccess<Any>): MdnsTxt {
        var txt = new MdnsTxt();

        for(key => value in bonjourTxt.keyValueIterator()){
            if(!Std.isOfType(value, String) && Std.isOfType(value, BinaryBufferType)){
                var buffer:BinaryBuffer = value;

                value = Binary.fromBytes(buffer.toBytes());
            }

            txt.set(key, value);
        }

        return txt;
    }
}