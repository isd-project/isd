package discovery.mdns.externs.multicastdns;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.externs.multicastdns.MulticastDnsRecord;

typedef MulticastDnsResponsePacket = {
    answers: Array<MulticastDnsRecord>
};

@:forward
abstract MulticastDnsResponses(MulticastDnsResponsePacket)
    from MulticastDnsResponsePacket
    to MulticastDnsResponsePacket
{
    @:from
    public static function fromResponse(dnsResponse: DnsResponse): MulticastDnsResponses{
        return {
            answers: [MulticastDnsRecord.fromDnsRecord(dnsResponse.record)]
        };
    }
}