package discovery.mdns.externs.multicastdns;


typedef MdnsRemoteInfo = {
    var port:Int;
    var family:String;
    var address:String;
    var size:Int;
};