package discovery.mdns.externs.multicastdns;

import haxe.io.BytesBuffer;
import haxe.io.Bytes;
import discovery.mdns.externs.bonjour.BinaryBuffer;
import discovery.mdns.externs.multicastdns.MulticastDnsRecord.MdnsRecordData;
import discovery.format.Binary;
import discovery.mdns.DnsRecord.DnsRecordType;

class MdnsRecordConverter {
    public function new() {

    }

    public function encodeData(type:DnsRecordType, data:Binary):MdnsRecordData {
        if(data == null) return null;

        return switch (type){
            case A|AAAA|PTR: data.toUTF8();
            case TXT: [BinaryBuffer.fromBytes(data.toBytes())];
            default: null;
        }
    }

    public function decodeData(type:DnsRecordType, data:MdnsRecordData):Binary {
        if (data == null) return null;

        var bufferArray = data.bufferArray();
        var stringArray = data.stringArray();

        if(bufferArray != null){
            return joinBufferArray(bufferArray);
        }

        if(stringArray != null){
            data = stringArray.join('');
        }

        if(data.isA(String)){
            return Binary.fromText(data);
        }

        if(data.isA(BinaryBufferType)){
            var binaryData:BinaryBuffer = data;
            return binaryData.toBytes();
        }

        return Binary.fromDynamic(data);
    }

    function joinBufferArray(bufferArray:Array<BinaryBuffer>):Binary {
        var bytesBuffer = new BytesBuffer();

        for (data in bufferArray){
            var bytes = data.toBytes();
            bytesBuffer.addBytes(bytes, 0, bytes.length);
        }

        return Binary.fromBytes(bytesBuffer.getBytes());
    }
}