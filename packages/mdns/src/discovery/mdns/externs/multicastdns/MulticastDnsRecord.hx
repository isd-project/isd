package discovery.mdns.externs.multicastdns;

import haxe.extern.EitherType;
import haxe.io.BytesBuffer;
import discovery.format.Binary;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.mdns.externs.bonjour.BinaryBuffer;

import equals.Equal;

using discovery.utils.Cast;


private typedef StringOrBuffer = EitherType<String, BinaryBuffer>;
private typedef ArrayOfStringOrBuffer = EitherType<Array<String>, Array<BinaryBuffer>>;
private typedef MdnsRecordDataType = EitherType<StringOrBuffer, ArrayOfStringOrBuffer>;

abstract MdnsRecordData(MdnsRecordDataType) from MdnsRecordDataType{
    public function new(recordData: MdnsRecordDataType) {
        this = recordData;
    }

    public function isA(type: Dynamic) {
        return Std.isOfType(this, type);
    }

    @:to
    public function buffer(): BinaryBuffer {
        return this.castIfIs(BinaryBufferType);
    }

    @:to
    public function text(): String {
        return this.castIfIs(String);
    }

    @:to
    public function bufferArray(): Array<BinaryBuffer> {
        return toArrayOf(BinaryBufferType);
    }

    @:to
    public function stringArray(): Array<String>{
        return toArrayOf(String);
    }

    function toArrayOf<T>(type:Dynamic): Array<T>{
        var array = this.castIfIs(Array);

        if(array == null || array.length == 0) return array;

        if(Std.isOfType(array[0], type)){
            return array;
        }

        return null;
    }
}

@:forward
abstract MulticastDnsRecord(MulticastDnsRecordStruct)
    from MulticastDnsRecordStruct
    to MulticastDnsRecordStruct
{
    public function new(record: MulticastDnsRecordStruct){
        this = record;
    }

    @:from
    public static function fromDnsRecord(record: DnsRecord):  MulticastDnsRecord
    {
        if(record == null) return null;

        return new MulticastDnsRecord({
            name: record.name,
            type: record.type,
            data: encodeData(record),
            ttl: record.ttl
        });
    }

    @:to
    public function toDnsRecord():  DnsRecord
    {
        if(this == null) return null;

        return {
            name: this.name,
            type: this.type,
            data: decodeData(),
            ttl: this.ttl
        };
    }

    public function decodeData():Binary {
        return converter().decodeData(this.type, this.data);
    }

    public static function encodeData(record:DnsRecord):MdnsRecordData {
        return converter().encodeData(record.type, record.data);
    }

    static function converter(): MdnsRecordConverter {
        return new MdnsRecordConverter();
    }
}

@:structInit
class MulticastDnsRecordStruct {
    public var name: String;
    public var type: DnsRecordType;
    public var data: MdnsRecordData;
    @:optional
    public var ttl: Int;

    public function equals(other: MulticastDnsRecordStruct){
        return Equal.equals(this, other);
    }
}