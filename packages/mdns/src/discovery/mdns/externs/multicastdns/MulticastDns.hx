package discovery.mdns.externs.multicastdns;

import discovery.mdns.externs.multicastdns.MdnsResponseInfo.MdnsRemoteInfo;
import haxe.Constraints.Function;


typedef OnMdnsResponse = (MulticastDnsResponses, MdnsRemoteInfo)->Void;

@:jsRequire("multicast-dns")
extern class MulticastDns {
    public function on<T:Function>(event:String, listener:T):Void;
    public function off<T:Function>(event:String, listener:T):Void;

    public function query(query: MulticastDnsQueries): Void;
    public function respond(response: MulticastDnsResponses): Void;
}



