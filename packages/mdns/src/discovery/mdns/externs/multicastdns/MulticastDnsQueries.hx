package discovery.mdns.externs.multicastdns;

import discovery.mdns.dns.DnsQuery;

typedef MulticastDnsQuery = {
    name: String,
    type: String
};

typedef MulticastDnsQueryPacket = {
    questions: Array<MulticastDnsQuery>
};

typedef MulticastDnsQueriesStruct = {
    questions: Array<DnsQuery>
};

@:forward
abstract MulticastDnsQueries(MulticastDnsQueriesStruct)
    from MulticastDnsQueriesStruct
    to MulticastDnsQueriesStruct
{
    @:from
    public static function
        fromPacket(packet: MulticastDnsQueryPacket): MulticastDnsQueries
    {
        if(packet == null) return null;

        return {
            questions: parseQuestions(packet.questions)
        };
    }

    @:from
    public static function fromQuery(dnsQuery: DnsQuery): MulticastDnsQueries{
        return {
            questions: [dnsQuery]
        };
    }


    static function parseQuestions(questions:Array<MulticastDnsQuery>) {
        if(questions == null) return [];

        return [for(q in questions) new DnsQuery(q.name, cast q.type)];
    }
}