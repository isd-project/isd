package discovery.mdns;

import discovery.domain.Struct.StructTyped;
import discovery.format.Binary;
import haxe.io.Bytes;
import haxe.extern.EitherType;
import haxe.DynamicAccess;

// typedef MdnsTxtValue = EitherType<String, EitherType<Bytes, Binary>>;
private typedef MdnsTxtBase = StructTyped<MdnsTxtValue>;

@:transitive
abstract MdnsTxtValue(Dynamic)
    from String
    from Binary
{
    @:from
    public static function fromBytes(bytes: Bytes): MdnsTxtValue {
        return Binary.fromBytes(bytes);
    }

    public function isText():Bool{
        return toString() != null;
    }

    @:to
    public function toString(): String {
        return convertTo(String);
    }

    @:to
    public function toBinary(): Binary {
        return convertTo(BinaryData);
    }

    @:to
    public function toBytes(): Bytes {
        return toBinary();
    }

    public function toText(): String{
        if(this == null) return null;

        var str = toString();

        if(str != null){
            return str;
        }

        try{
            return toBinary().toUTF8();
        }
        catch(e){
            return null;
        }
    }

    function convertTo<T>(type: Dynamic): T{
        return if(Std.isOfType(this, type)) this else null;
    }


    public static function fromDynamic(value:Any):MdnsTxtValue {
        if(Std.isOfType(value, String) || Std.isOfType(value, BinaryData)){
            return value;
        }
        else if(Std.isOfType(value, Bytes)){
            return fromBytes(value);
        }

        //maybe should throw
        return null;
    }
}

@:forward
@:forward.new
abstract MdnsTxt(MdnsTxtBase)
    from MdnsTxtBase to MdnsTxtBase
    to DynamicAccess<MdnsTxtValue>
{
    @:from
    public static function fromDynamic(dynamicValue: Dynamic): MdnsTxt{
        return fromDynamicAccess(dynamicValue);
    }

    @:from
    public static
    function fromDynamicAccess(dynamicAccess: DynamicAccess<Any>): MdnsTxt
    {
        if(dynamicAccess == null) return null;

        var txt = new MdnsTxt();

        for(key=>value in dynamicAccess.keyValueIterator()){
            txt.set(key, MdnsTxtValue.fromDynamic(value));
        }

        return txt;
    }

    public static function fromMap(map: Map<String, MdnsTxtValue>): MdnsTxt{
        var txt = new MdnsTxt();

        for(key => value in map.keyValueIterator()){
            txt.set(key, value);
        }

        return txt;
    }


    @:op(a.b)
    public function setField(key: String, value: MdnsTxtValue){
        return set(key, value);
    }

    @:arrayAccess
    public function set(key: String, value: MdnsTxtValue){
        if(value == null){
            this.remove(key);
            return null;
        }
        else{
            return this.set(key, value);
        }
    }

    public function equals(other: MdnsTxt) {
        if(this == null || other == null) return this == other;

        //TO-DO: optimize (avoid keys duplication)
        var allKeys = [for(k in this.keys()) k].concat(
                      [for(k in other.keys()) k]);

        for(key in allKeys){
            var value      = this.get(key);
            var othervalue = other.get(key);

            if(!valuesAreEquals(value, othervalue)){
                return false;
            }
        }

        return true;
    }
    public function valuesAreEquals(value:MdnsTxtValue, other: MdnsTxtValue){
        if(value == other) return true;
        if(value == null || other == null) return false;

        return Binary.areEquals(toBinary(value), toBinary(other));
    }

    function toBinary(value:MdnsTxtValue):Binary{
        if(Std.isOfType(value, BinaryData)) return value;
        if(Std.isOfType(value, String)) return BinaryData.fromText(value);
        if(Std.isOfType(value, Bytes)) return Binary.fromBytes(value);

        return null;
    }
}