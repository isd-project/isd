package discovery.mdns.building;

import discovery.keys.keydiscovery.KeyTransport;
import discovery.mdns.pki.MdnsKeyTransportBuilder;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.utils.dependencies.DependenciesDecorator;
import discovery.base.configuration.DiscoveryMechanismBuilder;

import discovery.keys.crypto.KeyLoader;
import discovery.async.promise.PromisableFactory;
import discovery.keys.Keychain;
import discovery.keys.PKI;
import discovery.mdns.MdnsDiscovery;
import discovery.mdns.DnsSdEngine;
import discovery.mdns.MdnsEngine;
import discovery.mdns.externs.bonjour.BonjourMdnsEngine;
import discovery.mdns.externs.bonjour.BonjourDnsSdEngine;
import discovery.mdns.externs.bonjour.Bonjour;

typedef MdnsEngines = {
    mdns: MdnsEngine,
    dnsSd: DnsSdEngine
};

typedef NBonjourMechanismsDependencies = {
    promises: PromisableFactory,
    keyloader: KeyLoader,
    keycacheGetter: ()->Keychain
};


class NBonjourMechanismsBuilder
    implements DiscoveryMechanismBuilder
    implements KeyTransportBuilder
{
    var options:NBonjourMechanismsDependencies;
    var mdnsEngines: MdnsEngines;

    var pki:PKI;
    var mdnsDiscovery:MdnsDiscovery;

    var mdnsKeyTransportBuilder:MdnsKeyTransportBuilder;

    public function new(options: NBonjourMechanismsDependencies) {
        this.options = options;

        var deps = DependenciesDecorator.decorate(options);
        deps.requireAll();
    }


    @:deprecated
    public function discoveryMechanism(): DiscoveryMechanism {
        return buildMechanism();
    }


    public function buildMechanism():DiscoveryMechanism {
        if(mdnsDiscovery == null){
            mdnsDiscovery = new MdnsDiscovery(engines().dnsSd);
        }

        return mdnsDiscovery;
    }

    public function buildKeyTransport():KeyTransport {
        return keyTransportBuilder().buildKeyTransport();
    }

    function engines() {
        if(mdnsEngines == null){
            mdnsEngines = buildEngines();
        }

        return mdnsEngines;
    }

    function buildEngines(): MdnsEngines{
        var bonjour = Bonjour.create();
        var dnsSd = new BonjourDnsSdEngine(bonjour);
        var mdns  = new BonjourMdnsEngine(bonjour);

        return {
            mdns: mdns,
            dnsSd: dnsSd
        };
    }

    function keyTransportBuilder() {
        if(mdnsKeyTransportBuilder == null){
            this.mdnsKeyTransportBuilder = new MdnsKeyTransportBuilder({
                promises: options.promises,
                engine: engines().mdns,
                keyLoader: options.keyloader
            });
        }

        return mdnsKeyTransportBuilder;
    }
}