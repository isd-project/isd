package discovery.mdns.conversion;

import discovery.domain.Validity;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signature;
import haxe.extern.EitherType;
import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.signature.Signed.SignedData;
import discovery.mdns.MdnsTxt.MdnsTxtValue;
import haxe.io.Bytes;
import discovery.utils.traversal.ObjectVisitor;
import discovery.utils.traversal.ObjectTraversal;
import discovery.format.Binary;
import discovery.domain.Description;

using StringTools;
using discovery.utils.Cast;

class TxtBuilder{
    var txt:MdnsTxt;

    public function new() {
        this.txt = new MdnsTxt();
    }

    public function buildTxt(description:Description):MdnsTxt {
        return this.addDescription(description).build();
    }

    public function build(): MdnsTxt {
        var out = txt;
        clear();

        return out;
    }

    public function clear() {
        txt = new MdnsTxt();
    }

    public function addAnnouncement(announcement:Announcement): TxtBuilder {
        return addDescription(announcement.description)
                .addValidity(announcement.validity);
    }

    public function addValidity(validity:Validity): TxtBuilder{
        txt.set(TxtFields.validFrom, serializeDate(validity.validFrom));
        txt.set(TxtFields.validTo, serializeDate(validity.validTo));

        return this;
    }

    function serializeDate(date:Date):MdnsTxtValue {
        return Binary.fromDouble(date.getTime());
    }

    public function addDescription(description: Description): TxtBuilder{
        parseIdentity(description);
        parseLocation(description.location);
        parseAttributes(description.attributes);

        return this;
    }

    public function addSignature(
        signature: EitherType<Signed<Any>, Signature>): TxtBuilder
    {
        var signed:Signed<Any> = signature.castIfIs(SignedData);
        if(signed != null){
            signature = signed.signature;
        }

        includeSignature(Signature.fromDynamic(signature));

        return this;
    }

    function includeSignature(signature: Signature){
        if(signature == null) return;

        txt.set(TxtFields.signature, signature);
    }

    function parseIdentity(description:Description) {
        txt.set(TxtFields.name, description.name);
        txt.set(TxtFields.type, description.type);
        txt.set(TxtFields.instanceId, description.instanceId);

        if(description.providerId != null){
            txt.set(TxtFields.providerId, description.providerId.id());
            txt.set(TxtFields.providerIdHashAlg,
                        description.providerId.hashAlg());
        }
    }

    function parseLocation(location:Location) {
        if(location == null) return;

        if(location.defaultPort != null){
            txt.set(TxtFields.defaultPort, Std.string(location.defaultPort));
        }

        putListAsString(TxtFields.addresses, location.addresses);
        putListAsString(TxtFields.channels , location.channels);
    }

    function putListAsString<T>(name:String, items:Array<T>) {
        if(items == null){
            return;
        }

        for (i in 0...items.length){
            txt.set('$name+$i', Std.string(items[i]));
        }
    }

    function parseAttributes(attrs:Attributes) {
        if(attrs == null) return;

        var visitor = new MdnsTxtVisitor(txt, TxtFields.attributes);
        var traversal = new ObjectTraversal(visitor);
        traversal.transform(BinaryData, (binary:BinaryData)->binary.toBytes());
        traversal.traverse(attrs);
    }
}


class MdnsTxtVisitor implements ObjectVisitor{
    var txt:MdnsTxt;

    var prefixers:Array<Prefixer>;

    public function new(txt:MdnsTxt, ?prefix:String) {
        this.txt = txt;
        prefixers = [];

        if(prefix != null){
            pushPrefixer(new Prefixer(prefix));
        }
    }

    function put(key:String, value:MdnsTxtValue) {
        txt.set(getName(key), value);
    }

    public function onValue(key:String, value:Dynamic):VisitStatus {
        return Continue;
    }

    public function onNull(key:String) {}

    public function onInt(key:String, value:Int) {
        put(key, Binary.fromInt(value));
    }

    public function onFloat(key:String, value:Float) {
        put(key, Binary.fromFloat(value));
    }

    public function onBool(key:String, value:Bool) {}

    public function onString(key:String, value:String) {
        put(key, value);
    }

    public function onBytes(key:String, value:Bytes) {
        put(key, Binary.fromBytes(value));
    }

    public function onEnum(key:String, e:EnumValue, enumType:Enum<Any>) {}

    public function onStartObject(key:String, value:Dynamic, ?classObj:Class<Any>) {
        pushPrefixer(new Prefixer(getName(key)));
    }

    public function onEndObject(key:String, value:Dynamic, ?classObj:Class<Any>) {
        popPrefixer();
    }

    public function onStartArray(key:String, value:Array<Any>) {
        pushPrefixer(new ArrayPrefixer(getName(key)));
    }

    public function onEndArray(key:String, value:Array<Any>) {
        popPrefixer();
    }

    function pushPrefixer(prefixer: Prefixer) {
        this.prefixers.push(prefixer);
    }

    function popPrefixer() {
        prefixers.pop();
    }

    function getName(name:String):String {
        if(prefixers.length == 0){
            return name;
        }

        return prefixers[prefixers.length - 1].getName(name);
    }
}

class Prefixer {
    var prefix:String;

    public function new(prefix:String) {
        this.prefix = prefix;
    }

    public function getName(name:String): String {
        return '$prefix.$name';
    }
}

class ArrayPrefixer extends Prefixer{
    var counter:Int;

    public function new(prefix:String) {
        super(prefix);

        counter = 0;
    }

    override function getName(name:String):String {
        var itemName = '$prefix+$counter';
        ++counter;

        return itemName;
    }
}