package discovery.mdns.conversion;

import discovery.domain.Validity;
import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.Struct;
import discovery.mdns.MdnsTxt.MdnsTxtValue;
import discovery.format.Binary;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.HashTypes;
import discovery.domain.ProviderId;
import discovery.domain.Description;
import discovery.mdns.TxtFields;


class TxtParser {
    var description:Description;

    public function new() {

    }

    public function extractSignedAnnouncement(txt:MdnsTxt): Signed<Announcement>
    {
        var announcement = extractAnnouncement(txt);
        var signature    = extractSignature(txt);

        return Signed.make(announcement, signature);
    }

    public function extractAnnouncement(txt:MdnsTxt): Announcement {
        var desc = extractDescription(txt);
        var validity = extractValidity(txt);

        return Announcement.make(desc, validity);
    }

    function extractValidity(txt:MdnsTxt): Validity {
        var validFrom = extractDate(txt, TxtFields.validFrom);
        var validTo   = extractDate(txt, TxtFields.validTo);

        if(validTo == null && validFrom == null){
            return null;
        }

        return new Validity(validTo, validFrom);
    }

    function extractDate(txt:MdnsTxt, key:String) {
        var dateBin = getBinary(txt, key);

        if(dateBin == null) return null;

        return Date.fromTime(dateBin.toDouble());
    }

    public function extractSignedDescription(txt:MdnsTxt):Signed<Description> {
        var description = extractDescription(txt);
        var signature   = extractSignature(txt);

        return Signed.make(description, signature);
    }

    function extractSignature(txt:MdnsTxt): Signature{
        return getBinary(txt, TxtFields.signature);
    }

    public function extractDescription(txt:MdnsTxt):Description {
        var deserialized:Struct = new TxtDeserializer(txt).deserialize();

        description = parseIdentity(txt);
        description.location = parseLocation(deserialized);
        description.attributes = deserialized.tryGet(TxtFields.attributes, {});

        return description;
    }

    function parseIdentity(txt:MdnsTxt):Description {
        return {
            name      : getText(txt, TxtFields.name),
            type      : getText(txt, TxtFields.type),
            instanceId: getValue(txt, TxtFields.instanceId),
            providerId: parseProviderId(txt)
        };
    }

    function parseProviderId(txt:MdnsTxt):Null<ProviderId> {
        if(txt == null) return null;

        var id = txt.get(TxtFields.providerId);
        var hashAlg = HashTypes.fromString(
                            getText(txt, TxtFields.providerIdHashAlg));

        if(id == null) return null;

        return new Fingerprint(hashAlg, id);
    }

    function parseLocation(txt:Struct):Location {
        var portBin = Binary.fromDynamic(txt.get(TxtFields.defaultPort));
        var port = if(portBin == null) null
                   else Std.parseInt(portBin.toUTF8());

        return {
            defaultPort: port,
            channels   : parseChannels(txt.get(TxtFields.channels)),
            addresses  : toStringArray(txt.get(TxtFields.addresses))
        };
    }

    function parseChannels(channelsStrings: Array<Dynamic>):Array<Channel> {
        if(channelsStrings == null) return null;

        channelsStrings = toStringArray(channelsStrings);

        return [for (c in channelsStrings) Channel.fromString(c)];
    }

    function getText(txt:MdnsTxt, key:String):String {
        var value = getValue(txt, key);

        if(value == null) return null;

        return value.toText();
    }

    function getBinary(txt: MdnsTxt, key: String): Binary{
        var value = getValue(txt, key);

        if(value != null){
            return value.toBinary();
        }

        return null;
    }

    function getValue(txt:MdnsTxt, key: String) {
        if(txt == null) return null;

        return txt.get(key);
    }

    function toStringArray(value:Null<Dynamic>):Null<Array<String>> {
        if(value == null) return null;
        if(!Std.isOfType(value, Array)) return null;

        var values:Array<Dynamic> = value;

        return [for(v in values) valueToString(v)];
    }

    function valueToString(value:Dynamic):String {
        var txtValue = MdnsTxtValue.fromDynamic(value);
        return if(txtValue == null) null else txtValue.toText();
    }

}
