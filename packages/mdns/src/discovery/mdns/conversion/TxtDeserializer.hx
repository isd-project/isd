package discovery.mdns.conversion;

import discovery.utils.traversal.ObjectTransformation;
import haxe.io.Bytes;
import discovery.utils.Comparator;
import discovery.domain.Struct;
import discovery.mdns.MdnsTxt.MdnsTxtValue;
import discovery.format.Binary;


private typedef TxtProperty = {
    key:String,
    value:Dynamic
};

private typedef ItemName = {
    name:String,
    subname:String,
    symbol:SymbolType
}

private enum SymbolType{
    ObjectItem;
    ArrayItem;
    ValueItem;
}


class TxtDeserializer {
    var txt:MdnsTxt;
    var deserialized:Struct;

    public function new(txt: MdnsTxt) {
        this.txt = txt;
        this.deserialized = new Struct();
    }

    public function deserialize(): Struct {
        for (key=>value in txt.keyValueIterator()){
            setProperty(key, value, deserialized);
        }

        applyTransformations(deserialized);

        return deserialized;
    }

    function setProperty(key:String, value:MdnsTxtValue, out:Struct) {
        var prop = parseProperty(key, value, out);

        out.set(prop.key, prop.value);
    }

    function insertItem(key:String, value:MdnsTxtValue, array:SparseArray<Dynamic>)
    {
        var prop = parseProperty(key, value, array);

        var pos = Std.parseInt(prop.key);
        array.set(pos, prop.value);
    }

    function parseProperty(
        key:String, value:MdnsTxtValue, out:Dynamic): TxtProperty
    {
        var itemName = splitNameAndSymbol(key);

        switch (itemName.symbol){
            case ObjectItem: return parseObjProperty(itemName, value, out);
            case ArrayItem:  return parseArrayProperty(itemName, value, out);
            default:         return parseItemProperty(itemName, value, out);
        }
    }

    function splitNameAndSymbol(key:String): ItemName {
        for(i in 0...key.length){
            switch (key.charCodeAt(i)){
                case '.'.code: return splitNameAt(i, key, ObjectItem);
                case '+'.code: return splitNameAt(i, key, ArrayItem);
                default: continue;
            }
        }

        return {name:key, symbol: ValueItem, subname: ''};
    }

    function splitNameAt(i:Int, key:String, symbol:SymbolType):ItemName {
        return {
            name: key.substr(0, i),
            symbol: symbol,
            subname: key.substr(i+1)
        };
    }


    function parseObjProperty(
        itemName:ItemName, value:MdnsTxtValue, out:Dynamic)
    {
        var obj = getObj(out, itemName.name);
        setProperty(itemName.subname, value, obj);

        return {key:itemName.name, value: obj};
    }

    function parseArrayProperty(
        itemName:ItemName, value:MdnsTxtValue, out:Dynamic)
    {
        var array = getArray(out, itemName.name);
        insertItem(itemName.subname, value, array);

        return {key:itemName.name, value: array};
    }

    function parseItemProperty(
        itemName:ItemName, value:MdnsTxtValue, out:Dynamic):TxtProperty
    {
        var itemValue:Dynamic = if(value.isText()) value.toString()
                                else value.toBinary();
        return {key:itemName.name, value: itemValue};
    }

    function getObj(parent:Dynamic, key:String): Struct {
        return getItemFromParent(parent, key, new Struct());
    }

    function getArray(parent:Dynamic, name:String):SparseArray<Dynamic> {
        return getItemFromParent(parent, name, new SparseArray());
    }

    function getItemFromParent(
        parent:Dynamic, key:String, defaultValue: Dynamic): Dynamic
    {
        var obj:Dynamic = null;

        if(Std.isOfType(parent, SparseArray)){
            obj = getItemFromArray(parent, key);
        }
        else{
            obj = getItemFromObj(parent, key);
        }

        return if(obj != null) obj else defaultValue;
    }

    function getItemFromArray(parent:SparseArray<Dynamic>, key:String):Dynamic {
        var idx = Std.parseInt(key);

        return parent.get(idx);
    }

    function getItemFromObj(parent:Struct, key:String){
        return parent.get(key);
    }

    // --------------------------------------------------------------

    /**
        Convert internal representations of arrays into real arrays.

        Internally, we use 'sparseArrays' to prevent an error (or malicious)
        txt that specify an array index too large.
    **/
    function applyTransformations(deserialized:Struct) {
        deserialized = ObjectTransformation
                .doTransformation(SparseArray, convertSparseArrays)
                .transform(Bytes, Binary.fromBytes)
                .apply(deserialized);
    }

    function convertSparseArrays(sparseArray:SparseArray<Any>){
        return sparseArray.toArray();
    }
}

class SparseArray<T> {

    var items:Map<Int, T>;

    public function new() {
        items = new Map();
    }

    public function get(idx: Int) {
        return items.get(idx);
    }


    public function set(idx: Int, value:T) {
        items.set(idx, value);
    }

    public function toArray(): Array<T> {
        var keys = [for(k in items.keys()) k];
        keys.sort(Comparator.compareInt);

        return [for(k in keys) get(k)];
    }
}