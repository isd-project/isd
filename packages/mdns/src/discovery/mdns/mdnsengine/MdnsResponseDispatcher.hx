package discovery.mdns.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.externs.multicastdns.MulticastDnsResponses;
import discovery.mdns.dns.DnsQuery;

class MdnsResponseDispatcher extends MdnsQueryDispatcher<DnsResponse>{

    public function onResponse(responsePacket:MulticastDnsResponses) {
        for (record in responsePacket.answers){
            var query = new DnsQuery(record.name, record.type);

            notify(query, new DnsResponse(record.toDnsRecord()));
        }
    }
}