package discovery.mdns.mdnsengine;

import discovery.utils.events.EventEmitterFactory;
import discovery.utils.events.EventEmitter;
import discovery.mdns.dns.DnsQuery;


class MdnsQueryDispatcher<T> {

    var queryToEvent:QueryMap<EventEmitter<T>>;
    var eventsFactory: EventEmitterFactory;

    public function new(?eventsFactory: EventEmitterFactory) {
        if(eventsFactory == null){
            eventsFactory = new discovery.impl.Defaults.EventEmitterFactory();
        }
        this.eventsFactory = eventsFactory;

        queryToEvent = new QueryMap();
    }

    public function listen(dnsQuery:DnsQuery, listener:(T)->Void) {
        getEvent(dnsQuery).listen(listener);
    }

    public function unlisten(dnsQuery:DnsQuery, listener:(T)->Void) {
        getEvent(dnsQuery).unlisten(listener);
    }

    public function notify(query:DnsQuery, value:T) {
        getEvent(query).notify(value);
    }

    function getEvent(dnsQuery:DnsQuery): EventEmitter<T> {
        var matcher = new QueryMatcher(queryToEvent, dnsQuery);
        var evt = matcher.match();

        if(evt == null){
            evt = eventsFactory.eventEmitter();
            queryToEvent.set(dnsQuery, evt);
        }

        return evt;
    }
}


class QueryMatcher<T>{
    var queryMap:QueryMap<T>;
    var query:DnsQuery;
    var queryTmp:DnsQuery;

    var nameParts:Array<String>;
    var wildcardPrefix:String;

    var matched:Null<T>;

    public function new(queryMap: QueryMap<T>, query:DnsQuery) {
        this.queryMap = queryMap;
        this.query = query;
        this.queryTmp = query.clone();

    }

    public function match(): Null<T> {
        if(matched != null) return matched;

        matched = queryMap.get(query);

        if(matched != null){
            return matched;
        }
        else{
            return prefixMatch();
        }
    }

    function prefixMatch() {
        setup();

        while(!found() && nameParts.length > 0){
            matchNext();

            nextPrefix();
        }

        return matched;
    }

    function setup() {
        nameParts = query.name.split('.');
        nameParts.shift();

        wildcardPrefix = '*';
    }

    inline function found() {
        return matched != null;
    }

    function matchNext(){
        matched = getWithPrefix(wildcardPrefix);

        if(matched == null){
            matched = getWithPrefix('**');
        }
    }

    function nextPrefix() {
        wildcardPrefix += '.*';
        nameParts.shift();
    }

    inline function getWithPrefix(prefix:String)
    {
        queryTmp.name = joinName(prefix);

        return queryMap.get(queryTmp);
    }

    inline function joinName(prefix: String){
        return prefix + '.' + nameParts.join('.');
    }

}