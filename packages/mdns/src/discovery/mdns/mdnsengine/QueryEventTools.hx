package discovery.mdns.mdnsengine;

import discovery.mdns.DnsRecord.ResponseRecordFields;
import discovery.mdns.dns.DnsResponse;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;

class QueryEventTools {
    public static function
        respondWith(query: QueryEvent, responseFields:ResponseRecordFields)
    {
        var response = DnsResponse.makeFromQuery(query.query(), responseFields);

        query.respond(response);
    }
}