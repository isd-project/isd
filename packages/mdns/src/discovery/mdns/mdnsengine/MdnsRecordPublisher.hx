package discovery.mdns.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import discovery.mdns.externs.multicastdns.MulticastDnsResponses;
import discovery.mdns.externs.multicastdns.MulticastDnsQueries;
import discovery.mdns.externs.multicastdns.MulticastDns;
import discovery.mdns.DnsRecord;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.externs.multicastdns.MulticastDnsRecord;
import discovery.mdns.externs.bonjour.MdnsServer;


class MdnsRecordPublisher {
    var mdnsServer: MdnsServer;
    var multicastDns: MulticastDns;

    var dispatcher:MdnsQueryDispatcher<QueryEvent>;

    public function new(mdnsServer: MdnsServer, multicastDns: MulticastDns) {
        this.mdnsServer = mdnsServer;
        this.multicastDns = multicastDns;
        this.dispatcher = new MdnsQueryDispatcher();

        multicastDns.on('query', onMdnsQuery);
    }

    public function publishRecord(record:DnsRecord) {
        publishRecords([record]);
    }

    public function publishRecords(records:Array<DnsRecord>) {
        this.mdnsServer.register([
            for(r in records) MulticastDnsRecord.fromDnsRecord(r)
        ]);
    }

    public function onQuery(listeningQuery:DnsQuery, handler: QueryHandler) {
        this.dispatcher.listen(listeningQuery, handler);
    }

    function onMdnsQuery(mdnsQueryPacket: MulticastDnsQueryPacket) {
        if(mdnsQueryPacket == null){
            return;
        }

        var mdnsQueries = MulticastDnsQueries.fromPacket(mdnsQueryPacket);

        for(query in mdnsQueries.questions){
            dispatcher.notify(query, makeQueryEvent(query));
        }
    }

    function makeQueryEvent(query:DnsQuery):QueryEvent {
        return new MdnsQueryEvent(query, multicastDns);
    }
}

class MdnsQueryEvent implements QueryEvent{
    var dnsQuery:DnsQuery;
    var multicastDns: MulticastDns;

    public function new(query: DnsQuery, multicastDns: MulticastDns) {
        this.dnsQuery = query;
        this.multicastDns = multicastDns;
    }

    public function query():DnsQuery {
        return dnsQuery;
    }

    public function respond(response:DnsResponse) {
        multicastDns.respond(MulticastDnsResponses.fromResponse(response));
    }
}