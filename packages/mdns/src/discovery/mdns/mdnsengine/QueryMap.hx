package discovery.mdns.mdnsengine;

import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.mdns.dns.DnsQuery;


class QueryMap<T>{
    var map:Map<DnsRecordType, Map<String, T>>;

    public function new() {
        map = new Map();
    }

    public function get(query: DnsQuery): T {
        var value = getValue(query.type, query.name);

        if(value == null){
            value = getValue(ANY, query.name);
        }

        return value;
    }

    public function set(query: DnsQuery, value: T) {
        var type = if(query.type == null) ANY else query.type;

        getMap(type).set(query.name, value);
    }

    function getValue(type: DnsRecordType, name: String): T{
        return getMap(type).get(name);
    }

    function getMap(type: DnsRecordType) {
        var typeMap = map.get(type);
        if(typeMap == null){
            typeMap = new Map();
            map.set(type, typeMap);
        }

        return typeMap;
    }
}