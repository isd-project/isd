package discovery.mdns.mdnsengine;

import discovery.mdns.dns.DnsResponse;
import discovery.mdns.dns.DnsQuery;

interface QueryEvent{
    function query(): DnsQuery;
    function respond(response: DnsResponse): Void;
}
typedef QueryHandler = (QueryEvent)->Void;