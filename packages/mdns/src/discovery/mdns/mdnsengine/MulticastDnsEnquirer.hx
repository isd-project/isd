package discovery.mdns.mdnsengine;

import discovery.base.search.Stoppable;
import discovery.mdns.externs.multicastdns.MdnsResponseInfo.MdnsRemoteInfo;
import discovery.mdns.externs.multicastdns.MulticastDnsResponses;
import discovery.mdns.MdnsEngine.OnErrorCallback;
import discovery.mdns.MdnsEngine.OnDnsResponse;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.externs.multicastdns.MulticastDns;

class MulticastDnsEnquirer {
    var multicastDns:MulticastDns;
    var responseDispatcher:MdnsResponseDispatcher;

    public function new(multicastDns: MulticastDns) {
        this.multicastDns = multicastDns;
        this.multicastDns.on("response", onResponse);

        responseDispatcher = new MdnsResponseDispatcher();
    }

    function onResponse(packet: MulticastDnsResponses, rinfo: MdnsRemoteInfo) {
        responseDispatcher.onResponse(packet);
    }

    public function query(
        query:DnsQuery, onResponse:OnDnsResponse, ?onError:OnErrorCallback): Stoppable
    {
        responseDispatcher.listen(query, onResponse);
        multicastDns.query(query);

        return new QueryStopListening(responseDispatcher, query, onResponse);
    }
}

class QueryStopListening implements Stoppable{

    var responseDispatcher:MdnsResponseDispatcher;
    var query:DnsQuery;
    var listener:OnDnsResponse;

    public function new(
        dispatcher: MdnsResponseDispatcher,
        query:DnsQuery,
        listener: OnDnsResponse)
    {
        this.responseDispatcher = dispatcher;
        this.query = query;
        this.listener = listener;
    }

    public function stop(?callback:() -> Void) {
        if(responseDispatcher != null && query != null && listener != null){
            responseDispatcher.unlisten(query, listener);
        }

        if(callback != null){
            callback();
        }
    }
}