package discovery.mdns;

interface MdnsQueryBuilder {
    function buildQuery(): MdnsQuery;
}