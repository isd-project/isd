package discovery.mdns;

import discovery.domain.Announcement;
import discovery.keys.crypto.signature.Signed;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import haxe.DynamicAccess;
import haxe.Exception;

import discovery.base.publication.PublicationControl;
import discovery.domain.PublishInfo;
import discovery.domain.PublishListeners;
import discovery.exceptions.PublicationException;
import discovery.format.Formatters;
import discovery.mdns.externs.bonjour.BonjourService;

import Assertion.*;

private typedef Txt = DynamicAccess<Dynamic>;

class MdnsPublicationControl implements PublicationControl{
    var mdns:DnsSdEngine;
    var service:BonjourService;
    var listeners:PublishListeners;
    var onStopCallbacks:Array<()->Void>;

    public function new(mdns: DnsSdEngine, ?listeners:PublishListeners) {
        this.mdns = mdns;
        this.listeners = listeners;

        onStopCallbacks = [];
    }

    public function publish(info:PublishInfo): PublicationControl {
        var description = info.description();

        var publishData:MdnsPublishData = {
            type    : description.type,
            name    : description.name,
            port    : description.port,
            txt     : makeServiceTxt(info.signed()),
            subtypes: makeServiceSubtypes(info)
        };

        publishService(publishData);

        return this;
    }

    function publishService(publishData:MdnsPublishData){
        service = mdns.publish(publishData);

        service.on("up", this.onUp);
        service.on("error", this.onError);
    }

    function onUp() {
        if(listeners != null && listeners.onPublish != null){
            listeners.onPublish();
        }
    }

    function onError(err:Dynamic) {
        var exception = new PublicationException("Publication received an error", err);
        finish(exception);
    }

    public function stop(?callback: ()->Void) {
        assert(service != null);

        if(callback != null){
            onStopCallbacks.push(callback);
        }
        service.stop(onStop);
    }

    function onStop(){
        for (callback in onStopCallbacks){
            callback();
        }
        //remove all
        onStopCallbacks.resize(0);

        finish();
    }

    function finish(?err: Exception) {
        if(listeners != null && listeners.onFinish != null){
            listeners.onFinish(err);
        }
    }

    function makeServiceTxt(announcement: Signed<Announcement>):MdnsTxt {
        return MdnsTxtConverter.builder()
                .addAnnouncement(announcement)
                .addSignature(announcement)
                .build();
    }

    function makeServiceSubtypes(info: PublishInfo):Null<Array<String>> {
        var subtypes:Array<String> = null;

        var providerId = info.getProviderId();

        if(providerId != null){
            var sub = Formatters.base32.encode(providerId).substr(0, 63);
            subtypes = [sub];
        }

        return subtypes;
    }
}