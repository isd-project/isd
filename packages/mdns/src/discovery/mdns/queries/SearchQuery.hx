package discovery.mdns.queries;

import discovery.domain.Query;


class SearchQuery implements MdnsQueryBuilder{
    var query: Query;

    public function new(query: Query) {
        this.query = query;
    }

    public function buildQuery():MdnsQuery {
        return {
            type: query.type
        };
    }
}