package discovery.mdns.queries;

import discovery.format.Formatters;
import discovery.exceptions.IllegalArgumentException;
import discovery.domain.Identifier;
import discovery.mdns.externs.bonjour.Bonjour;

class LocateQuery implements MdnsQueryBuilder{
    var identifier:Identifier;

    public function new(identifier:Identifier) {
        IllegalArgumentException
            .verify(identifier != null, "identifier", "its null");
        IllegalArgumentException
            .verify(identifier.providerId != null, "identifier", "the providerId is null");

        this.identifier = identifier;
    }

    public function buildQuery():MdnsQuery {
        var idSubtype = Formatters.base32.encode(identifier.providerId).substr(0, 63);

        return {
            type: identifier.type,
            subtypes: [idSubtype]
        };
    }
}