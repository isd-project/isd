package discovery.mdns.pki.search;

import discovery.base.search.Stoppable;
import discovery.async.promise.Promisable;
import discovery.format.Binary;
import discovery.base.search.SearchControl;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.Fingerprint;

typedef KeyDescriptorFound = {
    descriptor: KeyDescriptor
};

typedef ExportedKeyFound = {
    found: ExportedKey
};

typedef KeyPart = {
    data: Binary
};

interface KeyEnquirer {
    function searchKeyDescriptor(fingerprint: Fingerprint,
                                listener   : QueryListener<KeyDescriptorFound>): Stoppable;

    function getKeyPart(keyPart:KeyPartReference): Promisable<KeyPart>;
}