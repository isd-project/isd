package discovery.mdns.pki.search;

import discovery.mdns.pki.messages.KeyPartReference;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.pki.search.KeyEnquirer;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.keys.crypto.ExportedKey;
import discovery.async.promise.Promisable;

typedef KeyDataRetrieverDeps = {
    enquirer: KeyEnquirer,
    promises: PromisableFactory
};

class KeyDataRetriever{
    var keyDescriptor: KeyDescriptor;
    var deps: KeyDataRetrieverDeps;

    var partsCollector:KeyPartCollector;

    public function new(keyDescriptor: KeyDescriptor,
                        deps: KeyDataRetrieverDeps)
    {
        this.keyDescriptor = keyDescriptor;
        this.deps = deps;

        this.partsCollector = new KeyPartCollector(keyDescriptor);
    }

    public function getKeyData():Promisable<ExportedKey> {
        function getNextPart(): Promisable<ExportedKey> {
            return keyEnquirer()
                .getKeyPart(currentPart())
                .then(addPart)
                .then((_)->{
                    if(hasAllParts()){
                        return promises().resolved(joinParts());
                    }
                    else return getNextPart();
                });
        }


        return getNextPart();
    }

    function currentPart():KeyPartReference {
        return partsCollector.nextPartRef();
    }

    function hasAllParts() {
        return partsCollector.hasAllParts();
    }

    function addPart(keyPart: KeyPart) {
        this.partsCollector.addPart(keyPart);
    }

    function joinParts(): ExportedKey {
        return partsCollector.joinParts();
    }

    public function stop() {}

    function keyEnquirer() {
        return deps.enquirer;
    }
    function promises() {
        return deps.promises;
    }
}