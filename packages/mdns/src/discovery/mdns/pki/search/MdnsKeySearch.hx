package discovery.mdns.pki.search;

import haxe.Exception;
import hx.concurrent.collection.SynchronizedLinkedList;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.crypto.ExportedKey;
import discovery.base.search.StopOnce;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.base.search.Stoppable;
import hx.concurrent.collection.Collection;
import discovery.async.promise.Promisable;
import discovery.mdns.pki.search.KeyRetrievalFactory;
import discovery.mdns.pki.search.KeyEnquirer.KeyDescriptorFound;
import discovery.keys.crypto.Fingerprint;

typedef MdnsKeySearchDeps = {
    enquirer: KeyEnquirer,
    retrievalFactory: KeyRetrievalFactory
};

class MdnsKeySearch implements Stoppable{
    var deps:MdnsKeySearchDeps;
    var listeners:SearchKeyListeners;

    var keyFingerprint:Fingerprint;

    var stopControl:StopOnce;
    var searchDescriptorsControl: Stoppable;

    var pendingKeyRetrievals: Collection<KeyDataRetriever>;


    public function new(deps: MdnsKeySearchDeps) {
        this.deps = deps;

        this.stopControl = makeStopControl();
        this.pendingKeyRetrievals = new SynchronizedLinkedList();
    }

    public function start(fing: Fingerprint, cb:SearchKeyListeners) {
        this.keyFingerprint = fing;
        this.listeners = cb;

        this.startKeyQuery();
    }

    function startKeyQuery() {
        this.searchDescriptorsControl =
            enquirer().searchKeyDescriptor(keyFingerprint, {
                onFound: onFoundKeyDescriptor
            });
    }

    function onFoundKeyDescriptor(keyDescriptor: KeyDescriptorFound) {
        if(stopped()) return;

        retrieveKey(keyDescriptor.descriptor).then((pubKeyData)->{
            notifyKeyFound(pubKeyData);
        });
    }

    function
        retrieveKey(keyDescriptor: KeyDescriptor): Promisable<PublicKeyData>
    {
        var retrieval = keyRetrievalFactory().dataRetriever(keyDescriptor);

        pendingKeyRetrievals.add(retrieval);

        return retrieval.getKeyData()
            .then(function(exportedKey:ExportedKey):PublicKeyData
        {
            return {
                fingerprint: keyDescriptor.fingerprint,
                keyType: keyDescriptor.keyType,
                key: exportedKey
            };
        }).finally(()->{
            pendingKeyRetrievals.remove(retrieval);
        });
    }


    function notifyKeyFound(pubKeyData:PublicKeyData) {
        if(listeners != null && listeners.onKey != null){
            listeners.onKey({ found: pubKeyData});
        }
    }

    // stop -------------------------

    function makeStopControl():StopOnce {
        var stopControl = new StopOnce(doStop);
        stopControl.onceStop(()->notifySearchEnd());

        return stopControl;
    }

    public function stop(?callback:StopCallback) {
        this.stopControl.stop(callback);
    }

    function doStop(?callback: StopCallback) {
        for (keyRetrieval in pendingKeyRetrievals){
            keyRetrieval.stop();
        }

        if(searchDescriptorsControl != null){
            searchDescriptorsControl.stop(callback);
        }
        else{
            callback();
        }
    }

    function notifySearchEnd(?err: Exception) {
        //TO-DO: check if was not called yet

        if(listeners != null && listeners.onEnd != null){
            listeners.onEnd(err);
        }
    }

    // properties ----------------------------------

    function stopped() {
        return stopControl.stopWasCalled();
    }

    function enquirer(): KeyEnquirer {
        return deps.enquirer;
    }

    function keyRetrievalFactory(): KeyRetrievalFactory{
        return deps.retrievalFactory;
    }
}