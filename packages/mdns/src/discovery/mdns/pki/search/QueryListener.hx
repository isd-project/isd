package discovery.mdns.pki.search;

typedef QueryListener<T> = {
    onFound: (T)->Void
};