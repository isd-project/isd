package discovery.mdns.pki.search;

interface KeyRetrievalFactory {
    function dataRetriever(keyDescriptor: KeyDescriptor): KeyDataRetriever;
}