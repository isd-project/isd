package discovery.mdns.pki.search.defaults;

import discovery.base.search.Stoppable;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.dns.DnsQuery;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.Fingerprint;
import discovery.mdns.pki.search.KeyEnquirer.KeyDescriptorFound;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.async.promise.Promisable;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;

class DefaultKeyEnquirer implements KeyEnquirer{
    var mdns:MdnsEngine;
    var pkiMessages:MdnsPkiMessages;
    var promises: PromisableFactory;

    public function new(mdns: MdnsEngine, promises: PromisableFactory) {
        this.mdns = mdns;
        this.promises = promises;

        pkiMessages = new MdnsPkiMessages();
    }

    public function searchKeyDescriptor(
        fingerprint:Fingerprint, listener:QueryListener<KeyDescriptorFound>):Stoppable
    {
        IllegalArgumentException.require(
            listener != null && listener.onFound != null,
            "Missing query listener");

        var query:DnsQuery = {
            name: pkiMessages.descriptorUrl(fingerprint),
            type: TXT
        };

        return mdns.query(query, (response)->{
            var recordData = response.record.data;
            var descriptor = pkiMessages.descriptorFromTxt(recordData);

            if(descriptor != null){
                listener.onFound({ descriptor: descriptor});
            }
        });
    }

    public function getKeyPart(keyPart:KeyPartReference):Promisable<KeyPart> {
        var query:DnsQuery = pkiMessages.keyPartQuery(keyPart);

        return promises.promise((resolve, reject)->{
            mdns.query(query, (response)->{
                resolve(pkiMessages.parseKeyPart(response.record.data));
            }, (err)->{
                reject(err);
            });
        });
    }
}