package discovery.mdns.pki.search.defaults;

import discovery.keys.crypto.KeyLoader;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.pki.search.KeyEnquirer;

using discovery.utils.dependencies.DependenciesVerifications;


typedef DefaultKeyRetrievalDependencies = {
    enquirer: KeyEnquirer,
    promises: PromisableFactory,
    keyloader: KeyLoader
};

class DefaultKeyRetrievalFactory implements KeyRetrievalFactory{
    var deps: DefaultKeyRetrievalDependencies;

    public function new(deps: DefaultKeyRetrievalDependencies) {
        deps.requireAll();

        this.deps = deps;
    }

    public function dataRetriever(keyDescriptor:KeyDescriptor):KeyDataRetriever
    {
        return new KeyDataRetriever(keyDescriptor, deps);
    }
}