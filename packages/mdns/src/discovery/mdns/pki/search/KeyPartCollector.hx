package discovery.mdns.pki.search;

import discovery.mdns.pki.messages.KeyPartReference;
import haxe.io.BytesBuffer;
import discovery.format.BytesOrText;
import discovery.keys.crypto.ExportedKey;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;

class KeyPartCollector {

    var keyDescriptor: KeyDescriptor;
    var parts:List<KeyPart>;

    public function new(keyDescriptor: KeyDescriptor) {
        this.keyDescriptor = keyDescriptor;
        this.parts = new List();
    }

    public function nextPartRef(): KeyPartReference {
        if(hasAllParts()) return null;

        return keyDescriptor.keyParts[numParts()];
    }

    function numParts() {
        return parts.length;
    }

    public function addPart(part:KeyPart) {
        this.parts.add(part);
    }

    public function hasAllParts(): Bool{
        return numParts() == requiredParts();
    }

    function requiredParts():Int {
        return keyDescriptor.keyParts.length;
    }

    public function joinParts(): ExportedKey {
        return {
            format: keyDescriptor.keyFormat,
            data: joinData()
        };
    }

    function joinData():BytesOrText {
        var buffer = new BytesBuffer();

        for(part in parts){
            buffer.add(part.data);
        }

        return buffer.getBytes();
    }
}