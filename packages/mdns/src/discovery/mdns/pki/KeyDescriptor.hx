package discovery.mdns.pki;

import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.KeyType;



@:structInit
class KeyDescriptor{
    public var keyType(default, default): KeyType;
    public var keyFormat(default, default): KeyFormat;
    public var keySize(default, default): Int;
    public var fingerprint(default, default): Fingerprint;
    public var keyParts(default, default): Array<KeyPartReference>;
}