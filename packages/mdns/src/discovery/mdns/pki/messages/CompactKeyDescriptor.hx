package discovery.mdns.pki.messages;

import discovery.exceptions.UnsupportedParameterException;
import discovery.keys.crypto.KeyTypes;
import discovery.format.Binary;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.KeyType;

typedef CompactKeyDescriptorData = {
    var kty: String;
    @:optional
    var kparams: Dynamic;
    var fmt: String;
    var size: Int;
    var fing: Binary;
    var fingalg: String;
    var parts: Array<String>;
};

abstract CompactKeyDescriptor(CompactKeyDescriptorData)
    from CompactKeyDescriptorData
    to CompactKeyDescriptorData
{
    @:from
    public static function
        fromDescriptor(descriptor:KeyDescriptor): CompactKeyDescriptor
    {
        var desc:CompactKeyDescriptorData = {
            kty: keyTypeToString(descriptor.keyType),
            kparams: keyTypeParams(descriptor.keyType),
            fmt: descriptor.keyFormat.getName(),
            size: descriptor.keySize,
            fing: descriptor.fingerprint.hash,
            fingalg: descriptor.fingerprint.alg,
            parts: compactKeyParts(descriptor.keyParts)
        };
        return desc;
    }

    @:to
    public function toDescriptor(): KeyDescriptor{
        var fingerprint = makeFingerprint();

        return {
            keySize: this.size,
            keyType: makeKeyType(),
            keyParts: makeKeyParts(fingerprint),
            keyFormat: KeyFormat.createByName(this.fmt),
            fingerprint: fingerprint
        };
    }

    function makeKeyType(): KeyType {
        return switch (this.kty){
            case 'EC': EllipticCurve(
                            EllipticCurveParameters.from(this.kparams));
            case 'RSA': RSA(RSAKeyParameters.from(this.kparams));
            default: throw new UnsupportedParameterException(
                'Unsupported or invalid key type: ${this.kty}'
            );
        }
    }

    function makeKeyParts(keyId: KeyPartKeyId): Array<KeyPartReference>{
        return [
            for(part in this.parts)
                KeyPartReference.make(part, keyId)
        ];
    }
    function makeFingerprint(): Fingerprint{
        return {
            alg: HashTypes.fromString(this.fingalg),
            hash: this.fing
        };
    }

    static function keyTypeToString(keyType:KeyTypes):String {
        return switch (keyType){
            case EllipticCurve(_): return 'EC';
            case RSA(_): 'RSA';
            default: throw new UnsupportedParameterException(
                        'Key type ${keyType} unsupported or invalid' );
        };
    }


    static function keyTypeParams(keyType:KeyType):Null<Dynamic> {
        var params = keyType.getParamsObject();
        return params;
    }

    static function
        compactKeyParts(parts:Array<KeyPartReference>):Array<String>
    {
        return [for(p in parts) p.part];
    }
}


