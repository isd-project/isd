package discovery.mdns.pki.messages;

import discovery.domain.ProviderId;

typedef KeyPartKeyId = ProviderId;
typedef PartRef = String;

@:structInit
class KeyPartReference
{
    public var part(default, null): PartRef;
    public var keyId(default, null): KeyPartKeyId;

    public static function make(part: PartRef, keyId: KeyPartKeyId){
        return new KeyPartReference(part, keyId);
    }

    public function new(part: PartRef, keyId: KeyPartKeyId) {
        this.part = part;
        this.keyId = keyId;
    }
}