package discovery.mdns.pki;

import discovery.utils.dependencies.DependenciesDecorator;
import discovery.keys.Keychain;
import discovery.mdns.pki.publish.MdnsKeyPublisher;
import haxe.exceptions.NotImplementedException;
import discovery.mdns.pki.search.MdnsKeySearch;
import discovery.keys.storage.PublicKeyData;
import discovery.async.DoneCallback;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.mdns.pki.search.KeyRetrievalFactory;
import discovery.mdns.pki.search.KeyEnquirer;

using discovery.utils.dependencies.DependenciesVerifications;


typedef MdnsKeyTransportDeps = {
    enquirer: KeyEnquirer,
    retrievalFactory: KeyRetrievalFactory,
    keyPublisher: MdnsKeyPublisher
};

class MdnsKeyTransport implements KeyTransport {
    var deps:MdnsKeyTransportDeps;

    public function new(deps: MdnsKeyTransportDeps) {
        deps.requireAll();

        this.deps = deps;
    }

    public function publishKeychain(keychain:Keychain, ?done: DoneCallback) {
        keyPublisher().publishKeychain(keychain, done);
    }

    public function publishKey(key:PublicKeyData, ?done:DoneCallback) {
        keyPublisher().publishKey(key, done);
    }

    public function
        searchKey(fing:Fingerprint, listeners:SearchKeyListeners):Stoppable
    {
        var keySearch = new MdnsKeySearch({
            enquirer: this.deps.enquirer,
            retrievalFactory: this.deps.retrievalFactory
        });

        keySearch.start(fing, listeners);

        return keySearch;
    }

    function keyPublisher() {
        return this.deps.keyPublisher;
    }
}