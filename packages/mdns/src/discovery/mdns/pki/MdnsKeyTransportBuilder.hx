package discovery.mdns.pki;

import discovery.base.configuration.AbstractGetter;
import discovery.mdns.pki.publish.RangeKeySplitStrategy;
import discovery.mdns.pki.publish.KeySplitStrategy;
import discovery.mdns.pki.publish.MdnsKeyPublisher;
import discovery.keys.crypto.KeyLoader;
import discovery.mdns.pki.search.defaults.DefaultKeyRetrievalFactory;
import discovery.mdns.pki.search.KeyEnquirer;
import discovery.mdns.pki.search.defaults.DefaultKeyEnquirer;
import discovery.async.promise.PromisableFactory;
import discovery.mdns.MdnsEngine;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.KeyTransportBuilder;

typedef MdnsKeyTransportBuilderDependencies = {
    ?promises:PromisableFactory,
    ?keyLoader:KeyLoader,
    ?engine:AbstractGetter<MdnsEngine>
};

class MdnsKeyTransportBuilder implements KeyTransportBuilder{

    var engineGetter:AbstractGetter<MdnsEngine>;
    var promises:PromisableFactory;
    var _keyLoader:KeyLoader;
    var enquirer:KeyEnquirer;
    var splitStrategy:KeySplitStrategy;

    var maxKeyPartLength:Null<Int>;

    public function new(?deps: MdnsKeyTransportBuilderDependencies) {
        if(deps == null) deps = {};

        this.engineGetter = deps.engine;
        this._keyLoader = deps.keyLoader;
        this.promises = deps.promises;
    }


    // configure -----------------------------------------------------------

    public function mdnsEngine(engine:AbstractGetter<MdnsEngine>) {
        this.engineGetter = engine;
        return this;
    }

    function getEngine():MdnsEngine {
        return engineGetter();
    }

    public function promisesFactory(promises:PromisableFactory) {
        this.promises = promises;
        return this;
    }

    public function keyLoader(keyLoader:KeyLoader) {
        this._keyLoader = keyLoader;
        return this;
    }

    public function keyPartLength(length: Int){
        this.maxKeyPartLength = length;
        return this;
    }

    // build -----------------------------------------------------------

    public function buildKeyTransport():KeyTransport {
        var engine = getEngine();

        return new MdnsKeyTransport({
            enquirer: getKeyEnquirer(engine),
            retrievalFactory: buildKeyRetrieval(engine),
            keyPublisher: buildKeyPublisher(engine)
        });
    }

    function getKeyEnquirer(engine: MdnsEngine) {
        if(enquirer == null){
            enquirer = new DefaultKeyEnquirer(engine, promises);
        }

        return enquirer;
    }

    function buildKeyRetrieval(engine: MdnsEngine) {
        return new DefaultKeyRetrievalFactory({
            enquirer: getKeyEnquirer(engine),
            keyloader: _keyLoader,
            promises: promises
        });
    }

    function buildKeyPublisher(engine: MdnsEngine):MdnsKeyPublisher {
        return new MdnsKeyPublisher(engine,
                getSplitStrategy(),
                promises);
    }

    function getSplitStrategy():KeySplitStrategy {
        if(splitStrategy != null) return splitStrategy;

        var splitStrategy = new RangeKeySplitStrategy(getKeyPartLength());

        return splitStrategy;
    }

    function getKeyPartLength():Int {
        if(maxKeyPartLength != null) return maxKeyPartLength;

        return MdnsPkiMessages.defaultKeyPartLength();
    }
}