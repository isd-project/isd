package discovery.mdns.pki.publish;

import discovery.keys.crypto.ExportedKey;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.mdns.pki.messages.KeyPartReference;

interface KeySplitStrategy {
    function partitionKey(
        key: ExportedKey, keyId: KeyPartKeyId): Array<KeyPartReference>;
    function extractKeyPart(key:ExportedKey, ref:PartRef):KeyPart;
}