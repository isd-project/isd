package discovery.mdns.pki.publish;

import discovery.mdns.pki.publish.rangesplitter.RangePartitionIterator;
import discovery.mdns.pki.publish.rangesplitter.Range;
import discovery.mdns.pki.publish.rangesplitter.KeyPartRange;
import discovery.format.exceptions.InvalidValueException;
import haxe.io.Bytes;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.crypto.ExportedKey;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.keys.storage.KeyData;
import discovery.mdns.pki.messages.KeyPartReference;

using discovery.keys.crypto.KeyFormatTools;


class RangeKeySplitStrategy implements KeySplitStrategy{
    var maxPartLength:Int;
    var pkiMessages:MdnsPkiMessages;

    public function new(maxPartLength: Int) {
        this.maxPartLength = maxPartLength;
        pkiMessages = new MdnsPkiMessages();
    }

    public function partitionKey(
        key:ExportedKey, keyId: KeyPartKeyId):Array<KeyPartReference>
    {
        var dataLength = keyLength(key);
        var partition  = new RangePartitionIterator(maxPartLength, dataLength);

        return [
            for(range in partition) makePart(range, key.format, keyId)
        ];
    }

    function keyLength(key:ExportedKey) {
        return key.data.toBytes().length;
    }

    function makePart(
        range:Range, format:KeyFormat, keyId:KeyPartKeyId):KeyPartReference
    {
        return {
            part: KeyPartRange.fromRange(range, format).toPart(),
            keyId: keyId
        };
    }



    public function extractKeyPart(key:ExportedKey, part:PartRef):KeyPart
    {
        var keyRange = KeyPartRange.fromPart(part);
        var keyData  = key.data.toBytes();

        validateKeyRange(keyRange, keyData);

        return {
            data: keyData.sub(keyRange.start, keyRange.length())
        };
    }

    function validateKeyRange(keyRange:KeyPartRange, keyData:Bytes) {
        if(keyRange.end >= keyData.length || keyRange.start < 0){
            var err = 'KeyRange: ${keyRange.start}:${keyRange.end} is outside bounds of key data.';

            throw new InvalidValueException(err);
        }
    }
}

