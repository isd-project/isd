package discovery.mdns.pki.publish.rangesplitter;

import discovery.exceptions.IllegalArgumentException;


class RangePartitionIterator{

    var range:Range;

    var maxPartSize: Int;
    var dataLength: Int;

    public function new(maxPartSize: Int, dataLength: Int) {
        IllegalArgumentException.require(maxPartSize > 0,
            'Invalid part size, should be greater than 0');
        IllegalArgumentException.require(dataLength > 0,
            'Invalid data length, should be greater than 0');

        this.range = Range.fromLength(0, maxPartSize);
        this.range.limit(dataLength - 1);

        this.maxPartSize = maxPartSize;
        this.dataLength = dataLength;
    }

    public function hasNext(): Bool {
        return range.start < dataLength;
    }

    public function next(): Range {
        var current = range.copy();

        range.move(maxPartSize);
        range.limit(dataLength - 1);

        return current;
    }
}