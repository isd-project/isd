package discovery.mdns.pki.publish.rangesplitter;

import discovery.keys.crypto.KeyFormat;

using discovery.keys.crypto.KeyFormatTools;


@:structInit
class KeyPartRange{
    public var start(get, never):Int;
    public var end(get, never):Int;
    public var format(default, null):KeyFormat;

    var range:Range;

    public static function fromRange(range: Range, format: KeyFormat){
        return new KeyPartRange(range.start, range.end, format);
    }

    public function new(start:Int, end:Int, format: KeyFormat) {
        this.range = new Range(start, end);
        this.format = format;
    }

    function get_start() {
        return range.start;
    }

    function get_end(){
        return range.end;
    }

    public static function fromPart(part: String): KeyPartRange{
        var number = '[0-9]+';
        var fmt = '[A-Za-z]+';
        var pattern = new EReg('(${number})-(${number})-(${fmt})', 'i');

        if(pattern.match(part)){
            return {
                start: Std.parseInt(pattern.matched(1)),
                end: Std.parseInt(pattern.matched(2)),

                //TO-DO: throw if not valid
                format: KeyFormat.getByName(pattern.matched(3))
            };
        }


        return null;
    };

    public function toPart(): String {
        return '${start}-${end}-${format}';
    }

    public function length():Int {
        return range.length();
    }
}