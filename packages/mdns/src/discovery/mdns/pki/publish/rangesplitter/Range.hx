package discovery.mdns.pki.publish.rangesplitter;

@:structInit
class Range{
    public var start(default, null):Int;
    public var end(default, null):Int;


    public static function fromLength(start:Int, length:Int): Range{
        return make(start, start + length - 1);
    }

    public static function make(start:Int, end:Int):Range {
        return new Range(start, end);
    }

    public function new(start:Int, end:Int) {
        this.start = start;
        this.end = end;
    }

    public function length():Int {
        return end - start + 1;
    }

    public function copy() {
        return make(start, end);
    }

    public function move(delta:Int) {
        start += delta;
        end += delta;
    }

	public function limit(maxEnd:Int) {
        this.end = Std.int(Math.min(this.end, maxEnd));
    }
}