package discovery.mdns.pki.publish;

import discovery.async.DoneCallback;
import discovery.keys.KeyIdentifier;
import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.keys.storage.PublicKeyData;
import discovery.mdns.pki.messages.KeyPartReference.PartRef;
import haxe.Rest;
import haxe.Exception;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.mdns.pki.MdnsPkiMessages.KeyQuery;
import discovery.exceptions.ClientErrorException;
import discovery.async.promise.PromisableFactory;
import discovery.async.promise.Promisable;
import discovery.keys.Key;
import discovery.keys.storage.exceptions.EntryNotFoundException;
import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import discovery.mdns.dns.DnsQuery;
import discovery.keys.Keychain;

import discovery.keys.KeyTransformations.*;

using discovery.mdns.mdnsengine.QueryEventTools;


class MdnsKeyPublisher {
    var mdns:MdnsEngine;
    var pkiMessages:MdnsPkiMessages;
    var splitStrategy: KeySplitStrategy;
    var descriptorBuilder:KeyDescriptorBuilder;
    var promises: PromisableFactory;

    var keysRepo:KeyRepository;

    var registeredListener:Bool = false;

    public function new(
        mdnsEngine: MdnsEngine,
        splitStrategy: KeySplitStrategy,
        promises: PromisableFactory)
    {
        this.mdns = mdnsEngine;
        this.splitStrategy = splitStrategy;
        this.promises = promises;

        this.pkiMessages = new MdnsPkiMessages();
        this.descriptorBuilder = new KeyDescriptorBuilder(splitStrategy);

        this.keysRepo = new KeyRepository(promises);
    }


    //TO-DO: allow 'unpublish' keychain
    public function publishKeychain(keychain:Keychain, ?done: DoneCallback) {
        keysRepo.registerKeychain(keychain);

        registerQueryListeners();
        notifyListener(done);
    }

    public function publishKey(pubKeyData:PublicKeyData, ?done:DoneCallback) {
        keysRepo.registerKey(pubKeyData);

        registerQueryListeners();
        notifyListener(done);
    }

    function registerQueryListeners() {
        if(registeredListener) return;
        registeredListener = true;

        mdns.onQuery(descriptorQuery(), onGetDescriptor);
        mdns.onQuery(getPartQuery(),    onGetKeyPart);
    }

    function notifyListener(?done:DoneCallback) {
        if(done != null){
            done();
        }
    }

    function descriptorQuery(){ return keyQuery('*');}
    function getPartQuery()   { return keyQuery('*', '*');}

    inline function keyQuery(rest: Rest<String>) {
        var domain = pkiMessages.keysUrl(...rest);

        return new DnsQuery(domain, TXT);
    }

    function onGetDescriptor(queryEvt: QueryEvent) {
        var keyQuery = parseKeyQuery(queryEvt);

        getPublicKeyData(keyQuery)
            .then(sendKeyDescriptor.bind(queryEvt, _))
            .catchError(ignoreError);
    }

    function onGetKeyPart(queryEvt: QueryEvent) {
        var keyQuery = parseKeyQuery(queryEvt);

        getPublicKeyData(keyQuery)
            .then(extractKeyPart.bind(_, keyQuery.part))
            .then(sendKeyPart.bind(_, queryEvt))
            .catchError(ignoreError);
    }

    function parseKeyQuery(queryEvt: QueryEvent){
        var keyQuery = pkiMessages.parseKeyQuery(queryEvt.query());

        if(keyQuery == null || keyQuery.keyId == null){
            return null;
        }

        return keyQuery;
    }

    function getPublicKeyData(keyQuery: KeyQuery): Promisable<PublicKeyData>
    {
        if(keyQuery == null){
            return promises.rejected(new ClientErrorException(
                'Invalid key query'));
        }

        //TODO: check key format (?)
        return keysRepo.getPublicKeyData(keyQuery.keyId);
    }

    function extractKeyPart(publicKey: PublicKeyData, keyPart: PartRef): KeyPart{
        return splitStrategy.extractKeyPart(publicKey.key, keyPart);
    }


    function sendKeyDescriptor(queryEvt: QueryEvent, pubKeyData: PublicKeyData) {
        var keyData = convertToKeyData(pubKeyData);
        //TO-DO: should use public key data directly
        var descriptor = descriptorBuilder.buildDescriptor(keyData);

        pkiMessages.respondWithDescriptor(queryEvt, descriptor);
    }

    function sendKeyPart(keyPart: KeyPart, queryEvt: QueryEvent){
        pkiMessages.respondWithKeyPart(queryEvt, keyPart);
    }


    function ignoreError(err: Exception)
    {}
}
