package discovery.mdns.pki.publish;

import discovery.exceptions.IllegalArgumentException;
import discovery.keys.storage.KeyData;

class KeyDescriptorBuilder {
    var spliStrategy:KeySplitStrategy;

    public function new(splitStrategy: KeySplitStrategy) {
        this.spliStrategy = splitStrategy;
    }

    public function buildDescriptor(keyData:KeyData): KeyDescriptor{
        IllegalArgumentException.require(keyData != null, "KeyData is null");
        IllegalArgumentException.require(keyData.publicKey != null,
            "KeyData has no public key");

        var data = keyData.publicKey.data.toBytes();
        var keyPartRefs = spliStrategy.partitionKey(
                                        keyData.publicKey, keyData.fingerprint);

        return {
            fingerprint: keyData.fingerprint,
            keyType: keyData.keyType,
            keySize: data.length,
            keyParts: keyPartRefs,
            keyFormat: keyData.publicKey.format
        };
    }
}