package discovery.mdns.pki.publish;

import discovery.async.DoneCallback;
import discovery.async.Future;
import discovery.utils.functional.CompositeDoneCallback;
import hx.concurrent.collection.SynchronizedLinkedList;
import hx.concurrent.collection.Collection;
import discovery.keys.KeyIdentifier;
import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.keys.storage.PublicKeyData;
import discovery.async.promise.PromisableFactory;
import discovery.async.promise.Promisable;
import discovery.keys.Key;
import discovery.keys.Keychain;

import discovery.keys.KeyTransformations.*;
using discovery.utils.IteratorTools;


class KeyRepository {
    var keychains:Collection<Keychain>;
    var promises:PromisableFactory;

    var publishedKeys: IMap<KeyIdentifier, PublicKeyData>;


    public function new(promises:PromisableFactory) {
        this.promises = promises;

        publishedKeys = new ComparableMap();
        keychains     = new SynchronizedLinkedList();
    }


    public function registerKeychain(keychain:Keychain) {
        this.keychains.addIfAbsent(keychain);
    }

    public function registerKey(key:PublicKeyData) {
        publishedKeys.set(key.fingerprint.toFingerprint(), key);
    }

    //TO-DO: allow set format of exported key (?)
    public function getPublicKeyData(keyId: KeyIdentifier): Promisable<PublicKeyData>
    {
        var key = tryGetPublicKey(keyId);
        if(key != null){
            return promises.resolved(key);
        }

        //TO-DO: use cache to avoid retrieving and re-exporting the key each time
        return getKeyFromKeychain(keyId)
                    .then(exportToPubKeyData.bind(_, keyId));
    }

    function tryGetPublicKey(keyId:KeyIdentifier): Null<PublicKeyData> {
        return publishedKeys.get(keyId);
    }

    function getKeyFromKeychain(keyId: KeyIdentifier) {
        var getKeyComposite = new GetKeyComposite(promises);

        return getKeyComposite.getKey(keyId, keychains);
    }


    function exportToPubKeyData(key: Key, keyId: KeyIdentifier): Promisable<PublicKeyData>
    {
        if(key == null){
            return promises.resolved(null);
        }

        return toKeyData(key, Der)
                .then(toPublicKeyData);
    }
}

class GetKeyComposite{
    var promises: PromisableFactory;
    var keychains: Array<Keychain>;

    var keyId:KeyIdentifier;

    var resultFuture:Future<Key>;

    public function new(promises: PromisableFactory) {
        this.promises = promises;
        this.resultFuture = new Future();
    }

    public
    function getKey(keyId: KeyIdentifier, keychainsIterable: Iterable<Keychain>)
    {
        this.keyId = keyId;
        this.keychains = keychainsIterable.toArray();

        start();

        return futureToPromise(resultFuture, promises);
    }

    function start() {
        var composite = CompositeDoneCallback.make(keychains.length, (?err)->{
            resultFuture.notify(null, err);
        });

        keychains.forEach((k)->{
            retrieveKey(k, composite);
        });
    }

    function retrieveKey(keychain:Keychain, done:DoneCallback) {
        keychain.getKey(keyId).then((key)->{
            if(key != null){
                resultFuture.notify(key);
            }

            done();
        }, done);
    }

    function futureToPromise(future:Future<Key>, promises:PromisableFactory) {
        return promises.promise((resolve, reject)->{
            future.onResult((key, ?error)->{
                if(error != null){
                    reject(error);
                }
                else{
                    resolve(key);
                }
            });
        });
    }
}