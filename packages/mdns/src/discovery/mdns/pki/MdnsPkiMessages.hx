package discovery.mdns.pki;

import discovery.mdns.mdnsengine.QueryHandler.QueryEvent;
import discovery.mdns.pki.messages.CompactKeyDescriptor;
import discovery.mdns.pki.messages.KeyPartReference;
import discovery.keys.KeyIdentifier;
import discovery.mdns.dns.DnsQuery;
import haxe.Rest;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import haxe.io.Bytes;
import discovery.keys.crypto.KeyTypes;
import discovery.keys.crypto.KeyFormat;
import discovery.format.parsers.JsonParser;
import discovery.format.Binary;
import discovery.format.Formatters;
import discovery.keys.crypto.Fingerprint;

using discovery.mdns.mdnsengine.QueryEventTools;
using StringTools;


typedef KeyQuery = {
    //TO-DO: should use fingerprint (?)
    keyId: KeyIdentifier,
    ?part: String
};


class MdnsPkiMessages {
    public function new() {

    }

    public function keysUrl(parts: Rest<String>): String {
        var partsText = parts.toArray().join('.');

        return '${partsText}._keys.local';
    }


    public function descriptorUrl(fingerprint: Fingerprint): String {
        var fingBase32 = Formatters.base32.encode(fingerprint.hash);

        return keysUrl(fingBase32);
    }


    public function parseKeyQuery(query:DnsQuery): KeyQuery {
        var validWord = '([a-zA-Z0-9\\-_\\(\\)]+)';
        var validKeyId = validWord;
        var validPart  = '(${validWord}\\.)?';

        var pattern = validPart + keysUrl(validKeyId).replace('.','\\.');
        var regex   = new EReg(pattern, "i");

        if(!regex.match(query.name)){
            return null;
        }

        var part  = regex.matched(2);
        var keyId = regex.matched(3);

        return {
            keyId: Formatters.base32.decode(keyId),
            part : part
        };
    }

    public function descriptorTxt(descriptor:KeyDescriptor):Binary {
        var compact:CompactKeyDescriptor = descriptor;
        return Binary.fromText(descriptorToJson(descriptor));
    }

    function descriptorToJson(descriptor:CompactKeyDescriptor):String {
        return new JsonParser().stringify(descriptor, jsonValueTransformer);
    }

    public function descriptorFromTxt(txtData: Binary): KeyDescriptor {
        var txtText = txtData.toUTF8();
        var jsonValue = new JsonParser().parse(txtText);

        return parseCompactDescriptor(jsonValue).toDescriptor();
    }

    function parseCompactDescriptor(jsonValue: Dynamic): CompactKeyDescriptor {
        var desc:CompactKeyDescriptorData = {
            kty: jsonValue.kty,
            kparams: jsonValue.kparams,
            parts: jsonValue.parts,
            fing: parseBinary(jsonValue.fing),
            fingalg: jsonValue.fingalg,
            size: jsonValue.size,
            fmt: jsonValue.fmt
        };
        return desc;
    }

    function jsonValueTransformer(key:String, value:Dynamic): Dynamic{
        if(isEnum(value)){
            var enumValue:EnumValue = value;
            return {
                name: enumValue.getName(),
                parameters: enumValue.getParameters()
            };
        }
        return value;
    }

    function isEnum(value: Dynamic):Bool{
        return switch (Type.typeof(value)){
            case TEnum(_): true;
            default:       false;
        };
    }

    function parseEnum<T>(value: Dynamic, enumType:Enum<T>):T {
        if(value == null) return null;

        return enumType.createByName(value.name, value.parameters);
    }

    function parseFingerprint(value: Dynamic):Fingerprint {
        if(value == null) return null;

        return {
            alg: value.alg,
            hash: parseBinary(value.hash)
        };
    }

    function parseBinary(value:String):Bytes {
        return Formatters.base64Url.decode(value);
    }


    function parseKeyPartRefs(jsonValue: Dynamic, fingerprint: Fingerprint):Array<KeyPartReference> {
        var keyParts:Array<Dynamic> = jsonValue.keyParts;

        return [for(keyPartRef in keyParts)
                    parseKeyPartRef(keyPartRef, fingerprint)];
    }

    function parseKeyPartRef(
        partRef: Dynamic, fingerprint: Fingerprint): KeyPartReference
    {
        // var fing = parseFingerprint(partRef.keyId);
        return {
            part: partRef.part,
            keyId: fingerprint
        };
    }

    public function parseKeyPart(keyPartData:Binary): KeyPart {
        return {
            data: keyPartData
        };
    }

    public function maxKeyPartLength():Int {
        return defaultKeyPartLength(); //make configurable
    }
    public static function defaultKeyPartLength():Int {
        return 200; //review this size
    }

    public function keyPartQuery(keyPart:KeyPartReference):DnsQuery {
        return {
            name: keyPartUrl(keyPart),
            type: TXT
        };
    }

    public function keyPartUrl(keyPart:KeyPartReference):String {
        var keyId = Formatters.base32.encode(keyPart.keyId.id());
        return '${keyPart.part}.${keyId}._keys.local';
    }

    public function
        respondWithDescriptor(queryEvt:QueryEvent, descriptor:KeyDescriptor)
    {
        queryEvt.respondWith({
            data: descriptorTxt(descriptor),
            //TO-DO: set ttl
        });
    }

    public function respondWithKeyPart(queryEvt:QueryEvent, keyPart:KeyPart) {
        queryEvt.respondWith({
            data: keyPart.data,
            //TO-DO: set ttl
        });
    }
}