package discovery.mdns;

import discovery.mdns.MdnsQuery;
import discovery.utils.functional.Callback;
import discovery.base.search.Stoppable;
import discovery.async.DoneCallback;
import discovery.mdns.DiscoveredService;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;

typedef MdnsServiceFound = (discovered:DiscoveredService) -> Void;

typedef DnsSdPublishListener = {
    ?onPublish: Callback,
    ?onFinish: DoneCallback
};

typedef DnsSdBaseQuery = {
    ?type: String,
    ?protocol:String
}
typedef DnsSdQuery = DnsSdBaseQuery & {
    ?subtypes: Array<String>
};

typedef DnsSdResolveQuery = DnsSdBaseQuery & {
    name: String
};

interface DnsSd {
    function find(query:DnsSdQuery, onFound:MdnsServiceFound):Stoppable;
    function resolve(query:DnsSdResolveQuery, onFound:MdnsServiceFound):Stoppable;
    function publish(publishArgs:MdnsPublishData,
                    ?listener: DnsSdPublishListener):Stoppable;
    function finish(callback:Callback): Void;
}

