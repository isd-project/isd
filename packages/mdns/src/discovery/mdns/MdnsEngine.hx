package discovery.mdns;

import discovery.mdns.mdnsengine.QueryHandler;
import discovery.mdns.dns.DnsQuery;
import discovery.mdns.dns.DnsResponse;
import discovery.base.search.Stoppable;
import discovery.mdns.DnsRecord;
import haxe.Exception;

typedef OnPublishRecord = (?err:Exception)->Void;

typedef OnDnsResponse = (DnsResponse)->Void;
typedef OnErrorCallback = (Exception)->Void;

interface MdnsEngine {
    function publish(record:DnsRecord, ?onPublish: OnPublishRecord): Void;
    function onQuery(query: DnsQuery, onQuery:QueryHandler): Void;

    function query(query:DnsQuery,
                    onResponse:OnDnsResponse,
                    ?onError: OnErrorCallback): Stoppable;

    function finish():Void;
}