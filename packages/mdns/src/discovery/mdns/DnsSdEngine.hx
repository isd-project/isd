package discovery.mdns;

import discovery.mdns.DiscoveredService;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.mdns.externs.bonjour.Browser;

typedef MdnsOnFound = (discovered:DiscoveredService) -> Void;

interface DnsSdEngine {
    function find(findOpt:MdnsQuery, onFound:MdnsOnFound):Browser;
    function publish(publishArgs:MdnsPublishData):DnsSdService;
    function finish(callback:() -> Void): Void;
}

