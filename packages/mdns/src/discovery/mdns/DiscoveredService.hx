package discovery.mdns;

import discovery.mdns.MdnsTxt;

typedef DiscoveredService = {
    name: String,
    port: Int,
    type: String,
    addresses: Array<String>,

    ?fqdn: String,
    ?host: String,
    ?txt: MdnsTxt,
    ?protocol: String,
    ?subtypes: Array<Dynamic>
}
