package discovery.mdns;

import discovery.mdns.dns.DnsQuery;
import discovery.format.Binary;

enum abstract DnsRecordType(String) to String{
    var ANY  = '*';
    var A    = 'A';
    var AAAA = 'AAAA';
    var PTR  = 'PTR';
    var TXT  = 'TXT';
    var SRV  = 'SRV';
    //...

    public static function types():Array<DnsRecordType> {
        return [A, AAAA, PTR, TXT, SRV];
    }
}

typedef ResponseRecordFields = {
    data: Binary,
    ?ttl: Int
};

@:structInit
class DnsRecord {
    public var name: String;
    public var type: DnsRecordType;
    public var data: Binary;
    @:optional
    public var ttl: Int;

    public static function
        makeFromQuery(query: DnsQuery, fields: ResponseRecordFields): DnsRecord
    {
        return {
            name: query.name,
            type: query.type,
            data: fields.data,
            ttl: fields.ttl
        };
    }

    public function equals(other:DnsRecord):Bool {
        if(this == other) return true;

        return other != null
            && name == other.name
            && type == other.type
            && Binary.areEquals(data, other.data)
            && ttl == other.ttl;
    }
}