package discovery.mdns.dns;

import discovery.mdns.DnsRecord.ResponseRecordFields;
import discovery.format.Binary;

@:structInit
class DnsResponse{
    public var record(default, null): DnsRecord;

    public function new(record: DnsRecord) {
        this.record = record;
    }

    public static function
        makeFromQuery(query: DnsQuery,
                      fields: ResponseRecordFields): DnsResponse
    {
        return new DnsResponse(DnsRecord.makeFromQuery(query, fields));
    }
}