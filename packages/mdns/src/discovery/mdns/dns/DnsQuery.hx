package discovery.mdns.dns;

import discovery.mdns.DnsRecord.DnsRecordType;


@:structInit
class DnsQuery{
    public var name(default, default):String;
    @:optional
    public var type(default, null):DnsRecordType;

    public static function make(name: String, ?type: DnsRecordType){
        return new DnsQuery(name, type);
    }

    public function new(name: String, ?type: DnsRecordType) {
        this.name = name;
        this.type = type;
    }

    public function clone(): DnsQuery{
        return new DnsQuery(name, type);
    }
}