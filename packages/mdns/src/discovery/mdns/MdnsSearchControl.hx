package discovery.mdns;

import discovery.domain.Description;
import discovery.base.search.SearchControl;
import discovery.domain.Description.Location;
import discovery.domain.Discovered;
import discovery.mdns.externs.bonjour.Browser;
import discovery.base.search.SearchBase;


class MdnsSearchControl implements SearchControl {
    var search:SearchBase;
    var mdns:DnsSdEngine;
    var browser:Browser;

    public function new(mdns:DnsSdEngine) {
        search = new SearchBase(this);
        this.mdns = mdns;
    }

    public function active(): Bool {
        return browser != null;
    }

    public function start(query: MdnsQuery): SearchBase {
        browser = mdns.find(query, onMdnsFound);

        return search;
    }


    public function stop(?callback:() -> Void) {
        if(!active()){
            return;
        }

        browser.stop();
        browser = null;

        if (callback != null){
            callback();
        }
    }

    // ------------------------------------------------------------

    function onMdnsFound(discovered:DiscoveredService) {
        this.search.discovered(parseDiscovered(discovered));
    }

    function parseDiscovered(mdnsService: DiscoveredService): Discovered
    {
        var signed = MdnsTxtConverter
                            .parser()
                            .extractSignedAnnouncement(mdnsService.txt);

        var desc = signed.data.description;
        /*
            Review: extracting data from the service info into the description
            can mess with the verification/signature,
            since the fields outside of the txt may not have been included
            on signature.
        */
        desc = extractMissingDataFromServiceInfo(desc, mdnsService);

        return signed;
    }

    function extractMissingDataFromServiceInfo(
        description:Description,
        mdnsService:DiscoveredService):Description
    {
        description.name = getIfNull(description.name, mdnsService.name);
        description.type = getIfNull(description.type, mdnsService.type);

        var loc:Location = getIfNull(description.location, new Location());
        loc.defaultPort  = getIfNull(loc.defaultPort, mdnsService.port);
        loc.addresses    = getIfNull(loc.addresses  , mdnsService.addresses);

        description.location = loc;

        return description;
    }

    function getIfNull(v1:Dynamic, v2:Dynamic):Dynamic {
        return if(v1 != null) v1 else v2;
    }
}