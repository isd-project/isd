package discovery.mdns;

import discovery.domain.PublishInfo;
import discovery.base.publication.PublicationControl;
import discovery.domain.PublishListeners;
import discovery.exceptions.IllegalArgumentException;
import discovery.base.search.filters.IdentifierFilter;
import discovery.base.search.SearchFilter;
import discovery.mdns.queries.LocateQuery;
import discovery.mdns.queries.SearchQuery;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.domain.Identifier;


import Assertion.*;

class MdnsDiscovery implements DiscoveryMechanism{
    var dnsSd:DnsSdEngine;

    public function new(dnsSd:DnsSdEngine) {
        this.dnsSd = dnsSd;
    }

    public function search(query:Query):Search {
        return doSearch(new SearchQuery(query));
    }

    public function locate(id:Identifier):Search {
        IllegalArgumentException.verify(id != null, "identifier", "identifier is null");

        return doSearch(new LocateQuery(id), new IdentifierFilter(id));
    }

    function doSearch(query: MdnsQueryBuilder, ?filter:SearchFilter): Search {
        var searchControl = new MdnsSearchControl(dnsSd);
        var search = searchControl.start(query.buildQuery());

        if(filter != null){
            search.filters.addFilter(filter);
        }

        return search;
    }

    public function publish(
        info:PublishInfo,
        ?listeners:PublishListeners):PublicationControl
    {
        requireDnsSd();

        var publication = new MdnsPublicationControl(dnsSd, listeners);

        return publication.publish(info);
    }

    public function finish(callback:() -> Void) {
        requireDnsSd().finish(callback);
    }

    // --------------------------------------------------------------------

    function requireDnsSd(): DnsSdEngine {
        assert(this.dnsSd != null);

        return dnsSd;
    }
}