package discovery.samples.nodejs.https;

import js.node.Buffer;
import js.node.tls.TLSSocket;
import js.node.http.IncomingMessage;
import js.node.Https;
import discovery.async.promise.PromisableFactory;
import discovery.domain.Search;
import discovery.exceptions.IllegalStateException;
import discovery.exceptions.IllegalArgumentException;
import discovery.domain.Discovered;
import discovery.async.promise.Promisable;
import discovery.domain.Identifier;

typedef HttpsConsumerOptions = {
    promises: PromisableFactory
};

class HttpsConsumer {
    var discovery: Discovery;
    var search:Search;
    var promises:PromisableFactory;

    var options:HttpsConsumerOptions;

    public function new(opts:HttpsConsumerOptions) {
        this.options = opts;
        this.promises = opts.promises;
    }

    public function setup(discovery:Discovery) {
        this.discovery = discovery;
    }

    public function stop(?callback:()->Void): Promisable<Any> {
        if(discovery == null) return promises.resolved(null);

        final d = discovery;
        discovery = null;

        return promises.promise((resolve, reject)->{
            d.finish(()->{
                if(callback != null) callback();

                resolve(null);
            });
        });
    }

    public function locate(srvId:Identifier):Promisable<Discovered> {
        if(srvId == null)
            throw new IllegalArgumentException('Cannot locate service: identifier is null');
        if(discovery == null)
            throw new IllegalStateException('Discovery instance was not set yet.');

        search = discovery.locate(srvId);

        return options.promises.promise((resolve, reject)->{
            var disc:Discovered = null;

            search.onFound.listen((discovered)->{
                disc = discovered;
                search.stop();
            });
            search.onceFinished.listen((evt)->{
                if(evt.error != null){
                    reject(evt.error);
                }
                else{
                    resolve(disc);
                }
            });
        });
        // search.
    }

    public function getFromServer(path:String, srv:Discovered):Promisable<String> {
        var ch = selectChannel(srv);
        if(ch == null){
            return promises.rejected(new IllegalArgumentException(
                'Discovered service has no valid channel'
            ));
        }

        return promises.promise((resolve, reject)->{
            var opt:HttpsRequestOptions = {
                path:path,
                host: ch.address,
                port: ch.port,
                rejectUnauthorized: false
            };

            var req = Https.get(opt, (res)->{
                trace('received msg');

                var body = '';
                res.on('data', (chunk)->{ body += chunk; });
                res.on('end', ()->{
                    trace('response body: "${body}"');
                    res.destroy();
                    resolve(body);
                });
            });

            req.on('socket', (_)->{
                var tlsSocket = cast(req.socket, TLSSocket);

                tlsSocket.on('secureConnect', ()->{
                    var cert = tlsSocket.getPeerCertificate(false);
                    trace('server cert: ', cert);

                    //TO-DO: verify server certificate
                });
            });

            req.on('error', (err)->{
                trace('on error: ${err}');
                req.abort();
                reject(err);
            });
        });
    }

    function selectChannel(srv: Discovered) {
        for(ch in srv.description().location.channels){
            if(ch.protocol == "https"
                && ch.address != null){
                return ch;
            }
        }

        return null;
    }
}