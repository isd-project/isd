#files
PRIV_KEY=priv-key.pem
PUBL_KEY=publ-key.pem
CERT=cert.pem
CSR=csr.pem

# Generate private key using elliptic curve P-256 (prime256v1)
#
openssl ecparam -genkey -name prime256v1 -noout -out $PRIV_KEY

# Generate public key from private key
#
openssl ec -in $PRIV_KEY -pubout > $PUBL_KEY


# Create signing request, using generated key
#
openssl req -new -key $PRIV_KEY -out $CSR


# Create self-signed certificate
#
CERT_DURATION_DAYS=$((365*100))
#
openssl x509 -req -in $CSR -signkey $PRIV_KEY -out $CERT -days $CERT_DURATION_DAYS
