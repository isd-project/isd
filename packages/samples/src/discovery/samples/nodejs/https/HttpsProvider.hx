package discovery.samples.nodejs.https;

import discovery.domain.IPAddress;
import discovery.domain.Publication;
import discovery.domain.Description;
import discovery.exceptions.IllegalStateException;
import discovery.keys.storage.KeyData;
import discovery.keys.crypto.ExportedKey;
import discovery.domain.Identifier;
import js.node.http.ServerResponse;
import js.node.http.IncomingMessage;
import js.node.Https;
import discovery.async.promise.Promisable;
import discovery.async.promise.PromisableFactory;

typedef HttpsProviderOptions = {
    name: String,
    promises: PromisableFactory,

    ?port:Int,
    ?address:String
};

@:structInit
class NetAddress{
    public var port: Int=0;
    public var address: String;

    public function toString() {
        return (if(address==null) '' else address) + ':$port';
    }
}

class HttpsProvider {
    static final PRIV_KEY =
'-----BEGIN EC PRIVATE KEY-----
MHcCAQEEILzxEP6tG2EViwBl6A78cfvKPTXCZVvMvTYIODcoEXe1oAoGCCqGSM49
AwEHoUQDQgAEdIs++MfDC3GLwv5yFrLnDUzEY2zhtInZYcTc/65iEx5kNum1aqlb
IC6MeVqi1gvBQdhDL5UdZN60OliMifTLjw==
-----END EC PRIVATE KEY-----';

    static final PUB_KEY =
'-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEdIs++MfDC3GLwv5yFrLnDUzEY2zh
tInZYcTc/65iEx5kNum1aqlbIC6MeVqi1gvBQdhDL5UdZN60OliMifTLjw==
-----END PUBLIC KEY-----';

    public static final CERTIFICATE =
'-----BEGIN CERTIFICATE-----
MIIBLzCB1QIUT6y5KxyU/UkP7bRgsEpSuE5/lCowCgYIKoZIzj0EAwIwGTEXMBUG
A1UEAwwOZXhhbXBsZSBzZXJ2ZXIwIBcNMjIxMDA0MTM1MTIyWhgPMjEyMjA5MTAx
MzUxMjJaMBkxFzAVBgNVBAMMDmV4YW1wbGUgc2VydmVyMFkwEwYHKoZIzj0CAQYI
KoZIzj0DAQcDQgAEdIs++MfDC3GLwv5yFrLnDUzEY2zhtInZYcTc/65iEx5kNum1
aqlbIC6MeVqi1gvBQdhDL5UdZN60OliMifTLjzAKBggqhkjOPQQDAgNJADBGAiEA
8B9pFqIkGv2ZWZhG4VqIoK8N6gV98Kb6oYIOoJBGg1oCIQDZhUJOfIxPDLq1mRea
L9J5DTmgiSKFKAvSPsUyyklbgg==
-----END CERTIFICATE-----';

    var options:HttpsProviderOptions;

    var promises: PromisableFactory;
    var discovery:Discovery;

    var httpsServer:js.node.https.Server;

    var publication:Publication;
    var publishedDesc: Description;

    public function new(options: HttpsProviderOptions) {
        this.promises = options.promises;
        this.options = options;

        if(this.options.port == null){
            this.options.port = 0;
        }
    }

    public function boundAddress(): Null<NetAddress> {
        if(httpsServer == null || httpsServer.address() == null){
            return null;
        }

        var sockAddr = httpsServer.address();
        return {
            port: sockAddr.port,
            address: sockAddr.address
        };
    }

    public function start(discovery: Discovery):Promisable<Any> {
        trace('starting server');

        this.discovery = discovery;

        var httpsOptions:HttpsCreateServerOptions = {
            key: PRIV_KEY,
            cert: CERTIFICATE
        };

        httpsServer = Https.createServer(httpsOptions, onRequest);

        return promises.promise((resolve, reject)->{
            httpsServer.listen(options.port, options.address, ()->{
                resolve(this);
            });
            httpsServer.on('error', (error)->{
                reject(error);
            });
        });
    }

    function onRequest(req: IncomingMessage, res: ServerResponse) {
        res.writeHead(200);
        res.end("hello world!\n");
    }

    public function publish(): Promisable<Publication>{
        return discovery.keychain().importKey({
            privateKey:new ExportedKey(PRIV_KEY, Pem),
            publicKey:new ExportedKey(PUB_KEY, Pem)
        })
        .then((keyData:KeyData)->{
            trace('Imported key: ', keyData.fingerprint.toString());

            publishedDesc = genDescription(keyData);

            return discovery.publish(publishedDesc);
        });
    }

    function genDescription(keyData: KeyData): Description {
        var addr = boundAddress();

        if(addr == null)
            throw new IllegalStateException('Server not started yet');

        return {
            name: options.name,
            port: addr.port,
            type: 'http',
            location: {
                addresses: [addr.address],
                channels: [
                    {
                        protocol: 'https',
                        address: addr.address,
                        port: addr.port,
                        keys: [keyData.fingerprint.toString()]
                    }
                ]
            },
            providerId: keyData.fingerprint
        };
    }


    public function stop(?callback:()->Void) {
        if(discovery == null) return;

        var d = discovery;
        discovery = null;

        d.finish(()->{
            if(callback != null) callback();
        });

        if(httpsServer != null){
            httpsServer.close();
        }
    }

    public function identifier():Identifier{
        if(publishedDesc == null) return null;

        return publishedDesc.identifier();
    }
}

