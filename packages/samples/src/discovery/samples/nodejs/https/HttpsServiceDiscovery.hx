package discovery.samples.nodejs.https;

import discovery.exceptions.IllegalStateException;
import discovery.test.integration.tools.TemporaryDir;
import discovery.bundle.BundleDiscoveryConfiguration;
import discovery.bundle.ConfigurableDiscoveryBuilder;
import discovery.domain.Discovered;
import haxe.exceptions.NotImplementedException;
import discovery.domain.Identifier;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.dht.dhttypes.PeerAddress;
import discovery.async.promise.Promisable;
import discovery.cli.console.SysConsole;
import discovery.cli.console.Console;
import haxe.Exception;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.dht.bootstrapper.Bootstrapper;
import discovery.dht.DhtBuilder;

class HttpsServiceDiscovery {
    static function main() {
        var sample = new HttpsServiceDiscovery(new SysConsole());
        sample.run();
    }

    var console:Console;

    var provider:HttpsProvider;
    var client:HttpsConsumer;

    var config:ConfigurableDiscoveryBuilder;
    var keysDir:String;
    var promises:PromisableFactory;
    var dhtBuilder:DhtBuilder;
    var bootstrapper:Bootstrapper;

    public function new(console:Console) {
        this.console = console;


        promises = new JsPromisableFactory();
        dhtBuilder = new DrpcNodeBuilder();

        config = new BundleDiscoveryConfiguration({
            promises: promises,
            dhtBuilder: dhtBuilder
        });

        var srvName = "my-https-service";

        bootstrapper = new Bootstrapper(dhtBuilder);
        provider = new HttpsProvider({
            name: srvName,
            address: 'localhost',
            promises: promises
        });
        client   = new HttpsConsumer({
            promises: promises
        });
    }

    public function run() {
        startBootstrap()
            .then((_)->startServer())
            .then((_)->publishService())
            // .then(discoverService)
            // .then(connectToServer)
            .catchError(notifyFailure)
            .finally(finish);
    }

    function startBootstrap(): Promisable<PeerAddress> {
        return promises.promise((resolve, reject)->{
            bootstrapper.start({
                bind:'127.0.0.1',
                onStart: (?err)->{
                    trace('bootstrapper started');

                    if(err != null) reject(err);
                    else resolve(bootstrapper.dht.boundAddress());
                }
            });
        });
    }

    function startServer() {
        return provider.start(buildDiscovery()).then((any)->{
            console.println('Listening on: ${provider.boundAddress()}');

            return any;
        });
    }

    function publishService():Promisable<Identifier>
    {
        return provider.publish()
                .then((_)->provider.identifier());
    }

    function discoverService(srvId:Identifier):Promisable<Discovered>{
        client.setup(buildDiscovery());

        return client.locate(srvId);
    }

    function buildDiscovery(): Discovery{
        var keysDir = TemporaryDir.tempdirPath();
        trace('temp keysDir: ${keysDir}');
        var bootstrapAddress = bootstrapper.dht.boundAddress();

        config.configure({
            keys: {
                keysdir: keysDir
            },
            dht: {
                bootstrap: [bootstrapAddress]
            }
        });

        return config.build();
    }

    function connectToServer(srv:Discovered):Promisable<String> {
        trace('found service: ${srv}');

        return client.getFromServer('/', srv).then((response)->{
            console.println('got response from server: "${response}"');
            return response;
        });
    }

    function finish() {
        client.stop();
        provider.stop();
        bootstrapper.stop();
    }

    function notifyFailure(err: Dynamic) {
        if(err != null){
            console.printError('Failed due to exception:\n\t${err}');
            throw err;
        }

        return err;
    }
}