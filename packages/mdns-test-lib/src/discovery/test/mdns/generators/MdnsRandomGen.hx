package discovery.test.mdns.generators;

import discovery.test.helpers.generators.PrimitivesGenerator;
import discovery.test.helpers.generators.BinaryGen;


class MdnsRandomGen {
    public static final primitives = new PrimitivesGenerator();
    public static final binary = new BinaryGen(primitives);
    public static final mdns = new MdnsGenerator(primitives, binary);
}