package discovery.test.mdns.generators;

import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.mdns.DnsSd.DnsSdResolveQuery;
import discovery.mdns.externs.bonjour.Bonjour.BonjourDiscoveredService;
import discovery.mdns.DiscoveredService;
import discovery.mdns.DnsSd.DnsSdQuery;
import discovery.mdns.dns.DnsResponse;
import discovery.format.Binary;
import discovery.mdns.DnsRecord;
import discovery.mdns.DnsRecord.DnsRecordType;
import discovery.mdns.dns.DnsQuery;

import discovery.test.helpers.RandomGen;
import discovery.test.helpers.generators.PrimitivesGenerator;
import discovery.test.helpers.generators.BinaryGen;


class MdnsGenerator {
    var primitives:PrimitivesGenerator;
    var binary: BinaryGen;

    public function new(?primitives: PrimitivesGenerator, ?binary: BinaryGen) {
        if(primitives == null) primitives = new PrimitivesGenerator();
        if(binary == null) binary = new BinaryGen(primitives);

        this.primitives = primitives;
        this.binary = binary;
    }

    public function publishData():MdnsPublishData {
        return {
            name: primitives.name(),
            type: primitives.name(),
            port: RandomGen.port(),
            host: primitives.maybe(primitives.name()),
            subtypes: primitives.maybe(subtypes()),
            protocol: primitives.maybe(dnsSdProtocol()),
            // txt: MdnsTxt
        };
    }

    public function responseForQuery(query:DnsQuery):DnsResponse {
        return {
            record: recordMatching(query)
        };
    }

    public function recordMatching(query:DnsQuery):DnsRecord {
        var record = dnsRecordWith(query.type);
        record.name = query.name;

        return record;
    }

    public function dnsRecord(?type: DnsRecordType): DnsRecord{
        type = if(type != null) type else recordType();

        return dnsRecordWith(type);
    }

    public function dnsRecordWith(type:DnsRecordType, ?data:Binary): DnsRecord {
        if(data == null){
            data = recordDataForType(type);
        }

        return {
            name: domainName(),
            ttl: RandomGen.primitives.int(100, 32000),
            type: type,
            data: data
        };
    }

    public function recordDataForType(type:DnsRecordType):Null<Binary> {
        return switch (type){
            case A: RandomGen.ipv4Address().toString();
            case AAAA: RandomGen.ipv6Address().toString();
            case PTR: domainName();
            case TXT: binary.bytes(primitives.int(1, 200));
            default: null;
        };
    }

    public function dnsQuery(?type: DnsRecordType):DnsQuery {
        return {
            name: domainName(),
            type: if(type != null) type else recordType()
        };
    }

    public function domainName(tld:String = 'local'):String {
        var domain = RandomGen.name();
        var subdomain = RandomGen.name();

        return '${subdomain}.${domain}.$tld';
    }

    public function recordType():DnsRecordType {
        return Random.fromArray(DnsRecordType.types());
    }

    function anyData():Binary {
        return binary.bytes(RandomGen.int(4, 100));
    }

    public function dnsSdSearchQuery():DnsSdQuery {
        return {
            type: primitives.name(),
            subtypes: primitives.maybe(subtypes()),
            protocol: primitives.maybe(dnsSdProtocol())
        };
    }


    public function dnsSdResolveQuery():DnsSdResolveQuery {
        return {
            name: primitives.name(),
            type: primitives.name(),
            protocol: primitives.maybe(dnsSdProtocol())
        };
    }

    public function discoveredService(): DiscoveredService {
        return {
            name: primitives.name(),
            subtypes: primitives.maybe(subtypes()),
            protocol: dnsSdProtocol(),
            addresses: addresses(),
            port: RandomGen.port(),
            type: primitives.name(),
            // txt: ...,
            // host: ...,
            // fqdn:
        };
    }

    public function bonjourDiscoveredService(): BonjourDiscoveredService {
        return {
            name: primitives.name(),
            subtypes: primitives.maybe(subtypes()),
            protocol: dnsSdProtocol(),
            addresses: addresses(),
            port: RandomGen.port(),
            type: primitives.name(),
            // txt: ...,
            // rawTxt: ...,
            // host: ...,
            // fqdn: ...,
            // referer: ...,
        };
    }

    function subtypes() {
        var amount = primitives.int(1, 3);

        return [for (i in 0...amount) primitives.name()];
    }

    function dnsSdProtocol(){
        return primitives.choice('tcp', 'udp');
    }

    function addresses():Array<String> {
        var amount = primitives.int(1, 4);

        return [for(a in RandomGen.addresses(amount)) a.toString()];
    }
}