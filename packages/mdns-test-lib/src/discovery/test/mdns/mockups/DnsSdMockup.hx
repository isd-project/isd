package discovery.test.mdns.mockups;

import discovery.utils.functional.Callback;
import discovery.test.common.mockups.discovery.StoppableMockup;
import discovery.mdns.queries.SearchQuery;
import discovery.mdns.DiscoveredService;
import discovery.mdns.MdnsQuery;
import discovery.mdns.DnsSd;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import mockatoo.Mockatoo;

import discovery.test.common.matchers.MockatooMatchers.*;
using discovery.test.matchers.CommonMatchers;


typedef DnsSdPublishCall = {
    publishArgs: MdnsPublishData,
    ?listener: DnsSdPublishListener
};

typedef SearchCall<Query> = {
    query: Query,
    listener: MdnsServiceFound,
    stopMock: StoppableMockup
};

class DnsSdMockup {
    var dnsSdMock = Mockatoo.mock(DnsSd);

    var publishCalls:Array<DnsSdPublishCall> = [];
    var searchCalls:Array<SearchCall<MdnsQuery>> = [];
    var locateCalls:Array<SearchCall<DnsSdResolveQuery>> = [];

    public function new() {
        Mockatoo.when(dnsSdMock.publish(any, any))
                .thenCall(onPublish);
        Mockatoo.when(dnsSdMock.find(any, any))
                .thenCall(makeSearchHandler(searchCalls));
        Mockatoo.when(dnsSdMock.resolve(any, any))
                .thenCall(makeSearchHandler(locateCalls));
    }

    function onPublish(args:Array<Dynamic>) {
        publishCalls.push({
            publishArgs: args[0],
            listener: args[1]
        });
    }

    function makeSearchHandler<Query>(calls:Array<SearchCall<Query>>) {
        return (args: Array<Dynamic>) -> {
            var stop = new StoppableMockup();

            calls.push({
                query: args[0],
                listener: args[1],
                stopMock: stop
            });

            return stop;
        }
    }

    public function dnsSd():DnsSd{
        return dnsSdMock;
    }

    public function notifyDiscoveryForLastQuery(srv:DiscoveredService)
    {
        notifyDiscoveryFor(searchCalls, srv);
    }

    public function notifyDiscoveryForLastResolveQuery(srv:DiscoveredService) {
        notifyDiscoveryFor(locateCalls, srv);
    }

    function notifyDiscoveryFor<Q>(
        calls:Array<SearchCall<Q>>, srv:DiscoveredService)
    {
        calls.shouldNotBeEmpty('No query executed');

        var s = calls[calls.length - 1];

        if(s.listener != null){
            s.listener(srv);
        }
    }


    public function completeLastPublication() {
        publishCalls.length.shouldBeGreaterThan(0, 'No publish call made');

        var last = publishCalls[publishCalls.length - 1];
        var listener = last.listener;

        if(listener != null && listener.onPublish != null){
            listener.onPublish();
        }
    }

    public function verifyPublishedService(?srvInfo:MdnsPublishData) {
        var matcher = if(srvInfo == null) isNotNull else equalsMatcher(srvInfo);

        Mockatoo.verify(dnsSdMock.publish(matcher, any));
    }

    public function verifySearchWith(dnsSdQuery:MdnsQuery) {
        Mockatoo.verify(dnsSdMock.find(equalsMatcher(dnsSdQuery), any));
    }

    public function verifyResolveSearchWith(query:DnsSdResolveQuery) {
        Mockatoo.verify(dnsSdMock.resolve(equalsMatcher(query), any));
    }

    public function verifyFinishedWith(callback:Callback) {
        Mockatoo.verify(dnsSdMock.finish(callback));
    }
}