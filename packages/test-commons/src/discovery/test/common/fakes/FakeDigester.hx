package discovery.test.common.fakes;

import discovery.format.Binary;
import discovery.format.Binary.BinaryData;
import discovery.test.fakes.FakePromisable;
import haxe.io.Bytes;
import discovery.keys.crypto.signature.Digester;

import org.hamcrest.Matchers.*;


class FakeDigester<T> implements Digester{
    var appendedValues:Array<Dynamic>;

    public var result(default, null):T;
    public var resultPromise(default, null):FakePromisable<T>;

    public function new(?result: T) {
        appendedValues = [];

        this.result = result;
        this.resultPromise = new FakePromisable(result);
    }

    public function resolveResult(?result:T){
        if(result != null){
            this.result = result;
        }

        resultPromise.resolve(result);
    }

    public function values(){
        return appendedValues;
    }

    public function appendString(v:String) {
        addValue(v);
    }

    public function appendData(data:Bytes) {
        addValue(data.toHex());
    }

    function addValue(v: Dynamic) {
        appendedValues.push(v);
    }

    public function verifyAppendValue(value:Dynamic) {
        value = convertValue(value);

        assertThat(appendedValues, hasItem(value),
            'Value: ${value} is not on appended data: ${appendedValues}'
        );
    }

    function convertValue(value: Dynamic){
        if(Std.isOfType(value, String)){
            return value;
        }
        if(Std.isOfType(value, Bytes) || Std.isOfType(value, BinaryData)){
            return Binary.fromDynamic(value).toHex();
        }
        else if(Std.isOfType(value, Int)){
            return Binary.fromInt(value).toHex();
        }
        else if(Std.isOfType(value, Float)){
            return Binary.fromFloat(value).toHex();
        }

        return value;
    }
}