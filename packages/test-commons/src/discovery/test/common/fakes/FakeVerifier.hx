package discovery.test.common.fakes;

import discovery.keys.crypto.signature.Signature;
import discovery.async.promise.Promisable;
import discovery.keys.crypto.signature.Verifier;

class FakeVerifier extends FakeDigester<Verify> implements Verifier{

    public var capturedSignature:Signature;

    public function verify(signature:Signature):Promisable<Verify> {
        this.capturedSignature = signature;

        return resultPromise;
    }
}