package discovery.test.common.fakes;

import discovery.test.fakes.FakePromisable;
import discovery.async.promise.Promisable;
import haxe.io.Bytes;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.signature.Signer;


class FakeSigner extends FakeDigester<Signature> implements Signer{
    public var signature(get, null):Signature;
    public var signaturePromise(get, null):FakePromisable<Signature>;

    public function new(?resolvedSignature:Signature) {
        super(resolvedSignature);
    }

    function get_signature(): Signature {
        return this.result;
    }

    function get_signaturePromise(): FakePromisable<Signature> {
        return this.resultPromise;
    }

    public function end():Promisable<Signature> {
        return resultPromise;
    }

    public function resolveSignature(?signature:Signature): Signature{
        signature = if(signature != null) signature
                    else RandomGen.binary.bytes(32);

        super.resolveResult(signature);

        return signature;
    }
}