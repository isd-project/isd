package discovery.test.common.fakes.domain;

import discovery.domain.Event.ListenOptions;
import discovery.utils.events.EventEmitter;

private typedef Callback<T> = T->Void;

class FakeEvent<T> implements EventEmitter<T>{

    var onceCallback: Array<Callback<T>> = [];
    var listeners: Array<Callback<T>> = [];

    public function new() {}

    public function notify(value:T) {
        for (callbacks in [onceCallback, listeners]){
            for (l in callbacks){
                notifyListener(l, value);
            }
        }

        onceCallback = [];
    }

    function notifyListener(l:Callback<T>, value:T) {
        if(l != null){
            l(value);
        }
    }

    public function listen(callback:T -> Void, ?options:ListenOptions) {
        if(callback == null){
            return; //Maybe should throw
        }

        var listenersList = this.listeners;

        if(options!=null && options.once){
            listenersList = this.onceCallback;
        }

        listenersList.push(callback);
    }

    public function unlisten(callback:T -> Void) {
        for (callbacks in [onceCallback, listeners]){
            callbacks.remove(callback);
        }
    }
}