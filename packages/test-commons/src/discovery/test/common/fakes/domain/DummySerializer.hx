package discovery.test.common.fakes.domain;

import discovery.domain.serialization.Serializer;

class DummySerializer<T> implements Serializer<T> {
    public function new() {

    }

    public function serialize(value:T):Any {
        return value;
    }

    public function deserialize(serialized:Dynamic):T {
        return serialized;
    }
}