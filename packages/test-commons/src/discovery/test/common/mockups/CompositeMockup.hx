package discovery.test.common.mockups;

import discovery.test.helpers.RandomGen;

typedef BuilderCallback<T> = ()->T;

class CompositeMockup<T> {
    var builder: BuilderCallback<T>;
    var mockups:Array<T>;


    public function new(builder: BuilderCallback<T>) {
        this.builder = builder;

        mockups = [];
    }

    public function populate(?amount: Int) {
        if(amount == null){
            amount = RandomGen.primitives.int(2, 5);
        }

        for (i in 0...amount)
            addOne();
    }


    public function addOne() {
        var mockup = builder();
        mockups.push(mockup);

        return mockup;
    }

    public function numComponents() {
        return mockups.length;
    }

    public function map<Tout>(callback:(T)->Tout): Array<Tout> {
        return this.mockups.map(callback);
    }

    public function forEach(callback:(T) -> Void) {
        if(callback == null) return;

        for (mockup in mockups){
            callback(mockup);
        }
    }

    public function forSomeComponents(
        doForSome:(T)->Void,
        ?doForOthers: (T)->Void,
        ?amount:Int,
        shuffle:Bool=true)
    {
        if(amount == null){
            amount = RandomGen.primitives.int(1, numComponents() - 1);
        }

        var mocks = mockups;

        if(shuffle){
            mocks = RandomGen.primitives.shuffle(mocks.copy());
        }

        for (i in 0...mocks.length){
            if(i < amount && doForSome != null){
                doForSome(mocks[i]);
            }
            else if(i >= amount && doForOthers != null){
                doForOthers(mocks[i]);
            }
        }
    }

    public function pickOne():T {
        return RandomGen.primitives.pickOne(mockups);
    }
}