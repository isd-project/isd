package discovery.test.common.mockups;

import mockatoo.Mockatoo;
import discovery.utils.filter.Filter;

class FilterMockup<T> {
    var filterMock:Filter<T>;

    public function new() {
        filterMock = Mockatoo.mock(Filter);
    }

    public function filter():Filter<T> {
        return filterMock;
    }

    public function verifyFilterUsed() {
        Mockatoo.verify(filterMock.filter(any));
    }
}