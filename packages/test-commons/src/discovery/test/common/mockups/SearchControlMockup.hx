package discovery.test.common.mockups;

import discovery.base.search.Stoppable.StopCallback;
import mockatoo.Mockatoo;
import discovery.base.search.SearchControl;

using discovery.test.matchers.CommonMatchers;


class SearchControlMockup {
    public var searchControl: SearchControl;

    var stopCallback:StopCallback;

    public function new() {
        searchControl = Mockatoo.mock(SearchControl);

        Mockatoo.when(searchControl.stop(any)).thenCall(onStop);
    }

    function onStop(args: Array<Dynamic>) {
        this.stopCallback = args[0];
    }

    public function assertWasStopped() {
        Mockatoo.verify(searchControl.stop(any));
    }

    public function assertWasStoppedOnlyOnce() {
        Mockatoo.verify(searchControl.stop(any), times(1));
    }

    public function notifyStop() {
        stopCallback.shouldNotBeNull('The search stop needs to be called first');

        stopCallback();
    }
}