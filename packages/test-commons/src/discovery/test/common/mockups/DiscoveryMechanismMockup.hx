package discovery.test.common.mockups;

import discovery.test.common.mockups.discovery.SearchMockup;
import discovery.utils.functional.Callback;
import haxe.Exception;
import discovery.domain.PublishListeners;
import discovery.domain.Identifier;
import discovery.domain.Query;
import discovery.domain.Description;
import discovery.base.search.SearchControl;
import discovery.base.search.SearchBase;
import discovery.domain.Discovered;
import discovery.domain.PublishInfo;
import discovery.base.publication.PublicationControl;
import mockatoo.Mockatoo;

class DiscoveryMechanismMockup {
    public var mechanism(default, default): DiscoveryMechanism;
    public var publishedInfo(default, null):PublishInfo;

    var publishListener:PublishListeners;
    var finishListener:Callback;

    var publicationControl:PublicationControl;
    var searchMockup = new SearchMockup();

    var discoveredRegister:Array<Discovered>;

    public function new() {
        mechanism = Mockatoo.mock(DiscoveryMechanism);
        publicationControl = Mockatoo.mock(PublicationControl);

        discoveredRegister = [];

        Mockatoo.when(mechanism.publish(any, any)).thenCall(onPublish);
        Mockatoo.when(mechanism.search(any)).thenCall(onSearch);
        Mockatoo.when(mechanism.locate(any)).thenCall(onSearch);
        Mockatoo.when(mechanism.finish(any)).thenCall(onFinish);
    }

    function onPublish(args: Array<Dynamic>) {
        publishedInfo = args[0];
        publishListener = args[1];

        return publicationControl;
    }

    function onSearch(args: Array<Dynamic>) {

        return searchMockup.search();
    }

    function onFinish(args: Array<Dynamic>) {
        finishListener = args[0];
    }

    public function getPublishedInfo() {
        return publishedInfo;
    }

    public function getPublishedDescription() {
        return if(publishedInfo != null) publishedInfo.description() else null;
    }

    public function getLastSearchMockup() {
        //By now uses a fixed mockup, but could create one for each request
        return searchMockup;
    }

    public function notifyFound(discovered:Discovered) {
        searchMockup.notifyDiscovered(discovered);
    }

    public function registerDiscovered(discovered:Discovered) {
        discoveredRegister.push(discovered);
    }

    public function notifyAllDiscoveredServices() {
        for (d in discoveredRegister){
            notifyFound(d);
        }
    }

    public function notifyPublished() {
        if(publishListener != null && publishListener.onPublish != null){
            publishListener.onPublish();
        }
    }

    public function notifyPublicationFinished(?err:Exception) {
        if(publishListener != null && publishListener.onFinish != null){
            publishListener.onFinish(err);
        }
    }

    public function notifyFinished() {
        if(finishListener != null){
            finishListener();
        }
    }

    public function checkSearchCalledWith(query:Query) {
        Mockatoo.verify(mechanism.search(query));
    }

    public function checkLocateCalledWith(identifier:Identifier) {
        Mockatoo.verify(mechanism.locate(identifier));
    }

    public function checkDescriptionPublished(srvDescription:Description) {
        function hasDescription(pInfo: PublishInfo) {
            return pInfo.description().equals(srvDescription);
        }

        Mockatoo.verify(mechanism.publish(customMatcher(hasDescription), any));
    }

    public function checkPublishCalledWith(publishInfo:PublishInfo) {
        Mockatoo.verify(mechanism.publish(publishInfo, any));
    }

    public function checkFinishCalled() {
        Mockatoo.verify(mechanism.finish(any));
    }
}