package discovery.test.common.mockups;

import org.hamcrest.AssertionException;
import discovery.keys.pki.KeyPublishListener;
import haxe.Exception;
import discovery.base.configuration.PkiBuilder;
import discovery.keys.Keychain;
import discovery.keys.crypto.Fingerprint;
import discovery.test.fakes.FakePromisable;
import discovery.keys.Key;
import mockatoo.Mockatoo;
import discovery.keys.PKI;

typedef PublishKeyArgs = {
    key: Key,
    ?listener: KeyPublishListener,
    response: FakePromisable<Key>
};

class PkiMockup {
    public var pki(default, default): PKI;

    var searchKeyPromise: FakePromisable<Key>;
    var pkiBuilderMock:PkiBuilder;

    var keyPublicationBlocked:Bool = false;

    var keyPublicationCalls:Array<PublishKeyArgs> = [];

    public function new() {
        pki = Mockatoo.mock(PKI);
        pkiBuilderMock = Mockatoo.mock(PkiBuilder);

        searchKeyPromise = new FakePromisable();

        Mockatoo.when(pkiBuilderMock.buildPki()).thenReturn(pki);
        Mockatoo.when(pki.searchKey(any)).thenReturn(searchKeyPromise);
        Mockatoo.when(pki.publishKey(any, any)).thenCall(onPublishKey);
    }

    function onPublishKey(args: Array<Dynamic>) {
        var key:Key = args[0];
        var listener:KeyPublishListener = args[1];
        var response:FakePromisable<Key>;

        if(!keyPublicationBlocked){
            response = FakePromisable.resolved(key);
        }
        else{
            response = new FakePromisable();
        }

        keyPublicationCalls.push({
            key: key,
            listener: listener,
            response: response
        });

        return response;
    }

    public function pkiBuilder():PkiBuilder {
        return pkiBuilderMock;
    }


    public function blockKeyPublication() {
        keyPublicationBlocked = true;
    }

    public function checkSearchKey(keyId:Fingerprint) {
        function equalToKeyId(otherKeyId: Fingerprint){
            return keyId.equals(otherKeyId);
        }

        Mockatoo.verify(pki.searchKey(customMatcher(equalToKeyId)));
    }

    public function
        checkKeychainPublished(keychain:Keychain, ?mode: VerificationMode)
    {
        Mockatoo.verify(pki.publishKeychain(keychain), mode);
    }


    public function
        notifyInternalPublicationError(key:Key, err:Exception)
    {
        var listener:KeyPublishListener = getPublicationListenerFor(key);

        if(listener != null && listener.onInternalError != null){
            listener.onInternalError(err);
        }
    }

    function getPublicationListenerFor(key:Key):KeyPublishListener {
        for (args in keyPublicationCalls){
            if(args.key == key)
                return args.listener;
        }

        throw new AssertionException('Key (${key.fingerprint}) was not published');
    }

    public function resolveSearchKey(): KeyMockup {
        var searchKeyMockup = new KeyMockup();

        resolveSearchKeyTo(searchKeyMockup.key);

        return searchKeyMockup;
    }

    public function resolveSearchKeyTo(key: Key) {
        searchKeyPromise.resolve(key);
    }

    public function rejectSearchKeyWith(exception:Exception) {
        searchKeyPromise.reject(exception);
    }

    public function verifyPkiBuilt() {
        Mockatoo.verify(pkiBuilderMock.buildPki());
    }

    public function verifyKeyPublished(key:Key) {
        Mockatoo.verify(pki.publishKey(key, any));
    }
}