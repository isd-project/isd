package discovery.test.common.mockups.callbacks;

import haxe.Exception;
import discovery.async.DoneCallback;

class DoneCallbackMockup {
    var listenerMockup = new ListenerMock<Exception>();

    public function new() {

    }

    public function callback(): DoneCallback {
        return cast listenerMockup.listener();
    }

    public function assertWasCalled() {
        listenerMockup.assertWasCalled();
    }
}