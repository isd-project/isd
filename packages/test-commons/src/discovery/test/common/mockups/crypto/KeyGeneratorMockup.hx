package discovery.test.common.mockups.crypto;

import discovery.keys.crypto.KeyGenerator;
import mockatoo.Mockatoo;

class KeyGeneratorMockup extends KeyLoaderMockup {
    var generator:KeyGenerator;

    public function new() {
        super();

        generator = Mockatoo.mock(KeyGenerator);
        loader = generator;

        setupMockup();
    }

    public function keyGenerator(): KeyGenerator {
        return generator;
    }
}