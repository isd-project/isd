package discovery.test.common.mockups.crypto;

import discovery.keys.crypto.CryptoKey;
import discovery.test.fakes.FakePromisable;
import discovery.keys.crypto.ExportedKey;
import mockatoo.Mockatoo;
import discovery.keys.crypto.KeyLoader;

class KeyLoaderMockup {
    var loader:KeyLoader;
    var cryptoKeyPromise: FakePromisable<CryptoKey>;

    var cryptoKeyMockup:CryptoKeyMockup;

    public function new() {
        cryptoKeyPromise = new FakePromisable();
        cryptoKeyMockup = new CryptoKeyMockup();

        loader = Mockatoo.mock(KeyLoader);

        setupMockup();
    }

    function setupMockup() {
        Mockatoo.when(loader.loadExportedKey(any))
                .thenReturn(cryptoKeyPromise);
    }

    public function keyLoader(): KeyLoader {
        return loader;
    }

    public function resolveLoadExportedKey() {
        cryptoKeyPromise.resolve(cryptoKeyMockup.key());
    }

    public function loadedKeyMockup():CryptoKeyMockup{
        return cryptoKeyMockup;
    }

    public function shouldHaveLoadedExportedKey(exportedKey: ExportedKey) {
        Mockatoo.verify(loader.loadExportedKey(exportedKey));
    }

    public function shouldNotHaveLoadedExportedKey() {
        Mockatoo.verify(loader.loadExportedKey(any), times(0));
    }
}