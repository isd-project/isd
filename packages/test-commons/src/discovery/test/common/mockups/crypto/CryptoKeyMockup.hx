package discovery.test.common.mockups.crypto;

import discovery.impl.keys.jscu.externs.JscuKey.KeyFormat;
import discovery.async.promise.Promisable;
import discovery.exceptions.IllegalArgumentException;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.Fingerprint;
import discovery.test.fakes.FakePromisable;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.Fingerprint;
import mockatoo.Mockatoo;
import discovery.keys.crypto.CryptoKey;

class CryptoKeyMockup {
    var cryptoKey:CryptoKey;
    var keyFingerprint:Fingerprint;
    var exporter:KeyExporterMockup;

    public var defaultFormat(default, default):KeyFormat = Jwk;

    public function new(?keyType: KeyType, ?privateKey:Bool) {
        keyFingerprint = RandomGen.crypto.fingerprint();
        exporter = new KeyExporterMockup(privateKey);

        if(keyType == null){
            keyType = RandomGen.crypto.keyType();
        }
        cryptoKey = mockCryptoKey(keyType);
    }

    function mockCryptoKey(keyType: KeyType) {
        var cryptoKey = Mockatoo.mock(CryptoKey);

        Mockatoo.when(cryptoKey.keyType).thenReturn(keyType);
        Mockatoo
            .when(cryptoKey.fingerprint())
            .thenCall(getFingerprintPromise);

        Mockatoo.when(cryptoKey.export(any)).thenCall(onExportKey);

        return cryptoKey;
    }

    function onExportKey(args: Array<Dynamic>): Promisable<ExportedKey> {
        return new FakePromisable(exportKey(args[0]));
    }

    public function exportKey(?format:KeyFormat):ExportedKey {
        if(format == null) format = defaultFormat;

        return exporter.export(format);
    }

    function getFingerprintPromise(args: Array<Dynamic>) {
        return new FakePromisable(keyFingerprint);
    }

    public function key(): CryptoKey {
        return cryptoKey;
    }

    public function fingerprint(): Fingerprint {
        return keyFingerprint;
    }

    public function setFingerprintTo(fing:Fingerprint) {
        this.keyFingerprint = fing;
    }
}


class KeyExporterMockup {
    var keyFormats: Map<KeyFormat, ExportedKey>;
    var privateKey:Bool;

    public function new(privateKey:Bool = false) {
        keyFormats = new Map();
        this.privateKey = privateKey;
    }

    public function export(format: KeyFormat): ExportedKey {
        IllegalArgumentException.require(format != null,
            "KeyFormat is null");

        var exported = keyFormats.get(format);

        if(exported == null){
            exported = makeExported(format);
            keyFormats.set(format, exported);
        }

        return exported;
    }

    function makeExported(format:KeyFormat):Null<ExportedKey> {
        return RandomGen.crypto.exportedKeyWithFormat(format, privateKey);
    }
}

class NullKeyExporterMockup extends KeyExporterMockup{
    override function export(format:KeyFormat):ExportedKey {
        return null;
    }
}