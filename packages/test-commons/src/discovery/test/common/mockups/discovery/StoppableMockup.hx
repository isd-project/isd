package discovery.test.common.mockups.discovery;

import mockatoo.Mockatoo;
import discovery.base.search.Stoppable;

class StoppableMockup {
    var stoppableMock:Stoppable;

    public function new() {
        stoppableMock = Mockatoo.mock(Stoppable);
    }

    public function stoppable():Stoppable {
        return stoppableMock;
    }

    public function shouldHaveBeenStopped(?mode: VerificationMode) {
        Mockatoo.verify(stoppableMock.stop(any), mode);
    }
}