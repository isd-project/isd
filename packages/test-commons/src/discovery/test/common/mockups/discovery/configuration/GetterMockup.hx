package discovery.test.common.mockups.discovery.configuration;

import discovery.base.configuration.AbstractGetter.GetterFunction;

class GetterMockup<T> {
    var callableMockup:CallableMockup;
    var callable:()->Void;

    var value:T;

    public function new(?value: T) {
        callableMockup = new CallableMockup();
        callable = callableMockup.callable();

        this.value = value;
    }

    public function setValue(value: T) {
        this.value = value;
    }

    public function getter(): GetterFunction<T> {
        return getValue;
    }

    function getValue(): T {
        callable();

        return value;
    }

    public function assertWasCalled() {
        callableMockup.verifyCallbackCalled();
    }
}