package discovery.test.common.mockups.discovery;

import haxe.Exception;
import mockatoo.Mockatoo;
import discovery.domain.PublishListeners;

class PublishListenersMockup {
    var onPublishListener:CallbackMock;
    var onFinishListener:ListenerMock<Null<Exception>>;

    public function new() {
        onPublishListener = Mockatoo.mock(CallbackMock);
        onFinishListener = Mockatoo.mock(ListenerMock);
    }

    public function listeners():PublishListeners {
        return {
            onPublish: onPublishListener.onCallback,
            onFinish: cast onFinishListener.onEvent
        };
    }

    public function verifyOnPublishCalled(?verificationMode:VerificationMode) {
        Mockatoo.verify(onPublishListener.onCallback(), verificationMode);
    }

    public function verifyOnFinishCalledWith(
        expectedException:Exception, ?mode: VerificationMode)
    {
        Mockatoo.verify(onFinishListener.onEvent(expectedException), mode);
    }

    public function verifyOnFinishCalled(?verificationMode: VerificationMode) {
        Mockatoo.verify(onFinishListener.onEvent(any), verificationMode);
    }
}