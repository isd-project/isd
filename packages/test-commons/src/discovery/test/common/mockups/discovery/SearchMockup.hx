package discovery.test.common.mockups.discovery;

import org.hamcrest.AssertionException;
import haxe.Exception;
import mockatoo.exception.VerificationException;
import discovery.domain.Discovered;
import discovery.base.search.SearchBase;
import discovery.domain.Search;

class SearchMockup {
    var _search:SearchBase;

    var searchControlMockup:SearchControlMockup;

    public function new() {
        searchControlMockup = new SearchControlMockup();

        _search = new SearchBase(searchControlMockup.searchControl);
    }

    public function search(): Search {
        return _search;
    }

    public function notifyDiscovered(discovered:Discovered) {
        _search.discovered(discovered);
    }

    public function finishSearch(?error: Exception) {
        _search.finished({
            error: error
        });
    }

    public function verifySearchStopped() {
        try{
            searchControlMockup.assertWasStopped();
        }
        catch(e:VerificationException){
            throw new AssertionException('Search was not stopped', e);
        }
    }
}