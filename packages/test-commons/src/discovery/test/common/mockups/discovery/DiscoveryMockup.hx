package discovery.test.common.mockups.discovery;

import discovery.exceptions.IllegalStateException;
import discovery.base.search.SearchOptions;
import discovery.domain.Query;
import discovery.test.matchers.CommonMatchers;
import discovery.test.helpers.RandomGen;
import discovery.domain.PublishInfo;
import discovery.domain.Identifier;
import discovery.domain.Discovered;
import discovery.domain.Description;
import mockatoo.Mockatoo;

import discovery.test.common.matchers.MockatooMatchers.*;


class DiscoveryMockup {
    var mock:Discovery;

    var publications:Array<PublicationMockup> = [];
    var pendingPublications:Array<PublicationMockup>=[];

    var searches:Array<SearchMockup> = [];

    public function new() {
        mock = Mockatoo.mock(Discovery);

        Mockatoo.when(mock.publish(any)).thenCall(onPublish);
        Mockatoo.when(mock.search(any)).thenCall(onSearch);
        Mockatoo.when(mock.locate(any)).thenCall(onSearch);
    }

    function onPublish(args: Array<Dynamic>) {
        var pub = new PublicationMockup();

        publications.push(pub);
        pendingPublications.push(pub);

        return pub.publication();
    }

    function onSearch(args:Array<Dynamic>){
        var s = new SearchMockup();

        searches.push(s);

        return s.search();
    }

    public function discovery() {
        return mock;
    }

    public function notifyAllsSearchesFinished() {
        for(s in searches){
            s.finishSearch();
        }
    }

    public function lastSearchMockup() {
        if(searches.length == 0)
            throw new IllegalStateException('no search was executed');

        return searches[searches.length - 1];
    }

    public function notifyPublications(?publishInfo:Array<PublishInfo>) {
        if(publishInfo == null) publishInfo = [];

        for (p in publishInfo){
            notifyPublication(p);
        }
    }

    public function notifyPublication(?publishInfo: PublishInfo) {
        if(publishInfo == null){
            publishInfo = RandomGen.publishInfo();
        }

        var pub = pendingPublications.shift();
        CommonMatchers.shouldNotBeNull(pub, 'No pending publications');

        pub.notifyPublication(publishInfo);
    }

    public function verifyPublished(description:Description) {
        Mockatoo.verify(mock.publish(description));
    }


    public function notifyDiscovered(discovered:Discovered) {
        lastSearchMockup().notifyDiscovered(discovered);
    }

    public function verifyLocate(?mode:VerificationMode) {
        Mockatoo.verify(mock.locate(isNotNull), mode);
    }

    public function
        verifyLocateCalledWith(identifier:Identifier, ?options: SearchOptions)
    {
        Mockatoo.verify(mock.locate(
            equalsMatcher(identifier),
            searchOptMatcher(options)
        ));
    }

    public function verifySearchWith(query:Query, ?options: SearchOptions)
    {
        Mockatoo.verify(mock.search(
            equalsMatcher(query),
            searchOptMatcher(options)
        ));
    }

    public function verifyLocateMatching(
        ?idMatcher:(Identifier)->Bool,
        ?optMatcher:(SearchOptions)->Bool)
    {
        Mockatoo.verify(mock.locate(
            matcherOrAny(idMatcher),
            matcherOrAny(optMatcher)
        ));
    }

    public function verifySearchMatching(
        ?queryMatcher:(Query)->Bool,
        ?optMatcher:(SearchOptions)->Bool)
    {
        Mockatoo.verify(mock.search(
            matcherOrAny(queryMatcher),
            matcherOrAny(optMatcher)
        ));
    }

    function searchOptMatcher(?options: SearchOptions): Matcher{
        if(options == null) return any;

        return equalsMatcher(options);
    }
}