package discovery.test.common.mockups.discovery;

import discovery.domain.PublishInfo;
import discovery.test.common.fakes.domain.FakeEvent;
import discovery.domain.ErrorEventInfo;
import discovery.domain.Publication;

class PublicationMockup {
    var mock = new MockedPublication();

    public function new() {}

    public function publication(): Publication {
        return mock;
    }

    public function notifyPublication(?publishInfo:PublishInfo) {
        mock.oncePublished.notify({
            published: publishInfo,
            publication: mock
        });
    }
}


class MockedPublication implements Publication{

    public var oncePublished(default, null):FakeEvent<PublishedEventInfo>;
    public var onceFinished(default, null):FakeEvent<PublicationFinished>;
    public var onInternalError(default, null):FakeEvent<ErrorEventInfo>;

    public function new() {

        oncePublished = new FakeEvent();
        onceFinished = new FakeEvent();
        onInternalError = new FakeEvent();
    }

    public function stop() {}

}