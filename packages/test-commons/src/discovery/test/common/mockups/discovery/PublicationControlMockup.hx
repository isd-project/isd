package discovery.test.common.mockups.discovery;

import mockatoo.Mockatoo;
import discovery.base.publication.PublicationControl;

class PublicationControlMockup {

    var controlMock:PublicationControl;

    public function new() {
        controlMock = Mockatoo.mock(PublicationControl);
    }

    public function control() {
        return controlMock;
    }

    public function verifyStopCalled() {
        Mockatoo.verify(controlMock.stop(any));
    }
}