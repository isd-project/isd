package discovery.test.common.mockups.discovery.configuration;

import mockatoo.Mockatoo;
import discovery.base.configuration.DiscoveryMechanismBuilder;

class DiscoveryMechanismBuilderMockup {
    var builderMock:DiscoveryMechanismBuilder;

    public function new() {
        builderMock = Mockatoo.mock(DiscoveryMechanismBuilder);
    }

    public function builder(): DiscoveryMechanismBuilder{
        return builderMock;
    }

    public function verifyBuilt(?times:VerificationMode) {
        Mockatoo.verify(builderMock.buildMechanism(), times);
    }

    public function verifyNotBuilt() {
        verifyBuilt(times(0));
    }
}