package discovery.test.common.mockups;

import discovery.keys.KeyTransformations;
import discovery.keys.storage.PublicKeyData;
import discovery.test.common.mockups.crypto.CryptoKeyMockup;
import discovery.format.BytesOrText;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.storage.KeyData;
import discovery.keys.crypto.signature.Verifier.Verify;
import discovery.keys.crypto.signature.ObjectVerifier;
import discovery.test.common.fakes.FakeVerifier;
import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.Fingerprint;
import discovery.test.helpers.RandomGen;
import discovery.test.common.fakes.FakeSigner;
import discovery.keys.crypto.signature.Signature;
import mockatoo.Mockatoo;
import discovery.keys.Key;

import org.hamcrest.Matchers.*;

import discovery.test.helpers.RandomGen.*;

typedef KeyMockupOptions = {
    ?hasPrivateKey: Bool
}

class KeyMockup {
    public var key(default, default):Key;

    public var keySigner(default, default):FakeSigner;
    public var keyVerifier(default, default):FakeVerifier;

    public var keyFingerprint(default, null):Fingerprint;

    var keyName:String;
    var keyType:KeyType;

    var privateKeyMockup: CryptoKeyMockup;
    var publicKeyMockup:CryptoKeyMockup;

    public function new(?opt:KeyMockupOptions) {
        if(opt == null) opt = {};
        if(opt.hasPrivateKey == null) opt.hasPrivateKey = Random.bool();

        key            = Mockatoo.mock(Key);
        keySigner      = new FakeSigner();
        keyVerifier    = new FakeVerifier();
        keyFingerprint = RandomGen.crypto.fingerprint();
        keyName        = primitives.maybe(primitives.name());
        keyType        = crypto.keyType();

        publicKeyMockup = new CryptoKeyMockup(keyType);
        publicKeyMockup.setFingerprintTo(keyFingerprint);

        if(opt.hasPrivateKey){
            privateKeyMockup = new CryptoKeyMockup(keyType);
        }
        else{
            privateKeyMockup = null;
        }

        setupMockups();
    }

    function setupMockups() {
        Mockatoo.when(key.keyType).thenReturn(keyType);
        Mockatoo.when(key.name).thenReturn(keyName);
        Mockatoo.when(key.fingerprint).thenReturn(keyFingerprint);

        Mockatoo.when(key.publicKey).thenReturn(publicKeyMockup.key());
        if(privateKeyMockup != null){
            Mockatoo.when(key.privateKey).thenReturn(privateKeyMockup.key());
        }
        else{
            Mockatoo.when(key.privateKey).thenReturn(null);
        }

        Mockatoo.when(key.startSignature(any)).thenReturn(keySigner);
        Mockatoo.when(key.startVerification(any)).thenReturn(keyVerifier);
    }

    public function setKeyFingerprintTo(keyFingerprint:Fingerprint) {
        this.keyFingerprint = keyFingerprint;
        this.publicKeyMockup.setFingerprintTo(keyFingerprint);
        Mockatoo.when(key.fingerprint).thenReturn(keyFingerprint);
    }

    public function resolveSignature() {
        resolveSignatureTo(RandomGen.crypto.signature());
    }

    public function resolveSignatureTo(signature:Signature) {
        keySigner.resolveSignature(signature);
    }

    public function checkSignatureStarted() {
        Mockatoo.verify(key.startSignature(any));
    }

    public function checkVerificationStarted() {
        Mockatoo.verify(key.startVerification(any));
    }

    public function checkObjectVerified<T>(signed:Signed<T>) {
        var fakeVerifier = new FakeVerifier();
        ObjectVerifier.verifySigned(fakeVerifier, signed);

        var expectedFields = fakeVerifier.values();

        assertThat(keyVerifier.values(), is(array(expectedFields)),
            "Verified fields does not match the expected"
        );
    }

    public function resolveVerificationTo(verify:Verify) {
        keyVerifier.resolveResult(verify);
    }

    public function publicKeyData(format: KeyFormat):PublicKeyData {
        return KeyTransformations.toPublicKeyData(keyData(format));
    }

    public function keyData(?format: KeyFormat): KeyData {
        return {
            name: keyName,
            keyType: keyType,
            fingerprint: keyFingerprint,
            publicKey: getPublicKey(format),
            privateKey: getPrivateKey(format)
        };
    }

    function getPublicKey(?format:KeyFormat):Null<ExportedKey> {
        return publicKeyMockup.exportKey(format);
    }

    function getPrivateKey(?format:KeyFormat):Null<ExportedKey> {
        return if(privateKeyMockup == null) null
            else privateKeyMockup.exportKey(format);
    }
}
