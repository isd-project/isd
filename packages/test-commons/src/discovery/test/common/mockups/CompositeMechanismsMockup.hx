package discovery.test.common.mockups;

import discovery.test.common.mockups.discovery.configuration.DiscoveryMechanismBuilderMockup;
import discovery.test.helpers.RandomGen;

class CompositeMechanismsMockup
    extends CompositeMockup<DiscoveryMechanismMockup>
{

    var namedMechanisms:Map<String, DiscoveryMechanismBuilderMockup>;
    var includedMechanisms: Array<String>;
    var excludedMechanisms: Array<String>;

    public function new() {
        super(()->new DiscoveryMechanismMockup());
    }

    public function mechanisms():Array<DiscoveryMechanism> {
        return map((mockup)-> mockup.mechanism);
    }


    public function selecting_part_of_these_mechanims() {
        for(name in namedMechanisms.keys()){
            if(RandomGen.primitives.boolean())
                includedMechanisms.push(name);
            else excludedMechanisms.push(name);
        }

        return includedMechanisms;
    }

    public function only_the_selected_mechanisms_should_be_built() {
        for (mechName in includedMechanisms){
            namedMechanisms.get(mechName).verifyBuilt();
        }
        for(mechName in excludedMechanisms){
            namedMechanisms.get(mechName).verifyNotBuilt();
        }
    }
}