package discovery.test.common.mockups;

import discovery.utils.time.TimerLoop;
import discovery.test.common.mockups.utils.TimerLoopMockup;
import discovery.keys.PKI;
import discovery.test.fakes.FakePromisableFactory;
import discovery.Discovery.DiscoveryDependencies;
import discovery.domain.IpDiscover;
import discovery.keys.Keychain;


class DiscoveryFactoryMockup {

    var _keychainMockup:KeychainMockup;
    var _mechanismMockup:DiscoveryMechanismMockup;
    var _ipDiscoverMockup:IpDiscoverMockup;
    var _pkiMockup: PkiMockup;
    var promisableFactory:FakePromisableFactory;
    var _timerLoopMockup = new TimerLoopMockup();

    public function new() {
        _keychainMockup   = new KeychainMockup();
        _mechanismMockup  = new DiscoveryMechanismMockup();
        _ipDiscoverMockup = new IpDiscoverMockup();
        _pkiMockup        = new PkiMockup();
        promisableFactory = new FakePromisableFactory();
    }

    inline public function keychain():Keychain {
        return keychainMockup().keychain();
    }

    inline public function mechanism():DiscoveryMechanism {
        return mechanismMockup().mechanism;
    }

    inline public function ipDiscover(): IpDiscover {
        return ipDiscoverMockup().ipDiscover();
    }

    inline public function promises(): FakePromisableFactory {
        return promisableFactory;
    }

    public function timerLoop():TimerLoop {
        return timerLoopMockup().timer();
    }

    function pki():PKI {
        return pkiMockup().pki;
    }

    inline public function keychainMockup(): KeychainMockup {
        return _keychainMockup;
    }

    inline public function ipDiscoverMockup() {
        return _ipDiscoverMockup;
    }

    public function mechanismMockup() {
        return _mechanismMockup;
    }

    public function pkiMockup(): PkiMockup{
        return _pkiMockup;
    }

    inline public function timerLoopMockup() {
        return _timerLoopMockup;
    }


    public function build():Discovery {
        return buildWith({});
    }

    public function buildWith(options:DiscoveryDependencies):Discovery {
        return new Discovery(fillBuildOptions(options));
    }

    public function discoveryDependencies():DiscoveryDependencies {
        return fillBuildOptions({});
    }

    function fillBuildOptions(options:DiscoveryDependencies):Null<DiscoveryDependencies> {
        return {
            keychain  : getNonNull(options.keychain, keychain()),
            mechanism : getNonNull(options.mechanism, mechanism()),
            ipDiscover: getNonNull(options.ipDiscover, ipDiscover()),
            promises  : getNonNull(options.promises, promises()),
            pki       : getNonNull(options.pki, pki()),
            timerLoop : getNonNull(options.timerLoop, timerLoop())
        };
    }

    function getNonNull<T>(first:Null<T>, defaultValue:T):Null<T> {
        return if(first != null) first else defaultValue;
    }
}