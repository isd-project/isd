package discovery.test.common.mockups;

import discovery.test.helpers.RandomGen;
import discovery.domain.IPAddress;
import discovery.domain.IPAddress;
import discovery.test.fakes.FakePromisable;
import mockatoo.Mockatoo;
import discovery.domain.IpDiscover;

class IpDiscoverMockup {
    var _ipDiscover:IpDiscover;

    var listIpsPromise:FakePromisable<Array<IPAddress>>;

    public function new() {
        _ipDiscover = Mockatoo.mock(IpDiscover);

        listIpsPromise = new FakePromisable();

        setup();
    }

    function setup() {
        Mockatoo
            .when(_ipDiscover.listPublicIps())
            .thenReturn(listIpsPromise);
    }

    public function ipDiscover():Null<IpDiscover> {
        return _ipDiscover;
    }

    public function resolvePublicIps() {
        resolvePublicIpsTo(RandomGen.addresses(RandomGen.int(1, 5)));
    }

    public function resolvePublicIpsTo(addresses:Array<IPAddress>) {
        listIpsPromise.resolve(addresses);
    }

    public function getPublicIps() {
        return listIpsPromise.resolvedValue;
    }
}