package discovery.test.common.mockups.pki;

import mockatoo.Mockatoo;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.pki.KeySearchProcess;
import discovery.keys.pki.KeySearchBuilder;

using discovery.test.matchers.CommonMatchers;


class KeySearchProcessMockup implements KeySearchBuilder {
    var keySearchProcessMock: KeySearchProcess;

    var searchedFingerprints:Array<Fingerprint> = [];

    public function new() {
        keySearchProcessMock = Mockatoo.mock(KeySearchProcess);
    }

    public function build(fingerprint:Fingerprint):KeySearchProcess {
        searchedFingerprints.push(fingerprint);

        return keySearchProcessMock;
    }

    public function verifyStartedSearchFor(keyFingerprint:Fingerprint) {
        searchedFingerprints.shouldContainItems([keyFingerprint]);

        Mockatoo.verify(keySearchProcessMock.start());
    }
}