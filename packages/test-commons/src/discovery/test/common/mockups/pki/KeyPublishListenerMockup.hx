package discovery.test.common.mockups.pki;

import discovery.keys.pki.KeyPublishListener;
import haxe.Exception;

class KeyPublishListenerMockup {

    var internalErrorListener:ListenerMock<Exception>;

    public function new() {
        internalErrorListener = new ListenerMock();
    }

    public function listener():KeyPublishListener {
        return {
            onInternalError: internalErrorListener.listener()
        };
    }

    public function verifyInternalErrorsReceived(errs:Array<Exception>) {
        internalErrorListener.assertWasCalled();
        for(err in errs){
            internalErrorListener.assertWasCalledWith(err);
        }
    }
}