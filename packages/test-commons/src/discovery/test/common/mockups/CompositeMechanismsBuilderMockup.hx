package discovery.test.common.mockups;

import discovery.base.configuration.composite.NamedMechanismBuilder;
import haxe.Exception;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.discovery.configuration.DiscoveryMechanismBuilderMockup;


class CompositeMechanismsBuilderMockup
    extends CompositeMockup<DiscoveryMechanismBuilderMockup>
{

    var namedMechanisms:Map<String, DiscoveryMechanismBuilderMockup>;
    var includedMechanisms: Array<String>;
    var excludedMechanisms: Array<String>;

    public function new() {
        super(()->new DiscoveryMechanismBuilderMockup());

        namedMechanisms = new Map();
        includedMechanisms = [];
        excludedMechanisms = [];
    }

    public function forNamedBuilders(cb:(NamedMechanismBuilder) -> Void) {
        if(cb == null) return;

        for(name => builder in namedMechanisms.keyValueIterator()){
            cb({
                name: name,
                value: builder.builder()
            });
        }
    }

    override function populate(?amount:Int) {
        super.populate(amount);

        forEach((m)->{
            var name = uniqueName();
            namedMechanisms.set(name, m);
        });
    }

    public function selectMechanisms() {
        includedMechanisms = [];
        excludedMechanisms = [];

        var numToSelect = RandomGen.primitives.int(1, mockups.length);

        for(name in namedMechanisms.keys()){
            if(numToSelect > 0){
                numToSelect--;
                includedMechanisms.push(name);
            }
            else excludedMechanisms.push(name);
        }

        return includedMechanisms;
    }

    public function verifyOnlySelectedMechanismWereBuilt() {
        for (mechName in includedMechanisms){
            namedMechanisms.get(mechName).verifyBuilt();
        }
        for(mechName in excludedMechanisms){
            namedMechanisms.get(mechName).verifyNotBuilt();
        }
    }

    public function verifyAllMechanismsWereBuilt() {
        forEach((m)->{
            m.verifyBuilt();
        });
    }

    function uniqueName() {
        var name:String;

        for (i in 0...1000){
            name = RandomGen.primitives.name(10, 14);

            if(namedMechanisms.exists(name) == false){
                return name;
            }
        }

        throw new Exception('Could not find an unique name. Should be something wrong with the Random generator');
    }

    function genName() {
        return RandomGen.primitives.name(10, 14);
    }
}