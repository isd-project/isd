package discovery.test.common.mockups.keytransport;

import discovery.keys.crypto.CryptoKey;
import discovery.test.fakes.FakePromisable;
import discovery.keys.keydiscovery.validation.KeyFingerprintValidator;
import mockatoo.Mockatoo;

class KeyValidatorMockup {
    var keyValidatorMock:KeyFingerprintValidator;


    public function new() {
        keyValidatorMock = Mockatoo.mock(KeyFingerprintValidator);

        Mockatoo.when(keyValidatorMock.validateKey(any, any))
            .thenCall(onValidateKey);
    }

    function onValidateKey(args: Array<Dynamic>) {
        var key:CryptoKey = args[0];

        return new FakePromisable(key);
    }

    public function keyValidator() {
        return keyValidatorMock;
    }

}