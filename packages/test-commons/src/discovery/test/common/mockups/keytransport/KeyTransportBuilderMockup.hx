package discovery.test.common.mockups.keytransport;

import discovery.keys.keydiscovery.KeyTransportBuilderConfigurable;
import discovery.keys.keydiscovery.KeyTransport;
import mockatoo.Mockatoo;

using discovery.test.matchers.CommonMatchers;


class KeyTransportBuilderMockup {
    var builderMock:KeyTransportBuilderConfigurable<Any>;

    var keyTransportMockup:KeyTransportMockup;

    var configureCalls:Array<Any> = [];


    public function new() {
        builderMock = Mockatoo.mock(KeyTransportBuilderConfigurable);

        keyTransportMockup = new KeyTransportMockup();

        Mockatoo.when(builderMock.buildKeyTransport())
                .thenCall(onBuild);
        Mockatoo.when(builderMock.configure(any))
                .thenCall(onConfig);
    }

    function onBuild(args:Array<Dynamic>) {
        return builtValue();
    }

    function onConfig(args:Array<Dynamic>) {
        var config = args[0];

        configureCalls.push(config);
    }

    public function builder() {
        return builderMock;
    }

    public function builtValue(): KeyTransport {
        return keyTransportMockup.keyTransport();
    }

    public function verifyBuildCalled(?times: VerificationMode) {
        Mockatoo.verify(builderMock.buildKeyTransport(), times);
    }

    public function verifyConfiguredWith(config:Any) {
        configureCalls.shouldNotBeEmpty('No configure calls were made');
        configureCalls.shouldContainsInAnyOrder([config]);
    }
}