package discovery.test.common.mockups.keytransport;

import discovery.keys.Keychain;
import discovery.test.common.mockups.discovery.StoppableMockup;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.keys.crypto.Fingerprint;
import haxe.Exception;
import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.keydiscovery.KeyTransport;
import mockatoo.Mockatoo;

typedef SearchKeyArgs = {
    fingeprint: Fingerprint,
    listeners: SearchKeyListeners,
    stoppableMockup: StoppableMockup
};

class KeyTransportMockup {
    var transportMock:KeyTransport;
    var publishListeners:Array<DoneCallback>;
    var publishKeychainListeners:Array<DoneCallback>;

    var keySearchListeners:Map<String, SearchKeyListeners>;
    var keySearchArgs:Map<String, SearchKeyArgs>;

    public function new() {
        transportMock = Mockatoo.mock(KeyTransport);
        publishListeners = [];
        publishKeychainListeners = [];
        keySearchListeners = new Map();
        keySearchArgs = new Map();

        Mockatoo.when(transportMock.publishKey(any, any))
                .thenCall(onPublishKey);
        Mockatoo.when(transportMock.publishKeychain(any, any))
            .thenCall(onPublishKeychain);
        Mockatoo.when(transportMock.searchKey(any, any))
                .thenCall(onSearchkey);
    }

    function onPublishKey(args: Array<Dynamic>) {
        registerPublishListener(args, publishListeners);
    }

    function onPublishKeychain(args: Array<Dynamic>) {
        registerPublishListener(args, publishKeychainListeners);
    }

    function
        registerPublishListener(args: Array<Dynamic>, out:Array<DoneCallback>)
    {
        var listener:DoneCallback = args[1];

        if(listener != null){
            out.push(listener);
        }
    }

    function onSearchkey(args: Array<Dynamic>) {
        var fing:Fingerprint = args[0];
        var listeners:SearchKeyListeners = args[1];

        var stoppable = new StoppableMockup();

        //Problably should allow multiple listeners for fingerprint
        keySearchListeners.set(fing.toString(), listeners);
        keySearchArgs.set(fing.toString(), {
            fingeprint: fing,
            listeners: listeners,
            stoppableMockup: stoppable
        });

        return stoppable.stoppable();
    }

    public function keyTransport() {
        return transportMock;
    }

    public function notifyPublication() {
        notifyPublicationWith(null);
    }

    public function notifyPublicationWith(error:Null<Exception>) {
        notifyPublicationFor(publishListeners, error);
    }

    public function notifyKeychainPublication() {
        notifyPublicationFor(publishKeychainListeners);
    }

    function notifyPublicationFor(listeners:Array<DoneCallback>, ?err:Exception)
    {
        for(listener in listeners){
            listener(err);
        }
    }

    public
    function notifySearchResultFor(fing:Fingerprint, result:SearchKeyResult)
    {
        var listeners = keySearchListeners.get(fing.toString());

        if(listeners != null && listeners.onKey != null){
            listeners.onKey(result);
        }
    }

    public function notifySearchEnd(fing:Fingerprint, ?err:Exception) {
        var listeners = keySearchListeners.get(fing.toString());

        if(listeners != null && listeners.onEnd != null){
            listeners.onEnd(err);
        }
    }

    public function verifyPublishedKey(publishedKey:PublicKeyData) {
        Mockatoo.verify(transportMock.publishKey(publishedKey, any));
    }

    public function verifyPublishedSomeKey() {
        Mockatoo.verify(transportMock.publishKey(any, any));
    }

    public function verifyPublishedKeychain(keychain:Keychain) {
        Mockatoo.verify(transportMock.publishKeychain(keychain, any));
    }

    public function
        verifySearchKeyCalled(fingerprint:Fingerprint, ?mode: VerificationMode)
    {
        Mockatoo.verify(transportMock.searchKey(fingerprint, any), mode);
    }

    public function
        verifySearchStopped(fingerprint:Fingerprint, ?mode: VerificationMode)
    {
        var args = keySearchArgs.get(fingerprint.toString());

        args.stoppableMockup.shouldHaveBeenStopped(mode);
    }
}