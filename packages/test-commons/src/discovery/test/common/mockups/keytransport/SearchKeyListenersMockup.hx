package discovery.test.common.mockups.keytransport;

import haxe.Exception;
import discovery.keys.keydiscovery.SearchKeyListeners;

class SearchKeyListenersMockup {

    var onKeyListener:ListenerMock<SearchKeyResult>;
    var endListenerMockup:ListenerMock<Null<Exception>>;

    public function new() {
        onKeyListener = new ListenerMock();
        endListenerMockup   = new ListenerMock();
    }

    public function listener():SearchKeyListeners {
        var onEnd = endListenerMockup.listener();

        return {
            onKey: onKeyListener.listener(),
            onEnd: (?err)->onEnd(err)
        };
    }

    public function keyFoundListener() {
        return onKeyListener;
    }

    public function endListener() {
        return endListenerMockup;
    }

    public function lastFoundKey() {
        onKeyListener.assertWasCalled();

        return onKeyListener.lastCall().found;
    }
}