package discovery.test.common.mockups.utils;

import mockatoo.Mockatoo;
import discovery.utils.Cancellable;

class CancellableMockup {
    var cancellableMock: Cancellable;

    var cancelled:Bool=false;

    public function new() {
        cancellableMock = Mockatoo.mock(Cancellable);

        Mockatoo.when(cancellableMock.cancel()).thenCall(onCancel);
    }

    function onCancel(args: Array<Dynamic>) {
        cancelled = true;
    }

    public function cancellable(): Cancellable {
        return cancellableMock;
    }

    public function verifyCancelled() {
        Mockatoo.verify(cancellableMock.cancel());
    }

    public function wasCancelled() {
        return cancelled;
    }
}