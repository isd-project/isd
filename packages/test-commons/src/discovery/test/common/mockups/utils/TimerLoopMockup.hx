package discovery.test.common.mockups.utils;

import discovery.test.common.mockups.utils.CancellableMockup;
import discovery.utils.time.Duration.TimeUnit;
import discovery.utils.functional.Callback;
import mockatoo.Mockatoo;
import discovery.utils.time.TimerLoop;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


typedef RepeatArgs = {
    callback: ()->Void,
    intervalMs: Int,
    cancellableMock:CancellableMockup
};

typedef DelayedArgs = {
    callback: Callback,
    time: TimeUnit,
    cancellableMock: CancellableMockup
}


class TimerLoopMockup {
    var timerMock:TimerLoop;

    var callsToRepeat:Array<RepeatArgs> = [];
    var delayCalls:Array<DelayedArgs> = [];
    var pendingDelays:Array<DelayedArgs> = [];

    public function new() {
        timerMock = Mockatoo.mock(TimerLoop);

        Mockatoo.when(timerMock.repeat(any, any)).thenCall(onRepeat);
        Mockatoo.when(timerMock.delay(any, any)).thenCall(onDelay);
    }

    function onRepeat(args: Array<Dynamic>) {
        var cancellableMock = new CancellableMockup();

        callsToRepeat.push({
            callback: args[0],
            intervalMs: args[1],
            cancellableMock: cancellableMock
        });

        return cancellableMock.cancellable();
    }

    function onDelay(args: Array<Dynamic>) {
        var cancellableMock = new CancellableMockup();

        var args:DelayedArgs = {
            callback: args[0],
            time: args[1],
            cancellableMock: cancellableMock
        };

        delayCalls.push(args);
        pendingDelays.push(args);

        return cancellableMock.cancellable();
    }

    public function timer(): TimerLoop {
        return timerMock;
    }

    public function verifyRepeatCalled() {
        Mockatoo.verify(timerMock.repeat(any, any));
    }

    public function lastCallOfRepeat() {
        assertThat(callsToRepeat, not(isEmpty()),
            "No calls to repeat");

        return callsToRepeat[callsToRepeat.length - 1];
    }

    public function callLastRepeatCallback() {
        var args = lastCallOfRepeat();

        assertThat(args.callback, is(notNullValue()), "Last repeat callback was null");

        args.callback();
    }


    public function hasPendingDelays() {
        return pendingDelays.length > 0;
    }

    public function executeNextDelayed() {
        var next = pendingDelays.shift();

        if(next != null && next.callback != null
            && !next.cancellableMock.wasCancelled())
        {
            next.callback();
        }
    }

    public function verifyDelayCalled(?mode:VerificationMode) {
        Mockatoo.verify(timerMock.delay(any, any), mode);
    }

    public function verifyDelayCalledWith(time:TimeUnit) {
        Mockatoo.verify(timerMock.delay(any, time));
    }

    public function verifyPreviousDelayCancelled() {
        //currently, cancelled delays is not removed automatically
        var previousDelay = pendingDelays[0];

        previousDelay.cancellableMock.wasCancelled().shouldBe(true,
            'Delay should have been cancelled');
    }
}