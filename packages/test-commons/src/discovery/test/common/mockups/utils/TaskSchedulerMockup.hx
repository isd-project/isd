package discovery.test.common.mockups.utils;

import discovery.test.common.mockups.utils.TimerLoopMockup;
import discovery.utils.time.Duration;
import discovery.utils.time.scheduler.TaskScheduler;
import mockatoo.Mockatoo;

class TaskSchedulerMockup{
    var schedulerMock:FakeScheduler = Mockatoo.spy(FakeScheduler);

    public function new() {

    }

    public function scheduler():TaskScheduler {
        return schedulerMock;
    }

    public function
        verifyScheduledTaskWith(delay:Duration, ?times:VerificationMode)
    {
        Mockatoo.verify(schedulerMock.schedule(isNotNull, delay), times);
    }

    public function hasPendingTasks() {
        return schedulerMock.hasPendingTasks();
    }

    public function nextScheduledTimeComplete() {
        schedulerMock.completeNextScheduledTime();
    }

    public function completeAllScheduledTasks() {
        schedulerMock.completeAllScheduledTasks();
    }
}

class FakeScheduler extends TaskScheduler{

    public var timerLoopMockup = new TimerLoopMockup();

    public function new() {
        super(timerLoopMockup.timer());
    }

    public function hasPendingTasks() {
        return timerLoopMockup.hasPendingDelays();
    }

    public function completeNextScheduledTime(){
        timerLoopMockup.executeNextDelayed();
    }

    public function completeAllScheduledTasks() {
        while(timerLoopMockup.hasPendingDelays()){
            timerLoopMockup.executeNextDelayed();
        }
    }
}