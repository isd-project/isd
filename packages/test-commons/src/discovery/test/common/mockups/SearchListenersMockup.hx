package discovery.test.common.mockups;

import discovery.domain.Discovered;
import discovery.domain.Search;

import org.hamcrest.Matchers.*;


class SearchListenersMockup {
    var foundServices: Array<Discovered>;

    public function new() {
        foundServices = [];
    }

    public function registerInto(search: Search){
        search.onFound.listen(onFound);
    }

    function onFound(found: Discovered) {
        foundServices.push(found);
    }

    public function checkFound(foundService:Discovered) {
        assertThat(foundServices, hasItem(foundService),
            'Service was not found'
        );
    }

    public function checkNotFound(foundService:Discovered) {
        assertThat(foundServices, not(hasItem(foundService)),
            'Service should not have been found'
        );
    }
}