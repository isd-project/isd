package discovery.test.common.mockups;

import discovery.utils.collections.ComparableMap;
import haxe.Constraints.IMap;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.Key;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.keys.storage.KeyData;
import discovery.async.promise.Promisable;
import discovery.keys.KeyIdentifier;
import discovery.keys.Key;
import discovery.test.fakes.FakePromisable;
import mockatoo.Mockatoo;
import discovery.keys.Keychain;

private typedef KeyPromise = FakePromisable<Key>;

class KeychainMockup {
    var _keychain:Keychain;

    var getKeyPromises:IMap<KeyIdentifier, KeyPromise>;

    var _getKeyMockup: KeyMockup;

    var genKeyPromise:KeyPromise;
    var genKeyMock:KeyMockup;

    public function new() {
        _keychain = Mockatoo.mock(Keychain);

        _getKeyMockup = new KeyMockup();
        getKeyPromises = new ComparableMap();

        genKeyMock    = new KeyMockup();
        genKeyPromise = new FakePromisable();

        setupKeychain();
    }

    function setupKeychain() {
        Mockatoo.when(_keychain.getKey(any)).thenCall(onGetKey);
        Mockatoo.when(_keychain.generateKey(any)).thenReturn(genKeyPromise);

        Mockatoo
            .when(_keychain.storeKey(any))
            .thenCall(onStoreKey);

        //ensure key signature does not stop tests
        getKeyMockup().resolveSignature();
    }

    function onGetKey(args: Array<Dynamic>) {
        var keyIdentifier:KeyIdentifier = args[0];

        return getKeyPromiseFor(keyIdentifier);
    }

    function getKeyPromiseFor(keyIdentifier:KeyIdentifier) {
        var promise = getKeyPromises.get(keyIdentifier);

        if(promise == null){
            promise = new FakePromisable();
            getKeyPromises.set(keyIdentifier, promise);
        }

        return promise;
    }

    function onStoreKey(args: Array<Dynamic>): Promisable<Key>{
        var keyToStore:Key = args[0];

        return new FakePromisable(keyToStore);
    }

    // -----------------------------------------------------------------

    inline public function keychain(): Keychain {
        return _keychain;
    }

    public function defaultKey() {
        return getKeyMockup().key;
    }

    public function lastGeneratedKey(): KeyMockup {
        return genKeyMock;
    }

    public function setupListKeys(keyDataList:Array<KeyData>) {
        Mockatoo.when(_keychain.listKeys()).thenReturn(keyDataList);
    }


    public function resolveGetDefaultKey() {
        var fing = getKeyMockup().keyFingerprint;

        this.resolveGetKeyToDefault(fing);
    }

    public function resolveGetKeyToDefault(keyId:KeyIdentifier) {
        resolveGetKeyTo(keyId, _getKeyMockup.key);
    }

    public function resolveGetKeyTo(keyId:KeyIdentifier, key:Key) {
        getKeyPromiseFor(keyId).resolve(key);
    }

    public function resolveGenKeyPromise() {
        genKeyPromise.resolve(genKeyMock.key);
    }

    public function getKeyMockup(): KeyMockup {
        return _getKeyMockup;
    }

    public function verifyGetKey(keyIdentifier:KeyIdentifier) {
        Mockatoo.verify(_keychain.getKey(keyIdentifier));
    }

    public function verifyKeyStored(expectedKey:Key, ?mode:VerificationMode) {
        Mockatoo.verify(_keychain.storeKey(expectedKey), mode);
    }

    public function verifyListKeysCalled() {
        Mockatoo.verify(_keychain.listKeys());
    }

    public function verifyGenerateKeyWith(expected:GenerateKeyOptions) {
        Mockatoo.verify(_keychain.generateKey(expected));
    }
}