package discovery.test.common.mockups.parsing;

import discovery.domain.IdentifierUrl;
import haxe.Exception;
import discovery.test.helpers.RandomGen;
import mockatoo.Mockatoo;
import discovery.base.IdentifierParser;

class IdentifierParserMockup {
    var mock:IdentifierParser;

    var nextParseError:Exception = null;

    var expectedReturn:Map<String, IdentifierUrl>;

    public function new() {
        expectedReturn = new Map();

        mock = Mockatoo.mock(IdentifierParser);

        Mockatoo.when(mock.parseUrl(any)).thenCall(onParseUrl);
    }

    function onParseUrl(args: Array<Dynamic>) {
        var url = args[0];

        var identifier = expectedReturn.get(url);
        if(identifier == null){
            identifier = RandomGen.identifierUrl();
        }

        if(nextParseError != null){
            var err = nextParseError;
            nextParseError = null;

            throw err;
        }

        return identifier;
    }

    public function idParser() {
        return mock;
    }

    public function nextParseShouldFailWith(exception:Exception) {
        this.nextParseError = exception;
    }

    public function verifyUrlParsed(expectedUrl:String) {
        Mockatoo.verify(mock.parseUrl(expectedUrl));
    }


    public function
        forThisUrlParseShouldReturn(url:String, identifierUrl:IdentifierUrl)
    {
        expectedReturn.set(url, identifierUrl);
    }
}