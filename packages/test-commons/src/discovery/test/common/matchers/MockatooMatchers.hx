package discovery.test.common.matchers;

import discovery.utils.EqualsComparator;
import mockatoo.Mockatoo;

class MockatooMatchers {
    public static function equalsMatcher<T>(expected:T):Matcher {
        return customMatcher((value)->{
            return EqualsComparator.equals(value, expected);
        });
    }

    public static function matcherOrAny<T>(?matcher:T->Bool) {
        return if(matcher == null) any else customMatcher(matcher);
    }
}