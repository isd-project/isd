package discovery.test.common;

import discovery.domain.Publication;

typedef PublicationListener = ListenerMock<PublishedEventInfo>;
typedef PublicationFinishListener = ListenerMock<PublicationFinished>;
