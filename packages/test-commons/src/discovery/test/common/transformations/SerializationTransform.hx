package discovery.test.common.transformations;

import discovery.format.parsers.JsonParser;

class SerializationTransform {
    public static function jsonStringify<T>(value: T): String {
        return new JsonParser().stringify(value);
    }

    public static function jsonParse(jsonText: String): Dynamic {
        return new JsonParser().parse(jsonText);
    }

    public static function toJsonObject<T>(value: T): Dynamic {
        return jsonParse(jsonStringify(value));
    }
}