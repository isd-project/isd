package discovery.test.common;

import mockatoo.Mockatoo;

class CallableMockup {
    var callbackMock:CallbackMock;

    public function new() {
        callbackMock = Mockatoo.mock(CallbackMock);
    }

    public function callable():() -> Void {
        return callbackMock.onCallback;
    }

    public function verifyCallbackCalled(?mode: VerificationMode) {
        Mockatoo.verify(callbackMock.onCallback(), mode);
    }
}