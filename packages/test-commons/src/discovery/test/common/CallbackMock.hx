package discovery.test.common;

interface CallbackMock {
    function onCallback(): Void;
}