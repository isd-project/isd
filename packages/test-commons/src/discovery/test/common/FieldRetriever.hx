package discovery.test.common;

import discovery.format.Binary.BinaryData;
import haxe.io.Bytes;
import discovery.utils.traversal.ObjectVisitor.VisitStatus;
import discovery.utils.traversal.BaseObjectVisitor;
import discovery.utils.traversal.ObjectTraversal;

@:structInit
class FieldPair{
    public var key: String;
    public var value: Dynamic;
}

class FieldRetriever {
    public function new() {}

    public function listFieldsOf(object:Dynamic): Array<FieldPair>{
        var visitor = new FieldRetrieverVisitor();
        ObjectTraversal.doTraversal(visitor, object);

        return visitor.fields;
    }

    public function listSimpleFieldsOf(object:Dynamic): Array<FieldPair>{
        var fields = listFieldsOf(object);

        return [for(f in fields) if(!isComposite(f)) f];
    }

    function isComposite(field: FieldPair) {
        return switch (Type.typeof(field.value)){
            case TObject: true;
            case TClass(c): (c != String && c != Bytes && c != BinaryData);
            default: false;
        };
    }

    public function mapSimpleFieldsOf(object: Dynamic): Map<String, Dynamic> {
        var fields = listSimpleFieldsOf(object);
        return fieldsToMap(fields);
    }

    public function fieldsToMap(fields:Array<FieldPair>):Map<String, Dynamic> {
        var map = new Map<String, Dynamic>();

        for (field in fields){
            map.set(field.key, field.value);
        }

        return map;
    }
}

private class FieldRetrieverVisitor extends BaseObjectVisitor{
    public var fields:Array<FieldPair>;

    public function new() {
        super();
        fields = [];
    }

    override function onValue(key:String, value:Dynamic):VisitStatus {
        fields.push({
            key:key,
            value: value
        });

        return Continue;
    }
}