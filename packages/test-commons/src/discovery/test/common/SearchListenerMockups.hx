package discovery.test.common;

import discovery.domain.Discovered;
import discovery.domain.Search;
import discovery.test.common.ListenerMock;

typedef SearchListener = ListenerMock<Discovered>;
typedef FinishSearchListener = ListenerMock<FinishSearchEvent>;