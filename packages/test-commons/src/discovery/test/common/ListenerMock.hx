package discovery.test.common;

import org.hamcrest.Matcher;
import haxe.extern.EitherType;
import org.hamcrest.Matchers.*;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.EqualsComparator;

typedef TimesMatcher = EitherType<Int, Matcher<Int>>;

class ListenerMock<T> {
    public var captured:Null<T>;

    var captures:Array<T>;

    public function new() {
        captured = null;

        captures = [];
    }

    public function listener():(T)->Void {
        return this.onEvent;
    }

    public function onEvent(value: T): Void{
        captured = value;
        captures.push(value);
    }

    inline function numCalls() {
        return captures.length;
    }

    public function lastCall() {
        assertWasCalled();

        return captures[captures.length - 1];
    }

    public function assertWasCalled(errmsg:String = "Listener was not called")
    {
        assertThat(numCalls(), is(greaterThan(0)), errmsg);
    }

    public function assertWasCalledTheseTimes(
        times:TimesMatcher, ?errmsg:String)
    {
        if(errmsg == null){
            errmsg = 'Listener was called ${numCalls} times, but expected ${times} calls';
        }
        assertThat(numCalls(), is(times), errmsg);
    }

    public function assertWasCalledWith(value: T) {
        this.assertWasCalled();

        captured.shouldBeEqualsTo(value, 'Captured value does not match the expected');
    }

    public function assertWasCalledTheseTimesWith(times:TimesMatcher, value: T)
    {
        var matchingCalls = captures.filter((capt)->{
            return capt.equals(value);
        });

        assertThat(matchingCalls, hasSize(times),
            'Number of calls with value does not match\n'
            + '\tMatching calls: ' + [for(m in matchingCalls) Std.string(m)]);
    }
}