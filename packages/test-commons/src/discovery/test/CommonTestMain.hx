package discovery.test;

import haxe.io.Path;
import massive.sys.io.File;
import discovery.test.munit.JUnitReportWriter;
import massive.munit.client.PrintClient;
import massive.munit.client.RichPrintClient;
import massive.munit.client.HTTPClient;
import massive.munit.client.JUnitReportClient;
import massive.munit.client.SummaryReportClient;
import massive.munit.TestRunner;

/**
 * Modified test main
 */
class CommonTestMain
{
    static function main(){ new CommonTestMain(); }

    public function new(?relativeReportPath: Array<String>)
    {
        if(relativeReportPath == null){
            relativeReportPath = ["report"];
        }
        var paths = [File.current.toString()].concat(relativeReportPath);
        var reportPath = Path.join(paths);

        var suites = new Array<Class<massive.munit.TestSuite>>();
        suites.push(TestSuite);

        #if MCOVER
            var client = new mcover.coverage.munit.client.MCoverPrintClient();
            var httpClient = new HTTPClient(new mcover.coverage.munit.client.MCoverSummaryReportClient());
        #else
            var client = new RichPrintClient();
            // client.verbose = true;
            var httpClient = new HTTPClient(new SummaryReportClient());
        #end

        var runner:TestRunner = new TestRunner(client);

        #if (!nodejs)
            runner.addResultClient(new HTTPClient(new JUnitReportClient()));
        #else
            // Workaround for error in node (HTTPClient fails to connect with munit server)
            var writer = new JUnitReportWriter(reportPath);
            writer.clearDirectory();
            runner.addResultClient(writer);
        #end

        runner.completionHandler = completionHandler;

        #if (js && !nodejs)
        runInBrowser(runner);
        #else
        runner.run(suites);
        #end
    }

    #if(js && !nodejs)
    function runInBrowser(runner: TestRunner){
        var seconds = 0; // edit here to add some startup delay
        function delayStartup()
        {
            if (seconds > 0) {
                seconds--;
                js.Browser.document.getElementById("munit").innerHTML =
                    "Tests will start in " + seconds + "s...";
                haxe.Timer.delay(delayStartup, 1000);
            }
            else {
                js.Browser.document.getElementById("munit").innerHTML = "";
                runner.run(suites);
            }
        }
        delayStartup();
    }
    #end

    /**
     * updates the background color and closes the current browser
     * for flash and html targets (useful for continous integration servers)
     */
    function completionHandler(successful:Bool)
    {
        try
        {
            var exitCode = if(successful) 0 else 1;

            #if flash
                flash.external.ExternalInterface.call("testResult", successful);
            #elseif js
                #if nodejs
                    js.Node.process.exit(exitCode);
                #else
                    js.Lib.eval("testResult(" + successful + ");");
                #end
            #elseif (neko || cpp || java || cs || python || php || hl || eval)
                Sys.exit(exitCode);
            #end
        }
        // if run from outside browser can get error which we can ignore
        catch (e:Dynamic) {
            #if (js && nodejs)
            js.Node.console.error(e);
            #end
        }
    }
}
