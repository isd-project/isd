package discovery.test.integration.tools;

import discovery.test.integration.tools.munit.async.MunitExceptionAdapter;
import discovery.test.integration.tools.munit.async.AsyncHandler;
import haxe.PosInfos;
import massive.munit.async.AsyncFactory;
import haxe.Exception;

class AsyncHandlerBuilder {
    public static
    function buildHandle(asyncFactory: AsyncFactory, testCase:Dynamic, ?timeoutMillis:Int,?pos:PosInfos): AsyncHandler
    {
        function onDone(?err: Exception, ?posInfos:PosInfos) {
            if(err != null){
                throw new MunitExceptionAdapter(err, posInfos);
            }
        }

        var handle:AsyncHandler;
        handle = asyncFactory.createHandler(
                                        testCase, onDone, timeoutMillis, pos);

        return handle;

    }
}