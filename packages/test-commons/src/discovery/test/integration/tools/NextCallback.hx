package discovery.test.integration.tools;

import haxe.Exception;


typedef NextCallback = (?err:Exception)->Void;