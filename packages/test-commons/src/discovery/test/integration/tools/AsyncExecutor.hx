package discovery.test.integration.tools;

import discovery.test.integration.tools.munit.async.AsyncHandler;
import haxe.PosInfos;
import haxe.Exception;

class AsyncExecutor {
    var handler:AsyncHandler;

    public function new(handler: AsyncHandler) {
        this.handler = handler;
    }

    public function done(?err: Exception, ?posInfos:PosInfos) {
        handler(err, posInfos);
    }

    public function doNext(nextStep:()->Void, ?posInfos:PosInfos) {
        return function (?err:Exception) {
            if(err != null){
                done(err, posInfos);
                return;
            }

            try{
                nextStep();
            }
            catch(e: Exception){
                done(e, posInfos);
            }
        }
    }
}