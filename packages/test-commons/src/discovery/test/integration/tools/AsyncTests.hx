package discovery.test.integration.tools;

import haxe.PosInfos;
import massive.munit.async.AsyncFactory;
import haxe.Exception;

using discovery.test.integration.tools.AsyncHandlerBuilder;


class AsyncTests {
    var exec:AsyncExecutor;

    function done(?err: Exception) {
        exec.done(err);
    }

    function next(nextStep:()->Void, ?posInfos:PosInfos) {
        return exec.doNext(nextStep, posInfos);
    }

    function makeHandler(asyncFactory: AsyncFactory, ?timeoutMillis:Int, ?posInfo:PosInfos) {
        var handler = asyncFactory.buildHandle(this, timeoutMillis, posInfo);
        exec = new AsyncExecutor(handler);
    }
}