package discovery.test.integration.tools.munit.async;

import haxe.PosInfos;
import haxe.Exception;

typedef AsyncHandlerCallback = (?Exception, ?posInfos: PosInfos)->Void;

@:callable
abstract AsyncHandler(AsyncHandlerCallback)
    from AsyncHandlerCallback
    to   AsyncHandlerCallback
{

}