package discovery.test.integration.tools.munit.async;

import haxe.Exception;
import haxe.PosInfos;

class MunitExceptionAdapter extends massive.haxe.Exception{
    var cause:haxe.Exception;

    public function new(cause:Exception, ?posInfos:PosInfos)
    {
        super(cause.message, posInfos);

        this.cause = cause;
    }

    override function toString():String {
        var initMessage = makeBaseExceptionMessage(cause);

        return initMessage + '. Details:\n' +  cause.details();
    }

    function makeBaseExceptionMessage(e:Exception) {
        var typeName = Type.getClassName(Type.getClass(e));

        var str:String = typeName + ": " + e.message;

        if (this.info != null){
            str += " at " + info.className + "#" + info.methodName + " (" + info.lineNumber + ")";
        }

        return str;
    }
}