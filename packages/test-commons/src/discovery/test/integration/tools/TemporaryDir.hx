package discovery.test.integration.tools;

import haxe.Exception;
import sys.io.File;
import haxe.io.Path;
import sys.FileSystem;

/**
    Determines a temporary directory path from the system.

    This code is a (partial) port of python tempfile.
    @see https://github.com/python/cpython/blob/3.9/Lib/tempfile.py
**/
class TemporaryDir {
    static var _tempdir:String = null;

    public static function tempdir() {
        if(_tempdir == null){ //To-Do: handle concurrent access
            _tempdir = TemporaryDirChooser.choiceDefaultTempdir();
        }

        return _tempdir;
    }

    public static function tempdirPath(?subpath:String) {
        return makeSubpath(tempdir(), subpath);
    }


    var dirPath:String;

    public function new(?subpath:String) {
        this.dirPath = tempdirPath(subpath);

        FileSystem.createDirectory(dirPath);
    }

    public function removeDir() {
        if(FileSystem.exists(dirPath)){
            FileSystem.deleteDirectory(dirPath);
        }
    }

    public function path() {
        return dirPath;
    }

    public function subpath(?name:String): String {
        return makeSubpath(dirPath, name);
    }

    // helpers -----------------------------------------------------------

    static function makeSubpath(parent:String, ?subpath:String): String {
        if(subpath == null){
            subpath = randomName();
        }

        return Path.join([tempdir(), subpath]);
    }

    static function randomName():Null<String> {
        var randomSequence = new RandomNameSequence(12);
        return randomSequence.next();
    }
}

class TemporaryDirChooser {
    /** Calculate the default directory to use for temporary files.
    This routine should be called exactly once.
    We determine whether or not a candidate temp dir is usable by
    trying to create and write to a file in that directory.  If this
    is successful, the test file is deleted.  To prevent denial of
    service, the name of the test file must be randomized.
    **/
    public static function choiceDefaultTempdir() {
        var namer = new RandomNameSequence();
        for(dir in tempDirCandidates()){
            if(tryCreateFileIn(namer.next(), dir)){
                return dir;
            }
        }

        //TO-DO: define specific exception
        throw new Exception("Failed to find a valid temporary dir");
    }

    /**
        Generate a list of candidate temporary directories which will be tried.
    **/
    static function tempDirCandidates(){
        // First, try the environment.
        // Failing that, try OS-specific locations.
        var dirlist = envpaths().concat(systemPaths());

        // As a last resort, the current directory.
        dirlist.push(Sys.getCwd());

        return dirlist;
    }

    static function envpaths(): Array<String>{
        var dirlist = [];
        for(envname in ['TMPDIR', 'TEMP', 'TMP']){
            var dirname = Sys.getEnv(envname);
            if (dirname != null){
                dirlist.push(dirname);
            }
        }
        return dirlist;
    }

    static function systemPaths(): Array<String>{
        var systemPaths =
            if(Sys.systemName() == "Windows")
                ['~\\AppData\\Local\\Temp',
                '%SYSTEMROOT%\\Temp',
                'c:\\temp', 'c:\\tmp', '\\temp', '\\tmp'];
            else
                [ '/tmp', '/var/tmp', '/usr/tmp' ];

        return [for(p in systemPaths) FileSystem.fullPath(p)];
    }

    static function tryCreateFileIn(filename:String, dir:String): Bool {
        var filepath = Path.join([dir, filename]);
        var success = true;

        try{
            File.saveContent(filepath, "Lorem ipsum");
        }
        catch(e){
            success = false;
        }
        try{
            FileSystem.deleteFile(filepath);
        }
        catch(e){
            success = false;
        }

        return success;
    }
}

/**
    An instance of RandomNameSequence generates an endless sequence of unpredictable strings which can safely be incorporated into file names.
    Each string is eight characters long.
**/
private class RandomNameSequence {
    static var characters:String = "abcdefghijklmnopqrstuvwxyz0123456789";

    var numChars:Int;

    public function new(numChars:Int=8) {
        this.numChars = numChars;
    }

    public function next(): String {
        var buf = new StringBuf();

        for (i in 0...numChars){
            buf.addChar(choiceChar());
        }

        return buf.toString();
    }

    function choiceChar(): Int {
        var idx = Std.random(characters.length);
        return characters.charCodeAt(idx);
    }

}