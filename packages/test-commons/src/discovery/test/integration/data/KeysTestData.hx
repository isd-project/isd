package discovery.test.integration.data;

import discovery.keys.storage.PublicKeyData;
import discovery.format.EncodedData;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.storage.KeyData;
import discovery.format.Formats;
import discovery.keys.crypto.HashTypes;
import discovery.keys.storage.ExportedFingerprint;
import discovery.keys.crypto.KeyFormats;

class KeysTestData {

    public static final ellipticCurveJwk:ExportedKey = {
        format : Jwk,
        data : '{
    "kty" : "EC",
    "crv" : "P-256",
    "x" : "UsPLKLC4-1BNhhsjWdDn-CNUjiI3fbGZVnxQoHcqMCE",
    "y" : "CJbRJG1T79684NA83fa8TEIA1Vzm8-ouH1UwiYDkfnQ",
    "d" : "EE7vhGqAfU99UQmb5iuhs9KiH7k9ogBIZpgzgpN3fNU"
}'
   };

    public static final ellipticCurvePem:ExportedKey = {
        format: Pem,
        data: '-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgEE7vhGqAfU99UQmb
5iuhs9KiH7k9ogBIZpgzgpN3fNWgCgYIKoZIzj0DAQehRANCAARSw8sosLj7UE2G
GyNZ0Of4I1SOIjd9sZlWfFCgdyowIQiW0SRtU+/evODQPN32vExCANVc5vPqLh9V
MImA5H50
-----END PRIVATE KEY-----'
    };

    public static final elliptCurveKey:KeyData = {
        name: Random.string(8),
        privateKey: KeyFormats.PEM(
'-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgEE7vhGqAfU99UQmb
5iuhs9KiH7k9ogBIZpgzgpN3fNWgCgYIKoZIzj0DAQehRANCAARSw8sosLj7UE2G
GyNZ0Of4I1SOIjd9sZlWfFCgdyowIQiW0SRtU+/evODQPN32vExCANVc5vPqLh9V
MImA5H50
-----END PRIVATE KEY-----'),
        publicKey: KeyFormats.PEM(
'-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEUsPLKLC4+1BNhhsjWdDn+CNUjiI3
fbGZVnxQoHcqMCEIltEkbVPv3rzg0Dzd9rxMQgDVXObz6i4fVTCJgOR+dA==
-----END PUBLIC KEY-----'),
        keyType: EllipticCurve({namedCurve: "P-256"}),
        fingerprint: new ExportedFingerprint({
            alg: HashTypes.Sha256,
            hash: {
                data: "RrQvRwBSiQH510QyLtSkfHYvFUaeKlF2M1plSYiIjJc=",
                format: Formats.Base64Url
            }
        })
    };

    public static final rsaKey4096:PublicKeyData = {
        keyType : RSA({
                modulusLength : 4096,
                publicExponent : null
        }),
        fingerprint : {
            alg : HashTypes.Sha256,
            hash : new EncodedData({
                data: 'LH8jNKmM_gZ-ZyeBqUcdqg3ltY16rueSrKCG3HRWFGU',
                format: Base64Url
            })
        },
        key : {
            data: new EncodedData({
                data: 'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA09sTil'
                    + 'hsa3xWXqfrTly8m447j32C2PKw9yxPH3tVbIvYcNAgpg2jun2O'
                    + '5zEIigh0HotID4kR3hKF9LaSiPzzS7j9eQFIkRxt07Nf3XyA2X'
                    + 'lLxBtQtfOON8y1WESsK-qzjbOSnGVpG7O7VrIvVOWBUaV7Vhx9'
                    + 'veJ4Lfq4cNTc8UHrnnKnk8SqjvY2GxNhfmhIkeKgeTT_erRVgV'
                    + 'ssBx7tJTDIPIHrcdYrOYRQc4n-KrtmVW8RNIriw3CNaPiyfArD'
                    + 'o28LzRZRvquD1mm63sFf7UI0GgWGt6d6xvZXZvmKmxuC-67adL'
                    + 'aFVOgZOYuZENvcV7937UtBn7BvVilFgyE0AC0jqxV31Eon76YI'
                    + 'TTv4gTCdWGhIZm2Xae1Mki5aQ0I7NVYhUqdfv_d_tqY0WLOGKR'
                    + 'wJJoB5fUNgnig3NIpC_Pxe6SFRFWF69zenMHNIjvbfGpvAlEnf'
                    + 'jOq1aYf02m0ot06x7f5qfZNnAw5-mxPyBVJ-vHHymfimo77Zu1'
                    + 'zFjcMPMyP0EfYqUaD9BaaHxttt5MDHDOMoG_BGDP-DTaUpg9h0'
                    + 'L2sKbUwKuV3Z8LIs2pM4mdUDkYD6MmO42dzKtFj3KmPb4Y1TXf'
                    + 'coNMfn3sPQ29QAbqCwUygw5S8OwbN2m6sT36MbjIQX9gRJZZQm'
                    + 'KeVHOCVvQfWOW5Z8bJsdJsW4a08CAwEAAQ',
                format: Base64Url
            }),
            format : Der
        }
    };
}