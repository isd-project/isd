package discovery.test.steps;

import discovery.domain.serialization.Serializer;
import hxgiven.Steps;

using discovery.test.matchers.CommonMatchers;


abstract
class SerializerTestSteps<T, Self:SerializerTestSteps<T, Self>>
    extends Steps<Self>
{
    var serializer:Serializer<T>;

    var data:T;
    var deserializedData:T;
    var serializedData:Any;

    public function new(){
        super();
    }

    abstract function buildSerializer():Serializer<T>;

    // given ---------------------------------------------------

    @step
    public function a_serializer_instance() {
        serializer = buildSerializer();
    }


    @step
    public function this_data(data: T) {
        this.data = data;
    }

    // when  ---------------------------------------------------

    @step
    public function serializing_this(data: T) {
        given().this_data(data);
        when().serializing_it();
    }

    @step
    public function serializing_it() {
        serializedData = serializer.serialize(data);
    }

    @step
    public function deserializing_a_copy_back() {
        var serializedCopy  = Reflect.copy(serializedData);

        deserializedData = serializer.deserialize(serializedCopy);
    }

    // then  ---------------------------------------------------

    @step
    public function a_new_equals_instance_should_be_produced() {
        deserializedData.shouldNotBeTheSame(data);
        deserializedData.shouldBeEqualsTo(data);
    }
}