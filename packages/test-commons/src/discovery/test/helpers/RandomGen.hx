package discovery.test.helpers;

import haxe.io.Path;
import discovery.domain.IdentifierUrl;
import discovery.domain.Query;
import discovery.keys.Key;
import discovery.domain.PublishInfo;
import discovery.test.helpers.generators.PrimitivesGenerator;
import discovery.domain.Validity;
import discovery.test.helpers.generators.TimeGenerator;
import discovery.domain.Announcement;
import discovery.domain.Identifier;
import discovery.domain.Discovered;
import discovery.keys.crypto.signature.Signed;
import discovery.domain.IPAddress;
import discovery.test.helpers.generators.BinaryGen;
import discovery.test.helpers.generators.CryptoGen;
import discovery.domain.Description;
import haxe.io.Bytes;


class RandomGen {
    public static final primitives = new PrimitivesGenerator();
    public static final binary = new BinaryGen(primitives);
    public static final crypto = new CryptoGen(binary);
    public static final time = new TimeGenerator();

    public static function bytes(length:Int): Bytes {
        return binary.bytes(length);
    }
    public static function hex(length:Int): String {
        return binary.hex(length);
    }

    @:deprecated
    public static function byte(?min:Int, ?max:Int):Int {
        return primitives.int(min, max);
    }

    public static function int(?from:Int, ?to:Int) {
        return primitives.int(from, to);
    }

    public static function name(?min: Int, ?max: Int):String
    {
        return primitives.name(min, max);
    }

    @:deprecated
    public static function choice(value1:Any, value2:Any):Any {
        return primitives.choice(value1, value2);
    }

    @:deprecated
    public static function maybe<T>(value: T): Null<T> {
        return primitives.maybe(value);
    }

    public static function identifier():Identifier {
        var identifier = description().identifier();
        identifier.providerId = binary.bytes(int(8, 16));

        return identifier;
    }



    public static function typeQuery(): Query {
        return {
            type: serviceType()
        };
    }

    public static function
        discoveredWithAttributes(attrs:Attributes):Discovered
    {
        var d = discovered();
        d.description().attributes = attrs;

        return d;
    }

    public static function discovered():Discovered{
        return fullAnnouncement();
    }

    public static function expiredAnnouncement():Announcement {
        var validUntil = time.anyDateBefore(Date.now());
        var validFrom = time.anyDateBefore(validUntil);

        var validity = new Validity(validUntil, validFrom);

        return Announcement.make(fullDescription(), validity);
    }

    public static
        function publishInfo(?key: Key, ?desc: Description): PublishInfo
    {
        return PublishInfo.make(fullAnnouncement(desc), key);
    }

    public static function fullAnnouncement(?desc: Description): Announcement {
        if(desc == null) desc = fullDescription();

        desc.providerId = crypto.fingerprint(Random.int(32, 64));

        return announcementFrom(desc);
    }

    public static
    function announcementFrom(description: Description): Announcement{
        return Announcement.make(description, time.duration());
    }


    public static function fullDescription():Description {
        var desc = discoveredDescription();
        desc.location = location();
        desc.attributes = descriptionAttributes();
        return desc;
    }

    public static function discoveredDescription(): Description {
        var desc = description();
        desc.providerId = crypto.fingerprint(int(32,64));

        return desc;
    }

    public static function description():Description {
        var desc = basic_description();
        desc.port = port();

        return desc;
    }

    public static function basic_description():Description {
        return {
            name: name(),
            type: serviceType(),
            providerId: crypto.keyIdentifier(),
            instanceId: primitives.maybe(binary.bytes(int(4,12)))
        };
    }

    public static function serviceType():String {
        return name(4,10);
    }

    public static function location():Location {
        return {
            defaultPort: port(),
            channels: [for (i in 0...int(1,5)) channel()],
            addresses: addresses(Random.int(1, 5))
        };
    }

    public static function descriptionAttributes():Attributes {
        var attributes:Attributes = {};

        for(i in 0...int(1,5)){
            //set random attributes (string or bytes)
            var value = primitives.choice(name(), bytes(int(5, 10)));

            attributes.set(name(), value);
        };

        return attributes;
    }

    public static function channel(): Channel {
        var channel:Channel = {};

        while(channel.isEmpty()){
            if(Random.bool()) channel.port = port();
            if(Random.bool()) channel.protocol = name();
            if(Random.bool()) channel.address = name();
        }

        return channel;
    }

    public static function addresses(length:Int):Array<IPAddress> {
        return [for(i in 0...length) address()];
    }

    public static function address():IPAddress {
        if(Random.bool()){
            return ipv4Address();
        }
        else{
            return ipv6Address();
        }
    }

    public static function ipv4Address() {
        var byte = binary.byte;

        return '${byte(10, 255)}.${byte()}.${byte()}.${byte()}';
    }

    public static function ipv6Address(): IPAddress {
        var numGroups = primitives.int(1,8);
        if(numGroups == 7){
            //avoid case when only one group is supressed
            numGroups = 8;
        }

        var supressedGroup = if(numGroups == 8) null else primitives.int(0,numGroups - 1);

        var strBuf = new StringBuf();
        for(i in 0...numGroups){
            var group = removeZeroPrefix(binary.hex(4));

            if(group == ''){
                group = '1'; //avoid group of only zeroes
            }

            strBuf.add(group);

            if(i == supressedGroup){
                strBuf.add('::');
            }
            else if(i + 1 < numGroups){
                strBuf.add(':');
            }
        }

        return strBuf.toString();
    }

    static function removeZeroPrefix(group:String):String {
        if(group == null || group.length == 0) return group;

        var countZero = 0;
        while(countZero < group.length
            && group.charCodeAt(countZero) == '0'.code)
        {
            countZero++;
        }

        return group.substr(countZero);
    }

    public static function port():Int {
        return Random.int(1024, 65535);
    }

    public static function identifierUrl():IdentifierUrl {
        return IdentifierUrl.fromIdentifier(identifier());
    }

    public static function filepath(?filename:String):String {
        if(filename == null) filename = primitives.name() + '.example';

        return Path.join([dirpath(), filename]);
    }

    public static function dirpath():String {
        var numSegments = primitives.int(2, 6);

        var segments = [for(i in 0...numSegments) primitives.name()];

        return Path.join(segments);
    }
}