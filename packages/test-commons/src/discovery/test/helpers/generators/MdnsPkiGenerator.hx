package discovery.test.helpers.generators;

import discovery.format.Formatters;
import discovery.mdns.pki.search.KeyEnquirer.KeyPart;
import discovery.test.helpers.RandomGen;
import discovery.mdns.pki.KeyDescriptor;
import discovery.mdns.pki.messages.KeyPartReference;

class MdnsPkiGenerator {
    public function new() {}

    public function keyDescriptor(?minParts:Int, ?maxParts:Int): KeyDescriptor {
        var fingerprint = RandomGen.crypto.fingerprint();
        return {
            fingerprint: fingerprint,
            keyType: RandomGen.crypto.keyType(),
            keySize: RandomGen.primitives.int(256, 4096),
            keyFormat: RandomGen.crypto.keyFormat(),
            keyParts: keyPartsReferences(minParts, maxParts, fingerprint)
        };
    }

    public function keyPartsReferences(
                        minParts:Int = 1, maxParts:Int = 5,
                        ?keyId: KeyPartKeyId):Array<KeyPartReference>
    {
        if(keyId == null){
            keyId = keyPartKeyId();
        }
        var length = RandomGen.primitives.int(minParts, maxParts);

        return [for(i in 0...length) keyPartReference(keyId)];
    }

    public function keyPartReference(?keyId: KeyPartKeyId):KeyPartReference {
        return {
            part: RandomGen.primitives.name(),
            keyId: if(keyId != null) keyId else keyPartKeyId()
        };
    }

    function keyPartKeyId():KeyPartKeyId {
        return RandomGen.crypto.fingerprint();
    }

    public function keyPartsFor(keyDescriptor: KeyDescriptor): Array<KeyPart>{
        var numParts = keyDescriptor.keyParts.length;

        return [for(i in 0...numParts) someKeyPart()];
    }

    public function someKeyPart():KeyPart {
        return {
            data: RandomGen.binary.bytes(RandomGen.primitives.int(10,30))
        };
    }
}