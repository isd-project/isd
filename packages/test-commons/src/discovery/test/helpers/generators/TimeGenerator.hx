package discovery.test.helpers.generators;

import discovery.utils.time.TimeInterval;
import discovery.utils.time.Duration;

class TimeGenerator {
    public function new() {}

    public function anyDate():Date {
        return Date.fromTime(anyMillisTimestamp());
    }

    public function anyTime(): TimeUnit {
        return anyMillisTimestamp();
    }

    public function anyMillisTimestamp(?from:Float, ?to:Float){
        final three_thousand_years_in_millis = 3000*365*24*60*60*1000;

        from = if(from != null) from else 0;
        to = if(to != null) to else from + three_thousand_years_in_millis;

        return Math.abs(Random.float(from, to));
    }

    public function anyDateAfter(date:Date):Date {
        var timestamp = date.getTime() + anyMillisTimestamp();
        return Date.fromTime(timestamp);
    }

    public function anyDateBefore(date:Date):Date {
        var timestamp = date.getTime() - anyMillisTimestamp();
        return Date.fromTime(if(timestamp < 0) 0 else timestamp);
    }

    public function anyDateBetween(fromDate:Date, toDate:Date) {
        var fromTime = fromDate.getTime();
        var toTime   = toDate.getTime();
        var timestampMillis = anyMillisTimestamp(fromTime, toTime);

        return Date.fromTime(timestampMillis);
    }

    public function duration():Duration {
        return Duration.of(anyMillisTimestamp(), Milliseconds);
    }

    public function durationBetween(from:Duration, to:Duration):Duration {
        var time = anyMillisTimestamp(from.time(), to.time());
        return Duration.of(time, Milliseconds);
    }

    public function interval():TimeInterval {
        var start = anyDate();
        var end = anyDateAfter(start);

        return new TimeInterval(start, end);
    }
}