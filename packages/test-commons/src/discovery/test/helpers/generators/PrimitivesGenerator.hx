package discovery.test.helpers.generators;

class PrimitivesGenerator {

    public function new() {}

    public function int(?from:Int, ?to:Int) {
        final MAX_INT = 0x7FFFFFFF;
        final MIN_INT = -MAX_INT;

        from = if(from == null) MIN_INT else from;
        to   = if(to == null)   MAX_INT else to;

        return Random.int(from, to);
    }


    public function float(from:Float, to:Float): Float {
        return Random.float(from, to);
    }

    public function names(min:Int=1, max:Int=5) {
        var amount = int(min, max);

        return [for(i in 0...amount) name()];
    }

    public function name(min: Int=4, max: Int=25):String
    {
        return Random.string(int(min, max));
    }

    public function choice(value1:Dynamic, value2:Dynamic):Dynamic {
        return if(Random.bool()) value1 else value2;
    }

    public function pickOne<T>(values:Array<T>):T {
        if(values.length == 0) return null;

        var idx = int(0, values.length - 1);
        return values[idx];
    }

    public function maybe<T>(value: T): Null<T> {
        return if(Random.bool()) value else null;
    }

    public function boolean() {
        return Random.bool();
    }

    public function shuffle<T>(items:Array<T>):Array<T> {
        return Random.shuffle(items);
    }
}