package discovery.test.helpers.generators;

import discovery.format.Binary;
import discovery.format.Formats;
import haxe.io.Bytes;

class BinaryGen {
    var primitives: PrimitivesGenerator;

    public function new(primitives: PrimitivesGenerator) {
        this.primitives = primitives;
    }

    public function byte(min:Int=0, max:Int=255):Int {
        return primitives.int(min, max);
    }

    public function bytes(length:Int): Bytes {
        return Bytes.ofHex(hex(length * 2));
    }
    public function hex(length:Int): String {
        return Random.string(length, "0123456789abcdef");
    }

    public function formattedBytes(fmt:Formats, minLength:Int, maxLength:Int) {
        var length = primitives.int(minLength, maxLength);

        return fmt.getFormatter().encode(bytes(length));
    }

    public function binary(minLength:Int=5, maxLength:Int=20) {
        final length = primitives.int(minLength, maxLength);

        return Binary.fromBytes(this.bytes(length));
    }
}