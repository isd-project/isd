package discovery.test.helpers.generators;

import discovery.keys.storage.PublicKeyData;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.signature.Signed;
import discovery.keys.crypto.signature.Signature;
import discovery.keys.crypto.KeyTypes.EllipticCurveParameters;
import discovery.keys.crypto.KeyType;
import discovery.keys.crypto.KeyTypes.RSAKeyParameters;
import haxe.crypto.Base64;
import discovery.format.BytesOrText;
import discovery.keys.crypto.ExportedKey;
import discovery.keys.crypto.KeyFormat;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.crypto.HashTypes;
import discovery.keys.storage.KeyData;
import discovery.keys.KeyIdentifier;

import Random.*;

typedef ExportedKeyOptions = {
    ?format: KeyFormat,
    ?privateKey: Bool,
    ?length: Int
};

class CryptoGen {
    var binaryGen:BinaryGen;

    public function new(binaryGen:BinaryGen) {
        this.binaryGen = binaryGen;
    }

    public function keyIdentifier():KeyIdentifier {
        if(Random.bool()){
            return binaryGen.hex(Random.int(8,128));
        }
        else{
            return binaryGen.bytes(Random.int(4, 64));
        }
    }

    public function fingerprint(?hashLen:Int): Fingerprint {
        hashLen = if(hashLen != null) hashLen else 32;

        return {
            alg: hashType(),
            hash: RandomGen.bytes(hashLen)
        }
    }

    public function hashType(): HashTypes{
        return Random.fromArray(HashTypes.values);
    }

    public function keyData():KeyData {
        return {
            fingerprint: fingerprint(),
            publicKey: exportedKey(),
            privateKey: exportedKey(true),
            name: if(bool()) string(int(4, 15)) else null,
            keyType: keyType()
        };
    }

    public function publicKeyData():PublicKeyData {
        return {
            fingerprint: fingerprint(),
            key: exportedKey(),
            keyType: keyType()
        };
    }

    public function exportedKey(privateKey:Bool=false):Null<ExportedKey> {
        return exportedKeyWithFormat(keyFormat(), privateKey);
    }

    public function exportedKeyWithFormat(
        format: KeyFormat, privateKey:Bool=false):Null<ExportedKey>
    {
        return {
            data: exportedKeyData({
                format: format,
                privateKey: privateKey
            }),
            format: format
        };
    }

    public function exportedKeyWith(opt:ExportedKeyOptions):ExportedKey {
        if(opt == null) opt = {};
        if(opt.format == null) opt.format = keyFormat();

        return {
            format: opt.format,
            data: exportedKeyData(opt)
        };
    }

    function exportedKeyData(opt: ExportedKeyOptions): BytesOrText {
        var format = if(opt.format != null) opt.format else keyFormat();

        return switch(format){
            case Der: derData(opt.length);
            case Pem: pemData(opt);
            default: null;
        };
    }

    function derData(?length: Int): BytesOrText {
        if(length == null){
            length = int(32, 512);
        }
        return binaryGen.bytes(length);
    }

    function pemData(opt: ExportedKeyOptions): BytesOrText {
        var length = opt.length;
        if(length == null){
            length = int(256, 4096);
        }

        var keyTypeName = if(opt.privateKey==true) 'PRIVATE' else 'PUBLIC';

        return
            '-----BEGIN $keyTypeName KEY-----\n' +
            string(length, Base64.CHARS) + '\n' +
            '-----END $keyTypeName KEY-----';
    }

    public function keyFormat():KeyFormat {
        return fromArray([KeyFormat.Pem, KeyFormat.Der]);
    }

    public function keyType():Null<KeyType> {
        var useParams = bool();
        if(bool()){
            var params:RSAKeyParameters = if(!useParams) null
                else
                    {modulusLength: int(1024,4096)};

            return RSA(params);
        }
        else{
            var params:EllipticCurveParameters = if(!useParams) null
                else { namedCurve: string(int(3, 8))};

            return EllipticCurve(params);
        }
    }

    public function signature():Signature {
        return binaryGen.bytes(Random.int(16, 64));
    }

    public function signed<T>(value:T): Signed<T> {
        return Signed.make(value, signature());
    }
}