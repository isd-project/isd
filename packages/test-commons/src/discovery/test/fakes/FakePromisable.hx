package discovery.test.fakes;

import discovery.async.promise.PromisableFactory;
import discovery.async.promise.PromiseInitCallback;
import discovery.async.promise.Promisable;
import discovery.async.promise.PromiseHandler;

private typedef PromiseListener<T> = {
    handler: PromiseHandler<T, Dynamic>,
    next: FakePromisable<Dynamic>
};

class FakePromisable<T> implements Promisable<T>{

    static final promiseFactory = new FakePromisableFactory();


    public var resolvedValue(get, null):Null<T>;

    var resolver:FakePromisableResolver<T>;
    var rejector:FakePromisableResolver<Dynamic>;

    public static function resolved<T>(value:T): FakePromisable<T> {
        var promise = new FakePromisable();
        promise.resolve(value);
        return promise;
    }

    public function new(?resolvedValue: T, ?rejectedError:Dynamic) {
        resolver = new FakePromisableResolver(resolvedValue);
        rejector = new FakePromisableRejector(rejectedError);
    }

    function get_resolvedValue(): T {
        return resolver.resolvedValue;
    }

    public function factory():PromisableFactory {
        return promiseFactory;
    }

    inline public static
    function makePromise<T>(init: PromiseInitCallback<T>): FakePromisable<T>{
        var promise = new FakePromisable<T>();
        var resolve = promise.resolve;
        var reject  = promise.reject;

        try{
            init(resolve, reject);
        }
        catch(err){
            reject(err);
        }

        return promise;
    }

    public function then<TOut>(
        onFulfilled:Null<PromiseHandler<T, TOut>>,
        ?onRejected:PromiseHandler<Dynamic, TOut>):Promisable<TOut>
    {

        var next = new FakePromisable<TOut>();
        resolver.registerPromiseListener(onFulfilled, next);
        rejector.registerPromiseListener(onRejected, next);
        return next;
    }

    public
    function catchError(onRejected:PromiseHandler<Dynamic, T>):Promisable<T>
    {
        return then((result)->{return result;}, onRejected);
    }

    public function finally(onFinally:() -> Void):Promisable<T>{
        return then((result)->{
            onFinally();
            return result;
        }, (err)->{
            onFinally();

            throw err;
        });
    }

    public function resolve(value: T): Void{
        resolver.resolve(value);
    }

    public function reject(err: Dynamic) {
        rejector.resolve(err);
    }

    public function mergeWith(promisables:Array<Promisable<Dynamic>>):Promisable<Array<Dynamic>>
    {
        var allPromisables = [(this : Promisable<Dynamic>)].concat(promisables);

        return mergeAll(allPromisables);
    }

    public static function mergeAll(promisables:Array<Promisable<Any>>):Promisable<Array<Any>> {
        var resultPromise = new FakePromisable<Array<Dynamic>>();

        var results:Array<Dynamic>=[];
        results.resize(promisables.length);

        var total=results.length;
        var count=0;
        for (i in 0...total){
            var p = promisables[i];
            p.then(
                (result)->{
                    results[i] = result;
                    count++;

                    if(count == total){resultPromise.resolve(results);}
                    return result;
                },
                (error)->{
                    resultPromise.reject(error);
                    return error;
                }
            );
        }

        return resultPromise;
    }
}

class FakePromisableResolver<T> {
    public var resolvedValue(default, null):Null<T>;

    var alreadyResolved:Bool;
    var listeners:List<PromiseListener<T>>;

    public function new(?resolvedValue: T) {
        this.resolvedValue = resolvedValue;
        this.alreadyResolved = (resolvedValue != null);

        this.listeners = new List();
    }

    public function registerPromiseListener<TOut>(
        handler:Null<PromiseHandler<T, TOut>>,
        next:Promisable<TOut>)
    {
        var listener:PromiseListener<T> = {
            handler: handler,
            next: cast next
        };

        if(!alreadyResolved){
            listeners.add(listener);
        }
        else {
            propagateResult(resolvedValue, listener);
        }
    }


    public function resolve(value:T) {
        this.resolvedValue = value;
        this.alreadyResolved = true;

        for(listener in listeners){
            propagateResult(value, listener);
        }
        listeners.clear();
    }

    function propagateResult<T>(value:T, listener: Null<PromiseListener<T>>): Void {
        try{
            tryPropagateResult(value, listener);
        }
        catch(err){
            listener.next.reject(err);
        }
    }

    function tryPropagateResult<T>(value:T, listener: Null<PromiseListener<T>>){
        if(listener == null){
            return;
        }

        if(listener.handler == null){ //just pass the result through the chain
            propagateResultToNext(value, listener);
            return;
        }

        var callback:T->Dynamic = cast listener.handler;
        var nextValue = callback(value);


        if(Std.isOfType(nextValue, Promisable)){
            var returnedPromise:Promisable<Any> = nextValue;
            returnedPromise.then(
                listener.next.resolve,
                listener.next.reject);
        }
        else{
            listener.next.resolve(nextValue);
        }
    }

    public function popListener():Null<PromiseListener<T>> {
        return listeners.pop();
    }

    function propagateResultToNext<T>(
        nextValue:Dynamic, listener: PromiseListener<T>)
    {
        listener.next.resolve(nextValue);
    }
}


class FakePromisableRejector<T> extends FakePromisableResolver<T>
{
    override function propagateResultToNext<T>(
        nextValue:Dynamic, listener: PromiseListener<T>)
    {
        listener.next.reject(nextValue);
    }
}