package discovery.test.fakes;

import js.node.events.EventEmitter.Event;
import js.node.events.EventEmitter.IEventEmitter;
import haxe.extern.EitherType;
import js.lib.Symbol;
import discovery.utils.events.EventEmitterFactory;
import discovery.utils.events.EventEmitter;
import haxe.Rest;
import haxe.Constraints.Function;

private typedef DefaultEventEmitterFactory
    = discovery.impl.Defaults.EventEmitterFactory;


#if(js && nodejs)
typedef EventName<T:Function> = js.node.events.EventEmitter.Event<T>;
#else
typedef EventName<T> = String;
#end

private typedef Self = JsEventEmitterFake;

#if(js && nodejs)
class JsEventEmitterFake implements js.node.events.EventEmitter.IEventEmitter
#else
class JsEventEmitterFake
#end
{

    var listenersMap: Map<String, EventEmitter<Array<Dynamic>>>;
    var eventsFactory:EventEmitterFactory;

    public function new(?eventsFactory: EventEmitterFactory) {
        listenersMap = [];
        this.eventsFactory = if(eventsFactory != null) eventsFactory
                             else new DefaultEventEmitterFactory();
    }

    public function on<T:Function>(evt: EventName<T>, callback: T){
        getEmitter(evt).listen(function(args: Array<Dynamic>){
            Reflect.callMethod(null, callback, args);
        });

        return this;
    }

    public function emit<T:Function>(evt:EventName<T>, args:Rest<Dynamic>):Bool{
        var emitter = getEmitter(evt);

        emitter.notify(args.toArray());

        return true; //should return true only if some listener was called
    }

    function getEmitter(evtName:String): EventEmitter<Array<Dynamic>>{
        var evtEmitter = listenersMap.get(evtName);

        if(evtEmitter == null){
            evtEmitter = eventsFactory.eventEmitter();
            listenersMap.set(evtName, evtEmitter);
        }

        return evtEmitter;
    }


    #if (js && nodejs)
	public function addListener<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function eventNames():Array<EitherType<String, Symbol>> {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function getMaxListeners():Int {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function listenerCount<T:Function>(eventName:Event<T>):Int {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function listeners<T:Function>(eventName:Event<T>):Array<T> {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function off<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function once<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function prependListener<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function prependOnceListener<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function removeAllListeners<T:Function>(?eventName:Event<T>):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function removeListener<T:Function>(eventName:Event<T>, listener:T):IEventEmitter {
		throw new haxe.exceptions.NotImplementedException();
	}

	public function setMaxListeners(n:Int) {}

	public function rawListeners<T:Function>(eventName:Event<T>):Array<T> {
		throw new haxe.exceptions.NotImplementedException();
	}
    #end
}