package discovery.test.fakes;

import discovery.async.promise.PromiseInitCallback;
import discovery.async.promise.Promisable;
import haxe.Exception;
import discovery.async.promise.PromisableFactory;

class FakePromisableFactory implements PromisableFactory{

    public function new() {}

    public function promise<T>(callback:PromiseInitCallback<T>):Promisable<T> {
        return FakePromisable.makePromise(callback);
    }

    public function resolved<T>(value:T):Promisable<T> {
        return FakePromisable.resolved(value);
    }

    public function rejected<T>(error:Exception):Promisable<T> {
        var promise = new FakePromisable();
        promise.reject(error);

        return promise;
    }

    public function mergeAll(
        promises:Array<Promisable<Any>>):Promisable<Array<Any>>
    {
        return FakePromisable.mergeAll(promises);
    }
}