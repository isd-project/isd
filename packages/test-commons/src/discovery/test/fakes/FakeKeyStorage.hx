package discovery.test.fakes;

import discovery.format.Binary;
import discovery.keys.storage.KeyData;
import discovery.keys.KeyIdentifier;
import discovery.keys.storage.KeyStorage;

class FakeKeyStorage implements KeyStorage{
    var keys:Map<String, KeyData>;

    var lastAddedKey:KeyData = null;

    public function new() {
        keys = new Map();
    }

    public function listKeys():Array<KeyData> {
        return [for(k in keys.iterator()) k];
    }

    public function getKey(keyIdentifier:KeyIdentifier):KeyData {
        return keys.get(key(keyIdentifier));
    }

    public function addKey(key:KeyData) {
        setKey(key);

        lastAddedKey = key;
    }

    public function lastStoredKey():KeyData {
        return lastAddedKey;
    }

    public function update(key:KeyData) {
        setKey(key);
    }

    function setKey(keyData: KeyData) {
        keys.set(key(keyData.fingerprint.hash), keyData);
    }

    public function remove(keyId:KeyIdentifier) {
        keys.remove(key(keyId));
    }

    function key(keyIdentifier:KeyIdentifier) {
        return keyIdentifier.toBytes().toHex();
    }
}