package discovery.test.matchers;

import massive.munit.AssertionException;
import discovery.utils.IteratorTools;
import haxe.Exception;
import discovery.test.common.transformations.SerializationTransform;
import org.hamcrest.Matchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers.assertThat;
import org.hamcrest.Matchers.is;
import org.hamcrest.Matchers.notNullValue;
import org.hamcrest.Matchers.both;
import org.hamcrest.Matchers.greaterThanOrEqualTo;
import org.hamcrest.Matchers.lessThanOrEqualTo;

using discovery.utils.EqualsComparator;


class CommonMatchers {
    public static function shouldNotBeNull<T>(value: T, ?msg: String) {
        assertThat(value, is(notNullValue()), msg);
    }

    public static function shouldBe<T>(found: T, expected: T, ?msg:String){
        return shouldBeEqualsTo(found, expected, msg);
    }

    public static function shouldNotBe<T>(found: T, expected: T, ?msg:String){
        return shouldNotBeEqualsTo(found, expected, msg);
    }

    public static function
        shouldBeEqualsTo<T>(found:T, expected:T, ?msg:String)
    {
        equalsShouldBe(found, expected, msg, true);
    }
    public static function
        shouldNotBeEqualsTo<T>(found:T, expected:T, ?msg:String)
    {
        equalsShouldBe(found, expected, msg, false);
    }

    static function equalsShouldBe<T>(
        found:T, expected:T, msg:Null<String>, expectedResult:Bool)
    {
        assertThat(found.equals(expected), is(expectedResult),
            makeEqualsMessage(msg, found, expected, expectedResult)
        );
    }

    static function makeEqualsMessage<T>(
        msg:Null<String>, found:T, expected:T, shouldBeEquals:Bool): String
    {
        var output = new StringBuf();

        if(msg != null){
            output.add(msg);
            output.add('\n');
        }

        output.add('Expected');

        if(!shouldBeEquals){
            output.add(' not to be');
        }

        output.add(': ${tryToString(expected)}\n');
        output.add('but found: ${tryToString(found)}');

        return output.toString();
    }

    public static
    function shouldBeGreaterThan<T>(value: T, other: T, ?msg: String)
    {
        assertThat(value, is(Matchers.greaterThan(other)), msg);
    }

    public static
    function shouldBeGreaterThanOrEqualsTo<T>(value: T, other: T, ?msg: String)
    {
        assertThat(value, is(Matchers.greaterThanOrEqualTo(other)), msg);
    }

    public static
    function shouldBeLessThanOrEqualsTo<T>(value: T, other: T, ?msg: String)
    {
        assertThat(value, is(Matchers.lessThanOrEqualTo(other)), msg);
    }


    public static
    function shouldBeBetween<T>(value: T, lower: T, upper:T, ?msg: String)
    {
        assertThat(value, is(both(greaterThanOrEqualTo(lower))
                                .and(lessThanOrEqualTo(upper))), msg);
    }


    public static
    function shouldBeAn<T, X>(value: T, type: Class<X>, ?msg:String) {
        assertThat(value, Matchers.isA(type), msg);
    }


    public static function shouldBeTheSame<T>(value: T, other: T, ?msg: String) {
        assertThat(value, Matchers.is(Matchers.theInstance(other)), msg);
    }

    public static function shouldNotBeTheSame<T>(value: T, other: T, ?msg: String) {
        assertThat(value, Matchers.not(Matchers.theInstance(other)), msg);
    }


    public static
    function shouldStartWith(value: String, prefix: String, ?msg: String)
    {
        assertThat(value, Matchers.startsWith(prefix), msg);
    }

    public static
    function shouldContainString(value: String, part: String, ?msg:String)
    {
        assertThat(value, Matchers.containsString(part), msg);
    }


    public static
    function shouldNotBeEmpty<T, I:Iterable<T>>(iterable:I, ?msg:String) {
        assertThat(iterable, Matchers.not(Matchers.isEmpty()), msg);
    }

    public static
    function shouldBeEmpty<T, I:Iterable<T>>(iterable:I, ?msg:String) {
        assertThat(iterable, Matchers.isEmpty(), msg);
    }

    public static
    function shouldContainItems<T>(array: Array<T>, expected:Array<T>)
    {
        assertThat(array, Matchers.hasItems(expected),
            'Array should have items: ${expected}\n' +
            '\tbut found: ${array}'
        );
    }

    public static
        function shouldContainsInAnyOrder<T, Iter:Iterable<T>>(
            value:Iter, expectedItems:Iter, ?msg:String)
    {
        var toMatch = IteratorTools.toArray(value);

        for(expected in expectedItems){
            var matched = false;

            for(i in 0...toMatch.length){
                var item = toMatch[i];

                if(item.equals(expected)){
                    matched = true;
                    toMatch.splice(i, 1); //remove item
                    break;
                }
            }

            if(matched == false){
                throw new AssertionException(
                    '${value} should have ${expectedItems} in any order.\n'
                    + '\tbut: ${expected} was not found');
            }
        }
    }

    public static function shouldMatch<T>(
        value:T,
        matcher:(value:T) -> Bool,
        ?description:String,
        ?mismatchDescriptor: (T)->String):Matcher<T>
    {
        return shouldMatchWithType(value, Dynamic, matcher, description, mismatchDescriptor);
    }

    public static function shouldMatchWithType<T>(
        value: T,
        type: Dynamic,
        matcher:(value:T) -> Bool,
        ?description:String,
        ?mismatchDescriptor: (T)->String):Matcher<T>
    {
        if(description == null) description = "a value matching custom function " + Std.string(matcher);

        return new FunctionMatcher(type, matcher, description, mismatchDescriptor);
    }

    static function tryToString<T>(value:T) {
        var toStringErr:Exception;

        try{
            return Std.string(value);
        }
        catch(e)
        {
            toStringErr = e;
        }

        //Ignore error and try with json
        try{
            return SerializationTransform.jsonStringify(value);
        }
        catch(e){
            //failed
            return '{\n' +
            '\tError: Could not represent as string or Json.\n' +
            '\ttoString err: ${toStringErr}.\n' +
            '\ttoJson err: ${e}.\n' +
            '}';
        }
    }
}