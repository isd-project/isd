package discovery.test.matchers;

import org.hamcrest.CustomTypeSafeMatcher;

class FunctionMatcher<T> extends CustomTypeSafeMatcher<T> {
    var matcher:(T)->Bool;
    var expectedType:Dynamic;
    var mismatchDescriptor:(T)->String;

    public function new(expectedType:Dynamic, matcher:(T)->Bool, ?description:String, ?mismatchDescriptor:(T)->String) {
        if(description == null)
            description = "a value matching custom function " + Std.string(matcher);

        super(description);

        this.expectedType = expectedType;
        this.matcher = matcher;
        this.mismatchDescriptor = mismatchDescriptor;
    }
    override function matchesSafely(item:T):Bool{
        return matcher(item);
    }

    override function describeMismatchSafely(item:T, mismatchDescription:org.hamcrest.Description):Void
    {
        var desc:String;

        if(this.mismatchDescriptor != null){
            desc = this.mismatchDescriptor(item);
        }
        else{
            desc = "found: " + Std.string(item);
        }

        mismatchDescription.appendText(desc);
    }

    override function isExpectedType(value:Dynamic):Bool
    {
        return Std.isOfType(value, expectedType);
    }
}