/****
* Copyright 2017 Massive Interactive. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are
* permitted provided that the following conditions are met:
*
*    1. Redistributions of source code must retain the above copyright notice, this list of
*       conditions and the following disclaimer.
*
*    2. Redistributions in binary form must reproduce the above copyright notice, this list
*       of conditions and the following disclaimer in the documentation and/or other materials
*       provided with the distribution.
*
* THIS SOFTWARE IS PROVIDED BY MASSIVE INTERACTIVE ``AS IS'' AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL MASSIVE INTERACTIVE OR
* CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
* ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
* NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
* ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* The views and conclusions contained in the software and documentation are those of the
* authors and should not be interpreted as representing official policies, either expressed
* or implied, of Massive Interactive.
****/

package discovery.test.munit;

import massive.munit.ITestResultClient;
import massive.munit.TestResult;


/**
 * Abstract class to help create decorators for ITestResultClient's
 */
class AbstractReporterDecorator implements IAdvancedTestResultClient{


    /**
     * The unique identifier for the client.
     */
    public var id(default, null):String;

    /**
     * Handler which if present, is called when the client has completed sending the test results to the specificied url.
     */
    public var completionHandler(get, set):ITestResultClient->Void;

    private function get_completionHandler():ITestResultClient->Void
    {
        return client.completionHandler;
    }
    private function set_completionHandler(value:ITestResultClient->Void):ITestResultClient->Void
    {
        return client.completionHandler = value;
    }

    private var client:ITestResultClient;

    /**
     * @param    id                    client unique identifier
     * @param    client                the test result client to decorate
     */
    public function new(id: String, client:ITestResultClient)
    {
        this.id     = id;
        this.client = client;
    }

    /**
     * Classed when test class changes
     *
     * @param className        qualified name of current test class
     */
    public function setCurrentTestClass(className:String)
    {
        if(Std.isOfType(client, IAdvancedTestResultClient))
        {
            cast(client, IAdvancedTestResultClient).setCurrentTestClass(className);
        }
    }

    /**
     * Called when a test passes.
     *
     * @param    result            a passed test result
     */
    public function addPass(result:TestResult)
    {
        client.addPass(result);
    }

    /**
     * Called when a test fails.
     *
     * @param    result            a failed test result
     */
    public function addFail(result:TestResult)
    {
        client.addFail(result);
    }

    /**
     * Called when a test triggers an unexpected exception.
     *
     * @param    result            an erroneous test result
     */
    public function addError(result:TestResult)
    {
        client.addError(result);
    }

    /**
     * Called when a test has been ignored.
     *
     * @param    result            an ignored test
     */
    public function addIgnore(result:TestResult)
    {
        client.addIgnore(result);
    }

    /**
     * Called when all tests are complete.
     *
     * @param    testCount        total number of tests run
     * @param    passCount        total number of tests which passed
     * @param    failCount        total number of tests which failed
     * @param    errorCount        total number of tests which were erroneous
     * @param    ignoreCount        total number of ignored tests
     * @param    time            number of milliseconds taken for all tests to be executed
     * @return    collated test result data if any
     */
    public function reportFinalStatistics(testCount:Int, passCount:Int, failCount:Int, errorCount:Int, ignoreCount:Int, time:Float):Dynamic
    {
        var result = client.reportFinalStatistics(testCount, passCount, failCount, errorCount, ignoreCount, time);

        this.handleResult(result);

        return result;
    }

    function handleResult(result:Dynamic)
    {
        //TO-DO: override in subclasses
    }
}
