package discovery.test.munit;

import massive.munit.util.MathUtil;
import massive.munit.TestResult;
import massive.munit.client.JUnitReportClient;

/**
    A version of JunitReportClient that fixes some errors
**/
class JunitReportClientFixed extends JUnitReportClient{
    public function new() {
        super();
    }

    override function addFail(result:TestResult) {
        suiteFailCount++;
        addTestResult(result, result.failure);
    }

    override function addError(result:TestResult) {
        suiteErrorCount++;
        addTestResult(result, result.error);
    }

    function addTestResult(result: TestResult, error:Dynamic) {
        var resultTag = getResultTestTag(result);
        var message = xmlEscape(error.message);
        var errorStr = xmlEscape('${error}');

        testSuiteXML.add('<testcase classname="${result.className}" name="${result.name}" time="${MathUtil.round(result.executionTime, 5)}">');
        testSuiteXML.add(newline);
        testSuiteXML.add(
            '<$resultTag message="${message}" type="${error.type}">${errorStr}</$resultTag>'
        );
        testSuiteXML.add(newline);
        testSuiteXML.add('</testcase>');
        testSuiteXML.add(newline);
    }


    function getResultTestTag(result:TestResult): String{
        return switch (result.type){
            case FAIL: "failure";
            case ERROR: "error";

            //For now these other cases are not being treated/used
            //in the future they may be used to handle these other results
            case UNKNOWN:null;
            case PASS:null;
            case IGNORE:null;
            default: null;
        }
    }

    function xmlEscape(message:String):String {
        return StringTools.htmlEscape(message, true);
    }
}