package discovery.test.munit;

import sys.FileSystem;
import haxe.io.Path;
import massive.munit.client.JUnitReportClient;

import sys.io.File;

class JUnitReportWriter extends AbstractReporterDecorator{
    public static inline var PASSED:String = "PASSED";
    public static inline var FAILED:String = "FAILED";

    /**
    * Directory  base used to write files
    **/
    var reportDir:String;


    static function makeClient(?client: JUnitReportClient): JUnitReportClient {
        if(client == null){
            return new JunitReportClientFixed();
        }
        else{
            return client;
        }
    }


    public function new(reportDir:String, ?client: JUnitReportClient) {
        super("JunitReportWriter", makeClient(client));

        this.reportDir = reportDir;
    }

    public function clearDirectory(){
        var reportDirFile = massive.sys.io.File.create(reportDir);
        reportDirFile.deleteDirectoryContents();
    }

    override function handleResult(result: Dynamic)
    {
        var outDir = createOutDir();
        writeJUnitReportData(result, outDir);
    }

    function createOutDir(): String {
        var clientId = if   (this.client.id == null) "unknown-client"
                       else this.client.id;
        var platform = getPlatform();
        var outDir = Path.join([this.reportDir, clientId, platform, "xml"]);

        FileSystem.createDirectory(outDir);

        return outDir;
    }

    function getPlatform(): String {
        #if flash return "as3";
        #elseif js return "js";
        #elseif neko return "neko";
        #elseif cpp return "cpp";
        #elseif java return "java";
        #elseif cs return "cs";
        #elseif python return "python";
        #elseif php return "php";
        #elseif hl return "hl";
        #elseif eval return "eval";
        #elseif lua return "lua";
        #end
        return "unknown";
    }

    function writeJUnitReportData(data:String, writeDir:String):String
    {
        var xml:Xml = Xml.parse(data);

        var suites = xml.firstChild().elementsNamed("testsuite");

        var result:String = PASSED;
        var count = 0;

        for (test in suites){
            writeTest(test, writeDir);

            if (hasFailed(test)){
                result = FAILED;
            }

            count++;
        }

        return result;
    }

    function writeTest(test: Xml, dir: String) {
        var fileName:String = "TEST-" + test.get("name") + ".xml";
        var filePath:String = Path.join([dir, fileName]);

        var content = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                    + test.toString();

        File.saveContent(filePath, content);
    }

    function hasFailed(test:Xml): Bool{
        var failures:Int = Std.parseInt(test.get("failures"));
        var errors  :Int = Std.parseInt(test.get("errors"));

        if (failures == null) failures = 0;
        if (errors == null) errors = 0;

        return (failures > 0 || errors > 0);
    }
}