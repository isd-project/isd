import pexpect


class Command(object):
    EOF = pexpect.EOF

    def __init__(self, name, process, commands):
        self.name = name
        self.process = process
        self.commands = commands

    def close(self, force=True):
        self.process.terminate(force=force)

    def sendline(self, line, **kwargs):
        self._set_command_name_in(kwargs)

        self.commands.sending_line(line, **kwargs)

    def wait_for_message(self, msg, **kwargs):
        self._set_command_name_in(kwargs)

        return self.commands.wait_for_message(msg, **kwargs)

    def wait_for_eof(self, **kwargs):
        self.wait_for_message(pexpect.EOF, **kwargs)


    def _set_command_name_in(self, kwargs):
        kwargs['proc'] = self.name

    def _name_kwargs(self):
        return {
            'proc': self.name
        }
