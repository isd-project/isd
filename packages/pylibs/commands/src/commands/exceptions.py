class ExpectException(Exception):
    def __init__(self, msg, cmd_output=''):
        super().__init__(msg)

        self.cmd_output = cmd_output

class PExpectException(ExpectException):
    def __init__(self, msg, cmd):
        super().__init__(msg, '')

        self.cmd = cmd
        self.before = cmd.before
        self.after = cmd.after
        self.buffer = cmd.buffer