import pexpect

import sys
import logging

from commands.Command import Command
from commands.exceptions import PExpectException

class Commands(object):
    DEFAULT_NAME = 'default'
    CMD_NAME_ARG = 'proc'

    def __init__(self, logfile=sys.stdout):
        self._cmd_processes = {}
        self.logfile = logfile

    def starting_command(self, cmd, *args, **kwargs) -> Command:
        cmd_args = list(args)

        logging.info('running command: "{0}" with args: {1}'.format(cmd, cmd_args))

        encoding = kwargs.get('encoding', 'utf-8')

        proc = pexpect.spawn(cmd, cmd_args, logfile=self.logfile, encoding='utf-8')
        self._store_process(proc, **kwargs)

        return Command(self._cmd_name(**kwargs), proc, self)


    def run_command(self, *args, **kwargs):
        kwargs.pop('proc')

        cmd_args = list(args)

        cmd = ' '.join(cmd_args)

        return pexpect.run(cmd,**kwargs)


    def wait_for_messages(self, *msgs, **kwargs):
        for m in msgs:
            self.wait_for_message(m, **kwargs)

    def wait_for_message(self, msg, **kwargs):
        try:
            self._expect_message(msg, **kwargs)
        except pexpect.ExceptionPexpect as e:
            self._on_wait_message_error(e, msg, **kwargs)

    def wait_for_eof(self, **kwargs):
        self.wait_for_message(pexpect.EOF, **kwargs)

    def _expect_message(self, msg, **kwargs):
        process = self._require_cmd(**kwargs)
        timeout = kwargs.get('timeout', -1)

        process.expect(msg, timeout=timeout)

        logging.debug('found "{0}"\n\tbefore:{1}\n\tafter:{2}\n\tbuffer:{3}'\
            .format(msg, process.before, process.after, process.buffer))

    def _on_wait_message_error(self, error, msg, **kwargs):
        process, cmd_name = self._require_cmd_and_name(**kwargs)

        err_msg = None
        if isinstance(error, pexpect.EOF):
            err_msg = 'command exited before finding message'
        elif isinstance(error, pexpect.TIMEOUT):
            err_msg = 'command timed out before finding message'
        else: #unknown exception, so just re-raise
            raise error

        except_msg = '"{0}" {1}: {2}'.format(cmd_name, err_msg, msg)
        except_msg += '\n\tbuffer:{0}'.format(process.buffer)
        except_msg += '\n\tbefore:{0}'.format(process.before)
        except_msg += '\n\tcmd:{0} {1}'.format(process.command, process.args)

        raise PExpectException(except_msg, process) from error


    def wait_until_cmd_finishes(self, **kwargs):
        self.wait_for_message(pexpect.EOF, **kwargs)

    def sending_line(self, msg, **kwargs):
        cmd = self._require_cmd(**kwargs)

        cmd.sendline(msg)

    def command_has_terminated(self, **kwargs):
        cmd = self._require_cmd(**kwargs)

        return cmd.isalive() == False

    def close_command(self, **kwargs):
        cmd = self._get_cmd(**kwargs)
        self._terminate(cmd)

    def close_all(self):
        for _, cmd in self._cmd_processes.items():
            self._terminate(cmd)

    ###############################################################

    def _terminate(self, cmd):
        if cmd:
            cmd.terminate(force=True)
            cmd.close(force=True)

    def _require_cmd_and_name(self, **kwargs):
        cmd_name = self._cmd_name(**kwargs)
        process  = self._require_cmd(**kwargs)

        return process, cmd_name

    def _cmd_name(self, **kwargs):
        cmd_name = kwargs.get(self.CMD_NAME_ARG, self.DEFAULT_NAME)

        # Handle empty name
        return cmd_name if cmd_name else self.DEFAULT_NAME

    def _store_process(self, process, **kwargs):
        cmd_name = self._cmd_name(**kwargs)
        self._cmd_processes[cmd_name] = process

    def _get_cmd(self, **kwargs):
        cmd_name = self._cmd_name(**kwargs)
        return self._cmd_processes.get(cmd_name, None)

    def _require_cmd(self, **kwargs):
        cmd_name = self._cmd_name(**kwargs)
        cmd = self._get_cmd(**kwargs)

        assert cmd is not None, "Command with name {0} was not started".format(cmd_name)

        return cmd