from subprocess import Popen, PIPE
import shlex
import ipaddress

from . import utils

import contextlib
import logging

logger = logging.getLogger(__name__)


@contextlib.contextmanager
def start_session_from_file(session_file):
    from core.api.grpc.client import CoreGrpcClient

    # create grpc client and connect
    client = CoreGrpcClient()
    with client.context_connect():
        s = SessionWrapper(client)
        s.open_xml(session_file)

        try:
            yield s
        finally:
            s.close()


class SessionWrapper(object):
    def __init__(self, client):
        self.client = client
        self.session = None
        self.session_id = None
        self.nodes_index = None

    def open_xml(self, filepath, start=True):
        r = self.client.open_xml(filepath, start=start)
        self.session_id = r.session_id

        self._refresh_session()

    def _refresh_session(self):
        r = self.client.get_session(self.session_id)
        self.session = r.session

    def close(self):
        if self.session_id is not None:
            self._stop_session()
            self._delete_session()

    def _stop_session(self):
        response   = self.client.stop_session(self.session_id)
        logger.debug("stopped session(%s), Result: %s", self.session_id, response)

    def _delete_session(self):
        response = self.client.delete_session(self.session_id)
        logger.info("deleted session(%s), Result: %s", self.session_id, response)

        if response:
            self.session_id = None
            self.session = None

    def get_nodes(self):
        return self.session.nodes

    def _get_node_index(self):
        if not self.nodes_index:
            self.nodes_index = utils.index_session_objects(self.session)

        return self.nodes_index

    def get_node_by_name(self, name):
        return self._get_node_index().get_node_by_name(name)

    def get_node_interfaces(self, node_id):
        ifaces = self._get_node_index().get_node_ifaces(node_id)

        interfaces = []

        for iface in ifaces:
            if iface.ip4:
                addr = "{0}/{1}".format(iface.ip4, iface.ip4_mask)
                interfaces.append(ipaddress.ip_interface(addr))

            if iface.ip6:
                addr = "{0}/{1}".format(iface.ip6, iface.ip6_mask)
                interfaces.append(ipaddress.ip_interface(addr))

        return interfaces

    def get_node_first_ip(self, node_id, ip_version = None):
        interfaces = self.get_node_interfaces(node_id)

        for interface in interfaces:
            if ip_version == None or interface.version == ip_version:
                return interface.ip

        return None


    def node_commands(self, node_id, commands, *args, **kwargs):
        out = []

        for cmd in commands:
            r = self.node_command(node_id, cmd, *args, **kwargs)
            out.append(r)

        return out

    def node_command(self, node_id, cmd, *args, **kwargs):
        r = self.client.node_command(self.session_id, node_id, cmd, *args, **kwargs)
        out = r.output
        return out.encode('latin1').decode('unicode_escape')

    def make_node_cmd(self, node_id, cmd_str_or_args):
        r = self.client.get_node_terminal(self.session_id, node_id)

        vcmd = r.terminal.split('--')[0]
        p_cmd = shlex.split(vcmd) + ['--'] + self._parse_command(cmd_str_or_args)

        return p_cmd

    def _parse_command(self, cmd_str_or_args):
        if isinstance(cmd_str_or_args, str):
            return shlex.split(cmd_str_or_args)
        return list(cmd_str_or_args)

    def Popen(self, node_id, cmd, *args, **kwargs):
        p_cmd = self.make_node_cmd(node_id, cmd)

        return Popen(p_cmd, *args, **kwargs)
