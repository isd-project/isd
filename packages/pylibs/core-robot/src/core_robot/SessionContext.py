class SessionContext(object):
    def __init__(self, core, session):
        self.core = core
        self.session = session

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, exc_tb):
        if self.core:
            self.core.close()