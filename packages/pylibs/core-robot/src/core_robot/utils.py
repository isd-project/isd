import core
from core.emulator.coreemu import CoreEmu
from core.emulator.enumerations import EventTypes
from core.nodes.base import NodeBase, CoreNodeBase

def create_from_file(xml_file, start=True):
    # create emulator instance for creating sessions and utility methods
    coreemu = CoreEmu()
    session = coreemu.create_session()

    session.set_state(EventTypes.CONFIGURATION_STATE)
    session.open_xml(xml_file, start=start)

    return coreemu, session

def index_session_objects(session):
    '''Return an index to allow get nodes from name'''
    indexer = None

    if isinstance(session, core.api.grpc.core_pb2.Session):
        indexer = GrpcSessionIndexer(session)
    else:
        indexer = SessionIndexer(session)

    return indexer.build_index()

class SessionIndexer(object):

    def __init__(self, session):
        self.session = session

        self.build_index()

    def build_index(self):
        nodes_by_name = {}
        nodes_by_id = {}

        for n_id in self.session.nodes:
            n = self.session.get_node(n_id, NodeBase)
            nodes_by_name[n.name] = n
            nodes_by_id[n_id] = n

        return SessionIndex(nodes_by_id, nodes_by_name)

class GrpcSessionIndexer(object):
    def __init__(self, session: core.api.grpc.core_pb2.Session):
        self.session = session

        self.build_index()

    def build_index(self):
        nodes_by_name = {}
        nodes_by_id = {}

        for n in self.session.nodes:
            nodes_by_name[n.name] = n
            nodes_by_id[n.id] = n

        ifaces_by_id = self._index_nodes_ifaces()

        return SessionIndex(nodes_by_id, nodes_by_name, ifaces_by_id)

    def _index_nodes_ifaces(self):
        ifaces = {}

        for l in self.session.links:
            id_1 = l.node1_id
            id_2 = l.node2_id

            ifaces1 = ifaces.get(id_1, []) + [l.iface1]
            ifaces2 = ifaces.get(id_2, []) + [l.iface2]

            ifaces[id_1] = ifaces1
            ifaces[id_2] = ifaces2

        return ifaces


class SessionIndex(object):

    def __init__(self, id_index, name_index, ifaces_index):
        self.id_index = id_index
        self.name_index = name_index
        self.ifaces_index = ifaces_index

    def get_node_by_name(self, name):
        return self.name_index.get(name, None)

    def get_node(self, id):
        return self.id_index.get(id, None)

    def get_node_ifaces(self, id):
        return self.ifaces_index[id] if self.ifaces_index else None




def print_objects(session):
    print('objects in session:')

    for id, o in session.objects.items():
        print(o.name)


def get_first_ip(obj):
    ifaces = obj.get_ifaces(control=False)
    assert ifaces, "Object {} does not have a valid interface".format(obj)

    ips = ifaces[0].ips()
    assert ips, "Object {} does not have a valid IP address".format(obj)

    return ips[0].ip


def get_ips(obj, control=False):
    ifaces = obj.get_ifaces(control=control)
    assert ifaces, "Object {} does not have a valid interface".format(obj)

    addrs = []
    for iface in ifaces:
        for addr in iface.ips():
            addrs.append(addr.ip)

    return addrs

def get_ip(obj, ip_idx=0):
    return get_ips()[ip_idx]


def add_event(session, obj, cmd):
    '''Alternative way to execute command async'''

    session.event_loop.add_event(session.runtime(), session.run_event, node_id=obj.id, data=cmd)
