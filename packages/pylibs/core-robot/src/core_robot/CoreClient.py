from core.api.grpc.client import CoreGrpcClient

from .SessionWrapper import SessionWrapper

class CoreClient(object):
    def __init__(self):
        self.client = None

    def connected(self):
        return self.client is not None

    def connect_to_daemon(self):
        self.client = CoreGrpcClient()
        try:
            self.client.connect()

            #Check if connection was established
            self.client.check_session(0)
        except Exception as e:
            raise Exception("Failed to connect with daemon, please check if core daemon is running") from e

    def disconnect(self):
        if self.client:
            self.client.close()
            self.client = None

    def open_session(self, network_file) -> SessionWrapper:
        session = SessionWrapper(self.client)
        session.open_xml(network_file)

        return session