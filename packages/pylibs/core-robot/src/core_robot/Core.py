from .CoreClient import CoreClient
from .CoreNode import CoreNode
from .SessionContext import SessionContext
from .SessionWrapper import SessionWrapper
from commands import Commands

from os import path
import logging

class Core(object):
    session:SessionWrapper

    def __init__(self, network_files_dir=''):
        self.client = CoreClient()
        self.network_files_dir = network_files_dir
        self.session =  None
        self.commands = Commands()


    def open_network(self, network_name):
        network_file = self._get_network_file(network_name)

        return self.open_network_file(network_file)

    def open_network_file(self, network_file):
        if not self.client.connected():
            self.client.connect_to_daemon()

        self.session = self.client.open_session(network_file)

        return SessionContext(self, self.session)


    def close(self):
        self.commands.close_all()
        self.close_session()

    def close_session(self):
        if self.session:
            logging.info('core: closing session')
            # Warning: raises error if session does not exist
            self.session.close()
            self.session = None

        logging.info('core: disconnecting client')
        self.client.disconnect()

    def check_network_started(self):
        self._require_session()

    def get_node(self, name):
        self._require_session()

        node = self.session.get_node_by_name(name)
        if node is None:
            return None

        return CoreNode(self, name)

    def get_ip_of(self, node, family=None):
        self._require_session()

        family = family if family is None else family.strip().lower()

        ip_version = {
            "ipv4": 4,
            "ipv6": 6
        }.get(family, None)

        node = self.session.get_node_by_name(node)
        ip_address = self.session.get_node_first_ip(node.id, ip_version=ip_version)

        return str(ip_address)

    def run_command_in_node(self, *args, **kwargs):
        cmd_args, kwargs = self._make_cmd_args(*args, **kwargs)

        return self.commands.run_command(*cmd_args, **kwargs)

    def start_cmd_in_node(self, *cmd_args, **kwargs):
        return self.starting_command_in_node(*cmd_args, **kwargs)

    def starting_command_in_node(self, *args, **kwargs):
        cmd_args, kwargs = self._make_cmd_args(*args, **kwargs)

        return self.commands.starting_command(
                        cmd_args[0], *cmd_args[1:], **kwargs)

    def _make_cmd_args(self, *cmd_args, **kwargs):
        node = self._get_node(**kwargs)

        node_cmd_args = self.session.make_node_cmd(node.id, cmd_args)

        kwargs = self._change_cmd_name(**kwargs)
        kwargs.pop('node')

        return node_cmd_args, kwargs


    def expect_messages_in_node(self, *msgs, **kwargs):
        for m in msgs:
            self.expect_message_in_node(m, **kwargs)

    def expect_message_in_node(self, msg, **kwargs):
        logging.info('expect message: "{0}" in node {1}'.format(msg, kwargs.get("node")))

        kwargs = self._change_cmd_name(**kwargs)
        self.commands.wait_for_message(msg, **kwargs)

    ###################################################################

    def _get_network_file(self, network_name):
        return path.join(self.network_files_dir, network_name + '.xml')

    def _require_session(self):
        assert self.session is not None, "Network was not started"

    def _get_node(self, **kwargs):
        self._require_session()
        node_name = self._get_node_name(**kwargs)

        return self.session.get_node_by_name(node_name)

    def _get_node_name(self, **kwargs):
        return kwargs.get("node")

    def _change_cmd_name(self, **kwargs):
        '''
        Change kwargs to specify unique command name for each node
        '''

        node_name = self._get_node_name(**kwargs)
        cmd_name  = kwargs.get('proc', None)

        kwargs['proc'] = node_name + '.' + cmd_name if cmd_name else node_name

        return kwargs