class CoreNode(object):
    def __init__(self, core, name):
        self.name = name
        self.core = core

    def get_ip(self, ipfamily=None):
        return self.core.get_ip_of(self.name, ipfamily)

    def run_command(self, *args, **kwargs):
        kwargs['node'] = self.name

        out = self.core.run_command_in_node(*args, **kwargs)

        return string_escape(out)

    def start_command(self, *args, **kwargs):
        kwargs['node'] = self.name

        return self.core.starting_command_in_node(*args, **kwargs)



def string_escape(s, encoding='utf-8'):
    if isinstance(s, str):
        s = s.encode(encoding) # To bytes, required by 'unicode-escape'

    return (s.decode('unicode-escape') # Perform the actual octal-escaping decode
            .encode(encoding)         # 1:1 mapping back to bytes
            .decode(encoding))        # Decode original encoding