package discovery.experiments.testing.helpers;

import discovery.experiment.results.SearchResults;
import discovery.experiment.events.DiscoveredEvent;
import discovery.experiment.runner.SearchSpec;
import discovery.experiment.results.PublicationResult;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.results.ExperimentResults;
import discovery.experiment.events.PublicationEvent;
import discovery.test.helpers.RandomGen;
import discovery.experiment.ExperimentRunner.ServicePublication;

using discovery.utils.time.TimeUnitTools;
using discovery.experiment.results.ResultsTransformations;


class ExperimentInfoGenerator {
    public function new() {

    }

    public function nodeConfiguration():NodeConfiguration {
        return {
            nodeName: RandomGen.primitives.name(),
            keysdir: RandomGen.dirpath(),
            outputDir: RandomGen.dirpath(),
            publications: servicePublications()
        };
    }

    public function servicePublication(): ServicePublication{
        return {
            description: RandomGen.description(),
            delay: RandomGen.time.durationBetween(10.millis(), 100.millis())
        };
    }

    public function servicePublications(?count:Int):Array<ServicePublication> {
        if(count == null) count = RandomGen.int(1, 5);

        return [for(i in 0...count) servicePublication()];
    }

    public function experimentResults():ExperimentResults {
        return {
            publications: publicationResults(),
            searches: searchResults(),
            usedMechanisms: RandomGen.primitives.names(1, 3)
        };
    }

    function publicationResults():Array<PublicationResult> {
        return publicationEvents().toPublicationResults();
    }

    function publicationEvents():Array<PublicationEvent> {
        var len = RandomGen.primitives.int(1, 5);

        return [for(i in 0...len) publicationEvent()];
    }

    public function publicationEvent():PublicationEvent {
        return {
            interval: RandomGen.time.interval(),
            service: RandomGen.description()
        };
    }


    public function searchResults(?min:Int, ?max:Int):Array<SearchResults>
    {
        var searchSpecs = searchSpecifications(min, max);

        var results = searchSpecs.map((spec)->{
            var searchResult = new SearchResults(spec);

            for(evt in discoveredEvents(spec)){
                searchResult.addDiscovered(evt);
            }

            return searchResult;
        });

        return results;
    }

    public function searchSpecifications(?min:Int, ?max:Int):Array<SearchSpec> {
        if(min == null) min = 2;
        if(max == null) max = 5;

        var amount = RandomGen.primitives.int(min, max);

        return [for(i in 0...amount) searchSpec()];
    }

    public function searchSpec(?searchKind:SearchKind):SearchSpec {
        if(searchKind == null) searchKind = this.searchKind();

        return {
            kind: searchKind,
            srvType: if(searchKind == Search) serviceType() else null,
            srvId: if(searchKind == Locate) serviceId() else null,
            delay: RandomGen.time.durationBetween(10.millis(), 1.seconds())
        };
    }

    public function searchKind() {
        return RandomGen.primitives.choice(Search, Locate);
    }

    function serviceType():String {
        return RandomGen.serviceType();
    }

    function serviceId():String {
        return RandomGen.identifierUrl().toUrl();
    }

    public function discoveredEvents(searchSpec:SearchSpec) {
        var amount = RandomGen.primitives.int(2, 5);

        return [ for (i in 0...amount) discoveredEvent(searchSpec)];
    }

    public function discoveredEvent(?searchSpec:SearchSpec): DiscoveredEvent {
        if(searchSpec == null) this.searchSpec();

        return {
            searchSpec: searchSpec,
            interval: RandomGen.time.interval(),
            discovered: RandomGen.discovered()
        };
    }
}