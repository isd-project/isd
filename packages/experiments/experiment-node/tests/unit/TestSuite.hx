import massive.munit.TestSuite;

import discovery.experiments.testing.tests.results.ResultsPublisherTest;
import discovery.experiments.testing.tests.results.ResultsPersistenceTest;
import discovery.experiments.testing.tests.results.ResultsTransformationsTest;
import discovery.experiments.testing.tests.ExperimentNodeConfigureTest;
import discovery.experiments.testing.tests.parsing.ConfigurationLoaderTest;
import discovery.experiments.testing.tests.parsing.ConfigurationReaderTest;
import discovery.experiments.testing.tests.TaskSchedulerTest;
import discovery.experiments.testing.tests.mechanisms.ExperimentsSDConfigurationTest;
import discovery.experiments.testing.tests.mechanisms.MdnsRawMechanismTest;
import discovery.experiments.testing.tests.mechanisms.DummyKeyTransportTest;
import discovery.experiments.testing.tests.runner.ExperimentRunnerTest;
import discovery.experiments.testing.tests.runner.SearchStageNotificationsTest;
import discovery.experiments.testing.tests.runner.SearchStageTest;
import discovery.experiments.testing.tests.runner.SearchStageConfigTest;
import discovery.experiments.testing.tests.cli.ExperimentNodeCliTest;
import discovery.experiments.testing.tests.ExperimentNodeTest;
import discovery.experiments.testing.tests.serialization.ExperimentResultsSerializerTest;
import discovery.experiments.testing.tests.ConfigurationParserTest;
import discovery.experiments.testing.scenarios.MeasureSearchEventsTest;
import discovery.experiments.testing.scenarios.PublishServicesTest;
import discovery.experiments.testing.scenarios.ResultsMonitorTest;
import discovery.experiments.testing.scenarios.CaptureMeasuresTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.experiments.testing.tests.results.ResultsPublisherTest);
		add(discovery.experiments.testing.tests.results.ResultsPersistenceTest);
		add(discovery.experiments.testing.tests.results.ResultsTransformationsTest);
		add(discovery.experiments.testing.tests.ExperimentNodeConfigureTest);
		add(discovery.experiments.testing.tests.parsing.ConfigurationLoaderTest);
		add(discovery.experiments.testing.tests.parsing.ConfigurationReaderTest);
		add(discovery.experiments.testing.tests.TaskSchedulerTest);
		add(discovery.experiments.testing.tests.mechanisms.ExperimentsSDConfigurationTest);
		add(discovery.experiments.testing.tests.mechanisms.MdnsRawMechanismTest);
		add(discovery.experiments.testing.tests.mechanisms.DummyKeyTransportTest);
		add(discovery.experiments.testing.tests.runner.ExperimentRunnerTest);
		add(discovery.experiments.testing.tests.runner.SearchStageNotificationsTest);
		add(discovery.experiments.testing.tests.runner.SearchStageTest);
		add(discovery.experiments.testing.tests.runner.SearchStageConfigTest);
		add(discovery.experiments.testing.tests.cli.ExperimentNodeCliTest);
		add(discovery.experiments.testing.tests.ExperimentNodeTest);
		add(discovery.experiments.testing.tests.serialization.ExperimentResultsSerializerTest);
		add(discovery.experiments.testing.tests.ConfigurationParserTest);
		add(discovery.experiments.testing.scenarios.MeasureSearchEventsTest);
		add(discovery.experiments.testing.scenarios.PublishServicesTest);
		add(discovery.experiments.testing.scenarios.ResultsMonitorTest);
		add(discovery.experiments.testing.scenarios.CaptureMeasuresTest);
	}
}
