package discovery.experiments.testing.mockups.config;

import discovery.experiment.parsing.ConfigurationParser;
import discovery.experiment.parsing.ConfigurationParser;
import mockatoo.Mockatoo;

class ConfigurationParserMockup {
    var configParser = Mockatoo.mock(ConfigurationParser);

    public function new() {

    }

    public function parser():ConfigurationParser {
        return configParser;
    }

    public function shouldHaveParsedConfigForNode(nodeName:String) {
        Mockatoo.verify(configParser.parseNodeConfiguration(any, nodeName));
    }
}