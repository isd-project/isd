package discovery.experiments.testing.tests;

import discovery.utils.time.scheduler.TaskScheduler;
import discovery.utils.time.Duration.TimeUnit;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.utils.TimerLoopMockup;
import discovery.test.common.CallableMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class TaskSchedulerTest implements Scenario{
    @steps
    var steps: TaskSchedulerSteps;

    @Test
    @scenario
    public function execute_next_task(){
        given().a_task_was_scheduled();
        when().executing_the_next_task();
        then().that_task_should_have_been_executed();
    }

    @Test
    @scenario
    public function calling_next_task_after_done(){
        given().no_task_was_scheduled();
        when().executing_the_next_task();
        then().should_not_execute_anything();
    }

    @Test
    @scenario
    public function execute_next_after_schedule_time(){
        given().a_task_was_scheduled();
        when().the_schedule_time_completes();
        then().that_task_should_have_been_executed();
    }

    @Test
    @scenario
    public function wait_current_task_execution_to_schedule_next(){
        given().a_task_was_scheduled();
        when().scheduling_another_task();
        then().only_the_first_timeout_should_be_set();
    }

    @Test
    @scenario
    public function schedule_next_time_after_schedule_time(){
        given().a_task_was_scheduled();
        and().another_task_was_scheduled();
        when().the_schedule_time_completes();
        then().the_next_task_should_be_scheduled_with_the_given_time();
    }

    @Test
    @scenario
    public function schedule_next_time_after_executing_pending_task(){
        given().a_task_was_scheduled();
        and().another_task_was_scheduled();
        when().executing_the_next_task();
        then().the_next_task_should_be_scheduled_with_the_given_time();
    }

    @Test
    @scenario
    public function cancel_pending_time_after_executing_task(){
        given().a_task_was_scheduled();
        and().another_task_was_scheduled();
        when().executing_the_next_task();
        then().the_pending_timer_should_have_been_cancelled();
    }
}

typedef PendingTask = {
    callable: CallableMockup,
    scheduleTime: TimeUnit
}

class TaskSchedulerSteps extends Steps<TaskSchedulerSteps>{

    var pendingTasks:Array<PendingTask> = [];
    var executedTasks:Array<PendingTask> = [];
    var timerLoop = new TimerLoopMockup();

    var scheduler:TaskScheduler;
    var executed:Bool;

    public function new(){
        super();

        scheduler = new TaskScheduler(timerLoop.timer());
    }

    // given ---------------------------------------------------

    @step
    public function no_task_was_scheduled() {}

    @step
    public function a_task_was_scheduled() {
        when().scheduling_a_task();
    }

    @step
    public function another_task_was_scheduled() {
        when().scheduling_another_task();
    }

    // when  ---------------------------------------------------


    @step
    public function scheduling_another_task() {
        when().scheduling_a_task();
    }

    @step
    function scheduling_a_task() {
        var task = new CallableMockup();
        var time = RandomGen.time.anyTime();

        pendingTasks.push({
            callable: task,
            scheduleTime: time
        });

        scheduler.schedule(task.callable(), time);
    }

    @step
    public function executing_the_next_task() {
        executed = scheduler.executeNext();

        executedTasks.push(pendingTasks.shift());
    }

    @step
    public function the_schedule_time_completes() {
        timerLoop.executeNextDelayed();

        executedTasks.push(pendingTasks.shift());
    }

    // then  ---------------------------------------------------

    @step
    public function that_task_should_have_been_executed() {
        then().the_older_pending_task_should_have_been_executed();
    }

    @step
    public function the_older_pending_task_should_have_been_executed() {
        var lastTask = lastExecutedTask();

        lastTask.callable.verifyCallbackCalled();
    }

    @step
    public function should_not_execute_anything() {
        executed.shouldBe(false, 'no task should have been executed');
    }

    @step
    public function only_the_first_timeout_should_be_set() {
        timerLoop.verifyDelayCalled(times(1));
    }

    @step
    public function the_next_task_should_be_scheduled_with_the_given_time() {
        var nextTask = pendingTasks[0];

        timerLoop.verifyDelayCalledWith(nextTask.scheduleTime);
    }

    @step
    public function the_pending_timer_should_have_been_cancelled() {
        timerLoop.verifyPreviousDelayCancelled();
    }

    // ----------------------------------------------------------------------

    function lastExecutedTask() {
        return executedTasks[executedTasks.length - 1];
    }
}