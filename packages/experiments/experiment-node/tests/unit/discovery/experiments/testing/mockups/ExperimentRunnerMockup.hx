package discovery.experiments.testing.mockups;

import discovery.experiment.runner.NodeMonitor;
import discovery.experiment.runner.StageControl;
import discovery.experiment.ExperimentRunner;
import mockatoo.Mockatoo;

class ExperimentRunnerMockup {
    var expRunner = Mockatoo.mock(ExperimentRunner);

    var lastConfig:NodeConfiguration;

    public function new() {
        Mockatoo.when(expRunner.configure(any)).thenCall((args)->{
            lastConfig = args[0];
        });
    }

    public function runner():Null<ExperimentRunner> {
        return expRunner;
    }

    public function verifyConfigured() {
        Mockatoo.verify(expRunner.configure(any));
    }

    public function lastConfiguration():NodeConfiguration {
        return lastConfig;
    }

    public function shouldHaveBeenStarted() {
        Mockatoo.verify(expRunner.start(any));
    }

    public function shouldHaveBeenStartedWith(
        control:StageControl, monitor:NodeMonitor)
    {
        Mockatoo.verify(expRunner.start(control, monitor));
    }
}