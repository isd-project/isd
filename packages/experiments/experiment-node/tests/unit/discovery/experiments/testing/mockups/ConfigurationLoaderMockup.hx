package discovery.experiments.testing.mockups;

import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.parsing.ConfigurationLoader;
import mockatoo.Mockatoo;

class ConfigurationLoaderMockup {
    var configLoader = Mockatoo.mock(ConfigurationLoader);

    var configToBeLoaded:NodeConfiguration;

    var generator = new ExperimentInfoGenerator();

    public function new() {
        Mockatoo.when(configLoader.loadConfiguration(any, any))
            .thenCall(onLoadConfig);
    }

    function onLoadConfig(args: Array<Dynamic>): NodeConfiguration {
        if(configToBeLoaded == null){
            return generator.nodeConfiguration();
        }

        return this.configToBeLoaded;
    }

    public function configurer():ConfigurationLoader {
        return configLoader;
    }

    public function setupConfigToBeLoaded(config:NodeConfiguration) {
        this.configToBeLoaded = config;
    }

    public function
        verifyConfigurationLoaded(configFile:String, nodeName:String)
    {
        Mockatoo.verify(configLoader.loadConfiguration(configFile, nodeName));
    }
}