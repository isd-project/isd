package discovery.experiments.testing.tests;

import discovery.keys.crypto.HashTypes;
import discovery.keys.crypto.Fingerprint;
import discovery.format.Binary;
import discovery.format.Formatters;
import discovery.experiment.parsing.ConfigurationParser;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.parsing.ConfigurationModel;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.time.TimeUnitTools;
using discovery.test.matchers.CommonMatchers;


class ConfigurationParserTest implements Scenario{
    @steps
    var steps: ConfigurationParserSteps;


    @Test
    @scenario
    public function parse_node_configuration(){
        given().this_configuration({
            bootstrap_address: '10.0.4.2:12345',
            output_dir: '/base/output/dir',
            nodes: {
                n1: {
                    name: 'n1',
                    keyid: 'c4bb7f65cb34',
                    keyid_fmt:'b16',
                    keysdir: '/path/to/keys/directory',
                    output_dir: 'node/out/dir',
                    results_file: 'results_file.example',
                    publications:[{
                        name: 's1_n1',
                        type: 'type1',
                        delay: 15,
                        port: 12345
                    }],
                    searches: [
                        {
                            kind: 'search',
                            srvType: 'typeA',
                            delay: 300
                        },
                        {
                            kind: 'locate',
                            srvId: 'srv://...',
                            delay: 50
                        }
                    ]
                }
            }
        });
        when().parsing_configuration_for_node('n1');
        then().it_should_produce_this_node_configuration({
            nodeName: 'n1',
            keysdir: '/path/to/keys/directory',
            baseOutputDir: '/base/output/dir',
            outputDir: 'node/out/dir',
            resultsFile: 'results_file.example',
            bootstrapAddress: '10.0.4.2:12345',
            publications: [
                {
                    description: {
                        name: 's1_n1',
                        type: 'type1',
                        port: 12345,
                        providerId:new Fingerprint(
                            null,
                            Binary.fromHex('c4bb7f65cb34')
                        )
                    },
                    delay: 15.millis()
                }
            ],
            searches: [
                {
                    kind: Search,
                    srvType: 'typeA',
                    delay: 300.millis()
                },
                {
                    kind: Locate,
                    srvId: 'srv://...',
                    delay: 50.millis()
                }
            ]
        });
    }

    @Test
    @scenario
    public function parse_keyid_using_format(){
        final keyid = 't1oQU7n5L7A';

        given().this_configuration({
            nodes: {
                n1: {
                    keyid: keyid,
                    keyid_fmt: 'b64u',
                    keyid_alg: 'SHA-256',
                    publications:[{
                        name: 's1_n1',
                        type: 'type1',
                        delay: 15,
                        port: 12345
                    }],
                    searches: []
                }
            }
        });
        when().parsing_configuration_for_node('n1');
        then().it_should_produce_this_node_configuration({
            publications: [
                {
                    description: {
                        name: 's1_n1',
                        type: 'type1',
                        port: 12345,
                        providerId:new Fingerprint(
                            HashTypes.Sha256,
                            Formatters.base64Url.decode(keyid)
                        )
                    },
                    delay: 15.millis()
                }
            ],
            searches: [],
            keysdir : null,
            baseOutputDir : null,
            outputDir : null,
            resultsFile : null,
            bootstrapAddress: null,
        });
    }
}

class ConfigurationParserSteps extends Steps<ConfigurationParserSteps>{
    var parser:ConfigurationParser;

    var experimentConfig:ConfigurationModel;
    var parsedConfig:NodeConfiguration;

    public function new(){
        super();

        parser = new ConfigurationParser();
    }

    // given ---------------------------------------------------

    @step
	public function this_configuration(config:ConfigurationModel) {
        experimentConfig = config;
    }

    // when  ---------------------------------------------------

    @step
	public function parsing_configuration_for_node(node:String) {
        parsedConfig = parser.parseNodeConfiguration(experimentConfig, node);
    }

    // then  ---------------------------------------------------

    @step
	public function it_should_produce_this_node_configuration(nodeConfig:NodeConfiguration)
    {
        parsedConfig.shouldBeEqualsTo(nodeConfig,
            'Parsed configuration does not match the expected value');
    }
}