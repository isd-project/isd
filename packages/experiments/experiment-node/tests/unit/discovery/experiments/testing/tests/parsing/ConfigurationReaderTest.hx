package discovery.experiments.testing.tests.parsing;

import discovery.experiment.parsing.ConfigurationModel;
import discovery.experiment.parsing.ConfigurationModel.NodeConfigurationModel;
import discovery.test.matchers.CommonMatchers;
import discovery.experiment.parsing.ConfigurationReader;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ConfigurationReaderTest implements Scenario{
    @steps
    var steps: ConfigurationReaderSteps;

    var reader:ConfigurationReader;

    @Before
    public function setup(){
        reader = new ConfigurationReader();
    }


    @Test
    @scenario
    public function read_from_string(){
        final content = '
output_dir: replication0/output
nodes:
  n0:
    keyid: 0GQ4CRFQzjjF7w5vdNZbXF7NKxdhz9vKNb-D1BpLdT0
    keysdir: keys/rsa0
    name: n0
    output_dir: replication0/output/n0
    publications:
    - delay: 68
      name: srv0_n0
      port: 29173
      type: srvtype
    results_file: n0.results
    searches:
    - delay: 116
      kind: search
      srvId: null
      srvType: srvtype';

        var obj:ConfigurationModel = reader.readFromString(content);

        CommonMatchers.shouldBeEqualsTo(obj, {
            output_dir: 'replication0/output',
            nodes: {
                n0:{
                    keyid: '0GQ4CRFQzjjF7w5vdNZbXF7NKxdhz9vKNb-D1BpLdT0',
                    keysdir: 'keys/rsa0',
                    name: 'n0',
                    output_dir: 'replication0/output/n0',
                    publications:[
                        {
                            delay: 68,
                            name: 'srv0_n0',
                            port: 29173,
                            type: 'srvtype',
                        }
                    ],
                    results_file: 'n0.results',
                    searches:[
                        {
                            delay: 116,
                            kind: 'search',
                            srvId: null,
                            srvType: 'srvtype'
                        }
                    ]
                }
            }
        });
    }
}

class ConfigurationReaderSteps extends Steps<ConfigurationReaderSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}