package discovery.experiments.testing.scenarios;

import haxe.Exception;
import discovery.utils.EqualsComparator;
import discovery.experiment.results.SearchResults;
import discovery.test.helpers.RandomGen;
import discovery.experiment.events.DiscoveredEvent;
import discovery.experiment.runner.SearchSpec;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.utils.comparator.GenericComparator;
import discovery.experiment.events.PublicationEvent;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiments.testing.mockups.ResultsPublisherMockup;
import discovery.experiment.results.ResultsMonitor;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.experiment.results.ResultsTransformations;
using discovery.test.matchers.CommonMatchers;


class ResultsMonitorTest implements Scenario{
    @steps
    var steps: ResultsMonitorSteps;



    @Test
    @scenario
    public function configure_results_publisher(){
        given().a_node_configuration();
        when().configuring_the_monitor();
        then().it_should_configure_the_results_publisher();
    }

    @Test
    @scenario
    public function publish_results_on_done(){
        given().some_services_were_published();
        when().the_experiment_is_done();
        then().the_results_should_be_published();
    }


    @Test
    @scenario
    public function publish_results_with_publications(){
        given().some_services_were_published();
        when().the_experiment_is_done();
        then().the_results_should_contain_info_about_all_published_services();
    }


    @Test
    @scenario
    public function publish_search_results(){
        given().a_search_was_executed();
        and().some_services_were_discovered_for_that_search();
        when().the_experiment_is_done();
        then().the_search_results_should_have_the_found_services();
    }

    @Test
    @scenario
    public function publish_search_error(){
        given().a_search_was_executed();
        and().the_search_finishes_with_an_error();
        when().the_experiment_is_done();
        then().the_search_failure_cause_should_be_notified();
    }

    @Test
    @scenario
    public function publish_used_mechanisms(){
        given().the_monitor_was_configured_with_some_mechanisms(['a', 'b']);
        when().the_experiment_is_done();
        then().the_published_results_should_contain_the_used_mechanisms();
    }

    @Test
    @scenario
    public function notify_publication_stage_done(){
        given().an_stage_monitor();
        given().some_services_were_published();
        when().the_publication_completes();
        then().the_publication_done_should_be_notified();
    }

    @Test
    @scenario
    public function notify_experiment_finished(){
        given().an_stage_monitor();
        when().the_experiment_finishes();
        then().the_experiment_end_should_be_notified();
    }
}

class ResultsMonitorSteps extends Steps<ResultsMonitorSteps>{

    var resultsMonitor:ResultsMonitor;

    var resultsPublisher = new ResultsPublisherMockup();
    var stageListener = new NodeMonitorMockup();
    var generator = new ExperimentInfoGenerator();

    var configuration:NodeConfiguration;
    var publications:Array<PublicationEvent> = [];

    var searchSpec:SearchSpec;
    var searchFailure:Exception;
    var discoveredEvents:Array<DiscoveredEvent>;
    var expectedSearchResults:Array<SearchResults>=[];

    public function new(){
        super();

        resultsMonitor = new ResultsMonitor(resultsPublisher.publisher());
    }


    // summary ---------------------------------------------------

    @step
    public function the_monitor_was_configured_with_some_mechanisms(
        mechanisms:Array<String>)
    {
        given().a_node_configuration();
        configuration.mechanisms = mechanisms;

        when().configuring_the_monitor();
    }


    // given ---------------------------------------------------

    @step
    public function a_node_configuration() {
        configuration = generator.nodeConfiguration();
    }

    @step
    public function an_stage_monitor() {
        resultsMonitor.stageListener = stageListener.monitor();
    }

    @step
    public function some_services_were_published() {
        for(i in 0...3){
            var pub = generator.publicationEvent();

            publications.push(pub);

            resultsMonitor.onPublication(pub);
        }
    }

    @step
    public function a_search_was_executed() {
        searchSpec = generator.searchSpec();
    }

    @step
    public function some_services_were_discovered_for_that_search() {
        discoveredEvents = generator.discoveredEvents(searchSpec);

        for(evt in discoveredEvents){
            resultsMonitor.onDiscovery(evt);
        }
    }

    @step
    public function the_search_finishes_with_an_error() {
        searchFailure = new Exception('error example');
        resultsMonitor.onSearchFinished({
            error: searchFailure,
            searchSpec: searchSpec,
            interval: RandomGen.time.interval()
        });
    }

    // when  ---------------------------------------------------

    @step
    public function configuring_the_monitor() {
        resultsMonitor.configure(configuration);
    }

    @step
    public function the_experiment_is_done() {
        resultsMonitor.finished();
    }

    @step
    public function the_publication_completes() {
        resultsMonitor.publicationsDone();
    }

    @step
    public function the_experiment_finishes() {
        resultsMonitor.finished();
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_configure_the_results_publisher() {
        resultsPublisher.verifyConfiguredWith(configuration);
    }

    @step
    public function the_results_should_be_published() {
        resultsPublisher.verifyResultsPublished();
    }

    @step
    public function
        the_results_should_contain_info_about_all_published_services()
    {
        var results = resultsPublisher.lastResults();

        var expectedPublications = publications.toPublicationResults();
        expectedPublications.sort(GenericComparator.compare);

        var resultsPublications = results.publications;
        resultsPublications.sort(GenericComparator.compare);

        resultsPublications.shouldBeEqualsTo(expectedPublications);
    }

    @step
    public function the_search_results_should_have_the_found_services() {
        var searchResult = searchResultsShouldHaveThisSearch(searchSpec);

        searchResult.found.length.shouldBe(discoveredEvents.length,
            'The number of search results is not the expected');

        for(i in 0...searchResult.found.length){
            var found = searchResult.found[i];
            var evt = discoveredEvents[i];

            found.id.shouldBeEqualsTo(evt.discovered.identifier(),
                'Found service does not match the expected');
            found.searchTime.shouldBeEqualsTo(evt.interval,
                'Search time does not match the expected'
            );
        }
    }

    @step
    public function the_search_failure_cause_should_be_notified() {
        var searchResult = searchResultsShouldHaveThisSearch(searchSpec);

        searchResult.error.shouldBeEqualsTo(searchFailure,
            'search error is not the expected'
        );
    }

    @step
    public function the_published_results_should_contain_the_used_mechanisms() {
        var results = resultsPublisher.lastResults();

        results.usedMechanisms.shouldBeEqualsTo(configuration.mechanisms,
            '"Used mechanisms" on results does not match the expected value'
        );
    }

    @step
    public function the_publication_done_should_be_notified() {
        stageListener.verifyPublicationsCompleted();
    }

    @step
    public function the_experiment_end_should_be_notified() {
        stageListener.verifyFinishedCalled();
    }

    // -------------------------------------

    function searchResultsShouldHaveThisSearch(searchSpec:SearchSpec) {
        var results = resultsPublisher.lastResults();

        results.searches.shouldNotBeNull('search results should not be null');

        var searchResults = results.searches.filter((r)->{
            return EqualsComparator.equals(r.search, searchSpec);
        });
        searchResults.length.shouldBe(1,
            'Should have only one result for the expected search');

        return searchResults[0];
    }
}