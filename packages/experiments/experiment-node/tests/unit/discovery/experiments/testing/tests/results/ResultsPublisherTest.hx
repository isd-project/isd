package discovery.experiments.testing.tests.results;

import discovery.test.helpers.RandomGen;
import haxe.io.Path;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.results.ResultsPublisher;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ResultsPublisherTest implements Scenario{
    @steps
    var steps: ResultsPublisherSteps;

    @Test
    @scenario
    public function configure_publisher(){
        given().a_configuration_with_the_node_output_dir();
        when().configuring_the_publisher();
        then().the_output_path_should_be_under_that_directory();
        and().the_filename_should_contains_the_node_name();
    }

    @Test
    @scenario
    public function configure_publisher_with_explicit_filename(){
        given().a_configuration_with_the_node_output_dir();
        given().the_configuration_specifies_the_results_filename();
        when().configuring_the_publisher();
        then().the_output_path_should_be_under_that_directory();
        and().the_filename_should_be_the_specified_one();
    }

    @Test
    @scenario
    public function configure_results_output_path(){
        given().a_configuration_with_this_node_output_dir('relative/path');
        given().the_configuration_specifies_this_base_output_dir('output/dir');
        when().configuring_the_publisher();
        then().the_output_path_should_be_under('output/dir/relative/path');
    }
}

class ResultsPublisherSteps extends Steps<ResultsPublisherSteps>{
    var resultsPublisher:ResultsPublisher;

    var generator = new ExperimentInfoGenerator();

    var configuration:NodeConfiguration;

    public function new(){
        super();

        resultsPublisher = new ResultsPublisher();
    }

    // given ---------------------------------------------------


    @step
    public function a_configuration_with_this_node_output_dir(dirpath:String) {
        given().a_configuration_with_the_node_output_dir();

        configuration.outputDir = dirpath;
    }

    @step
    public function a_configuration_with_the_node_output_dir() {
        configuration = generator.nodeConfiguration();

        configuration.outputDir.shouldNotBeNull('outputDir was not generated');
    }

    @step
    public function
        the_configuration_specifies_this_base_output_dir(base_outpath:String)
    {
        configuration.baseOutputDir = base_outpath;
    }

    @step
    public function the_configuration_specifies_the_results_filename() {
        configuration.resultsFile = RandomGen.primitives.name();
    }

    // when  ---------------------------------------------------

    @step
    public function configuring_the_publisher() {
        resultsPublisher.configure(configuration);
    }

    // then  ---------------------------------------------------

    @step
    public function the_output_path_should_be_under_that_directory() {
        then().the_output_path_should_be_under(configuration.outputDir);
    }

    @step
    public function the_output_path_should_be_under(expected_outpath:String) {
        resultsPublisher.outputPath.shouldStartWith(expected_outpath);
    }

    @step
    public function the_filename_should_contains_the_node_name() {
        var filename = resultsFilename();

        filename.shouldContainString(configuration.nodeName);
    }

    @step
    public function the_filename_should_be_the_specified_one() {
        resultsFilename().shouldBeEqualsTo(configuration.resultsFile);
    }

    function resultsFilename(){
        return Path.withoutDirectory(resultsPublisher.outputPath);
    }
}