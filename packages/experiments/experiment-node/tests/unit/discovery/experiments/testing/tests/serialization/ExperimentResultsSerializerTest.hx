package discovery.experiments.testing.tests.serialization;

import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.runner.SearchSpec;
import haxe.Exception;
import discovery.test.helpers.RandomGen;
import discovery.utils.time.DateMaker;
import discovery.utils.time.TimeInterval;
import haxe.io.Bytes;
import discovery.experiment.results.ExperimentResults;
import discovery.experiment.serialization.ExperimentResultsSerializer;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ExperimentResultsSerializerTest implements Scenario{
    @steps
    var steps: ExperimentResultsSerializerSteps;

    var serializer:ExperimentResultsSerializer;
    var generator:ExperimentInfoGenerator;

    @Before
    public function setup() {
        serializer = new ExperimentResultsSerializer();
        generator = new ExperimentInfoGenerator();
    }

    @Test
    @scenario
    public function deserialize(){
        var expected:ExperimentResults = {
            publications: [
                {
                    service: {
                        name: 'name',
                        type: 'type',
                        providerId: Bytes.ofHex('abcdef'),
                    },
                    interval: new TimeInterval(
                        Date.fromString('1234-05-06'),
                        Date.fromString('1234-05-07')
                    )
                }
            ],
            searches: [],
            usedMechanisms: ['a', 'b']
        };

        var parsed = serializer.deserialize({
            publications: [
                {
                    service: {
                        name: 'name',
                        type: 'type',
                        providerId: Bytes.ofHex('abcdef'),
                    },
                    interval: {
                        start: Date.fromString('1234-05-06'),
                        end: Date.fromString('1234-05-07')
                    }
                }
            ],
            searches: [],
            usedMechanisms: ['a', 'b']
        });

        parsed.shouldBeEqualsTo(expected);
    }

    @Test
    @scenario
    public function serialize_deserialize_search_results(){
        test_serialize_and_deserialize({
            publications: [],
            searches: [
                {
                    search: {
                        delay: 12,
                        srvType: 'type1',
                        kind: Search
                    },
                    found: [
                        {
                            id: {
                                instanceId: Bytes.ofHex('abcd'),
                                providerId: Bytes.ofHex('1234abcd'),
                                type: 'type1',
                                name: 'srv1'
                            },
                            searchTime: new TimeInterval(
                                DateMaker.fromValues(2000, 1, 2, 3, 4, 5),
                                DateMaker.fromValues(2000, 2, 3, 4, 5, 6)
                            )
                        }
                    ]
                }
            ],
            usedMechanisms: ['a', 'b']
        });
    }

    @Test
    @scenario
    public function test_serialize_and_deserialize_failed_search(){
        test_serialize_and_deserialize({
            searches: [
                {
                    search: any_search_spec(),
                    finishedTime: RandomGen.time.interval(),
                    error: new Exception('error example')
                }
            ],
            publications: [],
            usedMechanisms: []
        });
    }

    function test_serialize_and_deserialize(results: ExperimentResults) {
        var serialized = serializer.serialize(results);
        var deserialized = serializer.deserialize(serialized);

        deserialized.shouldBeEqualsTo(results, 'Deserialized does not match the expected value');
    }

    function any_search_spec():SearchSpec {
        return generator.searchSpec();
    }
}

class ExperimentResultsSerializerSteps
    extends Steps<ExperimentResultsSerializerSteps>
{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}