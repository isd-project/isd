package discovery.experiments.testing.mockups;

import discovery.impl.Defaults;
import discovery.utils.functional.Callback;
import discovery.utils.events.EventEmitter;
import discovery.experiment.runner.StageControl;
import mockatoo.Mockatoo;

class StageControlMockup {
    var control = Mockatoo.mock(StageControl);

    var nextStageEvent: EventEmitter<Any>;

    public function new() {
        var factory = Defaults.eventFactory();
        nextStageEvent = factory.eventEmitter();

        Mockatoo.when(control.waitForNextStage(any))
            .thenCall(onWaitForNextStageCall);
    }

    function onWaitForNextStageCall(args: Array<Dynamic>) {
        var callback:Callback = args[0];

        nextStageEvent.listen((_)->callback());
    }

    public function stageControl():StageControl {
        return control;
    }

    public function releaseNextStage() {
        nextStageEvent.notify(null);
    }

    public function verifyWaitForNextStageWasCalled() {
        Mockatoo.verify(control.waitForNextStage(any));
    }
}