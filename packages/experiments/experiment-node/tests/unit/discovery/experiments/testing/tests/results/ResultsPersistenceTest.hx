package discovery.experiments.testing.tests.results;

import discovery.utils.time.DateMaker;
import discovery.utils.time.TimeInterval;
import haxe.io.Bytes;
import discovery.experiment.results.ExperimentResults;
import discovery.experiment.results.ResultsPersistence;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import hxgiven.Steps;
import hxgiven.Scenario;

using StringTools;
using discovery.test.matchers.CommonMatchers;


class ResultsPersistenceTest implements Scenario{
    @steps
    var steps: ResultsPersistenceSteps;

    var generator: ExperimentInfoGenerator;
    var persistence: ResultsPersistence;

    static final publicationResults:ExperimentResults = {
        publications: [
            {
                service: {
                    name: 'srvname',
                    type: 'srvtype',
                    providerId: Bytes.ofHex('abcdef')
                },
                interval: new TimeInterval(
                    DateMaker.fromValues(1234,5,6,7,8,9),
                    DateMaker.fromValues(1234,5,6,7,8,10)
                )
            }
        ],
        searches: [],
        usedMechanisms: ['a', 'b', 'c']
    };

    static final publicationResultsYaml = '
publications:
  - service:
      name: srvname
      type: srvtype
      providerId: !<tag:yaml.org,2002:binary> q83v
      instanceId: null
      parameters: null
    interval:
      start: !<tag:yaml.org,2002:timestamp> "1234-05-06T07:08:09.000Z"
      end: !<tag:yaml.org,2002:timestamp> "1234-05-06T07:08:10.000Z"
searches: []
usedMechanisms:
  - a
  - b
  - c
    '.trim();

    @Before
    public function setup() {
        generator = new ExperimentInfoGenerator();
        persistence = new ResultsPersistence();
    }

    @Test
    @scenario
    public function parse_results(){
        var parsed = persistence.parse(publicationResultsYaml);
        parsed.shouldBeEqualsTo(publicationResults);
    }


    @Test
    @scenario
    public function render_results(){
        var renderedResults = persistence.render(publicationResults);

        var renderedLines = renderedResults.split('\n');
        var expectedLines = publicationResultsYaml.split('\n');

        for(i in 0...expectedLines.length){
            var foundLine = renderedLines[i].rtrim();
            var expectedLine = expectedLines[i].rtrim();

            foundLine.shouldBeEqualsTo(expectedLine,
                'Differ in line ${i}');
        }
    }

    @Test
    @scenario
    public function render_and_parse(){
        var results = generator.experimentResults();

        var serialized = persistence.render(results);
        var deserialized = persistence.parse(serialized);

        deserialized.shouldBeEqualsTo(results);
    }
}

class ResultsPersistenceSteps extends Steps<ResultsPersistenceSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}