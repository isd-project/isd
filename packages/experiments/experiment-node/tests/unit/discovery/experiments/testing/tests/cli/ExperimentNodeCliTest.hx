package discovery.experiments.testing.tests.cli;

import haxe.Exception;
import discovery.experiment.cli.CliOptions;
import discovery.experiment.cli.CliHandler;
import discovery.experiment.cli.ExperimentNodeCli;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ExperimentNodeCliTest implements Scenario{
    @steps
    var steps: ExperimentNodeCliSteps;

    @Test
    @scenario
    public function parse_default_arguments(){
        final conf_file = 'config_file.yaml';
        final node_name = 'node1';
        final experimentDir = '/some/directory';

        given().a_cli_handler();
        when().parsing_these_arguments([conf_file, node_name, experimentDir]);
        then().the_handler_should_be_called_with_these_options({
            configFile: conf_file,
            nodeName: node_name,
            experimentDir: experimentDir,
            mechanisms: null,
            outputDir: null
        });
    }

    @Test
    @scenario
    public function parse_mechanism_options(){
        final conf_file = 'config_file.yaml';
        final node_name = 'node1';
        final expDir = '/some/directory';

        given().a_cli_handler();
        when().parsing_these_arguments([
            conf_file, node_name, expDir, '-m', 'mdns', '-m', 'dht'
        ]);
        then().the_handler_should_be_called_with_these_options({
            configFile: conf_file,
            nodeName: node_name,
            experimentDir: expDir,
            mechanisms: ['mdns', 'dht'],
            outputDir: null
        });
    }


    @Test
    @scenario
    public function parse_output_dir(){
        final conf_file = 'config_file.yaml';
        final node_name = 'node1';
        final expDir = '/some/directory';
        final output_dir = 'path/to/outputdir';

        given().a_cli_handler();
        when().parsing_these_arguments([
            conf_file, node_name, expDir, '-o', output_dir
        ]);
        then().the_handler_should_be_called_with_these_options({
            configFile: conf_file,
            nodeName: node_name,
            experimentDir: expDir,
            mechanisms: null,
            outputDir: output_dir
        });
    }

    @Test
    @scenario
    public function handle_missing_arguments(){
        given().a_cli_handler();
        when().parsing_these_arguments([]);
        then().the_handler_should_receive_an_error();
    }

    @Test
    @scenario
    public function getting_cli_usage(){
        when().getting_the_cli_usage();
        then().it_should_return_a_text_that_contains_the_cli_options();
    }
}

class ExperimentNodeCliSteps extends Steps<ExperimentNodeCliSteps>{

    var cliHandler = new FakeCliHandler();

    var nodeCli = new ExperimentNodeCli();

    var usage:String;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function a_cli_handler() {}

    // when  ---------------------------------------------------

    @step
    public function parsing_these_arguments(args:Array<String>) {
        nodeCli.parseArguments(args, cliHandler);
    }

    // then  ---------------------------------------------------

    @step
    public function the_handler_should_be_called_with_these_options(
        options:CliOptions)
    {
        cliHandler.options.shouldBeEqualsTo(options,
            'Received cliOptions does not match the expected');
    }

    @step
    public function the_handler_should_receive_an_error() {
        cliHandler.receivedError.shouldNotBeNull('Should receive an error');
    }

    @step
    public function getting_the_cli_usage() {
        usage = nodeCli.getUsage();
    }

    @step
    public function it_should_return_a_text_that_contains_the_cli_options() {
        usage.shouldNotBeNull('usage should not be null');

        usage.shouldContainString('configFile');
        usage.shouldContainString('nodeName');
        usage.shouldContainString('--mechanism');
    }
}

class FakeCliHandler implements CliHandler {
    public var options:CliOptions;
    public var receivedError:Exception;

    public function new() {

    }

    public function start(options:CliOptions) {
        this.options = options;
    }

    public function onError(err:Exception) {
        this.receivedError = err;
    }
}