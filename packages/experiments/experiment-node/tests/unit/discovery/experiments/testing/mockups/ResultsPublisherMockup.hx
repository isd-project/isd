package discovery.experiments.testing.mockups;

import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.results.ExperimentResults;
import mockatoo.Mockatoo;
import discovery.experiment.results.ResultsPublisher;

class ResultsPublisherMockup {
    var resultsPublisher = Mockatoo.mock(ResultsPublisher);

    var publishedResults:Array<ExperimentResults> = [];

    public function new() {
        Mockatoo.when(resultsPublisher.publishResults(any)).thenCall(onResults);
    }

    function onResults(args: Array<Dynamic>) {
        publishedResults.push(args[0]);
    }

    public function publisher():ResultsPublisher {
        return resultsPublisher;
    }

    public function verifyResultsPublished() {
        Mockatoo.verify(resultsPublisher.publishResults(isNotNull));
    }

    public function lastResults() {
        return publishedResults[publishedResults.length - 1];
    }

    public function verifyConfiguredWith(configuration:NodeConfiguration) {
        Mockatoo.verify(resultsPublisher.configure(configuration));
    }
}