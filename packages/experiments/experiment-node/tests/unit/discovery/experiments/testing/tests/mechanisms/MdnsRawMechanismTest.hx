package discovery.experiments.testing.tests.mechanisms;

import discovery.test.common.CallableMockup;
import discovery.mdns.DnsSd.DnsSdResolveQuery;
import discovery.domain.Identifier;
import discovery.format.Binary;
import discovery.domain.Discovered;
import discovery.mdns.DiscoveredService;
import discovery.mdns.MdnsQuery;
import discovery.domain.Query;
import discovery.test.common.mockups.discovery.PublishListenersMockup;
import discovery.test.mdns.generators.MdnsGenerator;
import discovery.test.mdns.mockups.DnsSdMockup;
import discovery.domain.Announcement;
import discovery.domain.PublishInfo;
import discovery.experiment.mechanisms.MdnsRawMechanism;
import discovery.mdns.MdnsPublishOptions.MdnsPublishData;
import discovery.domain.Description;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.time.TimeUnitTools;
using discovery.test.matchers.CommonMatchers;


class MdnsRawMechanismTest implements Scenario{
    @steps
    var steps: MdnsRawMechanismSteps;

    @Test
    @scenario
    public function publish_service(){
        given().a_description_of({
            name: 'srvname',
            type: 'srvtype',
            providerId: RandomGen.binary.binary(),
            port: 12345
        });
        when().publishing_the_service();
        then().a_service_should_be_published_with({
            name: 'srvname',
            type: 'srvtype',
            port: 12345
        });
    }

    @Test
    @scenario
    public function notify_publish_done(){
        given().a_service_was_published();
        when().the_publication_is_done();
        then().the_publish_listener_should_be_notified();
    }

    @Test
    @scenario
    public function search_for_service(){
        given().a_search_query_of({
            type: 'srvtype'
        });
        when().starting_a_service_search();
        then().this_search_should_be_executed({
            type: 'srvtype'
        });
    }

    @Test
    @scenario
    public function notify_discovery(){
        given().a_search_started();
        when().this_dnssd_service_is_found({
            name: 'srvname',
            type: 'srvtype',
            port: 12345,
            addresses: [
                '123.45.67.89', 'dead::beef'
            ]
        });
        then().the_discovery_should_be_notified_as({
            name: 'srvname',
            type: 'srvtype',
            port: 12345,
            location: {
                addresses: [
                    '123.45.67.89', 'dead::beef'
                ]
            }
        });
    }

    @Test
    @scenario
    public function locate_service_instance(){
        given().a_locate_search_for({
            name: 'srvname',
            type: 'srvtype',
            providerId: RandomGen.binary.binary()
        });
        when().starting_locating_the_service();
        then().this_resolve_search_should_be_executed({
            name: 'srvname',
            type: 'srvtype'
        });
    }

    @Test
    @scenario
    public function notify_locate_discovery(){
        var found:DiscoveredService = {
            name: 'srvname',
            type: 'srvtype',
            port: 12345,
            addresses: [
                '123.45.67.89', 'dead::beef'
            ]
        };

        given().a_locate_search_started();
        when().this_dnssd_service_is_resolved(found);
        then().the_discovery_should_be_notified_as({
            name: found.name,
            type: found.type,
            port: found.port,
            location: {
                addresses: found.addresses
            }
        });
    }

    @Test
    @scenario
    public function finish_mechanism(){
        when().finishing_the_mechanism();
        then().it_should_finish_the_underlying_engine();
    }
}

class MdnsRawMechanismSteps extends Steps<MdnsRawMechanismSteps>{
    var mechanism:DiscoveryMechanism;

    var dnsSd = new DnsSdMockup();
    var generator = new MdnsGenerator(RandomGen.primitives, RandomGen.binary);
    var publishListeners = new PublishListenersMockup();
    var finishCallback = new CallableMockup();

    var description:Description;
    var searchQuery:Query;
    var identifier:Identifier;
    var foundService:Discovered;

    public function new(){
        super();

        mechanism = new MdnsRawMechanism(dnsSd.dnsSd());
    }


    // summary ---------------------------------------------------

    @step
    public function a_service_was_published() {
        given().a_description_of(RandomGen.description());
        when().publishing_the_service();
    }

    @step
    public function a_search_started() {
        given().a_search_query_of({
            type: RandomGen.primitives.name()
        });
        when().starting_a_service_search();
    }

    @step
    public function a_locate_search_started() {
        given().a_locate_search_for({
            name: RandomGen.primitives.name(),
            type: RandomGen.primitives.name(),
            providerId: RandomGen.binary.binary()
        });
        when().starting_locating_the_service();
    }

    // given ---------------------------------------------------

    @step
    public function a_description_of(description:Description) {
        this.description = description;
    }

    @step
    public function a_search_query_of(query:Query) {
        this.searchQuery = query;
    }

    @step
    public function a_locate_search_for(identifier:Identifier) {
        this.identifier = identifier;
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_the_service() {
        var validity = AnnouncementValidity.validFor(30.minutes());
        var announcement = Announcement.make(description, validity);

        mechanism.publish(PublishInfo.make(announcement),
                            publishListeners.listeners());
    }

    @step
    public function the_publication_is_done() {
        dnsSd.completeLastPublication();
    }

    @step
    public function starting_a_service_search() {
        mechanism.search(searchQuery).onFound.listen((discovered)->{
            foundService = discovered;
        });
    }

    @step
    public function starting_locating_the_service() {
        mechanism.locate(identifier).onFound.listen((discovered)->{
            foundService = discovered;
        });
    }

    @step
    public function this_dnssd_service_is_found(srv:DiscoveredService) {
        dnsSd.notifyDiscoveryForLastQuery(srv);
    }

    @step
    public function this_dnssd_service_is_resolved(srv:DiscoveredService) {
        dnsSd.notifyDiscoveryForLastResolveQuery(srv);
    }

    @step
    public function finishing_the_mechanism() {
        mechanism.finish(finishCallback.callable());
    }

    // then  ---------------------------------------------------

    @step
    public function a_service_should_be_published_with(srvInfo:MdnsPublishData)
    {
        dnsSd.verifyPublishedService(srvInfo);
    }

    @step
    public function the_publish_listener_should_be_notified() {
        publishListeners.verifyOnPublishCalled();
    }

    @step
    public function this_search_should_be_executed(dnsSdQuery: MdnsQuery) {
        dnsSd.verifySearchWith(dnsSdQuery);
    }

    @step
    public function
        this_resolve_search_should_be_executed(query:DnsSdResolveQuery)
    {
        dnsSd.verifyResolveSearchWith(query);
    }

    @step
    public function the_discovery_should_be_notified_as(d:Description) {
        foundService.shouldNotBeNull('no service found');

        foundService.description().shouldBeEqualsTo(d,
            'Discovered service does not match the expected');
    }

    @step
    public function it_should_finish_the_underlying_engine() {
        dnsSd.verifyFinishedWith(finishCallback.callable());
    }
}