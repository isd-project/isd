package discovery.experiments.testing.scenarios;

import discovery.test.common.mockups.utils.TaskSchedulerMockup;
import discovery.experiments.testing.mockups.StageControlMockup;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import discovery.experiment.ExperimentRunner;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class CaptureMeasuresTest implements Scenario{
    @steps
    var steps: CaptureMeasuresSteps;

    @Test
    @scenario
    public function measures_publication_time(){
        given().a_node_monitor();
        given().a_publication_has_started();
        when().the_publication_completes();
        then().the_publication_should_be_notified();
        and().the_publication_time_should_be_valid();
    }

    @Test
    @scenario
    public function receive_all_publication_notifications(){
        given().a_node_monitor();
        given().a_publication_of_some_services_has_started();
        when().all_publications_completes();
        then().all_publication_events_should_be_notified();
    }
}


class CaptureMeasuresSteps extends Steps<CaptureMeasuresSteps>{

    var nodeMonitor = new NodeMonitorMockup();
    var discovery = new DiscoveryMockup();
    var scheduler = new TaskSchedulerMockup();
    var stageControl = new StageControlMockup();
    var experimentRunner:ExperimentRunner;

    var publishedSrvs:Array<ServicePublication>;

    var beforeStartTime:Date;
    var beforeEndTime:Date;
    var afterEndTime:Date;

    public function new(){
        super();

        experimentRunner = new ExperimentRunner(discovery.discovery(),
                                            scheduler.scheduler());
    }

    // given ---------------------------------------------------

    @step
    public function a_node_monitor() {}

    @step
    public function a_publication_has_started() {
        publishAndStartServices(1);
    }

    @step
    public function a_publication_of_some_services_has_started() {
        publishAndStartServices(3);
    }

    // when  ---------------------------------------------------

    @step
    public function the_publication_completes() {
        beforeEndTime = Date.now();
        discovery.notifyPublication();
        afterEndTime = Date.now();
    }

    @step
    public function all_publications_completes() {
        discovery.notifyPublications([
            for(p in publishedSrvs) RandomGen.publishInfo(p.description)
        ]);
    }

    // then  ---------------------------------------------------

    @step
    public function the_publication_should_be_notified() {
        nodeMonitor.assertPublicationNotified();
    }

    @step
    public function all_publication_events_should_be_notified() {
        nodeMonitor.assertPublicationsNotified(publishedSrvs.length);
    }

    @step
    public function the_publication_time_should_be_valid() {
        var pubEvt = nodeMonitor.lastPublication();

        pubEvt.interval.shouldNotBeNull('Publication interval should not be null');

        pubEvt.interval.start
            .shouldBeBetween(beforeStartTime, beforeEndTime, 'start time');
        pubEvt.interval.end
            .shouldBeBetween(beforeEndTime, afterEndTime, 'end time');
    }

    // helpers -------------------------------------------------------

    function publishAndStartServices(numSrvs: Int){
        publishServices(numSrvs);
        startServicesPublication(numSrvs);
    }

    function publishServices(numSrvs: Int) {
        publishedSrvs = [for(i in 0...numSrvs) srvPublication()];

        experimentRunner.configure({
            publications: publishedSrvs
        });

        experimentRunner.start(stageControl.stageControl(), nodeMonitor.monitor());
    }

    function startServicesPublication(numSrvs: Int){
        beforeStartTime = Date.now();

        for(i in 0...numSrvs){
            scheduler.nextScheduledTimeComplete();
        }
    }

    function srvPublication(): ServicePublication {
        return {
            description: RandomGen.description()
        };
    }
}