package discovery.experiments.testing.tests.parsing;

import discovery.experiments.testing.mockups.config.ConfigurationReaderMockup;
import discovery.experiments.testing.mockups.config.ConfigurationParserMockup;
import discovery.experiments.testing.mockups.ConfigurationLoaderMockup;
import discovery.experiment.parsing.ConfigurationLoader;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class ConfigurationLoaderTest implements Scenario{
    @steps
    var steps: ConfigurationLoaderSteps;

    @Test
    @scenario
    public function load_configuration(){
        given().a_configuration_file_and_node_name();
        when().loading_the_configuration();
        then().the_config_file_should_be_read();
        and().the_config_content_should_be_parsed();
    }
}

class ConfigurationLoaderSteps extends Steps<ConfigurationLoaderSteps>{
    var configLoader:ConfigurationLoader;

    var configReader = new ConfigurationReaderMockup();
    var configParser = new ConfigurationParserMockup();

    var configFilePath:String;
    var nodeName:String;

    public function new(){
        super();

        configLoader = new ConfigurationLoader({
            reader: configReader.reader(),
            parser: configParser.parser()
        });
    }

    // given ---------------------------------------------------

    @step
    public function a_configuration_file_and_node_name() {
        configFilePath = RandomGen.filepath();
        nodeName = 'node1';
    }

    // when  ---------------------------------------------------

    @step
    public function loading_the_configuration() {
        configLoader.loadConfiguration(configFilePath, nodeName);
    }

    // then  ---------------------------------------------------

    @step
    public function the_config_file_should_be_read() {
        configReader.shouldHaveReadConfiguration(configFilePath);
    }

    @step
    public function the_config_content_should_be_parsed() {
        configParser.shouldHaveParsedConfigForNode(nodeName);
    }
}