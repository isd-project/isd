package discovery.experiments.testing.tests.runner;

import discovery.test.common.mockups.callbacks.DoneCallbackMockup;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.impl.parser.TinkIdentifierParser;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import discovery.test.common.mockups.utils.TaskSchedulerMockup;
import discovery.experiment.runner.SearchStage;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.runner.SearchSpec;
import hxgiven.Steps;

using discovery.test.matchers.CommonMatchers;


class SearchStageBaseSteps<Self:SearchStageBaseSteps<Self>>
    extends Steps<Self>
{
    var searchStage:SearchStage;

    var discovery   = new DiscoveryMockup();
    var scheduler   = new TaskSchedulerMockup();
    var nodeMonitor = new NodeMonitorMockup();
    var generator   = new ExperimentInfoGenerator();

    var callbackMockup = new DoneCallbackMockup();

    var identifierParser = new TinkIdentifierParser();

    var searchSpecs:Array<SearchSpec> = [];


    public function new(){
        super();

        searchStage = new SearchStage({
            discovery: discovery.discovery(),
            scheduler: scheduler.scheduler(),
            idParser: identifierParser,
            monitor: nodeMonitor.monitor()
        });
    }

    // summary ---------------------------------------------------

    @step
    public function some_searches_were_executed() {
        given().some_searches_were_started();
        when().all_tasks_have_been_scheduled();
    }

    @step
    public function some_search_is_executed(?kind:SearchKind) {
        if(kind == null) kind = generator.searchKind();

        given().a_search_of_this_kind_were_started(kind);
        when().the_first_task_is_scheduled();
    }

    @step
    public function some_searches_were_started() {
        given().some_searches_specifications();
        when().searching_them_all();
    }


    // given ---------------------------------------------------

    @step
    public function a_node_monitor() {}

    @step
    public function a_search_end_callback() {}

    @step
    public function a_search_of_this_kind_were_started(searchKind:SearchKind) {
        var s = generator.searchSpec(searchKind);

        given().these_search_specs([s]);
        when().searching_them_all();
    }

    @step
    public function some_searches_specifications() {
        given().these_search_specs(generator.searchSpecifications());
    }


    @step
    function these_search_specs(sSpecs: Array<SearchSpec>) {
        searchSpecs = sSpecs;
    }

    // when  ---------------------------------------------------

    @step
    public function searching_them_all() {
        when().starting_the_search();
    }

    @step
    public function starting_the_search() {
        searchStage.searchAll(searchSpecs, callbackMockup.callback());
    }

    @step
    public function all_tasks_have_been_scheduled() {
        while(scheduler.hasPendingTasks()){
            when().the_next_task_is_scheduled();
        }
    }

    @step
    function the_next_task_is_scheduled() {
        scheduler.nextScheduledTimeComplete();
    }

    @step
    public function the_first_task_is_scheduled() {
        when().the_next_task_is_scheduled();
    }

    @step
    public function all_searches_concludes() {
        discovery.notifyAllsSearchesFinished();
    }

    // then  ---------------------------------------------------

}