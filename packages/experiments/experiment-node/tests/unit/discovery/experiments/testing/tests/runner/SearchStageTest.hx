package discovery.experiments.testing.tests.runner;

import discovery.base.search.SearchOptions;
import org.hamcrest.AssertionException;
import discovery.experiment.runner.SearchSpec;
import hxgiven.Scenario;


class SearchStageTest implements Scenario{
    @steps
    var steps: SearchStageSteps;

    @Test
    @scenario
    public function schedule_searches(){
        given().some_searches_specifications();
        when().searching_them_all();
        then().those_searches_should_be_scheduled_for_execution();
    }

    @Test
    @scenario
    public function execute_search_query_when_scheduled(){
        given().a_search_of_this_kind_were_started(Search);
        when().the_first_task_is_scheduled();
        then().the_first_search_should_be_executed();
    }

    @Test
    @scenario
    public function execute_locate_query_when_scheduled(){
        given().a_search_of_this_kind_were_started(Locate);
        when().the_first_task_is_scheduled();
        then().the_first_search_should_be_executed();
    }

    @Test
    @scenario
    public function execute_all_tasks_when_scheduled(){
        given().some_searches_were_started();
        when().all_tasks_have_been_scheduled();
        then().all_tasks_should_have_been_executed();
    }

    @Test
    @scenario
    public function notify_search_stage_end__after_all_searches_finish(){
        given().a_node_monitor();
        and().a_search_end_callback();
        given().some_searches_were_executed();
        when().all_searches_concludes();
        then().the_search_end_should_be_notified();
    }

    @Test
    @scenario
    public function notify_search_stage_end__if_no_search_was_specified(){
        given().no_search_were_specified();
        when().starting_the_search();
        then().the_search_end_should_be_notified();
    }
}

class SearchStageSteps extends SearchStageBaseSteps<SearchStageSteps>{

    // summary ---------------------------------------------------

    // given ---------------------------------------------------

    @step
    public function no_timeout_was_specified() {}

    @step
    public function the_node_configuration_has_an_specified_timeout() {

    }

    @step
    public function no_search_were_specified() {}

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

    @step
    public function those_searches_should_be_scheduled_for_execution() {
        for(s in searchSpecs){
            scheduler.verifyScheduledTaskWith(s.delay);
        }
    }

    @step
    public function all_tasks_should_have_been_executed() {
        for(s in searchSpecs){
            then().this_search_should_be_executed(s);
        }
    }

    @step
    public function the_first_search_should_be_executed() {
        then().this_search_should_be_executed(searchSpecs[0]);
    }

    @step
    function this_search_should_be_executed(searchSpec:SearchSpec) {
        if(searchSpec.isSearch()){
            var query = searchSpec.query();

            discovery.verifySearchWith(query);
        }
        else{
            var identifier = searchSpec.identifier(identifierParser);

            discovery.verifyLocateCalledWith(identifier);
        }
    }

    @step
    public function the_search_end_should_be_notified() {
        nodeMonitor.verifySearchesFinishedCalled();

        try{
            callbackMockup.assertWasCalled();
        }catch(e){
            throw new AssertionException('search end callback was not called',e);
        }
    }
}