package discovery.experiments.testing.mockups;

import discovery.experiment.events.SearchEndEvent;
import discovery.experiment.runner.NodeMonitor;
import haxe.Exception;
import discovery.experiment.events.DiscoveredEvent;
import discovery.test.matchers.CommonMatchers;
import massive.munit.Assert;
import discovery.experiment.events.PublicationEvent;
import mockatoo.Mockatoo;

class NodeMonitorMockup {
    var nodeMonitor = Mockatoo.spy(FakeNodeMonitor);

    public function new() {
    }

    public function monitor(): NodeMonitor {
        return nodeMonitor;
    }

    public function verifyPublicationsCompleted() {
        Mockatoo.verify(nodeMonitor.publicationsDone());
    }

    public function assertPublicationNotified() {
        Assert.isNotNull(lastPublication(), 'No publication notified');
    }

    public function assertPublicationsNotified(count:Int) {
        CommonMatchers.shouldBeEqualsTo(nodeMonitor.numPublications(), count, 'The number of publications is not the expected');
    }

    public function lastPublication() {
        return nodeMonitor.lastPublication();
    }

    public function lastDiscoveredEvent() {
        return nodeMonitor.lastDiscoveryEvent();
    }


    public function verifySearchesFinishedCalled() {
        Mockatoo.verify(nodeMonitor.searchesFinished());
    }

    public function verifyFinishedCalled() {
        this.verifyFinishCalled();
    }

    public function verifyFinishNotCalledYet() {
        this.verifyFinishCalled(never);
    }

    function verifyFinishCalled(?times:VerificationMode) {
        Mockatoo.verify(nodeMonitor.finished(), times);
    }

    public function verifySearchFinishedWhere(matcher:(SearchEndEvent)->Bool) {


        Mockatoo.verify(nodeMonitor.onSearchFinished(customMatcher(matcher)));
    }
}

class FakeNodeMonitor implements NodeMonitor{
    var publications:Array<PublicationEvent> = [];
    var discoveryEvents:Array<DiscoveredEvent> = [];
    var searchEndEvent:Array<SearchEndEvent> = [];

    public function new() {}

    public function publicationsDone() {}
    public function searchesFinished() {}
    public function finished() {}

    public function onPublication(pubEvt:PublicationEvent) {
        publications.push(pubEvt);
    }

    public function onSearchFinished(evt:SearchEndEvent) {
        searchEndEvent.push(evt);
    }

    public function lastPublication() {
        return lastItem(publications);
    }

    public function numPublications() {
        return publications.length;
    }

    public function onDiscovery(evt:DiscoveredEvent) {
        discoveryEvents.push(evt);
    }

    public function lastDiscoveryEvent() {
        return lastItem(discoveryEvents);
    }

    function lastItem<T>(items:Array<T>): Null<T> {
        if(items.length == 0){
            return null;
        }

        return items[items.length - 1];
    }
}