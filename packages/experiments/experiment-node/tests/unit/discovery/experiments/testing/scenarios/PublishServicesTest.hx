package discovery.experiments.testing.scenarios;

import discovery.experiment.runner.NodeMonitor;
import discovery.experiments.testing.mockups.StageControlMockup;
import discovery.test.common.mockups.utils.TaskSchedulerMockup;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import discovery.experiment.ExperimentRunner;
import hxgiven.Steps;
import hxgiven.Scenario;


class PublishServicesTest implements Scenario{
    @steps
    var steps: PublishServicesSteps;

    @Test
    @scenario
    public function publish_specified_service(){
        var srv = steps.generator.servicePublication();

        given().a_configured_node({
            publications: [srv]
        });
        when().starting_the_node();
        then().it_should_schedule_these_service_publications([srv]);
    }

    @Test
    @scenario
    public function should_publish_service_when_scheduled(){
        given().a_service_publication_was_started();
        when().the_schedule_time_completes();
        then().the_first_service_should_be_published();
    }

    @Test
    @scenario
    public function notify_publication_done(){
        given().a_node_monitor();
        given().a_service_publication_was_started();
        when().the_publication_completes();
        then().the_publication_completion_should_be_notified();
    }

    @Test
    @scenario
    public function schedule_services_publications_with_given_times(){
        given().an_experiment_configured_to_publish_some_services();
        when().starting_the_experiment();
        then().the_publications_should_be_scheduled_with_given_times();
    }

    @Test
    @scenario
    public function should_publish_all_services_when_delay_times_completes(){
        given().an_experiment_configured_to_publish_some_services();
        when().starting_the_experiment();
        and().all_scheduled_publications_are_executed();
        then().all_services_should_be_published();
    }
}

class PublishServicesSteps extends Steps<PublishServicesSteps>{
    var experimentRunner:ExperimentRunner;

    public var generator = new ExperimentInfoGenerator();
    var discoveryMockup = new DiscoveryMockup();
    var nodeMonitorMockup = new NodeMonitorMockup();
    var schedulerMockup = new TaskSchedulerMockup();
    var stageControl = new StageControlMockup();

    var nodeMonitor:NodeMonitor;
    var configuration:NodeConfiguration;

    public function new(){
        super();
    }

    // given summary -------------------------------------------

    @step
    public function a_service_publication_was_started() {
        given().a_configured_node({
            publications: [generator.servicePublication()]
        });
        when().starting_the_node();
    }

    // given ---------------------------------------------------


    @step
    public function a_node_monitor() {
        nodeMonitor = nodeMonitorMockup.monitor();
    }

    @step
    public function an_experiment_configured_to_publish_some_services() {
        given().a_configured_node({
            publications: generator.servicePublications(3)
        });
    }

    @step
    public function a_configured_node(config: NodeConfiguration) {
        experimentRunner = new ExperimentRunner(
                            discoveryMockup.discovery(),
                            schedulerMockup.scheduler());

        experimentRunner.configure(config);
        this.configuration = config;
    }

    // when  ---------------------------------------------------

    @step
    public function starting_the_node() {
        when().starting_the_experiment();
    }

    @step
    public function starting_the_experiment() {
        experimentRunner.start(stageControl.stageControl(), nodeMonitor);
    }

    @step
    public function the_publication_completes() {
        when().the_schedule_time_completes();

        discoveryMockup.notifyPublication();
    }

    @step
    public function the_schedule_time_completes() {
        schedulerMockup.nextScheduledTimeComplete();
    }

    @step
    public function all_scheduled_publications_are_executed() {
        for(srv in configuration.publications){
            schedulerMockup.nextScheduledTimeComplete();
        }
    }

    // then  ---------------------------------------------------

    @step
    public function the_publications_should_be_scheduled_with_given_times() {
        then().it_should_schedule_these_service_publications(
                                                    configuration.publications);
    }

    @step
    public function it_should_schedule_these_service_publications
                        (publications:Array<ServicePublication>)
    {
        for(publication in publications){
            schedulerMockup.verifyScheduledTaskWith(publication.delay);
        }
    }

    @step
    public function the_first_service_should_be_published() {
        then().it_should_publish_these_services([
            configuration.publications[0]
        ]);
    }

    @step
    public function all_services_should_be_published() {
        then().it_should_publish_these_services(configuration.publications);
    }

    @step
    public function
        it_should_publish_these_services(srvs:Array<ServicePublication>)
    {
        for(srv in srvs){
            discoveryMockup.verifyPublished(srv.description);
        }
    }

    @step
    public function the_publication_completion_should_be_notified() {
        nodeMonitorMockup.verifyPublicationsCompleted();
    }
}