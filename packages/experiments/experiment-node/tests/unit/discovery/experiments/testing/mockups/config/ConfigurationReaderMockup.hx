package discovery.experiments.testing.mockups.config;

import discovery.experiment.parsing.ConfigurationReader;
import mockatoo.Mockatoo;

class ConfigurationReaderMockup {
    var configReader = Mockatoo.mock(ConfigurationReader);

    public function new() {

    }

    public function reader(): ConfigurationReader {
        return configReader;
    }

    public function shouldHaveReadConfiguration(configFilePath:String) {
        Mockatoo.verify(configReader.readConfiguration(configFilePath));
    }
}