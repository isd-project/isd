package discovery.experiments.testing.tests;

import haxe.io.Path;
import discovery.dht.dhttypes.PeerAddress;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.runner.StageControl;
import discovery.experiment.events.StageListener;
import discovery.experiments.testing.mockups.StageControlMockup;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.experiments.testing.mockups.ResultsMonitorMockup;
import discovery.experiments.testing.mockups.ExperimentRunnerMockup;
import discovery.experiments.testing.mockups.ConfigurationLoaderMockup;
import discovery.test.mockups.ConfigurableDiscoveryBuilderMockup;
import discovery.experiment.ExperimentNode;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ExperimentNodeTest implements Scenario{
    @steps
    var steps: ExperimentNodeSteps;

    @Test
    @scenario
    public function read_node_configuration_and_configure(){
        given().a_configuration_file();
        and().a_node_name();
        when().configuring_the_node();
        then().the_node_configuration_should_be_loaded();
        and().the_experiment_should_be_configured();
    }

    @Test
    @scenario
    public function run_experiment(){
        given().a_stage_control();
        and().a_results_monitor();
        given().the_node_was_configured();
        when().starting_the_execution();
        then().the_experiment_should_be_started_with_the_given_parameters();
    }

    @Test
    @scenario
    public function configure_base_output_dir(){
        given().a_configuration_file();
        when().configuring_the_node_with_an_output_dir();
        then().the_base_ouput_dir_should_be_set_on_configuration();
    }

    @Test
    @scenario
    public function configure_bootstrap_address(){
        given().a_configuration_with_this_bootstrap_address('1.2.3.4:5678');
        when().configuring_the_node();
        then().the_discovery_should_receive_the_given_bootstrap_address();
    }

    @Test
    @scenario
    public function monitor_experiment_stages(){
        given().a_stage_listener();
        given().the_node_was_configured();
        when().starting_the_execution_passing_the_stage_monitor();
        then().the_stage_monitor_should_be_passed_to_the_results_monitor();
    }
}

class ExperimentNodeSteps extends Steps<ExperimentNodeSteps>{

    var experimentNode:ExperimentNode;
    var discoveryBuilder = new ConfigurableDiscoveryBuilderMockup();
    var configLoader = new ConfigurationLoaderMockup();
    var runner = new ExperimentRunnerMockup();
    var resultsMonitor = new ResultsMonitorMockup();
    var stageListenerMockup = new NodeMonitorMockup();
    var stageControlMockup = new StageControlMockup();
    var generator = new ExperimentInfoGenerator();

    var stageListener:StageListener;
    var stageControl:StageControl;

    var configurationFile:String;
    var nodeName:String;
    var configuredOutputDir:String;

    var expectedBootstrapAddress:String;

    var usedOptions:ExperimentNodeOptions;

    public function new(){
        super();

        experimentNode = new ExperimentNode({
            discovery: discoveryBuilder.discoveryBuilder(),
            configurer: configLoader.configurer(),
            runner: runner.runner(),
            monitor: resultsMonitor.monitor()
        });
    }


    // given summary ---------------------------------------------------

    @step
    public function the_node_was_configured() {
        given().a_configuration_file();
        and().a_node_name();
        when().configuring_the_node();
    }

    // given ---------------------------------------------------

    @step
    public function a_configuration_file() {
        configurationFile = RandomGen.filepath();
    }

    @step
    public function
        a_configuration_with_this_bootstrap_address(bootstrappAddr:String)
    {
        this.expectedBootstrapAddress = bootstrappAddr;

        var config = generator.nodeConfiguration();
        config.bootstrapAddress = bootstrappAddr;

        configLoader.setupConfigToBeLoaded(config);
    }

    @step
    public function a_node_name() {
        nodeName = 'node1';
    }

    @step
    public function a_stage_control() {
        stageControl = stageControlMockup.stageControl();
    }

    @step
    public function a_results_monitor() {}

    @step
    public function a_stage_listener() {
        stageListener = stageListenerMockup.monitor();
    }

    // when  ---------------------------------------------------

    @step
    public function configuring_the_node_with_an_output_dir() {
        configuredOutputDir = RandomGen.primitives.name();

        when().configuring_the_node_with({
            configFile: configurationFile,
            nodeName: nodeName,
            experimentDir: RandomGen.dirpath(),
            outputDir: configuredOutputDir
        });
    }

    @step
    public function configuring_the_node() {
        when().configuring_the_node_with(null);
    }

    @step
    public function configuring_the_node_with(options:ExperimentNodeOptions) {
        if(options == null){
            options = {
                configFile: configurationFile,
                nodeName: nodeName,
                experimentDir: RandomGen.dirpath()
            };
        }

        this.usedOptions = options;

        experimentNode.configure(options);
    }

    @step
    public function starting_the_execution_passing_the_stage_monitor() {
        when().starting_the_execution();
    }

    @step
    public function starting_the_execution() {
        experimentNode.start(stageControl, stageListener);
    }

    // then  ---------------------------------------------------

    @step
    public function the_node_configuration_should_be_loaded() {
        configLoader.verifyConfigurationLoaded(configurationFile, nodeName);
    }

    @step
    public function the_experiment_should_be_configured() {
        the_experiment_runner_should_be_configured();
        and().the_results_monitor_should_be_configured();
    }

    @step
    function the_experiment_runner_should_be_configured() {
        runner.verifyConfigured();
    }

    @step
    public function the_base_ouput_dir_should_be_set_on_configuration() {
        var config:NodeConfiguration = runner.lastConfiguration();

        config.baseOutputDir.shouldBeEqualsTo(configuredOutputDir);
    }

    @step
    public function the_discovery_should_receive_the_given_bootstrap_address() {
        var conf = discoveryBuilder.lastConfiguration();

        var expectedAddr = PeerAddress.fromString(expectedBootstrapAddress);

        conf.dht.shouldNotBeNull('no dht configuration');
        conf.dht.bootstrap[0].shouldBeEqualsTo(expectedAddr,
            'Mismatch in bootstrap address');
    }

    @step
	public function the_configuration_keysdir_should_be_an_absolute_path() {
        var conf = discoveryBuilder.lastConfiguration();

        Path.isAbsolute(conf.keys.keysdir).shouldBe(true,
                'Configured keysdir: ${conf.keys.keysdir} should be an absolute path'
            );
    }

    @step
    function the_results_monitor_should_be_configured() {
        resultsMonitor.shouldHaveBeenConfigured();
    }

    @step
    public function
        the_experiment_should_be_started_with_the_given_parameters()
    {
        runner.shouldHaveBeenStartedWith(stageControl, resultsMonitor.monitor());
    }

    @step
    public function the_stage_monitor_should_be_passed_to_the_results_monitor() {
        resultsMonitor.stageListenerShouldBe(stageListenerMockup.monitor());
    }
}