package discovery.experiments.testing.tests.mechanisms;

import discovery.utils.comparator.GenericComparator;
import discovery.experiment.mechanisms.ExperimentsDiscoveryConfiguration;
import discovery.test.bundle.mockups.BundleDependenciesMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;
using discovery.utils.IteratorTools;


class ExperimentsSDConfigurationTest implements Scenario{
    @steps
    var steps: ExperimentsSDConfigurationSteps;

    var bundleDependencies:BundleDependenciesMockup;

    var expSdConfiguration:ExperimentsDiscoveryConfiguration;

    @Before
    public function setup() {
        bundleDependencies = new BundleDependenciesMockup();

        expSdConfiguration = new ExperimentsDiscoveryConfiguration(
            bundleDependencies.dependencies()
        );
    }

    @Test
    @scenario
    public function list_mechanisms(){
        var mechanismNames = [
            for(namedBuilder in expSdConfiguration.listMechanismBuilders())
                namedBuilder.name
        ];

        mechanismNames.shouldContainItems(['mdns', 'dht', 'mdns-raw']);
    }

    @Test
    @scenario
    public function default_selected_mechanisms(){
        var selected = expSdConfiguration.listSelectedDiscoveryMechanisms()
                            .toArray();

        selected.sort(GenericComparator.compare);

        selected.shouldBeEqualsTo(['dht', 'mdns']);
    }
}

class ExperimentsSDConfigurationSteps extends Steps<ExperimentsSDConfigurationSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}