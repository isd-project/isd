package discovery.experiments.testing.tests.runner;

import mockatoo.Mockatoo.VerificationMode;
import discovery.experiment.runner.SearchSpec;
import discovery.experiments.testing.mockups.StageControlMockup;
import discovery.experiment.ExperimentRunner;
import discovery.test.helpers.RandomGen;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.ExperimentRunner.ServicePublication;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.test.common.mockups.utils.TaskSchedulerMockup;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import hxgiven.Steps;
import hxgiven.Scenario;


class ExperimentRunnerTest implements Scenario{
    @steps
    var steps: ExperimentRunnerSteps;

    @Test
    @scenario
    public function notify_publication_done(){
        given().some_services_to_publish();
        given().a_node_monitor();
        when().all_services_complete_publication();
        then().the_publication_done_should_be_notified();
    }

    @Test
    @scenario
    public function with_no_publication__should_notify_publication_done(){
        given().no_services_to_publish();
        given().a_node_monitor();
        when().starting_the_execution();
        then().the_publication_done_should_be_notified();
    }

    @Test
    @scenario
    public function wait_for_next_stage__after_publication(){
        given().a_stage_control();
        given().some_services_to_publish();
        when().all_services_complete_publication();
        then().the_runner_should_wait_for_the_next_stage();
        and().should_not_notify_experiment_finish_yet();
    }

    @Test
    @scenario
    public function start_searches__when_new_stage_begins(){
        given().some_searches_to_execute();
        given().the_publication_stages_has_completed();
        when().the_next_stage_starts();
        then().the_searches_should_be_scheduled();
        and().should_not_notify_experiment_finish_yet();
    }

    @Test
    @scenario
    public function should_start_search_only_once(){
        given().some_searches_to_execute();
        given().the_publication_stages_has_completed();
        when().the_next_stage_starts_is_called_twice();
        then().the_searches_should_be_scheduled(times(1));
    }

    @Test
    @scenario
    public function notify_searches_end__when_alls_searches_finishes(){
        given().some_searches_were_started();
        when().all_searches_are_completed();
        then().the_searches_end_should_be_notified();
    }

    @Test
    @scenario
    public function notify_finish_execution(){
        given().some_services_to_publish();
        given().some_searches_to_execute();
        when().all_experiment_stages_completes();
        then().the_experiment_end_should_be_notified();
    }
}

class ExperimentRunnerSteps extends Steps<ExperimentRunnerSteps>{

    var experimentRunner:ExperimentRunner;

    var discoveryMockup = new DiscoveryMockup();
    var schedulerMockup = new TaskSchedulerMockup();
    var monitorMockup = new NodeMonitorMockup();
    var stageControlMockup = new StageControlMockup();
    var generator = new ExperimentInfoGenerator();

    var toPublish:Array<ServicePublication> = [];
    var searches:Array<SearchSpec> = [];

    public function new(){
        super();

        experimentRunner = new ExperimentRunner(
            discoveryMockup.discovery(),
            schedulerMockup.scheduler()
        );
    }


    // summary ---------------------------------------------------

    @step
    public function all_experiment_stages_completes() {
        when().all_services_complete_publication();
        when().the_next_stage_starts();
        and().all_searches_are_completed();
    }

    @step
    public function the_publication_stages_has_completed() {
        given().some_services_to_publish();
        when().all_services_complete_publication();
    }

    @step
    public function some_searches_were_started() {
        given().some_searches_to_execute();
        given().the_publication_stages_has_completed();
        when().the_next_stage_starts();
    }

    // given ---------------------------------------------------

    @step
    public function some_services_to_publish() {
        toPublish = generator.servicePublications();
    }

    @step
    public function no_services_to_publish() {
        toPublish = [];
    }

    @step
    public function some_searches_to_execute() {
        searches = generator.searchSpecifications();
    }

    @step
    public function a_node_monitor() {}

    @step
    public function a_stage_control() {}

    // when  ---------------------------------------------------

    @step
    public function all_services_complete_publication() {
        when().starting_the_execution();

        schedulerMockup.completeAllScheduledTasks();
        discoveryMockup.notifyPublications(
            toPublish.map((_)->RandomGen.publishInfo()));
    }

    @step
    public function all_searches_are_completed() {
        schedulerMockup.completeAllScheduledTasks();
        discoveryMockup.notifyAllsSearchesFinished();
    }

    @step
    public function starting_the_execution() {
        experimentRunner.configure({
            publications: toPublish,
            searches: searches
        });
        experimentRunner.start(
            stageControlMockup.stageControl(),
            monitorMockup.monitor());
    }

    @step
	public function the_next_stage_starts_is_called_twice() {
        when().the_next_stage_starts();
        when().the_next_stage_starts();
    }

    @step
    public function the_next_stage_starts() {
        stageControlMockup.releaseNextStage();
    }

    // then  ---------------------------------------------------

    @step
    public function the_publication_done_should_be_notified() {
        monitorMockup.verifyPublicationsCompleted();
    }

    @step
    public function the_runner_should_wait_for_the_next_stage() {
        stageControlMockup.verifyWaitForNextStageWasCalled();
    }

    @step
    public function should_not_notify_experiment_finish_yet() {
        monitorMockup.verifyFinishNotCalledYet();
    }

    @step
    public function the_experiment_end_should_be_notified() {
        monitorMockup.verifyFinishedCalled();
    }

    @step
    public function the_searches_should_be_scheduled(?mode:VerificationMode) {
        for(s in searches){
            schedulerMockup.verifyScheduledTaskWith(s.delay, mode);
        }
    }

    @step
    public function the_searches_end_should_be_notified() {
        monitorMockup.verifySearchesFinishedCalled();
    }
}