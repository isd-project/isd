package discovery.experiments.testing.tests.runner;

import discovery.test.helpers.RandomGen;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.utils.time.Duration;
import discovery.base.search.SearchOptions;
import discovery.experiment.runner.SearchSpec;
import hxgiven.Scenario;


class SearchStageConfigTest implements Scenario{
    @steps
    var steps: SearchStageConfigSteps;


    @Test
    @scenario
    public function specify_search_timeout_on_configuration(){
        test_specify_timeout_on_configuration(Search);
    }
    @Test
    @scenario
    public function specify_locate_timeout_on_configuration(){
        test_specify_timeout_on_configuration(Locate);
    }

    @Test
    @scenario
    public function ensure_default_search_timeout(){
        test_default_timeout(Search);
    }
    @Test
    @scenario
    public function ensure_default_locate_timeout(){
        test_default_timeout(Locate);
    }

    function test_specify_timeout_on_configuration(searchKind:SearchKind) {
        given().the_node_configuration_has_an_specified_timeout();
        when().some_search_is_executed(searchKind);
        then().it_should_executes_with_the_given_timeout();
    }

    function test_default_timeout(searchKind: SearchKind){
        given().no_timeout_was_specified();
        when().some_search_is_executed(searchKind);
        then().it_should_executes_with_a_valid_timeout();
    }
}

class SearchStageConfigSteps
    extends SearchStageBaseSteps<SearchStageConfigSteps>
{
    var nodeConfig:NodeConfiguration;

    // summary ---------------------------------------------------

    // given ---------------------------------------------------

    @step
    public function no_timeout_was_specified() {}

    @step
    public function the_node_configuration_has_an_specified_timeout() {
        nodeConfig = generator.nodeConfiguration();
        nodeConfig.searches = this.searchSpecs;

        nodeConfig.searchTimeout = RandomGen.time.duration();

        searchStage.changeConfiguration(nodeConfig);
    }

    @step
    public function no_search_were_specified() {}

    // when  ---------------------------------------------------


    // then  ---------------------------------------------------


    @step
    public function it_should_executes_with_the_given_timeout() {
        then().it_should_executes_with_a_timeout(nodeConfig.searchTimeout);
    }

    @step
    public function it_should_executes_with_a_valid_timeout() {
        then().it_should_executes_with_a_timeout();
    }

    @step
    function it_should_executes_with_a_timeout(?timeout:Duration) {
        function hasTimeout(options:SearchOptions){
            if(options == null
                || options.timeout == null
                || options.timeout.time() <= 0)
            {
                return false;
            }

            return timeout == null //default
                || options.timeout == timeout;
        }

        var searchSpec = searchSpecs[0];

        if(searchSpec.isSearch()){
            discovery.verifySearchMatching(null, hasTimeout);
        }
        else{
            discovery.verifyLocateMatching(null, hasTimeout);
        }
    }
}