package discovery.experiments.testing.mockups;

import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.test.matchers.CommonMatchers;
import discovery.experiment.events.StageListener;
import discovery.experiment.results.ResultsMonitor;
import mockatoo.Mockatoo;

class ResultsMonitorMockup {
    var expMonitor = Mockatoo.mock(ResultsMonitor);

    var nodeConfig:NodeConfiguration;


    public function new()
    {
        Mockatoo.when(expMonitor.configure(any)).thenCall((args)->{
            nodeConfig = args[0];
        });
    }

    public function monitor():Null<ResultsMonitor> {
        return expMonitor;
    }

    public function configuration() {
        return nodeConfig;
    }

    public function shouldHaveBeenConfigured() {
        Mockatoo.verify(expMonitor.configure(any));
    }

    public function stageListenerShouldBe(expected:StageListener) {
        CommonMatchers.shouldBeEqualsTo(expMonitor.stageListener, expected);
    }
}