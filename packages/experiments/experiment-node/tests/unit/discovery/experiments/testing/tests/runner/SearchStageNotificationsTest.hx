package discovery.experiments.testing.tests.runner;

import haxe.Exception;
import discovery.test.helpers.RandomGen;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.utils.time.Duration;
import discovery.base.search.SearchOptions;
import hxgiven.Scenario;


class SearchStageNotificationsTest implements Scenario{
    @steps
    var steps: SearchStageNotificationsSteps;

    @Test
    @scenario
    public function notify_search_failure__if_an_error_occur(){
        given().some_search_is_executed();
        when().the_search_ends_with_a_failure();
        then().the_search_finish_should_be_notified_with_error();
    }
}

class SearchStageNotificationsSteps
    extends SearchStageBaseSteps<SearchStageNotificationsSteps>
{

    var searchFailure:Exception;

    // summary ---------------------------------------------------

    // given ---------------------------------------------------


    // when  ---------------------------------------------------

    @step
    public function the_search_ends_with_a_failure() {
        searchFailure = new Exception('search failed');
        discovery.lastSearchMockup().finishSearch(searchFailure);
    }


    // then  ---------------------------------------------------

    @step
    public function the_search_finish_should_be_notified_with_error() {
        nodeMonitor.verifySearchFinishedWhere((evt)->{
            return evt.error == searchFailure;
        });
    }

}