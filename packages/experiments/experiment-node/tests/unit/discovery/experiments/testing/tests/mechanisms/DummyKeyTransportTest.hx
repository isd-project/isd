package discovery.experiments.testing.tests.mechanisms;

import haxe.Exception;
import discovery.keys.storage.PublicKeyData;
import mockatoo.Mockatoo;
import discovery.keys.Key;
import discovery.keys.storage.KeyData;
import discovery.test.helpers.RandomGen;
import discovery.async.promise.PromisableFactory;
import discovery.experiment.mechanisms.DummyKeyTransport;
import discovery.test.fakes.FakePromisableFactory;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class DummyKeyTransportTest implements Scenario{
    @steps
    var steps: DummyKeyTransportSteps;

    var keyMechanism:DummyKeyTransport;

    @Before
    public function setup() {
        keyMechanism = new DummyKeyTransport();
    }

    @Test
    @scenario
    public function search_for_key(){
        var anyKeyId = RandomGen.crypto.fingerprint();

        var onKeyCalled= false, onEndCalled=false;
        var error:Exception = null;

        var stoppable = keyMechanism.searchKey(anyKeyId, {
            onKey: (key)->{
                onKeyCalled = true;
            },
            onEnd: (?err)->{
                error = err;
                onEndCalled = true;
            }
        });

        stoppable.shouldNotBeNull(
            'Should have returned a non null "stoppable"');
        onKeyCalled.shouldBe(false, 'Should not receive any key');
        onEndCalled.shouldBe(true, 'Should finish the key search');
        error.shouldBe(null, 'No error expected');
    }

    @Test
    @scenario
    public function publish_key(){
        var publicKeyData = RandomGen.crypto.publicKeyData();
        var published=false;

        keyMechanism.publishKey(publicKeyData, (?err)->{
            published = true;
        });

        published.shouldBe(true, 'should notify success to publish key');
    }
}

class DummyKeyTransportSteps extends Steps<DummyKeyTransportSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}