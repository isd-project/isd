package discovery.experiments.testing.scenarios;

import discovery.experiments.testing.tests.runner.SearchStageBaseSteps;
import discovery.utils.time.TimeInterval;
import discovery.utils.time.TimeCount;
import discovery.test.helpers.RandomGen;
import discovery.domain.Discovered;
import discovery.experiment.runner.SearchSpec;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class MeasureSearchEventsTest implements Scenario{
    @steps
    var steps: MeasureSearchEventsSteps;

    @Test
    @scenario
    public function notify_service_found_event__on_search(){
        test_notify_service_found_event(Search);
    }

    @Test
    @scenario
    public function notify_service_found_event__on_locate(){
        test_notify_service_found_event(Locate);
    }

    function test_notify_service_found_event(kind: SearchKind){
        given().a_node_monitor();
        given().a_search_of_this_kind_was_executed(kind);
        when().a_service_is_found();
        then().the_monitor_should_receive_a_valid_discovered_event();
    }

    @Test
    @scenario
    public function should_measure_search_time_from_start(){
        test_discovery_time_measure(Search);
    }

    @Test
    @scenario
    public function should_measure_locate_time_from_start(){
        test_discovery_time_measure(Locate);
    }


    function test_discovery_time_measure(kind: SearchKind){
        given().a_service_was_found_for_a_search_of_this_kind(kind);
        then().the_discovery_time_should_be_measured();
        then().the_discovery_interval_should_be_between_start_and_discovery();
    }
}


class MeasureSearchEventsSteps
    extends SearchStageBaseSteps<MeasureSearchEventsSteps>
{
    var discovered:Discovered;
    var searchTime:TimeCount;

    var timeBeforeDiscovery:TimeInterval;
    var timeUntilDiscover:TimeInterval;

    // summary ---------------------------------------------------

    @step
	public function
        a_service_was_found_for_a_search_of_this_kind(kind:SearchKind)
    {
        given().a_node_monitor();
        given().a_search_of_this_kind_was_executed(kind);
        when().a_service_is_found();
    }

    @step
	public function a_search_of_this_kind_was_executed(kind:SearchKind) {
        given().a_search_of_this_kind_were_started(kind);
        given().all_tasks_have_been_scheduled();
    }

    // given ---------------------------------------------------

    // when ---------------------------------------------------

    override public function searching_them_all() {
        searchTime = new TimeCount();

        super.searching_them_all();
    }

    @step
    public function a_service_is_found() {
        discovered = RandomGen.discovered();

        timeBeforeDiscovery = searchTime.measure();

        discovery.lastSearchMockup().notifyDiscovered(discovered);

        timeUntilDiscover = searchTime.measure();
    }

    // then ---------------------------------------------------

    @step
    public function the_monitor_should_receive_a_valid_discovered_event() {
        var evt = nodeMonitor.lastDiscoveredEvent();

        evt.shouldNotBeNull('no discovered event received');

        evt.discovered.shouldBeEqualsTo(discovered,
            'Discovered service does not match the expected');

        var expectedSearch = this.searchSpecs[0];
        evt.searchSpec.shouldBeEqualsTo(expectedSearch,
            'Search spec in event is not the expected');
    }

    @step
	public function the_discovery_time_should_be_measured() {
        var evt = nodeMonitor.lastDiscoveredEvent();

        evt.interval.shouldNotBeNull('no discovery time measurement');
    }

    @step
    public function
        the_discovery_interval_should_be_between_start_and_discovery()
    {
        var evt = nodeMonitor.lastDiscoveredEvent();

        var evtTime = evt.interval;

        evtTime.start.shouldBeBetween(
            timeBeforeDiscovery.start,
            timeBeforeDiscovery.end,
            'Discovered start time is not in the expected interval'
        );
        evtTime.end.shouldBeBetween(
            timeBeforeDiscovery.end,
            timeUntilDiscover.end,
            'Discovered end time is not in the expected interval'
        );

    }
}