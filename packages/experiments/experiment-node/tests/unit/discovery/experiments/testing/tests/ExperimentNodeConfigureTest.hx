package discovery.experiments.testing.tests;

import haxe.io.Path;
import discovery.base.configuration.DiscoveryBuilder;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import discovery.experiment.runner.StageControl;
import discovery.experiment.events.StageListener;
import discovery.experiments.testing.mockups.StageControlMockup;
import discovery.experiments.testing.mockups.NodeMonitorMockup;
import discovery.experiments.testing.mockups.ResultsMonitorMockup;
import discovery.experiments.testing.mockups.ExperimentRunnerMockup;
import discovery.experiments.testing.mockups.ConfigurationLoaderMockup;
import discovery.test.mockups.ConfigurableDiscoveryBuilderMockup;
import discovery.experiment.ExperimentNode;
import discovery.test.helpers.RandomGen;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ExperimentNodeConfigureTest implements Scenario{
    @steps
    var steps: ExperimentNodeConfigureSteps;

    @Test
    @scenario
    public function should_not_build_discovery_before_configuration(){
        given().an_experiment_node_without_an_initial_runner();
        then().the_discovery_should_not_have_been_build_yet();
    }

    @Test
    @scenario
    public function should_build_discovery_only_after_configuration(){
        given().an_experiment_node_without_an_initial_runner();
        when().configuring_the_node();
        then().the_discovery_should_be_built();
    }


    @Test
    @scenario
    public function configure_discovery_keysdir(){
        given().a_node_configuration_with_the_keysdir();
        when().configuring_the_node();
        then().the_discovery_should_be_configured_with_the_keysdir();
    }

    @Test
    @scenario
    public function resolve_keysdir_based_on_experiment_dir(){
        final keysdir = 'relative/path';
        final expDir = '/experiment/dir';

        given().a_node_configuration_with_this_keysdir(keysdir);
        given().this_experiment_dir(expDir);
        when().configuring_the_node();
        then().the_discovery_should_be_configured_with_this_keysdir(
            Path.join([expDir, keysdir])
        );
    }

    @Test
    @scenario
    public function configure_keys_cache_to_output_dir(){
        given().an_output_dir_is_set();
        when().configuring_the_node();
        then().the_cache_keysdir_should_be_under_the_output_dir();
    }

    @Test
    @scenario
    public function configure_mechanisms(){
        var mechanisms = ['a', 'b', 'c'];

        given().a_default_node_configuration();
        when().configuring_the_node_with_these_mechanisms(mechanisms);
        then().the_discovery_mechanisms_should_be_configured_to(mechanisms);
        and().the_key_mechanisms_should_be_configured_to(mechanisms);
    }

    @Test
    @scenario
    public function configure_mechanisms_should_be_passed_to_the_node_monitor(){
        var mechanisms = ['a', 'b', 'c'];

        given().a_default_node_configuration();
        when().configuring_the_node_with_these_mechanisms(mechanisms);
        then().the_node_monitor_should_be_configured_with_these_mechanisms(
            mechanisms
        );
    }
}

class ExperimentNodeConfigureSteps extends Steps<ExperimentNodeConfigureSteps>{

    var experimentNode:ExperimentNode;
    var discoveryBuilder = new ConfigurableDiscoveryBuilderMockup();
    var configLoader = new ConfigurationLoaderMockup();
    var runner = new ExperimentRunnerMockup();
    var resultsMonitor = new ResultsMonitorMockup();
    var stageListenerMockup = new NodeMonitorMockup();
    var stageControlMockup = new StageControlMockup();
    var generator = new ExperimentInfoGenerator();

    var stageListener:StageListener;
    var stageControl:StageControl;

    var configurationFile:String;
    var nodeName:String;
    var experimentDir:String;
    var outputDir:String;
    var nodeConfig:NodeConfiguration;


    public function new(){
        super();

        this.experimentDir = RandomGen.dirpath();

        experimentNode = new ExperimentNode({
            discovery: discoveryBuilder.discoveryBuilder(),
            configurer: configLoader.configurer(),
            runner: runner.runner(),
            monitor: resultsMonitor.monitor()
        });
    }


    // given summary ---------------------------------------------------

    @step
    public function the_node_was_configured() {
        given().a_configuration_file();
        and().a_node_name();
        when().configuring_the_node();
    }

    // given ---------------------------------------------------

    @step
    public function an_experiment_node_without_an_initial_runner() {
        experimentNode = new ExperimentNode({
            discovery: discoveryBuilder.discoveryBuilder(),
            configurer: configLoader.configurer(),
            runner: null,
            monitor: resultsMonitor.monitor()
        });
    }

    @step
    public function a_default_node_configuration() {
        given().this_configuration_will_be_loaded({
            publications: []
        });
    }

    @step
    public function a_node_configuration_with_the_keysdir() {
        given().a_node_configuration_with_this_keysdir(RandomGen.dirpath());
    }

    @step
    public function a_node_configuration_with_this_keysdir(keysdir:String) {
        var nodeConfig = given().a_node_configuration_to_be_loaded();

        nodeConfig.keysdir = keysdir;
    }

    @step
    public function
        a_node_configuration_with_these_mechanisms(mechanisms:Array<String>)
    {
        var nodeConfig = given().a_node_configuration_to_be_loaded();

        nodeConfig.mechanisms = mechanisms;
    }

    function a_node_configuration_to_be_loaded() {
        var config = generator.nodeConfiguration();

        return given().this_configuration_will_be_loaded(config);
    }

    function this_configuration_will_be_loaded(config: NodeConfiguration) {
        given().a_configuration_file();

        this.nodeConfig = config;
        configLoader.setupConfigToBeLoaded(this.nodeConfig);

        return this.nodeConfig;
    }

    @step
    public function a_configuration_file() {
        configurationFile = RandomGen.filepath();
    }

    @step
    public function this_experiment_dir(expDir:String) {
        this.experimentDir = expDir;
    }

    @step
    public function an_output_dir_is_set() {
        outputDir = RandomGen.dirpath();
    }

    @step
    public function a_node_name() {
        nodeName = 'node1';
    }

    @step
    public function a_stage_control() {
        stageControl = stageControlMockup.stageControl();
    }

    @step
    public function a_results_monitor() {}

    @step
    public function a_stage_listener() {
        stageListener = stageListenerMockup.monitor();
    }

    // when  ---------------------------------------------------

    @step
    public function configuring_the_node() {
        when().configuring_the_node_with_these_mechanisms();
    }

    @step
    public function
        configuring_the_node_with_these_mechanisms(?mechanisms:Array<String>)
    {
        experimentNode.configure({
            configFile: configurationFile,
            nodeName: nodeName,
            experimentDir: experimentDir,
            outputDir: outputDir,
            mechanisms: mechanisms
        });
    }

    @step
    public function starting_the_execution_passing_the_stage_monitor() {
        when().starting_the_execution();
    }

    @step
    public function starting_the_execution() {
        experimentNode.start(stageControl, stageListener);
    }

    // then  ---------------------------------------------------

    @step
    public function the_node_configuration_should_be_loaded() {
        configLoader.verifyConfigurationLoaded(configurationFile, nodeName);
    }

    @step
    public function the_discovery_should_not_have_been_build_yet() {
        discoveryBuilder.verifyNeverBuilt();
    }

    @step
    public function the_discovery_should_be_built() {
        discoveryBuilder.verifyBuilt();
    }


    @step
    public function the_discovery_should_be_configured_with_the_keysdir() {
        then().the_discovery_should_be_configured_with_this_keysdir(
            nodeConfig.keysdir);
    }

    @step
    public function
        the_discovery_should_be_configured_with_this_keysdir(expected:String)
    {
        discoveryBuilder.verifyConfigured();

        var config = discoveryBuilder.lastConfiguration();
        config.keys.keysdir.shouldBeEqualsTo(expected,
            'configured keysdir has not the expected value'
        );
    }

    @step
    public function the_discovery_mechanisms_should_be_configured_to(
        mechanisms:Array<String>
    )
    {
        var config = discoveryBuilder.lastConfiguration();
        config.mechanisms.shouldBeEqualsTo(mechanisms,
            'configured mechanisms has not the expected value'
        );
    }

    @step
    public function
        the_key_mechanisms_should_be_configured_to(mechanisms:Array<String>)
    {
        var config = discoveryBuilder.lastConfiguration();
        config.pkis.shouldBeEqualsTo(mechanisms,
            'configured key mechanisms has not the expected value'
        );
    }

    @step
    public function the_node_monitor_should_be_configured_with_these_mechanisms(
        mechanisms:Array<String>)
    {
        var config = resultsMonitor.configuration();

        config.mechanisms.shouldBeEqualsTo(mechanisms);
    }

    @step
    public function the_cache_keysdir_should_be_under_the_output_dir() {
        var conf = discoveryBuilder.lastConfiguration();

        conf.keysCache.shouldNotBeNull('No keyCache configuration');
        conf.keysCache.keysdir.shouldStartWith(outputDir);
    }
}