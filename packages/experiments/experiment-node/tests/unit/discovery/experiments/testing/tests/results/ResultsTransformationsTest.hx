package discovery.experiments.testing.tests.results;

import discovery.experiment.results.PublicationResult;
import discovery.test.helpers.RandomGen;
import discovery.utils.time.TimeInterval;
import discovery.experiment.events.PublicationEvent;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.experiment.results.ResultsTransformations;
using discovery.test.matchers.CommonMatchers;


class ResultsTransformationsTest implements Scenario{
    @steps
    var steps: ResultsTransformationsSteps;

    @Test
    @scenario
    public function publication_event_to_result(){
        var interval = RandomGen.time.interval();
        var srv = RandomGen.description();

        when().transforming_this_publication_event({
            interval: interval,
            service: srv
        });
        then().it_should_produce_this_result({
            service: srv.identifier(),
            interval: interval
        });
    }
}

class ResultsTransformationsSteps extends Steps<ResultsTransformationsSteps>{

    var pubResult:PublicationResult;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    @step
    public function transforming_this_publication_event(evt:PublicationEvent)
    {
        pubResult = evt.toPublicationResult();
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_produce_this_result(expected:PublicationResult)
    {
        pubResult.shouldBeEqualsTo(expected);
    }
}