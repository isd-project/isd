from commands import Commands

from .experiment_paths import experiment_node_path
from experiment_testing.helpers import ExperimentDirectories

import pytest
from os import path

import testdata
from experiment_testing import data

class ExperimentNodeTests(object):
    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.tmp_path = tmp_path
        self.dirs = ExperimentDirectories(tmp_path)

    def setup_method(self):
        self.cmds = Commands()
        self.cmd_map = {}

    def teardown_method(self):
        self.cmds.close_all()

    def test_results_publication(self):
        results_file = 'n1_results.yaml'

        self.given_node_configuration(results_file, '')
        self.when_executing_node('n0')
        self.then_results_file_should_be_created(self.expected_results_n0)

    def test_execute_commands_from_two_nodes(self):
        self.given_node_configuration('n0.results', 'n1.results')
        self.starting_node_command('n0')
        self.starting_node_command('n1')
        self.wait_publication(timeout=3)
        self.start_next_stage()
        self.wait_finish(timeout=5)
        self.then_results_file_should_be_created(self.expected_results_n0)
        self.then_results_file_should_be_created(self.expected_results_n1)

    # ##############################################

    def experiment_dir(self):
        return str(self.dirs.out_path)

    def given_node_configuration(self, n0results_file, n1results_file):
        keysdir = data.testkeysdir
        key0 = data.get_keyid(0)
        key1 = data.get_keyid(1)
        output_dir = self.experiment_dir()

        config_path = self.dirs.save_experiment_config(f'''
output_dir: '{output_dir}'
nodes:
    n0:
        keyid: {key0}
        keysdir: '{keysdir}'
        results_file: '{n0results_file}'
        publications:
            - name: s1_n0
              type: type1
              delay: 0
              port: 12345
    n1:
        keyid: {key1}
        keysdir: '{keysdir}'
        results_file: '{n1results_file}'
        publications:
            - name: s1_n1
              type: type2
              delay: 25
              port: 1010
''')

        self.node_config_path = config_path
        self.expected_results_n0 = path.join(self.dirs.out_path, n0results_file)
        self.expected_results_n1 = path.join(self.dirs.out_path, n1results_file)

    def given_configuration_file(self):
        self.node_config_path = testdata.datapaths.single_network_exp_file()
        exp_dir = path.dirname(self.node_config_path)

        self.expected_results_base = path.join(
                self.dirs.out_path, 'replication0', 'output'
            )
        self.expected_results_n0 = path.join(
            self.expected_results_base, 'n0', 'n0.results')
        self.expected_results_n1 = path.join(
            self.expected_results_base, 'n1', 'n1.results')

    def when_executing_node(self, node, timeout=2):
        cmd = self.starting_node_command(node)

        self.wait_publication(timeout=timeout)
        self.start_next_stage()
        self.wait_finish(timeout=timeout)

    def starting_node_command(self, node):
        cmd = self.start_experiment_node(
                self.node_config_path, node, output_dir=self.dirs.out_path
            )

        self.cmd_map[node] = cmd

        return cmd

    def start_experiment_node(self, config_path, node_name, output_dir=None):
        exp_node = experiment_node_path()

        args = ['nodejs', exp_node, str(config_path), node_name, self.experiment_dir()]

        if output_dir:
            args.extend(('-o', str(output_dir)))

        return self.cmds.starting_command(*args, proc=node_name)

    def wait_publication(self, timeout=2):
        for node, cmd in self.cmd_map.items():
            cmd.wait_for_message('publication: done', timeout=timeout)

    def start_next_stage(self):
        for node, cmd in self.cmd_map.items():
            cmd.sendline('')

    def wait_finish(self, timeout=2):
        for node, cmd in self.cmd_map.items():
            cmd.wait_for_message('finished execution', timeout=timeout)

    def then_results_file_should_be_created(self, results_file):
        results_path = self.dirs.out_path / results_file
        assert path.exists(str(results_path)), \
                'File "{0}" should exist'.format(str(results_path))


