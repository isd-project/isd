from testdata import projectpaths

def experiment_node_path():
    return projectpaths.project_path('packages',
        'experiments', 'experiment-node', 'build', 'experiment-node.js')