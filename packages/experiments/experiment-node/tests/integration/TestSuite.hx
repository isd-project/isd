import massive.munit.TestSuite;

import discovering.experiments.integrationtests.tests.ConfigurationReaderTest;
import discovering.experiments.integrationtests.tests.mechanisms.MdnsRawMechanismsIntegrationTest;
import discovering.experiments.integrationtests.tests.ResultsPublisherIntegrationTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovering.experiments.integrationtests.tests.ConfigurationReaderTest);
		add(discovering.experiments.integrationtests.tests.mechanisms.MdnsRawMechanismsIntegrationTest);
		add(discovering.experiments.integrationtests.tests.ResultsPublisherIntegrationTest);
	}
}
