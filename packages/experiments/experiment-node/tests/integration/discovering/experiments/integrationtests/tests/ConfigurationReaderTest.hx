package discovering.experiments.integrationtests.tests;

import discovery.experiment.parsing.ConfigurationReader;
import sys.io.File;
import discovery.test.integration.tools.TemporaryDir;
import discovery.experiment.parsing.ConfigurationModel;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ConfigurationReaderTest implements Scenario{
    @steps
    var steps: ConfigurationReaderSteps;

    @After
    public function teardown() {
        steps.teardown();
    }

    @Test
    @scenario
    public function parse_yaml_file(){
        given().yaml_file('
nodes:
    n1:
        keyid: c4bb7f65cb34
        keysdir: /example/path/to/keys
        publications:
            - name: s1_n1
              type: type1
              port: 9876
              delay: 15
        searches:
            - kind: search
              srvType: type1
              delay: 0
            - kind: locate
              srvId: srv://...
              delay: 200
');
        when().parsing_yaml_file();
        then().this_object_should_be_produced({
            nodes: {
                n1: {
                    keyid: 'c4bb7f65cb34',
                    keysdir: '/example/path/to/keys',
                    publications:[{
                        name: 's1_n1',
                        type: 'type1',
                        port: 9876,
                        delay: 15
                    }],
                    searches: [
                        {
                            kind: 'search',
                            srvType: 'type1',
                            delay: 0
                        },
                        {
                            kind: 'locate',
                            srvId: 'srv://...',
                            delay: 200
                        }
                    ]
                }
            }
        });
    }
}

class ConfigurationReaderSteps extends Steps<ConfigurationReaderSteps>{
    var reader:ConfigurationReader;

    var tempDir:TemporaryDir;

    var configFilePath:String;
    var readedConfig:ConfigurationModel;

    public function new(){
        super();

        reader = new ConfigurationReader();
        tempDir = new TemporaryDir();
    }

    public function teardown() {
        tempDir.removeDir();
    }

    // given ---------------------------------------------------

    @step
    public function yaml_file(content:String) {
        configFilePath = tempDir.subpath() + '.yaml';

        File.saveContent(configFilePath, content);
    }

    // when  ---------------------------------------------------

    @step
    public function parsing_yaml_file() {
        readedConfig = reader.readConfiguration(configFilePath);
    }

    // then  ---------------------------------------------------

    @step
    public function this_object_should_be_produced(config: ConfigurationModel)
    {
        readedConfig.shouldBeEqualsTo(config,
            'Readed configuration does not match the expected value');
    }
}