package discovering.experiments.integrationtests.tests;

import haxe.io.Path;
import discovery.experiment.results.ResultsPersistence;
import haxe.io.Bytes;
import discovery.utils.time.TimeInterval;
import sys.FileSystem;
import discovery.test.helpers.RandomGen;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.results.ExperimentResults;
import discovery.experiment.results.ResultsPublisher;
import discovery.test.integration.tools.TemporaryDir;
import discovery.experiments.testing.helpers.ExperimentInfoGenerator;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ResultsPublisherIntegrationTest implements Scenario{
    @steps
    var steps: ResultsPublisherIntegrationSteps;

    @After
    public function teardown() {
        steps.teardown();
    }

    @Test
    @scenario
    public function publish_result(){
        given().the_publisher_was_configured();
        given().an_experiment_result();
        when().publishing_the_results();
        then().a_file_should_be_created_with_the_results();
    }

    @Test
    @scenario
    public function create_parent_directory_if_needed(){
        given().the_output_dir_does_not_exist_yet();
        given().the_publisher_was_configured();
        given().an_experiment_result();
        when().publishing_the_results();
        then().the_output_dir_should_be_created();
    }
}

class ResultsPublisherIntegrationSteps
    extends Steps<ResultsPublisherIntegrationSteps>
{
    var resultsPublisher:ResultsPublisher;

    var generator = new ExperimentInfoGenerator();
    var tempDir:TemporaryDir;
    var outputDir:String;

    var results:ExperimentResults;

    public function new(){
        super();

        resultsPublisher = new ResultsPublisher();
        tempDir = new TemporaryDir();
        outputDir = tempDir.path();
    }

    public function teardown() {
        tempDir.removeDir();
    }

    // given ---------------------------------------------------

    @step
    public function the_output_dir_does_not_exist_yet() {
        outputDir = Path.join([tempDir.path(), 'newdir', 'subdir']);
    }

    @step
    public function the_publisher_was_configured() {
        var config:NodeConfiguration = {
            nodeName: 'node1',
            outputDir: outputDir,
            publications: []
        };

        resultsPublisher.configure(config);
    }

    @step
    public function an_experiment_result() {
        results = generator.experimentResults();
    }

    // when  ---------------------------------------------------

    @step
    public function publishing_the_results() {
        resultsPublisher.publishResults(results);
    }

    // then  ---------------------------------------------------

    @step
    public function a_file_should_be_created_with_the_results() {
        then().a_file_should_be_created_at_the_output_dir();
        and().the_file_should_have_the_serialized_results();
    }

    @step
    function a_file_should_be_created_at_the_output_dir() {
        var outPath = resultsPublisher.outputPath;
        var wasCreated = FileSystem.exists(outPath);

        wasCreated.shouldBe(true,
            'Results file "${outPath}" should have been created');
    }

    @step
    function the_file_should_have_the_serialized_results() {
        var persistence = new ResultsPersistence();
        var loadedResults = persistence.load(resultsPublisher.outputPath);

        loadedResults.shouldBeEqualsTo(results,
                'loaded results does not match the expected');

    }

    @step
    public function the_output_dir_should_be_created() {
        FileSystem.exists(this.outputDir).shouldBe(true,
            'Output dir "${this.outputDir}" should be created');
    }
}