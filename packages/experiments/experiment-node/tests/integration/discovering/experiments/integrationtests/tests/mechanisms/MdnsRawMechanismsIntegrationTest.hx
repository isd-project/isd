package discovering.experiments.integrationtests.tests.mechanisms;

import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import discovery.experiment.mechanisms.ExperimentsDiscoveryConfiguration;
import discovery.Discovery;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class MdnsRawMechanismsIntegrationTest extends AsyncTests implements Scenario{
    @steps
    var steps: MdnsRawMechanismsIntegrationSteps;

    var configuration:ExperimentsDiscoveryConfiguration;
    var client:Discovery;
    var publisher:Discovery;

    @Before
    public function setup() {
        configuration = new ExperimentsDiscoveryConfiguration();
        configuration.configure({
            mechanisms: ['mdns-raw'],
            pkis: ['mdns-raw']
        });
        client = configuration.build();
        publisher = configuration.build();
    }

    @AsyncTest
    @scenario
    public function publish_service(factory: AsyncFactory)
    {
        makeHandler(factory, 1500);

        var publication = publisher.publish({
            name: 'mysrv',
            type: 'example',
            port: 12345
        });

        publication.oncePublished.listen((evt)->{
            done();
        });

        //Maybe: include search
    }
}

class MdnsRawMechanismsIntegrationSteps extends Steps<MdnsRawMechanismsIntegrationSteps>{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}