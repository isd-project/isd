package discovery.experiment.cli;

import haxe.Exception;

interface CliHandler {
    function start(options:CliOptions):Void;
    function onError(err:Exception):Void;
}