package discovery.experiment.cli;

import discovery.experiment.ExperimentNode.ExperimentNodeOptions;

typedef CliOptions = ExperimentNodeOptions;