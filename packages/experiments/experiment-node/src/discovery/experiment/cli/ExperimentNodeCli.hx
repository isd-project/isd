package discovery.experiment.cli;

import tink.Cli;
import haxe.Exception;
import discovery.cli.tink.NonePrompt;
import tink.CoreApi;

class ExperimentNodeCli {
    public function new() {

    }

    public function parseArguments(args:Array<String>, handler:CliHandler) {
        var cli = new ExperimentNodeCliInterface(handler);

        var result = tink.Cli.process(args, cli, new NonePrompt());
        result.handle(function(outcome: Outcome<Noise, Error>){
            switch (outcome){
                case Failure(error): processError(error, handler);
                default:
            }
        });
    }

    function processError(error:Error, handler:CliHandler) {
        handler.onError(new TinkCliError(error));
    }

    public function getUsage():String {
        return new ExperimentNodeCliInterface(null).getUsage();
    }
}

class TinkCliError extends CliError{
    public var tinkError: Error;

    public function new(?error:Error, ?previous:Exception) {
        super(error.message, previous);

        this.tinkError = error;
    }

    override function toString():String {
        if(this.tinkError != null){
            return tinkError.toString();
        }

        return super.toString();
    }
}

class ExperimentNodeCliInterface {
    var cliHandler: CliHandler;

    /**
        Specify discovery and key mechanisms to be used.
    **/
    @:flag('mechanism')
    @:alias('m')
    public var mechanisms(default, default):Array<String>=null;


    /**
        Output directory, where produced files will be written.
    **/
    @:flag('output-dir')
    @:alias('o')
    public var outputdir(default, default):String=null;

    public function new(handler: CliHandler) {
        this.cliHandler = handler;
    }

    /**
        Runs an experiment node with given configuration.
        Args:
            - configFile: the path to the configuration file
            - nodeName: the name of the node in the configuration to execute
            - experimentDir: the base path where experiment files are located
    **/
    @:defaultCommand
    public function
        run(configFile:String, nodeName: String, experimentDir:String)
    {
        this.cliHandler.start(
            extractOptions(configFile, nodeName, experimentDir));
    }

    function extractOptions(
        configFile:String, nodeName: String, experimentDir:String
    ): CliOptions
    {
        return {
            configFile: configFile,
            nodeName: nodeName,
            experimentDir: experimentDir,
            mechanisms: this.mechanisms,
            outputDir: this.outputdir
        };
    }

    public function getUsage():String {
        return Cli.getDoc(this);
    }
}

