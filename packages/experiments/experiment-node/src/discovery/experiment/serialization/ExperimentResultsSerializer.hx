package discovery.experiment.serialization;

import discovery.experiment.results.ErrorResults;
import haxe.Exception;
import discovery.experiment.runner.SearchSpec;
import discovery.experiment.results.SearchResults;
import discovery.utils.time.TimeInterval;
import discovery.domain.Identifier;
import discovery.experiment.results.PublicationResult;
import discovery.utils.GenericDeserializer;
import discovery.utils.GenericSerializer;
import discovery.experiment.results.ExperimentResults;
import discovery.domain.serialization.Serializer;

using StringTools;


class ExperimentResultsSerializer implements Serializer<ExperimentResults>{
    var serializer = new GenericSerializer();

    public function new() {
    }

    public function deserialize(serialized:Dynamic):ExperimentResults {
        if(serialized == null) return null;

        return {
            usedMechanisms: asStringArray(serialized.usedMechanisms),
            publications: extractPublications(serialized.publications),
            searches: extractSearches(serialized.searches)
        };
    }

    public function serialize(value:ExperimentResults):Any {
        return serializer.serialize(value);
    }

    function extractPublications(publications:Dynamic):Array<PublicationResult> {
        return GenericDeserializer.makeArrayAs(publications, (value)->{
            return ({
                service: extractIdentifier(value.service),
                interval: extractInterval(value.interval)
            }:PublicationResult);
        });
    }

    function extractSearches(searches:Dynamic):Array<SearchResults> {
        return GenericDeserializer.makeArrayAs(searches, (value)->{
            return ({
                search: extractSearchSpec(value.search),
                found: extractFoundServices(value.found),
                finishedTime: extractInterval(value.finishedTime),
                error: extractError(value.error)
            }: SearchResults);
        });
    }

    function extractSearchSpec(value: Dynamic):SearchSpec {
        return {
            delay: value.delay,
            srvType: value.srvType,
            srvId: value.srvId,
            kind: SearchSpec.searchKindFromString(value.kind)
        };
    }

    function extractFoundServices(foundSrvs: Dynamic):Array<FoundService> {
        return GenericDeserializer.makeArrayAs(foundSrvs, (value)->{
            return ({
                id: extractIdentifier(value.id),
                searchTime: extractInterval(value.searchTime)
            }: FoundService);
        });
    }

    function extractIdentifier(id: Dynamic):Identifier {
        return  GenericDeserializer.makeClassFrom(Identifier, id);
    }

    function extractInterval(interval: Dynamic):TimeInterval {
        return GenericDeserializer.makeClassFrom(TimeInterval, interval);
    }

    function extractError(err:Dynamic):Null<ErrorResultsModel> {
        if(err == null) return null;

        return {
            errorType: err.errorType,
            message: err.message,
            previous: extractError(err.previous)
        };
    }

    function asStringArray(arr:Dynamic):Null<Array<String>> {
        if(arr == null) return null;

        return GenericDeserializer.makeArrayAs(arr, (value)->{
            return Std.string(value);
        });
    }
}