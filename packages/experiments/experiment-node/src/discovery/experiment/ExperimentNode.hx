package discovery.experiment;

import haxe.io.Path;
import discovery.dht.dhttypes.PeerAddress;
import discovery.experiment.mechanisms.ExperimentsDiscoveryConfiguration;
import discovery.bundle.ConfigureOptions;
import discovery.experiment.runner.StageControl;
import discovery.experiment.events.StageListener;
import discovery.experiment.results.ResultsMonitor;
import discovery.experiment.ExperimentRunner;
import discovery.experiment.parsing.ConfigurationLoader;
import discovery.bundle.ConfigurableDiscoveryBuilder;

typedef ExperimentNodeDependencies = {
    ?discovery: ConfigurableDiscoveryBuilder,
    ?configurer: ConfigurationLoader,
    ?runner: ExperimentRunner,
    ?monitor: ResultsMonitor
};

typedef ExperimentNodeOptions = {
    configFile:String,
    nodeName:String,
    experimentDir:String,
    ?mechanisms:Array<String>,
    ?outputDir:String
};

class ExperimentNode {
    var deps:ExperimentNodeDependencies;

    public function new(?deps: ExperimentNodeDependencies) {
        if(deps == null) deps = {};

        this.deps = fillDependencies(deps);
    }

    public function configure(options:ExperimentNodeOptions)
    {
        var config = deps.configurer.loadConfiguration(
                options.configFile, options.nodeName
            );
        config = updateConfig(config, options);

        deps.discovery.configure(discoveryConfigs(config));
        runner().configure(config);
        deps.monitor.configure(config);
    }

    function updateConfig(
        config:NodeConfiguration,
        options:ExperimentNodeOptions): NodeConfiguration
    {
        if(config.nodeName == null && options.nodeName != null){
            config.nodeName == options.nodeName;
        }
        if(options.mechanisms != null){
            config.mechanisms = options.mechanisms;
        }
        if(options.outputDir != null){
            config.baseOutputDir = options.outputDir;
        }
        if(config.keysdir != null && !Path.isAbsolute(config.keysdir)){
            config.keysdir = Path.join([options.experimentDir, config.keysdir]);
        }

        return config;
    }

    function discoveryConfigs(config:NodeConfiguration):ConfigureOptions
    {
        if(config == null) return {};

        var bootstrapAddresses:Array<PeerAddress> = null;

        if(config.bootstrapAddress != null){
            bootstrapAddresses = [
                PeerAddress.fromString(config.bootstrapAddress)
            ];
        }

        return {
            keys: {
                keysdir: config.keysdir
            },
            keysCache: {
                keysdir: config.outputPath()
            },
            dht: {
                bootstrap: bootstrapAddresses
            },
            mechanisms: config.mechanisms,
            pkis: config.mechanisms
        };
    }

    function fillDependencies(
        deps:ExperimentNodeDependencies):ExperimentNodeDependencies
    {
        if(deps.monitor == null) deps.monitor = new ResultsMonitor();
        if(deps.configurer == null) deps.configurer = new ConfigurationLoader();
        if(deps.discovery == null)
            deps.discovery = new ExperimentsDiscoveryConfiguration();

        return deps;
    }

    public function start(stageControl:StageControl, ?stageListener: StageListener)
    {
        deps.monitor.stageListener = stageListener;
        deps.runner.start(stageControl, deps.monitor);
    }

    function runner() {
        if(this.deps.runner == null){
            this.deps.runner = buildRunner();
        }

        return this.deps.runner;
    }

    function buildRunner():Null<ExperimentRunner> {
        var runner = new ExperimentRunner(deps.discovery.build());

        return runner;
    }
}