package discovery.experiment;

import discovery.experiment.cli.CliOptions;
import haxe.Exception;
import discovery.experiment.cli.CliHandler;
import discovery.experiment.cli.ExperimentNodeCli;
import discovery.cli.console.SysConsole;
import discovery.cli.console.Console;
import discovery.utils.functional.Callback;
import discovery.experiment.runner.StageControl;
import discovery.experiment.events.StageListener;


class Main implements CliHandler{
    public static function main() {
        new Main().run(Sys.args());
    }

    var nodeCli = new ExperimentNodeCli();
    var node = new ExperimentNode();
    var console = new SysConsole();
    var stageController:CliStageListener;

    public function new() {
        stageController = new CliStageListener(console);
    }

    public function run(args: Array<String>){
        nodeCli.parseArguments(args, this);
    }

    public function start(options:CliOptions) {
        node.configure(options);

        node.start(stageController, stageController);
    }

    public function onError(err:Exception) {
        console.printError(err.toString());

        console.println('');
        console.println(usage());
        console.println(nodeCli.getUsage());
    }

    function usage() {
        return 'Usage: <configFile> <nodeName> [options]';
    }
}

class CliStageListener
    implements StageListener
    implements StageControl
{
    var console:Console;

    public function new(console: Console) {
        this.console = console;
    }

    public function waitForNextStage(callback:Callback) {
        if(callback == null) return;

        console.onReadline((_)->callback());
    }

    public function publicationsDone() {
        console.println('publication: done');
    }

    public function searchesFinished() {}

    public function finished() {
        console.println('finished execution');
    }
}