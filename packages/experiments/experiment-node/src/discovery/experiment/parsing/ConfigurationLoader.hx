package discovery.experiment.parsing;

import discovery.experiment.parsing.ConfigurationParser;
import discovery.experiment.parsing.ConfigurationReader;
import discovery.experiment.ExperimentRunner.NodeConfiguration;

typedef ConfigurationLoaderDeps = {
    ?reader: ConfigurationReader,
    ?parser: ConfigurationParser
};

class ConfigurationLoader {
    var deps:ConfigurationLoaderDeps;

    public function new(?deps: ConfigurationLoaderDeps) {
        if(deps == null) deps = {};

        if(deps.parser == null) deps.parser = new ConfigurationParser();
        if(deps.reader == null) deps.reader = new ConfigurationReader();

        this.deps = deps;
    }

    public function
        loadConfiguration(configFile:String, nodeName:String): NodeConfiguration
    {
        var config = deps.reader.readConfiguration(configFile);

        return deps.parser.parseNodeConfiguration(config, nodeName);
    }
}