package discovery.experiment.parsing;

import sys.io.File;
import yaml.Parser.ParserOptions;
import yaml.Yaml;

class ConfigurationReader {
    public function new() {

    }

    public function readConfiguration(configFilePath:String):ConfigurationModel {
        var content = File.getContent(configFilePath);

        return readFromString(content);
    }

    public function readFromString(configText:String):ConfigurationModel {
        return Yaml.parse(configText, new ParserOptions().useObjects());
    }
}