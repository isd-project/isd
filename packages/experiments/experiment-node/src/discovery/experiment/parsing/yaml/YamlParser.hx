package discovery.experiment.parsing.yaml;

import yaml.YamlType;
import yaml.YamlType.AnyYamlType;
import yaml.type.YTimestamp;
import yaml.Renderer.RenderOptions;
import yaml.Yaml;
import haxe.crypto.Base64;
import haxe.io.Bytes;
import yaml.schema.DefaultSchema;
import yaml.type.YBinary;
import yaml.Parser;

class YamlParser{
    var yamlSchema = new FixedSchema();

    var parseOptions:ParserOptions;
    var renderOptions:RenderOptions;

    public function new() {
        parseOptions = new ParserOptions(yamlSchema).useObjects();
        renderOptions = new RenderOptions(new ExplicitTimestampSchema());
    }

    public function parse(results:String): Dynamic {
        return Yaml.parse(results, parseOptions);
    }

    public function render(serialized:Any):String {
        return Yaml.render(serialized, renderOptions);
    }
}


class FixedSchema extends yaml.Schema{
    public function new()
    {
        super([new DefaultSchema()], [new YamlBinary()]);
    }
}

class ExplicitTimestampSchema extends yaml.Schema{
    public function new() {
        var timestampType = new YTimestamp();

        super([new DefaultSchema()], [new YamlBinary(), timestampType]);

        this.compiledImplicit = removeTypeFrom(timestampType.tag, compiledImplicit);
    }

    function removeTypeFrom(typeTag:String, list:Array<AnyYamlType>)
    {
        return list.filter((yamlType)->{
            return yamlType.tag != typeTag;
        });
    }
}

class YamlBinary extends YBinary{

    var complement:Bool;

    public function new(complement:Bool=false) {
        super();
        this.complement = complement;
    }

    override public function resolve(object:String, ?usingMaps:Bool = true, ?explicit:Bool):Bytes
    {
        return Base64.decode(object, complement);
    }

    override function represent(object:Bytes, ?style:String):String {
        return Base64.encode(object, complement);
    }
}