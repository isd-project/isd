package discovery.experiment.parsing;

import discovery.keys.crypto.Fingerprint;
import discovery.format.Binary;
import discovery.format.Formatters;
import discovery.domain.ProviderId;
import discovery.utils.time.Duration;
import discovery.experiment.parsing.ConfigurationModel.SearchSpecModel;
import discovery.experiment.runner.SearchSpec;
import discovery.domain.Description;
import haxe.DynamicAccess;
import discovery.experiment.parsing.ConfigurationModel.ServicePublicationModel;
import discovery.experiment.ExperimentRunner.ServicePublication;
import discovery.experiment.parsing.ConfigurationModel.NodeConfigurationModel;
import discovery.experiment.ExperimentRunner.NodeConfiguration;

using discovery.utils.time.TimeUnitTools;


class ConfigurationParser {
    public function new() {

    }

    public function parseNodeConfiguration(
        experimentConfig:ConfigurationModel, node:String):NodeConfiguration
    {
        var nodeConf = getNode(experimentConfig.nodes, node);

        return parseNodeConfig(nodeConf, experimentConfig);
    }

    function getNode(nodesConfig:DynamicAccess<NodeConfigurationModel>,
                     node:String): NodeConfigurationModel
    {
        return nodesConfig.get(node);
    }

    function parseNodeConfig(
        nodeConf:NodeConfigurationModel,
        experimentConfig:ConfigurationModel):NodeConfiguration
    {
        return {
            nodeName: nodeConf.name,
            keysdir: nodeConf.keysdir,
            baseOutputDir: experimentConfig.output_dir,
            outputDir: nodeConf.output_dir,
            resultsFile: nodeConf.results_file,
            bootstrapAddress: experimentConfig.bootstrap_address,
            publications: parsePublications(nodeConf, nodeConf.publications),
            searches: parseSearches(nodeConf.searches)
        };
    }

    function parsePublications(
        nodeConf:NodeConfigurationModel,
        srvs:Array<ServicePublicationModel>):Array<ServicePublication>
    {
        if(srvs == null) return null;

        var providerId:ProviderId = parseProviderId(nodeConf);

        return [
            for(srvPubl in srvs)
                parseSrvPublication(providerId, srvPubl)
        ];
    }

    function parseSrvPublication(providerId:ProviderId,
                                 srv:ServicePublicationModel):ServicePublication
    {
        return {
            description: parseDescription(providerId, srv),
            delay: parseDelay(srv.delay)
        };
    }

    function parseDescription(providerId:ProviderId,
                            srv:ServicePublicationModel):Description
    {
        return {
            name: srv.name,
            type: srv.type,
            port: srv.port,
            providerId: providerId
        };
    }

    function parseSearches(searches:Array<SearchSpecModel>):Array<SearchSpec> {
        if(searches == null) return [];

        return [for(s in searches) parseSearchSpec(s)];
    }

    function parseSearchSpec(s:SearchSpecModel):SearchSpec {
        return {
            kind: SearchSpec.searchKindFromString(s.kind),
            srvType: s.srvType,
            srvId: s.srvId,
            delay: parseDelay(s.delay)
        };
    }

    function parseDelay(delay:Null<Int>):Null<Duration> {
        if(delay == null) return 0.millis();

        return delay.millis();
    }

    function parseProviderId(nodeConf:NodeConfigurationModel):ProviderId {
        return new Fingerprint(
            HashTypes.fromString(nodeConf.keyid_alg),
            parseKeyId(nodeConf)
        );
    }

    function parseKeyId(nodeConf:NodeConfigurationModel):Binary {
        var formatter = Formatters.getFormatter(nodeConf.keyid_fmt);

        if(nodeConf.keyid_fmt != null && formatter != null){
            return formatter.decode(nodeConf.keyid);
        }

        return Binary.fromHex(nodeConf.keyid);
    }
}