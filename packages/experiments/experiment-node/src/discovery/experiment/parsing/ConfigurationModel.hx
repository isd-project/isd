package discovery.experiment.parsing;

import haxe.extern.EitherType;
import haxe.DynamicAccess;

typedef ConfigurationModel = {
    ?output_dir:String,
    ?bootstrap_address:String,
    nodes: EitherType<Dynamic, DynamicAccess<NodeConfigurationModel>>
};

typedef NodeConfigurationModel = {
    ?name: String,
    ?keyid: String,
    ?keyid_fmt:String,
    ?keyid_alg:String,
    ?keysdir: String,
    ?output_dir: String,
    ?results_file: String,
    ?publications: Array<ServicePublicationModel>,
    ?searches: Array<SearchSpecModel>
};

typedef ServicePublicationModel = {
    ?name: String,
    ?type: String,
    ?delay: Int,
    ?port: Int
};

typedef SearchSpecModel = {
    ?kind: String,
    ?srvType:String,
    ?srvId:String,
    ?delay:Int
};