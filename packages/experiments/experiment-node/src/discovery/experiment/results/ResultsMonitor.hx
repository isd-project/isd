package discovery.experiment.results;

import discovery.experiment.runner.NodeMonitor;
import discovery.experiment.events.SearchEndEvent;
import discovery.utils.collections.ComparableMap;
import discovery.experiment.runner.SearchSpec;
import haxe.Constraints.IMap;
import discovery.experiment.events.DiscoveredEvent;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import discovery.experiment.events.StageListener;
import discovery.experiment.events.PublicationEvent;

using discovery.experiment.results.ResultsTransformations;


class ResultsMonitor implements NodeMonitor{
    var publisher:ResultsPublisher;
    var publications:Array<PublicationEvent> = [];

    var searchResults:IMap<SearchSpec, SearchResults>;

    var configuration:NodeConfiguration;

    public function new(?resultsPublisher: ResultsPublisher) {
        if(resultsPublisher == null)
            resultsPublisher = new ResultsPublisher();

        this.publisher = resultsPublisher;

        searchResults = new ComparableMap();
    }


    public var stageListener(default, default):StageListener;


    public function configure(configuration:NodeConfiguration) {
        this.configuration = configuration;
        this.publisher.configure(configuration);
    }


    public function onPublication(pubEvt:PublicationEvent) {
        publications.push(pubEvt);
    }

    public function publicationsDone() {
        if(stageListener != null){
            stageListener.publicationsDone();
        }
    }

    public function onDiscovery(evt:DiscoveredEvent) {
        var results = getSearchResultsFor(evt.searchSpec);
        results.addDiscovered(evt);
    }

    function getSearchResultsFor(searchSpec:SearchSpec):SearchResults {
        var results = searchResults.get(searchSpec);
        if(results == null){
            results = new SearchResults(searchSpec);
            searchResults.set(searchSpec, results);
        }

        return results;
    }

    public function onSearchFinished(evt:SearchEndEvent) {
        getSearchResultsFor(evt.searchSpec).finished(evt);
    }

    public function searchesFinished() {}

    public function finished() {
        publishResults();

        if(stageListener != null){
            stageListener.finished();
        }
    }

    function publishResults() {
        this.publisher.publishResults({
            usedMechanisms: usedMechanisms(),
            publications: publications.toPublicationResults(),
            searches: [for(k=>result in searchResults) result]
        });
    }

    function usedMechanisms():Null<Array<String>> {
        return if(configuration == null) null
            else configuration.mechanisms;
    }
}