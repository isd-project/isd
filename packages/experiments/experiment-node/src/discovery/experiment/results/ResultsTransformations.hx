package discovery.experiment.results;

import discovery.experiment.events.PublicationEvent;

class ResultsTransformations {

    public static function toPublicationResults(
        pubEvts: Array<PublicationEvent>): Array<PublicationResult>
    {
        return pubEvts.map(toPublicationResult);
    }

    public static function
        toPublicationResult(pubEvt: PublicationEvent): PublicationResult
    {
        return {
            service: pubEvt.service.identifier(),
            interval: pubEvt.interval
        };
    }
}