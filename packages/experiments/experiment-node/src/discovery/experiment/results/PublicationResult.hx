package discovery.experiment.results;

import discovery.utils.time.TimeInterval;
import discovery.domain.Identifier;

typedef PublicationResult = {
    ?service: Identifier,
    ?interval: TimeInterval
};