package discovery.experiment.results;

import sys.FileSystem;
import haxe.io.Path;
import discovery.experiment.parsing.yaml.YamlParser;
import discovery.experiment.serialization.ExperimentResultsSerializer;
import sys.io.File;


class ResultsPersistence {

    var serializer = new ExperimentResultsSerializer();
    var parser = new YamlParser();

    public function new() {

    }

    public function load(serializedPath:String): ExperimentResults {
        var content = File.getContent(serializedPath);
        return parser.parse(content);
    }

    public function parse(results: String): ExperimentResults
    {
        var parsed = parser.parse(results);
        return serializer.deserialize(parsed);
    }

    public function save(results: ExperimentResults, outputPath: String) {
        var parentDir = Path.directory(outputPath);

        FileSystem.createDirectory(parentDir);
        File.saveContent(outputPath, render(results));
    }

    public function render(results: ExperimentResults): String
    {
        var serialized = serializer.serialize(results);
        return parser.render(serialized);
    }
}