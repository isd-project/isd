package discovery.experiment.results;

import discovery.experiment.events.SearchEndEvent;
import haxe.Exception;
import discovery.experiment.events.DiscoveredEvent;
import discovery.utils.time.TimeInterval;
import discovery.domain.Identifier;
import discovery.experiment.runner.SearchSpec;

@:structInit
class SearchResults {
    public var search: SearchSpec;

    @:optional
    public var found: Array<FoundService> = [];

    @:optional
    public var error:ErrorResults;

    @:optional
    public var finishedTime:TimeInterval;

    public function new(search: SearchSpec, ?found: Array<FoundService>,
        ?error:ErrorResults, ?finishedTime: TimeInterval)
    {
        if(found == null) found = [];

        this.search = search;
        this.found = found;
        this.error = error;
        this.finishedTime = finishedTime;
    }

    public function addDiscovered(dEvt: DiscoveredEvent) {
        found.push({
            id: dEvt.discovered.identifier(),
            searchTime: dEvt.interval
        });
    }

    public function finished(evt:SearchEndEvent) {
        this.error = evt.error;
        this.finishedTime = evt.interval;
    }
}

typedef FoundService = {
    id: Identifier,
    searchTime: TimeInterval
};