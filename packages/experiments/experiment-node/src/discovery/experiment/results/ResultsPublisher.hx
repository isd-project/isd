package discovery.experiment.results;

import haxe.io.Path;
import discovery.experiment.ExperimentRunner.NodeConfiguration;

class ResultsPublisher {

    public var outputPath(default, null):String;

    var configuration:NodeConfiguration;

    var persistence:ResultsPersistence;

    public function new() {
        persistence = new ResultsPersistence();
    }

    // ------------------------------------

    public function configure(configuration:NodeConfiguration) {
        this.configuration = configuration;

        outputPath = Path.join([
            configuration.outputPath(), genFileName(configuration)
        ]);
    }

    function genFileName(config: NodeConfiguration) {
        if(configuration.resultsFile != null)
            return configuration.resultsFile;

        return '${config.nodeName}_results.yaml';
    }

    // ------------------------------------

    public function publishResults(results: ExperimentResults) {
        trace('publishing results to ${outputPath}');
        persistence.save(results, outputPath);
    }
}