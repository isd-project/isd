package discovery.experiment.results;

import haxe.CallStack;
import haxe.Exception;

@:forward
abstract ErrorResults(ErrorResultsModel)
    from ErrorResultsModel to ErrorResultsModel
{
    public function new(err: ErrorResultsModel){
        this = err;
    }

    @:from
    public static function fromException(e:Exception):ErrorResults{
        if(e == null) return null;

        var cl = Type.getClass(e);
        var clName = if(cl == null) null else Type.getClassName(cl);

        return {
            errorType: clName,
            message: e.message,
            previous: fromException(e.previous)
        };
    }
}

typedef ErrorResultsModel = {
    public var errorType:String;

    @:optional
    public var message:String;

    @:optional
    public var previous:ErrorResults;
}