package discovery.experiment.results;

import discovery.experiment.results.SearchResults;
import discovery.experiment.results.PublicationResult;

typedef ExperimentResults = {
    ?usedMechanisms:Array<String>,
    ?publications: Array<PublicationResult>,
    ?searches: Array<SearchResults>
};