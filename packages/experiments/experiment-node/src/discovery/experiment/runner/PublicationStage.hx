package discovery.experiment.runner;

import discovery.utils.time.scheduler.TaskScheduler;
import discovery.utils.time.TimeInterval;
import discovery.utils.time.Chronometer;
import discovery.domain.Publication.PublishedEventInfo;
import discovery.async.DoneCallback;
import haxe.Exception;
import discovery.utils.functional.CompositeDoneCallback;


typedef PublicationStageDependencies = {
    discovery: Discovery,
    scheduler: TaskScheduler,
    ?monitor: NodeMonitor
}

class PublicationStage {
    var discovery: Discovery;
    var monitor:NodeMonitor;
    var scheduler:TaskScheduler;

    var chronometer:Chronometer;

    public function new(deps: PublicationStageDependencies) {
        this.discovery = deps.discovery;
        this.monitor = deps.monitor;
        this.scheduler = deps.scheduler;

        this.chronometer = new Chronometer();
    }

    public function publishAll(
        publications: Array<ServicePublication>, ?callback: DoneCallback)
    {
        if(publications == null || publications.length == 0){
            publicationsDone(null, callback);

            return;
        }

        startPublication(publications, callback);
    }

    function startPublication(
        publications: Array<ServicePublication>, ?callback: DoneCallback)
    {
        var oncePublished = CompositeDoneCallback.make(
            publications.length, (?err)->{
                publicationsDone(err, callback);
            });

        for (srv in publications){
            schedulePublication(srv, oncePublished);
        }
    }

    function schedulePublication(
        srv: ServicePublication, oncePublished:DoneCallback)
    {
        var publish = publishService.bind(srv, oncePublished);

        scheduler.schedule(publish, srv.delay.time());
    }

    function publishService(srv:ServicePublication, oncePublished:DoneCallback) {
        var measure = chronometer.start();

        var publication = this.discovery.publish(srv.description);
        publication.oncePublished.listen((evt)->{
            notifyPublication(evt, measure.measure(), oncePublished);
        });
    }

    function notifyPublication(
        evt:PublishedEventInfo,
        publicationTime: TimeInterval,
        oncePublished:DoneCallback)
    {
        if(monitor != null){
            monitor.onPublication({
                service: evt.published.description(),
                interval: publicationTime
            });
        }

        oncePublished();
    }

    function publicationsDone(?err: Exception, ?onDone: DoneCallback) {
        if(monitor != null){
            monitor.publicationsDone();
        }

        if(onDone != null) onDone(err);
    }
}