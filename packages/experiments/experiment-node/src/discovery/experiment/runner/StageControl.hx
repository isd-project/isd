package discovery.experiment.runner;

import discovery.utils.functional.Callback;

interface StageControl {
    function waitForNextStage(callback:Callback):Void;
}