package discovery.experiment.runner;

import discovery.domain.Identifier;
import discovery.base.IdentifierParser;
import discovery.domain.Query;
import discovery.utils.time.Duration;

enum SearchKind {
    Search;
    Locate;
}

@:structInit
class SearchSpec{

    public var kind(default, default):SearchKind;

    @:optional
    public var srvType(default, default):String;

    @:optional
    public var srvId(default, default):String;

    @:optional
    public var delay(default, default):Duration;

    public function isSearch() {
        return kind == Search;
    }

    public function query(): Query {
        return {
            type: srvType
        };
    }

    public function identifier(identifierParser:IdentifierParser): Identifier {
        return identifierParser.parseUrl(this.srvId);
    }

    public static function searchKindFromString(kindStr: String): SearchKind {
        if(kindStr == null) kindStr = '';
        else kindStr = new String(kindStr).toLowerCase();

        return switch (kindStr){
            case 'search': Search;
            case 'locate': Locate;
            default: null;
        };
    }

    public function toString(): String {
        var arg = if(kind == Search) srvType else srvId;

        return '{${kind}: ${arg}, d:${delay}ms}';
    }
}