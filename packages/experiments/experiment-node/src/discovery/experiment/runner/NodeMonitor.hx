package discovery.experiment.runner;

import discovery.experiment.events.SearchEndEvent;
import discovery.experiment.events.DiscoveredEvent;
import discovery.experiment.events.StageListener;
import discovery.experiment.events.PublicationEvent;

interface NodeMonitor extends StageListener{
    public function onPublication(pubEvt: PublicationEvent):Void;
    public function onDiscovery(evt: DiscoveredEvent):Void;
    public function onSearchFinished(evt:SearchEndEvent):Void;
}