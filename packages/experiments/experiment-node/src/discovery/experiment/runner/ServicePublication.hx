package discovery.experiment.runner;

import discovery.utils.time.Duration;
import discovery.domain.Description;

typedef ServicePublication = {
    description: Description,
    ?delay: Duration
};