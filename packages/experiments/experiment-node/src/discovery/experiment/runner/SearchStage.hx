package discovery.experiment.runner;

import discovery.experiment.runner.NodeMonitor;
import discovery.utils.time.Duration;
import discovery.base.search.SearchOptions;
import discovery.experiment.ExperimentRunner.NodeConfiguration;
import haxe.Exception;
import discovery.utils.functional.CompositeDoneCallback;
import discovery.async.DoneCallback;
import discovery.utils.time.Chronometer;
import discovery.utils.time.TimeCount;
import discovery.domain.Search;
import discovery.domain.Discovered;
import discovery.base.IdentifierParser;
import discovery.utils.time.scheduler.TaskScheduler;

using discovery.utils.time.TimeUnitTools;


typedef SearchStageDependencies = {
    discovery: Discovery,
    scheduler: TaskScheduler,
    idParser: IdentifierParser,
    ?monitor: NodeMonitor,
    ?config: NodeConfiguration
};

class SearchStage {
    var deps:SearchStageDependencies;

    public function new(deps: SearchStageDependencies) {
        this.deps = deps;
    }

    public function changeConfiguration(nodeConfig: NodeConfiguration){
        deps.config = nodeConfig;
    }

    function scheduler() {
        return deps.scheduler;
    }

    public function
        searchAll(searchSpecs:Array<SearchSpec>, ?onEnd: DoneCallback)
    {
        if(searchSpecs == null || searchSpecs.length == 0){
            onSearchFinish(null, onEnd);
            return;
        }

        startSearch(searchSpecs, onEnd);
    }

    function startSearch(searchSpecs:Array<SearchSpec>, ?onEnd: DoneCallback)
    {
        var onDone = CompositeDoneCallback.make(
            searchSpecs.length, onSearchFinish.bind(_, onEnd)
        );

        for(s in searchSpecs){
            scheduler().schedule(doSearch.bind(s, onDone), s.delay.time());
        }
    }

    function doSearch(searchSpec: SearchSpec, onDone:DoneCallback){
        var searchOp = new SearchOperation(searchSpec, deps);

        try{
            var s = searchOp.start();

            s.onceFinished.listen((evt)->{
                onDone(evt.error);
            });
        }
        catch(e){
            onDone(e);
        }
    }

    function onSearchFinish(?err:Exception, ?onEnd:DoneCallback){
        if(nodeMonitor() != null){
            nodeMonitor().searchesFinished();
        }

        if(onEnd != null){
            onEnd();
        }
    }

    function discovery() {
        return deps.discovery;
    }

    function idParser():IdentifierParser {
        return deps.idParser;
    }

    function nodeMonitor() {
        return deps.monitor;
    }
}


class SearchOperation {
    static final DEFAULT_TIMEOUT = 4.seconds();

    var searchSpec:SearchSpec;
    var deps:SearchStageDependencies;

    var chronometer = new Chronometer();
    var searchTime:TimeCount;

    public function new(searchSpec: SearchSpec, deps:SearchStageDependencies) {
        this.searchSpec = searchSpec;
        this.deps = deps;
    }

    public function start() {
        var searcher = makeSearchOperation();

        searchTime = chronometer.start();

        try{
            var s = searcher();
            s.onFound.listen(notifyDiscovered.bind(searchSpec, _));
            s.onceFinished.listen(notifyFinished.bind(searchSpec, _));

            return s;
        }
        catch(e){
            notifyFinished(searchSpec, {
                error:e
            });

            throw e;
        }
    }


    function makeSearchOperation(): ()->Search{
        var opt = searchOptions();

        if(searchSpec.isSearch()){
            var q = searchSpec.query();

            return deps.discovery.search.bind(q, opt);
        }
        else{
            var identifier = searchSpec.identifier(deps.idParser);

            return deps.discovery.locate.bind(identifier, opt);
        }
    }

    function notifyDiscovered(searchSpec:SearchSpec, discovered: Discovered) {
        if(deps.monitor != null){
            deps.monitor.onDiscovery({
                searchSpec: searchSpec,
                discovered: discovered,
                interval: searchTime.measure()
            });
        }
    }

    function notifyFinished(searchSpec:SearchSpec, evt:FinishSearchEvent){
        if(deps.monitor != null){
            deps.monitor.onSearchFinished({
                error: evt.error,
                searchSpec: searchSpec,
                interval: searchTime.measure()
            });
        }
    }

    function searchOptions(): SearchOptions {
        var searchTimeout:Duration = DEFAULT_TIMEOUT;

        if(deps.config != null
            && deps.config.searchTimeout != null)
        {
            searchTimeout = deps.config.searchTimeout;
        }

        return {
            timeout: searchTimeout
        };
    }
}