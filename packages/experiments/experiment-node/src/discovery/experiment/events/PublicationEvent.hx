package discovery.experiment.events;

import discovery.utils.time.TimeInterval;
import discovery.domain.Description;

@:structInit
class PublicationEvent {
    public var service(default, null):Description;

    @:optional
    public var interval(default, null): TimeInterval;
}