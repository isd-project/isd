package discovery.experiment.events;


import haxe.Exception;
import discovery.experiment.runner.SearchSpec;
import discovery.utils.time.TimeInterval;

@:structInit
class SearchEndEvent {
    public var searchSpec(default, null):SearchSpec;

    @:optional
    public var error(default, null):Exception;

    public var interval(default, null): TimeInterval;
}