package discovery.experiment.events;

interface StageListener {
    public function publicationsDone():Void;
    public function searchesFinished():Void;
    public function finished():Void;
}