package discovery.experiment.events;

import discovery.domain.Discovered;
import discovery.experiment.runner.SearchSpec;
import discovery.utils.time.TimeInterval;

@:structInit
class DiscoveredEvent {
    public var searchSpec(default, null):SearchSpec;

    public var discovered(default, null):Discovered;

    public var interval(default, null): TimeInterval;
}