package discovery.experiment.mechanisms;

import discovery.base.search.SearchController;
import discovery.base.publication.StoppablePublicationControl;
import discovery.utils.functional.Callback;
import discovery.mdns.MdnsQuery;
import discovery.domain.Announcement;
import discovery.utils.time.Duration;
import discovery.domain.Description;
import discovery.mdns.DiscoveredService;
import discovery.domain.Discovered;
import discovery.mdns.DnsSd;
import discovery.domain.Query;
import discovery.domain.Search;
import discovery.domain.Identifier;
import discovery.domain.PublishInfo;
import discovery.domain.PublishListeners;
import discovery.base.publication.PublicationControl;

using discovery.utils.time.TimeUnitTools;


class MdnsRawMechanism implements DiscoveryMechanism{
    var engine:DnsSd;

    static final DEFAULT_VALIDITY = 10.minutes();
    /** Time in which a discovered service will be considered valid. **/
    var serviceValidity:Duration;

    public function new(engine:DnsSd, ?serviceValidity:Duration) {
        if(serviceValidity == null) serviceValidity =  DEFAULT_VALIDITY;

        this.engine = engine;
        this.serviceValidity = serviceValidity;
    }

    public function finish(callback:Callback) {
        this.engine.finish(callback);
    }

    public function search(query:Query):Search {
        var mdnsQuery:MdnsQuery = { type: query.type};

        return executeSearch((listener)->{
            return engine.find(mdnsQuery, discoveryListener(listener));
        });
    }

    public function locate(id:Identifier):Search {
        var query:DnsSdResolveQuery = {
            name: id.name,
            type: id.type
        };

        return executeSearch((listener)->{
            return engine.resolve(query, discoveryListener(listener));
        });
    }

    function executeSearch(searchOp:SearchOperation) {
        var searchController = new SearchController(searchOp);
        return searchController.start();
    }

    function discoveryListener(listener:SearchListener):MdnsServiceFound {
        return (mdnsDiscoveredSrv)->{
            if(listener != null && listener.discovered != null){
                listener.discovered(toDiscovered(mdnsDiscoveredSrv));
            }
        };
    }

    function toDiscovered(mdnsService:DiscoveredService):Discovered {

        var loc:Location = new Location();
        loc.defaultPort  = mdnsService.port;
        loc.addresses    = mdnsService.addresses;

        var description:Description = {
            name: mdnsService.name,
            type: mdnsService.type,
            location: loc
        };

        var validity = AnnouncementValidity.validFor(serviceValidity);

        return Discovered.fromAnnouncement(
                    Announcement.make(description, validity));
    }

    public function publish(
        info:PublishInfo, ?listeners:PublishListeners):PublicationControl
    {
        var d = info.description();

        var stoppable = engine.publish({
            name: d.name,
            type: d.type,
            port: d.port
        }, makePublishListener(listeners));

        return new StoppablePublicationControl(stoppable);
    }

    function makePublishListener(
        listeners:PublishListeners):DnsSdPublishListener
    {
        if(listeners == null) return null;

        return {
            onPublish: listeners.onPublish,
            onFinish: listeners.onFinish
        };
    }
}