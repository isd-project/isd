package discovery.experiment.mechanisms;

import discovery.async.DoneCallback;
import discovery.keys.storage.PublicKeyData;
import discovery.keys.keydiscovery.SearchKeyListeners;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.async.promise.PromisableFactory;
import discovery.base.search.StopOnce;
import discovery.base.search.Stoppable.StopCallback;
import discovery.keys.crypto.Fingerprint;
import discovery.keys.Keychain;

/**
    A KeyTranspart that does nothing (only default operations).
    Useful for tests/simulation in which key search is not executed.
**/
class DummyKeyTransport implements KeyTransport{

    public function new()
    {}

    public function publishKeychain(keychain:Keychain, ?done:DoneCallback) {
        if(done != null){
            done();
        }
    }

    public function publishKey(key:PublicKeyData, ?done:DoneCallback) {
        if(done != null){
            done();
        }
    }

    public function
        searchKey(fin:Fingerprint, listeners:SearchKeyListeners):Stoppable
    {
        if(listeners != null && listeners.onEnd != null){
            listeners.onEnd();
        }

        return new DummyStoppable();
    }
}

class DummyStoppable implements Stoppable{
    public function new() {

    }

    public function stop(?callback:StopCallback) {
        if(callback != null){
            callback();
        }
    }
}