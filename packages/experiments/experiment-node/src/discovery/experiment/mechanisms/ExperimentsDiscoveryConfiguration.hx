package discovery.experiment.mechanisms;

import discovery.bundle.BundleDiscoveryConfiguration;

class ExperimentsDiscoveryConfiguration extends BundleDiscoveryConfiguration{
    public function new(?options: BundleDiscoveryConfigurationOptions) {
        super(options);

        addMechanism(new BonjourMdnsRawMechanismBuilder(), 'mdns-raw');

        keyTransportBuilder().addBuilder(
            new DummyKeyTransportBuilder(), "mdns-raw");

        configure({
            mechanisms: ['mdns', 'dht'],
            pkis: ['mdns', 'dht']
        });
    }
}