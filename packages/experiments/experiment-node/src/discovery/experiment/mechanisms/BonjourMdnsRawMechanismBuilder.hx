package discovery.experiment.mechanisms;

import discovery.mdns.externs.bonjour.engine.BonjourDnsSd;
import discovery.mdns.externs.bonjour.Bonjour;
import discovery.mdns.DnsSd;
import discovery.experiment.mechanisms.MdnsRawMechanismBuilder.DnsSdEngineBuilder;

class BonjourEngineBuilder implements DnsSdEngineBuilder{
    var reuseEngine:Bool;

    var _bonjour:Bonjour;

    public function new(reuseEngine:Bool=true) {
        this.reuseEngine = reuseEngine;
    }

    public function buildDnsSd():DnsSd {
        return new BonjourDnsSd(bonjour());
    }

    public function bonjour():Bonjour {
        if(_bonjour == null || !reuseEngine){
            _bonjour = Bonjour.create();
        }

        return _bonjour;
    }
}

class BonjourMdnsRawMechanismBuilder extends MdnsRawMechanismBuilder{
    public function new(reuseEngines=false) {
        super(new BonjourEngineBuilder(reuseEngines));
    }
}