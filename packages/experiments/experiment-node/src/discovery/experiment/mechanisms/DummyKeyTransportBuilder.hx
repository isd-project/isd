package discovery.experiment.mechanisms;

import discovery.async.promise.PromisableFactory;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.keys.keydiscovery.KeyTransportBuilder;

class DummyKeyTransportBuilder implements KeyTransportBuilder{

    public function new()
    {}

    public function buildKeyTransport():KeyTransport {
        return new DummyKeyTransport();
    }
}