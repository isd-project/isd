package discovery.experiment.mechanisms;

import discovery.mdns.DnsSd;
import discovery.base.configuration.DiscoveryMechanismBuilder;

interface DnsSdEngineBuilder{
    function buildDnsSd(): DnsSd;
}

class MdnsRawMechanismBuilder implements DiscoveryMechanismBuilder{
    var engineBuilder:DnsSdEngineBuilder;

    public function new(engineBuilder: DnsSdEngineBuilder) {
        this.engineBuilder = engineBuilder;
    }

    public function buildMechanism():DiscoveryMechanism {
        return new MdnsRawMechanism(engineBuilder.buildDnsSd());
    }
}