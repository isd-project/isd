package discovery.experiment;

import hx.concurrent.atomic.AtomicBool;
import haxe.Log;
import haxe.PosInfos;
import haxe.io.Path;
import discovery.experiment.runner.NodeMonitor;
import discovery.utils.time.Duration;
import discovery.experiment.runner.SearchSpec;
import discovery.impl.parser.TinkIdentifierParser;
import discovery.experiment.runner.SearchStage;
import discovery.utils.time.scheduler.TaskScheduler;
import discovery.experiment.runner.StageControl;
import discovery.experiment.runner.PublicationStage;
import haxe.Exception;

using discovery.utils.time.TimeUnitTools;


typedef ServicePublication = discovery.experiment.runner.ServicePublication;

@:structInit
class NodeConfiguration{
    @:optional public var nodeName: String;
    @:optional public var keysdir      : String;
    @:optional public var baseOutputDir: String;
    @:optional public var outputDir    : String;
    @:optional public var resultsFile  : String;
    @:optional public var bootstrapAddress: String;
    @:optional public var searchTimeout   :Duration;
    @:optional public var publications: Array<ServicePublication>;
    @:optional public var searches    : Array<SearchSpec>;
    @:optional public var mechanisms  : Array<String>;

    public function outputPath(): String {
        var paths = [];

        if(baseOutputDir != null){
            paths.push(baseOutputDir);
        }
        if(outputDir != null && outputDir.length > 0){
            paths.push(outputDir);
        }
        return Path.join(paths);
    }
}

class ExperimentRunner {
    var discovery: Discovery;
    var scheduler:TaskScheduler;

    var configuration:NodeConfiguration;

    var publicationCompleted = new AtomicBool(false);
    var startedSearch = new AtomicBool(false);


    public function new(discovery: Discovery, ?scheduler:TaskScheduler) {
        if(scheduler == null) scheduler = new TaskScheduler();

        this.discovery = discovery;
        this.scheduler = scheduler;
    }

    public function configure(config:NodeConfiguration) {
        this.configuration = config;
    }

    public function start(stageControl:StageControl, ?nodeMonitor: NodeMonitor)
    {
        var publications = new PublicationStage({
            discovery: discovery,
            monitor: nodeMonitor,
            scheduler: scheduler
        });

        log('publishing');
        publications.publishAll(configuration.publications,
                        publicationFinished.bind(stageControl, nodeMonitor, _));
    }

    function publicationFinished(
        stageControl:StageControl, ?nodeMonitor:NodeMonitor, ?err:Exception)
    {
        if(publicationCompleted.getAndSet(true)){
            //already published
            log('WARNING: publicationFinished callback called twice');
            return;
        }

        log('waiting for next stage, after publication completed with -> ${err}');
        stageControl.waitForNextStage(()->{
            executeSearches(nodeMonitor);
        });
    }

    function executeSearches(?nodeMonitor:NodeMonitor) {
        if(startedSearch.getAndSet(true)){
            //already started
            log('WARNING: searches completed callback called twice');
            return;
        }

        var searchStage = new SearchStage({
            discovery: discovery,
            scheduler: scheduler,
            monitor: nodeMonitor,
            idParser: new TinkIdentifierParser()
        });

        log('starting searches');
        searchStage.searchAll(configuration.searches,
                                onSearchFinished.bind(_, nodeMonitor));
    }

    function onSearchFinished(?err:Exception, ?nodeMonitor: NodeMonitor) {
        log('searches completed. Err -> ${err}');
        finish(nodeMonitor);
    }

    function finish(nodeMonitor:NodeMonitor) {
        log('finishing run');
        if(nodeMonitor != null){
            nodeMonitor.finished();
        }
    }

    function log(msg: String, ?pos:PosInfos) {
        Log.trace('[${time()}] ${node()}: ${msg}', pos);
    }

    function node(){
        return if(configuration != null && configuration.nodeName != null)
                configuration.nodeName else '';
    }

    function time() {
        var now = Date.now();
        return '${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}';
    }
}