from unittest.mock import Mock, MagicMock, patch
from functools import partial

import sys

# prevent loading module core_robot
sys.modules['core_robot'] = MagicMock()

@patch("core_robot.Core")
def mock_emulator(CoreMock):
    from experiments.core import CoreEmulator

    core = Mock()
    mock = Mock(wraps=CoreEmulator(core))

    mock.session_open.side_effect =  partial(CoreEmulator.session_open, mock)

    return mock