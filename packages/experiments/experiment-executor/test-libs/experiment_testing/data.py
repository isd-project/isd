from experiments.model import ExperimentConfig
from experiments.parsing import ConfigurationParser

from testdata import datapaths

testkeysdir = datapaths.test_keysdir()
testkeys = [
    {
        'id': '5026701f75e3ae37a96e67b8d80af5857654d2856adc11d01d379e3983c8e2f4',
        'type': 'ec',
        'format': 'b16'
    },
    {
        'id': '98b7de511604ab98141fc399d75a7d2e4b1e208d1cb5f3fcc876c3ccfe22f98b',
        'type': 'ec',
        'format': 'b16'
    },
    {
        'id': 'd93ee40b4db963b7ab5abe8563ce0901f29248e6d4afa4c65eac8bd314d7a27b',
        'type': 'RSA',
        'format': 'b16'
    }
]

def get_keyid(idx:int=None):
    return get_key(idx).get('id')

def get_key(idx:int=None):
    import random

    if idx is None:
        idx = random.randint(0, len(testkeys))

    return testkeys[idx]


def sample_experiment_configuration_model(output_dir, network_file=None, keysdir=None):
    if not isinstance(output_dir, str):
        output_dir = str(output_dir)

    if not keysdir:
        keysdir = output_dir

    if not network_file:
        network_file = datapaths.wired_lan_network()

    n1_key = get_key(0)
    n1_keyid = n1_key.get('id')
    n1_keyfmt = n1_key.get('format')
    n2_keyid = get_keyid(1)

    s1_n1_srv_id = 'srv://s1_n1.type1.{0};fmt={1}'.format(n1_keyid, n1_keyfmt)

    return ExperimentConfig.from_dict({
        'keysdir': keysdir,
        'output_dir': output_dir,
        'network_file': network_file,
        'nodes': {
            'n1': {
                'keyid': n1_keyid,
                'keysdir': keysdir,
                'results_file': 'n1_results.yaml',
                'net': 'net1',
                'publications':[{
                    'name': 's1_n1',
                    'type': 'type1',
                    'port': 1234,
                    'delay': 0
                }],
                'searches': [
                    {
                        'kind': 'search',
                        'srvType': 'type2',
                        'delay': 0
                    }
                ]

            },
            'n2': {
                'keyid': n2_keyid,
                'keysdir': keysdir,
                'results_file': 'n2_results.yaml',
                'net': 'net2',
                'publications':[{
                    'name': 's1_n2',
                    'type': 'type2',
                    'port': 1234,
                    'delay': 0
                }],
                'searches': [
                    {
                        'kind': 'locate',
                        'srvId': s1_n1_srv_id,
                        'delay': 20
                    }
                ]
            }
        }
    })

def sample_experiment_configuration_yaml(output_dir, network_file=None,keysdir=None):
    config = sample_experiment_configuration_model(
        output_dir, network_file=network_file, keysdir=keysdir
    )

    config_parser = ConfigurationParser()

    return config_parser.render_configuration(config)


_sample_output_dir = '/sample/output/dir'

experiment_configuration_model = sample_experiment_configuration_model(
    _sample_output_dir
)

experiment_configuration_yaml = sample_experiment_configuration_yaml(
    _sample_output_dir
)