import random
import string
from os import path
from datetime import datetime

from experiments.model import ExperimentConfig, NodeConfigurationModel
from experiments.model.results import *

class ExperimentDataGenerator(object):
    def merged_results(self):
        merged = MergedResults()

        for n in self._nodes_list():
            merged.add_results(n, self.node_result())

        return merged

    def node_result(self) -> NodeResults:
        return NodeResults(
            publications=self.publications_results(),
            usedMechanisms=self.mechanisms()
        )

    def mechanisms(self):
        amount = random.randint(1, 3)

        return [self.random_name() for i in range(amount)]

    def publications_results(self) -> List[PublicationResult]:
        num_publications = random.randint(1, 3)

        return [self.publication_result() for i in range(0, num_publications)]

    def publication_result(self) -> PublicationResult:
        return PublicationResult(
            service= self.service_identifier(),
            interval=self.time_interval()
        )

    def service_identifier(self):
        return {
            'name': self.random_name(),
            'type': self.random_name(min_len=4, max_len=8),
            'providerId': None, #To-DO...
            'instanceId': None #To-DO...
        }

    def time_interval(self):
        seconds_in_hour =  60*60
        date_diff = random.randint(1, 1 * seconds_in_hour)

        mindatetime = datetime.now().replace(hour=0, minute=0,second=0,microsecond=0)

        start = self.datetime(mindatetime=mindatetime)
        end = datetime.fromtimestamp(start.timestamp() + date_diff)

        return {
            'start': start.isoformat(),
            'end': end.isoformat()
        }

    def datetime(self, maxdatetime=None, mindatetime=None):
        if maxdatetime is None:
            maxdatetime = datetime.now()

        mintimestamp = mindatetime.timestamp() if mindatetime else 0

        t = random.randint(mintimestamp, int(maxdatetime.timestamp()))

        return datetime.fromtimestamp(t)

    def experiment_config(self, nodes=None, network_file=None)->ExperimentConfig:
        if nodes is None:
            nodes = self._nodes_list()

        nodes_dict = {n:self._node_config(n) for n in nodes}
        config_dict = {
            'nodes': nodes_dict
        }

        return ExperimentConfig(
            nodes=nodes_dict,
            network_file=network_file if network_file else 'network.xml'
        )

    def _nodes_list(self, num_nodes:int=None):
        if num_nodes is None:
            num_nodes = random.randint(1, 5)

        return [f'n{i}' for i in range(1,num_nodes + 1)]

    def _node_config(self, node_name) -> NodeConfigurationModel:
        # To-Do: populate node configuration

        return NodeConfigurationModel(
            output_dir = self.random_path(),
            results_file = self.random_name() + '.yaml'
            # ...
        )

    def random_path(self, num_segments=None, min_segments=3, max_segments=7):
        if num_segments is None:
            num_segments = random.randint(min_segments, max_segments)

        segments = [self.random_name() for i in range(0, num_segments)]

        return path.join(*segments)

    def random_name(self, str_len=None, min_len=4, max_len=12, allowed_chars=None):
        if str_len is None:
            str_len = random.randint(min_len, max_len)

        if allowed_chars is None:
            allowed_chars = string.ascii_letters

        str_chars = [random.choice(allowed_chars) for x in range(str_len)]

        return ''.join(str_chars)
