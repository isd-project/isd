from pathlib import Path

import shutil


class ExperimentDirectories(object):
    def __init__(self, tmp_path:Path):
        self.experiment_path = tmp_path / 'experiment'
        self.experiment_path.mkdir(exist_ok=True)

        self.out_path = self.experiment_path / 'output'
        self.out_path.mkdir(exist_ok=True)

    def save_experiment_config(self, config, config_filename=None):
        if not config_filename:
            config_filename = 'config.yaml'

        config_path = str(self.experiment_path / config_filename)

        with open(config_path, 'w') as f:
            f.write(config)

        return config_path

    def copy_keysfile(self, keysdir, dst_dir=None):
        if not dst_dir:
            dst_dir = self.out_path

        dst_dir = Path(dst_dir)
        dst_file = dst_dir / 'keys.json'
        keyfile = Path(keysdir) / 'keys.json'

        shutil.copy(str(keyfile), str(dst_file))

        return str(dst_dir)