from experiments.model.results import MergedResults, NodeResults

from dataclasses import asdict
from pathlib import Path

from . import yaml_parser


class ResultsPersistence(object):
    def __init__(self):
        pass

    def read_node_results(self, results_path:str) -> NodeResults:
        content = Path(results_path).read_text()

        return self.parse_node_results(content)

    def parse_node_results(self, results_str:str) -> NodeResults:
        return self._parse_type(results_str, NodeResults)

    def save_node_results(self, results:NodeResults, results_path:str):
        content = self.render_node_results(results)
        self._save_content(content, results_path)

    def render_node_results(self, results:NodeResults) -> str:
        return self._render_type(results)


    def read_results(self, results_path:str) -> MergedResults:
        content = Path(results_path).read_text()

        return self.parse_results(content)

    def parse_results(self, results_str:str)->MergedResults:
        return self._parse_type(results_str, MergedResults)

    def save_results(self, results:MergedResults, results_path:str):
        content = self.render_results(results)
        self._save_content(content, results_path)

    def _save_content(self, content:str, results_path:str):
        p = Path(results_path)
        p.write_text(content)

    def render_results(self, results:MergedResults)->str:
        return self._render_type(results)

    # -------------------------------------------------

    def _render_type(self, value):
        return yaml_parser.render_yaml(asdict(value))

    def _parse_type(self, results_str:str, objtype):
        dict_results = yaml_parser.load_yaml(results_str)

        return objtype.from_dict(dict_results)

