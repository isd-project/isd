import yaml
import yaml.constructor
from yaml.nodes import ScalarNode

import binascii, base64

def load_yaml(text):
    return yaml.safe_load(text)

def render_yaml(obj):
    return yaml.safe_dump(obj)

# Code below is adapted from pyyaml.
# The goal is to handle padding missing, that python does not accept

def parse_yaml_binary__fixed(loader, node):
    try:
        value = construct_scalar(node).encode('ascii')
    except UnicodeEncodeError as exc:
        raise ConstructorError(None, None,
                "failed to convert base64 data into ascii: %s" % exc,
                node.start_mark)
    try:
        '''
        Python base64 trim the padding, but does not support padding missing,
        so we add the max padding for base64 (2), even if not needed
        '''
        value = value + b'=='

        return base64.decodebytes(value)
    except binascii.Error as exc:
        raise ConstructorError(None, None,
                "failed to decode base64 data: %s" % exc, node.start_mark)


def construct_scalar(node):
    if not isinstance(node, ScalarNode):
        raise ConstructorError(None, None,
                "expected a scalar node, but found %s" % node.id,
                node.start_mark)
    return node.value


yaml.constructor.SafeConstructor().add_constructor(
    'tag:yaml.org,2002:binary', parse_yaml_binary__fixed
)