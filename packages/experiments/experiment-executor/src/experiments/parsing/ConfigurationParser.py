from experiments.model import ExperimentConfig

import yaml
import dataclasses
from pathlib import Path

class ConfigurationParser(object):

    def load_configuration(self, config_path):
        content = Path(config_path).read_text()

        return self.parse_configuration(content)

    def parse_configuration(self, config_yaml:str):
        obj = yaml.safe_load(config_yaml)

        return ExperimentConfig.from_dict(obj)

    def save_configuration(self, config:ExperimentConfig, savepath:str):
        content = self.render_configuration(config)

        p = Path(savepath)
        p.parent.mkdir(exist_ok=True, parents=True)
        p.write_text(content)

    def render_configuration(self, config:ExperimentConfig) -> str:
        config_dict = config.asdict()

        return  yaml.safe_dump(config_dict)