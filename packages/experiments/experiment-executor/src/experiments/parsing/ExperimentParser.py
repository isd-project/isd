from experiments.model import Experiment, ExperimentParameters

import yaml



class ExperimentParser(object):

    def parse_experiment(self, experiment_repr):
        return Experiment.from_dict(yaml.safe_load(experiment_repr))

    def save_experiment(self, exp_params:ExperimentParameters):
        pass