from experiments.model import ExperimentConfig
from experiments.model.results import MergedResults
from .ResultsPersistence import ResultsPersistence
from experiments.utils.filesystem import Fs

from os import path
import logging


class ResultsCollector(object):
    def __init__(self, results_persistence=None, fs=None):
        if not fs: fs = Fs()
        if results_persistence is None:
            results_persistence = ResultsPersistence()

        self.fs = fs
        self.results_persistence = results_persistence

    def collect_results(self, config:ExperimentConfig) -> MergedResults:
        merged = MergedResults({})

        for n in config.get_nodes():
            results_path = config.get_results_path_for(n)

            if self.fs.exists(results_path):
                results = self.results_persistence.read_node_results(results_path)
                merged.add_results(n, results)
            else:
                logging.warning(
                    f'Collecting results: missing file ({results_path})')

        return merged

    def save_results(self, results:MergedResults, results_path:str):
        self.results_persistence.save_results(results, results_path)