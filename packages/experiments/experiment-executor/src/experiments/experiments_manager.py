from experiments.executor import ExperimentExecutor, ExecutionsGenerator
from experiments.executor import ExecutionOptions
from experiments.utils.filesystem import Fs
from experiments.model import ExperimentScript

import contextlib
import os
from os import path
from pathlib import PurePath

import logging

from io import StringIO
import traceback
import random


class ExperimentsManager(object):

    def __init__(self, exec_config=None, executor=None, fs=None):
        if not fs: fs = Fs()
        if not executor: executor = ExperimentExecutor(exec_config, fs=fs)

        self.executor = executor
        self.fs = fs

        self.max_tries = 3
        self.error_notifier = ErrorNotifier(self.fs)

        self.execution_options:ExecutionOptions = None

    def close(self):
        pass

    def set_options(self, options:ExecutionOptions):
        self.execution_options = options

    def execute_experiments(self,
        *experiments, output_dir=None, mechanism_variations=None
    ):
        def shuffled(l):
            l = list(l)
            random.shuffle(l)
            return l

        shuffled_experiments = shuffled(experiments)
        logging.info('Executing:\n\t-' + '\n\t-'.join(shuffled_experiments))

        total=len(experiments)
        for i in range(len(shuffled_experiments)):
            exp_file = shuffled_experiments[i]

            count = i + 1
            logging.info('---------------------------------------------------')
            logging.info(f'executing: {exp_file} ({count}/{total})')
            self.execute_experiment(
                    exp_file, output_dir=output_dir, mechanism_variations=mechanism_variations
                )
            logging.info(f'finished: {exp_file}  ({count}/{total})')
            logging.info('---------------------------------------------------')

    def execute_experiment(self,
        experiment_file, output_dir=None, mechanism_variations=None
    ):
        experiment_file = os.path.abspath(experiment_file)
        output_dir      = path.abspath(output_dir) if output_dir else None

        scripts = ExecutionsGenerator(mechanism_variations).executions_for(
            experiment_file, output_dir=output_dir
        )

        with self.changedir(experiment_file):
            for script in scripts:
                logging.info(f'starting execution for mechanisms: {script.mechanisms}')
                self.try_execute(script, max_tries=self.max_tries)

    def try_execute(self, script, max_tries):
        for i in range(max_tries):
            try:
                self.executor.execute(script, options=self.execution_options)
                return True
            except Exception as e:
                self.notify_error(script, e, i)

    def notify_error(self, script, error, trycount):
        self.error_notifier.on_error(script, error, trycount)


    @contextlib.contextmanager
    def changedir(self, experiment_file):
        initial_dir = os.getcwd()

        p = PurePath(experiment_file)
        experiment_dir = os.path.abspath(p.parent)

        self.fs.chdir(experiment_dir)

        try:
            yield experiment_dir
        finally:
            self.fs.chdir(initial_dir)


class ErrorNotifier(object):
    def __init__(self, fs):
        self.fs = fs

    def on_error(self, script, error, trycount):
        logging.error(f'Experiment failed in try {trycount+1}. Configured with: {script}', exc_info=error)

        self.fs.save_content(
            self._error_message(script, trycount, error), script.error_file(trycount))

    def _error_message(self, script, trycount, error):
        buffer = StringIO()

        buffer.write(f'Experiment failed in try {trycount+1}.\n')
        buffer.write(f'Configuration: {script}\n\n')
        buffer.write(f'Exception:\n')

        e = error
        buffer.write(''.join(traceback.format_exception(type(e), e, e.__traceback__)))

        return buffer.getvalue()