import dataclasses
from dataclasses import dataclass, field
from typing import List, Dict
from datetime import timedelta

import experiments
from experiments.model.nodes_config import SearchSpec, ServicePublicationModel
from experiments.model.topology_models import Topology


@dataclass
class HostInfo(object):
    name:str=None
    keyid:str=None
    keyid_fmt:str=None

@dataclass
class ServicePublication(ServicePublicationModel):
    node:HostInfo=None

    def service_id(self):
        if self.name is None \
            or self.type is None \
            or self.node is None\
            or self.node.keyid is None:
            return None

        srv_id = f'srv://{self.name}.{self.type}.{self.node.keyid}'

        if self.node.keyid_fmt is not None:
            srv_id += f';fmt={self.node.keyid_fmt}'

        return srv_id


@dataclass
class NodeOperations(object):
    node:HostInfo=None
    publications:List[ServicePublication] = field(default_factory=list)
    searches:List[SearchSpec] = field(default_factory=list)

@dataclass
class ExperimentOperations(object):
    nodes_operations:Dict[str, NodeOperations] = field(
        default_factory=dict
    )

    def get_publications_for(self, node_name):
        return self._get_operations_for(node_name).publications

    def get_searches_for(self, node_name):
        return self._get_operations_for(node_name).searches

    def _get_operations_for(self, node_name):
        return self.nodes_operations.get(node_name, NodeOperations())


@dataclass
class ExperimentParameters(object):
    num_hosts:int=0
    num_nets:int=None
    num_services:int=0
    num_searches:int=0
    num_localizations:int=0
    delay_interval:List[timedelta]=None
    srvtypes:List[str]=None

    def __post_init__(self):
        self.delay_interval = self._parse_interval(self.delay_interval)

    def _parse_interval(self, delay_interval):
        if delay_interval is None: return None

        def parse_timedelta(tdelta):
            if isinstance(tdelta, timedelta):
                return tdelta

            return timedelta(seconds=tdelta)

        return [
            parse_timedelta(tdelta_seconds) for tdelta_seconds in delay_interval
        ]

    def asdict(self):
        def serialize_value(k, value):
            if value is None: return None

            if k == 'delay_interval':
                return [
                    tdelta.total_seconds() if tdelta is not None else None
                    for tdelta in value
                ]
            return value

        def make_dict(keyvalues):
            return {
                k:serialize_value(k, v)
                    for k,v in keyvalues
            }

        return dataclasses.asdict(self, dict_factory=make_dict)
