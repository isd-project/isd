from experiments.executor import ExperimentExecutable, ExecutionConfig, ExecutionOptions
from experiments import ExperimentsManager

from .core_emulator import CoreEmulator


class CoreExperimentsExecutor(object):

    def __init__(self,
        node_executable:ExperimentExecutable,
        bootstrap_executable:ExperimentExecutable
    ):
        # TO-DO: default ExperimentExecutable
        self.emulator = CoreEmulator()
        self.exec_config = ExecutionConfig.from_executable(
            node_executable=node_executable,
            bootstrapper=bootstrap_executable,
            emulator=self.emulator
        )
        self.exp_manager = ExperimentsManager(
            exec_config=self.exec_config
        )

    def close(self):
        self.emulator.close_session()

    def set_options(self, options:ExecutionOptions):
        self.exp_manager.set_options(options)

    def execute_experiments(self,
        *experiments, output_dir=None, mechanism_variations=None
    ):
        self.exp_manager.execute_experiments(
            *experiments,
            output_dir=output_dir,
            mechanism_variations=mechanism_variations
        )

    def execute_experiment(self,
        experiment_file, output_dir=None, mechanism_variations=None
    ):
        self.exp_manager.execute_experiment(
                experiment_file,
                output_dir=output_dir,
                mechanism_variations=mechanism_variations
            )
