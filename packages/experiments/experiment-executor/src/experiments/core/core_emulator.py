from core_robot import Core

import logging
import contextlib

class CoreEmulator(object):
    def __init__(self, core=None):
        self.core = core if core else Core()

    @contextlib.contextmanager
    def session_open(self, network_file):
        self.start_session(network_file)
        try:
            yield
        finally:
            self.close_session()

    def start_session(self, network_file):
        logging.info(f'starting session for file: {network_file}')
        self.core.open_network_file(network_file)
        logging.info(f'started session')

    def close_session(self):
        self.core.close()


    def start_cmd_in_node(self, *cmd_args, **kwargs):
        cmd_name = kwargs.get('proc', self._gen_command_name(*cmd_args))
        kwargs['proc'] = cmd_name

        logging.debug(f'[CoreEmulator] running command named "{cmd_name}" in node: {kwargs.get("node")}')

        return self.core.start_cmd_in_node(*cmd_args, **kwargs)

    def _gen_command_name(self, *cmd_args):
        cmd_name = '-'.join(cmd_args[:3]).replace(' ', '')

        return cmd_name