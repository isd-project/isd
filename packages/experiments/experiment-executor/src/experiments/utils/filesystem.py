import os
from os import path
from pathlib import Path


class Fs(object):
    def ensure_dir(self, dir_path):
        self.makedirs(dir_path)

    def makedirs(self, some_path):
        os.makedirs(some_path, exist_ok=True)

    def exists(self, some_path):
        return path.exists(some_path)

    def joinpaths(self, *path_segments):
        return path.join(*path_segments)

    def listdir(self, dir_path):
        return os.listdir(dir_path)

    def chdir(self, dir_path):
        os.chdir(dir_path)

    def save_content(self, content:str, out_path:str, mkdir=True, mkparents=True):
        p = Path(out_path)
        if mkdir:
            p.parent.mkdir(parents=mkparents, exist_ok=True)
        p.write_text(content)