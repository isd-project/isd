from dataclasses import dataclass
from typing import Dict, List
from os import path

from .experiment_config import ExperimentConfig

class ExperimentScript(object):
    def __init__(self, config_path:str,
                        mechanisms:List[str]=None,
                        output_dir:str=None,
                        experiment_config:ExperimentConfig=None):
        self.config_path = config_path
        self.mechanisms = mechanisms
        self.config = experiment_config
        self._output_dir = output_dir

    def __eq__(self, other):
        if other is None: return self is None

        return self.config_path == other.config_path \
            and self.mechanisms == other.mechanisms

    def has_loaded(self):
        return self.config is not None

    def load_configuration(self, config_parser):
        if self.has_loaded(): return

        config = config_parser.load_configuration(self.config_path)

        if self._output_dir:
            config.output_dir = self.output_dir

        self.config = config
        self.experiment_nodes = list(config.nodes.keys())

        return self.config

    def get_config_for(self, node_name) -> str:
        return self.config_path

    def nodes(self):
        if self.config:
            return self.config.get_nodes()

        return None

    @property
    def output_dir(self):
        segments = [self._output_dir] if self._output_dir is not None else []

        subdir = self._experiment_subdir()
        if self._output_dir and subdir:
            segments.append(subdir)

        if self.mechanisms:
            segments.append(self._mechanisms_subdir())

        return path.join(*segments) if segments else None

    def experiment_dir(self):
        return path.dirname(self.config_path)

    def _experiment_subdir(self):
        if not self.config_path: return None

        experiment_filename = path.basename(self.config_path)
        experiment_name     = path.splitext(experiment_filename)[0]

        return experiment_name

    def _mechanisms_subdir(self):
        return '+'.join(self.mechanisms)

    def results_path(self):
        if self.config is None:
            return None

        return self.config.results_path()

    def error_file(self, trynumber=0):
        outdir = self.config.output_dir if self.config else self.output_dir

        return path.join(outdir, f'experiment-failure{trynumber}.txt')

    def network_file(self):
        return None if not self.config else self.config.network_file