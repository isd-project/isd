import dataclasses
from dataclasses import dataclass
from typing import Dict, List
from os import path

from experiments.utils import nested_dataclass
from .topology_models import Topology
from experiments.generator.models import ExperimentParameters
from .nodes_config import *


@nested_dataclass
class ExperimentConfig(object):
    nodes: Dict[str, NodeConfigurationModel]
    output_dir: str = ''
    results_file: str = 'experiment_results.yaml'
    network_file:str=None
    keysdir:str=None
    bootstrap_address:str=None

    params:ExperimentParameters = None
    topology:Topology = None

    @classmethod
    def from_dict(cls, dict_values):
        fields = [f.name for f in dataclasses.fields(cls)]
        dict_values = {k:v for k,v in dict_values.items() if k in fields}

        dict_values['topology'] = Topology.from_dict(dict_values.get('topology', None))

        return ExperimentConfig(**dict_values)

    def asdict(self):
        return dataclasses.asdict(self, dict_factory=self.asdict_factory)

    def asdict_factory(self, kvalues):
        def get_value(k, v):
            if k in ('topology', 'params') and v is not None:
                return getattr(self, k).asdict()
            return v

        return {
            k:get_value(k, v) for k,v in kvalues
        }

    def __post_init__(self):
        self.nodes = {
            n: self._make_node_config(v) for n, v in self.nodes.items()
        }

    def _make_node_config(self, node_config):
        if isinstance(node_config, NodeConfigurationModel):
            return node_config

        return NodeConfigurationModel.from_dict(node_config)

    def results_path(self):
        return path.join(self.output_dir, self.results_file)

    def get_nodes(self):
        if not self.nodes:
            return None

        return list(self.nodes.keys())

    def get_results_path_for(self, node_name):
        node_conf = self.nodes.get(node_name, None)

        result_path = node_conf.output_file() if node_conf else None

        if result_path and self.output_dir:
            result_path = path.join(self.output_dir, result_path)

        return result_path