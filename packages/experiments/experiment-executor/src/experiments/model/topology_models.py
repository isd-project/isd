import dataclasses
from dataclasses import dataclass, field
from typing import Dict, List, Set
from enum import Enum, auto

import ipaddress

from experiments.utils import nested_dataclass


@dataclass
class IpPrefixes(object):
    ip4: ipaddress.IPv4Network = None
    ip6: ipaddress.IPv6Network = None

    def __post_init__(self):
        self.ip4 = self._as_ipnetwork(self.ip4)
        self.ip6 = self._as_ipnetwork(self.ip6)

    def _as_ipnetwork(self, ip_network:str):
        if isinstance(ip_network, str):
            ip_network = ipaddress.ip_network(ip_network)

        return ip_network

    @classmethod
    def from_dict(self, dict_value):
        return IpPrefixes(**dict_value)

    def asdict(self):
        d = {}

        if self.ip4:
            d['ip4'] = str(self.ip4)
        if self.ip6:
            d['ip6'] = str(self.ip6)

        return d

class NetTypes(Enum):
    LAN = 1
    WAN = 2

    def label(self):
        return {
            1: 'lan',
            2: 'wan'
        }.get(self.value)

@dataclass
class Network(object):
    name: str = None
    nodes: Set[str] = field(default_factory=set)
    nettype: NetTypes = None
    ip_prefixes: IpPrefixes = field(default=None, hash=False)

    def __post_init__(self):
        if isinstance(self.nodes, list):
            self.nodes = set(self.nodes)
        if isinstance(self.nettype, str):
            self.nettype = NetTypes[self.nettype.upper()]
        if isinstance(self.ip_prefixes, dict):
            self.ip_prefixes = IpPrefixes.from_dict(self.ip_prefixes)

    def asdict(self):
        d = dataclasses.asdict(self)
        d['ip_prefixes'] = self.ip_prefixes.asdict() \
                                    if self.ip_prefixes else None
        d['nettype'] = self.nettype.label() if self.nettype else None

        def make_dict(keyvalues):
            d = dict()

            for k, v in keyvalues:
                if k == 'ip_prefixes':
                    v = self.ip_prefixes.asdict() if self.ip_prefixes else None
                elif k == 'nettype':
                    v = self.nettype.label() if self.nettype else None
                elif k == 'nodes':
                    v = list(self.nodes)

                d[k] = v

            return d

        return dataclasses.asdict(self, dict_factory=make_dict)

class NodeType(Enum):
    host = 1
    router = 2
    switch = 3
    server = 4

    def label(self):
        return self.name

    @classmethod
    def from_name(cls, name):
        return NodeType[name]

@dataclass(unsafe_hash=True)
class Node(object):
    id:int=None
    name: str = None
    nodetype: NodeType = None
    networks: List[str] = field(default_factory=list, hash=False)
    extra_services: List[str] = field(default=None, hash=False)

    def __post_init__(self):
        if isinstance(self.nodetype, str):
            self.nodetype = NodeType.from_name(self.nodetype)

    def is_a(self, nodetype: NodeType):
        return self.nodetype == nodetype

    def add_network(self, net):
        self.networks.append(net.name)
        net.nodes.add(self.name)

    def asdict(self):
        def make_dict(keyvalues):
            return {
                k:v if k != 'nodetype' else self.nodetype.label()
                    for k,v in keyvalues
            }

        return dataclasses.asdict(self, dict_factory=make_dict)


@dataclass
class TopologySpec(object):
    networks: int
    hosts: int

@nested_dataclass
class Topology(object):
    networks: List[Network] = field(default_factory=list)
    nodes: Set[Node] = field(default_factory=set)

    @classmethod
    def as_topology(cls, topology_value):
        if isinstance(topology_value, Topology):
            return topology_value
        if isinstance(topology_value, dict):
            return cls.from_dict(topology_value)

        return None

    @classmethod
    def from_dict(self, topology_dict):
        if topology_dict is None: return None

        for k, cls in (('networks', Network), ('nodes', Node)):
            topology_dict[k] = [
                cls(**dict_value) for dict_value in topology_dict.get(k, [])
            ]
        topology_dict['nodes'] = set(topology_dict['nodes'])

        return Topology(**topology_dict)

    def asdict(self):
        d = dict()
        d['networks'] = [net.asdict() for net in self.networks if net]
        d['nodes']    = [n.asdict() for n in self.nodes if n]

        return d

    def hosts(self):
        return self._filter_node(nodetype=NodeType.host)

    def routers(self):
        return self._filter_node(nodetype=NodeType.router)

    def switches(self):
        return self._filter_node(nodetype=NodeType.switch)

    def servers(self):
        return self._filter_node(nodetype=NodeType.server)

    def lan_networks(self):
        return [net for net in self.networks if net.nettype == NetTypes.LAN]

    def _filter_node(self, nodetype:NodeType):
        return [n for n in self.nodes if n.is_a(nodetype)]

    def add_network(self, network):
        self.networks.append(network)

    def add_node(self, node):
        self.nodes.add(node)

    def get_network(self, net_name):
        for net in self.networks:
            if net.name == net_name:
                return net

        return None

    def get_node(self, node_name):
        for node in self.nodes:
            if node.name == node_name:
                return node

        return None