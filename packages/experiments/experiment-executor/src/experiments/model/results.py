from dataclasses import dataclass, field, asdict
from typing import Dict, List

from experiments.utils import nested_dataclass
from .experiment_config import SearchSpec

@nested_dataclass
class PublicationResult(object):
    service: object
    interval: object

@nested_dataclass
class SearchResult(object):
    search: SearchSpec
    found: List[object] = field(default_factory=list)
    error: object = None
    finishedTime: object = None #interval

@nested_dataclass
class NodeResults(object):
    usedMechanisms: List[str] = None
    publications: List[PublicationResult] = field(default_factory=list)
    searches: List[SearchResult] = field(default_factory=list)

    @classmethod
    def from_dict(self, dict_values):
        return NodeResults(**dict_values)

    def __post_init__(self):
        self.publications = self._from_dict_values(
            self.publications, PublicationResult
        )
        self.searches = self._from_dict_values(
            self.searches, SearchResult
        )

    def _from_dict_values(self, values, ModelClass):
        return [self._from_dict(v, ModelClass) for v in values]

    def _from_dict(self, value, ModelClass):
        if isinstance(value, dict):
            return ModelClass(**value)

        return value

@nested_dataclass
class MergedResults(object):
    nodes: Dict[str, NodeResults] = field(default_factory=dict)

    def add_results(self, node_name, results:NodeResults):
        self.nodes[node_name] = results

    def results(self):
        return self.nodes.values()


    @classmethod
    def from_dict(self, dict_values):
        return MergedResults(**dict_values)

    def as_dict(self):
        return asdict(self)

    def __post_init__(self):
        self.nodes = {n:self._node_result(v) for n, v in self.nodes.items()}

    def _node_result(self, value):
        if isinstance(value, dict):
            return NodeResults(**value)

        return value
