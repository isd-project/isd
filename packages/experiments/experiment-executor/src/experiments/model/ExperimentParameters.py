from dataclasses import dataclass

@dataclass
class ExperimentParameters(object):
    num_nodes: int
    num_queries: int