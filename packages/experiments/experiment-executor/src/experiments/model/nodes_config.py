from dataclasses import dataclass
from typing import List
from os import path

from experiments.utils import nested_dataclass

@dataclass
class ServicePublicationModel(object):
    name:str=None
    type:str=None
    delay:int=0
    port:int=None

    @classmethod
    def service_url_from(cls, name, type, keyid, keyid_fmt=None):
        if name is None \
            or type is None \
            or keyid is None:
            return None

        srv_id = f'srv://{name}.{type}.{keyid}'

        if keyid_fmt is not None:
            srv_id += f';fmt={keyid_fmt}'

        return srv_id

@dataclass
class SearchSpec(object):
    kind:str
    srvId:str = None
    srvType:str = None
    delay:int = 0

@nested_dataclass
class NodeConfigurationModel(object):
    name:str = None
    keyid: str = None
    keyid_fmt:str = None
    keysdir: str = None
    output_dir: str = None
    results_file: str = None
    publications: List[ServicePublicationModel] = None
    searches: List[SearchSpec] = None

    @classmethod
    def from_dict(self, dict_values):
        node_conf = NodeConfigurationModel()

        for k, v in dict_values.items():
            # allow non specified values
            setattr(node_conf, k, v)

        # TO-DO: convert dict of publications

        return node_conf

    def output_file(self):
        if not self.results_file:
            return None

        segments = [self.output_dir] if self.output_dir else []
        segments.append(self.results_file)

        return path.join(*segments)
