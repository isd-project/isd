from dataclasses import dataclass

from . import ExperimentParameters
from experiments.utils import nested_dataclass

@nested_dataclass
class Experiment(object):
    parameters: ExperimentParameters

    @classmethod
    def from_dict(self, dict_values):
        return Experiment(**dict_values)