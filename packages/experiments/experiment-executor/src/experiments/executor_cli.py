import click

from experiments.core import CoreExperimentsExecutor
from experiments.executor import ExperimentExecutable, ExecutionOptions

import logging


@click.group()
@click.option('-v', '--verbose', 'verbosity', count=True)
@click.option('--logfile', '--lf', 'logfile',
                help='Path to store log output')
@click.pass_context
def main(ctx, verbosity, logfile=None):
    if verbosity or logfile:
        setup_logging(verbosity, logfile)


def avaliable_mechanisms():
    return ['mdns-raw', 'mdns', 'dht', 'mdns+dht']
def default_publication_timeout():
    return 90.0 #seconds
def default_search_timeout():
    return 90.0 #seconds

@main.command()
@click.argument('experiments', nargs=-1)
@click.option('--output-dir','-o',help='directory where the output files will be saved')
@click.option('--mechanism', '-m', 'mechanisms', multiple=True,
    help='specifies the set of mechanism variations to use for execution. To combine multiple mechanisms, use "+". Example: -m mdns+dht. Defaults to all available mechanisms: ' + str(avaliable_mechanisms()))
@click.option('--publication-timeout', '--pt', 'pub_timeout',
    type=click.FLOAT,
    default=default_publication_timeout(),
    help='Max time in seconds to wait for publication stage (on each node)')
@click.option('--search-timeout', '--st', 'search_timeout',
    type=click.FLOAT,
    default=default_search_timeout(),
    help='Max time in seconds to wait for search stage (on each node)')
@click.pass_context
def execute(ctx, experiments, output_dir, mechanisms, pub_timeout=None, search_timeout=None):
    '''Executes one or more given <experiments>.'''

    mechanisms = mechanisms if mechanisms else avaliable_mechanisms()

    click.echo(
'''Execute experiments: [{0}]
    with mechanisms: {1}
'''.format(', '.join(experiments), mechanisms))

    options = ExecutionOptions(
        publication_timeout=pub_timeout,
        search_timeout=search_timeout
    )
    logging.info(f'Execution options:\t\t{options}')

    executor = CoreExperimentsExecutor(
        node_executable='experiment-node',
        bootstrap_executable='dht-bootstrapper'
    )
    executor.set_options(options)

    executor.execute_experiments(
        *experiments, output_dir=output_dir,mechanism_variations=mechanisms
    )

def setup_logging(verbosity, logfile=None):
    level = get_log_level(verbosity)
    logging.getLogger().setLevel(level)

    add_file_handler(logfile)

def add_file_handler(filename):
    if not filename: return

    handler = logging.FileHandler(filename=filename, encoding='utf-8')
    handler.setFormatter(logging.Formatter(
            '[%(asctime)s] %(message)s', datefmt='%H:%M:%S'
        ))
    handler.setLevel(logging.INFO)

    logging.getLogger().addHandler(handler)

def get_log_level(verbosity):
    if verbosity >= 2:
        level = logging.DEBUG
    elif verbosity >= 1:
        level = logging.INFO
    else: #default
        level = logging.WARNING
    return level

if __name__ == '__main__':
    main()