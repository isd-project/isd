from dataclasses import dataclass
from experiments.model import ExperimentScript, NodeType
from experiments.parsing import ConfigurationParser, ResultsCollector

from .NodeExecutor import NodeExecutor
from .NodeExecutorBuilder import NodeExecutorBuilder
from .ExecutionConfig import ExecutionConfig
from .packet_capturer import PacketCapturer

import contextlib
import logging

@dataclass
class ExecutionOptions(object):
    publication_timeout:float=None
    search_timeout:float=None

class ExperimentExecutor(object):

    def __init__(self, exec_config:ExecutionConfig=None,
                       node_builder:NodeExecutorBuilder=None,
                       config_parser:ConfigurationParser=None,
                       results_collector:ResultsCollector=None,
                       packets_capturer=None,
                       fs=None):

        if not node_builder:     node_builder = NodeExecutorBuilder(exec_config)
        if not config_parser:    config_parser = ConfigurationParser()
        if not results_collector:results_collector = ResultsCollector(fs=fs)
        if not packets_capturer :packets_capturer = PacketCapturer(fs=fs)

        self.exec_config = exec_config
        self.node_builder = node_builder
        self.config_parser = config_parser
        self.results_collector = results_collector
        self.packets_capturer = packets_capturer

    def execute(self,
        experiment_script:ExperimentScript, options:ExecutionOptions=None
    ):
        experiment_script.load_configuration(self.config_parser)

        executors = self._build_executors(experiment_script)

        with self.start_session(experiment_script):
            self._start_bootstrapper(experiment_script)
            self._capture_packets(experiment_script)

            try:
                self._execute_experiment(executors, experiment_script, options)
            finally:
                self._stop_packet_capture()

            return self._save_results(experiment_script)


    @contextlib.contextmanager
    def start_session(self, experiment_script):
        with self.emulator().session_open(experiment_script.network_file()):
            yield

    def _execute_experiment(self, executors, experiment_script, options):
        if not options: options = ExecutionOptions()

        logging.info('starting experiments')
        for e in executors:
            e.execute(experiment_script)

        logging.info('waiting for publications')
        for e in executors:
            e.wait_for_publication(timeout=options.publication_timeout)

        logging.info('finished publications, starting next stage')
        for e in executors:
            e.start_next_stage()

        logging.info('waiting nodes to finish')
        for e in executors:
            e.wait_for_finish(timeout=options.search_timeout)

    def emulator(self):
        return self.exec_config.emulator

    def _publication_timeout(self):
        return self.exec

    def _build_executors(self, script):
        return [ self._build_node(n) for n in script.nodes()]

    def _build_node(self, node_name):
        return self.node_builder.build(node_name)

    def _start_bootstrapper(self, script:ExperimentScript):
        if self.exec_config.bootstrapper and script.config.bootstrap_address:
            logging.debug(f'starting bootstrap at: {script.config.bootstrap_address}')

            self.exec_config.start_bootstrap_command(
                ['--bind', script.config.bootstrap_address]
            )

    def _capture_packets(self, script:ExperimentScript):
        if not script.config or not script.config.topology:
            return

        logging.info('start capturing packets')

        for node in script.config.topology.nodes:
            if node.name == 'backbone': continue
            if node.nodetype == NodeType.router:
                self.packets_capturer.monitor_router(
                    node.name, self.emulator(), script.output_dir)

                logging.info(f'monitoring router {node.name}')

        if script.config.bootstrap_address is not None:
            self.packets_capturer.monitor_node(
                'bootstrapper', self.emulator(), script.output_dir)


    def _stop_packet_capture(self):
        self.packets_capturer.stop_capture()

    def _save_results(self, script:ExperimentScript):
        results = self.results_collector.collect_results(script.config)

        self.results_collector.save_results(results, script.results_path())

        return results