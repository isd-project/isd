import random

from experiments.model import ExperimentScript

class ExecutionsGenerator(object):
    def __init__(self, mechanisms):
        self.mechanisms = self._parse_mechanisms(mechanisms)

    def _parse_mechanisms(self, mechanisms):
        if mechanisms is None: return None
        parsed = []

        for m in mechanisms:
            if isinstance(m, str) and '+' in m:
                m = m.split('+')

            parsed.append(m)

        return parsed


    def executions_for(self, experiment_file, output_dir=None):
        scripts = []

        for m in self.list_mechanisms():
            scripts.append(ExperimentScript(
                config_path=experiment_file,
                output_dir=output_dir,
                mechanisms=self._mechanisms_list(m)
            ))

        return scripts

    def _mechanisms_list(self, mechanism):
        if isinstance(mechanism, str): return [mechanism]

        # should be tuple/list
        return mechanism

    def list_mechanisms(self):
        if not self.mechanisms: return [None]

        m = list(self.mechanisms)
        random.shuffle(m)
        return m