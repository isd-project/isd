from experiments.model import ExperimentScript

from . import ExecutionConfig

from commands import Command

class NodeExecutor(object):
    PUBLICATION_MSG = 'publication: done'
    FINISHED_MSG = 'finished execution'

    def __init__(self, exec_config:ExecutionConfig, node:str):
        self.exec_config = exec_config
        self.node = node
        self.running_cmd:Command = None

    def execute(self, script:ExperimentScript, exec_config=None):
        config = script.get_config_for(self.node)

        self._start_node(self.node, script, config, exec_config)

    def _start_node(self, node, script, config, exec_config=None):
        if not exec_config: exec_config = self.exec_config

        cmd_args = self._make_cmd_args(config, script, node)

        cmd = exec_config.start_node_command(cmd_args, node=node)

        self.running_cmd = cmd

    def _make_cmd_args(self, config, script, node):
        args =  [config, node, script.experiment_dir()]

        if script.mechanisms:
            for m in script.mechanisms:
                args.extend(('-m', m))

        if script.output_dir:
            args.extend(('-o', script.output_dir))

        return args

    def wait_for_publication(self, timeout=None):
        self._wait_for_message(self.PUBLICATION_MSG, timeout=timeout)

    def start_next_stage(self):
        self.running_cmd.sendline('start next stage')

    def wait_for_finish(self, timeout=None):
        self._wait_for_message(
                [self.FINISHED_MSG, Command.EOF], timeout=timeout
            )

    def _wait_for_message(self, msg, timeout=None):
        self.running_cmd.wait_for_message(msg, timeout=timeout)