from .NodeExecutor import NodeExecutor
from .ExecutionConfig import ExecutionConfig


class NodeExecutorBuilder(object):
    def __init__(self, executionConfig:ExecutionConfig):
        self.exec_config = executionConfig

    def build(self, node_name):
        return NodeExecutor(self.exec_config, node_name)