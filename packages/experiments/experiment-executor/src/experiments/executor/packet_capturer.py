from .ExecutionConfig import ExperimentExecutable
from experiments.utils.filesystem import Fs

from os import path
import logging


class PacketCapturer(object):

    def __init__(self, tcpdump=None, fs = None):
        if not fs:      fs = Fs()
        if not tcpdump: tcpdump = TcpdumpCommand()

        self.tcpdump = tcpdump
        self.fs = fs
        self.capture_cmds = []

    def stop_capture(self):
        cmds = self.capture_cmds
        self.capture_cmds = []

        for cmd in cmds:
            if cmd:
                cmd.close(force=False)

    def monitor_router(self, router_name, emulator, output_dir, ifaces=None):
        if not ifaces:
            ifaces = ['eth0', 'eth1']

        self.monitor_node(router_name, emulator, output_dir, ifaces = ifaces)

    def monitor_node(self, node_name, emulator, output_dir, ifaces=None):
        if not ifaces: ifaces = ['eth0']

        self.fs.ensure_dir(output_dir)

        commands = []

        for iface in ifaces:
            cmd = self.tcpdump.monitor_interface(
                    emulator, node_name, iface, condition='tcp or udp' ,
                    output_dir=output_dir
                )

            self.capture_cmds.append(cmd)
            commands.append(cmd)

        return commands

class TcpdumpCommand(object):
    # General command:
    #   tcpdump -i <interface> -w <file> [-Q out] tcp or udp

    def __init__(self, executable=None):
        self.tcpdump = executable if executable \
                        else ExperimentExecutable('tcpdump')

    def monitor_interface(self, emulator, node, iface, output_dir, direction=None, condition=None):
        args = self.capture_args(node, iface, output_dir, direction, condition)

        logging.debug(f'[tcpdump] monitor interface: {args}')

        return self.tcpdump.start_command_in(emulator, args, node)


    def capture_args(
        self, node, iface, output_dir, direction=None, condition=None
    ):
        cap_file = self.capture_file(node, iface, output_dir)

        args = ['-i', iface, '-n', '-w', cap_file]

        if direction:
            args.extend(('-Q', direction))
        if condition:
            args.append(condition)

        return args

    def capture_file(self, node, iface, output_dir):
        return path.join(output_dir, f'{node}_{iface}.pcap')