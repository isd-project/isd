from dataclasses import dataclass
from typing import Dict, Union, List

@dataclass
class ExperimentExecutable(object):
    executable:str
    vm:str=''

    @classmethod
    def make(cls, *args, **kwargs):
        return ExperimentExecutable(*args, **kwargs)

    def __init__(self, executable:Union[str, List], vm:str=None):
        args = None

        if isinstance(executable, ExperimentExecutable):
            vm = executable.vm
            executable = executable.executable
        elif executable and not isinstance(executable, str):
            args = executable
            if len(args) == 0:
                executable = args[0]
            elif len(args) > 1:
                vm = args[0]
                executable = args[1]

        self.executable = executable
        self.vm = vm

    def args_for_command(self, cmd_args):
        args = [self.executable]

        if self.vm:
            args.insert(0, self.vm)

        return args + list(cmd_args)

    def start_command_in(self, emulator, cmd_args, node):
        args = self.args_for_command(cmd_args)

        return emulator.start_cmd_in_node(*args, node=node)

@dataclass
class ExecutionConfig(object):
    node_executable:ExperimentExecutable=None
    bootstrapper:ExperimentExecutable=None
    emulator:object=None

    @classmethod
    def from_executable(cls,
        node_executable:ExperimentExecutable, emulator=None,
        bootstrapper:ExperimentExecutable=None
    ):
        return ExecutionConfig(
            node_executable=node_executable,
            emulator=emulator,
            bootstrapper = bootstrapper
        )

    def __post_init__(self):
        if self.node_executable is not None:
            self.node_executable = ExperimentExecutable(self.node_executable)

        if self.bootstrapper is not None:
            self.bootstrapper = ExperimentExecutable(self.bootstrapper)

    def node_command(self, cmd_args):
        return self.node_executable.args_for_command(cmd_args)

    def start_node_command(self, cmd_args, node):
        return self.node_executable.start_command_in(
            self.emulator, cmd_args, node
        )

    def start_bootstrap_command(self, cmd_args, node='bootstrapper'):
        return self.bootstrapper.start_command_in(self.emulator, cmd_args, node)