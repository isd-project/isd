from unittest.mock import Mock
from os import path

from experiment_testing import ExperimentDataGenerator
from experiments.model import ExperimentScript, ExperimentConfig

import pytest


class ExperimentScriptTest(object):

    def setup_method(self):
        self.config_parser = Mock()

        self.generator = ExperimentDataGenerator()

    def test_load_configuration(self):
        config_path = self.given_a_configuration_with(nodes=['n1', 'n2'])

        script = ExperimentScript(config_path)

        script.load_configuration(self.config_parser)

        assert script.nodes() == ['n1', 'n2']


    def test__load_configuration_with_output_dir(self):
        config_path = self.given_a_configuration_with(
            nodes=['n1', 'n2'], output_dir='config/output/dir',
            config_path='myexperiment.yaml'
        )

        script = ExperimentScript(config_path, output_dir='other/outputdir' )
        script.load_configuration(self.config_parser)

        assert script.config.output_dir == script.output_dir

    def test__derive_results_path(self):
        # Given
        config_path = self.given_a_configuration_with(
            results_file='results.yaml',
            config_path=path.join('anydir', 'basefilename.yaml')
        )

        # When
        script = self.load_script_from_config(
                    config_path=config_path, output_dir='outputdir'
                )

        # Then
        assert script.output_dir == path.join('outputdir', 'basefilename')
        assert script.results_path() == path.join(
                                            script.output_dir, 'results.yaml')

    def test__return_experiment_dir_based_on_configuration_path(self):
        script = ExperimentScript(path.join('experiment_dir', 'config.yaml'))

        assert script.experiment_dir() == 'experiment_dir'

    def test__generate_error_file_path(self):
        script = ExperimentScript('experiment.yaml', output_dir='outdir')

        assert script.error_file(0) == path.join(script.output_dir, 'experiment-failure0.txt')
        assert script.error_file(1) == path.join(script.output_dir, 'experiment-failure1.txt')


    @pytest.mark.parametrize(
        "mechanisms,mechanism_subdir",
        [
            pytest.param(['dht'], 'dht'),
            pytest.param(['dht', 'mdns'], 'dht+mdns'),
        ],
    )
    def test__change_output_dir_based_on_used_mechanisms(
        self, mechanisms, mechanism_subdir
    ):
        script = ExperimentScript(
            'sample-experiment.yaml', output_dir='outdir', mechanisms=mechanisms
        )

        assert script.output_dir == path.join(
                'outdir', 'sample-experiment', mechanism_subdir
            )

    # steps -------------------------------------------------------------

    def given_a_configuration_with(self,
        nodes=None, output_dir=None, results_file=None, config_path=None
    ):
        if not nodes: nodes = ['n1', 'n2']

        self.config = self.generator.experiment_config(nodes=nodes)

        if output_dir:
            self.config.output_dir = output_dir
        if results_file:
            self.config.results_file = results_file

        self.config_parser.load_configuration = Mock(return_value=self.config)

        return config_path if config_path else 'any/path/config.yaml'

    def load_script_from_config(self, config_path, output_dir=None):
        script = ExperimentScript(config_path, output_dir=output_dir)
        script.load_configuration(self.config_parser)

        return script