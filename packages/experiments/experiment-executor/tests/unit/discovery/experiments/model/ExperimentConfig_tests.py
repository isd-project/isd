from experiments.model import ExperimentConfig, NodeConfigurationModel

from os import path


class ExperimentConfigTests(object):
    def test__get_node_results_path(self):
        base_outputdir = 'base/outputdir'
        node_outputdir = 'n0_output_dir'
        results_file = 'n0.results'

        config = ExperimentConfig(
            output_dir=base_outputdir,
            nodes={
                'n0': NodeConfigurationModel(
                    output_dir=node_outputdir,
                    results_file=results_file
                )
            }
        )

        assert config.get_results_path_for('n0') == path.join(
            base_outputdir, node_outputdir, results_file
        )