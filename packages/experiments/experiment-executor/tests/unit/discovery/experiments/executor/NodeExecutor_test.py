import pytest
import random
from os import path
from unittest.mock import Mock

from experiment_testing import ExperimentDataGenerator

import experiments
from experiments.model import ExperimentScript
from experiments.executor import NodeExecutor, ExecutionConfig

from commands import Command


class NodeExecutorTest(object):

    def setup_method(self):
        self.generator = ExperimentDataGenerator()

        self.cmd = Mock()
        self.emu = Mock()
        self.emu.start_cmd_in_node = Mock(return_value=self.cmd)

        self.expected_pubtimeout = None

    def test_execute_with_execution_configuration(self):
        exec_config = Mock()

        self.given_a_configured_executor_with_some_node_name()
        self.given_an_experiment_script()
        self.when_the_script_is_executed_with(exec_config)

        self.should_execute_command_starting_with([
            self.script.config_path, self.node_name,
            self.script.experiment_dir()
        ], exec_config_mock=exec_config)

    def test_start_execution(self):
        self.given_a_configured_executor_with_some_node_name()
        self.given_an_experiment_script()
        self.when_the_script_is_executed()
        self.then_a_command_should_be_executed_with_the_node_configurations()

    def test_start_execution__with_specified_mechanisms(self):
        self.given_a_configured_executor_with_some_node_name()
        self.given_an_experiment_script_with_these_mechanims(['a', 'b', 'c'])
        self.when_the_script_is_executed()
        self.then_the_node_should_be_executed_with_the_given_mechanims()

    def test_start_execution__with_given_output_directory(self):
        self.given_a_configured_executor_with_some_node_name()
        self.given_an_experiment_script_with(
                output_dir = 'example/dir', config_path='myexperiment.yaml'
            )
        self.when_the_script_is_executed()
        self.then_the_node_command_should_contains([
            '-o', 'example/dir/myexperiment'
        ])

    def test_wait_for_publications(self):
        self.given_a_node_has_started_execution()
        self.when_waiting_for_publication()
        self.then_should_wait_for_publication_message_on_command()

    def test_wait_for_publications__with_timeout(self):
        self.given_a_publication_timeout()
        self.given_a_node_has_started_execution()
        self.when_waiting_for_publication()
        self.then_should_wait_for_publication_message_with_timeout()

    def test_start_next_stage(self):
        self.given_a_node_has_finished_publication()
        self.when_starting_the_next_stage()
        self.then_a_text_line_should_be_send_to_the_command()

    def test_wait_for_finish(self):
        self.given_all_execution_stages_were_completed()
        self.when_waiting_for_finish()
        self.then_should_wait_for_finish_message_or_EOF()

    # Steps --------------------------------------------------------

    def given_all_execution_stages_were_completed(self):
        self.given_a_node_has_started_execution()

        # the other stages does not need to be called for the tests

    def given_a_node_has_finished_publication(self):
        self.given_a_node_has_started_execution()
        # For the test, we do not need to wait for publication

    def given_a_node_has_started_execution(self):
        self.given_a_configured_executor_with_some_node_name()
        self.given_an_experiment_script()
        self.when_the_script_is_executed()

    def given_a_configured_executor_with_some_node_name(self):
        self.node_name = 'n1'
        self.exec_config = ExecutionConfig(
            emulator=self.emu,
            node_executable=['nodejs', 'experiment-node.js']
        )
        self.executor = NodeExecutor(self.exec_config, node=self.node_name)

        return self.executor

    def given_a_publication_timeout(self):
        self.expected_pubtimeout = random.randint(10, 120)

    def given_an_experiment_script_with_these_mechanims(self, mechanisms):
        self.given_an_experiment_script(mechanisms=mechanisms)

    def given_an_experiment_script(self, mechanisms=None):
        self.given_an_experiment_script_with(mechanisms=mechanisms)

    def given_an_experiment_script_with(self,
        mechanisms=None, output_dir=None, config_path=None
    ):
        if not config_path:
            config_path = path.join('exec_dir', 'nodes_config.yaml')

        self.config_mechanisms = mechanisms
        self.script = ExperimentScript(
            config_path,
            mechanisms=mechanisms,
            experiment_config = self.generator.experiment_config(nodes = [
                'n1', 'n2'
            ]),
            output_dir=output_dir
        )

    def when_the_script_is_executed(self):
        self.when_the_script_is_executed_with(None)

    def when_the_script_is_executed_with(self, exec_config=None):
        self.executor.execute(self.script, exec_config)

    def when_waiting_for_publication(self):
        self.executor.wait_for_publication(timeout=self.expected_pubtimeout)

    def when_starting_the_next_stage(self):
        self.executor.start_next_stage()

    def when_waiting_for_finish(self):
        self.executor.wait_for_finish()

    def then_a_command_should_be_executed_with_the_node_configurations(self):
        expected_config = self.script.get_config_for(self.node_name)

        self.should_start_node_command(
            expected_config, self.node_name, self.script.experiment_dir(),
                                    node=self.node_name, once=True)

    def then_the_node_should_be_executed_with_the_given_mechanims(self):
        mechanisms = self.mechanisms_in_last_start_command()

        assert sorted(self.config_mechanisms) == sorted(mechanisms)

    def then_should_wait_for_publication_message_with_timeout(self):
        self.then_should_wait_for_publication_message_on_command()

    def then_should_wait_for_publication_message_on_command(self):
        publication_msg = NodeExecutor.PUBLICATION_MSG

        self.cmd.wait_for_message.assert_called_once_with(
            publication_msg, timeout=self.expected_pubtimeout)

    def then_a_text_line_should_be_send_to_the_command(self):
        self.cmd.sendline.assert_called_once()

    def then_should_wait_for_finish_message_or_EOF(self):
        msg = NodeExecutor.FINISHED_MSG

        self.cmd.wait_for_message.assert_called_once_with(
            [msg, Command.EOF], timeout=None
        )

    # Helpers --------------------------------------------------------

    def should_execute_command_starting_with(
            self, expected_args, exec_config_mock
        ):
        cmd_args = self.last_args_in(exec_config_mock.start_node_command)

        assert self.has_subsequence(cmd_args[0], expected_args)

    def should_start_node_command(self, *args, node=None,  once=False):
        cmd_args = self.exec_config.node_command(args)

        method = self.emu.start_cmd_in_node
        if once:
            method.assert_called_once_with(*cmd_args, node=node)
        else:
            method.assert_any_call(*cmd_args, node=node)

    def mechanisms_in_last_start_command(self):
        cmd_args = self.last_start_command_args()

        mechanisms = []

        for i in range(len(cmd_args)):
            if(cmd_args[i] in ('-m', '--mechanism')):
                mechanisms.append(cmd_args[i + 1])

        return mechanisms

    def last_start_command_args(self):
        return self.last_args_in(self.emu.start_cmd_in_node)

    def last_args_in(self, mock_method):
        last_call = mock_method.mock_calls

        cmd_args = last_call[0][1]
        return cmd_args

    def then_the_node_command_should_contains(self, expected_args):
        last_cmd = self.emu.start_cmd_in_node.mock_calls

        cmd_args = last_cmd[0][1]

        assert self.has_subsequence(cmd_args, expected_args), f'sequence {expected_args} was not found in {cmd_args}'

    def has_subsequence(self, sequence, subsequence):
        count = 0

        for i in range(len(sequence)):
            if sequence[i] == subsequence[count]:
                count += 1
                if count == len(subsequence):
                    return True
            else:
                count = 0

        return False