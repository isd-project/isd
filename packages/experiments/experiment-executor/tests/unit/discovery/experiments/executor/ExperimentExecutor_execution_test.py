import pytest
from unittest import mock
from unittest.mock import Mock, ANY

from experiment_testing import ExperimentDataGenerator
from experiment_testing.mockups.emulator import mock_emulator

import experiments
from experiments.model import ExperimentScript, ExperimentConfig
from experiments.model.topology_models import Topology, Node, NodeType
from experiments.executor import *


class ExperimentExecutorExecutionTest(object):

    def setup_method(self):
        self.generator = ExperimentDataGenerator()

        self.node_builder = Mock(NodeExecutorBuilder)
        self.node_builder.build = Mock(side_effect=self.build_node)
        self.node_executors = {}

        self.config_parser = Mock()
        self.results_collector = Mock()
        self.emulator = mock_emulator()
        self.exec_config = ExecutionConfig(
            emulator=self.emulator,
            node_executable='node-command',
            bootstrapper='bootstrapper'
        )
        self.packets_capturer = Mock()

        self.collected_results = results = self.generator.merged_results()
        self.results_collector.collect_results.return_value = results

        self.executor = ExperimentExecutor(
                exec_config=self.exec_config,
                node_builder=self.node_builder,
                config_parser = self.config_parser,
                results_collector = self.results_collector,
                packets_capturer = self.packets_capturer
        )

        self.config:ExperimentConfig = None


    def build_node(self, node_name):
        self.node_executors[node_name] = executor = Mock(NodeExecutor)

        return executor

    def executors(self):
        return self.node_executors.values()

    # tests -------------------------------------------------------------

    def test__load_nodes_configuration__on_start(self):
        config = self.generator.experiment_config(nodes = ['n1', 'n2'])

        self.given_an_experiment_script_with_this_config('config.yaml', config)

        self.executor.execute(self.script)

        self.should_load_configuration_with(self.config_parser, 'config.yaml')

    def test__build_node_executors__on_start(self):
        self.given_an_experiment_script_with_these_nodes(['n1', 'n2'])

        self.executor.execute(self.script)

        self.should_build_node_executors_for(['n1', 'n2'])

    def test__start_emulator_session(self):
        self.given_an_experiment_started_for_some_nodes()

        self.emulator.start_session.assert_called_with(self.config.network_file)

    def test__finish_emulator_session__on_end(self):
        self.given_an_experiment_was_executed_for_some_nodes()

        self.emulator.close_session.assert_called()

    def test__execute_bootstrapper__on_start(self):
        self.given_a_configuration_with_a_bootstrap_address()
        self.when_executing_experiment()

        self.should_start_bootstrapper_command_on_node('bootstrapper')

    def test__capture_packets_on_nodes__on_start(self):
        self.given_a_configuration_with_a_bootstrap_address()
        self.config.topology = Topology(
            nodes=[
                Node( name='r0', nodetype=NodeType.router ),
                Node( name='r1', nodetype=NodeType.router ),
                Node( name='bootstrapper', nodetype=NodeType.server )
            ]
        )

        self.when_executing_experiment()
        self.should_monitor_routers(['r0', 'r1'])
        self.should_monitor_nodes(['bootstrapper'])

    def test__stop_packet_capture__on_end(self):
        self.given_an_experiment_was_executed_for_some_nodes()

        self.should_have_stopped_packet_capturing()

    def test__execute_script_in_nodes__on_start(self):
        self.given_experiment_started_for_these_nodes(['n1', 'n2'])

        self.should_execute_script_in(self.executors(), self.script)


    def test__wait_for_nodes_publications__after_start(self):
        self.given_experiment_started_for_these_nodes(['n1', 'n2'])

        self.should_wait_for_publications_in(self.executors())

    def test__notify_for_start_of_search_stage__after_publications_end(self):
        self.given_an_experiment_started_for_some_nodes()

        self.should_notify_next_stage_for(self.executors())

    def test__wait_for_nodes_finish__after_experiment_stages(self):
        self.given_experiment_started_for_these_nodes(['n1', 'n2'])

        self.should_wait_for_finish_in(self.executors())


    def test__group_nodes_results__after_finish(self):
        self.given_an_experiment_was_executed_for_some_nodes()

        self.should_collect_results_based_on_config(self.results_collector)

    def test__save_collected_results__after_finish(self):
        self.given_an_experiment_was_executed_for_some_nodes()

        self.should_save_collected_results_on_output_dir()

    def test__return_merged_execution_results__after_execution(self):
        self.given_an_experiment_was_executed_for_some_nodes()

        self.should_have_returned_the_collected_results()

    # summary steps --------------------------------------------------

    def given_an_experiment_was_executed_for_some_nodes(self):
        self.given_an_experiment_started_for_some_nodes()

    def given_an_experiment_started_for_some_nodes(self):
        self.given_experiment_started_for_these_nodes(['n1', 'n2'])


    def given_experiment_started_for_these_nodes(self, nodes):
        self.given_an_experiment_script_with_these_nodes(nodes)

        self.when_executing_experiment()

    # steps --------------------------------------------------------


    def given_a_configuration_with_a_bootstrap_address(self):
        self.given_an_experiment_script_with_these_nodes(['n1', 'n2'])

        self.config.bootstrap_address = '10.9.8.7:6543'

    def given_an_experiment_configuration(self):
        self.given_an_experiment_script_with_these_nodes(['n1', 'n2'])

    def given_an_experiment_script_with_these_nodes(self, nodes):
        self.config = self.generator.experiment_config(nodes = nodes)

        return self.given_an_experiment_script_with_this_config('config.yaml', self.config)

    def given_an_experiment_script_with_this_config(self, config_path, config):
        self.script = ExperimentScript(config_path)

        self.config_parser.load_configuration = Mock(return_value=config)

        return self.script


    def when_executing_experiment(self):
        self.returned_results = self.executor.execute(self.script)


    def should_load_configuration_with(self, config_parser:Mock, config):
        config_parser.load_configuration.assert_called_once_with(config)

    def should_start_bootstrapper_command_on_node(self, node):
        expected_args = self.exec_config.bootstrapper.args_for_command(
            ['--bind', self.config.bootstrap_address]
        )
        self.emulator.start_cmd_in_node.assert_any_call(
                *expected_args, node=node
            )


    def should_build_node_executors_for(self, nodes):
        for n in nodes:
            self.node_builder.build.assert_any_call(n)

    def should_execute_script_in(self, executors, script):
        for e in executors:
            e.execute.assert_called_once_with(script)

    def should_wait_for_publications_in(self, executors):
        for e in executors:
            e.wait_for_publication.assert_called_once()

    def should_notify_next_stage_for(self, executors):
        for e in executors:
            e.start_next_stage.assert_called_once()

    def should_wait_for_finish_in(self, executors):
        for e in executors:
            e.wait_for_finish.assert_called_once()

    def should_collect_results_based_on_config(self, results_collector:Mock):
        config = self.script.config
        results_collector.collect_results.assert_called_once_with(config)

    def should_save_collected_results_on_output_dir(self):
        results_path = self.script.results_path()

        self.results_collector.save_results.assert_any_call(self.collected_results, results_path)

    def should_have_returned_the_collected_results(self):
        assert self.returned_results == self.collected_results

    def should_monitor_routers(self, routers):
        self.packets_capturer.monitor_router.assert_called()

        for r in routers:
            self.packets_capturer.monitor_router.assert_any_call(
                r, ANY, ANY
            )

    def should_monitor_nodes(self, nodes):
        self.packets_capturer.monitor_router.assert_called()

        self.packets_capturer.monitor_node.assert_called()

        for n in nodes:
            self.packets_capturer.monitor_node.assert_any_call(
                n, ANY, ANY
            )

    def should_have_stopped_packet_capturing(self):
        self.packets_capturer.stop_capture.assert_called()