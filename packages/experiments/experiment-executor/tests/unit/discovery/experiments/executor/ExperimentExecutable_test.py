from experiments.executor import ExperimentExecutable

class ExperimentExecutableTest(object):

    def test__generate_command_without_vm(self):
        exec_config = ExperimentExecutable(
            executable='example-command'
        )

        args = exec_config.args_for_command(['cmd'])

        assert args == ['example-command', 'cmd']


    def test__create_from_tuple(self):
        exec_config = ExperimentExecutable(['nodejs', 'command'])

        assert exec_config.vm == 'nodejs'
        assert exec_config.executable == 'command'

    def test__make_executables(self):
        assert ExperimentExecutable.make('exec') == ExperimentExecutable(
            executable='exec'
        )
        assert ExperimentExecutable.make(['vm', 'exec'])== ExperimentExecutable(
            executable='exec',
            vm='vm'
        )

        e = ExperimentExecutable.make(['vm', 'exec'])
        assert ExperimentExecutable.make(e) == e