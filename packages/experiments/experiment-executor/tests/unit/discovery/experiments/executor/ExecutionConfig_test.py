from experiments.executor import ExecutionConfig, ExperimentExecutable

from unittest.mock import Mock


class ExecutionConfigTest(object):

    def test__instantiate_node_executable(self):
        assert ExecutionConfig(node_executable='example-command') == ExecutionConfig(node_executable=ExperimentExecutable('example-command'))

    def test__generate_node_command(self):
        node_base_cmd = ['nodejs', 'script.js']

        exec_config = ExecutionConfig( node_executable=node_base_cmd )

        assert exec_config.node_command(['cmd']) == [
            *node_base_cmd, 'cmd'
        ]

    def test__generate_command_without_vm(self):
        exec_config = ExecutionConfig(
            node_executable='command'
        )

        assert exec_config.node_command(['args']) == ['command', 'args']

    def test__bootstrapper_command(self):
        exec_config = ExecutionConfig(
            node_executable='node_cmd',
            bootstrapper='bootstrapper')

        assert exec_config.bootstrapper.args_for_command(['example']) == ['bootstrapper', 'example']


    def test__start_node_command_in_emulator(self):
        emulator = Mock()
        exec_config = ExecutionConfig(
            node_executable='command',
            emulator=emulator
        )

        exec_config.start_node_command(['args'], node='n0')

        emulator.start_cmd_in_node.assert_called_with(
            *exec_config.node_command(['args']), node='n0'
        )
