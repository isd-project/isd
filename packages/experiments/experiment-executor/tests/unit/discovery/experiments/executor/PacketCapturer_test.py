from unittest.mock import Mock
from os import path

from experiments.executor import PacketCapturer

class PacketCapturerTests(object):
    def setup_method(self):
        self.fs_mock = Mock()
        self.capturer = PacketCapturer(fs=self.fs_mock)

        self.output_dir  = 'outdir'
        self.emulator    = Mock()
        self.emulator.start_cmd_in_node.side_effect = self.mock_start_cmd

        self.started_cmds = []

    def mock_start_cmd(self, *args, **kwargs):
        cmd = Mock()
        self.started_cmds.append(cmd)

        return cmd

    def test_capture_router(self):
        # Given:
        router_name = 'r0'

        # When:
        self.monitor_router('r0')

        # Then
        for iface in ['eth0', 'eth1']:
            self.emulator.start_cmd_in_node.assert_any_call(
                'tcpdump', '-i', iface, '-n',
                '-w', path.join(self.output_dir, f'r0_{iface}.pcap'),
                'tcp or udp',
                node='r0'
            )

    def test_ensure_outputdir__on_monitor_router(self):
        self.monitor_router('r0')

        self.fs_mock.ensure_dir.assert_called_with(self.output_dir)

    def test_capture_node(self):
        # When:
        self.monitor_node('n0')

        # Then
        self.emulator.start_cmd_in_node.assert_any_call(
            'tcpdump', '-i', 'eth0', '-n',
            '-w', path.join(self.output_dir, 'n0_eth0.pcap'), 'tcp or udp', node='n0'
        )

    def test_stop_capture_of_packets(self):
        self.monitor_node('n0')
        self.monitor_router('r0')
        self.monitor_router('r1')

        self.capturer.stop_capture()

        for cmd in self.started_cmds:
            cmd.close.assert_called_once()


    def monitor_node(self, node_name):
        return self.capturer.monitor_node(
                    node_name, self.emulator, output_dir=self.output_dir)

    def monitor_router(self, router_name):
        return self.capturer.monitor_router(
                    router_name, self.emulator, output_dir=self.output_dir)