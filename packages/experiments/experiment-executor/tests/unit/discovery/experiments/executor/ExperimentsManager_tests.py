from experiments import ExperimentsManager
from experiments.executor import ExperimentExecutor
from experiments.utils.filesystem import Fs
from experiments.model import ExperimentScript

from unittest.mock import Mock, ANY, patch

import os
from os import path
import pytest


class ExperimentsManagerTests(object):
    def setup_method(self):
        self.executor = Mock(spec=ExperimentExecutor)
        self.fs = Mock(spec=Fs)
        self.exp_manager = ExperimentsManager(
            executor=self.executor, fs=self.fs
        )

        self.experiment_dir = path.join('base', 'experimentdir')
        self.experiment_file = path.join(self.experiment_dir, 'experiment.config')

    def test__change_directory_on_execution(self):
        initial_dir = os.getcwd()

        self.exp_manager.execute_experiments(self.experiment_file)

        self.fs.chdir.assert_any_call(path.abspath(self.experiment_dir))
        self.fs.chdir.assert_any_call(initial_dir)

    def test__execute_experiment(self):
        self.exp_manager.execute_experiments(self.experiment_file)

        self.executor.execute.assert_called_with(ExperimentScript(
            config_path=path.abspath(self.experiment_file),
        ), options=ANY)

    def test__configure_experiments_output_dir(self):
        self.when_executing_experiment_with(
                'experiment/my-experiment.yaml',
                output_dir='outdir'
            )

        output_dir = self.the_last_experiment_script().output_dir

        assert output_dir == path.abspath('outdir/my-experiment'), \
            'A subdir should be created on output dir, based on filename'


    def test__execute_experiment__with_given_paths_as_absolute_paths(self):
        self.when_executing_experiment_with(
                'myexperiment.yaml', output_dir='output/example'
            )

        self.last_execute_should_be_called_with(
            experiment_file= path.abspath('myexperiment.yaml'),
            output_dir     = path.abspath('output/example/myexperiment')
        )

    def test__execute_one_experiment_replication_for_each_mechanism(self):
        m_variations = ['mdns', 'dht', ('mdns', 'dht'), 'mdns-raw']

        self.when_executing_experiment_with('exp.yaml',
                                            mechanism_variations=m_variations)

        scripts = self.executed_scripts()

        assert len(scripts) == len(m_variations)

        for script in scripts:
            assert script.config_path.endswith('exp.yaml')
            assert set(script.mechanisms) <= set(m_variations)

    @patch('experiments.experiments_manager.logging')
    def test_retry_experiment_until_2_times_in_case_of_failures(self, logPatch):
        self.given_there_are_failures_on_the_experiment_execution()

        self.when_executing_experiment_with('somefile.yaml', output_dir='outdir')

        assert self.the_number_of_executions_with('somefile.yaml') == 3

        for i in range(3):
            self.assert_has_written_error_file(i)

    # -------------------------------------------------

    def given_there_are_failures_on_the_experiment_execution(self):
        self.executor.execute.side_effect = Exception('failed experiment')

    def when_executing_experiment_with(
            self, experiment_file=None, output_dir=None,
            mechanism_variations=None
        ):
        if not experiment_file: experiment_file = self.experiment_file

        self.output_dir = output_dir

        self.exp_manager.execute_experiments(
                experiment_file,
                output_dir=output_dir,
                mechanism_variations=mechanism_variations
            )

    def last_execute_should_be_called_with(self, experiment_file, output_dir):
        script = self.the_last_experiment_script()

        assert script.output_dir == output_dir
        assert script.config_path == experiment_file

    def the_last_experiment_script(self)->ExperimentScript:
        args, kwargs = self.last_execute_call()
        script = args[0]

        return script

    def last_execute_call(self):
        return self.executor.execute.call_args

    def the_number_of_executions_with(self, config_file):
        scripts = [s for s in self.executed_scripts()
                    if path.basename(s.config_path) == config_file
                    ]

        return len(scripts)

    def executed_scripts(self):
        return [
            call[0][0] for call in self.executor.execute.call_args_list
        ]

    def assert_has_written_error_file(self, errnumber):
        script = self.the_last_experiment_script()

        expected_file = script.error_file(errnumber)

        self.fs.save_content.assert_any_call(ANY, expected_file)