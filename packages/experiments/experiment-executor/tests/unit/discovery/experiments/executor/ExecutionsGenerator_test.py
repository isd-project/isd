from experiments.executor import ExecutionsGenerator

class ExecutionsGeneratorTests(object):

    def setup_method(self):
        self.avaliable_mechanisms = ['dht', 'mdns', 'mdns-raw', ('dht', 'mdns')]
        self.generator = ExecutionsGenerator(self.avaliable_mechanisms)

        self.exp_file = 'experiment_file.yaml'
        self.outdir = 'outdir'

    def test__list_mechanisms_for_execution(self):
        generated = self.generator.list_mechanisms()

        assert set(generated) == set(self.avaliable_mechanisms)

    def test__listed_mechanisms_order_should_be_random(self):
        gen_mechanisms = [self.generator.list_mechanisms() for i in range(6)]

        assert gen_mechanisms[:3] != gen_mechanisms[3:]

    def test__generate_execution_scripts_for_experiment_file(self):
        exp_file = 'experiment_file.yaml'
        outdir = 'outdir'

        scripts = self.generator.executions_for(exp_file, output_dir=outdir)

        assert len(scripts) == len(self.avaliable_mechanisms)

        for s in scripts:
            assert s.config_path == exp_file
            assert s.output_dir.startswith(outdir)
            assert set(s.mechanisms) <= set(self.avaliable_mechanisms)


    def test__list_None_mechanisms__if_no_mechanisms_are_given(self):
        generator = ExecutionsGenerator([])

        mechanisms = generator.list_mechanisms()

        assert mechanisms == [None]

    def test__generate_one_execution_if_no_mechanisms_are_given(self):
        scripts = self.gen_executions_with([])

        assert len(scripts) == 1

        s = scripts[0]
        assert s.config_path == self.exp_file
        assert s.output_dir.startswith(self.outdir)
        assert s.mechanisms is None


    def test__generate_execution_with_simple_mechanisms(self):
        scripts = self.gen_executions_with(mechanisms=['a'])

        assert len(scripts) == 1
        assert scripts[0].mechanisms == ['a']

    def test__generate_execution_with_combination_of_mechanisms(self):
        scripts = self.gen_executions_with(mechanisms=[('a', 'b')])

        assert len(scripts) == 1
        assert scripts[0].mechanisms == ('a', 'b')


    def test__parse_combination_f_mechanisms(self):
        scripts = self.gen_executions_with(mechanisms=['a+b'])

        assert set(scripts[0].mechanisms) == set(('a', 'b'))

    # -------------------------------------------------------------

    def gen_executions_with(self, mechanisms):
        generator = ExecutionsGenerator(mechanisms)
        return generator.executions_for(self.exp_file, output_dir=self.outdir)

