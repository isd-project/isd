import pytest
from unittest.mock import Mock

from experiment_testing import ExperimentDataGenerator
from experiment_testing.mockups.emulator import mock_emulator

import experiments
from experiments.model import ExperimentScript
from experiments.executor import *


class ExperimentExecutorTest(object):

    def setup_method(self):
        self.generator = ExperimentDataGenerator()

    # tests -------------------------------------------------------------

    def test_can_build_executor_from_execution_config(self):
        self.build_executor_from_config()


    def test_start_nodes_with_execution_config(self):
        self.build_executor_from_config()
        self.when_executing_experiment()
        self.should_start_command_for_nodes()

    # ---------------------------------------------------------------


    # summary steps --------------------------------------------------


    # steps --------------------------------------------------------


    def build_executor_from_config(self):
        self.emulator = mock_emulator()

        self.exec_config = ExecutionConfig(
            emulator = self.emulator,
            node_executable = 'example'
        )

        self.executor = ExperimentExecutor(exec_config=self.exec_config,
                                           config_parser=Mock(),
                                           results_collector=Mock(),
                                           packets_capturer=Mock())

    def when_executing_experiment(self):
        config = self.generator.experiment_config()
        self.script = ExperimentScript('nodes_config.yaml',
                                        experiment_config =config)

        self.executor.execute(self.script)

    def should_start_command_for_nodes(self):
        for n in self.script.nodes():
            config = self.script.get_config_for(n)
            exp_dir = self.script.experiment_dir()
            args = self.exec_config.node_command([config, n, exp_dir])

            self.emulator.start_cmd_in_node.assert_any_call(*args, node=n)