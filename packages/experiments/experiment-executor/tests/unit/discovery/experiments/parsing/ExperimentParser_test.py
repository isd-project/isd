import pytest

from experiments.parsing import ExperimentParser

class ExperimentParserTests(object):
    def setup_method(self):
        self.parser = ExperimentParser()

    def test_parse_experiment_yaml(self):
        exp = self.parser.parse_experiment('''
parameters:
    num_nodes: 2
    num_queries: 1
        ''')

        assert exp.parameters.num_nodes == 2
        assert exp.parameters.num_queries == 1