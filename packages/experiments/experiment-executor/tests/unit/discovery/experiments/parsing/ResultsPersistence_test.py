from experiments.parsing import ResultsPersistence

from experiment_testing import ExperimentDataGenerator


class ResultsPersistenceTest(object):

    def setup_method(self):
        self.persistence = ResultsPersistence()

        self.generator = ExperimentDataGenerator()

    def test__render_and_parse_experiment_results(self):
        results = self.generator.merged_results()

        rendered = self.persistence.render_results(results)
        parsed = self.persistence.parse_results(rendered)

        assert parsed == results

    def test__render_and_parse_node_results(self):
        node_results = self.generator.node_result()

        rendered = self.persistence.render_node_results(node_results)
        parsed   = self.persistence.parse_node_results(rendered)

        assert parsed == node_results

    def test__parse_results_with_publication(self):
        results_yaml = '''publications:
  - service:
      name: s1_n1
      type: type1
      providerId: !<tag:yaml.org,2002:binary> UCZwH3Xjrjepbme42Ar1hXZU0oVq3BHQHTeeOYPI4vQ
      instanceId: null
      parameters: null
    interval:
      start: !<tag:yaml.org,2002:timestamp> "2022-04-14T01:42:50.430Z"
      end: !<tag:yaml.org,2002:timestamp> "2022-04-14T01:42:51.407Z"
searches: []'''

        parsed   = self.persistence.parse_node_results(results_yaml)

        pub_srv = parsed.publications[0].service

        assert pub_srv.get('providerId') is not None