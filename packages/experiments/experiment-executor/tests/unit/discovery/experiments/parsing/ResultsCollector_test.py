from unittest.mock import Mock
from os import path

from experiment_testing import ExperimentDataGenerator

from experiments.parsing import ResultsCollector
from experiments.model.results import NodeResults, MergedResults


class ResultsCollectorTest(object):

    def setup_method(self):
        self.results_persistence = Mock()
        self.fs = Mock()
        self.collector = ResultsCollector(self.results_persistence, fs=self.fs)

        self.generator = ExperimentDataGenerator()

    def test__grab_results_from_configuration(self):
        config = self.given_an_experiment_configuration()

        self.collector.collect_results(config)

        self.collector_should_read_all_nodes_results(config)


    def test__merged_results_should_combine_node_results(self):
        config = self.given_an_experiment_configuration()
        results = self.given_each_node_published_a_result(config)
        self.fs.exists.return_value = True

        merged = self.collector.collect_results(config)

        self.the_merged_result_should_have_all_nodes_results(merged, results)


    def test__save_merge_results(self):
        config      = self.given_an_experiment_configuration()
        results     = self.generator.merged_results()
        expected_path = config.results_path()

        self.collector.save_results(results, expected_path)

        self.should_save_experiment_results_on_given_path(results, expected_path)

    # steps ------------------------------------------------------------------

    def given_an_experiment_configuration(self):
        config = self.generator.experiment_config(nodes=['n1', 'n2', 'n3'])

        return config

    def given_each_node_published_a_result(self, config):
        results = [self.generator.node_result() for n in config.get_nodes()]

        self.results_persistence.read_node_results.side_effect = results

        return results

    def collector_should_read_all_nodes_results(self, config):
        for node, node_config in config.nodes.items():
            out = node_config.output_file()

            assert out is not None

            self.results_persistence.read_node_results.assert_any_call(out)


    def the_merged_result_should_have_all_nodes_results(self, merged, results):
        values = merged.results()

        for r in results:
            assert r in values

    def should_save_experiment_results_on_given_path(self, results, res_path):
        self.results_persistence.save_results.assert_any_call(results, res_path)