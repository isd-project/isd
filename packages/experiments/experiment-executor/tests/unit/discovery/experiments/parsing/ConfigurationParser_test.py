from experiments.parsing import ConfigurationParser
from experiments.model import ExperimentConfig, NodeConfigurationModel
from experiments.model.topology_models import *
from experiments.generator.models import ExperimentParameters

from experiment_testing import data

from datetime import timedelta


class ConfigurationParserTest(object):

    def setup_method(self):
        self.parser = ConfigurationParser()

    def test_parse_simple_configuration(self):
        config_parser = ConfigurationParser()

        config = config_parser.parse_configuration('''
output_dir: path/to/output
results_file: experiment_results.yaml
network_file: network.xml
keysdir: path/to/keys
nodes:
    n1:
        name: n1
        keyid: c58cac60ed63ed2a9abaf2e4e3f
        keysdir: n1_keys
        output_dir: path/to/output
        results_file: n1.results
        publications: []
        searches: []
        ''')

        assert config == ExperimentConfig(
            output_dir='path/to/output',
            results_file='experiment_results.yaml',
            network_file='network.xml',
            keysdir='path/to/keys',
            nodes = {
                'n1': NodeConfigurationModel(
                    name= 'n1',
                    keyid= 'c58cac60ed63ed2a9abaf2e4e3f',
                    keysdir= 'n1_keys',
                    output_dir= 'path/to/output',
                    results_file= 'n1.results',
                    searches= [],
                    publications= []
                )
            }
        )

    def test_parse_full_configuration(self):
        config_parser = ConfigurationParser()

        config = config_parser.parse_configuration(data.experiment_configuration_yaml)

        assert config == data.experiment_configuration_model


    def test_parse_topology(self):
        config_parser = ConfigurationParser()

        config = config_parser.parse_configuration('''
nodes: {}
topology:
  networks:
  - ip_prefixes:
      ip4: 10.0.0.0/24
      ip6: 2001::/64
    name: lan0
    nettype: lan
    nodes:
    - r0
    - n0
  nodes:
  - extra_services: null
    id: 4
    name: n0
    networks:
    - lan0
    nodetype: host
  - extra_services:
    - NAT
    id: 2
    name: r0
    networks:
    - lan0
    nodetype: router
        ''')

        assert config == ExperimentConfig(
            nodes = {},
            topology=Topology(
                networks=[
                    Network(
                        ip_prefixes=IpPrefixes(
                            ip4= '10.0.0.0/24',
                            ip6= '2001::/64'
                        ),
                        name='lan0',
                        nettype=NetTypes.LAN,
                        nodes=set(['r0', 'n0'])
                    )
                ],
                nodes=set([
                    Node(
                        id=4,
                        name='n0',
                        networks= ['lan0'],
                        nodetype=NodeType.host
                    ),
                    Node(
                        id=2,
                        name='r0',
                        networks= ['lan0'],
                        nodetype=NodeType.router,
                        extra_services=['NAT']
                    )
                ])
            )
        )

    def test_parse_parameters(self):
        config = ExperimentConfig(
            nodes = {},
            params = ExperimentParameters(
                num_hosts=4,
                num_nets=2,
                num_localizations=2,
                num_searches=2,
                srvtypes=['srv1', 'srv2'],
                delay_interval=[
                    timedelta(milliseconds=0), timedelta(seconds=2, milliseconds=300)]
            )
        )

        rendered = self.parser.render_configuration(config)
        parsed   = self.parser.parse_configuration(rendered)

        assert parsed == config