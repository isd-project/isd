import testdata

from core_robot import Core


class TestPing(object):

    @classmethod
    def setup_class(cls):
        """ setup any state specific to the execution of the given class (which
        usually contains tests).
        """
        cls.core = Core(testdata.networks_dir())
        cls.core.open_network('wired-LAN')

    @classmethod
    def teardown_class(cls):
        """ teardown any state that was previously setup with a call to
        setup_class.
        """
        cls.core.close()


    def test_ping_ipv4(self):
        self._test_ping_template(ipfamily='ipv4')

    def test_ping_ipv6(self):
        self._test_ping_template(ipfamily='ipv6')


    def _test_ping_template(self, ipfamily=None):
        n2 = self.get_node('n2')
        n2_ip = n2.get_ip(ipfamily)

        cmd = self.start_ping('n1', n2, ipfamily)

        if ipfamily == 'ipv6':
            expected_msg = f'PING {n2_ip}.* data bytes'
        else:
            expected_msg = f'PING {n2_ip}.* bytes of data'

        cmd.wait_for_message(expected_msg)

    def start_ping(self, node1, node2, ipfamily=None):
        cmd = self.make_ping_command(node1, node2, ipfamily)

        return self.get_node(node1).start_command(*cmd)


    def make_ping_command(self, node1, node2, ipfamily=None):
        node1 = self.get_node(node1)
        node2 = self.get_node(node2)

        ip = node2.get_ip(ipfamily)

        cmd = ['ping', ip, '-c', '1']

        if ipfamily == 'ipv6':
            cmd.extend(['-6', '-w', '3'])

        return cmd

    def get_node(self, node_or_name):
        if isinstance(node_or_name, str):
            return self.core.get_node(node_or_name)

        return node_or_name