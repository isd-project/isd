import subprocess
import sys
import pytest
import os
import logging
from os import path

import testdata
from core_robot import Core

import experiments
from experiments.parsing import ExperimentParser, ResultsParser
from experiments.executor import ExperimentExecutor, ExecutionConfig
from experiments.model import ExperimentScript

from experiment_testing.helpers import ExperimentDirectories
from experiment_testing import data

from experiments.core import CoreEmulator


class TestExperimentExecutor(object):

    @pytest.fixture(autouse=True)
    def core_emulator(self):
        self.core_emulator = CoreEmulator()

        yield self.core_emulator

        self.core_emulator.close_session()


    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.tmp_path = tmp_path
        self.dirs = ExperimentDirectories(tmp_path)

        test_keysdir = testdata.datapaths.test_keysdir()

        self.keysdir = self.dirs.copy_keysfile(test_keysdir)

    def setup_method(self):
        self.network_file = path.join(testdata.networks_dir(), 'wired-LAN.xml')


    def test_execute_simple_experiment(self, core_emulator):
        self.execute_simple_experiment(core_emulator)

    def test_execute_simple_experiment__for_raw_mdns(self, core_emulator):
        self.execute_simple_experiment(core_emulator, ['mdns-raw'])


    def execute_simple_experiment(self, core_emulator, mechanisms=None):
        self.core = core_emulator

        self.given_the_experiment_setup_was_configured(mechanisms)
        self.given_some_nodes_configurations(
            data.sample_experiment_configuration_yaml(
                self.dirs.out_path,
                network_file=self.network_file,
                keysdir=self.keysdir)
        )
        self.when_running_the_experiment_script()
        self.then_should_produce_metrics_for_the_given_configuration(
            data.sample_experiment_configuration_model(self.dirs.out_path)
        )
        self.then_all_searches_should_be_successful()

    # Helpers --------------------------------------------------------

    def given_the_experiment_setup_was_configured(self, mechanisms=None):
        exec_config = ExecutionConfig(
            emulator = self.core,
            node_executable = [
                'nodejs', testdata.buildpaths.experiment_node_path()
            ]
        )

        self.executor = ExperimentExecutor(exec_config=exec_config)

        self.mechanisms = mechanisms


    def given_some_nodes_configurations(self, config):
        self.nodes_config_path = self.dirs.save_experiment_config(config, 'config.yaml')

    def when_running_the_experiment_script(self):
        self.experiment_script = ExperimentScript(
            self.nodes_config_path, mechanisms=self.mechanisms,
            output_dir=self.dirs.out_path
        )
        self.results = self.executor.execute(self.experiment_script)

    def then_should_produce_metrics_for_the_given_configuration(
        self, expected
    ):
        results = self.results

        assert results is not None, 'Should have collected the results!'

        found_nodes    = self.verify_found_nodes(results, expected)

        for n in found_nodes:
            node_results = results.nodes.get(n)
            node_config = expected.nodes.get(n)

            self.verify_number_of_publications(n, node_results, node_config)
            self.verify_number_of_searches(n, node_results, node_config)


    def then_all_searches_should_be_successful(self):
        for node, results in self.results.nodes.items():
            for search in results.searches:
                assert search.error is None, f'node: {node}'
                assert len(search.found) > 0, \
                    f'node "{node}" failed to find results for search: {search.search}'

    # -------------------------------------------------------

    def verify_found_nodes(self, results, expected):
        expected_nodes = sorted(list(expected.nodes.keys()))
        found_nodes    = sorted(list(results.nodes.keys()))

        assert found_nodes == expected_nodes
        assert found_nodes is not None

        return found_nodes

    def verify_number_of_publications(self, n, node_results, node_config):
        self._verify_length_match(
            node_results.publications, node_config.publications,
            f'The number of executed publications does not match the expected, for node {n}'
        )

    def verify_number_of_searches(self, n, node_results, node_config):
        self._verify_length_match(node_results.searches, node_config.searches,
            f'The number of executed searches does not match the expected, for node {n}'
        )

    def _verify_length_match(self, found_list, expected_list, msg=''):
        expected_length = len(expected_list)
        found_length    = len(found_list)

        assert found_length == expected_length, msg
