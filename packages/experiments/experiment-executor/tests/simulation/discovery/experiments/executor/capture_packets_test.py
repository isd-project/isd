import pytest
import os
from os import path

import testdata

from experiments.executor import ExperimentExecutable, PacketCapturer

from experiments.core import CoreEmulator


class CapturePacketsTests(object):

    @pytest.fixture(autouse=True)
    def core_emulator(self):
        self.core_emulator = CoreEmulator()

        yield self.core_emulator

        self.core_emulator.close_session()


    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.tmp_path = tmp_path

    def setup_method(self):
        self.network_file = path.join(testdata.networks_dir(), 'wired-LAN.xml')
        self.capturer = PacketCapturer()

    def test_capture_packets(self, core_emulator):
        outdir = path.join(self.tmp_path, 'capture')

        with self.core_emulator.session_open(self.network_file):
            cmds = self.capturer.monitor_node('n1', self.core_emulator, outdir)
            cmds[0].wait_for_message('tcpdump: listening', timeout=2)

            self.capturer.stop_capture()

            expected_file = self.capturer.tcpdump.capture_file(
                    'n1', 'eth0', outdir
                )
            assert path.exists(expected_file)