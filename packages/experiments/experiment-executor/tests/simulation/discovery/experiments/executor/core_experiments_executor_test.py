import pytest
from os import path

import testdata
from testdata import datapaths
from experiment_testing.helpers import ExperimentDirectories
from experiment_testing import data

from experiments.core import CoreExperimentsExecutor, CoreEmulator
from experiments.executor import ExperimentExecutable, ExecutionConfig
from experiments import ExperimentsManager
from experiments.model import ExperimentScript, ExperimentConfig, MergedResults
from experiments.parsing import ConfigurationParser, ResultsPersistence

from experiments.executor.packet_capturer import TcpdumpCommand


class CoreExperimentsExecutorTest(object):

    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.tmp_path = tmp_path

        self.output_dir = path.join(tmp_path, 'output')

    def setup_method(self):
        self.config_parser = ConfigurationParser()
        self.executor = CoreExperimentsExecutor(
            node_executable = [
                'nodejs', testdata.buildpaths.experiment_node_path()],
            bootstrap_executable= [
                'nodejs', testdata.buildpaths.dht_bootstrapper_path()
            ]
        )

        self.results_persistence = ResultsPersistence()

    def teardown_method(self):
        # ensure to release resources
        if self.executor:
            self.executor.close()


    def test_execute_experiment_single_network(self):
        self.execute_experiment(datapaths.single_network_exp_file())

    @pytest.mark.debug
    def test_execute_experiment_two_networks(self):
        self.execute_experiment(datapaths.two_networks_exp_file())

    def execute_experiment(self, experiment_file):
        self.executor.execute_experiments(
            experiment_file, output_dir=self.output_dir
        )

        config = self.verify_results_generated(experiment_file)
        self.verify_experiment_results(config)
        self.verify_packets_captured(config)

    # Helpers --------------------------------------------------------

    def verify_results_generated(self, experiment_file):
        assert path.exists(self.output_dir), f'output_dir: {self.output_dir}'

        script = ExperimentScript(
                config_path=experiment_file, output_dir=self.output_dir
            )
        config:ExperimentConfig = script.load_configuration(self.config_parser)


        assert path.exists(config.results_path())

        return config

    def verify_experiment_results(self, config):
        results:MergedResults = self.results_persistence.read_results(config.results_path())

        self.verify_results_match_configuration(results, config)
        self.verify_all_searches_are_successfull(results)

    def verify_results_match_configuration(self, results, config):
        config_publications = []
        config_searches = []

        publications = []
        search_results = []

        for node_config in config.nodes.values():
            config_publications.extend(node_config.publications)
            config_searches.extend(node_config.searches)

        for node_result in results.results():
            publications.extend(node_result.publications)
            search_results.extend(node_result.searches)

        assert len(publications) == len(config_publications)
        assert len(search_results) == len(search_results)

    def verify_all_searches_are_successfull(self, results):
        for node, node_results in results.nodes.items():
            for s_result in node_results.searches:
                assert s_result.error is None
                assert len(s_result.found) > 0, f'no results for search of node {node}: {s_result.search}'


    def verify_packets_captured(self, config:ExperimentConfig):
        cmd = TcpdumpCommand()

        for router in config.topology.routers():
            if router.name == 'backbone': continue

            for iface in ['eth0', 'eth1']:
                assert path.exists(
                    cmd.capture_file(router.name, iface, config.output_dir)
                )

        assert path.exists(
                    cmd.capture_file('bootstrapper', 'eth0', config.output_dir)
                )