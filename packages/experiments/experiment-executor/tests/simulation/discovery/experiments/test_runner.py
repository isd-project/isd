import subprocess
import sys
import pytest
import os
import logging

import experiments
from experiments.parsing import ExperimentParser, ResultsCollector
from experiments.model import ExperimentParameters


class TestRunner(object):

    @pytest.fixture(autouse=True)
    def setup_tmp_dir(self, tmp_path):
        self.tmp_path = tmp_path
        self.experiment_path = tmp_path / 'experiment'
        self.experiment_path.mkdir()

    def setup_method(self):
        self.exp_parser = ExperimentParser()

    @pytest.mark.skip('To-Do: implement runner')
    @pytest.mark.debug
    def test_experiments_runner(self):
        self.given_runner_parameters('''
parameters:
    num_nodes: 2
    num_queries: 1
''')
        self.when_running_the_experiment()
        self.and_collecting_the_results()
        self.then_should_produce_metrics_for_the_given_parameters({
            'num_publications': 2,
            'num_queries': 1
        })

    # Helpers --------------------------------------------------------

    def given_runner_parameters(self, parameters):
        self.params_path = self.experiment_path / 'exp.yml'
        self.params_path.write_text(parameters)

        self.out_path = self.experiment_path / 'output'


    def when_running_the_experiment(self):
        exp_path = str(self.params_path)
        args = [sys.executable, '-m', 'experiments.runner', exp_path,
                    '-o', str(self.out_path)]
        cmd = ' '.join(args)

        logging.info('executing: {0}'.format(cmd))
        os.system(cmd)


    def and_collecting_the_results(self):
        results_collector = ResultsCollector()

        # TO-DO: use experiment configuration
        # self.results = results_collector.collect_results(str(self.out_path))


    def then_should_produce_metrics_for_the_given_parameters(self, expected):
        results = self.results

        assert results is not None, 'No results collected!'

        assert len(results.publications) is expected.get('num_publications')
        assert len(results.queries) is expected.get('num_queries')