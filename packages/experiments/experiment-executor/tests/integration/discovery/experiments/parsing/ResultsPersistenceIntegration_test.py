from experiments.parsing import ResultsPersistence

from experiment_testing import ExperimentDataGenerator


class ResultsPersistenceIntegrationTest(object):

    def setup_method(self):
        self.persistence = ResultsPersistence()

        self.generator = ExperimentDataGenerator()

    def test__save_and_load_experiment_results(self, tmp_path):
        output = str(tmp_path / 'results.yaml')

        results = self.generator.merged_results()

        rendered = self.persistence.save_results(results, output)
        parsed = self.persistence.read_results(output)

        assert parsed == results

    def test__save_and_read_node_results(self, tmp_path):
        output = str(tmp_path / 'node_result.yaml')
        node_results = self.generator.node_result()

        self.persistence.save_node_results(node_results, output)
        parsed  = self.persistence.read_node_results(output)

        assert parsed == node_results