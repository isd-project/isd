from experiments_generator.exporter import XmlParser, CoreExporter
from experiments_generator.exporter.core_model import CoreScenario
from experiments_generator.generators import TopologyGenerator
from experiments.model.topology_models import TopologySpec

class CoreNetworkGenerator(object):
    def __init__(self, xml_parser=None):
        self.topo_gen = TopologyGenerator()
        self.exporter = CoreExporter()
        self.xml_parser = xml_parser if xml_parser else XmlParser()

    def generate_network_file(self, num_hosts, num_nets, out_path):
        topo = self.generate_topology(num_hosts, num_nets)
        self.generate_network_file_for_topology(topo, out_path)

    def generate_topology(self, num_hosts, num_nets):
        spec = TopologySpec(num_nets, num_hosts)
        topo = self.topo_gen.gen_experiment_topology(spec)

        return topo

    def generate_network_file_for_topology(self, topo, out_path)->CoreScenario:
        core_scenario = self.exporter.export_topology(topo)
        self.xml_parser.serialize_to_file(core_scenario, out_path)

        return core_scenario