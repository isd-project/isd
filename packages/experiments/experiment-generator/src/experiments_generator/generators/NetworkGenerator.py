from experiments_generator.models import *
from experiments.model.topology_models import *

from .IpPrefixesGenerator import IpPrefixesGenerator
from .NameGenerator import NameGenerator


class NetworkGenerator(object):
    NET_PREFIXES = {
        NetTypes.LAN: 'lan',
        NetTypes.WAN: 'wan'
    }

    def __init__(self):
        self.counters = {nt:0 for nt in self.NET_PREFIXES.keys()}

        self.namegen = NameGenerator(self.NET_PREFIXES)
        self.ipprefixGen = IpPrefixesGenerator()

    def gen_next(self, nettype=NetTypes.LAN):
        name = self.namegen.gen_next(nettype)

        network_ips = self.ipprefixGen.gen_next()

        return Network(name, nettype=nettype, ip_prefixes=network_ips)
