from experiments.model import IpPrefixes

class IpPrefixesGenerator(object):

    def __init__(self):
        self.counter = 0

    def gen_next(self):
        count = self.get_and_increment_counter()

        if count > 255:
            raise Exception('Not implemented: trying to generate more than 255 networks')

        return IpPrefixes(ip4=f'10.0.{count}.0/24',
                          ip6=f'2001:{count}::/64')

    def get_and_increment_counter(self):
        counter = self.counter

        self.counter = counter + 1

        return counter