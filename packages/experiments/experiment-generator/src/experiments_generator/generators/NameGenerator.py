from typing import Dict

class NameGenerator(object):

    def __init__(self, prefixes: Dict):
        self.prefixes = prefixes
        self.counters = {key:0 for key in self.prefixes.keys()}

    def gen_next(self, prefixtype):
        return self._gen_name(prefixtype)

    def _gen_name(self, prefixtype):
        prefix = self.prefixes.get(prefixtype)
        counter = self._get_and_inc_counter(prefixtype)

        return f'{prefix}{counter}'

    def _get_and_inc_counter(self, prefixtype):
        counter = self.counters[prefixtype]

        self.counters[prefixtype] = counter + 1

        return counter