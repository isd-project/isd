class MacGenerator(object):
    def __init__(self):
        self.counter0 = 0
        self.counter1 = 0

    def next(self):
        c0 = self._hex_format(self.counter0)
        c1 = self._hex_format(self.counter1)

        mac = f'00:00:00:aa:{c1}:{c0}'

        self.increment_counters()

        return mac

    def _hex_format(self, num):
        return '{:02X}'.format(num)

    def increment_counters(self):
        self.counter0 += 1

        if self.counter0 > 255:
            self.counter0 = 0
            self.counter1 += 1

            if self.counter1 > 255:
                raise Exception('Reached max max currently supported')