import random
from datetime import timedelta
from dataclasses import dataclass
from typing import List, Callable, Dict

from experiments.generator.models.experiment_models import *
from experiments.model import SearchSpec

from . import OperationsDistributor


@dataclass
class SearchesSpec(object):
    hosts:List[HostInfo]
    num_searches:int
    num_localizations:int
    delay_interval:List[timedelta]
    publications:Dict[str, List[ServicePublication]]

    def list_publications(self):
        if not self.publications: return []

        publis = []

        for node_publications in self.publications.values():
            publis.extend(node_publications)

        return publis


class SearchesDistribution(object):
    def __init__(self, search_spec:SearchesSpec):
        self.search_spec = search_spec


        self.search_distributor = OperationsDistributor(
            search_spec.hosts,
            search_spec.num_searches,
            search_spec.delay_interval,
            self.search_generator,
            operations_data=search_spec.list_publications()
        )

        self.locate_distributor = OperationsDistributor(
            search_spec.hosts,
            search_spec.num_localizations,
            search_spec.delay_interval,
            self.locate_search_generator,
            operations_data=search_spec.list_publications(),
        )

    def generate(self)->Dict[str,SearchSpec]:
        searches = self.search_distributor.generate()
        locate_searches = self.locate_distributor.generate()

        return self.merge_searches(searches, locate_searches)


    def search_generator(self, op_args)->SearchSpec:
        publications = op_args.operation_data

        srvType = self.pick_service_type_for(op_args.node, publications)

        if srvType is None:
            return None

        return SearchSpec(
            kind='search',
            srvType=srvType,
            delay=op_args.start_delay
        )

    def locate_search_generator(self, op_args)->SearchSpec:
        publications = op_args.operation_data

        srvId = self.pick_service_id_for(op_args.node, publications)

        if srvId is None:
            return None

        return SearchSpec(
            kind='locate',
            srvId=srvId,
            delay=op_args.start_delay
        )

    def pick_service_type_for(self, node, publications):
        for pub in self.shuffled_publications(publications):
            if pub.type and pub.node.name != node.name:
                return pub.type

        return None

    def pick_service_id_for(self, node, publications):
        for pub in self.shuffled_publications(publications):
            if pub.node.name == node.name: #ignore same host
                continue

            srvId = pub.service_id()

            if srvId is not None:
                return srvId

        return None

    def shuffled_publications(self, publications):
        shuffled_publis = list(publications)
        random.shuffle(shuffled_publis)

        for pub in shuffled_publis:
            yield pub

    def merge_searches(self, searches_map, locate_searches_map):
        merged = {}

        for node in searches_map.keys():
            searches = searches_map[node]
            locate_searches = locate_searches_map[node]

            merged[node] = self._merge_node_searches(searches, locate_searches)

        return merged

    def _merge_node_searches(self, searches, locate_searches):
        merged = list(searches) + locate_searches

        random.shuffle(merged)

        return merged


