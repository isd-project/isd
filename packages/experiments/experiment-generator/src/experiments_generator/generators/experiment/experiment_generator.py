from experiments.generator.models.experiment_models import *
from experiments_generator.exporter.core_model import CoreScenario
from experiments.model import ExperimentConfig, NodeConfigurationModel

from experiments_generator import CoreNetworkGenerator
from experiments_generator.generators.keys import KeysGenerator
from experiments.utils.filesystem import Fs
from experiments.parsing import ConfigurationParser

from .ExperimentOperationsGenerator import ExperimentOperationsGenerator

from dataclasses import dataclass, replace
from os import path
from datetime import timedelta


class ExperimentGenerator(object):
    BOOTSTRAP_PORT = 12345

    @classmethod
    def default_delay_interval(cls):
        return (
            timedelta(milliseconds=0),
            timedelta(milliseconds=500)
        )

    @classmethod
    def default_srvtypes(cls):
        return ['srvtype']


    def __init__(self, network_gen=None, keysgen=None, config_parser=None):
        if not network_gen  : network_gen   = CoreNetworkGenerator()
        if not keysgen      : keysgen       = KeysGenerator()
        if not config_parser: config_parser = ConfigurationParser()

        self.network_gen   = network_gen
        self.keysgen       = keysgen
        self.config_parser = config_parser
        self.operation_gen = ExperimentOperationsGenerator()

        self.fs = Fs()

    def gen_and_save_experiment(self, params, exp_dir, output):
        config = self.generate_experiment_run(params, exp_dir)

        self.config_parser.save_configuration(config, output)

        return config

    def gen_and_save_experiments(self,
        params, exp_dir, filename=None, replications=1
    ):
        configs = self.generate_experiment_runs(
            params, exp_dir, replications=replications
        )

        return self._save_experiment_configs(
            configs, params, exp_dir, filename, replications)

    def generate_experiment_run(self, params, exp_dir):
        configs = self.generate_experiment_runs(params, exp_dir, 1)
        return configs[0]

    def generate_experiment_runs(self, params, exp_dir, replications=1):
        params   = self.fill_experiment_params(params)
        exp_dirs = ExperimentDirs(exp_dir, self.fs)
        exp_dirs.ensure_dir()

        net_info = self.generate_network(params, exp_dirs)
        # node_keys, keysdir = self.generate_keys(net_info, exp_dirs, None)

        return [
            self._gen_experiment_config(
                    params, net_info, exp_dirs, i
                )
                for i in range(replications)
        ]


    # ------------------------------------------------------------------------

    def fill_experiment_params(self, params:ExperimentParameters):
        params = replace(params)

        if not params.delay_interval:
            params.delay_interval = ExperimentGenerator.default_delay_interval()
        if not params.srvtypes:
            params.srvtypes = ExperimentGenerator.default_srvtypes()

        return params

    def generate_network(self, params:ExperimentParameters, exp_dirs):
        net_filename = f'network-{params.num_hosts}_{params.num_nets}.xml'

        topo = self.network_gen.generate_topology(
            params.num_hosts, params.num_nets
        )
        core_scenario = self.network_gen.generate_network_file_for_topology(
            topo, exp_dirs.subdir(net_filename)
        )

        return NetworkInfo(
                topology=topo, network_file=net_filename,
                core_scenario=core_scenario
            )

    def _gen_experiment_config(self,
        params, net_info, exp_dirs, runid=0
    ):
        run_dir_name = f'replication{runid}'
        rundir       = run_dir_name
        # relative path
        node_outdir = path.join(run_dir_name, 'output')

        node_keys, keysdir = self.generate_keys(net_info, exp_dirs, None)
        node_operations    = self.gen_node_operations(node_keys, params)
        node_configs       = self.gen_nodes_configs(
                                node_keys, node_operations, node_outdir, exp_dirs
                            )

        return ExperimentConfig(
            topology          = net_info.topology,
            network_file      = net_info.network_file,
            bootstrap_address = self.gen_bootstrapper_address(net_info),

            output_dir   = '.',
            keysdir      = keysdir,
            nodes        = node_configs,

            params=params
        )

    def gen_bootstrapper_address(self, net_info):
        ip = net_info.bootstrapper_ip()

        return f'{ip}:{self.BOOTSTRAP_PORT}'

    def generate_keys(self, net_info, exp_dirs, rundir=None):
        node_names = net_info.node_names()

        relative_keysdir = 'keys'
        out_keysdir      = exp_dirs.subdir(relative_keysdir)

        node_keys = self.keysgen.generate_keys(
            nodes=node_names, out_dir=out_keysdir
        )

        return node_keys, relative_keysdir

    def gen_node_operations(self, node_keys, params):
        hosts = [nk.to_host_info() for nk in node_keys]

        return self.operation_gen.generate_experiment_operations(params, hosts)

    def gen_nodes_configs(self,
        node_keys, node_operations, node_outdir, exp_dirs
    ):
        node_configs = {}

        for nk in node_keys:
            n_conf = self.gen_node_config(
                            nk, node_operations, node_outdir, exp_dirs)

            node_configs[nk.node] = n_conf

        return node_configs

    def gen_node_config(self, node_key, exp_operations, node_outdir, exp_dirs):
        nk = node_key

        return NodeConfigurationModel(
            name = nk.node,

            keyid    = nk.keyinfo.fingerprint.hash,
            keyid_fmt= nk.keyinfo.fingerprint.hash_fmt,
            keysdir  = exp_dirs.remove_prefix(nk.keyinfo.keysdir),

            output_dir   = path.join(node_outdir, nk.node),
            results_file = f'{nk.node}.results',

            publications = exp_operations.get_publications_for(nk.node),
            searches     = exp_operations.get_searches_for(nk.node)
        )


    def _save_experiment_configs(self,
        configs, params, exp_dir, filename, replications
    ):
        filepaths = self._gen_filepaths(params, exp_dir, filename, replications)

        for i in range(len(configs)):
            conf = configs[i]
            filepath = filepaths[i]

            self.config_parser.save_configuration(conf, filepath)

        return filepaths

    def _gen_filepaths(self, params, exp_dir, filename, replications):
        if filename is None:
            hs = f'h{params.num_hosts}'
            ns = f'n{params.num_nets}'
            srvs = f'sv{params.num_services}'
            srs = f's{params.num_searches}'
            locs = f'l{params.num_localizations}'

            filename = f'experiment-{hs}-{ns}-{srvs}-{srs}-{locs}.yaml'

        nameparts = path.splitext(filename)
        if(nameparts[1] is None): nameparts[1] = '.yaml'
        filenames = [
            f'{nameparts[0]}-r{i}{nameparts[1]}' for i in range(replications)
        ]

        return [path.join(exp_dir, fname) for fname in filenames]


# Helper classes -----------------------------------

class ExperimentDirs(object):
    def __init__(self, experiment_dir, fs):
        self.experiment_dir = experiment_dir
        self.fs = fs

    def ensure_dir(self):
        self.fs.makedirs(self.experiment_dir)

    def subdir(self, *path_segments):
        return self.fs.joinpaths(self.experiment_dir, *path_segments)

    def remove_prefix(self, subpath):
        return path.relpath(subpath, self.experiment_dir)

@dataclass
class NetworkInfo(object):
    topology:Topology
    network_file:str
    core_scenario:CoreScenario=None

    def node_names(self):
        return [n.name for n in self.topology.hosts()]

    def bootstrapper_ip(self):
        if self.core_scenario is None: return None

        ifaces_map = self.core_scenario.gen_interfaces_map()
        boots_ifaces = ifaces_map.get('bootstrapper')

        if boots_ifaces and len(boots_ifaces) > 0:
            iface0 = boots_ifaces[0]

            return iface0.ip4

        return None
