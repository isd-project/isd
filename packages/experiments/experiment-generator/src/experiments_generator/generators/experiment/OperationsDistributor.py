import random
from functools import partial
from datetime import timedelta
from dataclasses import dataclass
from typing import List, Callable, Dict

from experiments.generator.models.experiment_models import HostInfo


class TimeIntervalSpec(object):
    def __init__(self, delay_interval:List[timedelta]):
        self.min_millis = self._to_millis(delay_interval[0])
        self.max_millis = self._to_millis(delay_interval[1])
        self.mean_millis = (self.min_millis + self.max_millis) / 2

    def _to_millis(self, timed:timedelta):
        return timed.total_seconds() * 1000

    def gen_random_millis(self):
        return random.randint(self.min_millis, self.max_millis)

@dataclass
class OperationsSpec(object):
    num_nodes:int
    num_operations:int
    delay_interval:List[timedelta]
    operations_generator:Callable
    operations_data:object = None

    def interval_spec(self):
        return TimeIntervalSpec(self.delay_interval)

@dataclass
class OperationArgs(object):
    node:HostInfo=None
    operation_data:object=None
    start_delay:int=None


class OperationsDistributor(object):
    def __init__(self,
        hosts,
        num_operations, delay_interval,
        operations_generator:Callable,
        operations_data=None,
    ):
        assert delay_interval is not None, 'Specified interval is None'

        self.hosts = hosts
        self.num_nodes = len(hosts)
        self.num_operations = num_operations
        self.delay_spec = TimeIntervalSpec(delay_interval)

        self.operations_generator = operations_generator
        self.operations_data = operations_data

        self.op_distribution = partial(random.gauss, num_operations/self.num_nodes, 1)
        self.operations_map = {h.name:[] for h in hosts}

        self.excluded_nodes_idxs = set()
        self.remaining_ops = self.num_operations

    def generate(self) -> Dict:
        excluded_nodes = set()
        remaining_ops = self.num_operations

        while remaining_ops > 0 and len(excluded_nodes) < self.num_nodes:
            remaining_ops -= self._distribute_and_generate_remaining_ops(
                remaining_ops, excluded_nodes
            )

        return self.operations_map

    def _distribute_and_generate_remaining_ops(self,
        remaining_ops, excluded_nodes
    ):
        distribution = self.distribute_operations_by_nodes(
            remaining_ops, excluded_nodes
        )

        allocated_ops = 0

        for i in range(self.num_nodes):
            num_operations = distribution[i]

            node_ops = self.add_operations_for_node(
                self.hosts[i], num_operations
            )

            if node_ops == 0 and num_operations > 0:
                excluded_nodes.add(i)

            allocated_ops += node_ops

        return allocated_ops

    def distribute_operations_by_nodes(self,
        remaining_ops=None, excluded_nodeidxs=None
    ):
        if remaining_ops is None: remaining_ops = self.num_operations
        if not excluded_nodeidxs: excluded_nodeidxs = set()

        operations_count = []

        for i in range(self.num_nodes):
            if i in excluded_nodeidxs:
                operations_count.append(0)
                continue

            num_node_operations = int(self.op_distribution())
            num_node_operations = min(remaining_ops, num_node_operations)
            num_node_operations = max(num_node_operations, 0)
            remaining_ops -= num_node_operations

            operations_count.append(num_node_operations)

        self.distribute_remaining_operations(
            remaining_ops, operations_count, excluded_nodeidxs
        )

        return operations_count

    def distribute_remaining_operations(self,
        remaining_ops, operations_count, excluded_nodeidxs
    ):
        if len(excluded_nodeidxs) == self.num_nodes:
            # no valid nodes
            return

        while remaining_ops > 0:
            nodeidx = random.randrange(0, self.num_nodes)

            if nodeidx not in excluded_nodeidxs:
                operations_count[nodeidx] += 1
                remaining_ops -= 1

    def add_operations_for_node(self, node, num_operations):
        operations = self.gen_node_operations(
            node, num_operations
        )

        self.operations_map[node.name].extend(operations)

        return len(operations)

    def gen_node_operations(self, node, num_operations):
        operations = []

        for i in range(num_operations):
            op = self.gen_operation(node)

            if op: operations.append(op)

        return operations

    def gen_operation(self, node):
        return self.operations_generator(OperationArgs(
            node=node,
            operation_data=self.operations_data,
            start_delay=self.gen_delay_time()
        ))

    def gen_delay_time(self):
        return self.delay_spec.gen_random_millis()


