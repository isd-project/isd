import random
from functools import partial
from datetime import timedelta
from dataclasses import dataclass
from typing import List, Callable, Dict

from experiments.generator.models.experiment_models import *
from experiments.model import SearchSpec

from . import PublicationsDistribution, SearchesDistribution, SearchesSpec


class ExperimentOperationsGenerator(object):

    def generate_experiment_operations(
        self, params:ExperimentParameters, hosts: List[HostInfo]
    ):
        p = params

        publications = self.distribute_publications(
            hosts, p.num_services, p.delay_interval, p.srvtypes
        )
        searches = self.distribute_searches(
            hosts, p.num_searches, p.num_localizations, p.delay_interval, publications
        )

        return self.make_experiment_operations(
            params, hosts, publications, searches
        )

    def distribute_publications(
        self, hosts, num_services, delay_interval, srvtypes=None
    ):
        distr = PublicationsDistribution(
            hosts, num_services, delay_interval, srvtypes
        )

        return distr.generate()

    def distribute_searches(self,
        hosts, num_searches, num_localizations, delay_interval, publications,
    ):
        distr = SearchesDistribution(SearchesSpec(
            num_searches=num_searches,
            num_localizations=num_localizations,
            delay_interval=delay_interval,
            publications=publications, hosts=hosts
        ))

        return distr.generate()

    def make_experiment_operations(self, params, hosts, publications, searches):
        def merge_operations(host, nodes_publications, nodes_searches):
            return NodeOperations(
                node=host,
                publications=nodes_publications[host.name],
                searches=nodes_searches[host.name]
            )

        nodes_ops = {}

        for h in hosts:
            nodes_ops[h.name] = merge_operations(h, publications, searches)

        return ExperimentOperations(
            nodes_operations=nodes_ops
        )



