import random
from typing import Dict

from experiments.generator.models.experiment_models import *
from .OperationsDistributor import OperationsDistributor


class PublicationsDistribution(object):
    def __init__(self,
        hosts, num_services, delay_interval, srvtypes=None
    ):
        if srvtypes is None: srvtypes = ['srvtype']

        self.services_per_node = {}

        self.distr = OperationsDistributor(
            hosts,
            num_services, delay_interval,
            self._publication_generator, srvtypes,
        )

    def generate(self)->Dict[str,ServicePublication]:
        return self.distr.generate()

    def _publication_generator(self, op_args)->ServicePublication:
        node = op_args.node
        srvtypes = op_args.operation_data

        nodes_srvs = self.services_per_node.get(node.name, 0)

        pub = ServicePublication(
            node=node,
            name=f'srv{nodes_srvs}_{node.name}',
            port=random.randint(1024, 65535),
            type=random.choice(srvtypes),
            delay=op_args.start_delay
        )

        self.services_per_node[node.name] = nodes_srvs + 1

        return pub
