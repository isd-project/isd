from experiments_generator.models import *
from experiments.model.topology_models import *
from .NetworkGenerator import NetworkGenerator
from .NodeGenerator import NodeGenerator


class TopologyGenerator(object):
    def gen_experiment_topology(self, spec:TopologySpec):
        builder = ExperimentTopologyBuilder(spec)
        return builder.generate()

class ExperimentTopologyBuilder(object):
    def __init__(self, spec: TopologySpec):
        self.spec = spec
        self.topo = Topology()

        self.netGen = NetworkGenerator()
        self.nodeGen = NodeGenerator()

    def generate(self):
        self.add_backbone()
        self.add_local_nodes_and_networks()
        self.add_bootstrapper()

        return self.topo

    def add_backbone(self):
        self.backbone = self.add_router(name='backbone', use_nat=False)
        self.topo.add_node(self.backbone)

    def add_bootstrapper(self):
        bootstrapper_net = self.add_network(NetTypes.WAN)
        self.add_node(NodeType.server, bootstrapper_net, name='bootstrapper')

        # Connect to backbone
        self.backbone.add_network(bootstrapper_net)

    def add_local_nodes_and_networks(self):
        num_nets = self.spec.networks
        num_hosts = self.spec.hosts

        hosts_per_network = int(num_hosts / num_nets)
        remainder = num_hosts % num_nets

        for i in range(self.spec.networks):
            h = hosts_per_network + (1 if remainder  else 0)
            if(remainder > 0): remainder -= 1

            self.add_local_network(self.backbone, h)

    def add_local_network(self, backbone, num_hosts):
        lan = self.add_network(NetTypes.LAN)
        wan_link = self.add_network(NetTypes.WAN)

        backbone.add_network(wan_link)

        self.add_router([wan_link, lan])
        self.add_node(NodeType.switch, lan)

        for i in range(num_hosts):
            self.add_node(NodeType.host, lan)

    def add_network(self, nettype):
        net = self.netGen.gen_next(nettype)

        self.topo.add_network(net)

        return net

    def add_router(self, networks=None, name=None, use_nat=True):
        if not networks:
            networks = []

        extra_srvs = ['NAT'] if use_nat else None

        return self.add_node(NodeType.router, *networks, name=name, extra_services=extra_srvs)

    def add_node(self, nodetype, *networks, name=None, extra_services=None):
        node = self.nodeGen.gen_node(nodetype, name=name, extra_services=extra_services)

        for net in networks:
            node.add_network(net)

        self.topo.add_node(node)

        return node