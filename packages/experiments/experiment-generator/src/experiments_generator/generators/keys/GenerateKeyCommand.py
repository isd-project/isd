from dataclasses import dataclass
from typing import List

from experiments_generator.models.key_models import *
from experiments.utils import nested_dataclass

import json
import re
import subprocess


class GenerateKeyCommand(object):

    def __init__(self, cli_cmd=None, executor=None):
        if executor is None:
            executor = self._execute_command
        if cli_cmd is None:
            cli_cmd = ['discovery']

        self.cli_cmd = cli_cmd
        self.executor = executor

    def _execute_command(self, args:List[str]):
        return subprocess.check_output(args, encoding='utf-8')

    def generate_key(self, keytype:KeyTypes, keysdir:str):
        kt = self._keytype_arg(keytype)
        args = [
            *self.cli_cmd,
            'keys', 'gen', '-t', kt, '--keysdir', keysdir, '--json'
        ]

        result_str = self.executor(args)
        result_json = json.loads(result_str)
        result = KeyGenResult.from_dict(result_json)

        return result.to_key_info(keysdir)


    def _keytype_arg(self, keytype: KeyTypes):
        return {
            KeyTypes.ec: 'ec',
            KeyTypes.rsa: 'rsa',
        }.get(keytype)


@nested_dataclass
class KeyGenResult(object):
    type:str=''
    fingerprint:str=''
    publicKey:PublicKey=None

    @classmethod
    def from_dict(self, dict_values):
        return KeyGenResult(**dict_values)

    def to_key_info(self, keys_dir):
        return KeyInfo(
            type=self._key_type(),
            fingerprint=self._parse_fingerprint(),
            publicKey=self.publicKey,
            keysdir=keys_dir
        )

    def _key_type(self):
        if self.type is None: return None

        return {
            'ellipticcurve': KeyTypes.ec,
            'rsa': KeyTypes.rsa
        }.get(self.type.lower())

    def _parse_fingerprint(self):
        if self.fingerprint is None: return None

        m = re.match('(.*):(.*):(.*)', self.fingerprint)

        if m:
            return Fingerprint(
                hash_alg=m.group(1),
                hash_fmt=m.group(2),
                hash=m.group(3)
            )