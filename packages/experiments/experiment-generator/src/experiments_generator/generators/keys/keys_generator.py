import random
from os import path
from dataclasses import dataclass
from typing import List, Tuple

from experiments_generator.models.key_models import KeyTypes, KeyInfo, NodeKey
from experiments.generator.models import HostInfo
from experiments_generator.persistence import KeysDb
from experiments.utils.filesystem import Fs

from .GenerateKeyCommand import GenerateKeyCommand

@dataclass()
class KeySpec(object):
    node:str=None
    keytype:KeyTypes=None

    def make_node_key(self, keyinfo:KeyInfo)->NodeKey:
        return NodeKey(
            node=self.node,
            keyinfo=keyinfo
        )


class KeysGenerator(object):
    def __init__(self, keygen_command=None, keysdb=None, fs=None):
        if not keygen_command: keygen_command = GenerateKeyCommand()
        if not keysdb:         keysdb = KeysDb()
        if not fs:             fs = Fs()

        self.keygen_command = keygen_command
        self.keysdb = keysdb
        self.fs = fs

    def generate_keys(self, nodes:List[str], out_dir:str):
        specs = self.generate_key_specs(nodes, out_dir)
        provided, missing = self.provide_key_specs(specs, out_dir)

        remaining = self.generate_keys_from_specs(missing, out_dir)

        self.save_node_keys(remaining, out_dir)

        return provided + remaining


    def generate_key_specs(self, nodes:List[str], out_dir:str):
        specs = []

        keytypes = self._distribute_key_types(len(nodes))

        for i in range(len(nodes)):
            node = nodes[i]
            kt = keytypes[i]
            specs.append(self._gen_spec(node, kt))

        return specs

    def _distribute_key_types(self, num_keys):
        options = [KeyTypes.ec, KeyTypes.rsa]
        population = options + [
            random.choice(options) for i in range(num_keys - 2)
        ]

        return random.sample(population, num_keys)

    def _gen_spec(self, node, keytype):
        return KeySpec(
            node=node,
            keytype=keytype
        )

    def provide_key_specs(self, key_specs, out_dir):
        keysinfo = self.keysdb.load_all(out_dir)

        return self.match_keys(key_specs, keysinfo)

    def match_keys(self, key_specs, keysinfo):
        keys_by_type = {
            t:[k for k in keysinfo if k.type == t]
                for t in (KeyTypes.values())
        }

        matching_keys, missing_specs = [], []

        for spec in key_specs:
            keys = keys_by_type[spec.keytype]
            matching_key = keys.pop(0) if keys else None

            if matching_key:
                matching_keys.append(spec.make_node_key(matching_key))
            else:
                missing_specs.append(spec)


        return matching_keys, missing_specs

    def generate_keys_from_specs(self, key_specs:List[KeySpec], out_dir):
        if not key_specs: return []

        self.fs.ensure_dir(out_dir)
        gen_keys = []

        for spec in key_specs:
            keysdir = self.gen_keysdir(spec, out_dir)
            ki = self.keygen_command.generate_key(spec.keytype, keysdir)

            gen_keys.append(NodeKey(
                node=spec.node,
                keyinfo=ki
            ))

        return gen_keys

    def gen_keysdir(self, spec, out_dir):
        prefix = spec.keytype.value

        dirfiles = self.fs.listdir(out_dir)
        last_keysdir = self._last_file_starting_with(prefix, dirfiles)

        sufixnum = self._sufixnumber(last_keysdir, prefix)
        dirnum = 0 if sufixnum is None else sufixnum + 1

        return path.join(out_dir, f'{prefix}{dirnum}')

    def _last_file_starting_with(self, prefix, dirfiles):
        if not dirfiles: return None

        files = sorted([f for f in dirfiles if f.startswith(prefix)])

        return files[-1] if files else None

    def _sufixnumber(self, file, prefix):
        if not file: return None

        prefix_len = len(prefix)
        sufix = file[prefix_len:] #remove prefix

        if sufix and sufix.isnumeric():
            return int(sufix)

        return None

    def save_node_keys(self, node_keys, out_dir):
        keys_to_save = [nk.keyinfo for nk in node_keys]

        self.keysdb.save_all(keys_to_save, out_dir)