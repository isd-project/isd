from experiments_generator.models import *
from experiments.model.topology_models import NodeType, Node

from .NameGenerator import NameGenerator


class NodeGenerator(object):

    def __init__(self):
        self.id_counter = 0

        self.name_gen = NameGenerator({
            NodeType.host: 'n',
            NodeType.router: 'r',
            NodeType.switch: 's',
            NodeType.server: 'srv'
        })

    def gen_node(self, nodetype, name=None, extra_services=None):
        if name is None:
            name = self._gen_name(nodetype)

        id = self._next_id()

        return Node(id=id, name=name, nodetype=nodetype,
                        extra_services=extra_services)

    def _next_id(self):
        self.id_counter += 1

        return self.id_counter

    def _gen_name(self, nodetype):
        return self.name_gen.gen_next(nodetype)

    def gen_host(self):
        return Node(name='n0', nodetype=NodeType.host)

    def gen_switch(self):
        return Node(name='s0', nodetype=NodeType.switch)

    def gen_router(self, name=None, use_nat=True):
        if name is None:
            name = 'r0' #TO-DO: generate

        return Node(name=name, nodetype=NodeType.router)
