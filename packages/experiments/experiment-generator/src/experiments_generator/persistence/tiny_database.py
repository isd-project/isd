from os import path
from tinydb import TinyDB

from experiments.utils.filesystem import Fs

from experiments_generator.models.key_models import KeyInfo


class KeysTinyDb(object):

    def __init__(self):
        self.fs = Fs()

    def dbfile(self, db_dir, filename='keysdb.json'):
        return path.join(db_dir, filename)

    def save_all(self, keys_info, db_dir):
        keysdb = self.getDb(db_dir, ensure_dir=True)

        keysdict = [k.asdict() for k in keys_info]

        keysdb.insert_multiple(keysdict)


    def load_all(self, db_dir):
        keysdb = self.getDb(db_dir)

        if not keysdb: return []

        allkeys = keysdb.all()

        return [KeyInfo.from_dict(k) for k in allkeys]

    def getDb(self, db_dir, ensure_dir=False):
        if ensure_dir:
            self.fs.ensure_dir(db_dir)

        try:
            return TinyDB(self.dbfile(db_dir))
        except FileNotFoundError:
            return None