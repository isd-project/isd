from experiments_generator.exporter.core_model import *

from experiments_generator.exporter import defaults
from experiments_generator.models import *
from experiments.model.topology_models import *


from .NodesPositioner import NodesPositioner
from .InterfacesGenerator import InterfacesGenerator


class CoreExporter(object):
    def export_topology(self, topo):
        return TopologyExporter(topo).export()

class TopologyExporter(object):

    def __init__(self, topo):
        self.topo = topo

        self.nodecounter = 0

        self.positioner = NodesPositioner()
        self.ifaces_gen = InterfacesGenerator()

    def export(self):
        self.nodes_positions = self.positioner.position_topology(self.topo)

        scenario = defaults.default_core_scenario()

        self.export_nodes(scenario)
        self.export_network_devices(scenario)
        self.export_links(scenario)


        return scenario

    def export_nodes(self, scenario):
        for n in self.topo.nodes:
            if n.nodetype != NodeType.switch:
                device = self.node_to_device(n)
                scenario.add_device(device)

    def export_network_devices(self, scenario):
        for switch in self.topo.switches():
            scenario.add_network_device(self.network_device(switch))

    def export_links(self, scenario):
        links = self.ifaces_gen.generate_links(self.topo)

        scenario.links = links

    def node_to_device(self, node):
        return Device(
            id=node.id,
            name=node.name,
            devtype=self.node_type_to_string(node.nodetype),
            services=self.gen_services(node),
            position=self.get_node_position(node.name)
        )

    def network_device(self, switch):
        return NetworkDevice(
            id=switch.id,
            name=switch.name,
            devtype='SWITCH',
            position=self.get_node_position(switch.name)
        )

    def gen_services(self, node):
        srvs = defaults.nodes_services(node.nodetype)

        if node.extra_services:
            for srv_name in node.extra_services:
                srvs.append(Service(name=srv_name))

        return srvs

    def gen_nodeid(self):
        self.nodecounter += 1
        id = self.nodecounter

        return id

    def node_type_to_string(self, nodetype):
        return {
            NodeType.host: 'PC',
            NodeType.router: 'router',
            NodeType.switch: 'switch',
            NodeType.server: 'host'
        }.get(nodetype)

    def get_node_position(self, node_name):
        return self.nodes_positions.get(node_name)