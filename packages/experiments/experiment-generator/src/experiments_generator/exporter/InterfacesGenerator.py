import re

from dataclasses import dataclass, field
from typing import List
from ipaddress import IPv4Network, IPv6Network


from experiments_generator.models import *
from experiments_generator.exporter.core_model import *
from experiments.model.topology_models import *

from experiments_generator.generators import MacGenerator


class InterfacesGenerator(object):

    def __init__(self):
        self.mac_gen = MacGenerator()

    @staticmethod
    def wanLinkOptions():
        return LinkOptions(
            delay=160,
            bandwidth=10000000,
            loss=0.0, dup=0, jitter=0, unidirectional=0
        )

    @staticmethod
    def lanLinkOptions()->LinkOptions():
        # unlimited
        return LinkOptions()

    def generate_links(self, topo):
        conn_map = self.generate_node_connections(topo)
        return self.generate_links_from_iface_map(topo, conn_map)

    def generate_links_from_iface_map(self, topo, ifaces_map):
        links = []
        connected_pairs = set([])

        for node_src in sorted(ifaces_map.keys()):
            connections = ifaces_map[node_src]

            for node_dst in sorted(connections.keys()):
                pair = frozenset([node_src, node_dst])

                if pair in connected_pairs:
                    continue
                connected_pairs.add(pair)

                links.append(self.gen_link(node_src, node_dst, topo, ifaces_map))

        return links

    def gen_link(self,node_src, node_dst, topo, ifaces_map):
        # node_src -> node_dst
        conn1 = ifaces_map.get(node_src, {}).get(node_dst)
        # node_dst -> node_src
        conn2 = ifaces_map.get(node_dst,{}).get(node_src)

        if conn1 is None:
            conn1 = self._connection_from_node(node_src, topo)
        if conn2 is None:
            conn2 = self._connection_from_node(node_dst, topo)

        link_opt = self.lanLinkOptions()

        if(conn1.net_type == NetTypes.WAN or conn2.net_type == NetTypes.WAN):
            link_opt = self.wanLinkOptions()

        return Link(
            node1=conn1.nodeid,
            iface1=conn1.iface,
            node2=conn2.nodeid,
            iface2=conn2.iface,
            options=link_opt
        )

    def _connection_from_node(self, node_name, topo):
        node = topo.get_node(node_name)

        if node is None: return ConnectionInfo()

        return ConnectionInfo(
            nodeid=node.id
        )

    def generate_node_connections(self, topo):
        ifaces_map = {}
        addr_generators = {}

        def sort_by_node_type_and_name(node):
            type_ranking_map = {
                NodeType.router:0,
                NodeType.server:1,
                NodeType.host:3,
                NodeType.switch:4
            }

            rank = type_ranking_map.get(node.nodetype)

            return (rank, node.name)

        for node in sorted(topo.nodes, key=sort_by_node_type_and_name):
            if node.nodetype == NodeType.switch:
                continue

            self._add_node_interfaces(node, topo, ifaces_map, addr_generators)

        return ifaces_map

    def _add_node_interfaces(self, node, topo, ifaces_map, addr_generators):
        for net_name in node.networks:
            net = topo.get_network(net_name)

            iface = self._gen_iface(node, net, ifaces_map, addr_generators)
            connection = ConnectionInfo(
                nodeid=node.id,
                iface=iface,
                net_name=net_name,
                net_type=net.nettype
            )

            target = self._get_connection_target(node, net)
            self._add_connection(node, target, connection, ifaces_map)

    def _get_connection_target(self, src_node:Node, net:Network)->str:
        target = None

        for node_name in net.nodes:
            if node_name == src_node.name:
                continue

            if re.match('s\\d', node_name): #switch
                # Lan: everyone connects to switch
                return node_name

            # connect to router if switch not found
            # or if src_node is router, may connect to other nodes
            elif node_name.startswith('r') \
                or target is None:
                target = node_name

        return target


    def _get_connection_targets(self, src_node:Node, net:Network)->List[str]:
        targets = []

        for node_name in net.nodes:
            if node_name == src_node.name:
                continue

            if re.match('s\\d', node_name): #switch
                # Lan: everyone connects to switch
                return [node_name]
            elif src_node.nodetype == NodeType.router \
                or node_name.startswith('r'): #router
                # router connects to everyone (if no switch)
                targets.append(node_name)

        return targets

    def _gen_iface(self, node:Node, net:Network, ifaces_map, addr_generators):
        iface_id = 0

        if node.name in ifaces_map:
            iface_id = len(ifaces_map[node.name])

        ip4, ip4_mask = self._gen_address(net.ip_prefixes.ip4, addr_generators)
        ip6, ip6_mask = self._gen_address(net.ip_prefixes.ip6, addr_generators)

        return DeviceInterface(
            id=iface_id,
            name=f'eth{iface_id}',
            ip4=ip4,
            ip4_mask=ip4_mask,
            ip6=ip6,
            ip6_mask=ip6_mask,
            mac=self.mac_gen.next()
        )

    def _gen_address(self, ip_network, addr_generators):
        net_str = str(ip_network)
        hosts_gen = addr_generators.get(net_str)

        if hosts_gen is None:
            addr_generators[net_str] = hosts_gen = ip_network.hosts()

        addr = str(next(hosts_gen))
        mask = str(ip_network.prefixlen)

        return addr, mask


    def _add_connection(self, node, target, connection, ifaces_map):
        src = node.name
        if src not in ifaces_map:
            ifaces_map[src] = {}

        ifaces_map[src][target] = connection

@dataclass
class ConnectionInfo(object):
    nodeid:int = None
    iface:DeviceInterface=None
    net_name:str = None
    net_type:NetTypes = None
