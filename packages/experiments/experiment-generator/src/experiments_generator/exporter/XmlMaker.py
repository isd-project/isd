from lxml import objectify, etree

class XmlMaker(objectify.ElementMaker):
    def __init__(self):
        super().__init__(annotate=False, namespace='')

    def object_to_xml(self, obj):
        if obj is None: return None

        return obj.to_xml(self)

    def tag(self, tag_name, *children, **attrs):
        children = [c for c in children if c is not None]
        filtered_attrs= {
            key:(value if value is not None else '')
            for key, value in attrs.items()
        }

        return self.__getattr__(tag_name)(*children, **filtered_attrs)

    def children_list(self, tag, children):
        return self(tag, *self.children(children))

    def children(self, children, **named_children):
        if children is None: children = []

        xml_children = [c.to_xml(self) for c in children]

        for name, child in named_children.items():
            if child is None:
                continue

            xml_child = child.to_xml(self)
            xml_child.tag = name

            xml_children.append(xml_child)


        return xml_children
