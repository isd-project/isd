from .core_model import *
from experiments_generator.models import *
from experiments.model.topology_models import NodeType

import re


class NodesPositioner(object):
    def col_size(self):
        return 300
    def row_size(self):
        return 70
    def vertical_gap(self):
        return 300
    def margin(self):
        return 100

    def position_topology(self, topo):
        positions = {}

        for node in topo.nodes:
            positions[node.name] = self.position(node, topo)

        return positions

    def position(self, node, topo=None):
        if node.nodetype == NodeType.router:
            return self.position_router(node)
        elif node.nodetype == NodeType.host:
            return self.position_host(node, topo)
        elif node.nodetype == NodeType.switch:
            return self.position_switch(node, topo)
        elif node.nodetype == NodeType.server:
            return self.position_server(node)

        return None

    def position_router(self, router):
        if self.is_backbone(router):
            num_nets = len(router.networks)
            total_width = self.col_size() * (num_nets - 1)
            x =  (total_width/2) + self.margin()
            y = self.margin()

            return Position(x=x, y=y)
        else:
            return self.position_level2(router)

    def position_server(self, server):
        return self.position_level2(server)

    def position_level2(self, node):
        num = self.wan_number(node)

        x = (self.col_size() * num) + self.margin()
        y = self.vertical_gap()

        return Position(x=x, y=y)

    def position_host(self, node, topo):
        lan_num = self.lan_number(node)

        x = (lan_num + 0.5) * self.col_size() + self.margin()
        y = self.vertical_gap() + self.node_row(node, topo) * self.row_size()

        return Position(x=x, y=y)

    def position_switch(self, node, topo):
        lan_num = self.lan_number(node)

        num_hosts = self.num_hosts('lan'+str(lan_num), topo)
        row = 1 + (num_hosts-1)/2

        x = lan_num * self.col_size() + self.margin()
        y = self.vertical_gap() + row * self.row_size()

        return Position(x=x, y=y)

    def is_backbone(self, node):
        for net in node.networks:
            if net.startswith('lan'):
                return False

        return True

    def wan_number(self, node):
        return self.net_number(node, 'wan')

    def lan_number(self, node):
        return self.net_number(node, 'lan')

    def net_number(self, node, prefix):
        for net in node.networks:
            m = re.match(prefix + '(\\d)', net)

            if m is not None:
                return int(m.group(1))

        return 0

    def node_row(self, node, topo):
        lan_name = 'lan' + str(self.lan_number(node))

        net = topo.get_network(lan_name)
        idx = self.get_host_index(node, net)

        return idx + 1

    def get_host_index(self, node, net):
        hosts = sorted(self.net_hosts(net))

        return hosts.index(node.name)

    def num_hosts(self, net_name, topo):
        net = topo.get_network(net_name)

        hosts = self.net_hosts(net)

        return len(hosts)

    def net_hosts(self, net):
        return [n for n in net.nodes if n.startswith('n')]
