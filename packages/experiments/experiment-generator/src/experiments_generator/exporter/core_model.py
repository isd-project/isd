from dataclasses import dataclass, field
from typing import Dict, List, Set

@dataclass
class GlobalPosition(object):
    lat:float=0.0
    lon:float=0.0
    alt:float=2.0

@dataclass
class Position(GlobalPosition):
    x:int=0
    y:int=0

    def to_xml(self, xml_maker):
        return xml_maker.position(
            x = str(self.x),
            y = str(self.y),
            lat = str(self.lat),
            lon = str(self.lon),
            alt = str(self.alt)
        )

@dataclass
class SessionOrigin(GlobalPosition):
    scale:float=150.0

    def to_xml(self, xml_maker):
        return xml_maker.session_origin(
            lat = str(self.lat),
            lon = str(self.lon),
            alt = str(self.alt),
            scale = str(self.scale)
        )


@dataclass
class Configuration(object):
    name:str=''
    value:str=''

    def to_xml(self, xml_maker):
        return xml_maker.configuration(
            name=self.name,
            value=self.value
        )

@dataclass
class Service(object):
    name:str=None

    def to_xml(self, xml_maker):
        return xml_maker.service(name=self.name)

@dataclass
class NodeServices(object):
    nodetype:str=None
    services:List[Service]=None

    def to_xml(self, xml_maker):
        return xml_maker.tag('node',
            type=self.nodetype,
            *xml_maker.children(self.services)
        )

@dataclass
class EmulatorConfig(object):
    def to_xml(self, xml_maker):
        return xml_maker.emulator()

@dataclass
class EmaneConfiguration(object):
    emulator:EmulatorConfig = field(default_factory=EmulatorConfig)
    core:List[Configuration] = field(default_factory=list)

    def to_xml(self, xml_maker):
        return xml_maker.emane_global_configuration(
            self.emulator.to_xml(xml_maker),
            xml_maker.children_list('core', self.core)
        )

@dataclass
class BaseDevice(object):
    id:int=None
    name:str=None
    devtype:str=None

    position:Position=None



@dataclass
class NetworkDevice(BaseDevice):

    def to_xml(self, xml_maker):
        return xml_maker.tag('network',
            self.position.to_xml(xml_maker) if self.position else None,

            id=str(self.id),
            name=self.name,
            type=self.devtype
        )


@dataclass
class Device(BaseDevice):
    devclass:str=''
    image:str=''

    services:List[Service]=field(default_factory=list)

    def to_xml(self, xml_maker):
        return xml_maker.tag('device',
            xml_maker.object_to_xml(self.position),
            xml_maker.children_list('services', self.services),

            id=str(self.id),
            name=self.name,
            type=self.devtype,
            image=self.image,
            **{'class':self.devclass}
        )


@dataclass
class LinkOptions(object):
    delay:int=0
    bandwidth:int=0
    loss:float=0.0
    dup:int=0
    jitter:int=0
    unidirectional:int=0

    def to_xml(self, xml_maker):
        return xml_maker.options(
            delay=str(self.delay),
            bandwidth=str(self.bandwidth),
            loss=str(self.loss),
            dup=str(self.dup),
            jitter=str(self.jitter),
            unidirectional=str(self.unidirectional)
        )

@dataclass
class DeviceInterface(object):
    id:int=0 # interface ID
    name:str="eth0"
    mac:str=None
    ip4:str=None
    ip4_mask:str=None
    ip6:str=None
    ip6_mask:str=None

    def to_xml(self, xml_maker):
        return xml_maker.tag('iface',
            id=str(self.id), name=self.name, mac=self.mac, ip4=self.ip4, ip4_mask=self.ip4_mask, ip6=self.ip6, ip6_mask=self.ip6_mask
        )

@dataclass
class Link(object):
    node1:int=None
    node2:int=None
    iface1:DeviceInterface=None
    iface2:DeviceInterface=None
    options:LinkOptions=field(default_factory=LinkOptions)

    def to_xml(self, xml_maker):
        link_xml = xml_maker.link(
            node1=str(self.node1),
            node2=str(self.node2),

            *xml_maker.children([],
                iface1=self.iface1,
                iface2=self.iface2
            ),
            *xml_maker.children([self.options])
        )

        return link_xml

@dataclass
class CoreScenario(object):
    networks:List[NetworkDevice] = field(default_factory=list)
    devices:List[Device] = field(default_factory=list)
    links:List[Link] = field(default_factory=list)
    emane_global_configuration:EmaneConfiguration = field(default_factory=EmaneConfiguration)
    session_origin:SessionOrigin = field(default_factory=SessionOrigin)
    session_options:List[Configuration] = field(default_factory=list)
    session_metadata:List[Configuration] = field(default_factory=list)
    default_services:List[NodeServices] = field(default_factory=list)

    def add_device(self, device):
        self.devices.append(device)

    def add_network_device(self, netdevice):
        self.networks.append(netdevice)

    def gen_interfaces_map(self):
        ifaces_id_map = {}

        for l in self.links:
            for nodeid, iface in ((l.node1, l.iface1), (l.node2, l.iface2)):
                if not iface or nodeid is None: continue

                ifaces = ifaces_id_map.get(nodeid, list())
                ifaces.append(iface)

                ifaces_id_map[nodeid] = ifaces

        return {
            dev.name: ifaces_id_map.get(dev.id, list())
                for dev in self.devices
        }

    def to_xml(self, xml_maker):
        return xml_maker.scenario(
            xml_maker.children_list('networks', self.networks),
            xml_maker.children_list('devices', self.devices),
            xml_maker.children_list('links', self.links),
            self.emane_global_configuration.to_xml(xml_maker),
            self.session_origin.to_xml(xml_maker),
            xml_maker.children_list('session_options', self.session_options),
            xml_maker.children_list('session_metadata', self.session_metadata),
            xml_maker.children_list('default_services', self.default_services)
        )