from .core_model import *

from experiments.model import NodeType


def default_core_scenario()->CoreScenario:
    return CoreScenario(
        default_services=default_services(),
        session_metadata=default_session_metadata(),
        session_options=default_session_options(),
        emane_global_configuration=default_emane_config()
    )


def default_services():
    return [
        NodeServices(nodetype='mdr', services=[
            Service(name='zebra'),
            Service(name="OSPFv3MDR"),
            Service(name="IPForward")
        ]),
        NodeServices(nodetype="PC", services=[
            Service(name="DefaultRoute")
        ]),
        NodeServices(nodetype="prouter"),
        NodeServices(nodetype="router", services=[
            Service(name="zebra"),
            Service(name="OSPFv2"),
            Service(name="OSPFv3"),
            Service(name="IPForward")
        ]),
        NodeServices(nodetype="host", services=[
            Service(name="DefaultRoute"),
            Service(name="SSH")
        ]),
    ]

def default_session_metadata():
    return [
        Configuration(name="canvas c1", value="{name {Canvas1}}"),
        Configuration(name="global_options", value="interface_names=no ip_addresses=yes ipv6_addresses=yes node_labels=yes link_labels=yes show_api=no background_images=no annotations=yes grid=yes traffic_start=0")
    ]

def default_session_options():
    return [
        Configuration(name="controlnet", value=""),
        Configuration(name="controlnet0", value=""),
        Configuration(name="controlnet1", value=""),
        Configuration(name="controlnet2", value=""),
        Configuration(name="controlnet3", value=""),
        Configuration(name="controlnet_updown_script", value=""),
        Configuration(name="enablerj45", value="1"),
        Configuration(name="preservedir", value="0"),
        Configuration(name="enablesdt", value="0"),
        Configuration(name="sdturl", value="tcp://127.0.0.1:50000/"),
        Configuration(name="ovs", value="0"),
    ]

def default_emane_config():
    return EmaneConfiguration(
        core=[
            Configuration(name="platform_id_start", value="1"),
            Configuration(name="nem_id_start", value="1"),
            Configuration(name="link_enabled", value="1"),
            Configuration(name="loss_threshold", value="30"),
            Configuration(name="link_interval", value="1"),
            Configuration(name="link_timeout", value="4"),
        ]
    )

def nodes_services(nodetype:NodeType):
    return {
        NodeType.router: [
            Service(name="OSPFv2"),
            Service(name="OSPFv3"),
            Service(name="zebra"),
            Service(name="IPForward")
        ],
        NodeType.host: [
            Service(name='DefaultRoute'),
            # Service(name='UserDefined')
        ],
        NodeType.server: [
            Service(name='DefaultRoute'),
            Service(name='SSH')
        ],
    }.get(nodetype).copy()