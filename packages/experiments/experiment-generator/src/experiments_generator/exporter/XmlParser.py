from .XmlMaker import XmlMaker

from lxml import etree, objectify

from io import StringIO


class XmlParser(object):
    def __init__(self):
        self.xml_maker = XmlMaker()

    def serialize_to_string(self, obj)->str:
        xml_obj = self.xml_maker.object_to_xml(obj)

        return self.xml_to_string(xml_obj)

    def serialize_to_file(self, obj, out_file):
        xml_text = self.serialize_to_string(obj)

        with open(out_file, 'w') as f:
            f.write(xml_text)

    def xml_to_string(self, xml_obj, pretty_print=True)->str:
        return etree.tostring(xml_obj, pretty_print=pretty_print, encoding=str)

    def parse(self, xml_text):
        return objectify.fromstring(xml_text)