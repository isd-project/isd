import dataclasses
from dataclasses import dataclass
from enum import Enum

from experiments.generator.models import HostInfo
from experiments.utils import nested_dataclass


class KeyTypes(Enum):
    ellipticCurves = 'ec'
    ec = ellipticCurves # alias
    rsa = 'rsa'

    @classmethod
    def values(cls):
        return (cls.ec, cls.rsa)

    @classmethod
    def from_value(cls, value):
        for e in cls.values():
            if e.value == value:
                return e

        return None

@dataclass()
class Fingerprint(object):
    hash_alg:str=None
    hash_fmt:str=None
    hash:str=None

@dataclass()
class PublicKey(object):
    format:str=None
    data:object=None

@nested_dataclass()
class KeyInfo(object):
    type: KeyTypes=None
    fingerprint: Fingerprint=None
    publicKey:PublicKey=None
    keysdir:str = None


    @classmethod
    def from_dict(cls, dict_values):
        fields = [f.name for f in dataclasses.fields(cls)]
        filtered = {
            k: v for k, v in dict_values.items()
            if k in fields
        }

        return cls(**filtered)

    def __post_init__(self):
        if self.type and not isinstance(self.type, KeyTypes):
            self.type = KeyTypes.from_value(self.type)

    def asdict(self):
        def fix_value(k, v):
            if k == 'type':
                return self.type.value

            return v

        def dict_factory(keyvalues):
            return {
                k:fix_value(k, v) for k,v in keyvalues
            }

        return dataclasses.asdict(self, dict_factory=dict_factory)


@dataclass
class NodeKey(object):
    node:str=None
    keyinfo:KeyInfo=None

    def to_host_info(self)->HostInfo:
        fing = self.keyinfo.fingerprint

        return HostInfo(
            name=self.node,
            keyid=fing.hash,
            keyid_fmt=fing.hash_fmt
        )