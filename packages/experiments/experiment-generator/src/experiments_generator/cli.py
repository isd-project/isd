from .CoreNetworkGenerator import CoreNetworkGenerator
from experiments_generator.generators.experiment import ExperimentGenerator
from experiments.generator.models.experiment_models import ExperimentParameters

import click


@click.group()
def main():
    pass

@main.command()
@click.option('--hosts', default=1, help='number of hosts to generate')
@click.option('--networks', default=1,help='number of networks to generate')
@click.option('--output-path', '-o', default='network.xml', help='output network file')
def network(hosts, networks, output_path):
    cli = ExperimentsGeneratorCli()
    cli.generate_network(hosts, networks, output_path)

    click.echo(f'Generated network with {hosts} hosts, {networks} networks, at: {output_path}')


@main.command()
@click.option('--hosts','-h', default=1, help='number of hosts to generate')
@click.option('--networks','-n', default=1,help='number of networks to generate')
@click.option('--services','--srvs', default=1,help='number of overall services to publish')
@click.option('--searches','-s', default=1,help='number of overall seaches to be executed')
@click.option('--localizations','-l', default=1,help='number of overall localizations to execute')
@click.option('--replications','-r', default=1,help='number of replications to the experiment')
@click.option('--filename', help='base filename for generated config files')
@click.option('--output-dir', '-o', help='output directory to write generated files', required=True)
def experiment(hosts, networks, services, searches, localizations, replications, filename, output_dir):
    cli = ExperimentsGeneratorCli()
    files = cli.generate_experiment(ExperimentParameters(
        num_hosts=hosts,
        num_nets=networks,
        num_services=services,
        num_searches=searches,
        num_localizations=localizations
    ),
        exp_dir=output_dir,
        filename=filename,
        replications=replications,
    )

    click.echo(f'Generated experiment files:')
    click.echo('\t- ' + '\n\t- '.join(files))

class ExperimentsGeneratorCli(object):
    def __init__(self):
        self.net_gen = CoreNetworkGenerator()
        self.exp_gen = ExperimentGenerator()

    def generate_network(self, num_hosts, num_nets, output_file=None):
        self.net_gen.generate_network_file(num_hosts, num_nets, output_file)

    def generate_experiment(self,params,exp_dir, filename=None, replications=1):
        return self.exp_gen.gen_and_save_experiments(
            params, exp_dir, filename=filename,replications=replications
        )


if __name__ == '__main__':
    main()