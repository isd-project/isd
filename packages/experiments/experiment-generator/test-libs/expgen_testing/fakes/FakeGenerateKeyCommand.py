from experiments_generator.generators.keys import GenerateKeyCommand
from experiments_generator.models.key_models import *

import secrets


class FakeGenerateKeyCommand(GenerateKeyCommand):

    def __init__(self):
        self.generated_keys = []

    def generate_key(self, keytype, keysdir):
        key_info = KeyInfo(
            type=keytype,
            fingerprint=Fingerprint(
                hash=secrets.token_hex(15),
                hash_fmt='b16',
                hash_alg='SHA-256'
            ),
            publicKey=PublicKey(),
            keysdir = keysdir
        )

        self.generated_keys.append(key_info)

        return key_info