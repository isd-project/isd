from unittest.mock import Mock

from .fakes import FakeGenerateKeyCommand

from experiments_generator.generators.keys import KeysGenerator

def mock_keysdb():
    db = Mock()
    db.load_all.return_value = []

    return db

def mocked_keys_generator(keygen_command=None):
    if not keygen_command: keygen_command = FakeGenerateKeyCommand()

    return KeysGenerator(
        keygen_command = keygen_command,
        keysdb=mock_keysdb(),
        fs=mocked_fs()
    )

def mocked_fs():
    fs = Mock()
    fs.listdir.return_value = []

    return fs