import random

from experiments_generator.models.key_models import KeyInfo, KeyTypes, Fingerprint, PublicKey
from experiments.generator.models.experiment_models import HostInfo, ServicePublication
from experiments_generator.generators.keys import KeySpec


def some_keys_info():
    specs = key_specs()

    return [gen_key_from_spec(spec) for spec in specs]


def some_node_keys():
    specs = key_specs()

    return [s.make_node_key(gen_key_from_spec(s)) for s in specs]

def gen_key_from_spec(spec:KeySpec)->KeyInfo:
    return KeyInfo(
        type=spec.keytype,
        fingerprint=Fingerprint(
            hash_alg='SHA-256',
            hash_fmt='b16',
            hash=hexa_string()
        ),
        publicKey=PublicKey(),
        keysdir=f'any/keydir'+str(random.randint(1, 100))
    )

def key_specs():
    specs = []

    for n in gen_nodes_list():
        specs.append(KeySpec(
            node=n,
            keytype=random.choice([KeyTypes.ec, KeyTypes.rsa])
        ))

    return specs


def publications_for(hosts):
    num_publications = random.randint(1, len(hosts) * 2)

    publ_map = {h.name:[] for h in hosts}

    for i in range(num_publications):
        h = random.choice(hosts)
        publ = service_publication_for(h)

        publ_map[h.name].append(publ)


    return {n:publis for n,publis in publ_map.items() if publis}

def service_publication_for(host):
    return ServicePublication(
        name    = random_string(),
        type    = random_string(4, 8),
        delay   = random.randint(0, 50),
        port    = random.randint(1024, 65535),
        node    = host
    )

def hosts_info():
    nodes = gen_nodes_list()

    return [host_info_for(n) for n in nodes]

def host_info_for(node_name):
    return HostInfo(
        name= node_name,
        keyid=hexa_string(),
        keyid_fmt='b16'
    )

def gen_nodes_list():
    num_nodes = random.randint(3, 6)

    return [ f'n{i}' for i in range(num_nodes)]


def hexa_string():
    return random_string(chars='0123456789abcdef')


def random_string(minlen=6, maxlen=16, chars='abcdefghijklmnopqrstuvwxyz'):
    size = random.randint(minlen, maxlen)

    return ''.join([random.choice(chars) for i in range(size)])