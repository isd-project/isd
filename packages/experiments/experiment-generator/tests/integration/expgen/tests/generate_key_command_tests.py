from experiments_generator.generators.keys import GenerateKeyCommand
from experiments_generator.models.key_models import *
from testdata import buildpaths

from os import path

import pytest


class GenerateKeyCommandIntegrationTest(object):
    def setup_method(self):
        self.command = GenerateKeyCommand([
            'nodejs', buildpaths.cli_app_path()
        ])

    @pytest.mark.parametrize(
        "keytype",
        [
            pytest.param(KeyTypes.rsa),
            pytest.param(KeyTypes.ec),
        ],
    )
    def test_generate_key(self, tmp_path, keytype):
        # Given
        keys_dir = self.output_path(tmp_path)
        keytype = keytype

        # When
        key_info = self.command.generate_key(keytype=keytype, keysdir=keys_dir)

        # Then
        self.key_info_is_valid(key_info)
        self.key_matches_the_specification(key_info, keytype, keys_dir)
        self.key_should_be_save_on_keys_dir(key_info)


    def output_path(self, tmp_path):
        return str(tmp_path / 'keys')

    def key_info_is_valid(self, key_info):
        assert key_info is not None

        assert key_info.type in [KeyTypes.ec, KeyTypes.rsa]
        assert key_info.fingerprint is not None
        assert key_info.fingerprint.hash is not None
        assert key_info.fingerprint.hash_alg is not None
        assert key_info.fingerprint.hash_fmt is not None
        assert key_info.publicKey is not None
        assert key_info.publicKey.format is not None
        assert key_info.publicKey.data is not None
        assert key_info.keysdir is not None

    def key_matches_the_specification(self, key_info, keytype, keysdir):
        assert key_info.type == keytype
        assert key_info.keysdir == keysdir

    def key_should_be_save_on_keys_dir(self, key_info):
        keys_file = path.join(key_info.keysdir, 'keys.json')

        assert path.exists(keys_file)
