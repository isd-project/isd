from experiments_generator import cli
from experiments_generator.cli import ExperimentsGeneratorCli

from expgen_testing.fakes import FakeGenerateKeyCommand
from expgen_testing import mocks

import pytest
from click.testing import CliRunner

from unittest.mock import patch


class CliTests(object):

    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.runner = CliRunner()

        self.tmp_dir = tmp_path
        self.tmp_dir.mkdir(exist_ok=True)

    def test_cli_gen_network(self):
        out_path = self.tmp_dir / 'outfile.xml'

        assert not out_path.exists()

        result = self.runner.invoke(cli.main,
            ['network', '--hosts', '1', '--networks', '1', '-o', str(out_path)]
        )

        assert result.exit_code == 0, str(result)
        assert out_path.exists(), 'Output file was not created'

    @patch('experiments_generator.generators.experiment.experiment_generator.KeysGenerator', autospec=True)
    def test_cli_gen_experiment_files(self, mockKeyGenerator):
        mockKeyGenerator.side_effect=mocks.mocked_keys_generator

        exp_dir = self.tmp_dir / 'experiment'

        assert not exp_dir.exists()

        result = self.runner.invoke(cli.main,
            ['experiment',
                '--hosts', '1',
                '--networks', '1',
                '--services', '3',
                '--searches', '2',
                '--localizations', '2',
                '--replications', '2',
                '--filename', 'exp-test.yaml',
                '-o', str(exp_dir)]
        )

        assert result.exit_code == 0, str(result)
        assert exp_dir.exists(), 'Experiment dir was not created'

        for r in range(2):
            exp_filename = f'exp-test-r{r}.yaml'
            exp_file = exp_dir / exp_filename

            assert exp_file.exists()