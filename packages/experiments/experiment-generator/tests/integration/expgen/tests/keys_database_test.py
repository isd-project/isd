import pytest
from os import path

from experiments_generator.persistence import KeysDb

from expgen_testing import randomgen


class KeysDatabaseTests(object):

    @pytest.fixture(autouse=True)
    def setup_dirs(self, tmp_path):
        self.keysdir = str(tmp_path / 'keys')

    def setup_method(self):
        self.keysdb = KeysDb()

    def test_write_keys_to_directory(self):
        keys_info = self.given_some_keys_info()

        self.keysdb.save_all(keys_info, self.keysdir)

        # Then
        assert path.exists(self.keysdb.dbfile(self.keysdir))


    def test_write_and_load_keys(self):
        self.given_some_keys_were_saved()

        loadedKeys = self.keysdb.load_all(self.keysdir)

        # Then
        self.keys_should_match(loadedKeys, self.saved_keys)

    def test_try_to_load_keys_from_unexistent_dir(self):
        new_dir = path.join(self.keysdir, 'new')

        loadedKeys = self.keysdb.load_all(new_dir)

        assert loadedKeys == []

    # ---------------------------------

    def given_some_keys_were_saved(self):
        keys_info = self.given_some_keys_info()

        self.keysdb.save_all(keys_info, self.keysdir)

        self.saved_keys = keys_info

    def given_some_keys_info(self):
        return randomgen.some_keys_info()

    def keys_should_match(self, keys_info, expected_keys):
        sorted_keys_info = self.sorted_keys(keys_info)
        sorted_expected_keys = self.sorted_keys(expected_keys)

        assert sorted_keys_info == sorted_expected_keys

    def sorted_keys(self, keys_info):
        def sort_key(key_info):
            h = key_info.fingerprint.hash if key_info.fingerprint else None

            return (h, key_info.type, key_info.keysdir)

        return sorted(keys_info, key=sort_key)
