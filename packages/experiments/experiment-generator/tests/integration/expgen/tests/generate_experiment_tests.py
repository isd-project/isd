from experiments_generator.generators.experiment import ExperimentGenerator
from experiments_generator.generators.keys import KeysGenerator
from experiments.generator.models.experiment_models import ExperimentParameters

from expgen_testing.fakes import FakeGenerateKeyCommand
from expgen_testing import mocks

import pytest

class GenerateExperimentTests(object):

    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.tmp_dir = tmp_path
        self.tmp_dir.mkdir(exist_ok=True)

    def setup_method(self):
        self.keysgen = mocks.mocked_keys_generator()
        self.generator = ExperimentGenerator(keysgen=self.keysgen)


    def test_gen_experiments(self):
        exp_dir = self.tmp_dir / 'experiments'

        assert not exp_dir.exists()

        filename_template = 'exp-test{}.yaml'
        filename = filename_template.format('')
        replications = 2

        self.generator.gen_and_save_experiments(
            ExperimentParameters(
                num_hosts=1,
                num_nets=1,
                num_services=1,
                num_searches=1,
                num_localizations=1,
            ),
            exp_dir=exp_dir,
            filename=filename,
            replications=replications,
        )

        assert exp_dir.exists(), 'Experiment dir was not created'

        for r in range(replications):
            exp_file = exp_dir / filename_template.format('-r0')

            assert exp_file.exists()