from experiments_generator.cli import ExperimentsGeneratorCli

import pytest


class GenerateNetworkTests(object):

    @pytest.fixture(autouse=True)
    def setup_directories(self, tmp_path):
        self.cli = ExperimentsGeneratorCli()

        self.tmp_dir = tmp_path
        self.tmp_dir.mkdir(exist_ok=True)

    def test_gen_network_file(self):
        out_path = self.tmp_dir / 'outfile.xml'

        assert not out_path.exists()

        self.cli.generate_network(num_hosts=15, num_nets=3,
                                    output_file=str(out_path))

        assert out_path.exists(), 'Output file was not created'
