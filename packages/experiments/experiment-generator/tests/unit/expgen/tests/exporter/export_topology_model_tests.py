from experiments_generator.exporter import CoreExporter
from experiments_generator.exporter import defaults
from experiments_generator.exporter.core_model import *
from experiments_generator.models import *
from experiments.model.topology_models import *

import pytest

class ExportTopologyModelTests(object):
    topology_example = Topology(
        networks =  [
            Network( name='lan0', nettype=NetTypes.LAN,
                nodes = set(['r0', 'n0', 's0']),
                ip_prefixes= IpPrefixes(ip4='10.0.0.0/24',
                                        ip6='2001:0::/64') ),
            Network( name='wan0', nettype=NetTypes.WAN,
                nodes=set(['backbone', 'r0']),
                ip_prefixes= IpPrefixes(ip4='10.0.1.0/24',
                                        ip6='2001:1::/64') ),
            Network( name='wan1', nettype=NetTypes.WAN,
                nodes=set(['backbone', 'bootstrapper']),
                ip_prefixes= IpPrefixes(ip4='10.0.2.0/24',
                                        ip6='2001:2::/64') )
        ],
        nodes = [
            Node(id=1, name='backbone', nodetype=NodeType.router,
                networks=['wan0', 'wan1'] ),
            Node(id=2, name='r0', nodetype=NodeType.router,
                networks=['wan0', 'lan0'], extra_services=['NAT']),
            Node(id=3, name='s0', nodetype=NodeType.switch,
                networks=['lan0']),
            Node(id=4, name='n0', nodetype=NodeType.host,
                networks=['lan0'] ),
            Node(id=5, name='bootstrapper', nodetype=NodeType.server,
                networks=['wan1'] ),
        ]
    )


    def setup_method(self):
        self.exporter = CoreExporter()

    def test_empty_topology(self):
        core_mdl = self.export_topology(Topology())

        assert core_mdl is not None

        assert core_mdl.session_origin == SessionOrigin()
        assert core_mdl.default_services == defaults.default_services()
        assert core_mdl.session_metadata == defaults.default_session_metadata()
        assert core_mdl.session_options == defaults.default_session_options()
        assert core_mdl.emane_global_configuration == defaults.default_emane_config()

        assert core_mdl.links == []
        assert core_mdl.devices == []
        assert core_mdl.networks == []

    def test_export_nodes(self):
        core_mdl = self.export_topology(self.topology_example)

        self.verify_devices(core_mdl.devices, [
            Device(id=1, name='backbone', devtype="router",
                    services=defaults.nodes_services(NodeType.router)),
            Device(id=2, name='r0', devtype='router',
                    services=defaults.nodes_services(NodeType.router)
                        + [Service('NAT')]),
            Device(id=4, name='n0', devtype='PC',
                    services=defaults.nodes_services(NodeType.host) ),
            Device(id=5, name='bootstrapper', devtype='host',
                    services=defaults.nodes_services(NodeType.server)),
        ])


    def test_export_networks(self):
        core_mdl = self.export_topology(self.topology_example)

        self.verify_network_devices(core_mdl.networks, [
            NetworkDevice(id=3, name='s0', devtype="SWITCH")
        ])

    def test_generate_links(self):
        core_mdl = self.export_topology(self.topology_example)

        self.verify_links(core_mdl.links, [
            # backbone <-> bootstrapper
            Link( node1=1, node2=5, ),
            # backbone <-> r0
            Link( node1=1, node2=2, ),
            # n0 <-> s0
            Link( node1=4, node2=3, ),
            # r0 <-> s0
            Link( node1=2, node2=3, )
        ])

    # --------------------------------------------------------

    def export_topology(self, topo):
        return self.exporter.export_topology(topo)

    def verify_devices(self, devices, expected):
        assert len(devices) == len(expected)

        for i in range(len(devices)):
            self.verify_device(devices[i], expected[i])

    def verify_device(self, device, expected):
        assert device.name == expected.name
        assert device.devtype == expected.devtype
        assert device.devclass == expected.devclass
        assert device.image == expected.image
        assert device.id == expected.id

        assert device.services == expected.services

        assert device.position is not None

    def verify_network_devices(self, netdevices, expected_devices):
        assert len(netdevices) == len(expected_devices)

        for i in range(len(netdevices)):
            netdev = netdevices[i]
            expected = expected_devices[i]

            assert netdev.name == expected.name
            assert netdev.devtype == expected.devtype
            assert netdev.id == expected.id

            assert netdev.position is not None

    def verify_links(self, links, expected_links):
        assert len(links) == len(expected_links)

        for i in range(len(links)):
            link = links[i]
            expected = expected_links[i]

            msg = f'{link.node1} -> {link.node2}'

            assert link.node1 == expected.node1
            assert link.node2 == expected.node2
            assert link.options is not None
