from experiments_generator.exporter import XmlMaker, XmlParser

from experiments_generator.exporter import defaults
from experiments_generator.exporter.core_model import *

class XmlConversionTests(object):

    example_scenario = CoreScenario(
        networks=[NetworkDevice()],
        devices=[Device()],
        links=[Link()],
        emane_global_configuration=EmaneConfiguration(),
        session_origin=SessionOrigin(),
        session_options=[
            Configuration()
        ],
        session_metadata=[
            Configuration()
        ],
        default_services=[
            NodeServices()
        ]
    )

    def setup_method(self):
        self.xml = XmlMaker()
        self.parser = XmlParser()

    def test_convert_to_xml_string(self):
        xml_text = self.parser.serialize_to_string(self.example_scenario)

        parsed = self.parser.parse(xml_text)

        assert parsed is not None

        self.verify_xml(parsed, 'scenario', children=[
            {'tag':'networks'},
            {'tag':'devices'},
            {'tag':'links'},
            {'tag':'emane_global_configuration'},
            {'tag':'session_origin'},
            {'tag':'session_options'},
            {'tag':'session_metadata'},
            {'tag':'default_services'},
        ])

    def test_scenario_to_xml(self):
        scenario = self.example_scenario

        self.verify_xml_conversion(scenario, 'scenario', children=[
            {'tag':'networks', 'children': [{'tag': 'network'}]},
            {'tag':'devices', 'children': [{'tag': 'device'}]},
            {'tag':'links', 'children': [{'tag': 'link'}]},
            {'tag':'emane_global_configuration'},
            {'tag':'session_origin'},
            {'tag':'session_options', 'children': [{'tag': 'configuration'}]},
            {'tag':'session_metadata', 'children': [{'tag': 'configuration'}]},
            {'tag':'default_services', 'children': [{'tag': 'node'}]},
        ])

    def test_session_origin_to_xml(self):
        origin = SessionOrigin()

        xml_obj = origin.to_xml(self.xml)

        self.verify_attributes(xml_obj, {
            'lat': 0.0, 'lon': 0.0, 'alt': 2.0, 'scale': 150.0
        })

    def test_configuration_to_xml(self):
        config = Configuration(name='confname', value='confvalue')

        xml_obj = config.to_xml(self.xml)

        self.verify_xml(xml_obj, tag='configuration', attributes={
            'name': 'confname', 'value': 'confvalue'
        })


    def test_node_services_to_xml(self):
        node = NodeServices(nodetype='mdr', services=[
            Service(name='zebra'),
            Service(name='IPForward')
        ])

        xml_obj = node.to_xml(self.xml)

        self.verify_xml(xml_obj, tag='node', attributes={
            'type': 'mdr'
        }, children = [
            { 'tag': 'service', 'attributes': {'name': 'zebra'}},
            { 'tag': 'service', 'attributes': {'name': 'IPForward'}}
        ])

    def test_position_to_xml(self):
        self.verify_xml_conversion(
            Position(x=100, y=200, alt=3.0),
            tag='position', attributes={
                'x': '100', 'y': '200', 'alt': '3.0', 'lat': '0.0', 'lon': '0.0'
            }
        )

    def test_network_device(self):
        self.verify_xml_conversion(
            NetworkDevice(
                id=5, name='s0', devtype='SWITCH', position=Position()),
            tag='network', attributes={
                'id': '5', 'name':'s0', 'type':'SWITCH'
            }, children=[
                {'tag': 'position'}
            ])

    def test_device_to_xml(self):
        self.verify_xml_conversion(
            Device(
                id=1, name="n1", devtype="PC", devclass="C", image="I",
                position=Position(),
                services=[
                    Service(name='s1'),
                    Service(name='s2')
                ]
            ), 'device', attributes={
                'id':'1', 'name':"n1", 'type':"PC", 'class':"C", 'image':"I"
            }, children=[
                {'tag': 'position'},
                {'tag': 'services', 'children': [
                    {'tag': 'service'},
                    {'tag': 'service'}
                ]}
            ])


    def test_link_to_xml(self):
        self.verify_xml_conversion(
            Link(
                node1=1, node2=2,
                iface1=DeviceInterface(),
                iface2=DeviceInterface(),
                options=LinkOptions()
            ), 'link', attributes={
                'node1':'1', 'node2':"2"
            }, children=[
                {'tag': 'iface1'},
                {'tag': 'iface2'},
                {'tag': 'options'}
            ])

    def test_interface_to_xml(self):
        self.verify_xml_conversion(
            DeviceInterface(
                id=0, name="eth0", mac="00:00:00:aa:00:08", ip4="10.0.0.20", ip4_mask="24", ip6="2001::20", ip6_mask="64"
            ), 'iface', attributes={
                'id':"0", 'name':"eth0", 'mac':"00:00:00:aa:00:08",
                'ip4':"10.0.0.20", 'ip4_mask':"24",
                'ip6':"2001::20", 'ip6_mask':"64"
            })


    def test_link_options_to_xml(self):
        self.verify_xml_conversion(
            LinkOptions(
                delay=160, bandwidth=10000000, loss=0.0, dup=0,jitter=0, unidirectional=0
            ), 'options', attributes={
                'delay':"160", 'bandwidth':"10000000", 'loss':"0.0", 'dup':"0",'jitter':"0", 'unidirectional':"0"
            })

    def test_emane_global_configuration(self):
        self.verify_xml_conversion(
            EmaneConfiguration(
                core=[
                    Configuration(name="a", value="1"),
                    Configuration(name="b", value="2")
                ]
            ), 'emane_global_configuration', children=[
                {'tag':'emulator'},
                {'tag':'core', 'children': [
                    {'tag': 'configuration'},
                    {'tag': 'configuration'}
                ]}
            ])



    # ---------------------------------------------

    def verify_xml_conversion(self, obj, tag, attributes=None, children=None):
        xml_obj = obj.to_xml(self.xml)

        self.verify_xml(xml_obj, tag, attributes, children)


    def verify_xml(self, xml_obj, tag, attributes=None, children=None):

        assert xml_obj.tag == tag

        if attributes:
            self.verify_attributes(xml_obj, attributes)

        if children:
            self.verify_children(xml_obj, children)

    def verify_children(self, xml_obj, children):
        xml_children = xml_obj.getchildren()

        assert len(xml_children) == len(children)

        for i in range(len(xml_children)):
            x_child = xml_children[i]
            expected_kwargs = children[i]

            self.verify_xml(x_child, **expected_kwargs)

    def verify_attributes(self, xml_obj, attributes_map):
        for key, value in attributes_map.items():
            assert xml_obj.get(key) == str(value), \
                f'node <{xml_obj.tag}>: attr={key}'