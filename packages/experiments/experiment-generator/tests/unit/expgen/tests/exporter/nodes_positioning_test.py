from experiments_generator.models import *
from experiments.model.topology_models import *
from experiments_generator.exporter import NodesPositioner
from experiments_generator.exporter.core_model import Position

class NodesPositioningTests(object):
    def setup_method(self):
        self.positioner = NodesPositioner()

    def col_size(self):
        return self.positioner.col_size()
    def margin(self):
        return self.positioner.margin()

    def test_positioning_topology(self):
        topo = Topology(
            networks =  [
                Network( name='lan0', nettype=NetTypes.LAN,
                    nodes = set(['r0', 'n0', 's0'])),
                Network( name='wan0', nettype=NetTypes.WAN,
                    nodes=set(['backbone', 'r0'])),
                Network( name='wan1', nettype=NetTypes.WAN,
                    nodes=set(['backbone', 'bootstrapper']))
            ],
            nodes = set([
                Node( name='backbone', nodetype=NodeType.router,
                    networks=['wan0', 'wan1'] ),
                Node( name='bootstrapper', nodetype=NodeType.server,
                    networks=['wan1'] ),
                Node( name='n0', nodetype=NodeType.host, networks=['lan0'] ),
                Node( name='r0', nodetype=NodeType.router,
                    networks=['wan0', 'lan0'], extra_services=['NAT']),
                Node(name='s0', nodetype=NodeType.switch, networks=['lan0']),
            ])
        )

        positions = self.positioner.position_topology(topo)

        assert positions is not None

        for node in topo.nodes:
            node_pos = positions.get(node.name)

            assert node_pos is not None


    def test_positioning_backbone(self):

        backbone = Node( nodetype=NodeType.router,
                            networks=['wan0', 'wan1', 'wan2', 'wan3', 'wan5'] )

        p = self.positioner.position(backbone)

        assert p is not None
        assert p.x == (self.positioner.col_size() * 2) + self.positioner.margin()
        assert p.y == self.positioner.margin()

    def test_positioning_LAN_router(self):
        lan_router = Node( nodetype=NodeType.router,
                            networks=['wan1', 'lan1'] )

        p = self.positioner.position(lan_router)

        assert p is not None
        assert p.x == (1 * self.positioner.col_size()) + self.positioner.margin()
        assert p.y == self.positioner.vertical_gap()

    def test_positioning_host(self):
        topo = Topology(
            networks = [
                Network( name='lan3', nettype=NetTypes.LAN,
                            nodes=set(['n6', 'n5', 'n7'])),
            ]
        )
        host = Node( name='n7', nodetype=NodeType.host,
                            networks=['lan3'] )

        self.verify_positioning(host, topo, Position(
            x = 3.5 * self.positioner.col_size() + self.margin(),
            y = self.positioner.vertical_gap() \
                        + 3 * self.positioner.row_size()
        ))

    def test_positioning_switch(self):
        topo = Topology(
            networks = [
                Network( name='lan2', nettype=NetTypes.LAN,
                            nodes=set(['n6', 's2', 'r2', 'n5', 'n7'])),
            ]
        )
        switch = Node( name='s2', nodetype=NodeType.switch,
                            networks=['lan2'] )

        self.verify_positioning(switch, topo, Position(
            x = 2 * self.col_size() + self.margin(),
            y = self.positioner.vertical_gap() \
                    + 2 * self.positioner.row_size()
        ))


    def test_positioning_bootstrapper(self):
        server = Node( name='bootstrapper', nodetype=NodeType.server,
                            networks=['wan2'] )

        p = self.positioner.position(server, None)

        assert p is not None
        assert p.x == 2 * self.positioner.col_size() + self.positioner.margin()
        assert p.y == self.positioner.vertical_gap()

    # ----------------------------------------------------

    def verify_positioning(self, node, topo, expected_pos):
        msg = f'node: {node.name}'

        p = self.positioner.position(node, topo)

        assert p is not None, msg
        assert p.x == expected_pos.x
        assert p.y == expected_pos.y