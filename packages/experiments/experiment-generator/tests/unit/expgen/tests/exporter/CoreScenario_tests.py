from experiments_generator.exporter.core_model import *

class CoreScenarioTests(object):

    def test__generate_map_with_nodes_interfaces(self):
        devices = [
            Device(id=0, name='n0'),
            Device(id=1,name='n1'),
            Device(id=2,name='r0')
        ]
        links = [
            Link(
                node1=0, node2=2,
                iface1=DeviceInterface(id=0, ip4='1.2.3.4'),
                iface2=DeviceInterface(id=0, ip4='1.2.3.1')
            ),
            Link(
                node1=1, node2=2,
                iface1=DeviceInterface(id=0, ip4='10.9.8.7'),
                iface2=DeviceInterface(id=1, ip4='10.9.8.1')
            )
        ]

        scenario = CoreScenario(
            devices= devices,
            links=links
        )

        ifaces_map = scenario.gen_interfaces_map()

        assert ifaces_map is not None

        assert ifaces_map['n0'][0]  == links[0].iface1
        assert ifaces_map['n1'][0]  == links[1].iface1
        assert ifaces_map['r0'][0]  == links[0].iface2
        assert ifaces_map['r0'][1]  == links[1].iface2