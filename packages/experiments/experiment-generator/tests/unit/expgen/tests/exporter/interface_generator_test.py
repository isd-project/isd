from experiments_generator.exporter.core_model import *
from experiments_generator.models import *

from experiments_generator.exporter import InterfacesGenerator
from experiments_generator.exporter.InterfacesGenerator import *

class InterfaceGeneratorTest(object):
    topology_example = Topology(
        networks =  [
            Network( name='lan0', nettype=NetTypes.LAN,
                nodes = set(['r0', 'n0', 's0']),
                ip_prefixes= IpPrefixes(ip4='10.0.0.0/24',
                                        ip6='2001:0::/64') ),
            Network( name='wan0', nettype=NetTypes.WAN,
                nodes=set(['backbone', 'r0']),
                ip_prefixes= IpPrefixes(ip4='10.0.1.0/24',
                                        ip6='2001:1::/64') ),
            Network( name='wan1', nettype=NetTypes.WAN,
                nodes=set(['backbone', 'bootstrapper']),
                ip_prefixes= IpPrefixes(ip4='10.0.2.0/24',
                                        ip6='2001:2::/64') )
        ],
        nodes = [
            Node(id=1, name='backbone', nodetype=NodeType.router,
                networks=['wan0', 'wan1'] ),
            Node(id=2, name='r0', nodetype=NodeType.router,
                networks=['wan0', 'lan0'], extra_services=['NAT']),
            Node(id=3, name='s0', nodetype=NodeType.switch,
                networks=['lan0']),
            Node(id=4, name='n0', nodetype=NodeType.host,
                networks=['lan0'] ),
            Node(id=5, name='bootstrapper', nodetype=NodeType.server,
                networks=['wan1'] ),
        ]
    )

    def setup_method(self):
        self.generator = InterfacesGenerator()

    def test_generate_connections_map(self):
        interfaces_map = self.generator.generate_node_connections(self.topology_example)

        self.verify_connections_map(interfaces_map, {
            'backbone': {
                'r0':ConnectionInfo(
                    nodeid=1, #backbone
                    net_name='wan0',
                    net_type=NetTypes.WAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.1.1', ip6='2001:1::1', ip4_mask="24", ip6_mask="64"
                    )
                ),
                'bootstrapper': ConnectionInfo(
                    nodeid=1, #backbone
                    net_name='wan1',
                    net_type=NetTypes.WAN,
                    iface = DeviceInterface(
                        id=1, name="eth1", ip4='10.0.2.1', ip6='2001:2::1', ip4_mask="24", ip6_mask="64"
                    )
                )
            },
            'bootstrapper': {
                'backbone': ConnectionInfo(
                    nodeid=5, #bootstrapper
                    net_name='wan1',
                    net_type=NetTypes.WAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.2.2', ip6='2001:2::2', ip4_mask="24", ip6_mask="64"
                    )
                )
            },
            'r0': {
                'backbone': ConnectionInfo(
                    nodeid=2, #r0
                    net_name='wan0',
                    net_type=NetTypes.WAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.1.2', ip6='2001:1::2', ip4_mask="24", ip6_mask="64"
                    )
                ),
                's0': ConnectionInfo(
                    nodeid=2, #r0
                    net_name='lan0',
                    net_type=NetTypes.LAN,
                    iface = DeviceInterface(
                        id=1, name="eth1", ip4='10.0.0.1', ip6='2001::1', ip4_mask="24", ip6_mask="64"
                    )
                )
            },
            'n0': {
                's0': ConnectionInfo(
                    nodeid=4, #n0
                    net_name='lan0',
                    net_type=NetTypes.LAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.0.2', ip6='2001::2', ip4_mask="24", ip6_mask="64"
                    )
                )
            }
        })


    def test__router_needs_to_have_the_first_network_ip(self):
        min_topology = Topology(
            networks =  [
                Network( name='lan0', nettype=NetTypes.LAN,
                    nodes = set(['r0', 'n0', 's0']),
                    ip_prefixes= IpPrefixes(ip4='10.0.0.0/24',
                                            ip6='2001:0::/64') )
            ],
            nodes = [
                Node(id=2, name='r0', nodetype=NodeType.router,
                    networks=['lan0'], extra_services=['NAT']),
                Node(id=3, name='s0', nodetype=NodeType.switch,
                    networks=['lan0']),
                Node(id=4, name='n0', nodetype=NodeType.host,
                    networks=['lan0'] )
            ]
        )


        interfaces_map = self.generator.generate_node_connections(min_topology)

        self.verify_connections_map(interfaces_map, {
            'r0': {
                's0': ConnectionInfo(
                    nodeid=2, #r0
                    net_name='lan0',
                    net_type=NetTypes.LAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.0.1', ip6='2001::1', ip4_mask="24", ip6_mask="64"
                    )
                )
            },
            'n0': {
                's0': ConnectionInfo(
                    nodeid=4, #n0
                    net_name='lan0',
                    net_type=NetTypes.LAN,
                    iface = DeviceInterface(
                        id=0, name="eth0", ip4='10.0.0.2', ip6='2001::2', ip4_mask="24", ip6_mask="64"
                    )
                )
            }
        })

    def test_generate_links(self):
        topo = self.topology_example
        links = self.generator.generate_links_from_iface_map(topo, {
            'backbone': {
                'r0':ConnectionInfo(
                    nodeid=1,
                    iface=DeviceInterface(ip4='10.0.1.1')
                ),
                'bootstrapper': ConnectionInfo(
                    nodeid=1,
                    iface=DeviceInterface(ip4='10.0.2.1')
                )
            },
            'bootstrapper': {
                'backbone': ConnectionInfo(
                    nodeid=5,
                    iface=DeviceInterface(ip4='10.0.2.2')
                )
            },
            'n0': {
                's0': ConnectionInfo(
                    nodeid=4,
                    iface=DeviceInterface(ip4='10.0.0.1')
                )
            },
            'r0': {
                'backbone': ConnectionInfo(
                    nodeid=2,
                    iface=DeviceInterface(ip4='10.0.1.2')
                ),
                's0': ConnectionInfo(
                    nodeid=2,
                    iface=DeviceInterface(ip4='10.0.0.2')
                )
            }
        })

        self.verify_links(links, [
            # backbone <-> bootstrapper
            Link(
                node1=1,
                iface1=DeviceInterface(ip4='10.0.2.1'),
                node2=5,
                iface2=DeviceInterface(ip4='10.0.2.2')
            ),
            # backbone <-> r0
            Link(
                node1=1,
                iface1=DeviceInterface(ip4='10.0.1.1'),
                node2=2,
                iface2=DeviceInterface(ip4='10.0.1.2')
            ),
            # n0 <-> s0
            Link(
                node1=4,
                iface1=DeviceInterface(ip4='10.0.0.1'),
                node2=3,
                iface2=None
            ),
            # r0 <-> s0
            Link(
                node1=2,
                iface1=DeviceInterface(ip4='10.0.0.2'),
                node2=3,
                iface2=None
            )
        ])


    def test_generate_link_options(self):
        topo = Topology(
            networks =  [
                # ...
            ],
            nodes = [
                Node(id=1, name='backbone', nodetype=NodeType.router),
                Node(id=2, name='r0', nodetype=NodeType.router),
                Node(id=3, name='s0', nodetype=NodeType.switch),
                Node(id=4, name='n0', nodetype=NodeType.host),
            ]
        )
        links = self.generator.generate_links_from_iface_map(topo, {
            'backbone': {
                'r0':ConnectionInfo(
                    nodeid=1,
                    iface=DeviceInterface(ip4='10.0.1.1'),
                    net_type=NetTypes.WAN
                )
            },
            'n0': {
                's0': ConnectionInfo(
                    nodeid=4, iface=DeviceInterface(ip4='10.0.0.1'), net_type=NetTypes.LAN
                )
            },
            'r0': {
                'backbone': ConnectionInfo(
                    nodeid=2, iface=DeviceInterface(ip4='10.0.1.2'),
                    net_type=NetTypes.WAN
                ),
                's0': ConnectionInfo(
                    nodeid=2, iface=DeviceInterface(ip4='10.0.0.2'),
                    net_type=NetTypes.LAN
                )
            }
        })

        self.verify_link_options(links, [
            # backbone <-> r0
            InterfacesGenerator.wanLinkOptions(),
            # n0 <-> s0
            InterfacesGenerator.lanLinkOptions(),
            # r0 <-> s0
            InterfacesGenerator.lanLinkOptions(),
        ])

    # ---------------------------

    def verify_connections_map(self, conn_map, expected_map):
        for node_src, connections in expected_map.items():
            for node_dst, expected_conn in connections.items():
                conn = conn_map[node_src][node_dst]
                msg = f'connection {node_src} -> {node_dst}'

                self.compare_connection(conn, expected_conn, msg)

    def compare_connection(self, conn, expected_conn, msg):

        assert conn.nodeid == expected_conn.nodeid, msg
        assert conn.net_name == expected_conn.net_name, msg
        assert conn.net_type == expected_conn.net_type, msg

        self.compare_interfaces(conn.iface, expected_conn.iface, msg)

    def compare_interfaces(self, iface, expected_iface, msg=''):
        assert iface.id == expected_iface.id, msg
        assert iface.name == expected_iface.name, msg
        assert iface.ip4 == expected_iface.ip4, msg
        assert iface.ip4_mask == expected_iface.ip4_mask, msg
        assert iface.ip6 == expected_iface.ip6, msg
        assert iface.ip6_mask == expected_iface.ip6_mask, msg

        assert iface.mac is not None , msg


    def verify_links(self, links, expected_links):
        assert len(links) == len(expected_links)

        for i in range(len(links)):
            link = links[i]
            expected = expected_links[i]

            assert link.node1 == expected.node1
            assert link.node2 == expected.node2
            assert link.iface1 == expected.iface1, f'{link.node1} -> {link.node2}'
            assert link.iface2 == expected.iface2, f'{link.node1} -> {link.node2}'
            assert link.options == expected.options, f'{link.node1} -> {link.node2}'

    def verify_link_options(self, links, expected_options):
        assert len(links) == len(expected_options)

        for i in range(len(links)):
            link = links[i]
            expected_opt = expected_options[i]

            assert link.options == expected_opt