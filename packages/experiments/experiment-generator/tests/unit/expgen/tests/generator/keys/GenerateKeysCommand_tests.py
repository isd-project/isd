from experiments_generator.generators.keys import GenerateKeyCommand
from experiments_generator.models.key_models import *

from unittest.mock import Mock

import pytest
import json
import random

class GenerateKeyCommandTest(object):
    def setup_method(self):
        self.executor = Mock(return_value=json.dumps({}))
        self.cli_cmd = ['nodejs', 'example/path/to/cli.js']
        self.given_command(self.cli_cmd)

    def given_command(self, cli_cmd=None):
        self.command = GenerateKeyCommand(cli_cmd, executor=self.executor)


    @pytest.mark.parametrize(
        "keytype,kt_arg,empty_cmd,expected_base_cmd",
        [
            pytest.param(KeyTypes.rsa, 'rsa', False, None),
            pytest.param(KeyTypes.ec, 'ec', False, None),
            pytest.param(KeyTypes.ec, 'ec', True, ['discovery']),
        ],
    )
    def test_execute_command(
        self, tmp_path, keytype, kt_arg, empty_cmd, expected_base_cmd
    ):
        # Given
        keysdir = 'example/path'

        self.given_command(self.cli_cmd if not empty_cmd else None)

        # When
        key_info = self.command.generate_key(
            keytype=keytype, keysdir=keysdir
        )

        # Then
        if expected_base_cmd is None: expected_base_cmd=self.cli_cmd

        self.should_execute_command(
            *expected_base_cmd,
                'keys', 'gen', '-t', kt_arg, '--keysdir', keysdir, '--json'
        )

    def test_parse_generated_key_info(self):
        key_data = {
            "kty": "EC",
            "crv": "P-256",
            "x": "1Dy_Eu69lN8Mh6PT50thfMbBu21e86G2Ya4sdjGs_Wk",
            "y": "6l5ly8KIy62GD3xwxmeY_QNvA3SzUUxAUKum-zSEvzM"
        }

        self.on_key_generate_result({
            "type": "EllipticCurve",
            "fingerprint": "SHA-256:b64u:xXXmzGIUmX6tPqKJH6uBMtvm4SysupuQgx43B_2ICRw",
            "publicKey": {
                "format": "Jwk",
                "data": key_data
            }
        })

        key_info = self.when_executing_command(keysdir = 'example/path')

        self.this_result_is_expected(key_info, KeyInfo(
            type=KeyTypes.ellipticCurves,
            fingerprint=Fingerprint(
                hash_alg='SHA-256',
                hash_fmt='b64u',
                hash='xXXmzGIUmX6tPqKJH6uBMtvm4SysupuQgx43B_2ICRw'
            ),
            publicKey=PublicKey( format='Jwk', data=key_data),
            keysdir = 'example/path'
        ))

    def on_key_generate_result(self, result):
        self.executor.return_value = json.dumps(result)

    def when_executing_command(self, keysdir):
        keytype = random.choice([KeyTypes.ec, KeyTypes.rsa])

        return self.command.generate_key(
            keytype=keytype, keysdir=keysdir
        )

    def should_execute_command(self, *expected_cmd):
        self.executor.assert_called_with(list(expected_cmd))

    def this_result_is_expected(self, key_info, expected):
        assert key_info is not None

        assert key_info.type == expected.type
        assert key_info.fingerprint == expected.fingerprint
        assert key_info.publicKey == expected.publicKey
        assert key_info.keysdir == expected.keysdir