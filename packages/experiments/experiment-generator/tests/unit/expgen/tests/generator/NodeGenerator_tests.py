from experiments_generator.generators import NodeGenerator
from experiments_generator.models import *
from experiments.model.topology_models import *


class NodeGeneratorTests(object):

    def setup_method(self):
        self.nodegen = NodeGenerator()

    def test_gen_node(self):
        node = self.gen_next(NodeType.host)

        assert node == Node(
            id=1,
            name = 'n0',
            nodetype = NodeType.host,
            networks = []
        )

    def test_gen_node_ids(self):
        nodetypes = [NodeType.host, NodeType.router, NodeType.switch,
                    NodeType.server, NodeType.host]
        ids = [self.gen_next(nt).id for nt in nodetypes]

        assert ids == [1, 2, 3, 4, 5]

    def test_gen_names(self):
        assert self.gen_names(NodeType.host, 2)   == ('n0', 'n1')
        assert self.gen_names(NodeType.router, 2) == ('r0', 'r1')
        assert self.gen_names(NodeType.switch, 2) == ('s0', 's1')
        assert self.gen_names(NodeType.server, 2) == ('srv0', 'srv1')

    def test_gen_with_name(self):
        backbone = self.gen_next(NodeType.router, name='backbone')

        assert backbone.name == 'backbone'

    def test_gen_with_name__does_not_change_counter(self):
        self.gen_next(NodeType.router, name='name')

        assert self.gen_next(NodeType.router).name == 'r0'



    # -------------------------------

    def gen_names(self, nodetype, count):
        return  tuple([self.gen_next(nodetype).name for i in range(count)])

    def gen_next(self, nodetype, name=None):
        return self.nodegen.gen_node(nodetype=nodetype, name=name)