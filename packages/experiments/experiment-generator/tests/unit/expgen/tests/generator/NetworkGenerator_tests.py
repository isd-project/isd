from experiments_generator.generators import NetworkGenerator
from experiments_generator.models import *
from experiments.model.topology_models import *



class NetworkGeneratorTests(object):

    def setup_method(self):
        self.netgen = NetworkGenerator()

    def test_generate_network(self):
        net = self.netgen.gen_next()

        assert net == Network(
            name = 'lan0',
            nodes = set([]),
            nettype = NetTypes.LAN,
            ip_prefixes = IpPrefixes(ip4='10.0.0.0/24',
                                     ip6='2001:0::/64')
        )


    def test_generate_names(self):
        lan0 = self.netgen.gen_next()
        lan1 = self.netgen.gen_next()
        wan0 = self.netgen.gen_next(NetTypes.WAN)
        wan1 = self.netgen.gen_next(NetTypes.WAN)

        assert (lan0.name, lan1.name) == ('lan0', 'lan1')
        assert (wan0.name, wan1.name) == ('wan0', 'wan1')