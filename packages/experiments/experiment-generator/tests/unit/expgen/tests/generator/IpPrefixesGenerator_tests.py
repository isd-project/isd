from experiments_generator.generators import IpPrefixesGenerator
from experiments_generator.models import *
from experiments.model.topology_models import *



class IpPrefixesGeneratorTests(object):

    def setup_method(self):
        self.generator = IpPrefixesGenerator()

    def test_generate_ip_prefix_base(self):
        ips = self.generator.gen_next()

        assert ips == IpPrefixes(ip4='10.0.0.0/24',
                                 ip6='2001:0::/64')


    def test_generate_ip_sequence(self):
        self.generator.gen_next()

        ips1 = self.generator.gen_next()

        assert ips1 == IpPrefixes(ip4='10.0.1.0/24',
                                 ip6='2001:1::/64')