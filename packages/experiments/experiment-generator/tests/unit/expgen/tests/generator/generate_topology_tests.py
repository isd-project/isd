from experiments_generator.generators import TopologyGenerator
from experiments_generator.models import *
from experiments.model.topology_models import *

import pytest


class GenerateTopologyTests(object):

    def setup_method(self):
        self.generator = TopologyGenerator()

    def test_gen_simpler_topology(self):
        simple_network = TopologySpec(
            networks = 1,
            hosts = 1
        )

        topology = self.generator.gen_experiment_topology(simple_network)

        assert topology is not None

        self.verify_topology_stats(topology, simple_network)

        self.verify_topology(topology, Topology(
            networks =  [
                Network( name='lan0', nettype=NetTypes.LAN,
                    nodes = set(['r0', 'n0', 's0']),
                    ip_prefixes= IpPrefixes(ip4='10.0.0.0/24',
                                            ip6='2001:0::/64') ),
                Network( name='wan0', nettype=NetTypes.WAN,
                    nodes=set(['backbone', 'r0']),
                    ip_prefixes= IpPrefixes(ip4='10.0.1.0/24',
                                            ip6='2001:1::/64') ),
                Network( name='wan1', nettype=NetTypes.WAN,
                    nodes=set(['backbone', 'bootstrapper']),
                    ip_prefixes= IpPrefixes(ip4='10.0.2.0/24',
                                            ip6='2001:2::/64') )
            ],
            nodes = set([
                Node(id=1, name='backbone', nodetype=NodeType.router,
                    networks=['wan0', 'wan1'] ),
                Node(id=2, name='r0', nodetype=NodeType.router,
                    networks=['wan0', 'lan0'], extra_services=['NAT']),
                Node(id=3, name='s0', nodetype=NodeType.switch,
                    networks=['lan0']),
                Node(id=4, name='n0', nodetype=NodeType.host,
                    networks=['lan0'] ),
                Node(id=5, name='bootstrapper', nodetype=NodeType.server,
                    networks=['wan1'] ),
            ])
        ))


    def test_gen_multiple_LANs(self):
        topo_spec = TopologySpec(
            networks = 4,
            hosts = 12
        )

        topology = self.generator.gen_experiment_topology(topo_spec)

        self.verify_topology_stats(topology, topo_spec)

    @pytest.mark.parametrize(
        "num_nets,num_hosts,min_hosts,max_hosts",
        [
            pytest.param(4, 12, 3, 3),
            pytest.param(4, 13, 3, 4),
            pytest.param(4, 15, 3, 4),
        ],
    )
    def test_hosts_per_LAN(self, num_nets, num_hosts, min_hosts, max_hosts):
        topo_spec = TopologySpec(
            networks = num_nets,
            hosts = num_hosts
        )

        topology = self.generator.gen_experiment_topology(topo_spec)

        self.verify_hosts_per_net(topology, min_hosts, max_hosts)

        assert len(topology.hosts()) == num_hosts



    def verify_topology_stats(self, topology, spec:TopologySpec):
        # 1 LAN + 1 between internal router and 'backbone' + 1 for bootstrapper
        assert len(topology.networks)  == spec.networks * 2 + 1

        # Central router + 1 for each network
        assert len(topology.routers()) == spec.networks + 1
        # 1 for each LAN
        assert len(topology.switches())== spec.networks
        # as specified
        assert len(topology.hosts())   == spec.hosts

        # bootstrap node
        assert len(topology.servers()) == 1

        # total count of nodes
        assert len(topology.nodes) == len(topology.routers()) \
                                    + len(topology.switches()) \
                                    + len(topology.hosts()) \
                                    + len(topology.servers())

    def verify_topology(self, found:Topology, expected:Topology):
        assert found.networks == expected.networks
        assert found.nodes == expected.nodes

    def verify_hosts_per_net(self, topology, min_hosts, max_hosts):
        for net in topology.networks:
            if net.nettype != NetTypes.LAN:
                continue

            hosts = [n for n in net.nodes if n.startswith('n')]
            num_hosts = len(hosts)

            assert min_hosts <= num_hosts and num_hosts <= max_hosts