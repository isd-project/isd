from experiments_generator.generators.experiment import ExperimentOperationsGenerator
from experiments.generator.models.experiment_models import *
from experiments.model import SearchSpec

import random
from datetime import timedelta
from functools import partial

import pytest

class ExperimentOperationsGeneratorTests(object):
    def setup_method(self):
        self.generator = ExperimentOperationsGenerator()

    def test_generate_publications(self):
        hosts          = self.given_some_hosts()
        num_srvs       = self.given_num_of_services_to_publish()
        delay_interval = self.given_delay_interval()

        srvs_map = self.generator.distribute_publications(
            hosts=hosts,
            num_services=num_srvs,
            delay_interval=delay_interval
        )

        assert srvs_map is not None

        self.should_contain_all_nodes(srvs_map, hosts)
        self.should_have_this_number_of_publications(srvs_map, num_srvs)
        self.all_publications_should_be_valid(srvs_map)
        self.delays_should_be_on_interval(srvs_map, delay_interval)


    def test_generate_searches(self):
        self.generate_searches_test_template('search')

    def test_generate_locate_searches(self):
        self.generate_searches_test_template('locate')

    def test_merge_searches(self):
        self.generate_searches_test_template()

    def generate_searches_test_template(self, search_type=None):
        hosts            = self.given_some_hosts()
        num_searches     = self.given_some_searches()
        num_locate       = self.given_some_searches()
        delay_interval   = self.given_delay_interval()
        publications_map = self.given_some_publications(hosts)

        if search_type=='search':
            num_locate = 0
        elif search_type=='locate':
            num_searches = 0

        searches_map = self.generator.distribute_searches(
            hosts = hosts,
            num_searches     = num_searches,
            num_localizations= num_locate,
            delay_interval=delay_interval,
            publications=publications_map
        )

        assert searches_map is not None

        self.should_contain_all_nodes(searches_map, hosts)
        self.should_have_the_given_number_of_searches(
            searches_map, num_searches + num_locate
        )
        self.all_searches_should_be_valid(searches_map, search_type)
        self.delays_should_be_on_interval(searches_map, delay_interval)


    def test_generate_experiment_operations(self):
        params = self.given_experiment_parameters()
        hosts = self.given_some_hosts(params.num_hosts)

        exp_script = self.generator.generate_experiment_operations(params, hosts)

        self.the_generated_script_should_have_the_specified_operations(
            exp_script, params, hosts
        )

    # ---------------------------------------------------------

    # summary steps

    def given_some_publications(self, hosts):
        return self.generator.distribute_publications(
            hosts         =hosts,
            num_services  =self.given_num_of_services_to_publish(),
            delay_interval=self.given_delay_interval()
        )
    # ---------------

    def given_some_hosts(self, num_hosts=None):
        if num_hosts is None:
            num_hosts = random.randint(3, 5)
        # start_node=random.randint(0, 5)
        start_node=0

        return [self._gen_host(i + start_node) for i in range(num_hosts)]

    def _gen_host(self, nodeid):
        return HostInfo(
            name=f'n{nodeid}',
            keyid=self._gen_hexa_string(),
            keyid_fmt='b16'
        )

    def _gen_hexa_string(self):
        chars = '0123456789abcdef'
        size = random.randint(6, 16)

        return ''.join([random.choice(chars) for i in range(size)])


    def given_some_nodes(self):
        return random.randint(2, 5)

    def given_num_of_services_to_publish(self):
        return random.randint(5, 10)

    def given_some_searches(self):
        return random.randint(5, 10)

    def given_delay_interval(self):
        return (timedelta(milliseconds=50), timedelta(milliseconds=150))

    def given_experiment_parameters(self)->ExperimentParameters:
        return ExperimentParameters(
            num_hosts= random.randint(2, 5),
            num_services=self.given_num_of_services_to_publish(),
            num_searches=self.given_some_searches(),
            num_localizations=self.given_some_searches(),
            delay_interval=self.given_delay_interval()
        )

    def should_contain_all_nodes(self, operations_map, hosts):
        assert len(operations_map) == len(hosts), str(operations_map)

        hosts_names = [h.name for h in hosts]

        assert sorted(operations_map.keys()) == sorted(hosts_names)

    def should_have_this_number_of_publications(self, srvs_map, num_srvs):
        self.verify_number_of_operations(srvs_map, num_srvs)

    def should_have_the_given_number_of_searches(self,
        searches_map, num_searches
    ):
        self.verify_number_of_operations(searches_map, num_searches)

    def verify_number_of_operations(self, op_map, num_operations, msg=None):
        if msg is None: msg = f'Generated operations: {op_map}'

        operations_count = 0

        for node_operations in op_map.values():
            operations_count += len(node_operations)

        assert operations_count == num_operations, msg

    def all_publications_should_be_valid(self, srvs_map):
        self.verify_operations(srvs_map, self.verify_publication)

    def all_searches_should_be_valid(self, searches_map, search_type=None):
        verify = partial(self.verify_search, expect_search_kind=search_type)

        self.verify_operations(searches_map, verify)


    def the_generated_script_should_have_the_specified_operations(self,
        generated_script:ExperimentOperations,
        specified_params:ExperimentParameters,
        hosts: List[HostInfo]
    ):
        assert generated_script is not None

        assert len(generated_script.nodes_operations) == len(hosts)

        num_srvs, num_searches, num_l_searches = self.count_operations(
            generated_script
        )

        assert num_srvs == specified_params.num_services
        assert num_searches == specified_params.num_searches
        assert num_l_searches == specified_params.num_localizations

    def count_operations(self, generated_script):
        num_publis = 0
        num_searches = 0
        num_locate_searches = 0

        for node, node_ops in generated_script.nodes_operations.items():
            num_publis += len(node_ops.publications)

            searches = [s for s in node_ops.searches if s.kind == 'search']
            l_searches = [s for s in node_ops.searches if s.kind == 'locate']

            num_searches += len(searches)
            num_locate_searches += len(l_searches)

        return num_publis, num_searches, num_locate_searches

    # ----------------------------------------------------

    def verify_operations(self, op_map, verifier):
        for node_operation in op_map.values():
            for operation in node_operation:
                verifier(operation)

    def verify_publication(self, publication:ServicePublication):
        assert publication is not None

        assert publication.port is not None
        assert publication.port > 0
        assert publication.type is not None
        assert publication.name is not None

    def verify_search(self, search: SearchSpec, expect_search_kind:str=None):
        assert search is not None

        assert search.kind in ['search', 'locate']

        if expect_search_kind is not None:
            assert search.kind == expect_search_kind

        if search.kind == 'search':
            assert search.srvType is not None
            assert search.srvId is None
        else:
            assert search.srvType is None
            assert search.srvId is not None

    def delays_should_be_on_interval(self, operation_map, delay_interval):
        for node_operations in operation_map.values():
            for operation in node_operations:
                self.verify_interval(operation.delay, delay_interval)

    def verify_interval(self, delay_millis, delay_interval):
        min_delay = delay_interval[0].total_seconds() * 1000
        max_delay = delay_interval[1].total_seconds() * 1000

        assert min_delay <= delay_millis and delay_millis <= max_delay