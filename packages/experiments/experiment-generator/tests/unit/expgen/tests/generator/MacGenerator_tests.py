from experiments_generator.generators import MacGenerator

import re

class MacGeneratorTests(object):
    def setup_method(self):
        self.mac_gen = MacGenerator()

    def test_mac_format(self):
        hexa_byte = '[0-9A-Fa-f]{2}'
        mac_regex = f'({hexa_byte}[:-]){{5}}({hexa_byte})'

        for i in range(255):
            mac = self.mac_gen.next()

            assert re.match(mac_regex, mac), 'mac = ' + mac
