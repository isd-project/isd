from experiments_generator.generators.experiment import SearchesDistribution, SearchesSpec
from experiments.generator.models.experiment_models import *

import random

from expgen_testing import randomgen
from datetime import timedelta


class OperationsDistributionTests(object):
    def setup_method(self):
        pass

    def test__distribute_searches(self):
        hosts = randomgen.hosts_info()
        publications_map = randomgen.publications_for(hosts)

        max_searches     = max(2, len(publications_map))
        num_searches     =random.randint(2, max_searches)
        num_localizations=random.randint(2, max_searches)

        distribution= SearchesDistribution(SearchesSpec(
            hosts=hosts,
            num_searches     =num_searches,
            num_localizations=num_searches,
            delay_interval=[timedelta(milliseconds=0),
                            timedelta(milliseconds=15)],
            publications=publications_map
        ))

        searches_map = distribution.generate()

        host_names = [h.name for h in hosts]

        assert sorted(searches_map.keys()) == sorted(host_names)

    def test__distribute_searches_to_other_nodes(self):
        provider = HostInfo( name='n0', keyid='deadbeef', keyid_fmt='b16' )
        client   = HostInfo( name='n1')

        distribution= SearchesDistribution(SearchesSpec(
            hosts=[provider, client],
            num_searches=1,
            num_localizations=1,
            delay_interval=[timedelta(milliseconds=0),
                            timedelta(milliseconds=15)],
            publications={
                'n0': [
                    ServicePublication(
                        name='srv1',
                        type='http',
                        node=provider
                    )
                ]
            }
        ))

        searches_map = distribution.generate()

        provider_searches = searches_map[provider.name]
        client_searches   = searches_map[client.name]

        assert not provider_searches
        assert len(client_searches) == 2

        for s in client_searches:
            if s.kind == 'locate':
                assert 'srv1' in s.srvId
            else:
                assert s.srvType == 'http'