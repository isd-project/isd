from experiments_generator import CoreNetworkGenerator
from experiments_generator.generators.experiment import ExperimentGenerator
from experiments_generator.generators.experiment.experiment_generator import ExperimentDirs
from experiments_generator.generators.keys import KeysGenerator, GenerateKeyCommand
from experiments.generator.models.experiment_models import *
from experiments_generator.models.key_models import *
from experiments.model.topology_models import Topology, NetTypes

from experiments.parsing import ConfigurationParser

from expgen_testing.fakes import FakeGenerateKeyCommand
from expgen_testing import mocks

import unittest
from unittest.mock import Mock, ANY, patch
import secrets
import random

import dataclasses
from os import path
from datetime import timedelta
from pathlib import Path

import pytest

class ExperimentGeneratorTest(object):
    def setup_method(self):
        self.xml_parser = Mock()
        self.network_gen = CoreNetworkGenerator(xml_parser=self.xml_parser)

        self.keygen_command = FakeGenerateKeyCommand()
        self.keys_generator = mocks.mocked_keys_generator(
            keygen_command=self.keygen_command
        )

        self.config_parser = Mock()

        self.exp_generator = ExperimentGenerator(
            network_gen   = self.network_gen,
            keysgen       = self.keys_generator,
            config_parser = self.config_parser
        )

        self.fs_mock = Mock()
        self.fs_mock.joinpaths.side_effect = path.join
        self.exp_generator.fs = self.fs_mock

        self.example_exp_dirs = ExperimentDirs('/experiment/dir', fs=self.fs_mock)


    def test_generate_experiment_config_from_parameters(self):
        params = self.given_experiment_parameters()
        exp_dir = 'experiment/dir'

        experiment_config = self.exp_generator.generate_experiment_run(
            params, exp_dir)

        self.fs_mock.makedirs.assert_called_with(exp_dir)

        self.verify_experiment_config_matches(
            experiment_config, params, exp_dir
        )



    def test__gen_node_config__should_have_bootstrap_address(self):
        params = self.given_experiment_parameters()

        experiment_config = self.exp_generator.generate_experiment_run(
            params, 'any/experiment/dir')

        assert experiment_config.bootstrap_address is not None


    def test_generate_and_save_experiment_configuration(self):
        params    = self.given_experiment_parameters()
        expdir    = 'experiment/dir'
        file_path = 'any/filepath.yaml'

        exp_config = self.exp_generator.gen_and_save_experiment(
            params, expdir, file_path
        )

        self.config_parser.save_configuration.assert_called_with(
            exp_config, file_path
        )

    def test_generate_experiment_with_replications(self):
        params  = self.given_experiment_parameters()
        expdir  = 'experiment/dir'

        configs = self.exp_generator.generate_experiment_runs(params, expdir, 3)

        assert len(configs) == 3

        self.should_use_the_same_topologies(configs)
        self.should_use_different_dirs_for_nodes(configs)
        self.should_use_same_keydirs(configs)
        self.should_have_different_operations(configs)

    def test__gen_and_save_experiment_configs_with_replications(self):
        params = self.given_experiment_parameters()
        output_dir = 'experiment/directory'

        saved_files = self.exp_generator.gen_and_save_experiments(
            params, output_dir, replications=3
        )

        for f in saved_files:
            assert f.startswith(output_dir)

            self.config_parser.save_configuration.assert_any_call(
                ANY, f
            )

    def test__export_generated_config(self):
        experiment_config = self.exp_generator.generate_experiment_run(
            self.given_experiment_parameters(), 'experiment/dir'
        )

        parser = ConfigurationParser()
        conf_yaml = parser.render_configuration(experiment_config)

        assert conf_yaml is not None
        # TO-DO: validate


    def test__generate_node_config(self):
        node_outdir = 'node/output'
        keyid = 'Hx5k1WBzSC6Q'

        node_key = self.given_node_key(keyid=keyid)
        exp_operations = ExperimentOperations(
            nodes_operations={
                node_key.node: NodeOperations(
                    node=node_key.to_host_info(),
                    publications=[],
                    searches=[]
                )
            }
        )

        node_config = self.exp_generator.gen_node_config(
            node_key=node_key, exp_operations=exp_operations,
            node_outdir=node_outdir, exp_dirs=self.example_exp_dirs
        )

        assert node_config.keyid == keyid
        assert node_config.keyid_fmt == 'b64u'

    # -----------------------------------------------------------------

    def given_experiment_parameters(self):
        return ExperimentParameters(
            num_hosts=random.randint(5, 10),
            num_nets=random.randint(2, 4),
            num_services=random.randint(8, 12),
            num_searches=random.randint(8, 10),
            num_localizations=random.randint(8, 10),
            delay_interval=[
                timedelta(milliseconds=0), timedelta(milliseconds=150)
            ]
        )

    def given_node_key(self, keyid=None, node='n0'):
        if not keyid: keyid = secrets.token_urlsafe()

        return NodeKey(
            node=node,
            keyinfo = KeyInfo(
                type=KeyTypes.ec,
                fingerprint=Fingerprint(
                    hash=keyid,
                    hash_fmt='b64u'
                ),
                keysdir=self.example_exp_dirs.subdir('keysdir')
            )
        )

    def verify_experiment_config_matches(self, exp_config, params, exp_dir):
        assert exp_config is not None

        self.verify_directories(exp_config, exp_dir)
        self.verify_topology(exp_config.topology, params)
        self.verify_generated_keys_for_nodes(params)
        self.verify_nodes(exp_config, params)
        self.verify_operations_count(exp_config, params)

        expected_params = self.exp_generator.fill_experiment_params(params)
        assert exp_config.params == expected_params

    def verify_directories(self, exp_config, experiment_dir):
        self._verify_is_relative_path_to(exp_config.output_dir, experiment_dir)
        self._verify_is_relative_path_to(exp_config.keysdir, experiment_dir)

        assert exp_config.network_file is not None
        assert exp_config.network_file.endswith('.xml')

        network_file = path.join(experiment_dir, exp_config.network_file)
        self.xml_parser.serialize_to_file.assert_called_with(ANY, network_file)

        for name, node in exp_config.nodes.items():
            self._verify_is_relative_path_to(
                node.output_dir, exp_config.output_dir)
            self._verify_is_relative_path_to(node.keysdir, experiment_dir)

    def _verify_is_relative_path_to(self, some_path, exp_dir):
        assert some_path is not None
        assert not some_path.startswith(exp_dir), 'should be relative to experiment dir'


    def verify_topology(self, topology:Topology, params):
        assert topology is not None

        assert len(topology.hosts()) == params.num_hosts
        assert len(topology.lan_networks()) == params.num_nets

    def verify_generated_keys_for_nodes(self, params):
        num_generated_keys = len(self.keygen_command.generated_keys)

        assert num_generated_keys == params.num_hosts

    def verify_nodes(self, exp_config, params):
        assert len(exp_config.nodes) == params.num_hosts

        for node in exp_config.nodes.values():
            self.verify_node(node, exp_config, params)

    def verify_node(self, node, exp_config, params):
        assert node is not None

        assert node.keyid is not None
        assert node.keysdir is not None
        assert node.output_dir is not None
        assert node.results_file is not None
        assert node.publications is not None
        assert node.searches is not None

    def verify_operations_count(self, exp_config, params):
        num_publications = 0
        num_searches = 0

        for node in exp_config.nodes.values():
            num_publications += len(node.publications)
            num_searches += len(node.searches)

        assert num_publications == params.num_services
        assert num_searches == params.num_searches + params.num_localizations



    def should_use_the_same_topologies(self, configs):
        def same_topology(conf1, conf2):
            assert conf1.topology == conf2.topology
            assert conf1.network_file == conf2.network_file

        self.compare_pairs(configs, same_topology)

    def should_use_same_keydirs(self, configs):
        def differ_keys(conf1, conf2):
            assert conf1.keysdir == conf2.keysdir

        self.compare_pairs(configs, differ_keys)

    def should_have_different_operations(self, configs):
        def differ_operations(conf1, conf2):
            assert conf1.nodes != conf2.nodes

        self.compare_pairs(configs, differ_operations)

    def should_use_different_dirs_for_nodes(self, configs):
        def differ_dirs(conf1, conf2):
            for n in conf1.nodes.keys():
                conf1_nodedir = path.dirname(conf1.nodes[n].output_dir)
                conf2_nodedir = path.dirname(conf2.nodes[n].output_dir)

                assert conf1_nodedir != conf2_nodedir

        self.compare_pairs(configs, differ_dirs)


    def compare_pairs(self, configs, comparison):
        for i in range(len(configs)):
            for j in range(i+1, len(configs)):
                comparison(configs[i], configs[j])