import random

from experiments_generator.models.key_models import *
from experiments_generator.generators.keys import KeysGenerator, KeySpec, GenerateKeyCommand

from expgen_testing import randomgen, mocks
from expgen_testing.fakes import FakeGenerateKeyCommand

import pytest
from unittest.mock import Mock, ANY
import pathlib
from os import path


class KeysGeneratorTests(object):
    def setup_method(self):
        self.fakecommand = FakeGenerateKeyCommand()
        self.command = Mock(spec=GenerateKeyCommand)
        self.command.generate_key.side_effect = self.fakecommand.generate_key
        self.keysdb = mocks.mock_keysdb()
        self.fs = mocks.mocked_fs()

        self.keys_generator = KeysGenerator(
            keygen_command=self.command,
            keysdb=self.keysdb,
            fs=self.fs
        )

    def test_generate_key_specs(self):
        # Given
        nodes   = randomgen.gen_nodes_list()
        out_dir = self.output_directory()

        specs = self.keys_generator.generate_key_specs(nodes, out_dir)

        # Then
        self.verify_has_specs_for_each_node(specs, nodes)
        self.verify_has_both_key_types(specs)


    def test_provide_keys_from_database(self):
        # Given
        out_dir        = self.output_directory()
        specs          = randomgen.key_specs()
        matching_keys = self.given_database_has_some_matching_keys(specs)

        # When
        provided, missing = self.keys_generator.provide_key_specs(
                                                                specs, out_dir)

        # Then
        assert len(provided) + len(missing) == len(specs)
        for spec in missing:
            assert spec in specs

        self.verify_node_keys_matches(provided, matching_keys)


    def test_generate_keys_for_each_node(self):
        # Given:
        out_dir   = self.output_directory()
        key_specs = randomgen.key_specs()

        gen_keys = self.keys_generator.generate_keys_from_specs(
                        key_specs, out_dir
                    )

        # Then:
        self.fs.ensure_dir.assert_any_call(out_dir)

        self.verify_keys_generated_for(key_specs)
        self.verify_has_keys_for(key_specs, gen_keys)

    def test_generate_keys(self):
        # Given
        nodes = randomgen.gen_nodes_list()
        out_dir = self.output_directory()

        gen_keys = self.keys_generator.generate_keys(nodes, out_dir)

        # Then
        self.verify_generated_keys_matches(gen_keys, nodes, out_dir)

    def test_generate_only_missing_keys(self):
        # Given
        self.given_database_has_some_matching_keys(randomgen.key_specs())

        # When
        self.generating_keys_for_some_nodes()

        # Then
        self.should_generate_only_missing_keys()

    def test_save_given_node_keys(self):
        # Given
        node_keys = randomgen.some_node_keys()
        out_dir   = self.output_directory()

        self.keys_generator.save_node_keys(node_keys, out_dir)

        # Then
        self.verify_keys_saved_on_dir_database(node_keys, out_dir)

    def test_save_generated_keys(self):
        # When
        node_keys = self.generating_keys_for_some_nodes()

        # Then
        self.verify_keys_saved_on_dir_database(node_keys, self.keysdir)


    def test_new_generated_keys_should_be_saved_on_new_directories(self):
        # Given
        keysdir = self.output_directory()
        self.these_directories_exist_on_keysdir(['ec0','ec2', 'ec1', 'rsa2', 'rsa0'], keysdir)

        # When
        node_keys = self.keys_generator.generate_keys_from_specs([
            KeySpec(node='n2', keytype=KeyTypes.ec),
            KeySpec(node='n3', keytype=KeyTypes.rsa)
        ], keysdir)

        # Then
        self.node_keys_directories_should_be(node_keys, [
            path.join(keysdir, 'ec3'), path.join(keysdir, 'rsa3')
        ])

    # ---------------------------------------------------------------------

    def generating_keys_for_some_nodes(self):
        self.nodes = randomgen.gen_nodes_list()
        self.keysdir = self.output_directory()

        return self.keys_generator.generate_keys(self.nodes, self.keysdir)

    # ---------------------------------------------------------------------

    def output_directory(self):
        return 'any/path'

    def nodes_map(self):
        n_map = {}

        for i in range(random.randint(3, 5)):
            n_name = f'n{i}'
            keytype = random.choice([KeyTypes.ec, KeyTypes.rsa])
            n_map[n_name] = None

        return n_map


    def given_database_has_some_matching_keys(self, specs):
        num_matching = random.randint(1, len(specs) - 1)
        matching     = random.sample(specs, num_matching)

        keys_info = [ randomgen.gen_key_from_spec(spec) for spec in matching]

        self.keysdb.load_all.return_value = keys_info

        return keys_info

    def these_directories_exist_on_keysdir(self, dirs, keysdir):
        self.fs.listdir.return_value = dirs


    def verify_keys_generated_for(self, key_specs):
        for spec in key_specs:
            self.command.generate_key.assert_any_call(
                    spec.keytype, ANY
                )

    def verify_has_keys_for(self, key_specs, gen_keys):
        assert gen_keys is not None

        assert len(gen_keys) == len(key_specs)

    def verify_has_specs_for_each_node(self, specs, nodes):
        assert specs is not None

        assert len(specs) == len(nodes)

        nodes_in_specs = [spec.node for spec in specs]

        assert sorted(nodes_in_specs) == sorted(nodes)

    def verify_has_both_key_types(self, specs):
        count_ec=0
        count_rsa=0

        for spec in specs:
            if spec.keytype == KeyTypes.ec:
                count_ec += 1
            elif spec.keytype == KeyTypes.rsa:
                count_rsa += 1

        assert count_ec > 0
        assert count_rsa > 0
        assert count_ec + count_rsa == len(specs)


    def verify_generated_keys_matches(self, gen_keys, nodes, out_dir):
        assert len(gen_keys) == len(nodes)

        # ...

    def verify_node_keys_matches(self, node_keys, keys_info):
        assert len(node_keys) == len(keys_info)

        for node_key in node_keys:
            assert node_key.node is not None
            assert node_key.keyinfo in keys_info

    def should_generate_only_missing_keys(self):
        # TO-DO: improve verification
        calls = self.command.generate_key.call_args_list

        assert len(calls) < len(self.nodes)

    def verify_keys_saved_on_dir_database(self, node_keys, out_dir):
        keys_info = [nk.keyinfo for nk in node_keys]

        self.keysdb.save_all.assert_called_with(keys_info, out_dir)

    def node_keys_directories_should_be(self, node_keys, expected_dirs):
        assert len(node_keys) == len(expected_dirs)

        found_dirs = [nk.keyinfo.keysdir for nk in node_keys]

        assert sorted(found_dirs) == sorted(expected_dirs)

    def dirname(self, dir_path):
        return pathlib.PurePath(dir_path).name