package discovery.test.bundle.mockups;

import discovery.test.mockups.DhtBuilderMockup;
import discovery.bundle.BundleDiscoveryConfiguration;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.common.mockups.IpDiscoverMockup;
import discovery.test.common.mockups.PkiMockup;
import discovery.bundle.BundleDiscoveryConfiguration.BundleDiscoveryConfigurationOptions;

class BundleDependenciesMockup {
    var keysFactoryMockup:KeysFactoryMockup;
    var pkiMockup:PkiMockup;
    var ipDiscoverMockup:IpDiscoverMockup;
    var dhtBuilderMockup:DhtBuilderMockup;

    public function new() {
        keysFactoryMockup= new KeysFactoryMockup();
        pkiMockup        = new PkiMockup();
        ipDiscoverMockup = new IpDiscoverMockup();
        dhtBuilderMockup = new DhtBuilderMockup();
    }

    public function dependencies(): BundleDiscoveryConfigurationOptions {
        return {
            keysfactory: keysFactoryMockup.keysFactory(),
            pkiBuilder: pkiMockup.pkiBuilder(),
            promises  : new FakePromisableFactory(),
            ipDiscover: ipDiscoverMockup.ipDiscover(),
            dhtBuilder: dhtBuilderMockup.builder()
        };
    }

    public function buildConfiguration() {
        return new BundleDiscoveryConfiguration(dependencies());
    }

    public function dhtBuilder() {
        return dhtBuilderMockup;
    }
}