package discovery.test.bundle.mockups;

import discovery.test.common.mockups.KeychainMockup;
import mockatoo.Mockatoo;
import mockatoo.Mock;
import discovery.bundle.keychain.KeychainsGetter;

class KeychainsGetterMockup {
    var keychainsGetterMock:KeychainsGetter;

    var keychainMock:KeychainMockup;
    var keycacheMock:KeychainMockup;

    public function new(?keychainsGetterMock: KeychainsGetter) {
        if(keychainsGetterMock == null){
            keychainsGetterMock = Mockatoo.mock(KeychainsGetter);
        }
        this.keychainsGetterMock = keychainsGetterMock;

        keychainMock = new KeychainMockup();
        keycacheMock = new KeychainMockup();

        Mockatoo
            .when(keychainsGetterMock.keychain())
            .thenReturn(keychainMock.keychain());
        Mockatoo
            .when(keychainsGetterMock.keyCache())
           .thenReturn(keycacheMock.keychain());
    }


    public function keychainsGetter():KeychainsGetter {
        return keychainsGetterMock;
    }

    public function verifyKeyCacheCalled() {
        Mockatoo.verify(keychainsGetterMock.keyCache());
    }
}

