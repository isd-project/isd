package discovery.test.bundle.mockups;

import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.test.common.mockups.crypto.KeyLoaderMockup;
import mockatoo.Mockatoo;
import discovery.bundle.keychain.KeysFactory;

class KeysFactoryMockup {
    var keysFactoryMock:KeysFactory;

    var keyloaderMockup:KeyLoaderMockup;
    var keychainsGetter:KeychainsGetterMockup;

    public function new() {
        keysFactoryMock = Mockatoo.mock(KeysFactory);

        keyloaderMockup = new KeyLoaderMockup();
        keychainsGetter = new KeychainsGetterMockup(keysFactoryMock);

        Mockatoo
            .when(keysFactoryMock.keyloader())
            .thenReturn(keyloaderMockup.keyLoader());
    }

    public function keysFactory():Null<KeysFactory> {
        return keysFactoryMock;
    }

    public function verifyKeyloaderCalled() {
        Mockatoo.verify(keysFactoryMock.keyloader());
    }

    public function verifyKeychainBuilt() {
        Mockatoo.verify(keysFactoryMock.keychain());
    }

    public function verifyKeycacheBuilt() {
        keychainsGetter.verifyKeyCacheCalled();
    }

    public function verifyConfiguredWith(
        config:ConfigureKeysOptions, ?cacheOptions:ConfigureKeysOptions)
    {
        Mockatoo.verify(keysFactoryMock.configure(config, cacheOptions));
    }
}