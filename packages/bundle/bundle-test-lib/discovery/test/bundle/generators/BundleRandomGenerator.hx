package discovery.test.bundle.generators;

import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.test.helpers.RandomGen;
import discovery.bundle.ConfigureOptions;


class BundleRandomGenerator {
    public function new() {

    }

    public function configureOptions():ConfigureOptions {
        return {
            keys: keysConfiguration(),
            mechanisms: mechanismList()
        };
    }

    public function keysConfiguration():ConfigureKeysOptions {
        return {
            keysdir: RandomGen.primitives.name(),
            keychainName: RandomGen.primitives.name()
        };
    }

    function mechanismList():Null<Array<String>> {
        if(RandomGen.primitives.boolean()) return null;

        var num = RandomGen.primitives.int(1, 5);

        return [ for(i in 0...num) RandomGen.primitives.name() ];
    }
}