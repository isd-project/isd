import massive.munit.TestSuite;

import discovery.test.bundle.tests.DefaultKeysFactoryTest;
import discovery.test.bundle.tests.BundleDiscoveryConfigurationTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.bundle.tests.DefaultKeysFactoryTest);
		add(discovery.test.bundle.tests.BundleDiscoveryConfigurationTest);
	}
}
