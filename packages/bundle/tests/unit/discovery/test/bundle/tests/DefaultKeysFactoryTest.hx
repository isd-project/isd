package discovery.test.bundle.tests;

import discovery.bundle.keychain.DefaultKeysFactory;
import discovery.bundle.keychain.KeychainBuilder;
import mockatoo.Mockatoo;
import discovery.bundle.keychain.KeysFactory;
import discovery.bundle.keychain.ConfigureKeysOptions;
import hxgiven.Steps;
import hxgiven.Scenario;

import discovery.test.common.matchers.MockatooMatchers.*;


class DefaultKeysFactoryTest implements Scenario{
    @steps
    var steps: DefaultKeysFactorySteps;

    @Test
    @scenario
    public function build_key_cache_at_configured_dir(){
        given().configurations({
            keysdir: 'some/keysdir'
        }, {
            keysdir: 'keycache/dir'
        });
        when().getting_key_cache();
        then().a_keychain_should_be_built_with({
            keysdir: 'keycache/dir',
            keychainName: 'cache'
        });
    }
}



class KeychainBuilderMockup {
    var keychainBuilder = Mockatoo.mock(KeychainBuilder);

    public function new() {
    }

    public function builder(){
        return keychainBuilder;
    }

	public function verifyBuiltKeychainWith(opt:ConfigureKeysOptions) {
        Mockatoo.verify(keychainBuilder.buildKeychain(equalsMatcher(opt)));
    }
}

class DefaultKeysFactorySteps extends Steps<DefaultKeysFactorySteps>{
    var keychainBuilder = new KeychainBuilderMockup();
    var defaultFactory:KeysFactory;

    public function new(){
        super();

        defaultFactory = new DefaultKeysFactory(keychainBuilder.builder());
    }

    // given ---------------------------------------------------
    @step
    public function configurations(
        defaultOpts:ConfigureKeysOptions, cacheOptions:ConfigureKeysOptions)
    {
        defaultFactory.configure(defaultOpts, cacheOptions);
    }

    // when  ---------------------------------------------------

    @step
    public function getting_key_cache() {
        defaultFactory.keyCache();
    }

    // then  ---------------------------------------------------

    @step
    public function a_keychain_should_be_built_with(opt:ConfigureKeysOptions) {
        keychainBuilder.verifyBuiltKeychainWith(opt);
    }
}