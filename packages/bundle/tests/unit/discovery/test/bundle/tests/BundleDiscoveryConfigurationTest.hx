package discovery.test.bundle.tests;

import discovery.dht.dhttypes.PeerAddress;
import discovery.test.mockups.DhtBuilderMockup;
import mockatoo.Mockatoo.VerificationMode;
import discovery.test.helpers.RandomGen;
import discovery.test.common.mockups.keytransport.KeyTransportBuilderMockup;
import discovery.test.common.mockups.CompositeMockup;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.base.configuration.NamedValue.Named;
import discovery.base.configuration.composite.NamedMechanismBuilder;
import discovery.test.common.mockups.CompositeMechanismsBuilderMockup;
import discovery.bundle.ConfigureOptions;
import discovery.test.bundle.generators.BundleRandomGenerator;
import discovery.test.bundle.mockups.KeysFactoryMockup;
import discovery.test.common.mockups.IpDiscoverMockup;
import discovery.test.fakes.FakePromisableFactory;
import discovery.test.common.mockups.PkiMockup;
import discovery.bundle.BundleDiscoveryConfiguration;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;
using discovery.utils.IteratorTools;
using discovery.test.matchers.CommonMatchers;


class BundleDiscoveryConfigurationTest implements Scenario{
    @steps
    var steps: BundleDiscoveryConfigurationSteps;

    @Test
    @scenario
    public function build_discovery_from_dependencies(){
        given().a_discovery_builder_instance();
        when().calling_build();
        then().a_discovery_instance_should_be_created_from_given_dependencies();
    }

    @Test
    @scenario
    public function default_mechanism_builders(){
        given().a_discovery_builder_instance();
        when().listing_the_available_mechanisms();
        then().it_should_list_mechanisms_named_with(['mdns', 'dht']);
    }

    @Test
    @scenario
    public function configure_keys_build(){
        given().a_discovery_builder_instance();
        given().some_keys_configuration();
        when().configuring_the_builder();
        then().the_keys_configuration_should_be_delegated_to_the_keys_factory();
    }

    @Test
    @scenario
    public function configure_key_cache(){
        given().a_configuration_with({
            keys: { keysdir: '/example/keys/dir' },
            keysCache: { keysdir: '/keys/cache/dir' }
        });
        when().configuring_the_builder();
        then().the_keys_factory_should_be_configured_with_the_given_options();
    }

    @Test
    @scenario
    public function select_mechanisms(){
        given().a_discovery_builder_instance();
        given().the_discovery_builder_has_some_named_mechanisms();
        given().a_configuration_with_a_subset_of_these_mechanisms();
        when().configuring_the_builder();
        and().calling_build();
        then().only_the_selected_mechanisms_should_be_built();
    }


    @Test
    @scenario
    public function should_not_select_mechanisms_if_none_are_specified(){
        given().a_discovery_builder_instance();
        given().the_discovery_builder_has_some_named_mechanisms();
        given().a_configuration_with_no_mechanisms();
        when().configuring_the_builder();
        and().calling_build();
        then().all_mechanisms_should_be_built();
    }

    // key mechanisms ----------------------------------------------

    @Test
    @scenario
    public function default_key_transport_builders(){
        given().a_discovery_builder_instance();
        when().listing_the_available_key_mechanisms();
        then().it_should_have_these_key_mechanisms(['mdns', 'dht']);
    }


    @Test
    @scenario
    public function build_key_transports(){
        given().a_discovery_builder_instance();
        given().these_key_transport_builders(['A', 'B', 'C', 'D']);
        when().calling_build();
        then().all_key_transport_builders_should_be_built();
    }

    @Test
    @scenario
    public function select_pkis(){
        given().a_discovery_builder_instance();
        given().these_key_transport_builders(['A', 'B', 'C', 'D']);
        given().a_configuration_with({
            pkis: ['D', 'A']
        });
        when().configuring_the_builder();
        and().calling_build();
        then().these_key_transports_should_be_built(['D', 'A']);
        but().these_key_transports_should_not_be_built(['B', 'C']);
    }
}

class BundleDiscoveryConfigurationSteps
    extends Steps<BundleDiscoveryConfigurationSteps>
{

    var bundleBuilder:BundleDiscoveryConfiguration;

    var generator:BundleRandomGenerator;
    var bundleDependencies:BundleDiscoveryConfigurationOptions;
    var keysFactoryMockup:KeysFactoryMockup;
    var pkiMockup:PkiMockup;
    var ipDiscoverMockup:IpDiscoverMockup;
    var dhtBuilderMockup:DhtBuilderMockup;

    var compositeMechanismBuilders:CompositeMechanismsBuilderMockup;
    var compositeKeyTransportBuilders:CompositeMockup<
                                                    KeyTransportBuilderMockup>;
    var keyBuilderMap:Map<String, KeyTransportBuilderMockup>;

    var discovery:Discovery;
    var availableMechanisms:Array<NamedMechanismBuilder>;
    var availableKeyMechanisms:Array<Named<KeyTransportBuilder>>;
    var configOptions:ConfigureOptions;

    public function new(){
        super();

        generator = new BundleRandomGenerator();

        keysFactoryMockup= new KeysFactoryMockup();
        pkiMockup        = new PkiMockup();
        ipDiscoverMockup = new IpDiscoverMockup();
        dhtBuilderMockup = new DhtBuilderMockup();

        compositeMechanismBuilders = new CompositeMechanismsBuilderMockup();
        compositeKeyTransportBuilders = new CompositeMockup(()->{
            return new KeyTransportBuilderMockup();
        });

        bundleDependencies = {
            keysfactory: keysFactoryMockup.keysFactory(),
            promises  : new FakePromisableFactory(),
            ipDiscover: ipDiscoverMockup.ipDiscover(),
            dhtBuilder: dhtBuilderMockup.builder()
        };
        bundleBuilder = new BundleDiscoveryConfiguration(bundleDependencies);

        keyBuilderMap = new Map();
    }

    public function teardown(){
        if(discovery != null){
            discovery.finish(()->{});
        }
    }

    // summary ---------------------------------------------------

    @step
    public function a_discovery_builder_instance()
    {}

    // given ---------------------------------------------------

    @step
    public function some_keys_configuration() {
        configOptions = {
            keys: generator.keysConfiguration()
        };
    }


    @step
    public function the_discovery_builder_has_some_named_mechanisms() {
        compositeMechanismBuilders.populate();
        compositeMechanismBuilders.forNamedBuilders((named)->{
            bundleBuilder.addMechanism(named.value, named.name);
        });
    }


    @step
    public function these_key_transport_builders(names:Array<String>) {
        for(name in names){
            var builderMockup = compositeKeyTransportBuilders.addOne();
            var builder = builderMockup.builder();

            keyBuilderMap.set(name, builderMockup);
            bundleBuilder.keyTransportBuilder().addBuilder(builder, name);
        }
    }

    @step
    public function a_configuration_with_no_mechanisms() {
        configOptions = {
            mechanisms: []
        };
    }

    @step
    public function a_configuration_with_a_subset_of_these_mechanisms() {
        var selected = compositeMechanismBuilders.selectMechanisms();

        configOptions = {
            mechanisms: selected
        };
    }


    @step
    public function a_configuration_with(config: ConfigureOptions) {
        this.configOptions = config;
    }

    // when  ---------------------------------------------------

    @step
    public function calling_build() {
        discovery = bundleBuilder.build();
    }

    @step
    public function listing_the_available_mechanisms() {
        availableMechanisms = bundleBuilder.listMechanismBuilders().toArray();
    }

    @step
    public function listing_the_available_key_mechanisms() {
        availableKeyMechanisms = bundleBuilder.keyTransportBuilder()
                                                .listBuilders().toArray();
    }

    @step
    public function configuring_the_builder() {
        bundleBuilder.configure(configOptions);
    }

    // then  ---------------------------------------------------

    @step
    public function
        a_discovery_instance_should_be_created_from_given_dependencies()
    {
        then().a_valid_discovery_instance_should_be_returned();
        and().the_dependencies_should_be_used();
    }

    @step
    public function a_valid_discovery_instance_should_be_returned() {
        discovery.shouldNotBeNull('discovery');
    }

    @step
    function the_dependencies_should_be_used() {
        keysFactoryMockup.verifyKeyloaderCalled();
        keysFactoryMockup.verifyKeychainBuilt();

        // Dht build is made only on demand
        // dhtBuilderMockup.verifyDhtBuilt(times(1));
    }

    @step
    public function
        it_should_list_mechanisms_named_with(expectedNames:Array<String>)
    {
        containsTheseNamedValues(availableMechanisms, expectedNames,
            'Available mechanism builders does not match the expected');
    }

    @step
    public function it_should_have_these_key_mechanisms(expected:Array<String>)
    {
        containsTheseNamedValues(availableKeyMechanisms, expected,
            'Available key transport builders does not match the expected');
    }

    function containsTheseNamedValues<T>(
        namedValues:Array<Named<T>>, expectedNames: Array<String>, ?msg:String)
    {

        var availableNames = namedValues.map((named)->named.name);

        assertThat(availableNames, containsInAnyOrder(expectedNames), msg);
    }

    @step
    public function the_keys_configuration_should_be_delegated_to_the_keys_factory() {
        then().the_keys_factory_should_be_configured_with_the_given_options();
    }

    @step
    public function
        the_keys_factory_should_be_configured_with_the_given_options()
    {
        keysFactoryMockup.verifyConfiguredWith(
                configOptions.keys, configOptions.keysCache
            );
    }

    @step
    public function all_mechanisms_should_be_built() {
        compositeMechanismBuilders.verifyAllMechanismsWereBuilt();
    }

    @step
    public function only_the_selected_mechanisms_should_be_built() {
        compositeMechanismBuilders.verifyOnlySelectedMechanismWereBuilt();
    }

    @step
    public function all_key_transport_builders_should_be_built() {
        compositeKeyTransportBuilders.numComponents().shouldBeGreaterThan(0);

        compositeKeyTransportBuilders.forEach((m)->m.verifyBuildCalled());
    }

    @step
    public function these_key_transports_should_be_built(names:Array<String>) {
        verifyKeyTransportsBuild(names, times(1));
    }

    @step
    public
    function these_key_transports_should_not_be_built(names:Array<String>) {
        verifyKeyTransportsBuild(names, never);
    }

    function verifyKeyTransportsBuild(
                names: Array<String>, ?times: VerificationMode)
    {
        filterKeyTransports(names)
            .forEach(mockup -> mockup.verifyBuildCalled(times));
    }

    function filterKeyTransports(names:Array<String>) {
        return [
            for(name in names) keyBuilderMap.get(name)
        ];
    }
}