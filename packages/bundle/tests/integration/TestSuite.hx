import massive.munit.TestSuite;

import discovery.test.bundle.tests.JsKeychainBuilderTest;
import discovery.test.bundle.tests.BundleBuilderIntegrationTest;
import discovery.test.bundle.tests.BundleDhtConfigurationTest;
import discovery.test.bundle.scenarios.IntegratedKeyPublicationTest;
import discovery.test.bundle.scenarios.IntegratedKeyDiscoveryTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.bundle.tests.JsKeychainBuilderTest);
		add(discovery.test.bundle.tests.BundleBuilderIntegrationTest);
		add(discovery.test.bundle.tests.BundleDhtConfigurationTest);
		add(discovery.test.bundle.scenarios.IntegratedKeyPublicationTest);
		add(discovery.test.bundle.scenarios.IntegratedKeyDiscoveryTest);
	}
}
