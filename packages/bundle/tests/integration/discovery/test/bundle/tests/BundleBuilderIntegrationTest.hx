package discovery.test.bundle.tests;

import discovery.Discovery;
import discovery.base.configuration.DiscoveryBuilder;
import discovery.bundle.BundleDiscoveryConfiguration;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.utils.IteratorTools;
using discovery.test.matchers.CommonMatchers;


class BundleBuilderIntegrationTest implements Scenario{
    @steps
    var steps: BundleBuilderIntegrationSteps;

    @After
    public function teardown(){
        steps.teardown();
    }

    @Test
    @scenario
    public function build_discovery(){
        given().a_bundle_discovery_builder();
        when().calling_build();
        then().a_valid_discovery_instance_should_be_returned();
    }
}

class BundleBuilderIntegrationSteps extends Steps<BundleBuilderIntegrationSteps>{

    var bundleBuilder:DiscoveryBuilder;

	var discovery:Discovery;

    public function new(){
        super();
    }

    public function teardown(){
        if(discovery != null){
            discovery.finish(()->{});
        }
    }

    // given ---------------------------------------------------

    @step
    public function a_bundle_discovery_builder() {
        bundleBuilder = new BundleDiscoveryConfiguration();
    }

    // when  ---------------------------------------------------

    @step
    public function calling_build() {
        discovery = bundleBuilder.build();
    }

    // then  ---------------------------------------------------

    @step
    public function a_valid_discovery_instance_should_be_returned() {
        discovery.shouldNotBeNull('discovery');
    }
}