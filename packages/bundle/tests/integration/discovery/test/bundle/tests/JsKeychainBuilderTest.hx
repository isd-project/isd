package discovery.test.bundle.tests;

import sys.FileSystem;
import discovery.keys.Keychain;
import discovery.test.helpers.RandomGen;
import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.bundle.keychain.js.JsKeychainBuilder;
import discovery.test.fakes.FakePromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.test.integration.tools.TemporaryDir;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class JsKeychainBuilderTest implements Scenario{
    @steps
    var steps: JsKeychainBuilderSteps;

    @After
    public function teardow() {
        steps.teardown();
    }

    @Test
    @scenario
    public function build_keychain_from_configuration(){
        given().a_builder_instance();
        given().a_configuration_with_the_keys_storage_directory_and_name();
        when().building_the_keychain();
        then().a_valid_keychain_should_be_created();
        and().the_keychain_storage_should_be_created();
    }
}

class JsKeychainBuilderSteps extends Steps<JsKeychainBuilderSteps>{
    var builder:JsKeychainBuilder;

    var tempDir:TemporaryDir;
    var promises:PromisableFactory;
    var config:ConfigureKeysOptions;

    var builtKeychain:Keychain;


    public function new(){
        super();

        promises = new FakePromisableFactory();
        tempDir = new TemporaryDir();
    }

    public function teardown() {
        tempDir.removeDir();
    }

    // given ---------------------------------------------------

    @step
    public function a_builder_instance() {
        builder = new JsKeychainBuilder(promises);
    }

    @step
    public function a_configuration_with_the_keys_storage_directory_and_name() {
        config = {
            keysdir: tempDir.path(),
            keychainName: RandomGen.primitives.name()
        };
    }

    // when  ---------------------------------------------------

    @step
    public function building_the_keychain() {
        builtKeychain = builder.buildKeychain(config);
    }

    // then  ---------------------------------------------------

    @step
    public function a_valid_keychain_should_be_created() {
        builtKeychain.shouldNotBeNull();
    }

    @step
    public function the_keychain_storage_should_be_created() {
        FileSystem.exists(config.keysdir).shouldBe(true,
            'Key directory was not created (${config.keysdir})');
    }
}