package discovery.test.bundle.tests;

import discovery.dht.dhttypes.DhtOptions;
import discovery.test.generators.DhtRandomGenerator;
import discovery.bundle.ConfigureOptions;
import discovery.bundle.BundleDiscoveryConfiguration;
import discovery.test.bundle.mockups.BundleDependenciesMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class BundleDhtConfigurationTest implements Scenario{
    @steps
    var steps: BundleDhtConfigurationSteps;

    @Test
    @scenario
    public function pass_start_options_to_dht(){
        given().a_bundle_discovery_configuration_is_built();
        given().the_bundle_is_configured_with_some_dht_configurations();
        when().a_discovery_is_built();
        and().the_dht_is_started();
        then().the_start_options_should_be_used_at_the_dht();
    }
}

class BundleDhtConfigurationSteps extends Steps<BundleDhtConfigurationSteps>{
	var bundle:BundleDiscoveryConfiguration;

    var bundleDependenciesMockup:BundleDependenciesMockup;
    var dhtGenerator:DhtRandomGenerator;

    var config:ConfigureOptions;
    var discovery:Discovery;
	var dhtOptions:DhtOptions;

    public function new(){
        super();

        bundleDependenciesMockup = new BundleDependenciesMockup();
        dhtGenerator = new DhtRandomGenerator();
    }

    function dhtMockup() {
        return bundleDependenciesMockup.dhtBuilder().dht();
    }

    // given ---------------------------------------------------

    @step
    public function a_bundle_discovery_configuration_is_built() {
        bundle = bundleDependenciesMockup.buildConfiguration();
    }

    @step
    public function the_bundle_is_configured_with_some_dht_configurations() {
        dhtOptions = dhtGenerator.dhtOptions();
        bundle.configure({
            dht: dhtOptions
        });
    }

    // when  ---------------------------------------------------

    @step
    public function a_discovery_is_built() {
        discovery = bundle.build();
    }

    @step
    public function the_dht_is_started() {
        //dht should starts
        discovery.search({type: 'example'});
    }

    // then  ---------------------------------------------------

    @step
    public function the_start_options_should_be_used_at_the_dht() {
        var startOpts = dhtMockup().lastStartOptions();

        startOpts.bind.shouldBeEqualsTo(dhtOptions.bind);
        startOpts.bootstrap.shouldBeEqualsTo(dhtOptions.bootstrap);
    }
}