package discovery.test.bundle.scenarios;

import discovery.async.DoneCallback;
import discovery.keys.crypto.Fingerprint;
import discovery.base.search.Stoppable;
import discovery.keys.keydiscovery.SearchKeyListeners.SearchKeyResult;
import discovery.utils.functional.CompositeDoneCallback;
import discovery.test.integration.data.KeysTestData;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.test.helpers.RandomGen;
import discovery.keys.storage.PublicKeyData;
import discovery.test.integration.tools.NextCallback;
import haxe.Exception;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;
using discovery.keys.KeyTransformations;


class NamedPair<T> {
    public var name:String;
    public var value:T;

    public function new(name:String, value:T) {
        this.name = name;
        this.value = value;
    }

    public static function make<T>(name:String, value:T) {
        return new NamedPair(name, value);
    }
}


class IntegratedKeyPublicationTest extends AsyncTests implements Scenario{
    @steps
    var steps: IntegratedKeyPublicationSteps;

    @After
    public function teardown() {
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public function publish_key_for_different_mechanisms(factory: AsyncFactory)
    {
        makeHandler(factory, 1500);

        given().the_required_dht_infrastructure(next(()->{
            given().an_dht_key_client();
                and().an_mdns_key_client();
            given().an_integrated_key_provider();
            when().the_integrated_provider_publishes_a_key(next(()->{
                and().both_clients_search_for_the_key(next(()->{
                    then().both_clients_should_have_found_the_key();
                    done();
                }));
            }));
        }));
    }

}

class IntegratedKeyPublicationSteps
    extends IntegratedKeyTransportSteps<IntegratedKeyPublicationSteps>
{
    var dhtKeyClient:KeyTransport;
    var mdnsKeyClient:KeyTransport;
    var integratedProvider:KeyTransport;

    var publishedKey:PublicKeyData;

    var clientSearchs:Array<ClientSearch>;

    public function new(){
        super();

        clientSearchs = new Array();
    }

    // given ---------------------------------------------------

    @step
    public function an_dht_key_client() {
        dhtKeyClient = dhtKeyTransportBuilder.buildKeyTransport();
    }

    @step
    public function an_mdns_key_client() {
        mdnsKeyClient = mdnsKeyTransportBuilder.buildKeyTransport();
    }

    @step
    public function an_integrated_key_provider() {
        integratedProvider = compositeKeyTransportBuilder.buildKeyTransport();
    }

    // when  ---------------------------------------------------

    @step
    public function the_integrated_provider_publishes_a_key(next: NextCallback)
    {
        //TO-DO: looks like using a random key fails the test (sometimes)
        // publishedKey = RandomGen.crypto.publicKeyData();
        publishedKey = KeysTestData.elliptCurveKey.toPublicKeyData();

        integratedProvider.publishKey(publishedKey, (?err)->{
            next(err);
        });
    }

    @step
    public function both_clients_search_for_the_key(next: NextCallback){
        clientSearchs = RandomGen.primitives.shuffle([
            new ClientSearch(dhtKeyClient, 'dht'),
            new ClientSearch(mdnsKeyClient, 'mdns')
        ]);

        var doneSearch = CompositeDoneCallback.make(clientSearchs.length, next);

        for (search in clientSearchs){
            search.start(publishedKey.fingerprint, doneSearch);
        }
    }

    // then  ---------------------------------------------------

    @step
    public function both_clients_should_have_found_the_key() {
        var errors:Array<Exception> = [];
        var keysFound:Array<PublicKeyData> = [];

        for(search in clientSearchs){
            if(search.error != null)    errors.push(search.error);
            if(search.foundKey != null) keysFound.push(search.foundKey);
        }

        errors.shouldBeEmpty('No errors on key search were expected');
        keysFound.length.shouldBe(clientSearchs.length,
            'Found only these keys: ${keysFound}');

        for(search in clientSearchs){
            search.foundKey.equals(publishedKey).shouldBe(true,
                'Failed for: ${search.name}\n'
                + '\tExpected: ${publishedKey}\n'
                + '\tbut found: ${search.foundKey}');
        }
    }

}

class ClientSearch {

    public var name(default, null):String;
    public var foundKey:PublicKeyData;
    public var error(default, null): Exception;

    var client:KeyTransport;
    var stopSearch:Stoppable = null;
	var doneCallback:Null<DoneCallback>;

    public function new(client:KeyTransport, name: String) {
        this.client = client;
        this.name = name;
    }

    public function start(fingerprint: Fingerprint, ?done:DoneCallback) {
        doneCallback = done;

        stopSearch = client.searchKey(fingerprint, {
            onKey: onKey,
            onEnd: onEnd
        });

        //already found, so ensure to stop search
        if(foundKey != null){
            stopSearch.stop();
        }
    }

    function onKey(result: SearchKeyResult) {
        foundKey = result.found;

        if(stopSearch != null){
            stopSearch.stop();
        }
    }

    function onEnd(?err: Exception) {
        if(err != null){
            error = err;
        }

        if(doneCallback != null){
            doneCallback(err);
        }
    }
}