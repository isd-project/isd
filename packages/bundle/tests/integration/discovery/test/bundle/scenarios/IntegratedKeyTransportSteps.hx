package discovery.test.bundle.scenarios;

import discovery.utils.building.DisposableBuilder;
import discovery.dht.DisposableDhtBuilder;
import discovery.mdns.MdnsEngine;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.mdns.externs.bonjour.BonjourMdnsEngine;
import discovery.mdns.pki.MdnsKeyTransportBuilder;
import discovery.base.configuration.composite.CompositeKeyTransportBuilder;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.dhttypes.building.DhtComponentsAdapter;
import discovery.dht.DhtKeyTransportBuilder;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.test.integration.tools.NextCallback;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.test.integration.DhtInfrastructureBuilder;
import discovery.dht.DhtBuilder;
import hxgiven.Steps;

class IntegratedKeyTransportSteps<Self:IntegratedKeyTransportSteps<Self>>
    extends Steps<Self>
{

    var dhtBuilder:DisposableDhtBuilder;
    var dhtComponents:DhtComponents;
    var dhtInfra:DhtInfrastructureBuilder;

    var mdnsBuilder:DisposableBuilder<MdnsEngine>;

    var dhtKeyTransportBuilder:DhtKeyTransportBuilder;
    var mdnsKeyTransportBuilder:KeyTransportBuilder;
    var compositeKeyTransportBuilder:KeyTransportBuilder;

    public function new(){
        super();

        dhtBuilder = new DisposableDhtBuilder(new DrpcNodeBuilder());
        dhtComponents = new DhtComponentsAdapter(dhtBuilder);
        dhtInfra = new DhtInfrastructureBuilder(dhtBuilder);

        var keyLoader = new JscuKeyGenerator();

        mdnsBuilder = new DisposableBuilder(
            ()->new BonjourMdnsEngine(),
            (engine:MdnsEngine)->engine.finish()
        );

        mdnsKeyTransportBuilder = new MdnsKeyTransportBuilder({
            engine: mdnsBuilder.build,
            keyLoader: keyLoader,
            promises: new JsPromisableFactory()
        });
        dhtKeyTransportBuilder = new DhtKeyTransportBuilder(dhtComponents);
        compositeKeyTransportBuilder = makeCompositeBuilder();
    }

    function makeCompositeBuilder():KeyTransportBuilder {
        var compositeBuilder = new CompositeKeyTransportBuilder();
        compositeBuilder.addBuilder(dhtKeyTransportBuilder, 'dht');
        compositeBuilder.addBuilder(mdnsKeyTransportBuilder, 'mdns');

        return compositeBuilder;
    }

    public function teardown() {
        dhtInfra.stop();

        mdnsBuilder.disposeAll();
        dhtBuilder.closeAll();
    }

    // given ---------------------------------------------------

    @step
    public function the_required_dht_infrastructure(next: NextCallback) {
        dhtInfra.startBootstrapNode((?err)->{

            if(err == null){
                var bootInfo = dhtInfra.bootstrapInfo();
                dhtKeyTransportBuilder.configure(bootInfo);
            }

            next(err);
        });
    }


    // when  ---------------------------------------------------


    // then  ---------------------------------------------------

}