package discovery.test.bundle.scenarios;

import discovery.base.search.Stoppable;
import discovery.keys.storage.PublicKeyData;
import discovery.utils.functional.CompositeDoneCallback;
import discovery.test.integration.data.KeysTestData;
import discovery.keys.keydiscovery.KeyTransport;
import discovery.test.integration.tools.NextCallback;
import massive.munit.async.AsyncFactory;
import discovery.test.integration.tools.AsyncTests;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class IntegratedKeyDiscoveryTest extends AsyncTests implements Scenario{
    @steps
    var steps: IntegratedKeyDiscoverySteps;

    @After
    public function teardown() {
        steps.teardown();
    }

    @AsyncTest
    @scenario
    public
    function discover_key_from_different_mechanisms(factory: AsyncFactory)
    {
        makeHandler(factory, 1500);


        given().the_required_dht_infrastructure(next(()->{
            given().an_dht_key_provider();
                and().an_mdns_key_provider();
            given().an_integrated_key_client();
            when().both_providers_publish_the_same_key(next(()->{
                and().the_integrated_client_searches_for_that_key(next(()->{
                    then().the_client_should_find_the_key_from_any_provider();
                    done();
                }));
            }));
        }));
    }
}

class IntegratedKeyDiscoverySteps
    extends IntegratedKeyTransportSteps<IntegratedKeyDiscoverySteps>
{
    var mdnsKeyProvider:KeyTransport;
    var dhtKeyProvider:KeyTransport;
    var integratedClient:KeyTransport;

    var publishedKey:PublicKeyData;
    var foundKey:PublicKeyData;
    var searchStop:Stoppable;

    // given ---------------------------------------------------

    @step
    public function an_mdns_key_provider() {
        mdnsKeyProvider = mdnsKeyTransportBuilder.buildKeyTransport();
    }

    @step
    public function an_dht_key_provider() {
        dhtKeyProvider = dhtKeyTransportBuilder.buildKeyTransport();
    }

    @step
    public function an_integrated_key_client() {
        integratedClient = compositeKeyTransportBuilder.buildKeyTransport();
    }

    // when  ---------------------------------------------------

    @step
    public function both_providers_publish_the_same_key(next: NextCallback) {
        publishedKey = KeysTestData.rsaKey4096;

        var compositeDone = CompositeDoneCallback.make(2, next);

        for (provider in [mdnsKeyProvider, dhtKeyProvider]){
            provider.publishKey(publishedKey, compositeDone);
        }
    }

    @step
    public
    function the_integrated_client_searches_for_that_key(next: NextCallback) {
        searchStop = integratedClient.searchKey(publishedKey.fingerprint, {
            onKey: (result)->{
                foundKey = result.found;
                searchStop.stop();
                next();
            }
        });
    }

    // then  ---------------------------------------------------

    @step
    public
    function the_client_should_find_the_key_from_any_provider()
    {
        foundKey.equals(publishedKey).shouldBe(true,
            'Expected: ${publishedKey}\n'
            + 'found: ${foundKey}');
    }

}