package discovery.bundle.keychain;


@:structInit
class ConfigureKeysOptions{

    @:optional
    public var keysdir: String;

    @:optional
    public var keychainName: String;

    public function clone(): ConfigureKeysOptions{
        return {
            keysdir: this.keysdir,
            keychainName: this.keychainName
        };
    }

    public function equals(other: ConfigureKeysOptions) {
        if(other == null) return false;

        return keysdir == other.keysdir
            && keychainName == other.keychainName;
    }
}