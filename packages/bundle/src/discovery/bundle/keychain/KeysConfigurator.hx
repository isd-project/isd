package discovery.bundle.keychain;

import discovery.utils.system.AppDirs;


class KeysConfigurator {
    public static final KEYS_DIR_ENV = "KEYSDIR";
    public static final APPNAME      = "discovery";

    public function new() {}

    public function getConfiguration(
        ?config: ConfigureKeysOptions): ConfigureKeysOptions
    {
        if(config == null) config = {};
        if(config.keysdir == null) config.keysdir = defaultKeysDir();

        return config;
    }

    function defaultKeysDir():Null<String> {
        var keysdir = Sys.getEnv(KEYS_DIR_ENV);
        if(keysdir == null){
            keysdir = appKeysDir();
        }

        return keysdir;
    }
    function appKeysDir():Null<String> {
        return AppDirs.user_data_dir(APPNAME);
    }
}