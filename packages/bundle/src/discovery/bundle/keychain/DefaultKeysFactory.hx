package discovery.bundle.keychain;

import discovery.keys.Keychain;
import discovery.keys.crypto.KeyLoader;

class DefaultKeysFactory implements KeysFactory{

    var _keychain:Keychain;
    var _keyCache:Keychain;
    var keychainBuilder: KeychainBuilder;
    var configOptions:ConfigureKeysOptions;
    var cacheOptions:ConfigureKeysOptions;

    public function new(keychainBuilder: KeychainBuilder) {
        this.keychainBuilder = keychainBuilder;
    }

    public function keyloader():KeyLoader {
        return keychainBuilder.keyloader();
    }

    public function configure(
        config: ConfigureKeysOptions, ?cacheOptions: ConfigureKeysOptions)
    {
        this.configOptions = config;
        this.cacheOptions = cacheOptions;
    }

    public function keychain() {
        if(_keychain == null){
            var conf = config();
            _keychain = buildKeychain(conf);
        }
        return _keychain;
    }

    public function keyCache() {
        if(_keyCache == null){
            var opt = keyCacheOptions();

            _keyCache = buildKeychain(opt);
        }
        return _keyCache;
    }

    function keyCacheOptions() {
        var opt = if(cacheOptions != null) cacheOptions.clone() else config();
        if(opt.keychainName == null || opt.equals(configOptions)){
            opt.keychainName = 'cache';
        }

        return opt;
    }

    function config(): ConfigureKeysOptions {
        if(configOptions == null) return {};

        return configOptions.clone();
    }

    function buildKeychain(conf:ConfigureKeysOptions):Keychain {
        return keychainBuilder.buildKeychain(conf);
    }
}