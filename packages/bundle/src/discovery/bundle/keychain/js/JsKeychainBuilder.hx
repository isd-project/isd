package discovery.bundle.keychain.js;

import discovery.keys.crypto.KeyLoader;
import discovery.async.promise.PromisableFactory;
import discovery.impl.keys.storage.lowdb.KeyStorageLowDb;
import discovery.impl.keys.jscu.JscuKeyGenerator;
import discovery.keys.storage.KeyStorage;
import discovery.keys.crypto.KeyGenerator;
import discovery.keys.BaseKeychain;
import discovery.keys.Keychain;


class JsKeychainBuilder implements KeychainBuilder{
    var promisesFactory:PromisableFactory;

    var keygen:KeyGenerator;

    public function new(promisesFactory: PromisableFactory) {
        this.promisesFactory = promisesFactory;
    }

    public function buildKeychain(?options: ConfigureKeysOptions):Keychain {
        options = configureKeys(options);
        return new BaseKeychain(
                    keyGenerator(),
                    keyStorage(options),
                    promisesFactory);
    }


    public function keyloader():KeyLoader {
        return keyGenerator();
    }

    public function keyGenerator():KeyGenerator {
        if(keygen == null){
            keygen = new JscuKeyGenerator();
        }
        return keygen;
    }

    function keyStorage(options:Null<ConfigureKeysOptions>):KeyStorage {
        return new KeyStorageLowDb({
            dir: options.keysdir,
            file: options.keychainName
        });
    }

    function configureKeys(
        options:Null<ConfigureKeysOptions>):Null<ConfigureKeysOptions>
    {
        var configurator = new KeysConfigurator();
        return configurator.getConfiguration(options);
    }
}