package discovery.bundle.keychain;

import discovery.keys.crypto.KeyLoader;
import discovery.keys.Keychain;

interface KeychainBuilder {
    public function buildKeychain(?options: ConfigureKeysOptions):Keychain;
    public function keyloader():KeyLoader;
}