package discovery.bundle.keychain;

import discovery.keys.Keychain;

interface KeychainsGetter{
    function keychain():Keychain;
    function keyCache():Keychain;
}