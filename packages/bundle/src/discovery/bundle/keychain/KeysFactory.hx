package discovery.bundle.keychain;

import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.keys.crypto.KeyLoader;

interface KeysFactory extends KeychainsGetter{
    public function configure(defaultOptions:ConfigureKeysOptions,
                                ?cacheOptions:ConfigureKeysOptions):Void;
    public function keyloader(): KeyLoader;
}