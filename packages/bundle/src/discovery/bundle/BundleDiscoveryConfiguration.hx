package discovery.bundle;

import discovery.keys.crypto.KeyLoader;
import discovery.async.promise.js.JsPromisableFactory;
import discovery.async.promise.PromisableFactory;
import discovery.base.configuration.composite.CompositeBuilder;
import discovery.base.configuration.composite.CompositeKeyTransportBuilder;
import discovery.base.configuration.composite.CompositeMechanismBuilder;
import discovery.base.configuration.composite.NamedMechanismBuilder;
import discovery.base.configuration.DiscoveryMechanismBuilder;
import discovery.base.configuration.PkiBuilder;
import discovery.bundle.keychain.DefaultKeysFactory;
import discovery.bundle.keychain.js.JsKeychainBuilder;
import discovery.bundle.keychain.KeysFactory;
import discovery.dht.DhtBuilder;
import discovery.dht.DhtDiscoveryMechanismBuilder;
import discovery.dht.dhttypes.building.DhtComponents;
import discovery.dht.dhttypes.building.DhtComponentsAdapter;
import discovery.dht.impl.js.dhtrpc.DrpcNodeBuilder;
import discovery.dht.DhtKeyTransportBuilder;
import discovery.domain.IpDiscover;
import discovery.impl.ipdiscovery.node.NodeIpDiscover;
import discovery.keys.keydiscovery.KeyTransportBuilder;
import discovery.keys.pki.BasePkiBuilder;
import discovery.mdns.building.NBonjourMechanismsBuilder;

typedef BundleDiscoveryConfigurationOptions = {
    ?promises: PromisableFactory,
    ?keysfactory: KeysFactory,
    ?pkiBuilder: PkiBuilder,
    ?ipDiscover: IpDiscover,
    ?dhtBuilder: DhtBuilder
};

class BundleDiscoveryConfiguration implements ConfigurableDiscoveryBuilder{

    var deps:BundleDiscoveryConfigurationOptions;
    var compositeMechanismBuilder:CompositeMechanismBuilder;
    var compositeKeyTransportBuilder:CompositeKeyTransportBuilder;

    var dhtBuilder:DhtDiscoveryMechanismBuilder;
    var _dhtComponents:DhtComponents;

    var _keyloader:KeyLoader;

    public function new(?options: BundleDiscoveryConfigurationOptions) {
        this.deps = fillDependencies(options);


        this.compositeMechanismBuilder = new CompositeMechanismBuilder();
        this.compositeKeyTransportBuilder = new CompositeKeyTransportBuilder();

        this.addDefaultMechanisms();
    }

    public function listSelectedDiscoveryMechanisms() {
        return compositeMechanismBuilder.listSelected();
    }


    static function fillDependencies(deps: BundleDiscoveryConfigurationOptions)
    {
        if(deps == null) deps = {};

        if(deps.promises == null) deps.promises = new JsPromisableFactory();

        if(deps.keysfactory == null){
            var keychainsBuilder = new JsKeychainBuilder(deps.promises);
            deps.keysfactory = new DefaultKeysFactory(keychainsBuilder);
        }

        if(deps.ipDiscover == null){
            deps.ipDiscover = new NodeIpDiscover();
        }

        if(deps.dhtBuilder == null) deps.dhtBuilder = new DrpcNodeBuilder();

        return deps;
    }

    function addDefaultMechanisms() {
        var bonjourBuilder = mdnsBuilder();
        dhtBuilder = dhtDiscoveryBuilder();

        compositeMechanismBuilder.addBuilder(bonjourBuilder, 'mdns');
        compositeMechanismBuilder.addBuilder(dhtBuilder, 'dht');

        compositeKeyTransportBuilder.addBuilder(bonjourBuilder, 'mdns');
        compositeKeyTransportBuilder.addBuilder(dhtKeyBuilder(), 'dht');
    }

    // -------------------------------------------------------------------------

    public
    function addMechanism(builder:DiscoveryMechanismBuilder, ?name:String)
    {
        compositeMechanismBuilder.addBuilder(builder, name);
    }

    public function listMechanismBuilders():Iterable<NamedMechanismBuilder> {
        return compositeMechanismBuilder.listBuilders();
    }

    public function keyTransportBuilder(): CompositeBuilder<KeyTransportBuilder>
    {
        return compositeKeyTransportBuilder;
    }

    public function configure(configOptions:ConfigureOptions) {
        if(deps.keysfactory != null){
            deps.keysfactory.configure(
                    configOptions.keys, configOptions.keysCache
                );
        }

        if(configOptions.mechanisms != null
            && configOptions.mechanisms.length > 0)
        {
            compositeMechanismBuilder.selectBuilders(configOptions.mechanisms);
        }

        var pkis = configOptions.pkis;
        if(pkis != null && pkis.length > 0)
        {
            compositeKeyTransportBuilder.selectBuilders(pkis);
        }

        if(configOptions.dht != null){
            dhtBuilder.configure(configOptions.dht);
        }
    }

    public function build():Discovery {
        return new Discovery({
            mechanism : compositeMechanismBuilder.buildMechanism(),
            promises  : deps.promises,
            keychain  : deps.keysfactory.keychain(),
            ipDiscover: deps.ipDiscover,
            pki       : pkiBuilder().buildPki()
        });
    }

    public function keysFactory(): KeysFactory {
        return deps.keysfactory;
    }

    // ---------------------------------------------------------

    function pkiBuilder() {
        if(deps.pkiBuilder == null){
            deps.pkiBuilder = new BasePkiBuilder({
                transportBuilder: compositeKeyTransportBuilder,
                promises: deps.promises,
                keyCache: deps.keysfactory.keyCache(),
                keyLoader: keyloader()
            });
        }

        return deps.pkiBuilder;
    }

    function mdnsBuilder() {
        return new NBonjourMechanismsBuilder({
            promises: deps.promises,
            keyloader: keyloader(),
            keycacheGetter: ()->deps.keysfactory.keyCache()
        });
    }

    function dhtDiscoveryBuilder():DhtDiscoveryMechanismBuilder {
        var mechanismBuilder = new DhtDiscoveryMechanismBuilder({
            dhtComponents: dhtComponents(),
            promises: deps.promises
        });

        return mechanismBuilder;
    }

    function dhtKeyBuilder():KeyTransportBuilder {
        return new DhtKeyTransportBuilder(dhtComponents());
    }

    function dhtComponents():DhtComponents {
        if(_dhtComponents == null){
            _dhtComponents = new DhtComponentsAdapter(deps.dhtBuilder);
        }

        return _dhtComponents;
    }

    function keyloader():KeyLoader {
        if(_keyloader == null){
            _keyloader = deps.keysfactory.keyloader();
        }

        return _keyloader;
    }
}