package discovery.bundle;

import discovery.base.configuration.DiscoveryBuilder;

interface ConfigurableDiscoveryBuilder extends DiscoveryBuilder{
    function configure(configOptions:ConfigureOptions):Void;
}