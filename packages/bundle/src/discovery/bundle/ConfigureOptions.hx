package discovery.bundle;

import discovery.dht.dhttypes.DhtOptions;
import discovery.bundle.keychain.ConfigureKeysOptions;

@:structInit
class ConfigureOptions{
    @:optional
    public var keys: ConfigureKeysOptions;

    @:optional
    public var keysCache: ConfigureKeysOptions;

    @:optional
    public var dht: DhtOptions;

    @:optional
    public var mechanisms: Array<String>;

    @:optional
    public var pkis(default, default): Array<String>;

    public function new(
        ?keys:ConfigureKeysOptions,
        ?keysCache:ConfigureKeysOptions,
        ?mechanisms: Array<String>,
        ?pkis: Array<String>,
        ?dht:DhtOptions)
    {
        if(keys == null) keys = {};

        this.keys = keys;
        this.keysCache = keysCache;
        this.mechanisms = mechanisms;
        this.pkis = pkis;
        this.dht = dht;
    }
}
