# Local Key Management

In order to easy the use of cryptographic keys,
the `cli` application should provide a simple mechanism
to the key management locally.

The `cli` application assumes that may exist a _main_ key that will be used
on the most part of time.
But, besides that, there can be a number of other keys used for different
purposes, applications or context.

So, should be possible to the user:

* **setup a default key** (or none)
  - This key will be used by default in each running of the `cli`.
* **Add (generate), list or remove keys from keychain**
* **specify one key (from the keychain) to be used in some operation**
  - The key may be selected from a unique _fingerprint_ prefix (similar to how git identify commits)
  - Or, from a name (alias) associated with the key
* **add, change or remove key aliases**
  - In order that a key can be selected

> **To-Do**: include key distribution (to import/export/share keys)

---------------------------------

## Examples

* **Select keys to publish**
  - `cli` publish "my server" http 8080 --id 25623b5
  - `cli` publish "another server" http 1234 --id mykey
* **List keys**
  - `cli` keys
* **Generate keys**
  - `cli` keys gen
    - > Need to select algorithm, and other parameters 
  - `cli` keys gen --alg rsa --len 2048 --name "rsakey"
* **Keys default**
  - `cli` keys default
    - show default key
  - `cli` keys default set mykey
  - `cli` keys default clear
* **Key alias**
  - `cli` keys name 25623b5
    - show name
  - `cli` keys name 25623b5 --as mykey
    - set name
  - `cli` keys name 25623b5 --clear
    - remove name
* ...