package discovery.cli.commands;

import discovery.async.promise.Promisable;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;

typedef OutputKeysOptions = {
    ?json:Bool,
    ?output:String
};

interface KeyCommands {
    function configureKeysOutput(options: OutputKeysOptions):Void;
    function listKeys():Void;
    function generateKeys(options: GenerateKeyOptions):Promisable<Any>;
    function importKey(keyId:String):Void;
}