package discovery.cli.exceptions;

import haxe.Exception;

class MissingArgumentsException extends ArgumentsException{
    public var missingArgument(default, null):String;

    public function new(argument:String, msg:String, ?previous:Exception) {
        super(msg, previous);

        this.missingArgument = argument;
    }
}