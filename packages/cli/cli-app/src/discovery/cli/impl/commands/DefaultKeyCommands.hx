package discovery.cli.impl.commands;

import discovery.keys.KeyTransformations;
import discovery.keys.Key;
import discovery.keys.crypto.FingerprintParser;
import discovery.cli.configuration.Configuration;
import discovery.cli.console.Console;
import discovery.keys.storage.KeyData;
import discovery.async.promise.Promisable;
import discovery.keys.Keychain;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.cli.commands.KeyCommands;

class DefaultKeyCommands implements KeyCommands{
    var configuration:Configuration;

    var console(get, never):Console;

    var keysFormatter = new KeysFormatter();
    var outputOptions:OutputKeysOptions;

    public function new(configuration:Configuration) {
        this.configuration = configuration;
    }

    function get_console():Console {
        return configuration.console;
    }

    public function configureKeysOutput(options:OutputKeysOptions) {
        this.outputOptions = options;

        if(options.output != null){
            configuration.redirectOutputToFile(options.output);
        }
    }

    public function listKeys() {
        var options = outputOptions;
        if(options == null) options = {};

        var keys = requireKeychain().listKeys();

        if(options.json){
            console.println(keysFormatter.keysToJson(keys));
        }
        else{
            //TO-DO: improve formatting
            for (k in keys){
                console.println(keysFormatter.keyToString(k));
            }
        }
    }

    public function generateKeys(options:GenerateKeyOptions): Promisable<Any>{
        return requireKeychain()
                .generateKey(options)
                .then(exportKey)
                .then(printGeneratedKey);
    }

    function exportKey(key: Key) {
        return KeyTransformations.toKeyData(key);
    }
    function printGeneratedKey(key:KeyData) {
        //TO-DO: improve formatting
        if(outputOptions != null && outputOptions.json){
            console.println(keysFormatter.keyToJson(key));
        }
        else{
            console.println("Generated key:\n");
            console.println(keysFormatter.keyToString(key));
        }
    }


    public function importKey(keyId:String):Void {
        var fingerprint = new FingerprintParser().parse(keyId);

        configuration.pki().searchKey(fingerprint).then((key)->{
            console.println('Imported key: ${key.fingerprint}');
        }, (err)->{
            console.printError(
                'Failed to import key (${keyId}), due to search error: ${err}\n');
        });
    }

    // --------------------------------------------------------------------

    function requireKeychain():Keychain {
        return configuration.keychain();
    }
}