package discovery.cli.impl.commands;

import discovery.domain.ErrorEventInfo;
import discovery.cli.console.Console;
import discovery.cli.formatting.DiscoveredFormatter;
import discovery.bundle.ConfigureOptions;
import discovery.domain.Discovered;
import discovery.domain.PublishInfo;
import discovery.format.exceptions.ParseException;
import discovery.format.exceptions.InvalidFormatException;
import discovery.base.IdentifierParser;
import discovery.cli.configuration.Configuration;
import discovery.domain.Publication.PublicationFinished;
import discovery.domain.Description;
import haxe.Exception;
import discovery.cli.impl.commands.DefaultKeyCommands;
import discovery.cli.commands.KeyCommands;
import discovery.domain.Search;
import tink.core.Noise;
import tink.core.Future;
import discovery.domain.Query;

using discovery.impl.async.tink.AsyncEvents;

typedef FinishDiscoveryOptions = {
    ?force:Bool,
    ?error:Exception
}

@await
class DiscoveryCommands implements CliCommands{
    var configuration:Configuration;
    var keyCommands:KeyCommands;

    var console(get, never):Console;

    public function new(configuration: Configuration)
    {
        this.configuration = configuration;

        this.keyCommands = new DefaultKeyCommands(configuration);
    }

    function get_console():Console {
        return configuration.console;
    }

    public function onError(error:Exception) {
        //To-Do: improve format
        this.console.printError('Failed due to error: ${error}\n');
        this.console.printError(error.details());
    }

    public function help(helpText: String) {
        this.console.println(helpText);
    }

    public function keys(): KeyCommands {
        return keyCommands;
    }


    public function configure(?config:ConfigureOptions) {
        configuration.configure(config);
    }

    @await
    public function publish(description: Description) {
        var discovery = discovery();

        //start publication
        var publication = discovery.publish(description);

        publication.onInternalError.listen((errInfo)->{
            notifyInternalError(errInfo);
        });

        var publishedEvt = @await publication.oncePublished.nextAsync();

        console.println(
            'published service "${description.name}" with type "${description.type}" at port "${description.port}"');

        var publishedInfo:PublishInfo = publishedEvt.published;
        var identifierUrl = publishedInfo.getIdentifier().toUrl();
        if(identifierUrl != null){
            console.println(identifierUrl);
        }

        //If publication finishs, force termination
        publication.onceFinished.listen((finished:PublicationFinished)->{
            finishDiscovery(discovery, {force: true});
        });

        waitForInputAsync((input)->{
            publication.stop();
            finishDiscovery(discovery);
        });
    }

    @await
    public function search(query:Query) {
        var discovery = this.discovery();
        //start search
        var search = discovery.search(query);

        doSearch(search);
    }

    public function locate(identifierUrl:String) {
        try{
            var identifier = idParser().parseUrl(identifierUrl);
            var search = discovery().locate(identifier);

            doSearch(search);
        }
        catch(e: InvalidFormatException){
            onInvalidUrl(identifierUrl, e);
        }
        catch(e: ParseException){
            onParsingUrlError(identifierUrl, e);
        }
    }

    function onInvalidUrl(identifierUrl:String, e:InvalidFormatException) {
        console.printError('Url "$identifierUrl" is invalid.\n${e}');
    }

    function onParsingUrlError(identifierUrl:String, e:ParseException) {
        console.printError('Could not parse url "$identifierUrl" due to error:\n${e}');
    }

    // ------------------------------------------------------------------------

    function discovery(): Discovery{
        return configuration.discovery();
    }

    function idParser(): IdentifierParser{
        return configuration.idParser();
    }

    // ------------------------------------------------------------------------

    function doSearch(search:Search) {
        var discovery = discovery();

        search.onFound.listen((found: Discovered)->{
            printFoundService(found);
        });

        search.onceFinished.listen((evt)->{
            finishSearch(search, discovery);
        });

        waitForInputAsync((line:String)->{
            finishSearch(search, discovery);
        });
    }

    function printFoundService(found: Discovered) {
        var formatter = new DiscoveredFormatter();
        console.println(formatter.formatDiscovered(found));
    }

    function notifyInternalError(errInfo:ErrorEventInfo) {
        var msg = new StringBuf();
        msg.add('Internal error: "${errInfo.error.details()}"');

        if(errInfo.context != null){
            msg.add('; on: "${errInfo.context}"');
        }
        // if debug:
        // msg.add('; at: ${errInfo.posInfo}');
        msg.add('\n');

        console.printError(msg.toString());
    }

    function waitForInput() {
        console.println('press enter to finish');
        console.readline();
    }

    function waitForInputAsync(callback:(String)->Void) {
        console.println('press enter to finish');
        console.onReadline(callback);
    }

    function finishSearch(search:Search, discovery:Discovery) {
        search.stop();
        finishDiscovery(discovery, {force:true});
    }

    function finishDiscovery(discovery: Discovery, ?options:FinishDiscoveryOptions)
    {
        @await Future.irreversible(function (handler:Noise->Void) {
            if(options != null && options.error != null){
                onError(options.error);
            }

            var callback = handler.bind(null);
            discovery.finish(callback);

            if (options != null && options.force){
                Sys.exit(0);
            }
        }).eager();
    }
}