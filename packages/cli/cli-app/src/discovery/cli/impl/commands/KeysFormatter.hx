package discovery.cli.impl.commands;

import discovery.format.Formatters;
import discovery.keys.crypto.ExportedKey;
import haxe.Json;
import discovery.keys.storage.KeyData;


class KeysFormatter {
    public function new() {}

    public function keysToJson(keys: Array<KeyData>): String {
        var compactKeys = [
            for(k in keys) CompactKeyData.fromKeyData(k)
        ];

        return toJson(compactKeys);
    }

    public function keyToJson(keyData:KeyData):String {
        return toJson(CompactKeyData.fromKeyData(keyData));
    }

    function toJson<T>(value: T) {
        return Json.stringify(value, '  ');
    }

    public function keyToString(k:KeyData):String {
        var name = k.name;
        var keyType = k.keyType;
        var fingerprint = k.fingerprint;
        var fingerprintHex = fingerprint.hash.toBytes().toHex();

        return 'key:\t${if(name!=null) name else ""}\n'
            +   '    - type: ${if(keyType == null) null else keyType.getName()}\n'
            +   '    - fingerprint: ${fingerprint.alg}:${fingerprintHex}\n';
    }
}

typedef CompactExportedKey = {
    format: String,
    data: Dynamic
};

@:structInit
class CompactKeyData {
    public var type:String;
    public var fingerprint:String;
    public var publicKey:CompactExportedKey;

    public static function fromKeyData(keyData: KeyData):CompactKeyData {
        return {
            type: keyData.keyType.getName(),
            fingerprint: keyData.fingerprint.toString(),
            publicKey: {
                format: keyData.publicKey.format.getName(),
                data: exportKeyData(keyData.publicKey)
            }
        };
    }

    static function exportKeyData(key:ExportedKey):Dynamic {
        if(key == null || key.data == null) return null;

        return switch (key.format){
            case Pem: key.data.toString();
            case Jwk: Json.parse(key.data.toString());
            case Der: Formatters.base64.encode(key.data.toBinary());
            default: key.data;
        }
    }
}