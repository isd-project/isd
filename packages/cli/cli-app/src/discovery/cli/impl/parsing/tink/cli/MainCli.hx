package discovery.cli.impl.parsing.tink.cli;

import discovery.bundle.ConfigureOptions;
import haxe.io.Path;
import tink.Cli;


class MainCli
    implements CliWithHelp
    implements ConfigurableCli
    implements CommonCliMixin
{
    var commands:CliCommands;
    var configureExtractor:ConfigureExtractor;

    public function new(commands: CliCommands) {
        this.commands = commands;
        this.keys = new KeysCli(commands);
        this.help = false;

        this.configureExtractor = new ConfigureExtractor();
    }

    // Cli interface ------------------------------------------------

    /**
        Provider id used to publish a service.
        This id should be a hexadecimal prefix or name of one of the stored keys.
    **/
    public var id(default, default):String=null;

    /**
        manage keys.
    **/
    @:command
    public var keys:KeysCli;


    @:defaultCommand
    public function helpCommand() {
        showHelp(buildHelpMessage());
    }

    /**
        publishes service.
        args: <name> <type> <port>
    **/
    @:command
    public function publish(name: String, type: String, port: Int) {
        if(setup()){
            this.commands.publish({
                name: name,
                type: type,
                port: port,
                providerId: id
            });
        }
    }

    /**
        search for services with given type.
        args: <type>
    **/
    @:command
    public function search(type: String) {
        if(setup()){
            this.commands.search({
                type: type
            });
        }
    }

    /**
        locate the specified service.
        args: <service url>
    **/
    @:command
    public function locate(serviceUrl: String) {
        if(setup()){
            this.commands.locate(serviceUrl);
        }
    }


    // helpers ------------------------------------------------

    public function showHelp(helpMessage:String):Void{
        this.commands.help(helpMessage);
    }

    public function configure(options:ConfigureOptions):Void {
        this.commands.configure(options);
    }

    public function commandName():String {
        return '';
    }
}