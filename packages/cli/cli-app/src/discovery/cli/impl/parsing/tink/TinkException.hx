package discovery.cli.impl.parsing.tink;

import tink.core.Error;
import haxe.Exception;

class TinkException extends Exception {
    public var error(default, null):tink.core.Error;

    public function new(err: Error, message:String, ?previous:Exception) {
        super(message, previous);
        this.error = err;
    }
}