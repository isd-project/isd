package discovery.cli.impl.parsing.tink.cli;

import discovery.bundle.ConfigureOptions;
import haxe.io.Path;
import tink.Cli;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.keys.crypto.KeyTypes;
import discovery.cli.commands.KeyCommands;
import discovery.cli.exceptions.MissingArgumentsException;
import discovery.cli.exceptions.InvalidArgumentException;

class KeysCli
    implements CliWithHelp
    implements ConfigurableCli
    implements CommonCliMixin
{
    var commands:CliCommands;
    var keyCommands(get, never):KeyCommands;

    public function new(?commands:CliCommands) {
        // IllegalArgumentException.verify(keyCommands != null, "keyCommands", "keyCommands should not be null");
        this.commands = commands;
    }

    function get_keyCommands() {
        return commands.keys();
    }

    /**
        Key type used to generate keys.
        Supported: 'rsa' or 'ec' (elliptic curves)
    **/
    @:flag('kt', 'keytype')
    @:alias('t')
    public var keytype(default, default):String=null;

    /**
        Length of generated RSA key
    **/
    public var len(default, default):Int=2048;

    /**
        Named curve used to generate elliptic curve keys.
        Supported: P-256, P-384, P-521, P-256K
    **/
    public var curve(default, default):String='P-256';

    /**
        Flag used on list key to output result as json.
    **/
    public var json(default, default):Bool=false;


    /**
        When specified, the output is saved on this path.
        Specially useful to combine with json option.
    **/
    public var output(default, default):String=null;

    /**
        Generate a new key.
    **/
    @:command
    public function gen() {
        if(setup()){
            this.keyCommands.generateKeys(genKeyOptions());
        }
    }

    /**
        List keys on keychain.
    **/
    @:defaultCommand
    @:command('list')
    public function listKeys() {
        if(setup()){
            keyCommands.listKeys();
        }
    }

    /**
        Searches and imports a given key
    **/
    @:command('import')
    public function importKey(keyId: String) {
        if(setup()){
            keyCommands.importKey(keyId);
        }
    }

    function genKeyOptions():GenerateKeyOptions{
        return {
            keyType: parseKeyType()
        };
    }

    function parseKeyType():KeyTypes {
        if(keytype == null){
            throw new MissingArgumentsException("--keytype",
                "The key algorithm was not specified.");
        }
        return switch (keytype.toLowerCase()){
            case 'rsa':
                RSA({
                    modulusLength: len
                });
            case 'ec':
                EllipticCurve({
                    namedCurve: curve
                });
            default: {
                final supportedTypes = ['rsa', 'ec'];
                throw new InvalidArgumentException('Invalid key type "$keytype" in key generation, supported types are: $supportedTypes');}
        }
    }

    function outputKeyOptions():OutputKeysOptions {
        return {
            json: this.json,
            output: this.output
        };
    }


    // CommonCli interface ---------------------------------------------

    public function configure(options:ConfigureOptions) {
        commands.configure(options);

        keyCommands.configureKeysOutput(outputKeyOptions());
    }

    public function showHelp(msg:String) {
        this.commands.help(msg);
    }

    public function commandName():String {
        return 'keys';
    }
}