package discovery.cli.impl.parsing.tink.cli;

import discovery.dht.dhttypes.PeerAddress;
import discovery.dht.dhttypes.DhtOptions;
import haxe.DynamicAccess;
import discovery.bundle.ConfigureOptions;
import discovery.bundle.keychain.ConfigureKeysOptions;

using discovery.utils.Cast;

class ConfigureExtractor {
    public function new() {}

    public function extract(cli:CliWithConfigurations): ConfigureOptions
    {
        var options:ConfigureOptions = {};
        var keyOptions:ConfigureKeysOptions = {};

        if(cli.keysdir != null){
            keyOptions.keysdir = cli.keysdir;
        }

        options.keys = keyOptions;
        options.mechanisms = extractField(cli, 'mechanisms', Array);
        options.pkis       = extractField(cli, 'pkis', Array);
        options.dht = extractDhtOptions(cli);

        return options;
    }

    function extractField(cli:CliWithConfigurations, field:String, type:Dynamic)
    {
        var cliObj:DynamicAccess<Dynamic> = (cli : Dynamic);

        return cliObj.get(field).castIfIs(type);
    }

    function extractDhtOptions(cli:CliWithConfigurations):DhtOptions {
        var dhtOptions:DhtOptions = {};
        var bootstrapAddresses = extractField(cli, 'bootstrapAddresses', Array);

        if(bootstrapAddresses != null){
            dhtOptions.bootstrap = bootstrapAddresses.map((a)->{
                return PeerAddress.fromString(a);
            });

            return dhtOptions;
        }

        return null;
    }
}