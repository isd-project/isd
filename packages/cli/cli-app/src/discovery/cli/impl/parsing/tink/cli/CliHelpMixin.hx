package discovery.cli.impl.parsing.tink.cli;


@baseImplements(CliWithHelp)
@mixin
interface CliHelpMixin
{

    // Cli interface ------------------------------------------------

    /**
        Print help message.
    **/
    public var help(default, default):Bool=false;

    // helpers ------------------------------------------------


    function handleHelp(): Bool {
        if(help){
            showHelp(buildHelpMessage());
            return false;
        }

        return true;
    }

    function buildHelpMessage(): String {
        return usageHeader() + commandsHelp();
    }

    function usageHeader(): String{
        return 'Usage: ${programName()} ${commandName()} [flags]\n';
    }

    function programName(): String {
        var programFilename = Path.withoutDirectory(Sys.programPath());

        return programFilename;
    }

    function commandsHelp():String {
        return Cli.getDoc(this);
    }
}