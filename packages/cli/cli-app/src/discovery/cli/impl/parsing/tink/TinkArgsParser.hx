package discovery.cli.impl.parsing.tink;

import discovery.cli.exceptions.ArgumentsException;
import discovery.cli.exceptions.InvalidArgumentException;
import discovery.cli.impl.parsing.tink.cli.MainCli;
import discovery.cli.tink.NonePrompt;
import tink.core.Outcome;
import tink.core.Error;
import tink.core.Noise;

class TinkArgsParser implements ArgsParser{
    public function new() {}

    public function processArgs(args: Array<String>, commands:CliCommands):Void{
        try{
            tink.Cli.process(args, new MainCli(commands), new NonePrompt())
            .handle(function(outcome: Outcome<Noise, Error>){
                switch (outcome){
                    case Failure(error): processError(error, commands);
                    default:
                }
            });
        }
        catch(e: ArgumentsException){
            commands.onError(e);
        }
    }

    function processError(err:Error, commands:CliCommands) {
        var cause = new TinkException(err, err.message);

        var exception = switch (err.code){
            case ErrorCode.UnprocessableEntity: new InvalidArgumentException(
                "Failed to parse command line", cause);
            default: cause;
        };

        commands.onError(exception);
    }
}