package discovery.cli.impl.parsing.tink.cli;

interface CliWithConfigurations {
    public var keysdir(default, default):String;
    public var pkis(default, default):Array<String>;
}