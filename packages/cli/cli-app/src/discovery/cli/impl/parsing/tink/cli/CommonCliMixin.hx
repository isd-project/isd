package discovery.cli.impl.parsing.tink.cli;

import discovery.cli.impl.parsing.tink.cli.CliWithHelp;
import discovery.cli.impl.parsing.tink.cli.ConfigurableCli;
import discovery.bundle.ConfigureOptions;

@baseImplements(CliWithHelp)
@baseImplements(ConfigurableCli)
@mixin
interface CommonCliMixin extends CliWithConfigurations{

    // Cli interface ------------------------------------------------

    /**
        Print help message.
    **/
    public var help(default, default):Bool=false;

    /**
        Directory used to store the generated and imported keys.
    **/
    @:alias(false)
    public var keysdir(default, default):String=null;

    /**
        Specify discovery mechanisms to be used.
    **/
    @:flag('mechanism')
    @:alias('m')
    public var mechanisms(default, default):Array<String>=null;


    /**
        Specify PKIs to be used.
    **/
    @:flag('pki')
    public var pkis:Array<String>=null;

    /**
        Define one or more addresses to be used for bootstrap.
    **/
    @:flag('bootstrap')
    public var bootstrapAddresses(default, default):Array<String>=null;

    // helpers ------------------------------------------------


    function setup(): Bool {
        if(help){
            showHelp(buildHelpMessage());
            return false;
        }

        configure(extractConfiguration());

        return true;
    }

    function extractConfiguration(): ConfigureOptions {
        var configureExtractor = new ConfigureExtractor();
        return configureExtractor.extract(this);
    }

    function buildHelpMessage(): String {
        return usageHeader() + commandsHelp();
    }

    function usageHeader(): String{
        return 'Usage: ${programName()} ${commandName()} [flags]\n';
    }

    function programName(): String {
        var programFilename = Path.withoutDirectory(Sys.programPath());

        return programFilename;
    }

    function commandsHelp():String {
        return Cli.getDoc(this);
    }
}