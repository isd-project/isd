package discovery.cli.impl.parsing.tink.cli;

interface CliWithHelp{
    var help(default, default):Bool;

    function showHelp(helpMessage:String):Void;
    function commandName(): String;
}