package discovery.cli.impl.parsing.tink.cli;

import discovery.bundle.ConfigureOptions;

interface ConfigurableCli {
    function configure(options:ConfigureOptions):Void;
}