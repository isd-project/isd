package discovery.cli;

interface ArgsParser {
    function processArgs(args: Array<String>, commands:CliCommands):Void;
}