package discovery.cli.formatting;

import discovery.domain.Discovered;

class DiscoveredFormatter {
    public function new() {

    }

    public function formatDiscovered(discovered:Discovered): String {
        var srvDescription = discovered.description();

        var buffer = new StringBuf();
        buffer.add(
            'Found service: ${srvDescription.name}\n'
            + '    type: ${srvDescription.type}\n'
        );

        var addresses = [
            for (a in srvDescription.addresses()) '${a.address}:${a.port}'
        ];
        buffer.add('    addresses: [${addresses.join(', ')}]\n');

        return buffer.toString();

    }
}