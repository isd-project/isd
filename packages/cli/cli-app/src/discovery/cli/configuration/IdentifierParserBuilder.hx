package discovery.cli.configuration;

import discovery.base.IdentifierParser;

interface IdentifierParserBuilder extends Builder<IdentifierParser>{
}