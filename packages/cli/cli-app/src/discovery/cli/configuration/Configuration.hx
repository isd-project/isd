package discovery.cli.configuration;

import discovery.cli.console.RedirectConsole;
import sys.io.File;
import discovery.keys.PKI;
import discovery.cli.configuration.IdentifierParserBuilder;
import discovery.bundle.keychain.KeysFactory;
import discovery.bundle.ConfigurableDiscoveryBuilder;
import discovery.cli.console.Console;
import discovery.bundle.ConfigureOptions;
import discovery.base.IdentifierParser;

typedef ConfigurationDependencies = {
    console: Console,
    discoveryBuilder: ConfigurableDiscoveryBuilder,
    keysFactory:KeysFactory,
    idParserBuilder:IdentifierParserBuilder
};

class Configuration{
    public var console  (default, null): Console;
    var originalConsole:Console;

    var discoveryBuilder:ConfigurableDiscoveryBuilder;
    var keysFactory:KeysFactory;
    var idParserBuilder:IdentifierParserBuilder;

    var _discovery:Discovery;
    var _idParser:IdentifierParser;

    public function new(deps: ConfigurationDependencies)
    {
        this.console = deps.console;
        this.originalConsole = deps.console;

        this.discoveryBuilder = deps.discoveryBuilder;
        this.keysFactory = deps.keysFactory;
        this.idParserBuilder = deps.idParserBuilder;
    }

    public function configure(?config: ConfigureOptions){
        if(config == null) config = {};

        discoveryBuilder.configure(config);

        keychains().configure(config.keys);
    }

    public function redirectOutputToFile(filepath: String){
        var output = File.write(filepath, false);

        this.console = new RedirectConsole(output, this.originalConsole);
    }

    public function discovery() {
        if(_discovery == null){
            _discovery = discoveryBuilder.build();
        }
        return _discovery;
    }

    public function keychain() {
        return keychains().keychain();
    }

    function keychains() {
        return keysFactory;
    }

    public function idParser(): IdentifierParser {
        if(_idParser == null){
            _idParser = idParserBuilder.build();
        }
        return _idParser;
    }

    public function pki(): PKI {
        return discovery().pki();
    }
}
