package discovery.cli.configuration;

interface Builder<T> {
    function build():T;
}