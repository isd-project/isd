package discovery.cli.configuration.builders;

import discovery.impl.parser.TinkIdentifierParser;
import discovery.base.IdentifierParser;

class TinkIdentifierParserBuilder implements IdentifierParserBuilder{
    public function new() {}

    public function build():IdentifierParser {
        return new TinkIdentifierParser();
    }
}