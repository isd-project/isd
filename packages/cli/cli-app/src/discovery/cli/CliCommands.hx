package discovery.cli;

import discovery.bundle.ConfigureOptions;
import discovery.domain.Description;
import haxe.Exception;
import discovery.cli.commands.KeyCommands;
import discovery.domain.Query;


interface CliCommands {
    function configure(?config:ConfigureOptions):Void;

    //To-Do: publish and search commands should be async (return a Promisable)
    function publish(description:Description):Void;
    function search (query:Query):Void;
    function locate (serviceUrl:String):Void;

    function help(helpText: String):Void;
    function keys():KeyCommands;

    function onError(error:Exception):Void;
}