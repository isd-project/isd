package discovery.cli;

import discovery.bundle.BundleDiscoveryConfiguration;
import discovery.cli.configuration.Configuration;
import discovery.cli.configuration.builders.TinkIdentifierParserBuilder;
import discovery.cli.impl.commands.DiscoveryCommands;
import discovery.cli.console.SysConsole;
import discovery.cli.impl.parsing.tink.TinkArgsParser;

class Main {
    static function main() {
        var configuration = buildConfiguration();
        var cliCommands = new DiscoveryCommands(configuration);
        var argsParser  = new TinkArgsParser();

        argsParser.processArgs(Sys.args(), cliCommands);
    }

    static function buildConfiguration(): Configuration {
        var bundle = new BundleDiscoveryConfiguration();

        var idParserBuilder = new TinkIdentifierParserBuilder();
        var console = new SysConsole();

        var configuration = new Configuration({
            console: console,
            discoveryBuilder: bundle,
            keysFactory: bundle.keysFactory(),
            idParserBuilder: idParserBuilder
        });

        return configuration;
    }
}
