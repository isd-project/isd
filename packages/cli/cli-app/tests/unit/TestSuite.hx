import massive.munit.TestSuite;

import discovery.test.tests.parsing.tink.ConfigureExtractorTest;
import discovery.test.tests.parsing.tink.DiscoveryCommandsParsingTest;
import discovery.test.tests.parsing.tink.DiscoveryDhtParsingTest;
import discovery.test.tests.parsing.tink.HelpParsingTest;
import discovery.test.tests.parsing.tink.keyscli.KeysCommandsParsingTest;
import discovery.test.tests.parsing.tink.keyscli.KeyGenCommandParsingTest;
import discovery.test.tests.parsing.tink.CommandsConfigurationTest;
import discovery.test.tests.commands.KeyCommandsTest;
import discovery.test.tests.commands.discovery.LocateCommandTest;
import discovery.test.tests.commands.discovery.ConfigureCommandTest;
import discovery.test.tests.commands.discovery.PublishCommandTest;
import discovery.test.tests.console.RedirectConsoleTest;
import discovery.test.tests.configuration.ConfigurationTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(discovery.test.tests.parsing.tink.ConfigureExtractorTest);
		add(discovery.test.tests.parsing.tink.DiscoveryCommandsParsingTest);
		add(discovery.test.tests.parsing.tink.DiscoveryDhtParsingTest);
		add(discovery.test.tests.parsing.tink.HelpParsingTest);
		add(discovery.test.tests.parsing.tink.keyscli.KeysCommandsParsingTest);
		add(discovery.test.tests.parsing.tink.keyscli.KeyGenCommandParsingTest);
		add(discovery.test.tests.parsing.tink.CommandsConfigurationTest);
		add(discovery.test.tests.commands.KeyCommandsTest);
		add(discovery.test.tests.commands.discovery.LocateCommandTest);
		add(discovery.test.tests.commands.discovery.ConfigureCommandTest);
		add(discovery.test.tests.commands.discovery.PublishCommandTest);
		add(discovery.test.tests.console.RedirectConsoleTest);
		add(discovery.test.tests.configuration.ConfigurationTest);
	}
}
