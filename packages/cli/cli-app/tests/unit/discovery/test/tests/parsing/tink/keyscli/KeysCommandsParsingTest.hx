package discovery.test.tests.parsing.tink.keyscli;

import discovery.cli.commands.KeyCommands.OutputKeysOptions;
import discovery.dht.dhttypes.PeerAddress;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps.ArgsParserCommonSteps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class KeysCommandsParsingTest implements Scenario{
    @steps
    var steps:KeysCommandsParsingSteps;

    @Before
    public function setup(){
        resetSteps();
    }

    @Test
    @scenario
    public function listing_keys() {
        given().a_cli_parser_and_commands();
        when().processing_args(["keys"]);
        then().the_configure_command_should_be_called_with_default();
        then().the_list_keys_command_should_be_called();
    }

    @Test
    @scenario
    public function configure_keys_output(){
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", '--json', '-o', 'output.json']);
        then().the_configure_command_should_be_called_with_default();
        and().the_keys_output_should_be_configured_with({
            json: true,
            output: 'output.json'
        });
    }

    @Test
    @scenario
    public function import_key(){
        var keyId = anyKeyId();

        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "import", keyId, "--pki", "mdns"]);
        then().the_configure_command_should_be_called_with_these_options({
            pkis: ["mdns"]
        });
        and().the_import_keys_command_should_be_called_with(keyId);
    }

    @Test
    @scenario
    public function import_key_bootstrap(){
        var keyId = anyKeyId();

        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "import", keyId,
                                    "--bootstrap", "1.2.3.4"]);
        then().the_configure_command_should_be_called_with_these_options({
            dht: {
                bootstrap: [PeerAddress.fromString('1.2.3.4')]
            }
        });
        and().the_import_keys_command_should_be_called_with(keyId);
    }

    function anyKeyId() {
        return 'b3d0bd4081c4a56270b2c0d3a09904e6';
    }
}

class KeysCommandsParsingSteps
    extends ArgsParserCommonSteps<KeysCommandsParsingSteps>
{
    //then ---------------------------------
    @step
    public function the_list_keys_command_should_be_called() {
        then().last_command_should_be("keys-list", []);
    }

    @step
    public function
        the_keys_output_should_be_configured_with(opt:OutputKeysOptions)
    {
        commands.keys().outputKeysOptions.shouldBeEqualsTo(opt,
            'Output keys options does not match the expected value'
        );
    }

    @step
    public function
        the_import_keys_command_should_be_called_with(keyId:String)
    {
        then().last_command_should_be("keys-importKey", [keyId]);
    }
}