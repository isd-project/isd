package discovery.test.tests.parsing.tink.keyscli;

import discovery.cli.exceptions.InvalidArgumentException;
import discovery.cli.exceptions.MissingArgumentsException;
import massive.munit.Assert;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps.ArgsParserCommonSteps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


using haxe.EnumTools.EnumValueTools;


class KeyGenCommandParsingTest implements Scenario{
    @steps
    var steps:KeyGenCommandParsingSteps;

    @Before
    public function setup(){
        resetSteps();
    }

    @Test
    @scenario
    public function generate_rsa_key() {
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "gen", "--keytype", "rsa", "--len", "1024", "--keysdir",  "some/directory"]);
        then().the_configure_command_should_be_called_with_these_key_options({
            keysdir: "some/directory"
        });
        then().the_generate_key_command_should_be_called_with(
            {
                keyType: RSA({
                    modulusLength: 1024
                })
            }
        );
    }

    @Test
    @scenario
    public function generate_elliptic_curve_key() {
        given().a_cli_parser_and_commands();
        when().processing_args([
            "keys", "gen", "--keytype", "ec", "--curve", "P-384"]);

        then().the_configure_command_should_be_called_with_default();
        then().the_generate_key_command_should_be_called_with(
            {
                keyType: EllipticCurve({
                    namedCurve: 'P-384'
                })
            }
        );
    }

    @Test
    @scenario
    public function generate_key_no_type() {
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "gen"]);
        then().a_command_error_should_be_notified_of_type(MissingArgumentsException);
    }


    @Test
    @scenario
    public function generate_key_invalid_type() {
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "gen", "--keytype", "Invalid"]);
        then().a_command_error_should_be_notified_of_type(InvalidArgumentException);
    }

    @Test
    @scenario
    public function generate_key_invalid_rsa_length() {
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "gen", "--len", "abcd"]);
        then().a_command_error_should_be_notified_of_type(InvalidArgumentException);
    }
}

class KeyGenCommandParsingSteps
    extends ArgsParserCommonSteps<KeyGenCommandParsingSteps>
{
    //then ---------------------------------
    @step
    public function the_generate_key_command_should_be_called_with(
        expectedGenOptions: GenerateKeyOptions)
    {
        then().last_command_is('keys-gen');
        // then().last_command_should_be('keys-gen', ([genKey, config]:Array<Dynamic>));

        var commandArgs = this.commands.args;
        assertThat(commandArgs, hasSize(1));
        assertThat(commandArgs[0], is(notNullValue()));

        var genOptions:GenerateKeyOptions = commandArgs[0];

        assertThat(genOptions, is(notNullValue()));
        assertThat(genOptions.keyType, is(notNullValue()), "Key type should not be null");
        assertThat(genOptions.keyType.getName(),
            is(equalTo(expectedGenOptions.keyType.getName())));

        if(expectedGenOptions.keyType.getParameters() != null){
            assertThat(genOptions.keyType.getParameters(), is(notNullValue()),
                'Key type parameters should be ${expectedGenOptions.keyType.getParameters()}'
            );

            Assert.areEqual(
                expectedGenOptions.keyType.getParameters(),
                genOptions.keyType.getParameters(),
                "KeyType parameters should be equal");
        }
    }

    @step
    public function a_command_error_should_be_notified_of_type(errType:Dynamic) {
        assertThat(commands.capturedError, isA(errType));
    }
}
