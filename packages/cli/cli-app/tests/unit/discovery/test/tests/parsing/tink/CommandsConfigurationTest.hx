package discovery.test.tests.parsing.tink;

import discovery.bundle.ConfigureOptions;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps.ArgsParserCommonSteps;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class CommandsConfigurationTest implements Scenario{
    @steps
    var steps: CommandsConfigurationSteps;

    @Test
    @scenario
    public function filter_mechanisms_configuration(){
        given().a_cli_parser_and_commands();
        when().processing_args([
            'search', 'type', '-m', 'mdns', '--mechanism', 'dht']);
        then().the_configure_command_should_be_called_with_these_options({
            mechanisms: ['mdns', 'dht']
        });
    }


    @Test
    @scenario
    public function filter_keys_configuration(){
        given().a_cli_parser_and_commands();
        when().processing_args([
            'publish', 'srv', 'type', '1234', '--pki', 'mdns']);
        then().the_configure_command_should_be_called_with_these_options({
            pkis: ['mdns']
        });
    }
}

class CommandsConfigurationSteps
    extends ArgsParserCommonSteps<CommandsConfigurationSteps>
{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}