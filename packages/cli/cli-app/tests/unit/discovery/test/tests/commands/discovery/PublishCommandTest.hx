package discovery.test.tests.commands.discovery;

import discovery.test.tests.commands.discovery.steps.DiscoveryCommandsSteps;
import discovery.base.publication.PublicationDefault;
import discovery.base.publication.PublicationControl;
import discovery.domain.Description;
import discovery.domain.Publication;
import discovery.test.helpers.RandomGen;
import discovery.keys.KeyIdentifier;
import hxgiven.Scenario;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.mock;
import org.hamcrest.Matchers.*;

class PublishCommandTest implements Scenario{
    @steps
    var steps:PublishCommandSteps;

    @Test
    @scenario
    public function publish_service_with_given_id() {
        given().a_discovery_command();
        and().a_key_identifier_to_some_key_on_storage();
        when().publishing_a_service_using_that_key_identifier();
        then().the_service_publication_should_be_delegated_to_the_discovery();
    }

    //To-Do: test failures
}

class PublishCommandSteps extends DiscoveryCommandsSteps<PublishCommandSteps>{

    var keyIdentifier:KeyIdentifier;
    var expectedDescription:Description;

    public function new() {
        super();
    }


    //given -----------------------------------------------------------------
    @step
    public function a_key_identifier_to_some_key_on_storage() {
        keyIdentifier = RandomGen.hex(6);
    }


    //when -----------------------------------------------------------------
    @step
    public function publishing_a_service_using_that_key_identifier() {
        expectedDescription = {
            name: Random.string(10),
            type: Random.string(6),
            port: 12345,
            providerId: keyIdentifier
        };
        discoveryCommand.publish(expectedDescription);
    }

    //then -----------------------------------------------------------------

    @step
    public
    function the_service_publication_should_be_delegated_to_the_discovery() {
        discoveryMockup().verifyPublished(expectedDescription);
    }
}