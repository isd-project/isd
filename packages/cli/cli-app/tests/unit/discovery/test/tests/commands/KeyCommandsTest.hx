package discovery.test.tests.commands;

import discovery.keys.KeyTransformations;
import discovery.cli.impl.commands.KeysFormatter;
import haxe.Json;
import discovery.test.common.mockups.KeyMockup;
import discovery.keys.crypto.Fingerprint;
import discovery.test.mockups.ConfigurationMockup;
import discovery.cli.configuration.Configuration;
import discovery.keys.storage.KeyData;
import discovery.test.helpers.RandomGen;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import haxe.Exception;
import discovery.cli.impl.commands.DefaultKeyCommands;
import discovery.cli.commands.KeyCommands;
import hxgiven.Steps;
import hxgiven.Scenario;



class KeyCommandsTest implements Scenario{
    @steps
    var steps:KeyCommandsSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function redirect_output_to_file(){
        var output = 'path/to/some-file.txt';

        when().configuring_key_output_options_with({
            output: output
        });
        then().the_output_should_be_redirected_to(output);
    }

    @Test
    @scenario
    public function list_keys() {
        given().the_keychain_has_some_keys();
        when().listing_keys();
        then().keys_in_keychain_should_be_retrieved();
        and().the_keys_should_be_printed_to_the_console();
    }

    @Test
    @scenario
    public function list_keys_to_json(){
        given().the_keychain_has_some_keys();
        given().these_key_output_options({
            json: true
        });
        when().listing_keys();
        then().the_keys_should_be_printed_to_the_console_as_json();
    }

    @Test
    @scenario
    public function generate_key() {
        given().the_key_commands_was_created_and_configured();
        when().generating_a_key_with_these_options({
            keyType: EllipticCurve({
                namedCurve: "XYZ"
            })
        });
        and().the_key_generation_completes();
        then().a_key_should_be_generated_with_the_given_options();
        and().the_generated_key_should_be_printed_to_the_console();
    }

    @Test
    @scenario
    public function generate_key_to_json(){
        given().these_key_output_options({
            json: true
        });
        when().generating_a_key();
        then().the_generated_key_should_be_printed_as_json();
    }

    @Test
    @scenario
    public function start_importing_key(){
        given().the_key_commands_was_created_and_configured();
        given().a_valid_key_fingerprint_representation();
        when().importing_the_key_with_that_fingerprint();
        then().the_parsed_fingerprint_should_be_searched();
    }

    @Test
    @scenario
    public function notify_imported_key(){
        given().a_key_importing_was_started();
        when().the_pki_search_concludes();
        then().an_import_message_should_be_print_to_console();
    }

    @Test
    @scenario
    public function notify_import_key_failure(){
        given().a_key_importing_was_started();
        when().the_pki_search_fails();
        then().a_failure_message_should_be_print_to_console();
    }
}

class KeyCommandsSteps extends Steps<KeyCommandsSteps> {
    var keyCommands:KeyCommands;

    var configMockup:ConfigurationMockup;

    var cmdConfig:Configuration;
    var keyDataList:Array<KeyData>;

    var genKeyOptions:GenerateKeyOptions;
    var capturedException:Exception;

    var keyFingerprint:Fingerprint;
    var keyFingString:String;
    var keyMockup:KeyMockup;
    var keysFormatter = new KeysFormatter();

    var searchFailure:Exception;

    public function new() {
        super();

        keyDataList = [];

        configMockup = new ConfigurationMockup();
        cmdConfig = configMockup.configuration();

        keyMockup = new KeyMockup();

        keyCommands = new DefaultKeyCommands(cmdConfig);
    }

    // mock -------------------------------------------------

    function keychainMockup() {
        return configMockup.keychain();
    }

    function pkiMockup() {
        return configMockup.pki();
    }

    function consoleMockup() {
        return configMockup.console();
    }


    // given (summary) ---------------------------------------

    @step
    public function a_key_importing_was_started() {
        given().the_key_commands_was_created_and_configured();
        given().a_valid_key_fingerprint_representation();
        when().importing_the_key_with_that_fingerprint();
    }

    // given -------------------------------------------------

    @step
    public function the_key_commands_was_created_and_configured()
    {}

    @step
    public function these_key_output_options(outputOpts:OutputKeysOptions) {
        when().configuring_key_output_options_with(outputOpts);
    }

    @step
    public function the_keychain_has_some_keys() {
        for (i in 0...5){
            keyDataList.push(RandomGen.crypto.keyData());
        }

        configMockup.keychain().setupListKeys(keyDataList);
    }


    @step
    public function a_valid_key_fingerprint_representation() {
        keyFingerprint = RandomGen.crypto.fingerprint();
        keyFingString = keyFingerprint.toString();
    }

    // when (summary) -------------------------------------------------

    @step
    public function generating_a_key() {
        when().generating_a_key_with_these_options({
            keyType: EllipticCurve()
        });
        when().the_key_generation_completes();
    }

    // when -------------------------------------------------

    @step
    public function
        configuring_key_output_options_with(outOpt:OutputKeysOptions)
    {
        keyCommands.configureKeysOutput(outOpt);
    }

    @step
    public function listing_keys() {
        captureException(()->{
            keyCommands.listKeys();
        });
    }

    @step
    public function
        generating_a_key_with_these_options(options:GenerateKeyOptions)
    {
        genKeyOptions = options;
        keyCommands.generateKeys(genKeyOptions);
    }

    @step
    public function importing_the_key_with_that_fingerprint() {
        keyCommands.importKey(keyFingString);
    }

    @step
    public function the_key_generation_completes() {
        keychainMockup().resolveGenKeyPromise();
    }

    @step
    public function the_pki_search_concludes() {
        keyMockup.setKeyFingerprintTo(keyFingerprint);
        pkiMockup().resolveSearchKeyTo(keyMockup.key);
    }

    @step
    public function the_pki_search_fails() {
        searchFailure = new Exception("failure reason");

        pkiMockup().rejectSearchKeyWith(searchFailure);
    }

    // then -------------------------------------------------

    @step
    public function keys_in_keychain_should_be_retrieved() {
        keychainMockup().verifyListKeysCalled();
    }


    @step
    public function the_keys_should_be_printed_to_the_console() {
        //TO-DO: improve verification
        configMockup.console().verifyPrintlnCalled(atLeast(keyDataList.length));
    }

    @step
    public function the_keys_should_be_printed_to_the_console_as_json() {
        consoleMockup().verifyPrintedLinesContains(
            keysFormatter.keysToJson(keyDataList)
        );
    }

    @step
    public function the_output_should_be_redirected_to(output:String) {
        configMockup.verifyRedirectOutputToFile(output);
    }

    function captureException(code:() -> Void) {
        try{
            code();
        }
        catch(e: Exception){
            capturedException = e;
        }
    }

    @step
    public function a_key_should_be_generated_with_the_given_options() {
        keychainMockup().verifyGenerateKeyWith(genKeyOptions);
    }

    @step
    public function the_generated_key_should_be_printed_to_the_console() {
        var keyData = keychainMockup().lastGeneratedKey().keyData();
        consoleMockup().verifyPrintedLinesContains(
            keysFormatter.keyToString(keyData)
        );
    }

    @step
    public function the_generated_key_should_be_printed_as_json() {
        var keyData = keychainMockup().lastGeneratedKey().keyData();

        consoleMockup().verifyPrintedLinesContains(
            keysFormatter.keyToJson(keyData)
        );
    }

    @step
    public function the_parsed_fingerprint_should_be_searched() {
        pkiMockup().checkSearchKey(keyFingerprint);
    }

    @step
    public function an_import_message_should_be_print_to_console() {
        consoleMockup().verifyPrintedLinesContains(keyFingerprint.toString());
    }

    @step
    public function a_failure_message_should_be_print_to_console() {
        consoleMockup().verifyPrintedErrorsContains(keyFingString);
        consoleMockup().verifyPrintedErrorsContains(searchFailure.toString());
    }
}