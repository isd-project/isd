package discovery.test.tests.commands.discovery.steps;

import discovery.test.mockups.ConfigurationMockup;
import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.bundle.ConfigureOptions;
import discovery.test.generator.CliRandomGenerator;
import discovery.bundle.keychain.KeychainBuilder;
import discovery.keys.Keychain;
import discovery.cli.console.Console;
import discovery.cli.impl.commands.DiscoveryCommands;
import hxgiven.Steps;

import mockatoo.Mockatoo;
import mockatoo.Mockatoo.mock;

class DiscoveryCommandsSteps<Sub:DiscoveryCommandsSteps<Any>>
    extends Steps<Sub>
{
    var discoveryCommand:DiscoveryCommands;

    var configs:ConfigureOptions;
    var keyConfigs:ConfigureKeysOptions;

    var configuration:ConfigurationMockup;
    var generator:CliRandomGenerator;

    public function new() {
        super();

        configuration = new ConfigurationMockup();
        generator = new CliRandomGenerator();
    }

    public function discoveryMockup() {
        return configuration.discovery();
    }

    public function idParserMockup() {
        return configuration.idParser();
    }

    public function consoleMockup() {
        return configuration.console();
    }

    //given -----------------------------------------------------------------
    @step
    public function a_discovery_command() {
        discoveryCommand = new DiscoveryCommands(configuration.configuration());
    }

    //when -----------------------------------------------------------------


    //then -----------------------------------------------------------------
}