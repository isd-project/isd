package discovery.test.tests.console;

import discovery.test.helpers.RandomGen;
import discovery.cli.console.RedirectConsole;
import discovery.test.mockups.OutputIoMockup;
import discovery.test.mockups.ConsoleMockup;
import hxgiven.Steps;
import hxgiven.Scenario;

import org.hamcrest.Matchers.*;


class RedirectConsoleTest implements Scenario{
    @steps
    var steps: RedirectConsoleSteps;

    @Test
    @scenario
    public function redirect_console_output(){
        given().an_redirect_console_to_an_io_output();
        when().printing_a_line();
        then().it_should_be_sent_to_the_output();
    }

    @Test
    @scenario
    public function delegate_print_error(){
        given().an_redirect_console_to_an_io_output();
        when().printing_an_error_message();
        then().the_message_should_be_delegated();
    }


    @Test
    @scenario
    public function delegate_readline(){
        given().an_redirect_console_to_an_io_output();
        when().executing_readline();
        then().the_readline_should_be_delegated();
    }
}

class RedirectConsoleSteps extends Steps<RedirectConsoleSteps>{

    var delegate = new ConsoleMockup();
    var output = new OutputIoMockup();

    var redirect:RedirectConsole;
    var expectedText:String;

    public function new(){
        super();
    }

    // given ---------------------------------------------------

    @step
    public function an_redirect_console_to_an_io_output() {
        redirect = new RedirectConsole(output.output(), delegate.console());
    }

    function some_text() {
        expectedText = RandomGen.primitives.name();
    }

    // when  ---------------------------------------------------

    @step
    public function executing_readline() {
        redirect.readline();
    }

    @step
    public function printing_a_line() {
        given().some_text();

        redirect.println(expectedText);
    }

    @step
    public function printing_an_error_message() {
        given().some_text();

        redirect.printError(expectedText);
    }

    // then  ---------------------------------------------------

    @step
    public function it_should_be_sent_to_the_output() {
        this.output.verifyWritedString(expectedText);
    }

    @step
    public function the_message_should_be_delegated() {
        delegate.verifyPrintedErrorsContains(expectedText);
    }

    @step
    public function the_readline_should_be_delegated() {
        delegate.verifyReadlineCalled();
    }
}