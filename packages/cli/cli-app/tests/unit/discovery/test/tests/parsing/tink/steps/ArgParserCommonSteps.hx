package discovery.test.tests.parsing.tink.steps;


import discovery.bundle.ConfigureOptions;
import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.test.fakes.FakeCliCommands;
import discovery.cli.impl.parsing.tink.TinkArgsParser;
import massive.munit.Assert;
import hxgiven.Steps;
import discovery.cli.ArgsParser;

import org.hamcrest.Matchers.*;
using discovery.test.matchers.CommonMatchers;


class ArgsParserCommonSteps<T:ArgsParserCommonSteps<T>> extends Steps<T> {
    var parser:ArgsParser;
    var commands: FakeCliCommands;

    public function new() {
        super();
        parser = new TinkArgsParser();
        commands = new FakeCliCommands();
    }

    // given ----------------------------------------------------------------

    @step
    public function a_cli_parser() {}

    @step
    public function the_cli_commands() {}

    @step
    public function a_cli_parser_and_commands() {
        given().a_cli_parser();
        and().the_cli_commands();
    }

    // when ----------------------------------------------------------------

    @step
    public function processing_args(args: Array<String>) {
        parser.processArgs(args, commands);
    }


    // then ----------------------------------------------------------------

    @step
    public function last_command_is(cmd: String){
        assertThat(this.commands, is(notNullValue()), "Commands instance invalid!");
        assertThat(commands.command, is(equalTo(cmd)), 'Last command should be $cmd, but was: ${commands.command}');
    }

    @step
    public function last_command_should_be(cmd: String, args: Array<Dynamic>){
        this.then().last_command_is(cmd);

        for(i in 0...args.length){
            var expectedArg = args[i];
            var arg = commands.args[i];

            var errMsg = 'Argument ${i}: ${Std.string(arg)} does not match ${Std.string(expectedArg)}';

            if(expectedArg != null && arg != null){
                Assert.areEqual(expectedArg, arg, errMsg);
            }
            else{
                assertThat(arg, is(equalTo(expectedArg)), errMsg);
            }
        }
    }

    @step
    public function the_configure_command_should_be_called_with_default() {
        then().the_configure_command_should_be_called_with_these_key_options({});
    }

    @step
    public function the_configure_command_should_be_called_with_these_key_options(expected:ConfigureKeysOptions)
    {
        var configOptions:ConfigureOptions = requireConfigureOptions();
        configOptions.shouldNotBeNull();
        configOptions.keys.shouldBeEqualsTo(expected, "Configure key options");
    }


    @step
    public function
        the_configure_command_should_be_called_with_these_options(
            expected: ConfigureOptions)
    {
        var options = requireConfigureOptions();
        options.shouldBeEqualsTo(expected);
    }

    function requireConfigureOptions(): ConfigureOptions {
        var command = checkCommand("configure");
        assertThat(command, is(notNullValue()), "command should not be null");
        assertThat(command.args, hasSize(greaterThan(0)));

        return command.args[0];
    }

    public function checkCommand(cmdName:String) {
        for(cmd in commands.commands()){
            if(cmd != null && cmd.command == cmdName){
                return cmd;
            }
        }

        var cmdNames = commands.commands().map((cmd)-> cmd.command);

        Assert.fail('Command $cmdName was not executed. Instead, these commands were executed: ${cmdNames}' );

        return null;
    }

    public function configureOptionsShouldBe(
        config:ConfigureKeysOptions, expectedConfig:ConfigureKeysOptions)
    {
        if(expectedConfig != null){
            assertThat(config, is(notNullValue()),
                "Key configuration should not be null");
            Assert.areEqual(expectedConfig, config,
                'Keys configuration should be: $expectedConfig\n' +
                'but was: $config');
        }
    }
}