package discovery.test.tests.commands.discovery;

import discovery.cli.formatting.DiscoveredFormatter;
import discovery.domain.Discovered;
import discovery.format.exceptions.InvalidFormatException;
import discovery.test.tests.commands.discovery.steps.DiscoveryCommandsSteps;
import discovery.domain.IdentifierUrl;
import discovery.test.helpers.RandomGen;
import hxgiven.Scenario;


class LocateCommandTest implements Scenario{
    @steps
    var steps:LocateCommandSteps;

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function start_locating_service_from_given_url(){
        given().a_discovery_command();
        and().some_valid_service_url();
        when().executing_the_locate_command_with_that_url();
        then().the_url_should_be_parsed();
        and().the_search_for_that_service_should_start();
    }

    @Test
    @scenario
    public function print_located_service(){
        given().the_location_for_some_service_started();
        when().a_service_is_located();
        then().it_should_be_print_to_console();
    }

    @Test
    @scenario
    public function handle_parse_exceptions(){
        given().a_discovery_command();
        and().a_url_with_invalid_format();
        when().executing_the_locate_command_with_that_url();
        then().the_url_should_be_parsed();
        then().an_error_should_be_print_to_console();
        and().the_service_location_should_not_be_started();
    }
}

class LocateCommandSteps extends DiscoveryCommandsSteps<LocateCommandSteps>{
    var srvUrl:String;
    var identifierUrl:IdentifierUrl;

    var locatedService:Discovered;

    public function new() {
        super();
    }

    //given -----------------------------------------------------------------

    @step
    public function some_valid_service_url(){
        given().some_service_url();
        given().this_url_parses_to_a_valid_identifier();
    }

    @step
    public function some_service_url() {
        srvUrl = "srv://myservice.http.CwG0uraRvSPEYdK5POdEE3lDO9MJWOz4Ezld5KDuVJU";
    }

    @step
    function this_url_parses_to_a_valid_identifier() {
        identifierUrl = makeIdentifierUrl();

        idParserMockup().forThisUrlParseShouldReturn(srvUrl, identifierUrl);
    }

    @step
    public function a_url_with_invalid_format() {
        given().some_service_url();

        idParserMockup().nextParseShouldFailWith(
            new InvalidFormatException('invalid format exception'));
    }

    @step
    public function the_location_for_some_service_started() {
        given().a_discovery_command();
        given().some_valid_service_url();
        when().executing_the_locate_command_with_that_url();
    }


    //when -----------------------------------------------------------------
    @step
    public function executing_the_locate_command_with_that_url() {
        discoveryCommand.locate(srvUrl);
    }

    @step
    public function a_service_is_located() {
        locatedService = RandomGen.discovered();
        discoveryMockup().lastSearchMockup().notifyDiscovered(locatedService);
    }

    //then -----------------------------------------------------------------
    @step
    public function the_url_should_be_parsed() {
        idParserMockup().verifyUrlParsed(this.srvUrl);
    }

    @step
    public function the_search_for_that_service_should_start() {
        discoveryMockup().verifyLocateCalledWith(identifierUrl);
    }

    @step
    public function the_service_location_should_not_be_started() {
        discoveryMockup().verifyLocate(never);
    }

    @step
    public function it_should_be_print_to_console() {
        var formatter = new DiscoveredFormatter();
        var expectedOutput = formatter.formatDiscovered(locatedService);

        consoleMockup().verifyPrintlnCalledWith(expectedOutput);
    }

    @step
    public function an_error_should_be_print_to_console() {
        consoleMockup().verifyAnErrorWasPrinted();
    }

    // -----------------------------------------------------------------------

    function makeIdentifierUrl():IdentifierUrl {
        return {
            name: "name",
            type: "http",
            providerId: RandomGen.binary.bytes(32)
        };
    }
}