package discovery.test.tests.parsing.tink;

import discovery.domain.Description;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps;
import discovery.domain.Query;
import hxgiven.Scenario;


class DiscoveryCommandsParsingTest implements Scenario{
    @steps
    var steps:DiscoveryCommandsParsingSteps;

    public function new()
    {}

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function publish_service_with_given_identifier() {
        given().a_cli_parser_and_commands();
        when().processing_args(["publish", "my server", "http", "3000",
                                "--id", "edfd9b5e3"]);
        then().the_publish_command_should_be_called_with({
            name: "my server",
            type: "http",
            port: 3000,
            providerId: "edfd9b5e3"
        });
    }

    @Test
    @scenario
    public function publish_with_unspecified_key() {
        given().a_cli_parser_and_commands();
        when().processing_args(["publish", "my server", "http", "3000"]);
        then().the_publish_command_should_be_called_with({
            name: "my server",
            type: "http",
            port: 3000,
            providerId: null
        });
    }

    @Test
    @scenario
    public function search_service_by_type() {
        given().a_cli_parser_and_commands();
        when().processing_args(["search", "http"]);
        then().the_search_command_should_be_called_with({
            type: "http"
        });
    }

    @Test
    @scenario
    public function locate_service_by_url() {
        var srvUrl = "srv://myservice.http.CwG0uraRvSPEYdK5POdEE3lDO9MJWOz4Ezld5KDuVJU";

        given().a_cli_parser_and_commands();
        when().processing_args(["locate", srvUrl]);
        then().the_locate_command_should_be_called_with(srvUrl);
    }

    // configure ---------------------------------------------------------

    @Test
    @scenario
    public function should_configure_before_publish_command(){
        given().a_cli_parser_and_commands();
        when().processing_args(["publish", "my server", "http", "3000",
                                "--keysdir", "exampledir"]);
        then().the_configure_command_should_be_called_with_these_key_options({
            keysdir:"exampledir"
        });
    }

    @Test
    @scenario
    public function should_configure_before_search_command(){
        given().a_cli_parser_and_commands();
        when().processing_args(["search", "http",
                                "--keysdir", "exampledir"]);
        then().the_configure_command_should_be_called_with_these_key_options({
            keysdir:"exampledir"
        });
    }
}

class DiscoveryCommandsParsingSteps
    extends ArgsParserCommonSteps<DiscoveryCommandsParsingSteps>
{

    // given ----------------------------------------------------------------

    // when ----------------------------------------------------------------


    // then ----------------------------------------------------------------

    @step
    public function the_publish_command_should_be_called_with(arg: Description)
    {
        this.then().last_command_should_be("publish", [arg]);
    }

    @step
    public function the_search_command_should_be_called_with(arg: Query) {
        this.then().last_command_should_be("search", [arg]);
    }

    @step
    public function the_locate_command_should_be_called_with(
        identifierUrl: String)
    {
        then().last_command_should_be("locate", [identifierUrl]);
    }
}