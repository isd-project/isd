package discovery.test.tests.parsing.tink;

import discovery.bundle.ConfigureOptions;
import discovery.cli.impl.parsing.tink.cli.ConfigureExtractor;
import discovery.cli.impl.parsing.tink.cli.CliWithConfigurations;
import hxgiven.Steps;
import hxgiven.Scenario;

using discovery.test.matchers.CommonMatchers;


class ConfigureExtractorTest implements Scenario{
    @steps
    var steps: ConfigureExtractorSteps;

    @Test
    @scenario
    public function extract_keys_dir(){
        given().this_configurable_instance(
            new ConfigurableCliExample('mykeysdir'));
        when().extracting_the_configuration_options();
        then().these_options_should_be_produced({
            keys: {
                keysdir: 'mykeysdir'
            }
        });
    }


    @Test
    @scenario
    public function extract_mechanisms(){
        var config:ConfigurableCliExample = {
            mechanisms: ['m1', 'm2', 'm3']
        };

        given().this_configurable_instance(config);
        when().extracting_the_configuration_options();
        then().these_options_should_be_produced({
            mechanisms: config.mechanisms
        });
    }
}

class ConfigureExtractorSteps extends Steps<ConfigureExtractorSteps>{
    var extractor:ConfigureExtractor;
    var configurable:CliWithConfigurations;
    var extracted:ConfigureOptions;

    public function new(){
        super();

        extractor = new ConfigureExtractor();
    }

    // given ---------------------------------------------------

    @step
    public function this_configurable_instance(cli:CliWithConfigurations) {
        this.configurable = cli;
    }

    // when  ---------------------------------------------------

    @step
    public function extracting_the_configuration_options() {
        extracted = extractor.extract(configurable);
    }

    // then  ---------------------------------------------------

    @step
    public function these_options_should_be_produced(expected:ConfigureOptions)
    {
        extracted.shouldBeEqualsTo(expected);
    }
}

@:structInit
private class ConfigurableCliExample implements CliWithConfigurations{
    public var keysdir:String;

    public var mechanisms:Array<String>;

    public var pkis:Array<String>=null;

    public function new(?keysdir:String, ?mechanisms:Array<String>) {
        this.keysdir = keysdir;
        this.mechanisms = mechanisms;
    }
}