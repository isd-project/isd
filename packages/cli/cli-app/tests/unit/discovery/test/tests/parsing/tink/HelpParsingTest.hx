package discovery.test.tests.parsing.tink;

import massive.munit.Assert;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps;
import hxgiven.Scenario;


import org.hamcrest.Matchers.*;


class HelpParsingTest implements Scenario{
    @steps
    var steps:HelpParsingSteps;

    public function new()
    {}

    @Before
    public function setup() {
        resetSteps();
    }

    @Test
    @scenario
    public function test_help_main() {
        given().a_cli_parser();
        and().the_cli_commands();
        when().processing_args(["--help"]);
        then().the_help_command_should_be_called_with_the_help_text();
    }

    @Test
    @scenario
    public function keys_help(){
        given().a_cli_parser_and_commands();
        when().processing_args(["keys", "--help"]);
        then().the_help_command_should_be_called_for("keys");
    }
}

class HelpParsingSteps extends ArgsParserCommonSteps<HelpParsingSteps> {

    // given ----------------------------------------------------------------

    // when ----------------------------------------------------------------


    // then ----------------------------------------------------------------

    @step
    public function the_help_command_should_be_called_with_the_help_text() {
        then().the_help_command_should_be_called_for("");
    }

    @step
    public function the_help_command_should_be_called_for(subcommand:String)
    {
        this.then().last_command_is("help");
        assertThat(commands.args[0], is(notNullValue()));

        var text:String = commands.args[0];

        var usageRegex = new EReg('Usage: .*${subcommand}.*$', 'm');

        Assert.doesMatch(text, usageRegex);

        assertThat(text, containsString("Usage"));
        assertThat(text, containsString("--help"));
    }
}