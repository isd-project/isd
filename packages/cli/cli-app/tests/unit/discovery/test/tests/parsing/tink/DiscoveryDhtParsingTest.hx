package discovery.test.tests.parsing.tink;

import discovery.dht.dhttypes.PeerAddress;
import discovery.test.tests.parsing.tink.steps.ArgParserCommonSteps.ArgsParserCommonSteps;
import hxgiven.Scenario;


class DiscoveryDhtParsingTest implements Scenario{
    @steps
    var steps: DiscoveryDhtParsingSteps;

    @Test
    @scenario
    public function publish_with_bootstrap_address(){
        given().a_cli_parser_and_commands();
        when().processing_args([
            "publish", "server", "type", "1234",
            "--bootstrap", "123.456.789.00:1000"]);
        then().the_configure_command_should_be_called_with_these_options({
            dht: {
                bootstrap: toAddresses(["123.456.789.00:1000"])
            }
        });
    }

    function toAddresses(strAddresses:Array<String>):Array<PeerAddress> {
        return strAddresses.map((a)->PeerAddress.fromString(a));
    }
}

class DiscoveryDhtParsingSteps
    extends ArgsParserCommonSteps<DiscoveryDhtParsingSteps>
{
    public function new(){
        super();
    }

    // given ---------------------------------------------------

    // when  ---------------------------------------------------

    // then  ---------------------------------------------------

}