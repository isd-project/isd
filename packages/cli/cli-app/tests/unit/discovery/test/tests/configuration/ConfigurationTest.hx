package discovery.test.tests.configuration;

import discovery.keys.PKI;
import discovery.test.mockups.ConfigurationDependenciesMockup;
import discovery.test.generator.CliRandomGenerator;
import discovery.bundle.ConfigureOptions;
import discovery.cli.configuration.Configuration;
import hxgiven.Scenario;
import hxgiven.Steps;

using discovery.test.matchers.CommonMatchers;


class ConfigurationTest implements Scenario{
    @steps
    var steps: ConfigurationSteps;

    @Test
    @scenario
    public function should_delegate_key_configurations_when_configuring(){
        given().a_configuration_instance();
        given().some_keys_configuration_option();
        when().configuring_it();
        then().the_underlying_keys_factory_should_be_configured();
    }

    @Test
    @scenario
    public function should_not_build_discovery_if_not_solicited(){
        given().a_configuration_instance();
        when().configuring_it();
        but().not_requesting_discovery();
        then().the_discovery_instance_should_not_be_built();
    }

    @Test
    @scenario
    public function get_pki_from_discovery(){
        given().a_configuration_instance();
        when().getting_the_pki();
        then().it_should_return_the_discovery_pki();
    }
}

class ConfigurationSteps extends Steps<ConfigurationSteps>{
    var config:Configuration;
    var configOptions:ConfigureOptions;

    var generator:CliRandomGenerator;
    var dependencies:ConfigurationDependenciesMockup;

    var pki:PKI;

    public function new(){
        super();

        generator = new CliRandomGenerator();
        dependencies = new ConfigurationDependenciesMockup();

        configOptions = {};
    }


    // given -----------------------------------------------------------------

    @step
    public function a_configuration_instance() {
        config = new Configuration(dependencies.dependencies());
    }

    @step
    public function some_keys_configuration_option() {
        configOptions = {
            keys: generator.configKeyOptions()
        };
    }

    // when ------------------------------------------------------------------

    @step
    public function configuring_it() {
        config.configure(configOptions);
    }

    @step
    public function not_requesting_discovery() {}



    @step
    public function getting_the_pki() {
        pki = config.pki();
    }

    // then ------------------------------------------------------------------

    @step
    public function the_discovery_instance_should_not_be_built() {
        dependencies.discoveryBuilder().verifyNeverBuilt();
    }

    @step
    public function the_underlying_keys_factory_should_be_configured() {
        dependencies.keysFactory().verifyConfiguredWith(configOptions.keys);
    }

    @step
    public function it_should_return_the_discovery_pki() {
        pki.shouldBeTheSame(config.discovery().pki());
    }
}