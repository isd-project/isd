package discovery.test.tests.commands.discovery;

import discovery.test.tests.commands.discovery.steps.DiscoveryCommandsSteps;
import hxgiven.Scenario;

using discovery.utils.EqualsComparator;


class ConfigureCommandTest implements Scenario{
    @steps
    var steps:ConfigureCommandSteps;

    @Test
    @scenario
    public function configure_commands(){
        given().a_discovery_command();
        given().some_keys_configuration();
        when().configuring_the_commands();
        then().the_configuration_should_be_delegated();
    }
}

class ConfigureCommandSteps
    extends DiscoveryCommandsSteps<ConfigureCommandSteps>
{
    public function new() {
        super();
    }


    //given -----------------------------------------------------------------
    @step
    public function some_keys_configuration() {
        configs = generator.configOptions();

        keyConfigs = configs.keys;
    }

    //when -----------------------------------------------------------------
    @step
    public function configuring_the_commands() {
        discoveryCommand.configure(configs);
    }

    //then -----------------------------------------------------------------

    @step
	public function the_configuration_should_be_delegated() {
        configuration.verifyConfiguredWith(configs);
    }
}