*** Settings ***
Documentation     Testing key distribution
Resource          ../cli.resource
Resource          ../keys.resource
Resource          ../discovery.resource
Library            OperatingSystem

*** Test Cases ***
Discovery key when locating service
    [Tags]    unstable
    #
    Given an authenticated service was published
    Given a client that does not know the publisher key
    When the client searches for that service
    Then the service should have been discovered and verified

Locate service with known key
    [Tags]    unstable
    #
    Given an authenticated service was published
    Given the service key is known by the client
    When the client searches for that service
    Then the service should have been discovered and verified

*** Variables ***
${OTHER_KEYS_DIR}    ${TEMP_DIR}${/}otherkeys
${srv_name}    my service
${srv_type}    exampletype
${srv_port}    1234

*** Settings ***
Test Setup       Setup
Test Teardown    Teardown

*** Keywords ***
Setup
    No Operation

Teardown
    close all clis
    clear keys directory


*** Keywords ***
an authenticated service was published
  Given there is an identity with some fingerprint
  Given a service was published with    ${srv_name}  ${srv_type}  ${srv_port}
  ...                                   --id  ${fingerprint}

a client that does not know the publisher key
    clear keys directory

the service key is known by the client
    Copy File     ${KEYS_DIR}${/}keys.json    ${OTHER_KEYS_DIR}${/}cache.json

the client searches for that service
    Locate service     ${srv_name}  ${srv_type}  ${fingerprint}
    ...                --keysdir    ${OTHER_KEYS_DIR}

the service should have been discovered and verified
  Then this service should be found       name=${srv_name}
  ...                                     type=${srv_type}  port=${srv_port}

*** Keywords ***
clear keys directory
    Remove Directory    ${OTHER_KEYS_DIR}    recursive=${TRUE}