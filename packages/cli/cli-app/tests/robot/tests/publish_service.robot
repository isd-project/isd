*** Settings ***
Documentation     Testing publication of services using cli
Resource          ../cli.resource
Resource          ../keys.resource
Resource          ../discovery.resource
Library            OperatingSystem

*** Variables ***
${WAIT_TIME}    200ms


*** Settings ***
Test Teardown    close all clis


*** Test Cases ***
Publish command
   # [Tags]    debug
   #
   Given there is a default key set up
   When starting a publication of a service with     my service    http    12345
   Then the publication of the service should be notified
   After some time
   And the service should still be running

Finish publish command
   #
   Given a service was published
   When sending line                    ${EMPTY}    cli=publisher
   And waiting for the cli to finish                cli=publisher
   Then the cli should terminate                    cli=publisher


Publish service with specified key
   #
   Given there is an identity with some fingerprint
   Log File    ${KEYS_DIR}${/}keys.json
   When a service is published with     service name    http  8080
   ...                                      --id     ${fingerprint}
   Then the service should be published with the given identity


*** Keywords ***
a service was published
   [Arguments]            ${cli}=publisher
   Given a service was published with    service  type  1234  cli=${cli}

After some time
   Sleep    ${WAIT_TIME}

the service should still be running
   Then the cli should not have terminated    cli=publisher


the service should be published with the given identity
    Not implemented yet