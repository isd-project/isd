*** Settings ***
Documentation     Testing search published services using cli.
Resource          ../cli.resource
Resource          ../keys.resource
Resource          ../discovery.resource

*** Variables ***
${srv_name}    my service
${srv_type}    exampletype
${srv_port}    1234

*** Settings ***
Test Teardown    close all clis


*** Test Cases ***
Searching service by type
  Given a service was published with      ${srv_name}  ${srv_type}  ${srv_port}
  When a client search for services matching  type=${srv_type}
  Then this service should be found           name=${srv_name}  type=${srv_type}  port=${srv_port}

Finish search command
  Given a client search is running
  When sending line                       ${EMPTY}    cli=client
  And waiting for the cli to finish                   cli=client
  Then the client cli should terminate

Locate service with identity
  [Tags]    unstable
  #
  Given there is an identity with some fingerprint
  Given a service was published with      ${srv_name}  ${srv_type}  ${srv_port}
  ...                                     --id  ${fingerprint}
  When a client try to locate a service identified by
  ...    srv://${srv_name}.${srv_type}.${fingerprint};fmt\=b16
  Then this service should be found       name=${srv_name}  type=${srv_type}  port=${srv_port}


*** Keywords ***

a client search is running
  When a client search for services matching    type=any

the client cli should terminate
  Then the cli should terminate    cli=client