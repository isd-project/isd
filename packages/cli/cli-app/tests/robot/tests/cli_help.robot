*** Settings ***
Documentation     Testing cli help command.
Library           OperatingSystem
Library           Process
Resource          ../cli.resource

*** Test Cases ***
Help command
    ${result} =       Running cli with    --help
    Result should contain help text       ${result.stdout}

Default command
    ${result} =       Running cli with no arguments
    Result should contain help text       ${result.stdout}

*** Keywords ***
Running cli with
    [Arguments]    @{args}
    ${result}      Run Process    node    ${CLI_SCRIPT}    @{args}
    [Return]       ${result}

Running cli with no arguments
    ${result} =    Running cli with
    [Return]       ${result}

Result should contain help text
    [Arguments]    ${resultText}
    Should Contain    ${resultText}    Usage: ${CLI_FILENAME}
    Should Contain    ${resultText}    --help
