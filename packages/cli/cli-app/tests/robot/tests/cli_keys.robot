*** Settings ***
Documentation     Testing cli keys management commands.
Resource          ../cli.resource
Resource          ../keys.resource
Library           OperatingSystem
Library           String

*** Test Cases ***
List keys empty
    Given there is no keys in the keychain
    When listing the keys
    Then no keys should be displayed

Listing existent keys
    Given there is an identity with some fingerprint
    When listing the keys
    Then the key with that fingerprint should be listed    ${fingerprint}

Generate key
    Given there is no keys in the keychain
    When generating a key with some algorithm
    Then the key storage should not be empty anymore

Generate and list key
    Given there is no keys in the keychain
    When generating a key with some algorithm
    And listing the keys
    Then a key with matching algorithm should be displayed

Generating and listing multiple key
    Given there is no keys in the keychain
    When generating a key with     --keytype    rsa
    And generating a key with      --keytype    rsa    --len 2048
    And generating a key with      --keytype    ec
    And listing the keys
    Then the number of listed keys should be    3

*** Variables ***
${output}    ${EMPTY}


*** Settings ***
Test Setup       Setup test
Test Teardown    Remove keys directory


*** Keywords ***
Setup test
    Create Directory        ${KEYS_DIR}
    Set Suite Variable      ${output}    ${EMPTY}


Remove keys directory
    Empty Directory     ${KEYS_DIR}
    Remove Directory    ${KEYS_DIR}


# given -----------------------

there is no keys in the keychain
    Create Directory    ${KEYS_DIR}
    Empty Directory     ${KEYS_DIR}

# when -----------------------

listing the keys
    When listing the keys with    --keysdir    ${KEYS_DIR}

listing the keys with
    [Arguments]    @{args}
    ${out}=  When run cli with    keys        @{args}
    ${out}=  Convert to String    ${out}
    Set Suite Variable            ${output}   ${out}
    Log                           ${output}

generating a key with
    [Arguments]    @{options}
    ${gen_out}=  When run cli with  keys  gen  --keysdir  ${KEYS_DIR}    @{options}
    Log        ${gen_out}    level=DEBUG

generating a key with some algorithm
    When generating a key with  --keytype  rsa  --len  1024

# then -----------------------

no keys should be displayed
    Log to console                ${output}
    Should Be Equal As Strings    ${output}    ${EMPTY}

the key storage should not be empty anymore
    Directory should not be empty    ${KEYS_DIR}    msg=Key storage directory should not be empty anymore

a key with matching algorithm should be displayed
    Should not be equal    ${output}    ${EMPTY}    msg=Output should not be empty

the key with that fingerprint should be listed
    [Arguments]    ${key_fingerprint}
    ${numlines} =  Get number of lines containing string   ${output}
    ...                                                    ${key_fingerprint}
    #
    Log    fingerprint: ${key_fingerprint}
    Log    ${output}
    #
    Should Be Equal As Integers        ${numlines}    ${1}


the number of listed keys should be
    [Arguments]    ${numkeys}
    ${numlines} =  Get number of lines containing string    ${output}    key:
    Should Be Equal As Integers        ${numlines}    ${numkeys}
    ...                  msg="Expected ${numkeys} keys, but found ${numlines}"

Get number of lines containing string
    [Arguments]    ${input}    ${str_to_match}
    ${lines} =     Get Lines Containing String    ${output}    ${str_to_match}
    ${numlines} =  Get Line Count                 ${lines}
    [Return]    ${numlines}