import pexpect

import sys
import logging


class Cli(object):
    DEFAULT_NAME = 'default'

    def __init__(self, cli_script):
        self._cli_script = cli_script
        self._cli_processes = {}

    def starting_cli_with(self, *args, **kwargs):
        logging.info("starting cli with {} {}".format(args, kwargs))

        kwargs   = self._common_run_args(**kwargs)
        cmd_args = self._make_cmd_args(*args)

        cli = pexpect.spawn('node', cmd_args, logfile=sys.stdout, encoding='utf-8')
        self._store_process(cli, **kwargs)

        return cli


    def run_cli_with(self, *args, **kwargs):
        kwargs   = self._common_run_args(**kwargs)
        cmd_args = self._make_cmd_args(*args)

        cmd = 'node ' + ' '.join(cmd_args)

        return pexpect.run(cmd,**kwargs)

    def _common_run_args(self, **kwargs):
        kwargs['encoding'] = kwargs.get('encoding', 'utf-8')
        return kwargs


    def wait_for_messages(self, *msgs, **kwargs):
        return [ self.wait_for_message(m, **kwargs) for m in msgs ]


    def wait_for_message(self, msg, **kwargs):
        return self.wait_for_one_of_messages(msg, **kwargs)


    def wait_for_one_of_messages(self, *msgs, **kwargs):
        try:
            return self._expect_message(*msgs, **kwargs)
        except pexpect.ExceptionPexpect as e:
            self._on_wait_message_error(e, msgs, **kwargs)

    def _expect_message(self, *msgs, **kwargs):
        process  = self._require_cli(**kwargs)

        msg_idx = process.expect(list(msgs), timeout=kwargs.get('timeout', -1))

        found_msg = msgs[msg_idx]

        return MatchedMessage(found_msg, process.match)

    def _on_wait_message_error(self, error, msg, **kwargs):
        process, cli_name = self._require_cli_and_name(**kwargs)

        err_msg = None
        if isinstance(error, pexpect.EOF):
            err_msg = 'cli exited before finding message'
        elif isinstance(error, pexpect.TIMEOUT):
            err_msg = 'cli timed out before finding message'
        else: #unknown exception, so just re-raise
            raise error

        except_msg = '"{0}" {1}: {2}'.format(cli_name, err_msg, msg)
        except_msg += '\n\tbuffer:{0}'.format(process.buffer)
        except_msg += '\n\tbefore:{0}'.format(process.before)

        raise RuntimeError(except_msg) from error


    def wait_until_cli_finishes(self, **kwargs):
        self.wait_for_message(pexpect.EOF, **kwargs)

    def sending_line(self, msg, **kwargs):
        cli = self._require_cli(**kwargs)

        cli.sendline(msg)

    def cli_has_terminated(self, **kwargs):
        cli = self._require_cli(**kwargs)

        return cli.isalive() == False

    def close_cli(self, **kwargs):
        cli = self._get_cli(**kwargs)
        self._terminate(cli)

    def close_all_clis(self):
        for _, cli in self._cli_processes.items():
            self._terminate(cli)

    ###############################################################

    def _terminate(self, cli):
        if cli:
            cli.terminate(force=True)

    def _make_cmd_args(self, *args):
        return [self._cli_script] + list(args)

    def _require_cli_and_name(self, **kwargs):
        cli_name = self._cli_name(**kwargs)
        process  = self._require_cli(**kwargs)

        return process, cli_name

    def _cli_name(self, **kwargs):
        cli_name = kwargs.get('cli', self.DEFAULT_NAME)

        # Handle empty name
        return cli_name if cli_name else self.DEFAULT_NAME

    def _store_process(self, proc, **kwargs):
        cli_name = self._cli_name(**kwargs)
        self._cli_processes[cli_name] = proc

    def _get_cli(self, **kwargs):
        cli_name = self._cli_name(**kwargs)
        return self._cli_processes.get(cli_name, None)

    def _require_cli(self, **kwargs):
        cli_name = self._cli_name(**kwargs)
        cli = self._get_cli(**kwargs)

        assert cli is not None, "cli with name {0} was not started".format(cli_name)

        return cli


class MatchedMessage(object):

    def __init__(self, msg, match):
        self.msg = msg
        self.match = match