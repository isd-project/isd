
class CliNode(object):
    def __init__(self, cli, name, keysdir=None, timeout=None):
        self.cli = cli
        self.name = name
        self.keysdir = keysdir
        self.timeout = timeout

    def start_command(self, *args, **kwargs):
        kwargs['cli'] = kwargs.get('cli', self.name)

        if(self.keysdir is not None):
            args = list(args)
            args.append("--keysdir")
            args.append(self.keysdir)

        return self.cli.starting_cli_with(*args, **kwargs)

    def wait_for_message(self, msg, **kwargs):
        self._set_default_timeout(kwargs)

        return self.cli.wait_for_message(msg, cli=self.name, **kwargs)

    def wait_for_messages_in_any_order(self, *msg_list, **kwargs):
        self._set_default_timeout(kwargs)

        expecting = list(msg_list)

        while expecting:
            found = self.cli.wait_for_one_of_messages(*expecting,  cli=self.name, **kwargs)

            expecting.remove(found.msg)

    def _set_default_timeout(self, kwargs):
        kwargs['timeout'] = kwargs.get('timeout', self.timeout)