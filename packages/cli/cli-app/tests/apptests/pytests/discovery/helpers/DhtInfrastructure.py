
from discovery.helpers import CliNode


class DhtInfrastructure(object):
    def __init__(self, cli, bind_addr='127.0.0.1'):
        self.cli = cli
        self.bootstrap_addr = None
        self.bind_addr = bind_addr

    def setup(self, number_of_helper_nodes=5):
        self.start_bootstrap_node()
        self.wait_for_bootstrap_complete()
        self.adding_helper_dht_nodes(number_of_helper_nodes)


    def start_bootstrap_node(self, name='bootstrap'):
        self.bootstrap = CliNode(self.cli, name)

        self.bootstrap.start_command("--bind", self.bind_addr)

    def wait_for_bootstrap_complete(self):
        msg = 'bound to address: (.*)\r\n'
        matched = self.bootstrap.wait_for_message(msg, timeout=1)

        self.bootstrap_addr = matched.match.group(1)

    def adding_helper_dht_nodes(self, number=5):
        for i in range(0, number):
            name = 'n{}'.format(i)
            self.start_dht_node(name)


    def start_dht_node(self, name):
        self.node = CliNode(self.cli, name)
        self.node.start_command("--bootstrap", self.bootstrap_addr)