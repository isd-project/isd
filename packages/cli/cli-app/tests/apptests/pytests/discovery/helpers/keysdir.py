import tempfile
import shutil

from os import path
from distutils.dir_util import copy_tree

from . import ProjectPaths

file_path = path.realpath(__file__)

class KeysDir(object):

    # DATA_DIR = path.abspath(path.join(file_path, '..', '..', '..', '..', 'robot', 'data'))
    # DEFAULT_KEYS_DIR = path.join(DATA_DIR, 'keys')

    DEFAULT_KEY = "a0984180118e78f5a19eb0017861f5d58b6af3b33b581376463e8d4ef9916cb1"

    @classmethod
    def default_keys_dir(cls):
        return ProjectPaths.pytests_path('discovery', 'tests', 'data', 'keys')

    @classmethod
    def create_from_default(cls):
        keysdir =  KeysDir()
        keysdir._copy_from(cls.default_keys_dir())

        return keysdir

    def __init__(self):
        self.tempdir = tempfile.mkdtemp()
        print(self.tempdir)
        self.path =  self.tempdir

    def _copy_from(self, from_path):
        copy_tree(from_path, self.tempdir)

    def destroy(self):
        shutil.rmtree(self.tempdir)