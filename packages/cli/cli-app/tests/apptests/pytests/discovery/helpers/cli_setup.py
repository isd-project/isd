from os import path

from . import ProjectPaths
from Cli import Cli


def build_cli():
    script_path = ProjectPaths.cli_script_path()

    return Cli(script_path)