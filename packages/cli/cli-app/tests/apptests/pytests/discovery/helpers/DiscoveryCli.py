from . import CliNode

class DiscoveryCli(object):
    def __init__(self, cli, name, keysdir=None, timeout=1.5):
        self.node = CliNode(cli, name, keysdir=keysdir, timeout=timeout)

        self.keys = KeysSubcommand(self.node)


    def publish(self, srv_name, srv_type, port, id=None, options=None):
        args = ['publish', srv_name, srv_type, port]

        if(id is not None):
            args.extend(('--id', id))

        args = extend_args_with_options(args, options)

        return self.node.start_command(*args)

    def wait_publication_completes(self):
        return self.node.wait_for_message('published service.*\r\n')

    def search(self, srv_type, options=None):
        args = ['search', srv_type]
        args = extend_args_with_options(args, options)

        return self.node.start_command(*args)

    def wait_for_service(self, srv_name, **kwargs):
        return self.wait_for_services(srv_name, **kwargs)

    def wait_for_services(self, *srv_names, **kwargs):
        msgs = ["Found service: {}".format(name) for name in srv_names]

        return self.node.wait_for_messages_in_any_order(*msgs, **kwargs)

def extend_args_with_options(args, options):
    if(options is not None):
        for k, v in options.items():
            args.extend((k, v))

    return args

class KeysSubcommand(object):
    def __init__(self, node):
        self.node = node

    def import_key(self, key_id, options=None):
        return self._command(['import', key_id], options)

    def _command(self, args, options=None):
        args = ['keys'] + extend_args_with_options(args, options)

        self.node.start_command(*args)

