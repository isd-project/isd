from os import path

file_path = path.realpath(__file__)


class ProjectPaths(object):
    _RELATIVE_ROOT = path.join('..', '..', '..', '..', '..', '..', '..', '..', '..')
    PROJ_ROOT = path.abspath(path.join(file_path, _RELATIVE_ROOT))

    @classmethod
    def project_path(cls, *segments):
        return path.abspath(path.join(cls.PROJ_ROOT, *segments))

    @classmethod
    def packages_path(cls, *segments):
        return cls.project_path('packages', *segments)


    @classmethod
    def pytests_path(cls, *segments):
        to_local_project = ['tests', 'apptests', 'pytests']
        return cls.cli_project_path(*to_local_project, *segments)

    @classmethod
    def cli_script_path(cls):
        return cls.cli_project_path('build', 'cli.js')

    @classmethod
    def cli_project_path(cls, *segments):
        return cls.packages_path('cli', 'cli-app', *segments)

    @classmethod
    def dht_bootstrapper_script(cls):
        return cls.packages_path('dht', 'dht-bootstrapper', 'build', 'bootstrapper.js')