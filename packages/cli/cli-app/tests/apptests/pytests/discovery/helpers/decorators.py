from functools import wraps

def retry_if_fails(max_attempts=2):
    remaining_attempts = max_attempts - 1

    def decorator(func):
        @wraps(func)
        def do_retry(*args, **kwargs):
            for i in range(0, max_attempts):
                remaining_attempts = max_attempts - (i + 1)

                if attempt(remaining_attempts, *args, **kwargs) == True:
                    return

        def attempt(remaining_attempts, *args, **kwargs):
            try:
                func(*args, **kwargs)

                #passed
                return True
            except:
                func_name = func.__name__

                if remaining_attempts == 0:
                    print('Tryied {} {} times, but failed.'.format(func_name, max_attempts))
                    raise
                else:
                    print('{} failed. Retrying...'.format(func_name))

                    return False


        return do_retry

    return decorator