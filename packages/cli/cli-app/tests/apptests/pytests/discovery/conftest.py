import pytest

from os import path

from discovery.helpers import cli_setup
from discovery.helpers import ProjectPaths
from discovery.helpers import DhtInfrastructure

from Cli import Cli


@pytest.fixture()
def keysdir(shared_datadir):
    return path.join(str(shared_datadir), 'keys')


@pytest.fixture()
def cli():
    cli = cli_setup.build_cli()
    yield cli
    cli.close_all_clis()

@pytest.fixture()
def dht_bootstrapper():
    script_path = ProjectPaths.dht_bootstrapper_script()

    cli = Cli(script_path)

    yield cli

    cli.close_all_clis()

@pytest.fixture()
def dht_infra(dht_bootstrapper):
    return DhtInfrastructure(dht_bootstrapper)