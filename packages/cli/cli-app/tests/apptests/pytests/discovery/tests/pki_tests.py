import pytest

from discovery.helpers import KeysDir
from discovery.helpers import DiscoveryCli
from discovery.helpers.decorators import *


class PkiTests(object):


    def test_search_key_mixed(self, keysdir, cli, tmp_path, dht_infra):
        dht_infra.setup()

        self.search_key_with_pki(keysdir, cli, tmp_path, {
            "--bootstrap": dht_infra.bootstrap_addr
        })

    @pytest.mark.skip() # disabled to reduce test times
    def test_search_key_with_mdns(self, keysdir, cli, tmp_path):
        self.search_key_with_pki(keysdir, cli, tmp_path, {
            '--pki': 'mdns',
            '--mechanism': 'mdns'
        })

    @pytest.mark.skip() # disabled to reduce test times
    def test_search_key_with_dht(self, keysdir, cli, tmp_path, dht_infra):
        dht_infra.setup()

        self.search_key_with_pki(keysdir, cli, tmp_path, {
            "--bootstrap": dht_infra.bootstrap_addr,
            '--pki': 'dht',
            '--mechanism': 'dht'
        })

    def search_key_with_pki(self, keysdir, cli, tmp_path, options):
        publisher = DiscoveryCli(cli, 'publisher', keysdir)
        client    = DiscoveryCli(cli, 'client', str(tmp_path))

        publisher.publish("myservice", "example", "4321", KeysDir.DEFAULT_KEY,
                                                            options)
        publisher.wait_publication_completes()

        client.keys.import_key(KeysDir.DEFAULT_KEY, options)
        client.node.wait_for_message('Imported key.+%s' % KeysDir.DEFAULT_KEY, timeout=2)

