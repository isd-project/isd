
import Cli

from discovery.helpers import cli_setup

class CommandLineTest(object):
    def setup_method(self):
        self.cli = cli_setup.build_cli()

    def test_help(self):
        out = self.cli.run_cli_with("--help", encoding='utf-8')

        assert 'publish' in out
        assert 'search' in out
        assert 'locate' in out
        assert 'keys' in out
