from discovery.helpers import KeysDir
from discovery.helpers import DiscoveryCli
from discovery.helpers.decorators import *

from Cli import Cli

import pytest

class DiscoveryTests(object):

    @retry_if_fails(max_attempts=3)
    def test_mdns_discovery(self, keysdir, cli):
        self._test_discovery(cli, keysdir, {
            '--mechanism': 'mdns',
            '--pki': 'mdns'
        })

    @retry_if_fails(max_attempts=3)
    def test_dht_discovery(self, keysdir, cli, dht_infra):
        dht_infra.setup()

        self._test_discovery(cli, keysdir, {
            "--bootstrap": dht_infra.bootstrap_addr,
            "--mechanism": "dht",
            '--pki': 'dht'
        })

    def _test_discovery(self, cli, keysdir, options):
        publisher = DiscoveryCli(cli, 'publisher', keysdir)
        client    = DiscoveryCli(cli, 'client')

        publisher.publish("myservice", "example", "4321", KeysDir.DEFAULT_KEY,
                            options)
        publisher.wait_publication_completes()

        client.search("example", options)
        client.wait_for_service("myservice")


    @retry_if_fails(max_attempts=3)
    def test_mixed_mechanisms(self, keysdir, cli, dht_infra):
        dht_infra.setup()

        p1 = DiscoveryCli(cli, 'publisher1', keysdir)
        p2 = DiscoveryCli(cli, 'publisher2', keysdir)
        p3 = DiscoveryCli(cli, 'publisher3', keysdir)

        client    = DiscoveryCli(cli, 'client')

        p1.publish("srvDht", "example", "4321", KeysDir.DEFAULT_KEY, {
            '--mechanism': 'dht',
            '--pki': 'dht',
            '--bootstrap': dht_infra.bootstrap_addr
        })

        p2.publish("srvMdns", "example", "7891", KeysDir.DEFAULT_KEY, {
            '--mechanism': 'mdns',
            '--pki': 'mdns'
        })

        p3.publish("srvBoth", "example", "5678", KeysDir.DEFAULT_KEY, {
            '--bootstrap': dht_infra.bootstrap_addr
        })

        for publisher in (p1, p2, p3):
            publisher.wait_publication_completes()

        client.search("example", {
            '--bootstrap': dht_infra.bootstrap_addr
        })
        client.wait_for_services("srvDht", "srvMdns", "srvBoth", timeout=2)