package discovery.cli.console;

import haxe.io.Output;

class RedirectConsole implements Console{
    var output:Output;
    var console:Console;

    public function new(output:Output, console:Console) {
        this.output = output;
        this.console = console;
    }

    public function print(v:Dynamic) {
        output.writeString(Std.string(v));
    }

    public function println(v:Dynamic) {
        print(v); print('\n');

        output.flush();
    }

    public function printError(msg:String) {
        console.printError(msg);
    }

    public function readline():String {
        return console.readline();
    }

    public function onReadline(callback:String -> Void) {
        console.onReadline(callback);
    }
}