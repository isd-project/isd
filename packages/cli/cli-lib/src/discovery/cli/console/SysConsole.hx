package discovery.cli.console;


class SysConsole implements Console{
    public function new() {}

    public function print(v:Dynamic) {
        Sys.print(v);
    }

    public function println(v:Dynamic) {
        Sys.println(v);
    }

    public function readline():String {
        return Sys.stdin().readLine();
    }

    public function onReadline(callback:String -> Void) {
        #if nodejs
            js.Node.process.stdin.on('data', callback);
        #else
            throw new haxe.exceptions.NotImplementedException();
        #end
    }

    public function printError(msg:String) {
        Sys.stderr().writeString(msg + "\n");
    }
}