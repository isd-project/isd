package discovery.cli.console;

interface Console {
    function print(v: Dynamic): Void;
    function println(v: Dynamic): Void;
    function printError(msg:String):Void;

    function readline(): String;

    function onReadline(callback:(String)->Void):Void;
}