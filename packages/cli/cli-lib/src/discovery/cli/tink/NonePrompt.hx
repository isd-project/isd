package discovery.cli.tink;

import tink.cli.Prompt;

import tink.Stringly;
import tink.core.Promise;
import tink.core.Noise;


class NonePrompt implements Prompt{
    public function new() {}

    public function print(v:String):Promise<Noise>{
        return null;
    }
    public function println(v:String):Promise<Noise>{
        return null;
    }
    public function prompt(type:PromptType):Promise<Stringly>{
        return '';
    }
}