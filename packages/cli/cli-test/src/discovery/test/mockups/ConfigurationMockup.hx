package discovery.test.mockups;

import discovery.test.common.mockups.PkiMockup;
import discovery.bundle.ConfigureOptions;
import discovery.test.common.mockups.KeychainMockup;
import discovery.test.common.mockups.parsing.IdentifierParserMockup;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import mockatoo.Mockatoo;
import discovery.cli.configuration.Configuration;

class ConfigurationMockup {
    var mock:Configuration;

    var discoveryMockup:DiscoveryMockup;
    var idParserMockup:IdentifierParserMockup;
    var consoleMockup:ConsoleMockup;

    var keychainMockup:KeychainMockup;
    var keycacheMockup:KeychainMockup;

    var pkiMockup:PkiMockup;

    public function new() {
        discoveryMockup = new DiscoveryMockup();
        idParserMockup  = new IdentifierParserMockup();
        consoleMockup   = new ConsoleMockup();
        keychainMockup  = new KeychainMockup();
        keycacheMockup  = new KeychainMockup();
        pkiMockup       = new PkiMockup();

        mock = Mockatoo.mock(Configuration);

        Mockatoo.when(mock.discovery()).thenReturn(discovery().discovery());
        Mockatoo.when(mock.idParser()).thenReturn(idParser().idParser());
        Mockatoo.when(mock.console).thenReturn(consoleMockup.console());
        Mockatoo.when(mock.keychain()).thenReturn(keychainMockup.keychain());
        Mockatoo.when(mock.pki()).thenReturn(pkiMockup.pki);
    }

    public function configuration():Configuration {
        return mock;
    }

    public function discovery() { return discoveryMockup; }
    public function idParser()  { return idParserMockup; }
    public function console()   { return consoleMockup; }
    public function keychain()  { return keychainMockup; }
    public function pki()       { return pkiMockup;}

    public function verifyConfiguredWith(configs:ConfigureOptions) {
        Mockatoo.verify(mock.configure(configs));
    }

    public function verifyRedirectOutputToFile(output:String)
    {
        Mockatoo.verify(mock.redirectOutputToFile(output));
    }
}