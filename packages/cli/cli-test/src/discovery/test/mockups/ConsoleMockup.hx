package discovery.test.mockups;

import discovery.test.fakes.FakeConsole;
import mockatoo.Mockatoo;
import discovery.cli.console.Console;

import org.hamcrest.Matchers.assertThat;
import org.hamcrest.Matchers.hasItem;
import org.hamcrest.Matchers.containsString;

using discovery.test.matchers.CommonMatchers;


class ConsoleMockup {
    var _console:FakeConsole;

    public function new() {
        _console = Mockatoo.spy(FakeConsole);
    }

    public function console(): Console {
        return _console;
    }

    public function verifyPrintlnCalledWithSomething() {
        Mockatoo.verify(_console.println(isNotNull), atLeastOnce);
    }

    public function verifyPrintlnCalled(?mode:VerificationMode) {
        Mockatoo.verify(_console.println(isNotNull), mode);
    }

    public function verifyPrintlnCalledWith(expected:String) {
        Mockatoo.verify(_console.println(expected));
    }

    public function verifyAnErrorWasPrinted() {
        Mockatoo.verify(_console.printError(isNotNull), atLeastOnce);
    }

    public function verifyPrintedLinesContains(expected:String) {
        linesContains(_console.printedLines(), expected);
    }

    public function verifyPrintedErrorsContains(expected:String) {
        linesContains(_console.printedErrors(), expected);
    }

    function linesContains(lines:Array<String>, expected:String) {
        assertThat(lines, hasItem(containsString(expected)));
    }

    public function verifyReadlineCalled() {
        Mockatoo.verify(_console.readline());
    }
}