package discovery.test.mockups;

import discovery.bundle.ConfigureOptions;
import discovery.test.common.mockups.discovery.DiscoveryMockup;
import mockatoo.Mockatoo;
import discovery.bundle.ConfigurableDiscoveryBuilder;

class ConfigurableDiscoveryBuilderMockup {
    var mock:ConfigurableDiscoveryBuilder;

    var discoveryMockup:DiscoveryMockup;

    var lastConfigOptions:ConfigureOptions;

    public function new() {
        discoveryMockup = new DiscoveryMockup();

        mock = Mockatoo.mock(ConfigurableDiscoveryBuilder);

        Mockatoo.when(mock.build()).thenReturn(discoveryMockup.discovery());
        Mockatoo.when(mock.configure(any)).thenCall(onConfigure);
    }

    function onConfigure(args: Array<Dynamic>) {
        lastConfigOptions = args[0];
    }

    public function lastConfiguration() {
        return lastConfigOptions;
    }

    public function discoveryBuilder() {
        return mock;
    }

    public function verifyNeverBuilt() {
        Mockatoo.verify(mock.build(), never);
    }

    public function verifyBuilt() {
        Mockatoo.verify(mock.build());
    }

    public function verifyConfigured() {
        Mockatoo.verify(mock.configure(any));
    }
}