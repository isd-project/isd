package discovery.test.mockups;

import discovery.test.bundle.mockups.KeysFactoryMockup;
import discovery.cli.configuration.IdentifierParserBuilder;
import mockatoo.Mockatoo;
import discovery.cli.configuration.Configuration.ConfigurationDependencies;
import discovery.test.common.mockups.parsing.IdentifierParserMockup;
import discovery.test.common.mockups.discovery.DiscoveryMockup;

class ConfigurationDependenciesMockup {

    var discoveryBuilderMockup:ConfigurableDiscoveryBuilderMockup;
    var idParserMockup:IdentifierParserMockup;
    var consoleMockup:ConsoleMockup;
    var keysFactoryMockup:KeysFactoryMockup;

    public function new() {
        discoveryBuilderMockup = new ConfigurableDiscoveryBuilderMockup();
        idParserMockup  = new IdentifierParserMockup();
        consoleMockup   = new ConsoleMockup();
        keysFactoryMockup = new KeysFactoryMockup();
    }

    public function dependencies(): ConfigurationDependencies {
        return {
            console: consoleMockup.console(),
            idParserBuilder: idParserBuilder(),
            keysFactory: keysFactoryMockup.keysFactory(),
            discoveryBuilder: discoveryBuilderMockup.discoveryBuilder()
        };
    }

    function idParserBuilder() {
        var idParserBuilder = Mockatoo.mock(IdentifierParserBuilder);

        Mockatoo
            .when(idParserBuilder.build())
            .thenReturn(idParserMockup.idParser());

        return idParserBuilder;
    }

    public function keysFactory() {
        return keysFactoryMockup;
    }

    public function discoveryBuilder() {
        return discoveryBuilderMockup;
    }
}