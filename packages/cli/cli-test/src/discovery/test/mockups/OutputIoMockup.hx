package discovery.test.mockups;

import discovery.test.common.matchers.MockatooMatchers.*;
import haxe.io.Output;
import mockatoo.Mockatoo;

class OutputIoMockup {
    var out = Mockatoo.mock(Output);

    public function new() {}

    public function output():Output {
        return out;
    }

    public function verifyWritedString(expectedText:String) {
        Mockatoo.verify(out.writeString(equalsMatcher(expectedText), any));
    }
}