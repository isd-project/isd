package discovery.test.fakes;

typedef StoredCommand = {
    command: String,
    args   : Array<Dynamic>
};

class CommandsStore {
    public var command(get, never):String;
    public var args(get, never):Array<Dynamic>;

    public var commands(default, null):Array<StoredCommand>;

    public function new() {
        commands = [];
    }

    function get_command() {
        var cmd = lastCommand();
        return if(cmd != null) cmd.command else null;
    }
    function get_args() {
        var cmd = lastCommand();
        return if(cmd != null) cmd.args else null;
    }

    public function lastCommand(){
        return if(commands.length > 0) commands[commands.length-1] else null;
    }

    public function storeCommand(cmd:String, args:Array<Dynamic>) {
        this.commands.push({
            command: cmd,
            args: if(args != null) args.copy() else null
        });
    }
}