package discovery.test.fakes;

import discovery.cli.console.Console;

class FakeConsole implements Console {
    var _printedLines:Array<String> = [];
    var _printedErrors:Array<String> = [];

    var linesToRead:Array<String> = [];

    public function new() {
    }

    public function print(v:Dynamic) {}

    public function println(v:Dynamic) {
        _printedLines.push(Std.string(v));
    }

    public function printError(msg:String) {
        _printedErrors.push(msg);
    }

    public function setNextReadLines(lines: Array<String>) {
        for(l in lines){
            linesToRead.push(l);
        }
    }

    public function readline():String {
        return linesToRead.shift();
    }

    public function onReadline(callback:String -> Void) {}

    // ------------------------------------------------------------------

    public function printedLines() {
        return _printedLines;
    }

    public function printedErrors():Array<String> {
        return _printedErrors;
    }
}