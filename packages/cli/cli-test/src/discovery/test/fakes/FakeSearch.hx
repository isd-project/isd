package discovery.test.fakes;

import discovery.utils.events.EventEmitterFactory;
import discovery.base.search.SearchBase;

class FakeSearch extends SearchBase{
    public var stopCallback:()->Void;

    public function new(?eventFactory: EventEmitterFactory) {
        super(null, eventFactory);
    }

    override public function stop(?callback:() -> Void) {
        this.stopCallback = callback;
    }
}