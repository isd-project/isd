package discovery.test.fakes;

import discovery.async.promise.Promisable;
import discovery.keys.crypto.KeyGenerator.GenerateKeyOptions;
import discovery.cli.commands.KeyCommands;

class FakeKeysCommands implements KeyCommands{
    var commandsStore: CommandsStore;

    public var outputKeysOptions(default, null):OutputKeysOptions;

    public function new(commandsStore: CommandsStore) {
        this.commandsStore = commandsStore;
    }

    public function configureKeysOutput(options:OutputKeysOptions) {
        this.outputKeysOptions = options;
    }

    public function listKeys() {
        storeCommand("list", []);
    }

    function storeCommand(cmd:String, args: Array<Dynamic>) {
        this.commandsStore.storeCommand("keys-" + cmd, args);
    }

    public function generateKeys(options:GenerateKeyOptions): Promisable<Any>
    {
        this.storeCommand("gen", [options]);

        return new FakePromisable("done");
    }

    public function importKey(keyId:String):Void {
        this.storeCommand("importKey", [keyId]);
    }
}