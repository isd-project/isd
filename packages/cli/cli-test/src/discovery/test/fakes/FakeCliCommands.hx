package discovery.test.fakes;

import discovery.bundle.ConfigureOptions;
import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.test.fakes.CommandsStore.StoredCommand;
import haxe.Exception;
import discovery.cli.commands.KeyCommands;
import discovery.domain.Query;
import discovery.domain.Description;
import discovery.cli.CliCommands;

class FakeCliCommands implements CliCommands{
    public var command(get, null):String;
    public var args(get, null):Array<Dynamic>;
    public var capturedError(default, null): Exception = null;

    var keyCommands:FakeKeysCommands;
    var commandStore:CommandsStore;

    public function new() {
        commandStore = new CommandsStore();
        keyCommands = new FakeKeysCommands(commandStore);
    }

    function get_command() {
        return commandStore.command;
    }
    function get_args() {
        return commandStore.args;
    }

    public function commands(): Array<StoredCommand>{
        return commandStore.commands;
    }

    public function help(helpText:String) {
        storeCommand("help", [helpText]);
    }

    public function configure(?config:ConfigureOptions)
    {
        storeCommand("configure", [config]);
    }

    public function publish(info: Description) {
        storeCommand("publish", [info]);
    }

    public function search(query:Query) {
        storeCommand("search", [query]);
    }

    public function locate(identifierUrl:String) {
        storeCommand("locate", [identifierUrl]);
    }

    public function keys(): FakeKeysCommands {
        return keyCommands;
    }

    function storeCommand(command:String, args: Array<Dynamic>) {
        this.commandStore.storeCommand(command, args);
    }

    public function onError(error:Exception) {
        this.capturedError = error;
    }
}