package discovery.test.generator;

import discovery.bundle.keychain.ConfigureKeysOptions;
import discovery.test.helpers.RandomGen;
import discovery.bundle.ConfigureOptions;

class CliRandomGenerator {
    public function new() {

    }

    public function configOptions(): ConfigureOptions {
        return {
            keys: configKeyOptions()
        };
    }

    public function configKeyOptions():ConfigureKeysOptions {
        return {
            keysdir: RandomGen.primitives.name(10,14)
        };
    }
}