const { exec, execSync, spawn }   = require('child_process');

const gulp     = require('gulp');
const series   = gulp.series;
const parallel = gulp.parallel;
const merge    = require('merge-stream');
const del      = require('del')
const src      = gulp.src;
const through2 = require('through2');
const Vinyl    = require('vinyl');

const path = require('path')
const fs   = require('fs')
const fsPromises = fs.promises
const process = require('process')
const util = require('util');
const { exitCode } = require('process');

// -------------------------------------------------------------------------

const bundleUnitTests        = unitTestsFor('bundle');
const bundleIntegrationTests = integrationTestsFor('bundle');

const dhtUnitTests        = unitTestsFor('dht/dht-mechanism', 'dht');
const dhtIntegrationTests = integrationTestsFor('dht/dht-mechanism', 'dht');

// composed tasks ----------------------------------------------------------

const postInstall = series(mountLocalLibs, lixDownload);

const buildCli         = series(buildCliPackage, turnCliExecutable)
const mountAndBuild    = series(mountLocalLibsIfNeeded, buildCli);
const build            = mountAndBuild;
const buildExperimentNode = series(buildExpNodePackage, turnExpNodeExecutable)
const buildBootstrapper = series(buildBootstrapperPackage,
                                 turnBootstrapperExecutable)
const buildAll         = series(mountAndBuild, buildBootstrapper, buildExperimentNode)

const unitTests        = series(
                                parallel(
                                    coreUnitTests, cliUnitTests,
                                    mdnsUnitTests, dhtUnitTests,
                                    dhtBootstrapperUnitTests,
                                    bundleUnitTests,
                                    experimentNodeUnitTests),
                                collectUnitTestsReports);
const integrationTests = series(
                                coreIntegrationTests,
                                mdnsIntegrationTests,
                                dhtIntegrationTests,
                                bundleIntegrationTests,
                                collectIntegrationTestsReports);

const allRobotTests = cliRobotTests;
const robotTests    = series(build, allRobotTests);
const robotDebug    = series(build, cliRobotTestsDebug);


const cliAppTests   = series(buildCli, buildBootstrapper, cliPytests);

const expUnitTests = parallel(
                        experimentExecUnitTests,
                        expGenUnitTests)
const expPyTests  = series(parallel(buildExperimentNode, expUnitTests),
                            expGenIntegrationTests,
                            experimentExecIntegrationTests,
                            experimentNodeE2ETests);
const e2eTests      = series(cliAppTests, expPyTests, allRobotTests);

const testAll       = series(unitTests, integrationTests, e2eTests);

// Export -----------------------------------------------------------------

module.exports = {
    default         : unitTests,
    robotTests      : robotTests,
    robotDebug      : robotDebug,
    mountLocalLibs  : mountLocalLibs,
    postInstall     : postInstall,
    build           : build,
    buildAll        : buildAll,
    buildCli        : build,
    buildBootstrapper: buildBootstrapper,
    buildExperimentNode: buildExperimentNode,
    buildSamples    : buildSamples,

    testAll         : testAll,
    test            : unitTests,
    unitTests       : unitTests,
    integrationTests: integrationTests,
    e2eTests        : e2eTests,

    // report --------------------------------------------

    collectUnitTestsReports: collectUnitTestsReports,
    collectIntegrationTestsReports: collectIntegrationTestsReports,
    collectAppTestsReports: collectAppTestsReports,

    // tests --------------------------------------------

    cliUnitTests        : cliUnitTests,
    cliAppTests         : cliAppTests,

    coreUnitTests       : coreUnitTests,
    coreIntegrationTests: coreIntegrationTests,

    mdnsUnitTests       : mdnsUnitTests,
    mdnsIntegrationTests: mdnsIntegrationTests,

    dhtUnitTests            : dhtUnitTests,
    dhtIntegrationTests     : dhtIntegrationTests,
    dhtBootstrapperUnitTests: dhtBootstrapperUnitTests,

    bundleUnitTests       : bundleUnitTests,
    bundleIntegrationTests: bundleIntegrationTests,

    // simulations --------------------------------------------

    simulationsRobotTests     : simulationsRobotTests,
    simulationsRobotTestsDebug: simulationsRobotTestsDebug,
    simulatedTests            : series(
                                    buildAll,
                                    experimentExecSimulTests,
                                    simulationsRobotTests
                                ),

    // experiment ---------------------------------------------
    experimentUnitTest: series(experimentExecUnitTests, expGenUnitTests),

    experimentExecUnitTests    : experimentExecUnitTests,
    experimentExecIntegrationTests: experimentExecIntegrationTests,
    experimentExecSimulTests   : series(buildExperimentNode,
                                        buildBootstrapper,
                                        experimentExecSimulTests),
    experimentExecSimulTestsDbg: series(buildExperimentNode,
                                        buildBootstrapper,
                                        experimentExecSimulTestsDbg),

    experimentExecRunSimulTestsDbg: experimentExecSimulTestsDbg,

    expGenUnitTests: expGenUnitTests,
    expGenIntegrationTests: expGenIntegrationTests,

    experimentNodeUnitTests: experimentNodeUnitTests,
    experimentNodeIntegrationTests: experimentNodeIntegrationTests,
    experimentNodeE2ETests: series(buildExperimentNode, experimentNodeE2ETests),

    experimentPythonTests: expPyTests,

    // install ------------------------------------------------------------
    installPythonLibs: series(
        installCommandsPackage, installCoreRobotPackage, installExpGenerator, installExpExecutor
    ),
    intallCommandsPackage: installCommandsPackage,
    installCoreRobotPackage: installCoreRobotPackage,
    installExpGenerator: installExpGenerator,
    installExpExecutor: installExpExecutor,
};

// Install ---------------------------------------------------------------

// enable workaround to fix error when trying to install local libs (in some environments)
const PYTHON_NO_BUILD_ISOLATION = true;


async function mountLocalLibs(){
    return maybeMountLocalLibs(false);
}

async function mountLocalLibsIfNeeded(){
    return maybeMountLocalLibs(true);
}

async function maybeMountLocalLibs(skip_if_exists=true){
    console.log(`skip_if_exists=${skip_if_exists}`);

    const pckgs = [
        'core', 'test-commons',
        'mdns', 'mdns-test-lib',
        'cli/cli-lib', 'cli/cli-test',
        'dht/dht-mechanism', 'dht/dht-bootstrapper', 'dht/dht-test-lib',
        'bundle', 'bundle/bundle-test-lib'
    ];

    return await Promise.all(pckgs.map(pkg => {
        return mountLocalLib(pkg, skip_if_exists);
    }));
}

function lixDownload(){
    return execPipe('lix', ['download']);
}


// Build -----------------------------------------------------------------

function buildCliPackage(){
    return haxe(packagePath("cli", "cli-app", "build.js.hxml"));
}
async function turnCliExecutable(){
    return turnIntoExecutable(packagePath("cli", "cli-app", "build", 'cli.js'))
}


function buildBootstrapperPackage(){
    return haxe(packagePath('dht', 'dht-bootstrapper', 'build.js.hxml'))
}
function turnBootstrapperExecutable(){
    return turnIntoExecutable(packagePath('dht', 'dht-bootstrapper', 'build', 'bootstrapper.js'))
}

function buildExpNodePackage(){
    return haxe(packagePath('experiments', "experiment-node", "build.js.hxml"));
}
function turnExpNodeExecutable(){
    return turnIntoExecutable(packagePath('experiments', "experiment-node", 'build', 'experiment-node.js'))
}

function buildSamples(){
    // return haxe(packagePath("cli", "cli-app", "build.js.hxml"));

    var buildDir = packagePath('samples', 'build');

    return src(packagePath("samples", "*-sample.hxml"))
            .pipe(through2.obj(buildHaxe))
            .pipe(through2.obj(getOutputPath(buildDir, 'js')))
            .pipe(through2.obj(turnIntoExec));
}

function buildHaxe(file, enc, cb){
    console.debug(`buildHaxe: ${file.path}`);

    return thenCallback(
        onExit(haxe(file.path))
            .then((_)=>{

                console.debug(`built ${file.path}`);
                return new Vinyl({path: file.path});
            }),
        cb
    )
}

function getOutputPath(buildDir, extension){
    return function(file, enc, cb){
        const output = buildDir + '/' + file.stem + '.' + extension;
        const outputFile = new Vinyl({path:output});

        console.debug(`getOutputPath(${file.path}) => ${output}`);


        cb(null, outputFile);
    };
}

function onExit(child_proc, cb=null){
    var p = new Promise((resolve, reject)=>{
        child_proc.on('error', (err)=>reject(err));
        child_proc.on('exit', (exitCode)=>{
            if (parseInt(exitCode) !== 0) {
                reject(`Exited with code: ${exitCode}`);
            }
            else{
                resolve();
            }
        });
    });

    if(cb !== null){
        return p.then(util.promisify(cb));
    }

    return p;
}

function turnIntoExec(file, enc, cb){
    console.debug(`Turning into executable: ${file.path}`);

    return thenCallback(
        turnIntoExecutable(file.path)
                .then((result)=>file.path),
        cb
    );
}

async function turnIntoExecutable(filePath){
    const st = await fsPromises.stat(filePath);
    const newmode = st.mode |
                    fs.constants.S_IXUSR |
                    fs.constants.S_IXGRP |
                    fs.constants.S_IXOTH;

    return await fsPromises.chmod(filePath, newmode);
}

function thenCallback(promise, cb){
    return promise.then(
        (res)=>cb(null,res),
        (err)=>cb(err,null));
}

// Tests -----------------------------------------------------------------

function coreUnitTests()       { return munitTest('core/tests/unit');}
function coreIntegrationTests(){ return munitTest('core/tests/integration');}

function mdnsUnitTests()       { return munitTest('mdns/tests/unit');}
function mdnsIntegrationTests(){ return munitTest('mdns/tests/integration');}


function dhtBootstrapperUnitTests() {
    return munitTest('dht/dht-bootstrapper/tests/unit');
}

function cliUnitTests()      { return munitTest('cli/cli-app/tests/unit');}
function cliPytests()        { return pytest('cli/cli-app/tests/apptests/pytests');}
function cliRobotTests()     { return _cliRobotTests(); }
function cliRobotTestsDebug(){ return _cliRobotTests(["--include", "debug"]); }
function _cliRobotTests(options=[]){
    options = options.concat([
        '--skiponfailure', 'pending',
        '--skiponfailure', 'unstable'
    ]);

    return robot(packagePath('cli', 'cli-app', 'tests', 'robot'), options);
}

// ** Experiment node ---------------------------

function experimentNodeUnitTests(){
    return munitTest('experiments/experiment-node/tests/unit');
}
function experimentNodeIntegrationTests(){
    return munitTest('experiments/experiment-node/tests/integration');
}
function experimentNodeE2ETests(){
    const testDir = packagePath('experiments', 'experiment-node', 'tests', 'e2e');

    return pytestExec(testDir);
}

// ** Experiment executor ---------------------------

function experimentExecUnitTests(){
    const testDir = packagePath(
                        'experiments', 'experiment-executor', 'tests', 'unit');
    return pytestExec(testDir);
}

function experimentExecIntegrationTests(){
    const testDir = packagePath('experiments', 'experiment-executor', 'tests',
                                    'integration');
    return pytestExec(testDir);
}

function experimentExecSimulTestsDbg(cb){
    return experimentExecSimulTests(cb, true);
}
function experimentExecSimulTests(cb, debug=false){
    const testDir = packagePath(
                'experiments', 'experiment-executor', 'tests', 'simulation');

    return simulationsPytests(cb, testDir, debug)
}

// ** Experiment generator ---------------------------

function expGenUnitTests(){
    const testDir = packagePath(
                        'experiments', 'experiment-generator', 'tests', 'unit');
    return pytestExec(testDir);
}

function expGenIntegrationTests(){
    const testDir = packagePath('experiments', 'experiment-generator',
                                    'tests', 'integration');
    return pytestExec(testDir);
}

function installExpGenerator(){
    const projectDir = packagePath('experiments', 'experiment-generator')

    return pipInstallLocal(projectDir)
}

function installExpExecutor(){
    const projectDir = packagePath('experiments', 'experiment-executor')

    return pipInstallLocal(projectDir)
}


function installCommandsPackage(){
    return pipInstallLocal(packagePath('pylibs', 'commands'))
}

function installCoreRobotPackage(){
    return pipInstallLocal(packagePath('pylibs', 'core-robot'))
}

function pipInstallLocal(projectDir){
    var args = ['-m', 'pip', 'install', '-e', projectDir];

    if(PYTHON_NO_BUILD_ISOLATION){
        args.push('--no-build-isolation')
    }

    return pythonExec(args)
}

// collect reports ------------------------------------------------------------

function collectUnitTestsReports(){
    return collectTestReports('unit');
}

function collectIntegrationTestsReports(){
    return collectTestReports('integration');
}

async function collectAppTestsReports(){
    srcFiles = packagePath(
        'cli', 'cli-app', 'tests', 'apptests', 'pytests', 'report', '*.xml');
    outDir = projectPath('report', 'app', 'cli-app');

    await removeFiles(path.join(outDir, '**'));
    return copyFiles(srcFiles, outDir);
}

async function collectTestReports(testType){
    await cleanReports(testType);
    return collectReports(testType);
}

function cleanReports(testType){
    return removeFiles(projectPath('report', testType,'**'));
}

function collectReports(testType){
    var sources = ['core', 'mdns', 'dht/dht-mechanism', 'dht/dht-bootstrapper', 'cli/cli-app', 'experiments/experiment-node'];

    return merge(sources.map((package)=>{
        return copyFiles(
            packagePath(package, 'tests', testType, 'report', 'junit', '**', '*'),
            projectPath('report', testType, package)
        )
    }));
}

function removeFiles(from){
    return del([from], {force: true});
}

function copyFiles(from, to){
    return gulp.src(from)
            .pipe(gulp.dest(to))
    ;
}


// simulations ---------------------------------------------------------------

function simulationsRobotTestsDebug(cb){
    return simulationsRobotTests(cb, ['--include', 'debug', '--loglevel', 'DEBUG'])
}


function simulationsRobotTests(cb, options=[]){
    const env     = getPythonEnv();
    const testDir = projectPath('simulations');

    return robot(testDir, options, {env: env, python: "core-python"})
}

function simulationsPytests(cb, testDir, debug=false, pytestargs=null){
    pytestargs = pytestargs || []

    if(debug){
        pytestargs = pytestargs.concat(['-m', 'debug']);
    }

    var pythonpath = getPythonpaths('core-python');

    return pytestExec(testDir, {
        pythonpath : pythonpath,
        pytestargs : pytestargs
    });
}


// Python helpers -------------------------------------------------------------

function pythonLibsPaths(){
    return [
        projectPath('simulations', 'src', 'pylibs'),
        packagePath('cli', 'cli-app', 'tests', 'robot', 'libs'),
        packagePath('pylibs', 'commands', 'src'),
        packagePath('pylibs', 'core-robot', 'src'),
        packagePath('experiments', 'experiment-generator', 'src'),
        packagePath('experiments', 'experiment-generator', 'test-libs'),
        packagePath('experiments', 'experiment-executor', 'src'),
        packagePath('experiments', 'experiment-executor', 'test-libs')
    ];
}

function pytest(pkgPath, options=null){
    return pytestExec(packagePath(pkgPath), options);
}

function pytestExec(testDir, options=null){
    if(options == null){
        options = {};
    }

    var reportDir   = options.reportdir || path.join(testDir, 'report');
    var junitReport = path.join(reportDir, 'report.xml');
    var pytestargs  = options.pytestargs || [];
    var args = [
        "-m", "pytest",
        '--tb=short', '--color=yes',
        '-s', testDir,
        `--junitxml=${junitReport}`].concat(pytestargs);

    var pypath = getPylibsPaths(options.pythonpath);

    var execOptions = {};
    execOptions.env = { ...process.env, PYTHONPATH: pypath};

    var debug = false;
    if (process.env['PYTEST_DBG']){
        args.push('--pdb');
        debug = true;
    }
    if(process.env['PYTEST_TRACE']){
        args.push('--trace');
        debug = true;
    }
    if (debug){
        args.push('-s');
        execOptions.stdio = 'inherit';
    }

    return pythonExec(args, execOptions);
}

function robot(testDir, robotOptions=[], options=undefined){
    robotOptions.push(testDir)

    const args = ['-m', 'robot'].concat(robotOptions)

    var execOptions = options || {};
    execOptions.env = extendPythonPath(execOptions.env, getPylibsPaths());

    return pythonExec(args, execOptions);
}

function pythonExec(args=[], options=undefined){
    options = options || {};
    python  = options.python || process.env.PYTHON || 'python3'

    return execPipe(python, args, options);
}

function getPylibsPaths(pythonpath = null){
    var pypath = pythonLibsPaths().join(':');

    if(pythonpath != null){
        pypath = pythonpath + ':' + pypath;
    }

    return pypath
}

function extendPythonPath(env, pythonpath){
    env = env || {...process.env};

    if(env.PYTHONPATH == null){
        env.PYTHONPATH = '';
    }
    else{
        env.PYTHONPATH += ':';
    }

    env.PYTHONPATH += pythonpath;

    return env;
}

function getPythonEnv(...pythons){
    if(!pythons || pythons.length == 0){
        pythons = [undefined];
    }

    pythonpath = getPythonpaths(...pythons)

    console.debug(`python3 sys.path: "${pythonpath}"`)

    return { ...process.env, PYTHONPATH: pythonpath }
}

function getPythonpaths(...pythons){
    pythonpaths = [];

    pythons.forEach((python)=>{
        pythonpaths.push(getPythonpath(python))
    })

    return pythonpaths.join(':').replace('::',':');
}
function getPythonpath(python=undefined){
    python = python || 'python3'

    cmd_print_path = 'import sys; print(":".join(sys.path))'

    pythonpath = execSync(`${python} -c '${cmd_print_path}'`).toString().trim()

    return pythonpath
}

// Task builders -----------------------------------------------------------

function unitTestsFor(project, taskBaseName=null){
    var taskName = makeTaskName(project, taskBaseName, 'UnitTests');

    return  munitTestsFor(`${project}/tests/unit`, taskName);
}

function integrationTestsFor(project, taskBaseName=null){
    var taskName = makeTaskName(project, taskBaseName, 'IntegrationTests');

    return munitTestsFor(`${project}/tests/integration`, taskName);
}

function makeTaskName(project, taskBaseName, suffix){
    if(taskBaseName == null){
        var paths = project.split('/');
        taskBaseName = paths[paths.length - 1];
    }

    return `${taskBaseName}${suffix}`;
}

function munitTestsFor(project, taskName, args=[], lang='js'){
    var task = ()=>{
        return munitTest(project, args, lang);
    };
    task.displayName = taskName;

    return task;
}

// Helpers -----------------------------------------------------------------

function haxe(hxmlFile){
    dir  = path.dirname(hxmlFile)
    file = path.basename(hxmlFile)

    return  execPipe('haxe', ['--cwd', dir, file]);
}

function munitTest(project, args=[], lang='js'){
    const projectDir = packagePath(project);

    return execPipe(
            'lix', ['run', 'munit' ,'test'].concat(args).concat([`-${lang}`]),
            {cwd: projectDir})
}

async function awaitExecFinish(child){
    const exitCode = await new Promise( (resolve, reject) => {
        child.on('close', resolve);
    });

    if( exitCode) {
        throw new Error( `subprocess error exit ${exitCode}, ${error}`);
    }

    return true;
}

function execPipe(cmd, args, options=undefined){
    var child = spawn(cmd, args, options)

    if(child.stdout){
        child.stdout.pipe(process.stdout);
    }
    if(child.stderr){
        child.stderr.pipe(process.stderr);
    }

    return child
}

function packagePath(...pathSegments){
    return projectPath('packages', ...pathSegments);
}

function projectPath(...pathSegments){
    return path.join(__dirname, ...pathSegments)
}

// Mount lix local libs ------------------------------------------------------


async function mountLocalLib(lib_dir, skip_if_exists=false){
    const lib_home = packagePath(lib_dir);
    const lib      = await loadLib(lib_home);
    const libfile  = projectPath('haxe_libraries', `${lib.name}.hxml`);

    if(skip_if_exists && await exists(libfile)){
        return libfile;
    }

    const content = makeLixLibContent(lib)

    console.debug(`creating local lib file "${libfile}":\n${content}`);

    return new Promise((resolve, reject)=>{
        fs.writeFile(libfile, content, (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve(libfile);
            }
        });
    });
}

function exists(filepath){
    return fs.promises.access(filepath, fs.constants.F_OK)
           .then(() => true)
           .catch(() => false)
}

async function loadLib(lib_home){
    const haxelib_file = path.join(lib_home, 'haxelib.json');
    const libJson  = await readJsonFile(haxelib_file);

    if(libJson.classPath){
        libJson.path = path.join(lib_home, libJson.classPath);
    }
    else{
        libJson.path = null
    }

    return libJson;
}

function readJsonFile(filepath){
    return new Promise((resolve, reject)=>{
        fs.readFile(filepath, 'utf8', function (err, data) {
            if (err) reject(err);

            try {
                const obj = JSON.parse(data);
                resolve(obj);
            } catch (error) {
                reject(error);
            }
        });
    })
}



function makeLixLibContent(lib){
    const libs = makeLibsInclude(lib);

    var lines = [
        '#',
        libs,
        `-D ${lib.name}=${lib.version}`
    ];

    if(lib.path){
        lines.push(`-cp ${lib.path}`)
    }

    return lines.join('\n');
}

function makeLibsInclude(lib){
    var libs = [];

    for (const dep in lib.dependencies) {
        libs.push(`-lib ${dep}`);
    }

    return libs.join('\n');
}