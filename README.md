# Interdimensional Service Discovery (ISD)

ISD integrates heterogeneous discovery environments and protocols through
an unified and extensible interface.

## Overview

### Why?

Multiple service discovery protocols (SDPs) has been developed,
targetting diverse network environments (like local networks, peer-to-peer overlays or IoT environments).
However, these protocols usually does not interoperate with each other.

So, in order to discover services in these heterogeneous environments (for example discover through local networks and the Internet),
we need a way to integrate these specialized SDPs
(since we cannot ensure convergence into only one protocol).

To handle this problem, we proposed the Interdimensional Service Discovery (ISD) as a way to integrate heterogeneous SDPs and network environments.


### How?

ISD combines multiple service discovery mechanisms,
each of them may implement a different service discovery protocol.
These discovery mechanisms are executed concurrently,
but their results are combined by the ISD _core_
and presented to applications through an unified interface.

ISD also defines an identity mechanism, based on cryptographic identifiers,
which allows to generate and authenticate globally unique identifiers,
without centralized third parties.

These identifiers may be represented as URLs like that:

`srv://Ow79.my-service.http.6VRbV3JN6HC0WUZrZQA7Kd9EUQbu-3V7yWJkjUa0xh8=`

Where `6VRbV3JN6HC0WUZrZQA7Kd9EUQbu-3V7yWJkjUa0xh8=` is the _provider ID_, defined as the fingerprint of the provider public key, `http` is the type of the service, `my-service` is the service name and `Ow79` is an optional instance identifier (which allows to differentiate between multiple executions of the same service).

To allow the discovery of the provider's key (used to authenticate the discovered services), ISD also integrates multiple _key distribution_ mechanisms,
which are similar to discovery mechanisms,
but are responsible to publish and retrieve cryptographic keys.

-------------------------------------------------------------------------------

## How to Use

ISD was implemented through some Haxe¹ libraries (see the `packages` directory):

* **core**: defines the unified interface and integrates the discovery and key distribution mechanisms. Also implements the common functionalities of the project.
* **mdns**: implements a discovery and a key distribution mechanism based on the mDNS/DNS-SD protocol (Multicast DNS over DNS Service Discovery).
* **dht**: offer a discovery and a key distribution mechanism based on a generic model for DHTs (Distributed Hash Tables), a kind of P2P overlay, which allows a decentralized discovery through the Internet.
    * > Includes also a `dht-bootstrapper` library and cli.
* **bundle**: simplifies the instantiation of an ISD instance, based on the currently implemented mechanisms and components.

> 1 - Haxe (haxe.org) is a high level programming language which may be compiled to other programming languages (targets), like C++, Python, Java or Javascript.

The currently implemented libraries targets Javascript (with focus on NodeJS).
But new components may be implemented to allow port ISD, and the developed mechanisms, to other languages.

* **TO BE DONE**:
    * details the API and other implementation details;
    * explain how to import the library (using Lyx package manager);
    * document the developed components;

-------------------------------------------------------------------------------

## How to Build and Test

### Quick Start

* **install**: `npm install` or `yarn install`
* **build** (cli): `npm run build` or `yarn run build`
* **unit test**: `npm test` or `yarn test`
* **robot tests**: `npm run robotTests` or `yarn run robotTests`

-------------------------------------------------------------------------------

### Installation Requirements

* [NodeJS](https://nodejs.org)
* [npm][npm_docs] or [yarn][yarn_docs]
  * npm should come with node
  * yarn may be installed from npm
* [npx][npx_docs] (should be part of npm)

[yarn_docs]: https://yarnpkg.com/
[npm_docs]: https://docs.npmjs.com/cli/v7
[npx_docs]: https://docs.npmjs.com/cli/v7/commands/npx


--------------------------------------------------------------------------------

### Installation

Run `npm install` or `yarn install` (from project folder).

> This will automatically install `lix` and `haxe` locally with the required dependencies.


#### Optional: lix and haxe global

[Lix][lix_docs] is a package manager for `haxe`.

If you want run _lix_ or _haxe_ commands manually, you can install them using `npm` or `yarn`:

`npm install --global lix`

or

`yarn global add lix`

This will install both lix and haxe globally.


[lix_docs]: https://github.com/lix-pm/lix.client


#### Optional: dependencies to system-level tests

The project uses [robot framework][robot_home], an open source automation framework, to create and run system-level tests.

To install `robot` and other dependencies used, run:

`pip install -r requirements.txt`


[robot_home]: https://robotframework.org/


#### Optional: dependencies to simulations

[Core emulator][core_home] is used (together with `robot`) to test the solution in a virtual network.
**Note** that `core` runs only on Linux.

To install core, see the [instructions][core_installation] on core homepage.

[core_home]: https://coreemu.github.io/core/
[core_installation]: https://coreemu.github.io/core/install.html


--------------------------------------------------------------------------------

### Build and Test

The build and test can be run using [gulp][gulp_home] or, indirectly, through `npm`|`yarn`.

> **Note** that `gulp` can be runned directly (if globally installed) or through `npx` (i.e. `npx gulp ...`)

* **build** (cli): `gulp build` or `npm|yarn run build`
* **unit test**: `gulp test` or `npm|yarn test`
* **robot tests**: `gulp robotTests` or `npm|yarn run robotTests`


[gulp_home]: https://gulpjs.com/


------------------------------------------------------------------------------

###  Running simulations

In order to run the simulations, the `core-daemon` must be running.
It can be started using:

`sudo service core-daemon start`

Once the daemon is running, the simulations can be executed using:

`yarn|npm run simulations` or `gulp simulations`


#### Warning: conflicts with docker

> **QUICKSTART**: use script `simulations/enable-packet-forwarding`
> before running simulations. This should enable the forwarding of
> ip packets and fix issues with docker.
>
> Note that the changes are not persisted across sessions (this is for more safety).

When docker is installed, by default it will block the packet routing,
which prevents the exchange of (IPv4) packets inside _core_.

So, in order to fix this issue, one approach is to add the following rule to `iptables`:

`iptables -I DOCKER-USER -i src_if -o dst_if -j ACCEPT`

as described by [docker documents on iptables][docker_iptables_router].

> It may be also necessary to allow port forwarding, using:
> `iptables -P FORWARD ACCEPT`

> To permanently save iptables rules, it is possible to use `iptables-persistent` package in debian/ubuntu based systems, as described [here][iptables_save_permanently] or [here][askubuntu_save_iptables].


Another approach, [recommended by _core_][core_docker_example], is to disable iptables (and bridges)
of docker, by adding the following file into `/etc/docker/daemon.json`:


```json

{
	"bridge": "none",
	"iptables": false
}

```

**However**, this prevents docker containers from communicate with the Internet.
So it may not be desirable.



[docker_iptables_router]: https://docs.docker.com/network/iptables/#docker-on-a-router
[iptables_save_permanently]: https://www.thomas-krenn.com/en/wiki/Saving_Iptables_Firewall_Rules_Permanently
[askubuntu_save_iptables]: https://askubuntu.com/questions/84781/iptables-resets-when-server-reboots
[core_docker_example]: https://github.com/coreemu/core/blob/master/daemon/examples/docker/README.md