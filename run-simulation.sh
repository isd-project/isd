#!/bin/bash

script_dir=`dirname "$(realpath $0)"`
simulation_path="$script_dir/simulations/scripts/$1.py"
libs_dir="$script_dir/simulations/src/robot-libs"


export PYTHONPATH="$libs_dir"

core-python $simulation_path