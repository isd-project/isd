#!/usr/bin/env python3

import os
import sys

REPETITIONS = 5

def main():
    cmd_args = sys.argv[1:]
    cmd      = ' '.join(cmd_args)

    for i in range(REPETITIONS):
        print('count {}:\n'.format(i+1))

        exit_code = os.system(cmd)
        if exit_code != 0:
            print('failed at execution {}'.format(i + 1))

            sys.exit((exit_code))

main()