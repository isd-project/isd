from os import path
from os.path import dirname

from core_robot import Core


class Pings(object):

    def __init__(self):
        net_dir = self._networks_dir()
        print(net_dir)

        self.core = Core(net_dir)

    def run(self):
        try:
            self.core.open_network('wired-LAN')
            self.run_session()
        finally:
            self.core.close()

    def run_session(self):
        n1 = self.core.get_node('n1')
        n2 = self.core.get_node('n2')

        self.ping(n1, n2)
        self.ping(n1, n2, 'ipv6')
        self.ping(n1, n2, 'ipv6')

        # input('press enter to finish')


    def ping(self, node1, node2, ipfamily=None):
        ip = node2.get_ip(ipfamily)

        cmd = ['ping', ip, '-c', '1']

        if ipfamily == 'ipv6':
            cmd.extend(['-6', '-w', '3'])

        print(' '.join(cmd))
        print(node1.run_command(*cmd))

    def _networks_dir(self):
        script_dir = path.dirname(__file__)
        networks_dir = path.join(script_dir, '..', 'network-files')

        return path.abspath(networks_dir)


def main():
    script = Pings()
    script.run()

main()