from .projectpaths import project_path

from os import path

def networks_dir():
    return project_path('simulations', 'network-files')

def networks_file(filename):
    return path.join(networks_dir(), filename)

def wired_lan_network():
    return networks_file('wired-LAN.xml')

def testdata_path(*segments):
    return project_path('testdata', *segments)

def test_keysdir():
    return testdata_path('keys')

def single_network_exp_file():
    return testdata_path('experiment', 'experiment-h2-n1-sv2-s2-l2-r0.yaml')

def two_networks_exp_file():
    return testdata_path('experiment', 'experiment-h2-n2-sv2-s2-l2-r0.yaml')