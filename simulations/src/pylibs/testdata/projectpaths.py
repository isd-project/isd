from os import path
from os.path import dirname


def _project_root():
    script_dir = path.dirname(__file__)
    root_relative = path.join(script_dir, '..', '..', '..', '..')

    return path.abspath(root_relative)


_root_project_path = _project_root()

def project_root():
    return _root_project_path

def project_path(*segments):
    return path.join(project_root(), *segments)

def packages_path(*segments):
    return project_path('packages', *segments)