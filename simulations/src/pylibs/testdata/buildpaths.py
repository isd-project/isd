from .projectpaths import project_path, packages_path

def experiment_node_path():
    return packages_path(
        'experiments', 'experiment-node', 'build', 'experiment-node.js')

def cli_app_path():
    return packages_path('cli', 'cli-app', 'build', 'cli.js')

def dht_bootstrapper_path():
    return packages_path('dht', 'dht-bootstrapper', 'build', 'bootstrapper.js')