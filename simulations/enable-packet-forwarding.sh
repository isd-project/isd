#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

set -x

sysctl -w net.ipv4.ip_forward=1
sysctl -w net.ipv6.conf.all.forwarding=1

# iptables: accepting forwarding packages
iptables -P FORWARD ACCEPT

# iptables: remove docker forwarding block
iptables -I DOCKER-USER -i src_if -o dst_if -j ACCEPT