*** Variables ***
${SIMU_LIBS_DIR}     ${CURDIR}/src/robot-libs
${CLI_LIBS_DIR}      ${CURDIR}/../packages/cli/robot-tests/libs

*** Settings ***
Suite Setup    Configure sys path


*** Keywords ***
Configure sys path
    evaluate    sys.path.append("${SIMU_LIBS_DIR}")    modules=os, sys
    evaluate    sys.path.append("${CLI_LIBS_DIR}")    modules=os, sys
