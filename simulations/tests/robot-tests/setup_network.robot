*** Settings ***
Documentation     Testing capalities of creating and configuring session.
Resource          ../../common.resource
Test Teardown     close session

*** Test Cases ***
Open network file
    Open network                wired-LAN
    Then check network started

Ping in LAN
    Given a network                          wired-LAN
    ${ip}=  Given the ip of                  node=n1
    When starting command in node            ping  ${ip}  -c  1     node=n2
    Then these messages should be print by
    ...    PING ${ip}(.)*bytes of data.
    ...    \\d* bytes from ${ip}: icmp_seq\=\\d* ttl\=\\d* time\=[\\d\\.]*
    ...    node=n2

Ping IPv6 in LAN
    Given a network                          wired-LAN
    ${ip}=  Given the ip of                  node=n1    family=ipv6
    When starting command in node            ping  -6   ${ip}  -c  1  -w   3
    ...                                      node=n2
    Then these messages should be print by
    ...    PING ${ip}(.)*data bytes
    ...    \\d* bytes from ${ip}:.*
    ...    node=n2
