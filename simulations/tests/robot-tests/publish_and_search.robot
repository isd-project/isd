*** Settings ***
Documentation     Testing capalities of creating and configuring session.
Resource          ../../common.resource
Test Teardown     close session

*** Variables ***
${srv_name}    my service
${srv_type}    http
${srv_port}    1234

*** Test Cases ***
Publish and find service by type
    [Tags]    debug
    Given a network                            wired-LAN
    When publishing a service in node          ${srv_name}  ${srv_type}
    ...                                        ${srv_port}             node=n1
    And searching for a service in other node  type=http               node=n2
    Then this service should be found by                               node=n2
    ...    name=${srv_name}  type=${srv_type}  port=${srv_port}


*** Keywords ***
starting cli in node with
  [Arguments]                      @{args}    ${node}=    ${timeout}=${4}
  When starting command in node    node  ${cli_script}  @{args}    node=${node}
  ...                                    timeout=${timeout}

publishing a service in node
  [Arguments]                ${name}=  ${type}=  ${port}=  ${node}=
  starting cli in node with  publish  ${name}   ${type}   ${port}  node=${node}

searching for a service in other node
  [Arguments]                         ${type}=   ${node}=
  starting cli in node with  search   ${type}    node=${node}

this service should be found by
  [Arguments]                         ${name}=  ${type}=  ${port}=  ${node}=
  Then these messages should be print by
  ...           Found service: ${name}
  ...           type: ${type}
  ...           addresses:
  ...           node=${node}
